#!/bin/bash

if [ $# -eq 0 ]; then
	BUILD_VERSION=dev
else
	BUILD_VERSION=$1
fi

# Update git
git pull

# Build image
docker build -t registry.gitlab.com/inetsdi/manage-service-plus-portal:$BUILD_VERSION .

# Login to registry.gitlab.com
docker login registry.gitlab.com

# Push image
docker push registry.gitlab.com/inetsdi/manage-service-plus-portal:$BUILD_VERSION
