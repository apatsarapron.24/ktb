export interface NavData {
    name?: string;
    url?: string;
    icon?: string;
    badge?: any;
    title?: boolean;
    children?: any;
    variant?: string;
    attributes?: object;
    divider?: boolean;
    class?: string;
}

export const navItems: NavData[] = [
    {
        name: 'Customers',
        url: '/admin/customers',
        icon: 'icon-people'
    },
    // {
    //     name: 'Tickets',
    //     url: '/admin/tickets',
    //     icon: 'icon-tag'
    // },
    {
        name: 'Users',
        url: '/admin/users',
        icon: 'icon-user'
    },
];
