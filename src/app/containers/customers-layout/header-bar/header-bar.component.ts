import { ChartsModule } from 'ng2-charts/ng2-charts';
import { Component, OnInit, ViewChild } from '@angular/core';
import { headerItems } from './header';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { AuthenticationService } from '../../../authener/_service/authentication.service';
import { RequestService } from '../../../services/request/request.service';
import { element } from '@angular/core/src/render3';
import { MatDialog } from '@angular/material/dialog';
import { AlertLoginComponent } from '../../../authener/alert-login/alert-login.component';
import { Subscription } from 'rxjs';

import { RiskService } from '../../../services/comment-point/risk/risk.service';
import { RetailService } from '../../../services/comment-point/retail/retail.service';
import { OperationService } from '../../../services/comment-point/operation/operation.service';
import { FinanceService } from '../../../services/comment-point/finance/finance.service';
import { ComplianceService } from './../../../services/comment-point/compliance/compliance.service';

import { SummaryOfConsiderationService } from '../../../services/comment-and-approval/summary-of-consideration/summary-of-consideration.service';
import { ConsiderBoardService } from '../../../services/consider-board/consider-board.service';
import { AgendaAddService } from '../../../services/agenda/agenda-add/agenda-add.service';
import { ManageSystemService } from '../../../services/manage-system/manage-system.service';
import { CommentService } from '../../../services/comment/comment.service';
import { SysManagementRulesService } from '../../../services/sys-management-rules/sys-management-rules.service';
import { ProductAnnouncementService } from '../../../services/product-announcement/product-announcement.service';
import { OutstandingService } from '../../../services/outstanding/outstanding.service';
import { ProductPerformanceService } from '../../../services/product-performance/product-performance.service';
@Component({
  selector: 'app-header-bar',
  templateUrl: './header-bar.component.html',
  styleUrls: ['./header-bar.component.scss']
})
export class HeaderBarComponent implements OnInit {

  public activeLink: any;
  eventSubscripHeaderBar: Subscription;
  role: any;
  role_detail: any;
  fullname: any;
  level: any;
  headernamelist: any = [];
  headername: any = [];
  username: any;
  sub_menu: [];
  id_menu: string;
  tab_sub_menu = false;
  requestId: any;
  // ========= sub menu ======
  listMainPage = [];
  listInformation = [];
  listIssue = [];
  listComment = [];
  listApprove = [];

  status_loading = false;
  // count = 30 * 60 * 1000;
  // mins = 0;
  // seconds = 0;

  constructor(private router: Router,
    private auth: AuthenticationService,
    private request: RequestService,
    private dialog: MatDialog,
    private Risk: RiskService,
    private Retail: RetailService,
    private Operation: OperationService,
    private Finance: FinanceService,
    private Compliance: ComplianceService,
    private SummaryOfConsideration: SummaryOfConsiderationService,
    private AgendaAdd: AgendaAddService,
    private ConsiderBoard: ConsiderBoardService,
    private ManageSystem: ManageSystemService,
    private Comment: CommentService,
    private SysManagementRules: SysManagementRulesService,
    private ProductAnnouncement: ProductAnnouncementService,
    private outstandingService: OutstandingService,
    private ProductPerformance: ProductPerformanceService
  ) {

    this.router.events.subscribe(
      (event: any) => {
        // console.log("event:", event)
        if (event instanceof NavigationStart) {
          // for start loading
          this.status_loading = true;
          // console.log('Change Path',this.Risk.status_state )
          if (this.Risk.status_state == true ||
            this.Retail.status_state == true ||
            this.Operation.status_state == true ||
            this.Finance.status_state == true ||
            this.Compliance.status_state == true ||
            this.request.outPageStatus() == true ||
            this.SummaryOfConsideration.status_state == true ||
            this.AgendaAdd.status_state == true ||
            this.ConsiderBoard.status_state == true ||
            this.ManageSystem.status_state == true ||
            this.Comment.status_state == true ||
            this.SysManagementRules.status_state == true ||
            this.ProductAnnouncement.status_state == true ||
            this.outstandingService.status_state == true ||
            this.ProductPerformance.status_state == true) {
            // console.log('Change Path', this.status_loading)
            this.status_loading = false;
          }
        }

        //  else {
        //   this.status_loading = false;
        // }
        if (event instanceof NavigationEnd) {
          // for end loading
          // กรณีที่ user ที่กำลังใช้งานฟังก์ชั่นใบคำขอ ต้องการกลับมาที่หน้า ติดตามงาน
          this.status_loading = false;
          if (this.router.url === '/dashboard1' && localStorage.getItem('requestId') !== '') {
            this.loadData();
          } else {
            this.loadData();
            console.log('this.router.url', this.router.url)
           
          }

        }
        // else {
        //   this.status_loading = false;
        // }
      }
    );

    this.eventSubscripHeaderBar = this.request.getEvent().subscribe(() => {
      // console.log('eventSubscripHeaderBar????')
      this.loadData();
    });
  }
  clickHeaderBar(url) {
    this.request.OtherHeaderBar(url)
  }
  ngOnInit() {
    window.addEventListener('load', () => {
      this.auth.mins = Math.floor((Number(localStorage.getItem('countTimer'))) / 60);
      this.auth.seconds = Number(localStorage.getItem('countTimer')) % 60;
      const time = setInterval(() => {
        this.auth.timnew = Number(new Date().getHours() * 60 * 60) +
          Number(new Date().getMinutes() * 60) + Number(new Date().getSeconds());
        // console.log('login::', sessionStorage.getItem('timeLogin'));
        // console.log('timer::', this.auth.timnew);
        this.auth.TimerCountdown = this.auth.timnew - Number(localStorage.getItem('timeLogin'));
        // console.log('TimerCountdown::', this.auth.TimerCountdown);
        this.auth.mins = Math.floor((1800 - this.auth.TimerCountdown) / 60);
        this.auth.seconds = (1800 - this.auth.TimerCountdown) % 60;
        // console.log('mins::', this.auth.mins);
        // console.log('seconds::', this.auth.seconds);
        if ((1800 - this.auth.TimerCountdown) <= 0) {
          clearInterval(time);
          this.dialog.open(AlertLoginComponent, {
            data: {
              time: Number(1800 - this.auth.TimerCountdown)
            }
          }).afterClosed().subscribe(result => {
            if (result === 'logout') {
              this.auth.logout();
            } else if (result === 'refresh') {
              localStorage.setItem('timeLogin',
                String(
                  Number(new Date().getHours() * 60 * 60) +
                  Number(new Date().getMinutes() * 60) +
                  Number(new Date().getSeconds())
                )
              );
              localStorage.setItem('Time',
                String(
                  new Date().getHours() + ':' +
                  new Date().getMinutes() + ':' +
                  new Date().getSeconds()
                )
              );
              this.auth.countTimer();
            }
          },(err) => {
            console.log(err)
            if (err === 'logout') {
              this.dialog.closeAll();
              this.auth.logout();
            } else {
              this.dialog.closeAll();
              localStorage.setItem('timeLogin',
                String(
                  Number(new Date().getHours() * 60 * 60) +
                  Number(new Date().getMinutes() * 60) +
                  Number(new Date().getSeconds())
                )
              );
              localStorage.setItem('Time',
                String(
                  new Date().getHours() + ':' +
                  new Date().getMinutes() + ':' +
                  new Date().getSeconds()
                )
              );
              this.auth.countTimer();
            }
          });
        }
        localStorage.setItem('countTimer', String(1800 - this.auth.TimerCountdown));
      }, 1000);
    });

    this.loadData();
  }


  loadData() {
    console.log('headerItems', headerItems);
    if (this.sub_menu === undefined) {
      this.tab_sub_menu = false;
    } else {
      this.tab_sub_menu = true;
    }
    this.username = localStorage.getItem('uid');
    this.role = localStorage.getItem('role');
    this.level = localStorage.getItem('level');
    this.role_detail = localStorage.getItem('role_detail');
    this.fullname = localStorage.getItem('fullname');
    console.log('this.level', this.level);

    if (location.pathname === '/dashboard1') {
      this.requestId = '';
    } else {
      this.requestId = localStorage.getItem('requestId');
    }

    console.log('requestId oninit : ', this.requestId);
    // ============================ permission menu ==================
    this.request.checkMenu(this.requestId).subscribe(res => {
      console.log('res menu', res['data']);
      const listMainMenu = [];
      // tslint:disable-next-line: no-shadowed-variable
      res['data'].forEach(element => {
        listMainMenu.push(element.name);
        if (element.name === 'หน้าหลัก') {
          element.children.forEach(item => {
            this.listMainPage.push(item.name);
          });
          console.log('หน้าหลัก', this.listMainPage);
        }
        if (element.name === 'ข้อมูลเบื้องต้น') {
          element.children.forEach(item => {
            this.listInformation.push(item.name);
          });
          console.log('ข้อมูลเบื้องต้น', this.listInformation);
        }
        if (element.name === 'ประเด็นความเห็น') {
          element.children.forEach(item => {
            this.listIssue.push(item.name);
          });
          console.log('ประเด็นความเห็น', this.listIssue);
        }
        if (element.name === 'ความเห็นและการอนุมัติ') {
          element.children.forEach(item => {
            this.listComment.push(item.name);
          });
          console.log('ความเห็นและการอนุมัติ', this.listComment);
        }
        if (element.name === 'ข้อมูลหลังการอนุมัติ') {
          element.children.forEach(item => {
            this.listApprove.push(item.name);
          });
          console.log('ข้อมูลหลังการอนุมัติ', this.listApprove);
        }
        console.log('listMainMenu', listMainMenu);
        this.permissonPage(listMainMenu);
      });
    });

    // this.permissonPage(listMainPage);

    this.activeLink = this.headernamelist[0];
  }

  permissonPage(list) {
    const headerItemsTemp = headerItems;
    // tslint:disable-next-line: no-shadowed-variable
    this.headernamelist = headerItemsTemp.map(element => ({ ...element }));
    console.log(this.headernamelist, headerItemsTemp[0].children);

    if (this.listMainPage.length !== 0) {
      this.headernamelist[0].children = headerItemsTemp[0].children.filter(child => {
        return this.listMainPage.includes(child.name);
      });
      // ============================ permission active ==============================
      if (this.role === 'PC') {
        const listFilter = ['/comments-finance', '/system-management-customer'];
        this.filterMenu(this.headernamelist, listFilter);
      } else if (this.role === 'Finance') {
        const listFilter = ['/system-management', '/system-management-customer'];
        this.filterMenu(this.headernamelist, listFilter);
      } else if (this.role === 'Compliance' || this.role === 'ฝ่ายกำกับระเบียบและการปฏิบัติงาน'
        || this.role === 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ทางการ'
        || this.role === 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ธุรกิจ 1'
        || this.role === 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ธุรกิจ 2'
        || this.role === 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ธุรกิจ 2'
        || this.role === 'ฝ่ายนิติการ') {
        const listFilter = ['/system-management', '/comments-finance'];
        this.filterMenu(this.headernamelist, listFilter);
      } else {
        const listFilter = ['/system-management', '/comments-finance', '/system-management-customer'];
        this.filterMenu(this.headernamelist, listFilter);
      }
    }

    if (this.listInformation.length !== 0) {
      this.headernamelist[1].children = headerItemsTemp[1].children.filter(child => {
        // console.log('listInformation', child)
        return this.listInformation.includes(child.name);
      });
    }

    if (this.listIssue.length !== 0) {
      // ================= permission Issue ================
      if (this.role === 'Risk') {
        const listDisActive = ['Compliance', 'Operation', 'Finance', 'Retail Shared Services'];
        this.permissionCommentPoint(listDisActive);
      } else if (this.role === 'Compliance' || this.role === 'ฝ่ายกำกับระเบียบและการปฏิบัติงาน'
        || this.role === 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ทางการ'
        || this.role === 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ธุรกิจ 1'
        || this.role === 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ธุรกิจ 2'
        || this.role === 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ธุรกิจ 2'
        || this.role === 'ฝ่ายนิติการ') {
        const listDisActive = ['Risk', 'Operation', 'Finance', 'Retail Shared Services'];
        this.permissionCommentPoint(listDisActive);
      } else if (this.role === 'Operation') {
        const listDisActive = ['Risk', 'Compliance', 'Finance', 'Retail Shared Services'];
        this.permissionCommentPoint(listDisActive);
      } else if (this.role === 'Finance') {
        const listDisActive = ['Risk', 'Operation', 'Compliance', 'Retail Shared Services'];
        this.permissionCommentPoint(listDisActive);
      } else if (this.role === 'Retail') {
        const listDisActive = ['Risk', 'Operation', 'Compliance', 'Finance'];
        this.permissionCommentPoint(listDisActive);
      } else {
        const listDisActive = [];
        this.permissionCommentPoint(listDisActive);
      }
    }

    if (this.listComment.length !== 0) {
      this.headernamelist[3].children = headerItemsTemp[3].children.filter(child => {
        // console.log('listInformation', child)
        return this.listComment.includes(child.name);
      });
    }

    if (this.listApprove.length !== 0) {
      this.headernamelist[4].children = headerItemsTemp[4].children.filter(child => {
        // console.log('listInformation', child)
        return this.listApprove.includes(child.name);
      });
    }



    this.headernamelist = this.headernamelist.filter(name => {
      console.log('menu', list.includes(name.name));
      return list.includes(name.name);
    });


    console.log(this.headernamelist);
  }


  listSubMenu(id, index) {
    this.activeLink = this.headernamelist[id];
    this.sub_menu = this.headernamelist[index].children;
    this.id_menu = id;
    if (this.sub_menu === undefined) {
      this.tab_sub_menu = false;
    } else {
      this.tab_sub_menu = true;
    }
  }

  permissionCommentPoint(listDisActive) {
    // tslint:disable-next-line: no-shadowed-variable
    this.headernamelist[2].children.forEach(element => {
      if (listDisActive.includes(element.name) === true) {
        element.active = false;
      }
    });
  }



  filterMenu(headerItemsTemp, listFilter) {
    this.headernamelist[0].children = headerItemsTemp[0].children.filter(child => {
      console.log(' includes ', !listFilter.includes(child.url));
      return !listFilter.includes(child.url);
    });


  }

  Logout() {
    this.auth.logout();
  }



}
