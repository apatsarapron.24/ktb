export interface Header {
    name?: string;
    url?: string;
    icon?: string;
    id?: string;
    badge?: any;
    title?: boolean;
    children?: any;
    variant?: string;
    attributes?: object;
    divider?: boolean;
    class?: string;
}


export const headerItems: Header[] = [
    {
        name: 'หน้าหลัก',
        url: '/dashboard1',
        icon: 'icon-home',
        id: '1b',
        children: [
            {
                name: 'ติดตามงาน',
                url: '/dashboard1',
                active: true,
            },
            {
                name: 'รายงาน',
                url: '/reports-tab',
                active: true
                // icon: 'icon-cursor'
            },
            {
                name: 'จัดการวาระ',
                url: '/agenda-manage',
                active: true
                // icon: 'icon-cursor'
            },
            {
                name: 'จัดการการแจ้งเตือน',
                url: '/notification-mgmt',
                active: true
                // icon: 'icon-cursor'
            },
            {
                name: 'จัดการข้อมูลในระบบ',
                url: '/system-management',
                active: true
                // icon: 'PC'
            },
            {
                name: 'จัดการข้อมูลในระบบ',
                url: '/comments-finance',
                active: true

                //finance
            },
            {
                name: 'จัดการข้อมูลในระบบ',
                url: '/system-management-customer',
                active: true
                //com
            },
            {
                name: 'กำหนดสิทธิ์ผู้ใช้งาน',
                url: '/manage-permission',
                active: true
                // icon: 'icon-cursor'
            },
            {
                name: 'จัดการประกาศข่าว',
                url: '/news-announcement',
                active: true
                // icon: 'icon-cursor'
            },
            {
                name: 'ประกาศข่าว',
                url: '/product-process-system',
                active: true
                // icon: 'icon-cursor'
            },
        ]
    },
    {
        name: 'ข้อมูลเบื้องต้น',
        url: '/save-manager',
        icon: 'icon-home',
        id: '2b',
        children: [
            {
                name: 'Executive Summary',
                url: '/executive-summary'
            },
            {
                name: 'บันทึกผู้ดำเนินการ',
                url: '/save-manager'
            },
            {
                name: 'รายละเอียดคำขอ',
                url: '/request/information-base/step1'
            },
            {
                name: 'ความเห็น/ส่งงาน',
                url: '/comment'
            },
            {
                name: 'บรรจุวาระ',
                url: '/agenda-add'
            },
        ]
    },
    {
        name: 'ประเด็นความเห็น',
        url: '/risk',
        icon: 'icon-home',
        id: '2b',
        children: [
            {
                name: 'Risk',
                url: '/comment-point/risk',
                active: true
            },
            {
                name: 'Compliance',
                url: '/comment-point/compliance',
                active: true
            },
            {
                name: 'Operation',
                url: '/comment-point/operation',
                active: true
            },
            {
                name: 'Finance',
                url: '/comment-point/finance',
                active: true
            },
            {
                name: 'Retail Shared Services',
                url: '/comment-point/retail',
                active: true
            },
        ]
    },
    {
        name: 'ความเห็นและการอนุมัติ',
        url: '',
        icon: 'icon-home',
        id: '2b',
        children: [
            {
                name: 'ผลการพิจารณา',
                url: '/consider-board',
                active: true
            },
            {
                name: 'สรุปผลการพิจารณา',
                url: '/comment-and-approval/summary-of-consideration',
                active: true
            },
        ]
    },
    {
        name: 'ข้อมูลหลังการอนุมัติ',
        url: '',
        icon: 'icon-home',
        id: '2b',
        children: [
            {
                name: 'ประเด็นคงค้าง/สิ่งที่ต้องดำเนินการ',
                url: '/outstanding',
                active: true
            },
            {
                name: 'ข้อมูลประกาศใช้ผลิตภัณฑ์',
                url: '/product-announcement',
                active: true
            },

            {
                name: 'Product Performance',
                url: '/product-performance',
                active: true
            },

        ]
    }
];

