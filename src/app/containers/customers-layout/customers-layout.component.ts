import { Component, OnDestroy, Inject, ViewChild, ElementRef } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { navItems } from './_nav';

import { ResoiutiondataService } from '../../resoiutiondata.service';
import { headerItems } from '../customers-layout/header-bar/header';


@Component({
  selector: 'app-dashboard',
  templateUrl: './customers-layout.component.html',
  styleUrls: ['./customers-layout.component.scss']
})
export class CustomersLayoutComponent implements OnDestroy {
  // public navItems = navItems;
  public headerItems = headerItems;

  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement;

  headername: any = [];

  username = '';
  project_manager;
  customer_id = '';
  presale_name = '';
  sale_name = '';

  @ViewChild('draggable') private draggableElement: ElementRef;

  constructor(private resoiution: ResoiutiondataService, @Inject(DOCUMENT) _document?: any) {

    this.headername = headerItems;
    // console.log(this.headerItems);

    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = _document.body.classList.contains('sidebar-minimized');
    });
    this.element = _document.body;
    this.changes.observe(<Element>this.element, {
      attributes: true,
      attributeFilter: ['class']
    });


  }

  ngOnDestroy(): void {
    this.changes.disconnect();

  }

}
