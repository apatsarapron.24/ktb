export interface NavData {
    name?: string;
    url?: string;
    icon?: string;
    badge?: any;
    title?: boolean;
    children?: any;
    variant?: string;
    attributes?: object;
    divider?: boolean;
    class?: string;
}

export const navItems: NavData[] = [
    {
        name: 'Dashboard',
        url: '/example/dashboard',
        icon: 'icon-speedometer',
        badge: {
            variant: 'info',
            text: 'NEW'
        }
    },
    {
        name: 'Test',
        url: '/example/test'
    },
    {
        title: true,
        name: 'Theme'
    },
    {
        name: 'Colors',
        url: '/example/theme/colors',
        icon: 'icon-drop'
    },
    {
        name: 'Typography',
        url: '/example/theme/typography',
        icon: 'icon-pencil'
    },
    {
        title: true,
        name: 'Components'
    },
    {
        name: 'Base',
        url: '/example/base',
        icon: 'icon-puzzle',
        children: [
            {
                name: 'Cards',
                url: '/example/base/cards',
                icon: 'icon-puzzle'
            },
            {
                name: 'Carousels',
                url: '/example/base/carousels',
                icon: 'icon-puzzle'
            },
            {
                name: 'Collapses',
                url: '/example/base/collapses',
                icon: 'icon-puzzle'
            },
            {
                name: 'Forms',
                url: '/example/base/forms',
                icon: 'icon-puzzle'
            },
            {
                name: 'Pagination',
                url: '/example/base/paginations',
                icon: 'icon-puzzle'
            },
            {
                name: 'Popovers',
                url: '/example/base/popovers',
                icon: 'icon-puzzle'
            },
            {
                name: 'Progress',
                url: '/example/base/progress',
                icon: 'icon-puzzle'
            },
            {
                name: 'Switches',
                url: '/example/base/switches',
                icon: 'icon-puzzle'
            },
            {
                name: 'Tables',
                url: '/example/base/tables',
                icon: 'icon-puzzle'
            },
            {
                name: 'Tabs',
                url: '/example/base/tabs',
                icon: 'icon-puzzle'
            },
            {
                name: 'Tooltips',
                url: '/example/base/tooltips',
                icon: 'icon-puzzle'
            }
        ]
    },
    {
        name: 'Buttons',
        url: '/example/buttons',
        icon: 'icon-cursor',
        children: [
            {
                name: 'Buttons',
                url: '/example/buttons/buttons',
                icon: 'icon-cursor'
            },
            {
                name: 'Dropdowns',
                url: '/example/buttons/dropdowns',
                icon: 'icon-cursor'
            },
            {
                name: 'Brand Buttons',
                url: '/example/buttons/brand-buttons',
                icon: 'icon-cursor'
            }
        ]
    },
    {
        name: 'Charts',
        url: '/example/charts',
        icon: 'icon-pie-chart'
    },
    {
        name: 'Icons',
        url: '/example/icons',
        icon: 'icon-star',
        children: [
            {
                name: 'CoreUI Icons',
                url: '/example/icons/coreui-icons',
                icon: 'icon-star',
                badge: {
                    variant: 'success',
                    text: 'NEW'
                }
            },
            {
                name: 'Flags',
                url: '/example/icons/flags',
                icon: 'icon-star'
            },
            {
                name: 'Font Awesome',
                url: '/example/icons/font-awesome',
                icon: 'icon-star',
                badge: {
                    variant: 'secondary',
                    text: '4.7'
                }
            },
            {
                name: 'Simple Line Icons',
                url: '/example/icons/simple-line-icons',
                icon: 'icon-star'
            }
        ]
    },
    {
        name: 'Notifications',
        url: '/example/notifications',
        icon: 'icon-bell',
        children: [
            {
                name: 'Alerts',
                url: '/example/notifications/alerts',
                icon: 'icon-bell'
            },
            {
                name: 'Badges',
                url: '/example/notifications/badges',
                icon: 'icon-bell'
            },
            {
                name: 'Modals',
                url: '/example/notifications/modals',
                icon: 'icon-bell'
            }
        ]
    },
    {
        name: 'Widgets',
        url: '/example/widgets',
        icon: 'icon-calculator',
        badge: {
            variant: 'info',
            text: 'NEW'
        }
    },
    {
        divider: true
    },
    {
        title: true,
        name: 'Extras',
    },
    {
        name: 'Pages',
        url: '/example/pages',
        icon: 'icon-star',
        children: [
            {
                name: 'Login',
                url: '/login',
                icon: 'icon-star'
            },
            {
                name: 'Register',
                url: '/register',
                icon: 'icon-star'
            },
            {
                name: 'Error 404',
                url: '/404',
                icon: 'icon-star'
            },
            {
                name: 'Error 500',
                url: '/500',
                icon: 'icon-star'
            }
        ]
    },
    {
        name: 'Disabled',
        url: '/example/dashboard',
        icon: 'icon-ban',
        badge: {
            variant: 'secondary',
            text: 'NEW'
        },
        attributes: { disabled: true },
    },
    {
        name: 'Download CoreUI',
        url: 'http://coreui.io/angular/',
        icon: 'icon-cloud-download',
        class: 'mt-auto',
        variant: 'success',
        attributes: { target: '_blank', rel: 'noopener' }
    },
    {
        name: 'Try CoreUI PRO',
        url: 'http://coreui.io/pro/angular/',
        icon: 'icon-layers',
        variant: 'danger',
        attributes: { target: '_blank', rel: 'noopener' }
    }
];
