export interface NavData {
    name?: string;
    url?: string;
    icon?: string;
    badge?: any;
    title?: boolean;
    children?: any;
    variant?: string;
    attributes?: object;
    divider?: boolean;
    class?: string;
}

export const navItems: NavData[] = []
//     [
//         {
//             name: 'กลับสู่หน้าหลัก',
//             url: '/dashboard1',
//             icon: 'icon-arrow-left'
//         },
//         {
//             title: true,
//             name: 'ขั้นตอนการสร้างใบคำขอฯ',
//         },
//         {
//             sub_menu: 'ข้อมูลทั่วไป',
//             link: '/request/information-base/step1',
//             icon: 'icon-speedometer',
//             // icon: 'icon-circle',
//         },
//         {
//             sub_menu: 'นโยบายและแผนผลิตภัณฑ์',
//             link: '/request/information-base/step2',
//             icon: 'icon-speedometer',
//         },
//         {
//             sub_menu: 'อำนาจอนุมัติผลิตภัณฑ์',
//             link: '/request/information-base/step3',
//             icon: 'icon-speedometer',
//         },
//         {
//             sub_menu: 'ข้อมูลผู้ออกผลิตภัณฑ์',
//             link: '/request/information-base/step4',
//             icon: 'icon-speedometer',
//         },
//         {
//             title: true,
//             link: 'รายละเอียดผลิตภัณฑ์'
//         },
//         {
//             sub_menu: 'Business Model & Risk',
//             link: '/request/product-detail/step1',
//             icon: 'icon-speedometer',
//         },
//         {
//             sub_menu: 'สภาพตลาดและการแข่งขัน',
//             link: '/request/product-detail/step2',
//             icon: 'icon-speedometer',
//         },
//         {
//             sub_menu: 'รูปแบบผลิตภัณฑ์',
//             link: '/request/product-detail/step3',
//             icon: 'icon-speedometer',
//         },
//     ];

