import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarRequestComponent } from './sidebar-request.component';

describe('SidebarRequestComponent', () => {
  let component: SidebarRequestComponent;
  let fixture: ComponentFixture<SidebarRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidebarRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
