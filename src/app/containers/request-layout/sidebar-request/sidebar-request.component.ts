import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { navItems } from './_nav';
import { RequestService } from '../../../services/request/request.service';
import { Subscription } from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import { RequestDialogComponent } from '../../dialog/request-dialog/request-dialog.component';


@Component({
  selector: 'app-sidebar-request',
  templateUrl: './sidebar-request.component.html',
  styleUrls: ['./sidebar-request.component.scss']
})
export class SidebarRequestComponent implements OnInit {
  public navItems = navItems;
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement;
  eventSubscription: Subscription;

  requestId: any = localStorage.getItem('requestId');
  constructor(
    public dialog: MatDialog,
    @Inject(DOCUMENT) _document?: any,
    private statusRequest?: RequestService,
    ) {

    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = _document.body.classList.contains('sidebar-minimized');
    });
    this.element = _document.body;
    this.changes.observe(<Element>this.element, {
      attributes: true,
      attributeFilter: ['class']
    });

    // receive event trigger from another component to call statusSidebar
    this.eventSubscription = this.statusRequest.getEvent().subscribe(() => {
      this.checkStatus();
    });
  }

  isClicked = [];
  statusPage: any;
  checkDefault: any;
  render: any;

  menu = [
    {
      id: 'a', title: 'ข้อมูลพื้นฐาน',
      statusDefule: false,
      title_list:
        [
          { sub_menu: 'ข้อมูลทั่วไป', link: '/request/information-base/step1', status: false },
          { sub_menu: 'นโยบายและแผนผลิตภัณฑ์', link: '/request/information-base/step2', status: false },
          { sub_menu: 'อำนาจอนุมัติผลิตภัณฑ์', link: '/request/information-base/step3', status: false },
          { sub_menu: 'ข้อมูลผู้ออกผลิตภัณฑ์', link: '/request/information-base/step4', status: false },
        ]
    },
    {
      id: 'b', title: 'รายละเอียดผลิตภัณฑ์',
      statusDefule: true,
      title_list:
        [
          { sub_menu: 'Business Model & Risk', link: '/request/product-detail/step1', status: false },
          { sub_menu: 'สภาพตลาดและการแข่งขัน', link: '/request/product-detail/step2', status: false },
          { sub_menu: 'รูปแบบผลิตภัณฑ์', link: '/request/product-detail/step3', status: false },
          { sub_menu: 'กระบวนการทำงาน', link: '/request/product-detail/step4', status: false },
          { sub_menu: 'ความพร้อมด้านการดำเนินการ (Operation)', link: '/request/product-detail/step5', status: false },
          {
            sub_menu: 'ความพร้อมด้านการให้บริการลูกค้าอย่างเป็นธรรม (Market Conduct)', link: '/request/product-detail/step6',
            status: false
          },
          { sub_menu: 'ความพร้อมด้านเทคโนโลยีสารสนเทศ', link: '/request/product-detail/step7', status: false },
          { sub_menu: 'Cost & Benefit Analysis', link: '/request/product-detail/step8', status: false },
          { sub_menu: 'เป้าหมาย/ตัวชี้วัด', link: '/request/product-detail/step9', status: false },
          { sub_menu: 'ข้อมูลด้าน Finance', link: '/request/product-detail/step10', status: false },
          { sub_menu: 'ข้อมูลด้าน Compliance', link: '/request/product-detail/step11', status: false },
          { sub_menu: 'สรุปข้อเสนอเพื่อพิจารณา', link: '/request/product-detail/step12', status: false },
        ]
    },
  ];

  menuRepeat = [
    {
      id: 'a', title: 'ข้อมูลพื้นฐาน',
      statusDefule: false,
      title_list:
        [
          { sub_menu: 'ข้อมูลทั่วไป', link: '/request/information-base/step1', status: false },
          { sub_menu: 'นโยบายและแผนผลิตภัณฑ์', link: '/request/information-base/step2', status: false },
          { sub_menu: 'อำนาจอนุมัติผลิตภัณฑ์', link: '/request/information-base/step3', status: false },
          { sub_menu: 'ข้อมูลผู้ออกผลิตภัณฑ์', link: '/request/information-base/step4', status: false },
        ]
    },
    {
      id: 'b', title: 'รายละเอียดผลิตภัณฑ์',
      statusDefule: true,
      title_list:
        [
          { sub_menu: 'Business Model & Risk', link: '/request/product-detail/step1', status: false },
          { sub_menu: 'สภาพตลาดและการแข่งขัน', link: '/request/product-detail/step2', status: false },
          { sub_menu: 'รูปแบบผลิตภัณฑ์', link: '/request/product-detail/step3', status: false },
          { sub_menu: 'กระบวนการทำงาน', link: '/request/product-detail/step4', status: false },
          { sub_menu: 'ความพร้อมด้านการดำเนินการ (Operation)', link: '/request/product-detail/step5', status: false },
          {
            sub_menu: 'ความพร้อมด้านการให้บริการลูกค้าอย่างเป็นธรรม (Market Conduct)', link: '/request/product-detail/step6',
            status: false
          },
          { sub_menu: 'ความพร้อมด้านเทคโนโลยีสารสนเทศ', link: '/request/product-detail/step7', status: false },
          { sub_menu: 'Cost & Benefit Analysis', link: '/request/product-detail/step8', status: false },
          { sub_menu: 'เป้าหมาย/ตัวชี้วัด', link: '/request/product-detail/step9', status: false },
          { sub_menu: 'ข้อมูลด้าน Finance', link: '/request/product-detail/step10', status: false },
          { sub_menu: 'ข้อมูลด้าน Compliance', link: '/request/product-detail/step11', status: false },
          { sub_menu: 'สรุปข้อเสนอเพื่อพิจารณา', link: '/request/product-detail/step12', status: false },
          { sub_menu: 'ข้อมูลด้าน Compliance', link: '/request/product-detail/step13', status: false },
          { sub_menu: 'สรุปข้อเสนอเพื่อพิจารณา', link: '/request/product-detail/step14', status: false },
        ]
    },
  ];





  checkStatus() {
    if (localStorage.getItem('requestId') !== '') {
      this.statusRequest.statusSibar(localStorage.getItem('requestId')).subscribe(res => {
        console.log('sidebar step', res['data'].repeat);
        this.statusPage = res['data'];
        this.checkDefault = Object.values(this.statusPage.pageDefault).some(value => value === false);
        // console.log('trueuuu', this.checkDefault);
        if (res['data'].repeat === true) {
          if (res['data'].isUpdate === true) {
            this.menuRepeat = [
              {
                id: 'a', title: 'ข้อมูลพื้นฐาน',
                statusDefule: false,
                title_list:
                  [
                    { sub_menu: 'ข้อมูลทั่วไป', link: '/request-summary/information-base/step1', status: this.statusPage.pageDefault.page1 },
                    { sub_menu: 'นโยบายและแผนผลิตภัณฑ์', link: '/request-summary/information-base/step2', status: this.statusPage.pageDefault.page2 },
                    { sub_menu: 'อำนาจอนุมัติผลิตภัณฑ์', link: '/request-summary/information-base/step3', status: this.statusPage.pageDefault.page3 },
                    { sub_menu: 'ข้อมูลผู้ออกผลิตภัณฑ์', link: '/request-summary/information-base/step4', status: this.statusPage.pageDefault.page4 },
                  ]
              },
              {
                id: 'b', title: 'รายละเอียดผลิตภัณฑ์',
                statusDefule: this.checkDefault,
                title_list:
                  [
                    {
                      sub_menu: 'Business Model & Risk',
                      link: '/request-summary/product-detail/step1', status: this.statusPage.pageDetail.page1
                    },
                    {
                      sub_menu: 'ปัญหาอุปสรรคและการจัดการ',
                      link: '/request-summary/product-detail/step2', status: this.statusPage.pageDetail.page2
                    },
                    {
                      sub_menu: 'สภาพตลาดและการแข่งขัน',
                      link: '/request-summary/product-detail/step3', status: this.statusPage.pageDetail.page3
                    },
                    { sub_menu: 'รูปแบบผลิตภัณฑ์', link: '/request-summary/product-detail/step4', status: this.statusPage.pageDetail.page4 },
                    { sub_menu: 'กระบวนการทำงาน', link: '/request-summary/product-detail/step5', status: this.statusPage.pageDetail.page5 },
                    {
                      sub_menu: 'ความพร้อมด้านการดำเนินการ (Operation)', link: '/request-summary/product-detail/step6',
                      status: this.statusPage.pageDetail.page6
                    },
                    {
                      sub_menu: 'ความพร้อมด้านการให้บริการลูกค้าอย่างเป็นธรรม (Market Conduct)',
                      link: '/request-summary/product-detail/step7',
                      status: this.statusPage.pageDetail.page7
                    },
                    {
                      sub_menu: 'ความพร้อมด้านเทคโนโลยีสารสนเทศ', link: '/request-summary/product-detail/step8',
                      status: this.statusPage.pageDetail.page8
                    },
                    {
                      sub_menu: 'Cost & Benefit Analysis',
                      link: '/request-summary/product-detail/step9', status: this.statusPage.pageDetail.page9
                    },
                    {
                      sub_menu: 'เป้าหมาย/ตัวชี้วัด',
                      link: '/request-summary/product-detail/step10', status: this.statusPage.pageDetail.page10
                    },
                    {
                      sub_menu: 'ข้อมูลด้าน Finance',
                      link: '/request-summary/product-detail/step11', status: this.statusPage.pageDetail.page11
                    },
                    {
                      sub_menu: 'ข้อมูลด้าน Compliance',
                      link: '/request-summary/product-detail/step12', status: this.statusPage.pageDetail.page12
                    },
                    {
                      sub_menu: 'สรุปข้อเสนอเพื่อพิจารณา',
                      link: '/request-summary/product-detail/step13', status: this.statusPage.pageDetail.page13
                    },
                    {
                      sub_menu: 'ประวัติการปรับปรุงผลิตภัณฑ์',
                      link: '/request-summary/product-detail/step14', status: this.statusPage.pageDetail.page14
                    }
                  ]
              },
            ];
          } else {
            this.menuRepeat = [
              {
                id: 'a', title: 'ข้อมูลพื้นฐาน',
                statusDefule: false,
                title_list:
                  [
                    { sub_menu: 'ข้อมูลทั่วไป', link: '/request-summary/information-base/step1', status: this.statusPage.pageDefault.page1 },
                    { sub_menu: 'นโยบายและแผนผลิตภัณฑ์', link: '/request-summary/information-base/step2', status: this.statusPage.pageDefault.page2 },
                    { sub_menu: 'อำนาจอนุมัติผลิตภัณฑ์', link: '/request-summary/information-base/step3', status: this.statusPage.pageDefault.page3 },
                    { sub_menu: 'ข้อมูลผู้ออกผลิตภัณฑ์', link: '/request-summary/information-base/step4', status: this.statusPage.pageDefault.page4 },
                  ]
              },
              {
                id: 'b', title: 'รายละเอียดผลิตภัณฑ์',
                statusDefule: this.checkDefault,
                title_list:
                  [
                    {
                      sub_menu: 'Business Model & Risk',
                      link: '/request-summary/product-detail/step1', status: this.statusPage.pageDetail.page1
                    },
                    {
                      sub_menu: 'ปัญหาอุปสรรคและการจัดการ',
                      link: '/request-summary/product-detail/step2', status: this.statusPage.pageDetail.page2
                    },
                    {
                      sub_menu: 'สภาพตลาดและการแข่งขัน',
                      link: '/request-summary/product-detail/step3', status: this.statusPage.pageDetail.page3
                    },
                    { sub_menu: 'รูปแบบผลิตภัณฑ์', link: '/request-summary/product-detail/step4', status: this.statusPage.pageDetail.page4 },
                    { sub_menu: 'กระบวนการทำงาน', link: '/request-summary/product-detail/step5', status: this.statusPage.pageDetail.page5 },
                    {
                      sub_menu: 'ความพร้อมด้านการดำเนินการ (Operation)', link: '/request-summary/product-detail/step6',
                      status: this.statusPage.pageDetail.page6
                    },
                    {
                      sub_menu: 'ความพร้อมด้านการให้บริการลูกค้าอย่างเป็นธรรม (Market Conduct)',
                      link: '/request-summary/product-detail/step7',
                      status: this.statusPage.pageDetail.page7
                    },
                    {
                      sub_menu: 'Cost & Benefit Analysis',
                      link: '/request-summary/product-detail/step9', status: this.statusPage.pageDetail.page9
                    },
                    {
                      sub_menu: 'เป้าหมาย/ตัวชี้วัด',
                      link: '/request-summary/product-detail/step10', status: this.statusPage.pageDetail.page10
                    },
                    {
                      sub_menu: 'ข้อมูลด้าน Finance',
                      link: '/request-summary/product-detail/step11', status: this.statusPage.pageDetail.page11
                    },
                    {
                      sub_menu: 'ข้อมูลด้าน Compliance',
                      link: '/request-summary/product-detail/step12', status: this.statusPage.pageDetail.page12
                    },
                    {
                      sub_menu: 'สรุปข้อเสนอเพื่อพิจารณา',
                      link: '/request-summary/product-detail/step13', status: this.statusPage.pageDetail.page13
                    },
                    {
                      sub_menu: 'ประวัติการปรับปรุงผลิตภัณฑ์',
                      link: '/request-summary/product-detail/step14', status: this.statusPage.pageDetail.page14
                    }
                  ]
              },
            ];
          }
          this.render = this.menuRepeat;
        } else {
          this.newProductMenu(res['data'].isUpdate);
          this.render = this.menu;
        }

      });

    } else {
      this.render = this.menu;
    }
  }


  newProductMenu(isUpdate) {
    if (isUpdate === true) {
      this.menu = [
        {
          id: 'a', title: 'ข้อมูลพื้นฐาน',
          statusDefule: false,
          title_list:
            [
              { sub_menu: 'ข้อมูลทั่วไป', link: '/request/information-base/step1', status: this.statusPage.pageDefault.page1 },
              { sub_menu: 'นโยบายและแผนผลิตภัณฑ์', link: '/request/information-base/step2', status: this.statusPage.pageDefault.page2 },
              { sub_menu: 'อำนาจอนุมัติผลิตภัณฑ์', link: '/request/information-base/step3', status: this.statusPage.pageDefault.page3 },
              { sub_menu: 'ข้อมูลผู้ออกผลิตภัณฑ์', link: '/request/information-base/step4', status: this.statusPage.pageDefault.page4 },
            ]
        },
        {
          id: 'b', title: 'รายละเอียดผลิตภัณฑ์',
          statusDefule: this.checkDefault,
          title_list:
            [
              { sub_menu: 'Business Model & Risk', link: '/request/product-detail/step1', status: this.statusPage.pageDetail.page1 },
              { sub_menu: 'สภาพตลาดและการแข่งขัน', link: '/request/product-detail/step2', status: this.statusPage.pageDetail.page2 },
              { sub_menu: 'รูปแบบผลิตภัณฑ์', link: '/request/product-detail/step3', status: this.statusPage.pageDetail.page3 },
              { sub_menu: 'กระบวนการทำงาน', link: '/request/product-detail/step4', status: this.statusPage.pageDetail.page4 },
              {
                sub_menu: 'ความพร้อมด้านการดำเนินการ (Operation)', link: '/request/product-detail/step5',
                status: this.statusPage.pageDetail.page5
              },
              {
                sub_menu: 'ความพร้อมด้านการให้บริการลูกค้าอย่างเป็นธรรม (Market Conduct)', link: '/request/product-detail/step6',
                status: this.statusPage.pageDetail.page6
              },
              {
                sub_menu: 'ความพร้อมด้านเทคโนโลยีสารสนเทศ', link: '/request/product-detail/step7',
                status: this.statusPage.pageDetail.page7
              },
              { sub_menu: 'Cost & Benefit Analysis', link: '/request/product-detail/step8', status: this.statusPage.pageDetail.page8 },
              { sub_menu: 'เป้าหมาย/ตัวชี้วัด', link: '/request/product-detail/step9', status: this.statusPage.pageDetail.page9 },
              { sub_menu: 'ข้อมูลด้าน Finance', link: '/request/product-detail/step10', status: this.statusPage.pageDetail.page10 },
              { sub_menu: 'ข้อมูลด้าน Compliance', link: '/request/product-detail/step11', status: this.statusPage.pageDetail.page11 },
              { sub_menu: 'สรุปข้อเสนอเพื่อพิจารณา', link: '/request/product-detail/step12', status: this.statusPage.pageDetail.page12 },
            ]
        },
      ];
    } else {
      this.menu = [
        {
          id: 'a', title: 'ข้อมูลพื้นฐาน',
          statusDefule: false,
          title_list:
            [
              { sub_menu: 'ข้อมูลทั่วไป', link: '/request/information-base/step1', status: this.statusPage.pageDefault.page1 },
              { sub_menu: 'นโยบายและแผนผลิตภัณฑ์', link: '/request/information-base/step2', status: this.statusPage.pageDefault.page2 },
              { sub_menu: 'อำนาจอนุมัติผลิตภัณฑ์', link: '/request/information-base/step3', status: this.statusPage.pageDefault.page3 },
              { sub_menu: 'ข้อมูลผู้ออกผลิตภัณฑ์', link: '/request/information-base/step4', status: this.statusPage.pageDefault.page4 },
            ]
        },
        {
          id: 'b', title: 'รายละเอียดผลิตภัณฑ์',
          statusDefule: this.checkDefault,
          title_list:
            [
              { sub_menu: 'Business Model & Risk', link: '/request/product-detail/step1', status: this.statusPage.pageDetail.page1 },
              { sub_menu: 'สภาพตลาดและการแข่งขัน', link: '/request/product-detail/step2', status: this.statusPage.pageDetail.page2 },
              { sub_menu: 'รูปแบบผลิตภัณฑ์', link: '/request/product-detail/step3', status: this.statusPage.pageDetail.page3 },
              { sub_menu: 'กระบวนการทำงาน', link: '/request/product-detail/step4', status: this.statusPage.pageDetail.page4 },
              {
                sub_menu: 'ความพร้อมด้านการดำเนินการ (Operation)', link: '/request/product-detail/step5',
                status: this.statusPage.pageDetail.page5
              },
              {
                sub_menu: 'ความพร้อมด้านการให้บริการลูกค้าอย่างเป็นธรรม (Market Conduct)', link: '/request/product-detail/step6',
                status: this.statusPage.pageDetail.page6
              },
              { sub_menu: 'Cost & Benefit Analysis', link: '/request/product-detail/step8', status: this.statusPage.pageDetail.page8 },
              { sub_menu: 'เป้าหมาย/ตัวชี้วัด', link: '/request/product-detail/step9', status: this.statusPage.pageDetail.page9 },
              { sub_menu: 'ข้อมูลด้าน Finance', link: '/request/product-detail/step10', status: this.statusPage.pageDetail.page10 },
              { sub_menu: 'ข้อมูลด้าน Compliance', link: '/request/product-detail/step11', status: this.statusPage.pageDetail.page11 },
              { sub_menu: 'สรุปข้อเสนอเพื่อพิจารณา', link: '/request/product-detail/step12', status: this.statusPage.pageDetail.page12 },
            ]
        },
      ];
    }
  }

  // tslint:disable-next-line: use-life-cycle-interface
  ngOnInit() {
    this.checkStatus();
    this.statusRequest.resetPageInfo();
  }

  // ngOnDestroy(): void {
  //   console.log('checkSavePage 555');
  //   this.changes.disconnect();
  // }

  checkSavePage(link?) {
    console.log('checkSavePage');
    const pathname = window.location.pathname;
    console.log('checkSavePage pathname:', pathname);
    console.log('checkSavePage goToLink:', link);
    if (link !== pathname) {

      const data = {
        pathname: pathname,
        goToLink: link
      };
      this.statusRequest.savePage(data);

      // const dialogRef = this.dialog.open(RequestDialogComponent, {
      //   disableClose: true,
      //   data: {
      //     headerDetail: 'ยืนยันการดำเนินการ',
      //     bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
      //     bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
      //     returnStatus: false,
      //     icon: 'assets/img/Alarm-blue.svg',
      //     pathname: pathname,
      //     goToLink: link
      //   }
      // });
      // dialogRef.afterClosed().subscribe((data) => {
      //   console.log('data::', data);
      //   if (data.returnStatus === true) {
      //     this.statusRequest.savePage(data);
      //   }
      // });
    }
  }
}
