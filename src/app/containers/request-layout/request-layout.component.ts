import { Component, OnInit } from '@angular/core';
import { headerItems } from '../customers-layout/header-bar/header';
import { RequestService } from '../../services/request/request.service';

@Component({
  selector: 'app-request-layout',
  templateUrl: './request-layout.component.html',
  styleUrls: ['./request-layout.component.scss']
})
export class RequestLayoutComponent implements OnInit {
  public headerItems = headerItems;
  constructor(private statusRequest: RequestService) { }
  headername: any = [];
  ngOnInit() {
    this.headername = headerItems;
    // this.statusRequest.statusSibar();

  }

}
