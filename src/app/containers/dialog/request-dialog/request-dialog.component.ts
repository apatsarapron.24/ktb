import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { RequestService } from '../../../services/request/request.service';
import { Router } from '@angular/router';

export interface DialogData {
  headerDetail: any;
  bodyDetail1: any;
  bodyDetail2: any;
  returnStatus: any;
  icon: any;
  pathname: any;
  goToLink: any;
}

@Component({
  selector: 'app-request-dialog',
  templateUrl: './request-dialog.component.html',
  styleUrls: ['./request-dialog.component.scss']
})
export class RequestDialogComponent implements OnInit {

  constructor(
    private router: Router,
    private sidebarService: RequestService,
    private dialogRef: MatDialogRef<RequestDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.dialogRef.disableClose = true;
  }

  ngOnInit() {
  }

  close() {
    this.data.returnStatus = false;
    this.dialogRef.close(this.data);
  }

  back() {
    this.data.returnStatus = false;
    this.dialogRef.close(this.data);
    console.log('this.data.goToLink 333', this.data.goToLink);
    this.router.navigate([this.data.goToLink]);
  }

  save() {
    this.data.returnStatus = true;
    // this.sidebarService.savePage(this.data);
    this.dialogRef.close(this.data);
  }
}
