import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { LocationStrategy, HashLocationStrategy, PathLocationStrategy, DatePipe } from '@angular/common';


import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { SelectDropDownModule } from 'ngx-select-dropdown';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

import { AppComponent } from './app.component';

// Import containers
import { DefaultLayoutComponent } from './containers';
import { AdminLayoutComponent } from './containers';
import { CustomersLayoutComponent } from './containers';
import { RequestLayoutComponent } from './containers';
import { RequestDialogComponent } from './containers/dialog/request-dialog/request-dialog.component';


import { AuthenerModule } from './authener/authener.module';


const APP_CONTAINERS = [
  DefaultLayoutComponent,
  AdminLayoutComponent,
  CustomersLayoutComponent,
  RequestLayoutComponent,
  RequestDialogComponent
];

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,

} from '@coreui/angular';

// Import routing module
import { AppRoutingModule } from './app.routing';

// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { httpInterceptorProviders } from './authener/http-interceptors/index';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderBarComponent } from './containers/customers-layout/header-bar/header-bar.component';
import { SidebarRequestComponent } from './containers/request-layout/sidebar-request/sidebar-request.component';
import { CommentModule } from './customers/comment/comment.module';
import { from } from 'rxjs';
import {MatDialogModule} from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';
import { TextareaAutoresizeDirectiveModule } from '../app/textarea-autoresize.directive/textarea-autoresize.directive.module';

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    AuthenerModule,
    // tslint:disable-next-line: deprecation
    HttpModule,
    HttpClientModule,
    SelectDropDownModule,
    BrowserAnimationsModule,
    CommentModule,
    FormsModule,
    MatDialogModule,
    TextareaAutoresizeDirectiveModule
  ],
  declarations: [
    AppComponent,
    APP_CONTAINERS,
    HeaderBarComponent,
    SidebarRequestComponent,
  ],

  entryComponents: [RequestDialogComponent],
  providers: [{
    provide: LocationStrategy,
    useClass: PathLocationStrategy,
  },
    httpInterceptorProviders,
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
