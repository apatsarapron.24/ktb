import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { registerLocaleData, DatePipe } from '@angular/common';
import localeIt from '@angular/common/locales/it';
import localeEnGb from '@angular/common/locales/en-GB';
import localeTH from '@angular/common/locales/en-GB';


@Component({
  // tslint:disable-next-line
  selector: 'body',
  template: '<router-outlet></router-outlet>'
})
export class AppComponent implements OnInit {

  myDate = new Date();
  date: string;
  constructor(private router: Router, private datePipe: DatePipe) {
    this.date = this.datePipe.transform(this.myDate, 'dd MMMM yyyy');
    localStorage.setItem('current_date', this.date);
  }

  checkRequestId() {
    console.log('lllll');
  }


  ngOnInit() {
    if (!window.console) {
      var console = {
        // log: function () { },
        warn: function () { },
        error: function () { },
        time: function () { },
        timeEnd: function () { }
      }
    }
    // this.router.events.subscribe((evt) => {
    //   if (!(evt instanceof NavigationEnd)) {
    //     return;
    //   }
    //   window.scrollTo(0, 0);
    // });
  }
}
