import { Injectable } from '@angular/core';
import { RouterLink, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ResoiutiondataService {

  constructor(private router: Router,
    private http: HttpClient) { }
  api_url = environment.apiUrl
  customer_id = '';


  // resolutionData(type="day"){
  //   return this.http.get(this.api_url+'/total?type_time='+type);
  // }


  apiGetCustomers() {
    this.customer_id = localStorage.getItem("customer_id");
    return this.http.get(environment.apiUrl + '/customers/' + this.customer_id);
  }

  getData() {
    return this.apiGetCustomers()
  }

  getCPU() {
    return this.http.get(environment.apiUrl + '/monitors?type_query=memory&cluster_name=Cluster-VC65&vm_name=VCD-Cell')
  }









}
