import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentAndApprovalComponent } from './comment-and-approval.component';

describe('CommentAndApprovalComponent', () => {
  let component: CommentAndApprovalComponent;
  let fixture: ComponentFixture<CommentAndApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentAndApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentAndApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
