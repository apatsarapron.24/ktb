import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommentAndApprovalComponent } from './comment-and-approval.component';
import { SummaryOfConsiderationComponent } from './summary-of-consideration/summary-of-consideration.component';
import { CanDeactivateGuard } from '../../services/configGuard/config-guard.service';
const routes: Routes = [
  {
    path: '',
    component: CommentAndApprovalComponent,
  },
  {
    path: 'summary-of-consideration',
    component: SummaryOfConsiderationComponent,
    canDeactivate: [CanDeactivateGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommentAndApprovalRoutingModule { }
