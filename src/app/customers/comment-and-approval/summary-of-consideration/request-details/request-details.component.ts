import { dateFormat } from 'highcharts';
import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { ProductDefultFirstStepService } from '../../../../services/request/information-base/first-step/product-defult-first-step.service';
import * as moment from 'moment';
import { IMyDateModel, AngularMyDatePickerDirective, IAngularMyDpOptions } from 'angular-mydatepicker';
import { StepTwoService } from '../../../../services/request/information-base/step-two/step-two.service';
import { StepThreeService } from '../../../../services/request/information-base/step-three/step-three.service';
import { DateComponent } from '../../../datepicker/date/date.component';

@Component({
  selector: 'app-request-details',
  templateUrl: './request-details.component.html',
  styleUrls: ['./request-details.component.scss']
})
export class RequestDetailsComponent implements OnInit {
  @Input() template_manage = { role: null , validate: null, caaStatus: null, pageShow: null };
  @Output() saveStatus = new EventEmitter<boolean>();
  @Output() dataStatus = new EventEmitter<boolean>();
  @ViewChild(DateComponent) Date: DateComponent;
  favoriteSeason: string;
  seasons: string[] = ['Winter', 'Spring', 'Summer', 'Autumn'];

  myDpOptions: IAngularMyDpOptions = {
    dateRange: false,
    dateFormat: 'dd/mm/yyyy',
    // other options are here...
    stylesData: {
      selector: 'dp',
      styles: `
           .dp .top:auto
           .dp .myDpIconLeftArrow,
           .dp .myDpIconRightArrow,
           .dp .myDpHeaderBtn {
               color: #3855c1;
            }
           .dp .myDpHeaderBtn:focus,
           .dp .myDpMonthLabel:focus,
           .dp .myDpYearLabel:focus {
               color: #3855c1;
            }
           .dp .myDpDaycell:focus,
           .dp .myDpMonthcell:focus,
           .dp .myDpYearcell:focus {
               box-shadow: inset 0 0 0 1px #66afe9;
            }
           .dp .myDpSelector:focus {
               border: 1px solid #ADD8E6;
            }
           .dp .myDpSelectorArrow:focus:before {
               border-bottom-color: #ADD8E6;
            }
           .dp .myDpCurrMonth,
           .dp .myDpMonthcell,
           .dp .myDpYearcell {
               color: #00a6e6;
            }
           .dp .myDpDaycellWeekNbr {
               color: #3855c1;
            }
           .dp .myDpPrevMonth,
           .dp .myDpNextMonth {
               color: #0098D2;
            }
           .dp .myDpWeekDayTitle {
               background-color: #0098D2;
               color: #ffffff;
            }
           .dp .myDpHeaderBtnEnabled:hover,
           .dp .myDpMonthLabel:hover,
           .dp .myDpYearLabel:hover {
               color:#0098D2;
            }
           .dp .myDpMarkCurrDay,
           .dp .myDpMarkCurrMonth,
           .dp .myDpMarkCurrYear {
               border-bottom: 2px solid #3855c1;
            }
           .dp .myDpDisabled {
               color: #999;
            }
           .dp .myDpHighlight {
               color: #74CBEC;
            }
           .dp .myDpTableSingleDay:hover,
           .dp .myDpTableSingleMonth:hover,
           .dp .myDpTableSingleYear:hover {
               background-color: #add8e6;
               color: #0098D2;
            }
           .dp .myDpRangeColor {
               background-color: #dbeaff;
            }
           .dp .myDpSelectedDay,
           .dp .myDpSelectedMonth,
           .dp .myDpSelectedYear {
               background-color: #0098D2;
               color: #ffffff;
            }
            `
    }
  };

  // private model: Object = {};
  poductDate: any;
  startDate: any;
  endDate: any;
  model: IMyDateModel = null;
  review_date = null;
  review_date_next = null;
  @ViewChild('dp') mydp: AngularMyDatePickerDirective;
  // All data in page +++++++++++++
  firstStepData = {
    applicationNumber: null,
    PresentType: null,
    agendaType: null,
    subAgendaType: null,
    presentPurpose: null,
    productInfor: null,
    productsOffered: {
      productProgram: false,
      nonCredit: false,
      pricing: false,
      outsourcing: false,
      insourcing: false,
    },
    productName: null,
    productCode: null,
    productGroup: {
      creditProduct: false,
      CP_BusinessLoan: false,
      CP_PersonalLoan: false,
      CP_HousingLoan: false,
      CP_Etc: false,
      CP_Etc_digital: null,
      nonCreditProduct: false,
      NCP_nonCreditProduct: false,
      NCP_Etc: false,
      NCP_Etc_digital: null,
      insourcing: false,
      outsourcing: false,
    },
    serviceDelivery: {
      generalBranch: false,
      electronics: false,
      businessOffice: false,
      CBC: false,
      blockChain: false,
      callCenter: false,
      ATM: false,
      digitalChannels: false,
      detailDigitalChannels: null,
      nonBank: false,
      detailNonBank: null,
      etc: false,
      detailEtc: null,
    },
    expectedDate: null,
    lastReviewDate: null,
    nextReviewDate: null,
    requestRepeatName: null,
    requestRepeatId: null
  };
  newProduct = true;
  saveDraftstatus = false;
  // step 2 data
  infoDataTwo_policy = null;
  infoDataTwo_strategic = null;
  infoDataTwo_strategic_detail = null;
  infoDataTwo_is_update = null;

   // step 3 data
   infoDataThree_approvers = {
    manage: false,
    other: false,
    product: false,
   };
   infoDataThree_approversOther: any;
   infoDataThree_commenters = {
    DSC: false,
    it: false,
    other: false,
   };
   infoDataThree_commentersOther: any;
   infoDataThree_outsider = {
    no: false,
    outhside: {
      BOT: false,
      IC: false,
      PON: false,
      SEC: false,
      other: false,
    },
    yes: false
   };
   infoDataThree_outsiderDetail: any;


  // objectiveShow
  objectiveShow = null;

  // alert
  alerts = false;

  validate = null;
  sendSaveStatus() {
    this.saveStatus.emit(this.saveDraftstatus);
  }

  sendDataStatus() {
    if (this.firstStepData.nextReviewDate !== null && this.firstStepData.nextReviewDate !== '') {
      this.dataStatus.emit(true);
    } else {
      this.dataStatus.emit(false);
    }
  }

  saveDraft() {
    this.upDateDataFirstStep( localStorage.getItem('requestId') );
  }
  next_step() {
    this.saveDraft();
    if ( this.alerts === false) {
      if (localStorage.getItem('requestId')) {
      }
    }

  }

  changeSaveDraft() {
    this.saveDraftstatus = false;
    this.sendSaveStatus();
  }

  productGroup(numder) {
    // console.log('numder', numder);
    if (numder === 1) {
      // console.log('zzz', this.firstStepData.productGroup.creditProduct);
      this.firstStepData.productGroup.creditProduct = !this.firstStepData.productGroup.creditProduct;
      this.firstStepData.productGroup.nonCreditProduct = false;
      this.firstStepData.productGroup.insourcing = false;
      this.firstStepData.productGroup.outsourcing = false;
      this.clearCreditProductSub();
      this.clearnonCreditProductSub();
    } else if (numder === 2) {
      this.firstStepData.productGroup.creditProduct = false;
      this.firstStepData.productGroup.nonCreditProduct = !this.firstStepData.productGroup.nonCreditProduct;
      this.firstStepData.productGroup.insourcing = false;
      this.firstStepData.productGroup.outsourcing = false;
      this.clearCreditProductSub();
      this.clearnonCreditProductSub();
    } else if (numder === 3) {
      this.firstStepData.productGroup.creditProduct = false;
      this.firstStepData.productGroup.nonCreditProduct = false;
      this.firstStepData.productGroup.insourcing = !this.firstStepData.productGroup.insourcing;
      this.firstStepData.productGroup.outsourcing = false;
      this.clearCreditProductSub();
      this.clearnonCreditProductSub();
    } else if (numder === 4) {
      this.firstStepData.productGroup.creditProduct = false;
      this.firstStepData.productGroup.nonCreditProduct = false;
      this.firstStepData.productGroup.insourcing = false;
      this.firstStepData.productGroup.outsourcing = !this.firstStepData.productGroup.outsourcing;
      this.clearCreditProductSub();
      this.clearnonCreditProductSub();
    }
  }

  clearCreditProductSub() {
    this.firstStepData.productGroup.CP_BusinessLoan = false;
    this.firstStepData.productGroup.CP_PersonalLoan = false;
    this.firstStepData.productGroup.CP_HousingLoan = false;
    this.firstStepData.productGroup.CP_Etc = false;
    this.firstStepData.productGroup.CP_Etc_digital = null;
  }

  clearAgendaType() {
    this.firstStepData.subAgendaType = '';
  }

  creditProductSub(numder) {
    if (this.firstStepData.productGroup.creditProduct) {
      if (numder === 1) {
        this.firstStepData.productGroup.CP_BusinessLoan = !this.firstStepData.productGroup.CP_BusinessLoan;
      } else if (numder === 2) {
        this.firstStepData.productGroup.CP_PersonalLoan = !this.firstStepData.productGroup.CP_PersonalLoan;
      } else if (numder === 3) {
        this.firstStepData.productGroup.CP_HousingLoan = !this.firstStepData.productGroup.CP_HousingLoan;
      } else if (numder === 4) {
        this.firstStepData.productGroup.CP_Etc = !this.firstStepData.productGroup.CP_Etc;
        this.firstStepData.productGroup.CP_Etc_digital = null;
      }
    }
    this.changeSaveDraft();
  }
  nonCreditProductSub(numder) {
    if (this.firstStepData.productGroup.nonCreditProduct) {
      if (numder === 1) {
        this.firstStepData.productGroup.NCP_nonCreditProduct = !this.firstStepData.productGroup.NCP_nonCreditProduct;
      } else if (numder === 2) {
        this.firstStepData.productGroup.NCP_Etc = !this.firstStepData.productGroup.NCP_Etc;
        this.firstStepData.productGroup.NCP_Etc_digital = null;
      }
    }
  }
  clearnonCreditProductSub() {
    this.firstStepData.productGroup.NCP_nonCreditProduct = false;
    this.firstStepData.productGroup.NCP_Etc = false;
    this.firstStepData.productGroup.NCP_Etc_digital = null;
  }
  clearServiceDeliveryDetailDigitalChannels() {
    this.firstStepData.serviceDelivery.digitalChannels = !this.firstStepData.serviceDelivery.digitalChannels;
    this.firstStepData.serviceDelivery.detailDigitalChannels = null;
  }
  clearServiceDeliveryDetailNonBank() {
    this.firstStepData.serviceDelivery.nonBank = !this.firstStepData.serviceDelivery.nonBank;
    this.firstStepData.serviceDelivery.detailNonBank = null;
  }
  clearServiceDeliveryDetailEtc() {
    this.firstStepData.serviceDelivery.etc = !this.firstStepData.serviceDelivery.etc;
    this.firstStepData.serviceDelivery.detailEtc = null;
  }


  // ====================  API =============================

  GetDataFirstStep(documentId) {
    this.productDefultFirstStep.getDefultFirstStep(documentId, this.template_manage.pageShow).subscribe(res => {
      if (res['data'] !== null) {
      const productDefaultStep1 = res['data'];
      console.log('productDefaultStep1 => 0001', productDefaultStep1);
      let agenda: string;
      let subAgenda: string;
      this.validate = productDefaultStep1['validate'];
      console.log('validate', this.validate );
          if (productDefaultStep1.agenda !== null) {
            if (productDefaultStep1.agenda['normal'] === true
              && productDefaultStep1.agenda['additional'] === false) {
              agenda = 'true';
            } else if (productDefaultStep1.agenda['normal'] === false
              && productDefaultStep1.agenda['additional'] === true) {
              agenda = 'false';
            } else { agenda = null; }

            if (productDefaultStep1.agenda.additional_detail['befor'] === true
              && productDefaultStep1.agenda.additional_detail['after'] === false) {
              subAgenda = 'befor';
            } else if (productDefaultStep1.agenda.additional_detail['befor'] === false
              && productDefaultStep1.agenda.additional_detail['after'] === true) {
              subAgenda = 'after';
            } else if (productDefaultStep1.agenda.additional_detail['befor'] === false
              && productDefaultStep1.agenda.additional_detail['after'] === false) {
              subAgenda = null;
            }

          } else { agenda = null; subAgenda = null; }
        // console.log(productDefaultStep1['name']);
        if (productDefaultStep1['name']) {
          this.newProduct = false;
        } else {
          this.newProduct = true;
        }

        // frontend <-- backend
        this.firstStepData = {
          applicationNumber: productDefaultStep1['no'],
          PresentType: productDefaultStep1['presentation'],
          agendaType: agenda,
          subAgendaType: subAgenda,
          presentPurpose: productDefaultStep1['objective'],
          productInfor: null,
          productsOffered: {
            productProgram: false,
            nonCredit: false,
            pricing: false,
            outsourcing: false,
            insourcing: false,
          },
          productName: productDefaultStep1['name'],
          productCode: productDefaultStep1['code'],
          productGroup: {
            creditProduct: false,
            CP_BusinessLoan: false,
            CP_PersonalLoan: false,
            CP_HousingLoan: false,
            CP_Etc: false,
            CP_Etc_digital: null,
            nonCreditProduct: false,
            NCP_nonCreditProduct: false,
            NCP_Etc: false,
            NCP_Etc_digital: null,
            insourcing: false,
            outsourcing: false,
          },
          serviceDelivery: {
            generalBranch: false,
            electronics: false,
            businessOffice: false,
            CBC: false,
            blockChain: false,
            callCenter: false,
            ATM: false,
            digitalChannels: false,
            detailDigitalChannels: null,
            nonBank: false,
            detailNonBank: null,
            etc: false,
            detailEtc: null,
          },
          expectedDate: productDefaultStep1['releaseDate'],
          lastReviewDate: productDefaultStep1['lastReviewDate'],
          nextReviewDate: productDefaultStep1['nextReviewDate'],
          requestRepeatName: productDefaultStep1['requestRepeatName'],
          requestRepeatId: productDefaultStep1['requestRepeatId']
        };

        // set productsOffered
        if (productDefaultStep1['productProgram'].length > 0) {
          this.firstStepData.productsOffered.productProgram = productDefaultStep1['productProgram'][0].flag;
          this.firstStepData.productsOffered.nonCredit = productDefaultStep1['productProgram'][1].flag;
          this.firstStepData.productsOffered.pricing = productDefaultStep1['productProgram'][2].flag;
          this.firstStepData.productsOffered.outsourcing = productDefaultStep1['productProgram'][3].flag;
          this.firstStepData.productsOffered.outsourcing = productDefaultStep1['productProgram'][4].flag;
        }

        // set productGroup
        console.log('55555555555555', productDefaultStep1);
        if (productDefaultStep1['group'] !== null) {
          this.firstStepData.productGroup.creditProduct = productDefaultStep1['group'].credit_product['flag'];
          this.firstStepData.productGroup.CP_BusinessLoan = productDefaultStep1['group'].credit_product['child'].business_loan;
          this.firstStepData.productGroup.CP_PersonalLoan =  productDefaultStep1['group'].credit_product['child'].persenal_loan;
          this.firstStepData.productGroup.CP_HousingLoan = productDefaultStep1['group'].credit_product['child'].housing_loan;
          this.firstStepData.productGroup.CP_Etc = productDefaultStep1['group'].credit_product['child'].other;
          this.firstStepData.productGroup.CP_Etc_digital = productDefaultStep1['group'].credit_product['child'].other_detail;
          this.firstStepData.productGroup.nonCreditProduct = productDefaultStep1['group'].non_credit_product['flag'];
          // tslint:disable-next-line:max-line-length
          this.firstStepData.productGroup.NCP_nonCreditProduct = productDefaultStep1['group'].non_credit_product['child'].non_credit_product;
          this.firstStepData.productGroup.NCP_Etc = productDefaultStep1['group'].non_credit_product['child'].other;
          this.firstStepData.productGroup.NCP_Etc_digital = productDefaultStep1['group'].non_credit_product['child'].other_detail;
          this.firstStepData.productGroup.insourcing = productDefaultStep1['group'].insousecing['flag'];
          this.firstStepData.productGroup.outsourcing = productDefaultStep1['group'].outsoucing['flag'];
        }

        // set serviceDelivery
        if (productDefaultStep1['channel'].length > 0) {
          this.firstStepData.serviceDelivery.generalBranch = productDefaultStep1['channel'][0].flag;
          this.firstStepData.serviceDelivery.electronics = productDefaultStep1['channel'][1].flag;
          this.firstStepData.serviceDelivery.businessOffice = productDefaultStep1['channel'][2].flag;
          this.firstStepData.serviceDelivery.CBC = productDefaultStep1['channel'][3].flag;
          this.firstStepData.serviceDelivery.blockChain = productDefaultStep1['channel'][4].flag;
          this.firstStepData.serviceDelivery.callCenter = productDefaultStep1['channel'][5].flag;
          this.firstStepData.serviceDelivery.ATM = productDefaultStep1['channel'][6].flag;
          this.firstStepData.serviceDelivery.digitalChannels = productDefaultStep1['channel'][7].flag;
          this.firstStepData.serviceDelivery.detailDigitalChannels = productDefaultStep1['digitalDetail'];
          this.firstStepData.serviceDelivery.nonBank = productDefaultStep1['channel'][8].flag;
          this.firstStepData.serviceDelivery.detailNonBank = productDefaultStep1['anotherDetail'];
          this.firstStepData.serviceDelivery.etc = productDefaultStep1['channel'][9].flag;
          this.firstStepData.serviceDelivery.detailEtc = productDefaultStep1['otherDetail'];
        }

        // set productsOffered
        console.log('productDefaultStep1', productDefaultStep1['productProgram'].length);
        if (productDefaultStep1['productProgram'].length > 0) {
          console.log('set productsOffered');
        }
        // this.firstStepData.productsOffered.productProgram
        if (productDefaultStep1['releaseDate']) {
          this.poductDate = moment(productDefaultStep1['releaseDate']).format('DD/MM/YY');
        } else {
          this.poductDate = null;
        }

        if (productDefaultStep1['lastReviewDate']) {
          this.startDate = moment(productDefaultStep1['lastReviewDate']).format('DD/MM/YY');
        } else {
          this.startDate = null;
        }
      }
    });
  }

  changeDateSearch(dateTime: any) {
    if (dateTime !== null) {
      // tslint:disable-next-line: max-line-length
      this.endDate = dateTime;
      const date = moment(dateTime);
      date.locale('th');
      const buddhishYear = (parseInt(date.format('YYYY'), 10) + 543).toString();
      this.changeSaveDraft();
      return date.format('DD/MM') + '/' + buddhishYear;
    } else {
      return '';
    }

  }


  upDateDataFirstStep(documentId) {
    console.log('upDateDataFirstStep', documentId);
    const countSuccess = 0;
    let statusSuccess = false;
    let statusPresentType = false;
    let statusAgendaType = false;
    let statusAgendaType_select = false;
    let statusPresentPurpose = false;
    let statusProductsOffered = false;
    let statusProductGroup = false;
    let statusProductGroup_creditselect = false;
    let statusProductGroup_creditProduct = true;
    let statusProductGroup_noncreditselect = false;
    let statusProductGroup_nonCreditProduct = true;
    let statusServiceDelivery = false;
    let statusServiceDelivery_etailDigitalChannels = true;
    let statusServiceDelivery_detailNonBank = true;
    let statusServiceDelivery_detailEtc = true;
    let statusExpectedDate = false;
    let statusNameProduct = false;

    let agendaType_normal = null;
    let agendaType_additional = null;
    let agendaType_sub_befor = null;
    let agendaType_sub_after = null;

    this.alerts = false;

    // = PresentType (ประเภทการนำเสนอ)
    if (this.firstStepData.PresentType) {
      statusPresentType = true;
    }

    // = agendaType (ประเภทวาระ)
    // console.log('66666666', this.firstStepData);
    if (this.firstStepData.agendaType === 'true' || this.firstStepData.agendaType === 'false') {
      statusAgendaType = true;
      if (this.firstStepData.agendaType === 'true') {
        agendaType_normal = true;
        agendaType_additional = false;
        agendaType_sub_befor = false;
        agendaType_sub_after = false;
        statusAgendaType_select = true;

      } else if (this.firstStepData.agendaType === 'false') {
        statusAgendaType = true;
        agendaType_normal = false;
        agendaType_additional = true;
          if (this.firstStepData.subAgendaType === 'after') {
            agendaType_sub_befor = false;
            agendaType_sub_after = true;
            statusAgendaType_select = true;
          } else if (this.firstStepData.subAgendaType === 'befor') {
            agendaType_sub_befor = true;
            agendaType_sub_after = false;
            statusAgendaType_select = true;
          } else {
            agendaType_sub_befor = false;
            agendaType_sub_after = false;
            statusAgendaType_select = true;
          }
      } else {
        agendaType_normal = false;
        agendaType_additional = false;
        agendaType_sub_befor = false;
        agendaType_sub_after = false;
        statusAgendaType_select = false;
      }
    }
    // console.log('aaaaaaaaaaa', statusAgendaType_select);
    // console.log('aaaaaaaaaaa2', this.firstStepData.agendaType);
    if (statusAgendaType_select === false) {
      this.alerts = true;
      alert('Agenda type: Please select an option for additional agenda.');
    }

    // = presentPurpose (วัตถุประสงค์การนำเสนอ) ** ยังไมเสร็จดี ขาดตรวจสอบ เรียกดูข้อมูลผลิตภัณฑ์
    if (this.firstStepData.presentPurpose) {
      statusPresentPurpose = true;
    }

    // = productsOffered (ผลิตภัณฑ์ที่นำเสนออยู่ภายใต้การกำกับของคณะกรรมการผลิตภัณฑ์)
    if (this.firstStepData.productsOffered.productProgram === true) {
      statusProductsOffered = true;
    }
    if (this.firstStepData.productsOffered.nonCredit === true) {
      statusProductsOffered = true;
    }
    if (this.firstStepData.productsOffered.pricing === true) {
      statusProductsOffered = true;
    }
    if (this.firstStepData.productsOffered.outsourcing === true) {
      statusProductsOffered = true;
    }
    if (this.firstStepData.productsOffered.insourcing === true) {
      statusProductsOffered = true;
    }

    // = productGroup (กลุ่มผลิตภัณฑ์)
    // - creditProduct
    if (this.firstStepData.productGroup.creditProduct === true) {
      statusProductGroup = true;
      if (this.firstStepData.productGroup.CP_BusinessLoan === true) {
        statusProductGroup_creditselect = true;
      }
      if (this.firstStepData.productGroup.CP_PersonalLoan === true) {
        statusProductGroup_creditselect = true;
      }
      if (this.firstStepData.productGroup.CP_HousingLoan === true) {
        statusProductGroup_creditselect = true;
      }
      if (this.firstStepData.productGroup.CP_Etc === true) {
        statusProductGroup_creditselect = true;
        if (!this.firstStepData.productGroup.CP_Etc_digital) {
          statusProductGroup_creditProduct = false;
        }
      }
      // + Alert Credit Product
      if (statusProductGroup_creditselect === false) {
        this.alerts = true;
        alert('Product group: Please select an option for credit products.');
      } else if (statusProductGroup_creditselect === true && statusProductGroup_creditProduct === false) {
        this.alerts = true;
        alert('Product group: If you choose other Please fill out credit product information.');
      }
    } else if (this.firstStepData.productGroup.creditProduct === false) {
      statusProductGroup_creditselect = true;
    }
    // - creditProduct
    if (this.firstStepData.productGroup.nonCreditProduct === true) {
      statusProductGroup = true;
      if (this.firstStepData.productGroup.NCP_nonCreditProduct === true) {
        statusProductGroup_noncreditselect = true;
      }
      if (this.firstStepData.productGroup.NCP_Etc === true) {
        statusProductGroup_noncreditselect = true;
        if (!this.firstStepData.productGroup.NCP_Etc_digital) {
          statusProductGroup_nonCreditProduct = false;
        }
      }
      // + Alert Non-Credit Product
      if (statusProductGroup_noncreditselect === false) {
        this.alerts = true;
        alert('Product group: Please select an option for non-credit products.');
      } else if (statusProductGroup_noncreditselect === true && statusProductGroup_nonCreditProduct === false) {
        this.alerts = true;
        alert('Product group: If you choose other Please fill out non-credit product information.');
      }
    } else if (this.firstStepData.productGroup.nonCreditProduct === false) {
      statusProductGroup_noncreditselect = true;
    }
    // - insourcing
    if (this.firstStepData.productGroup.insourcing === true) {
      statusProductGroup = true;
    }
    // - outsourcing
    if (this.firstStepData.productGroup.outsourcing === true) {
      statusProductGroup = true;
    }

    // = serviceDelivery (ช่องทางนำเสนอบริการ)
    if (this.firstStepData.serviceDelivery.generalBranch === true) {
      statusServiceDelivery = true;
    }
    if (this.firstStepData.serviceDelivery.electronics === true) {
      statusServiceDelivery = true;
    }
    if (this.firstStepData.serviceDelivery.businessOffice === true) {
      statusServiceDelivery = true;
    }
    if (this.firstStepData.serviceDelivery.CBC === true) {
      statusServiceDelivery = true;
    }
    if (this.firstStepData.serviceDelivery.blockChain === true) {
      statusServiceDelivery = true;
    }
    if (this.firstStepData.serviceDelivery.callCenter === true) {
      statusServiceDelivery = true;
    }
    if (this.firstStepData.serviceDelivery.ATM === true) {
      statusServiceDelivery = true;
    }
    if (this.firstStepData.serviceDelivery.digitalChannels === true) {
      statusServiceDelivery = true;
      if (!this.firstStepData.serviceDelivery.detailDigitalChannels) {
        statusServiceDelivery_etailDigitalChannels = false;
      }
    }
    if (this.firstStepData.serviceDelivery.nonBank === true) {
      statusServiceDelivery = true;
      if (!this.firstStepData.serviceDelivery.detailNonBank) {
        statusServiceDelivery_detailNonBank = false;
      }
    }
    if (this.firstStepData.serviceDelivery.etc === true) {
      statusServiceDelivery = true;
      if (!this.firstStepData.serviceDelivery.detailEtc) {
        statusServiceDelivery_detailEtc = false;
      }
    }


    // + Alert ServiceDelivery
    if (this.firstStepData.serviceDelivery.digitalChannels === true && statusServiceDelivery_etailDigitalChannels === false) {
      this.alerts = true;
      alert('Service delivery channels: If selected Digital Channels please fill out more details.');
    }
    if (this.firstStepData.serviceDelivery.nonBank === true && statusServiceDelivery_detailNonBank === false) {
      this.alerts = true;
      alert('Service delivery channels: If selected Channels other than the bank please fill out more details.');
    }
    if (this.firstStepData.serviceDelivery.etc === true && statusServiceDelivery_detailEtc === false) {
      this.alerts = true;
      alert('Service delivery channels: If selected Other please fill out more details.');
    }

    // = expectedDate (วันที่คาดว่าจะประกาศใช้)
    if (this.firstStepData.expectedDate) {
      statusExpectedDate = true;
    } else {
      this.firstStepData.expectedDate = null;
      statusExpectedDate = false;
    }

    if (this.firstStepData.productName) {
      statusNameProduct = true;
    }

    // console.log('000001 >====================');
    // console.log('statusPresentType', statusPresentType);
    // console.log('statusAgendaType', statusAgendaType);
    // console.log('statusAgendaType_select', statusAgendaType_select);
    // console.log('statusPresentPurpose', statusPresentPurpose);
    // console.log('statusProductsOffered', statusProductsOffered);
    // console.log('statusProductGroup', statusProductGroup);
    // console.log('statusProductGroup_creditProduct', statusProductGroup_creditProduct);
    // console.log('statusProductGroup_creditselect', statusProductGroup_creditselect);
    // console.log('statusProductGroup_nonCreditProduct', statusProductGroup_nonCreditProduct);
    // console.log('statusProductGroup_noncreditselect', statusProductGroup_noncreditselect);
    // console.log('statusServiceDelivery', statusServiceDelivery);
    // console.log('statusServiceDelivery_etailDigitalChannels', statusServiceDelivery_etailDigitalChannels);
    // console.log('statusServiceDelivery_detailNonBank', statusServiceDelivery_detailNonBank);
    // console.log('statusServiceDelivery_detailEtc', statusServiceDelivery_detailEtc);
    // console.log('statusExpectedDate', statusExpectedDate);
    // console.log('statusNameProduct', statusNameProduct);


    if ((((((((((((((((statusPresentType === true
      && statusAgendaType === true)
      && statusAgendaType_select === true)
      && statusPresentPurpose === true)
      && statusProductsOffered === true)
      && statusProductGroup === true)
      && statusProductGroup_creditProduct === true)
      && statusProductGroup_creditselect === true)
      && statusProductGroup_nonCreditProduct === true)
      && statusProductGroup_noncreditselect === true)
      && statusServiceDelivery === true)
      && statusServiceDelivery_etailDigitalChannels === true)
      && statusServiceDelivery_detailNonBank === true)
      && statusServiceDelivery_detailEtc === true)
      && statusExpectedDate === true)
      && statusNameProduct === true)
    ) {
      statusSuccess = true;
    } else {
      statusSuccess = false;
    }

    // backend <-- frontend
    const data = {
      'id': null,
      'no': this.firstStepData.applicationNumber,
      'presentation': this.firstStepData.PresentType,
      'agenda': {
        'normal': agendaType_normal,
        'additional_detail': {
          'befor': agendaType_sub_befor,
          'after': agendaType_sub_after
        },
        'additional': agendaType_additional,
      },
      'objective': this.firstStepData.presentPurpose,
      'productProgram': [
        {
          'topic': 'ออก/ทบทวนผลิตภัณฑ์สินเชื่อที่เป็น Product Program',
          'flag': this.firstStepData.productsOffered.productProgram
        },
        {
          'topic': 'ออก/ทบทวนผลิตภัณฑ์ทางอิเล็กทรอนิกส์และผลิตภัณฑ์อื่นๆ (Non Credit)',
          'flag': this.firstStepData.productsOffered.nonCredit
        },
        {
          'topic': 'ออก/ทบทวนหลักเกณฑ์......... Pricingแก่ลูกค้า',
          'flag': this.firstStepData.productsOffered.pricing
        },
        {
          'topic': 'การใช้บริการจากผู้ให้บริการภายนอก (Outsourcing)',
          'flag': this.firstStepData.productsOffered.outsourcing
        },
        {
          'topic': 'การใช้บริการทางการเงินหรือทางการ (Insourcing)',
          'flag': this.firstStepData.productsOffered.insourcing
        }
      ],
      'name': this.firstStepData.productName,
      'code': this.firstStepData.productCode,
      'group': {
        'credit_product': {
          'flag': this.firstStepData.productGroup.creditProduct,
          'child': {
            'business_loan': this.firstStepData.productGroup.CP_BusinessLoan,
            'persenal_loan': this.firstStepData.productGroup.CP_PersonalLoan,
            'housing_loan': this.firstStepData.productGroup.CP_HousingLoan,
            'other': this.firstStepData.productGroup.CP_Etc,
            'other_detail': this.firstStepData.productGroup.CP_Etc_digital
          }
        },
        'non_credit_product': {
          'flag': this.firstStepData.productGroup.nonCreditProduct,
          'child': {
            'non_credit_product': this.firstStepData.productGroup.NCP_nonCreditProduct,
            'other': this.firstStepData.productGroup.NCP_Etc,
            'other_detail': this.firstStepData.productGroup.NCP_Etc_digital
          }
        },
        'insousecing': {
          'flag': this.firstStepData.productGroup.insourcing
        },
        'outsoucing': {
          'flag': this.firstStepData.productGroup.outsourcing
        }
      },
      'channel': [
        {
          'topic': 'สาขาทั่วไป',
          'flag': this.firstStepData.serviceDelivery.generalBranch
        },
        {
          'topic': 'สาขาอิเล็กทรอนิกส์',
          'flag': this.firstStepData.serviceDelivery.electronics
        },
        {
          'topic': 'สำนักงานธุรกิจ',
          'flag': this.firstStepData.serviceDelivery.businessOffice
        },
        {
          'topic': 'CBC',
          'flag': this.firstStepData.serviceDelivery.CBC
        },
        {
          'topic': 'Blockchain',
          'flag': this.firstStepData.serviceDelivery.blockChain
        },
        {
          'topic': 'Call Center',
          'flag': this.firstStepData.serviceDelivery.callCenter
        },
        {
          'topic': 'ATM',
          'flag': this.firstStepData.serviceDelivery.ATM
        },
        {
          'topic': 'ช่องทางดิจิทัล',
          'flag': this.firstStepData.serviceDelivery.digitalChannels
        },
        {
          'topic': 'ช่องทางอื่นที่ไม่ใช่ของธนาคาร',
          'flag': this.firstStepData.serviceDelivery.nonBank
        },
        {
          'topic': 'อื่นๆ',
          'flag': this.firstStepData.serviceDelivery.etc
        }
      ],
      'digitalDetail': this.firstStepData.serviceDelivery.detailDigitalChannels,
      'anotherDetail': this.firstStepData.serviceDelivery.detailNonBank,
      'otherDetail': this.firstStepData.serviceDelivery.detailEtc,
      'releaseDate': null,
      'lastReviewDate': null,
      'nextReviewDate': null,
      'status': statusSuccess,
      'conclusions': null
    };
    if (localStorage.getItem('requestId')) {
      data.id = localStorage.getItem('requestId');
    }
    // this.firstStepData.expectedDate = '2020-10-18T00:00:00.000+0000';
    // this.firstStepData.lastReviewDate = '2020-10-18T00:00:00.000+0000';
    // this.firstStepData.nextReviewDate = '2020-10-18T00:00:00.000+0000';
    console.log('nextReviewDate 000', this.firstStepData.nextReviewDate);
    if (this.firstStepData.expectedDate) {
      // data.releaseDate = moment(this.firstStepData.expectedDate.singleDate.jsDate).format().substring(0, 10);
      data.releaseDate = this.firstStepData.expectedDate;
    } else { data.releaseDate = null; }
    if (this.firstStepData.lastReviewDate) {
      // data.releaseDate = moment(this.firstStepData.expectedDate.singleDate.jsDate).format().substring(0, 10);
      data.lastReviewDate = this.firstStepData.lastReviewDate;
    } else { data.releaseDate = null; }
    if (this.endDate) {
      // data.releaseDate = moment(this.firstStepData.expectedDate.singleDate.jsDate).format().substring(0, 10);
      data.nextReviewDate = this.endDate;
      data.status = true;
    } else {
      data.releaseDate = null;
      data.status = false;
    }


    // ===================  Send API =============================
    if ( this.template_manage.pageShow === 'summary') {
      data.conclusions = true;
    }

    // this.sendDataStatus();

    console.log('data', data);
    if (this.alerts === false) {
      this.productDefultFirstStep.upDateDefultFirstStep(data).subscribe(res => {
        if (res['status'] === 'success') {
          localStorage.setItem('requestId', res['data'].id);
          this.saveDraftstatus = true;
          this.sendSaveStatus();
          if (localStorage.getItem('requestId')) {
            this.GetDataFirstStep(localStorage.getItem('requestId'));
          }
        } else {
          alert(res['message']);
        }
      }, err => {
        this.sendSaveStatus();
        alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
        console.log('send API err', err);
      });
    }
  }

  // get api step 2
  dataStepTwoInfo(requestId) {
    if (requestId !== '') {
      const body = { id: requestId };
      this.infoStepTwo.getPage2(body).subscribe(res => {
        if (res['status'] === 'success') {
          this.infoDataTwo_policy = res['data'].policy;
          this.infoDataTwo_strategic = res['data'].strategic;
          this.infoDataTwo_strategic_detail = res['data'].strategic_detail;
          this.infoDataTwo_is_update = res['data'].is_update;
        }
      });
    }
  }

  // get api step 3
  dataStepthreeInfo(requestId) {
    if (requestId !== '') {
      const body = { id: requestId };
      this.infoStepThree.getPage3(body).subscribe(res => {
        if (res['status'] === 'success') {
          console.log('0000000 infoStepThree', res['data']);
          this.infoDataThree_approvers = res['data'].approvers;
          this.infoDataThree_approversOther = res['data'].approversOther;
          this.infoDataThree_commenters = res['data'].commenters;
          this.infoDataThree_commentersOther = res['data'].commentersOther;
          this.infoDataThree_outsider = res['data'].outsider;
          this.infoDataThree_outsiderDetail = res['data'].outsiderDetail;
        }
      });
    }
  }

  constructor(
    private productDefultFirstStep: ProductDefultFirstStepService,
    private infoStepTwo: StepTwoService,
    private infoStepThree: StepThreeService,
  ) { }

  ngOnInit() {
    if (localStorage) {
      if (localStorage.getItem('requestId')) {
        this.GetDataFirstStep(localStorage.getItem('requestId'));
        this.dataStepTwoInfo(localStorage.getItem('requestId'));
        this.dataStepthreeInfo(localStorage.getItem('requestId'));
      }
    } else {
      console.log('Brownser not support');
    }
  }

  dateFormat(date: any) {
    let dateForm = null;
    const year = Number(moment(date).format('YYYY')) + Number(543);
        dateForm = moment(date).format('DD/MM') + '/' + year;
    return dateForm;
  }
}
