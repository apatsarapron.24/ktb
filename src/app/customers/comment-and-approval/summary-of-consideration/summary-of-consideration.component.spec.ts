import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryOfConsiderationComponent } from './summary-of-consideration.component';

describe('SummaryOfConsiderationComponent', () => {
  let component: SummaryOfConsiderationComponent;
  let fixture: ComponentFixture<SummaryOfConsiderationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SummaryOfConsiderationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryOfConsiderationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
