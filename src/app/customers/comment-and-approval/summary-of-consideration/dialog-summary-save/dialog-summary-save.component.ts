import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface DialogData {
  headerDetail: any;
  bodyDetail1: any;
  bodyDetail2: any;
  deleteStatus: any;
  icon: any;
}

@Component({
  selector: 'app-dialog-summary-save',
  templateUrl: './dialog-summary-save.component.html',
  styleUrls: ['./dialog-summary-save.component.scss']
})
export class DialogSummarySaveComponent implements OnInit {

  close() {
    this.data.deleteStatus = false;
    this.dialogRef.close(this.data);
  }

  back() {
    this.data.deleteStatus = false;
    this.dialogRef.close(this.data);
  }

  save() {
    this.data.deleteStatus = true;
    this.dialogRef.close(this.data);
  }

  constructor(
    private dialogRef: MatDialogRef<DialogSummarySaveComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.dialogRef.disableClose = true;
  }

  ngOnInit() {
  }

}
