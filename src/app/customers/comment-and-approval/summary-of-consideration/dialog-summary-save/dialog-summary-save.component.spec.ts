import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogSummarySaveComponent } from './dialog-summary-save.component';

describe('DialogSummarySaveComponent', () => {
  let component: DialogSummarySaveComponent;
  let fixture: ComponentFixture<DialogSummarySaveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogSummarySaveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogSummarySaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
