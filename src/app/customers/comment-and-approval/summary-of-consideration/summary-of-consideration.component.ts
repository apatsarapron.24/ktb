import { RequestComponent } from './../../request/request.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DialogSummaryComponent } from './dialog-summary/dialog-summary.component';
import Swal from 'sweetalert2';
// tslint:disable-next-line:max-line-length
import { SummaryOfConsiderationService } from '../../../services/comment-and-approval/summary-of-consideration/summary-of-consideration.service';
import { StepOneComponent } from '../../request/product-detail/step-one/step-one.component';
import { StepTwoComponent } from '../../request/product-detail/step-two/step-two.component';
// import { StepThreeComponent } from '../../request/product-detail/step-three/step-three.component';
import { BusinessLoanComponent } from '../../request/product-detail/step-three/business-loan/business-loan.component';
import { PersonalLoanComponent } from '../../request/product-detail/step-three/personal-loan/personal-loan.component';
import { HousingLoanComponent } from '../../request/product-detail/step-three/housing-loan/housing-loan.component';
import { OtherComponent } from '../../request/product-detail/step-three/other/other.component';
import { NonCreditComponent } from '../../request/product-detail/step-three/non-credit/non-credit.component';
import { OtherNonCreaditComponent } from '../../request/product-detail/step-three/other-non-creadit/other-non-creadit.component';
import { StepFourComponent } from '../../request/product-detail/step-four/step-four.component';
import { StepFiveComponent } from '../../request/product-detail/step-five/step-five.component';
import { StepSixComponent } from '../../request/product-detail/step-six/step-six.component';
import { StepSevenComponent } from '../../request/product-detail/step-seven/step-seven.component';
import { StepEightComponent } from '../../request/product-detail/step-eight/step-eight.component';
import { StepNineComponent } from '../../request/product-detail/step-nine/step-nine.component';
import { StepTenComponent } from '../../request/product-detail/step-ten/step-ten.component';
import { StepElevenComponent } from '../../request/product-detail/step-eleven/step-eleven.component';
import { RequestDetailsComponent } from './request-details/request-details.component';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { DialogSummarySaveComponent } from './dialog-summary-save/dialog-summary-save.component';
import { ProductDefultFirstStepService } from '../../../services/request/information-base/first-step/product-defult-first-step.service';
import { StepTwoProductSummaryComponent } from '../../request-summary/product-detail/step-two/step-two.component';
import { StepTenSummaryComponent } from '../../request-summary/product-detail/step-ten/step-ten.component';
import { StepFourtSummaryeenComponent } from '../../request-summary/product-detail/step-fourteen/step-fourteen.component';
import { StepThirteenSummaryComponent } from '../../request-summary/product-detail/step-thirteen/step-thirteen.component';
import { StepTwoService } from '../../../services/request/information-base/step-two/step-two.service';


import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { RequestService } from '../../../services/request/request.service';
import { CanComponentDeactivate } from '../../../services/configGuard/config-guard.service';
// import { DialogSaveStatusComponent } from '../../../../request/dialog/dialog-save-status/dialog-save-status.component';
import { DialogSaveStatusComponent } from '../../request/dialog/dialog-save-status/dialog-save-status.component';



@Component({
  selector: 'app-summary-of-consideration',
  templateUrl: './summary-of-consideration.component.html',
  styleUrls: ['./summary-of-consideration.component.scss']
})
export class SummaryOfConsiderationComponent implements OnInit, CanComponentDeactivate {
  information_1 = false;
  information_2 = false;
  information_3 = false;
  information_4_1 = false;
  information_4_2 = false;
  information_4_3 = false;
  information_4_4 = false;
  information_4_5 = false;
  information_4_6 = false;
  information_5 = false;
  information_6 = false;
  information_7 = false;
  information_8 = false;
  information_9 = false;
  information_10 = false;
  information_11 = false;
  information_12 = false;
  information_13 = false;
  // ทบบวน information
  information_20 = false;
  information_21 = false;
  information_22 = false;
  information_23 = false;


  saveStatus_1 = true;
  saveStatus_2 = true;
  saveStatus_3 = true;
  saveStatus_4_1 = true;
  saveStatus_4_2 = true;
  saveStatus_4_3 = true;
  saveStatus_4_4 = true;
  saveStatus_4_5 = true;
  saveStatus_4_6 = true;
  saveStatus_5 = true;
  saveStatus_6 = true;
  saveStatus_7 = true;
  saveStatus_8 = true;
  saveStatus_9 = true;
  saveStatus_10 = true;
  saveStatus_11 = true;
  saveStatus_12 = true;
  saveStatus_13 = true;
  // ทบบวน saveStatus
  saveStatus_20 = true;
  saveStatus_21 = true;
  saveStatus_22 = true;
  saveStatus_23 = true;

  requestDataStatus_1 = false;
  saveDraftstatus: any = '';
  alertSaveStatus = false;
  send = false; // ส่งงาน
  boardApprove = null;
  edit = null;
  editText = null;

  approveStatus = null;

  businessStatus = null;
  personalStatus = null;
  housingStatus = null;
  otherStatus = null;
  nonCreditStatus = null;
  otherNonCreditStatus = null;

  is_updateBypass = false;
  page8bypass = false;
  page7show = false;
  typeSumary = false;

  // ทบทวน
  objective = null;

  // สอบทาน
  reviewStatus = true;
  reviewStatusShow = false;
  view = null;
  // role
  userRole = null;
  sendWorkStatus = false;
  level: any;
  // เเสดงหน้า set 12
  detailValidate_1 = null;  // เเสดงหน้า ข้อมูลประกาศใช้ผลิตภัณฑ์
  detailValidate_2 = null;
  detailValidate_3 = null;
  detailValidate_4_1 = null;
  detailValidate_4_2 = null;
  detailValidate_4_3 = null;
  detailValidate_4_4 = null;
  detailValidate_4_5 = null;
  detailValidate_4_6 = null;
  detailValidate_5 = null;
  detailValidate_6 = null;
  detailValidate_7 = null;
  detailValidate_8 = null;
  detailValidate_9 = null;
  detailValidate_10 = null;
  detailValidate_11 = null;
  detailValidate_12 = null;
  detailValidate_20 = null;
  detailValidate_21 = null;
  detailValidate_22 = null;
  detailValidate_23 = null;

  status_loading = false;
  validate = null;

  hideAlert = false;
  Timeout: any;

  @ViewChild(RequestDetailsComponent) RequestDetail: RequestDetailsComponent;
  @ViewChild(StepOneComponent) one: StepOneComponent;
  @ViewChild(StepTwoComponent) two: StepTwoComponent;
  // @ViewChild(StepOneComponent) three: StepThreeComponent;
  @ViewChild(BusinessLoanComponent) three_business: BusinessLoanComponent;
  @ViewChild(PersonalLoanComponent) three_personal: PersonalLoanComponent;
  @ViewChild(HousingLoanComponent) three_housing: HousingLoanComponent;
  @ViewChild(OtherComponent) three_other: OtherComponent;
  @ViewChild(NonCreditComponent) three_nonCredit: NonCreditComponent;
  @ViewChild(OtherNonCreaditComponent) three_other_nonCredit: OtherNonCreaditComponent;
  @ViewChild(StepFourComponent) four: StepFourComponent;
  @ViewChild(StepFiveComponent) five: StepFiveComponent;
  @ViewChild(StepSixComponent) six: StepSixComponent;
  @ViewChild(StepSevenComponent) seven: StepSevenComponent;
  @ViewChild(StepEightComponent) eight: StepEightComponent;
  @ViewChild(StepNineComponent) nine: StepNineComponent;
  @ViewChild(StepTenComponent) ten: StepTenComponent;
  @ViewChild(StepElevenComponent) eleven: StepElevenComponent;

  @ViewChild(StepTwoProductSummaryComponent) stepTwoProductSummary: StepTwoProductSummaryComponent;
  @ViewChild(StepTenSummaryComponent) stepTenSummaryComponent: StepTenSummaryComponent;
  @ViewChild(StepThirteenSummaryComponent) stepThirteenSummaryComponent: StepThirteenSummaryComponent;
  @ViewChild(StepFourtSummaryeenComponent) stepFourtSummaryeenComponent: StepFourtSummaryeenComponent;

  constructor(
    private router: Router,
    private service: SummaryOfConsiderationService,
    private dialog: MatDialog,
    private productDefultFirstStep: ProductDefultFirstStepService,
    private stepTwoService: StepTwoService,
    private sidebarService: RequestService,
  ) { }

  saveAll() {
    this.RequestDetail.saveDraft();
    this.one.saveDraft();
    this.two.saveDraft();
    if (this.businessStatus) {
      this.three_business.saveDraft();
    }
    if (this.personalStatus) {
      this.three_personal.saveDraft();
    }
    if (this.housingStatus) {
      this.three_housing.saveDraft();
    }
    if (this.nonCreditStatus) {
      this.three_nonCredit.saveDraft();
    }
    if (this.otherStatus) {
      this.three_other.saveDraft();
    }
    if (this.otherNonCreditStatus) {
      this.three_other_nonCredit.saveDraft();
    }
    this.four.saveDraft();
    this.five.saveDraft();
    this.six.saveDraft();
    this.seven.saveDraft();
    this.eight.saveDraft();
    this.nine.saveDraft();
    this.ten.saveDraft();
    this.eleven.saveDraft();
  }
  async saveInformation_1() {
    console.log('saveInformation_1');
    await this.RequestDetail.saveDraft();
    await this.CheckPage();
    this.alertSaveStatus = true;
  }
  saveInformation_2() {
    console.log('saveInformation_2');
    this.one.saveDraft();
    this.alertSaveStatus = true;
  }
  saveInformation_3() {
    console.log('saveInformation_3');
    this.two.saveDraft();
    this.alertSaveStatus = true;
  }
  saveInformation_4_1() {
    console.log('saveInformation_4_1');
    this.three_business.saveDraft();
    this.alertSaveStatus = true;
  }
  saveInformation_4_2() {
    console.log('saveInformation_4_2');
    this.three_personal.saveDraft();
    this.alertSaveStatus = true;
  }
  saveInformation_4_3() {
    console.log('saveInformation_4_3');
    this.three_housing.saveDraft();
    this.alertSaveStatus = true;
  }
  saveInformation_4_4() {
    console.log('saveInformation_4_4');
    this.three_other.saveDraft();
    this.alertSaveStatus = true;
  }
  saveInformation_4_5() {
    console.log('saveInformation_4_5');
    this.three_nonCredit.saveDraft();
    this.alertSaveStatus = true;
  }
  saveInformation_4_6() {
    console.log('saveInformation_4_6');
    this.three_other_nonCredit.saveDraft();
    this.alertSaveStatus = true;
  }
  saveInformation_5() {
    console.log('saveInformation_5');
    this.four.saveDraft();
    this.alertSaveStatus = true;
  }
  saveInformation_6() {
    console.log('saveInformation_6');
    this.five.saveDraft();
    this.alertSaveStatus = true;
  }
  saveInformation_7() {
    console.log('saveInformation_7');
    this.six.saveDraft();
    this.alertSaveStatus = true;
  }
  saveInformation_8() {
    console.log('saveInformation_8');
    this.seven.saveDraft();
    this.alertSaveStatus = true;
  }
  saveInformation_9() {
    console.log('saveInformation_9');
    this.eight.saveDraft();
    this.alertSaveStatus = true;
  }
  saveInformation_10() {
    console.log('saveInformation_10');
    this.nine.saveDraft();
    this.alertSaveStatus = true;
  }

  saveInformation_11() {
    console.log('saveInformation_11');
    this.ten.saveDraft();
    this.alertSaveStatus = true;
  }
  saveInformation_12() {
    console.log('saveInformation_12');
    this.eleven.saveDraft();
    this.alertSaveStatus = true;
  }

  saveInformation_13() {
    console.log('saveInformation_13');

    if (this.approveStatus !== null) {
      if (this.approveStatus === 'approve') {
        const data = {
          requestId: localStorage.getItem('requestId'),
          send: false,
          boardApprove: this.boardApprove,
          edit: this.edit,
          editText: this.editText
        };
        this.service.upDateReview(data).subscribe(ress => {
          if (ress['status'] === 'success') {
            this.saveStatus_13 = true;
            if (this.hideAlert === false) {
              Swal.fire({
                icon: 'success',
                title: 'บันทึกเรียบร้อย',
                showConfirmButton: false,
                timer: 1500,
              });
            }
          } else {
            Swal.fire({
              title: 'error',
              text: ress['message'],
              icon: 'error',
            });
            console.log('error', ress['message']);
          }
        });
      } else if (this.approveStatus === 'edit' && this.editText !== '' && this.editText !== null) {
        const data = {
          requestId: localStorage.getItem('requestId'),
          send: false,
          boardApprove: this.boardApprove,
          edit: this.edit,
          editText: this.editText
        };
        this.service.upDateReview(data).subscribe(ress => {
          if (ress['status'] === 'success') {
            this.saveStatus_13 = true;
            if (this.hideAlert === false) {
              Swal.fire({
                icon: 'success',
                title: 'บันทึกเรียบร้อย',
                showConfirmButton: false,
                timer: 1500,
              });
            }
          } else {

            Swal.fire({
              title: 'error',
              text: ress['message'],
              icon: 'error',
            });
            console.log('error', ress['message']);
          }
        });
      } else {
        alert('กรุณาระบุรายละเอียดเพิ่มเติมส่งกลับแก้ไข');
        return false;
      }
    } else {
      alert('กรุณาเลือกผลการสอบทาน สรุปผลการพิจารณา');
      return false;
    }

  }

  // ของทบทวน
  saveInformation_20() {
    console.log('saveInformation_20');
    this.stepTwoProductSummary.saveDraftData();
    this.alertSaveStatus = true;
  }

  saveInformation_21() {
    console.log('saveInformation_21');
    this.stepTenSummaryComponent.saveDraftData();
    this.alertSaveStatus = true;
  }

  saveInformation_22() {
    console.log('saveInformation_22');
    this.stepThirteenSummaryComponent.saveDraftData();
    this.alertSaveStatus = true;
  }

  saveInformation_23() {
    console.log('saveInformation_23');
    this.stepFourtSummaryeenComponent.saveDraftData();
    this.alertSaveStatus = true;
  }

  dataRequestStatus_1($event) {
    console.log('$event', $event);
    this.requestDataStatus_1 = $event;
  }

  getSaveStatus_1($event) {
    console.log('getSaveStatus_1 =>', $event);
    this.saveStatus_1 = $event;
    if ($event) {
      this.alertSave();
    } else {
      this.alertSaveStatus = false;
      this.service.status_state = true;
    }
  }

  getSaveStatus_2($event) {
    console.log('getSaveStatus_2 =>', $event);
    this.saveStatus_2 = $event;
    if ($event) {
      this.alertSave();
    } else {
      this.alertSaveStatus = false;
      this.service.status_state = true;
    }
  }
  getSaveStatus_3($event) {
    console.log('getSaveStatus_3 =>', $event);
    this.saveStatus_3 = $event;
    if ($event) {
      this.alertSave();
    } else {
      this.alertSaveStatus = false;
      this.service.status_state = true;
    }
  }
  getSaveStatus_4_1($event) {
    console.log('getSaveStatus_4_1 =>', $event);
    this.saveStatus_4_1 = $event;
    if ($event) {
      this.alertSave();
    } else {
      this.alertSaveStatus = false;
      this.service.status_state = true;
    }
  }

  getSaveStatus_4_2($event) {
    console.log('getSaveStatus_4_2 =>', $event);
    this.saveStatus_4_2 = $event;
    if ($event) {
      this.alertSave();
    } else {
      this.alertSaveStatus = false;
      this.service.status_state = true;
    }
  }

  getSaveStatus_4_3($event) {
    console.log('getSaveStatus_4_3 =>', $event);
    this.saveStatus_4_3 = $event;
    if ($event) {
      this.alertSave();
    } else {
      this.alertSaveStatus = false;
      this.service.status_state = true;
    }
  }

  getSaveStatus_4_4($event) {
    console.log('getSaveStatus_4_4 =>', $event);
    this.saveStatus_4_4 = $event;
    if ($event) {
      this.alertSave();
    } else {
      this.alertSaveStatus = false;
      this.service.status_state = true;
    }
  }

  getSaveStatus_4_5($event) {
    console.log('getSaveStatus_4_5 =>', $event);
    this.saveStatus_4_5 = $event;
    if ($event) {
      this.alertSave();
    } else {
      this.alertSaveStatus = false;
      this.service.status_state = true;
    }
  }

  getSaveStatus_4_6($event) {
    console.log('getSaveStatus_4_6 =>', $event);
    this.saveStatus_4_6 = $event;
    if ($event) {
      this.alertSave();
    } else {
      this.alertSaveStatus = false;
      this.service.status_state = true;
    }
  }

  getSaveStatus_5($event) {
    console.log('getSaveStatus_5 =>', $event);
    this.saveStatus_5 = $event;
    if ($event) {
      this.alertSave();
    } else {
      this.alertSaveStatus = false;
      this.service.status_state = true;
    }
  }

  getSaveStatus_6($event) {
    console.log('getSaveStatus_6 =>', $event);
    this.saveStatus_6 = $event;
    if ($event) {
      this.alertSave();
    } else {
      this.alertSaveStatus = false;
      this.service.status_state = true;
    }
  }

  getSaveStatus_7($event) {
    console.log('getSaveStatus_7 =>', $event);
    this.saveStatus_7 = $event;
    if ($event) {
      this.alertSave();
    } else {
      this.alertSaveStatus = false;
      this.service.status_state = true;
    }
  }

  getSaveStatus_8($event) {
    console.log('getSaveStatus_8 =>', $event);
    this.saveStatus_8 = $event;
    if ($event) {
      this.alertSave();
    } else {
      this.alertSaveStatus = false;
      this.service.status_state = true;
    }
  }

  getSaveStatus_9($event) {
    console.log('getSaveStatus_9 =>', $event);
    this.saveStatus_9 = $event;
    if ($event) {
      this.alertSave();
    } else {
      this.alertSaveStatus = false;
      this.service.status_state = true;
    }
  }

  getSaveStatus_10($event) {
    console.log('getSaveStatus_10 =>', $event);
    this.saveStatus_10 = $event;
    if ($event) {
      this.alertSave();
    } else {
      this.alertSaveStatus = false;
      this.service.status_state = true;
    }
  }

  getSaveStatus_11($event) {
    console.log('getSaveStatus_11 =>', $event);
    this.saveStatus_11 = $event;
    if ($event) {
      this.alertSave();
    } else {
      this.alertSaveStatus = false;
      this.service.status_state = true;
    }
  }

  getSaveStatus_12($event) {
    console.log('getSaveStatus_12 =>', $event);
    this.saveStatus_12 = $event;
    if ($event) {
      this.alertSave();
    } else {
      this.alertSaveStatus = false;
      this.service.status_state = true;
    }
  }

  // ของทบทวน
  getSaveStatus_20($event) {
    console.log('getSaveStatus_20 =>', $event);
    this.saveStatus_20 = $event;
    if ($event) {
      this.alertSave();
    } else {
      this.alertSaveStatus = false;
      this.service.status_state = true;
    }
  }

  getSaveStatus_21($event) {
    console.log('getSaveStatus_21 =>', $event);
    this.saveStatus_21 = $event;
    if ($event) {
      this.alertSave();
    } else {
      this.alertSaveStatus = false;
      this.service.status_state = true;
    }
  }

  getSaveStatus_22($event) {
    console.log('getSaveStatus_22 =>', $event);
    this.saveStatus_22 = $event;
    if ($event) {
      this.alertSave();
    } else {
      this.alertSaveStatus = false;
      this.service.status_state = true;
    }
  }

  getSaveStatus_23($event) {
    console.log('getSaveStatus_23 =>', $event);
    this.saveStatus_23 = $event;
    if ($event) {
      this.alertSave();
    } else {
      this.alertSaveStatus = false;
      this.service.status_state = true;
    }
  }

  alertSave() {
    if (this.alertSaveStatus && this.hideAlert === false) {
      Swal.fire({
        icon: 'success',
        title: 'คุณได้บันทึกเรียบร้อย',
        showConfirmButton: false,
        timer: 1500,
      });
      this.alertSaveStatus = false;
    }
  }

  checkSave() {

    // console.log('checkSave this.objective', this.objective);
    // console.log('checkSave this.saveStatus_1', this.saveStatus_1);
    // console.log('checkSave this.saveStatus_2', this.saveStatus_2);
    // console.log('checkSave this.saveStatus_3', this.saveStatus_3);
    // console.log('checkSave this.saveStatus_4_1', this.saveStatus_4_1);
    // console.log('checkSave this.saveStatus_4_2', this.saveStatus_4_2);
    // console.log('checkSave this.saveStatus_4_3', this.saveStatus_4_3);
    // console.log('checkSave this.saveStatus_4_4', this.saveStatus_4_4);
    // console.log('checkSave this.saveStatus_4_5', this.saveStatus_4_5);
    // console.log('checkSave this.saveStatus_4_6', this.saveStatus_4_6);
    // console.log('checkSave this.saveStatus_5', this.saveStatus_5);
    // console.log('checkSave this.saveStatus_6', this.saveStatus_6);
    // console.log('checkSave this.saveStatus_7', this.saveStatus_7);
    // console.log('checkSave this.saveStatus_8', this.saveStatus_8);
    // console.log('checkSave this.saveStatus_9', this.saveStatus_9);
    // console.log('checkSave this.saveStatus_10', this.saveStatus_10);
    // console.log('checkSave this.saveStatus_11', this.saveStatus_11);
    // console.log('checkSave this.saveStatus_12', this.saveStatus_12);
    // console.log('checkSave this.saveStatus_20', this.saveStatus_20);
    // console.log('checkSave this.saveStatus_21', this.saveStatus_21);
    // console.log('checkSave this.saveStatus_22', this.saveStatus_22);
    // console.log('checkSave this.saveStatus_23', this.saveStatus_23);

    if (this.objective === 'ผลิตภัณฑ์ใหม่') {
      if (this.saveStatus_1 === true
        && this.saveStatus_2 === true
        && this.saveStatus_3 === true
        && this.saveStatus_4_1 === true
        && this.saveStatus_4_2 === true
        && this.saveStatus_4_3 === true
        && this.saveStatus_4_4 === true
        && this.saveStatus_4_5 === true
        && this.saveStatus_4_6 === true
        && this.saveStatus_5 === true
        && this.saveStatus_6 === true
        && this.saveStatus_7 === true
        && this.saveStatus_8 === true
        && this.saveStatus_9 === true
        && this.saveStatus_10 === true
        && this.saveStatus_11 === true
        && this.saveStatus_12 === true
      ) {
        return true;
      } else {
        return false;
      }

    } else if (this.objective === 'ทบทวนผลิตภัณฑ์ระหว่างปี' || this.objective === 'ทบทวนผลิตภัณฑ์ประจำปี') {
      if (this.saveStatus_1 === true
        && this.saveStatus_2 === true
        && this.saveStatus_3 === true
        && this.saveStatus_4_1 === true
        && this.saveStatus_4_2 === true
        && this.saveStatus_4_3 === true
        && this.saveStatus_4_4 === true
        && this.saveStatus_4_5 === true
        && this.saveStatus_4_6 === true
        && this.saveStatus_5 === true
        && this.saveStatus_6 === true
        && this.saveStatus_7 === true
        && this.saveStatus_8 === true
        && this.saveStatus_9 === true
        && this.saveStatus_11 === true
        && this.saveStatus_12 === true
        && this.saveStatus_20 === true
        && this.saveStatus_21 === true
        && this.saveStatus_22 === true
        && this.saveStatus_23 === true

      ) {
        return true;
      } else {
        return false;
      }
    }
  }

  sendWork() {
    this.level = localStorage.getItem('level');
    let sendWork = false;
    if (this.userRole === 'PO') {
      if (this.level === 'ฝ่าย' || this.level === 'กลุ่ม' || this.level === 'สาย') {
        this.router.navigate(['/comment']);
      } else if (this.checkSave()) {
        const dialogRef_PO = this.dialog.open(DialogSummaryComponent, {
          disableClose: true,
          data: {
            headerDetail: 'ยืนยันการส่งงาน',
            bodyDetail1: 'คุณแน่ใจหรือไม่? ว่าต้องการส่งงาน',
            bodyDetail2: 'สรุปผลการพิจารณานี้',
            deleteQuestionStatus: false,
            icon: 'assets/img/checkmark-s.png'
          }
        });
        dialogRef_PO.afterClosed().subscribe(result => {
          sendWork = result.deleteStatus;
          console.log('sendWork =>', sendWork);
          if (sendWork) {
            // this.saveAll();
            this.service.productDetailPageStatus(localStorage.getItem('requestId')).subscribe(res => {
              const productPageStatus = res['data'];
              console.log('00009 ...> ', productPageStatus);
              console.log('sendWork this.typeSumary=> ', this.typeSumary);
              // summary
              if (this.typeSumary === true) {
                if (productPageStatus.pageDefault.page1 === true) {
                  if (productPageStatus.pageDetail.page1 === true) {
                    if (productPageStatus.pageDetail.page3 === true) {
                      if (productPageStatus.pageDetail.page4 === true) {
                        if (productPageStatus.pageDetail.page5 === true) {
                          if (productPageStatus.pageDetail.page6 === true) {
                            if (productPageStatus.pageDetail.page7 === true) {
                              if (productPageStatus.pageDetail.page8 === true || this.is_updateBypass === true) {
                                if (productPageStatus.pageDetail.page9 === true) {
                                  if (productPageStatus.pageDetail.page10 === true) {
                                    if (productPageStatus.pageDetail.page11 === true) {
                                      if (productPageStatus.pageDetail.page12 === true) {
                                        // send sendWork PO
                                        const data = {
                                          requestId: localStorage.getItem('requestId'),
                                          send: true,
                                          boardApprove: this.boardApprove,
                                          edit: this.edit,
                                          editText: this.editText
                                        };
                                        console.log('sendWork PO 01 data =>', data);
                                        this.service.upDateReview(data).subscribe(ress => {
                                          console.log('ress = >>>>', ress);
                                          const sendWork_PO = ress['status'];
                                          if (ress['status'] === 'success') {
                                            this.status_loading = false;
                                            Swal.fire({
                                              icon: 'success',
                                              title: 'คุณได้ส่งงานแล้วเรียบร้อย',
                                              showConfirmButton: false,
                                              timer: 1500,
                                            }).then(() => {
                                              this.checkDocument();
                                              this.getReview();
                                            });
                                          } else {
                                            this.status_loading = false;
                                            Swal.fire({
                                              title: 'error',
                                              text: ress['message'],
                                              icon: 'error',
                                            });
                                          }
                                        });
                                        // --------------------
                                      } else { alert('กรุณากรอกข้อมูลด้าน Complience ให้ครบถ้วน'); }
                                    } else { alert('กรุณากรอกข้อมูลด้าน Finance ให้ครบถ้วน'); }
                                  } else { alert('กรุณากรอกข้อมูล เป้าหมาย / ตัวชี้วัด ให้ครบถ้วน'); }
                                } else { alert('กรุณากรอกข้อมูล Cost & Benefit Analysis ให้ครบถ้วน'); }
                              } else { alert('กรุณากรอกข้อมูลความพร้อมด้านเทคโนโลยีสารสนเทศ ให้ครบถ้วน'); }
                            } else { alert('กรุณากรอกข้อมูลความพร้อมด้านการให้บริการลูกค้าอย่างเป็นธรรม ( Market Conduct ) ให้ครบถ้วน'); }
                          } else { alert('กรุณากรอกข้อมูลความพร้อมด้านการดำเนินงาน ( Operation ) ให้ครบถ้วน'); }
                        } else { alert('กรุณากรอกข้อมูลกระบวนการทำงาน ให้ครบถ้วน'); }
                      } else { alert('กรุณากรอกข้อมูลรูปแบบผลิตภัณฑ์  ให้ครบถ้วน'); }
                    } else { alert('กรุณากรอกข้อมูลสภาพตลาดและการแข่งขัน ให้ครบถ้วน'); }
                  } else { alert('กรุณากรอกข้อมูลแนวคิดและผลิตภัณฑ์ ให้ครบถ้วน'); }
                } else { alert('กรุณากรอกข้อมูลรายละเอียดคำขอ ให้ครบถ้วน'); }
              } else if (this.typeSumary === false) {
                if (productPageStatus.pageDefault.page1 === true) {
                  if (productPageStatus.pageDetail.page1 === true) {
                    if (productPageStatus.pageDetail.page2 === true) {
                      if (productPageStatus.pageDetail.page3 === true) {
                        if (productPageStatus.pageDetail.page4 === true) {
                          if (productPageStatus.pageDetail.page5 === true) {
                            if (productPageStatus.pageDetail.page6 === true) {
                              if (productPageStatus.pageDetail.page7 === true || this.is_updateBypass === true) {
                                if (productPageStatus.pageDetail.page8 === true) {
                                  if (productPageStatus.pageDetail.page9 === true) {
                                    if (productPageStatus.pageDetail.page10 === true) {
                                      if (productPageStatus.pageDetail.page11 === true) {
                                        // send sendWork PO
                                        const data = {
                                          requestId: localStorage.getItem('requestId'),
                                          send: true,
                                          boardApprove: this.boardApprove,
                                          edit: this.edit,
                                          editText: this.editText
                                        };
                                        console.log('sendWork PO 02 data =>', data);
                                        this.service.upDateReview(data).subscribe(ress => {
                                          console.log('ress = >>>>', ress);
                                          const sendWork_PO = ress['status'];
                                          if (ress['status'] === 'success') {
                                            this.status_loading = false;
                                            Swal.fire({
                                              icon: 'success',
                                              title: 'คุณได้ส่งงานแล้วเรียบร้อย',
                                              showConfirmButton: false,
                                              timer: 1500,
                                            }).then(() => {
                                              this.checkDocument();
                                              this.getReview();
                                            });
                                          } else {
                                            this.status_loading = false;
                                            Swal.fire({
                                              title: 'error',
                                              text: ress['message'],
                                              icon: 'error',
                                            });
                                          }
                                        });
                                        // --------------------
                                      } else { alert('กรุณากรอกข้อมูลด้าน Complience ให้ครบถ้วน'); }
                                    } else { alert('กรุณากรอกข้อมูลด้าน Finance ให้ครบถ้วน'); }
                                  } else { alert('กรุณากรอกข้อมูล เป้าหมาย / ตัวชี้วัด ให้ครบถ้วน'); }
                                } else { alert('กรุณากรอกข้อมูล Cost & Benefit Analysis ให้ครบถ้วน'); }
                              } else { alert('กรุณากรอกข้อมูลความพร้อมด้านเทคโนโลยีสารสนเทศ ให้ครบถ้วน'); }
                            } else { alert('กรุณากรอกข้อมูลความพร้อมด้านการให้บริการลูกค้าอย่างเป็นธรรม ( Market Conduct ) ให้ครบถ้วน'); }
                          } else { alert('กรุณากรอกข้อมูลความพร้อมด้านการดำเนินงาน ( Operation ) ให้ครบถ้วน'); }
                        } else { alert('กรุณากรอกข้อมูลกระบวนการทำงาน ให้ครบถ้วน'); }
                      } else { alert('กรุณากรอกข้อมูลรูปแบบผลิตภัณฑ์  ให้ครบถ้วน'); }
                    } else { alert('กรุณากรอกข้อมูลสภาพตลาดและการแข่งขัน ให้ครบถ้วน'); }
                  } else { alert('กรุณากรอกข้อมูลแนวคิดและผลิตภัณฑ์ ให้ครบถ้วน'); }
                } else { alert('กรุณากรอกข้อมูลรายละเอียดคำขอ ให้ครบถ้วน'); }
              }
            });
          }
        });
      } else {
        const dialogRef_PO_save = this.dialog.open(DialogSummarySaveComponent, {
          disableClose: true,
          data: {
            headerDetail: 'ยืนยันการดำเนินการ',
            bodyDetail1: 'ไม่สามารถส่งงานได้ กรุณาบันทึกหัวข้อดังต่อไปนี้',
            bodyDetail2: {
              objective: this.objective,
              saveStatus_1: this.saveStatus_1,
              saveStatus_2: this.saveStatus_2,
              saveStatus_3: this.saveStatus_3,
              saveStatus_4_1: this.saveStatus_4_1,
              saveStatus_4_2: this.saveStatus_4_2,
              saveStatus_4_3: this.saveStatus_4_3,
              saveStatus_4_4: this.saveStatus_4_4,
              saveStatus_4_5: this.saveStatus_4_5,
              saveStatus_4_6: this.saveStatus_4_6,
              saveStatus_5: this.saveStatus_5,
              saveStatus_6: this.saveStatus_6,
              saveStatus_7: this.saveStatus_7,
              saveStatus_8: this.saveStatus_8,
              saveStatus_9: this.saveStatus_9,
              saveStatus_10: this.saveStatus_10,
              saveStatus_11: this.saveStatus_11,
              saveStatus_12: this.saveStatus_12,
              saveStatus_20: this.saveStatus_20,
              saveStatus_21: this.saveStatus_21,
              saveStatus_22: this.saveStatus_22,
              saveStatus_23: this.saveStatus_23,
            },
            deleteQuestionStatus: false,
            icon: 'assets/img/Alarm-blue.svg'
          }
        });
      }
    } else if (this.userRole === 'PC') {
      if (this.saveStatus_13 === true && (this.boardApprove !== null || this.edit !== null)) {
        const dialogRef_PO = this.dialog.open(DialogSummaryComponent, {
          disableClose: true,
          data: {
            headerDetail: 'ยืนยันการส่งงาน',
            bodyDetail1: 'คุณแน่ใจหรือไม่? ว่าต้องการส่งงาน',
            bodyDetail2: 'สอบทานสรุปผลการพิจารณานี้', deleteQuestionStatus: false,
            icon: 'assets/img/checkmark-s.png'
          }
        });
        dialogRef_PO.afterClosed().subscribe(result => {
          sendWork = result.deleteStatus;
          console.log('sendWork PC =>', sendWork);
          if (sendWork) {
            const data = {
              requestId: localStorage.getItem('requestId'),
              send: true,
              boardApprove: this.boardApprove,
              edit: this.edit,
              editText: this.editText
            };
            console.log('sendWork PC data =>', data);
            this.service.upDateReview(data).subscribe(ress => {
              if (ress['status'] === 'success') {
                this.saveStatus_13 = true;
                this.status_loading = false;
                Swal.fire({
                  icon: 'success',
                  title: 'คุณได้ส่งงานแล้วเรียบร้อย',
                  showConfirmButton: false,
                  timer: 1500,
                }).then(() => {
                  this.getReview();
                });
              } else {
                this.status_loading = false;
                Swal.fire({
                  title: 'error',
                  text: ress['message'],
                  icon: 'error',
                });
                console.log('error', ress['message']);
              }
            });
          }
        });
      } else {
        this.dialog.open(DialogSummaryComponent, {
          disableClose: true,
          // tslint:disable-next-line: max-line-length
          data: {
            headerDetail: 'ยืนยันการดำเนินการ',
            bodyDetail1: 'ไม่สามารถส่งงานได้ กรุณาบันทึกหัวข้อ',
            bodyDetail2: 'สอบทานการสรุปผลการพิจารณา',
            deleteQuestionStatus: false,
            icon: 'assets/img/Alarm-blue.svg'
          }
        });
      }
    }
  }


  checkProductDetailData() {
    this.service.productDetailPageStatus(localStorage.getItem('requestId')).subscribe(res => {
      const data = res['data'];
    });
  }

  changeBoardApprove() {
    if (this.approveStatus === 'approve') {
      this.boardApprove = true;
      this.edit = false;
      this.editText = null;
    } else if (this.approveStatus === 'edit') {
      this.boardApprove = false;
      this.edit = true;
    } else {
      this.boardApprove = false;
      this.edit = true;
    }
    this.saveStatus_13 = false;
    this.service.status_state = true;
    console.log('7777777');
  }
  getReview() {
    this.approveStatus = null;
    this.service.getReview(localStorage.getItem('requestId')).subscribe(res => {
      if (res['status'] === 'success') {
        const dataReview = res['data'];
        console.log('0007>>>>>', dataReview);
        console.log('0008>>>>>', res['view']);
        this.view = res['view'];
        this.validate = dataReview.validate;
        if (dataReview) {
          if (dataReview.boardApprove === true && dataReview.edit === false) {
            this.approveStatus = 'approve';
            this.boardApprove = true;
            this.edit = false;
            this.editText = null;
          } else if (dataReview.boardApprove === false && dataReview.edit === true) {
            this.approveStatus = 'edit';
            this.boardApprove = false;
            this.edit = true;
            this.editText = dataReview.editText;
          } else {
            this.boardApprove = null;

            this.edit = null;
            if (dataReview.editText) {
              this.editText = dataReview.editText;
            } else {
              this.editText = null;
            }
          }
        }

        if (this.validate) {
          if (this.approveStatus === 'approve') {
            this.saveStatus_13 = true;
          } else if (this.approveStatus === 'edit') {
            if (this.editText !== null || this.editText !== '') {
              this.saveStatus_13 = true;
            } else {
              this.saveStatus_13 = false;
            }
          } else {
            this.saveStatus_13 = true;
          }
        }
      } else {
        alert(res['message']);
      }
    });
  }

  CheckPage() {
    if (localStorage) {
      this.service.productGetGroup(localStorage.getItem('requestId')).subscribe(res => {
        console.log('=> three =>', res);
        this.businessStatus = false;
        this.personalStatus = false;
        this.housingStatus = false;
        this.otherStatus = false;
        this.nonCreditStatus = false;
        this.otherNonCreditStatus = false;
        const productGet = res['data'];
        console.log('productGet =>', productGet);
        productGet.product.forEach(ele => {
          if (ele === 'business_loan') {
            this.businessStatus = true;
          }
          if (ele === 'persenal_loan') {
            this.personalStatus = true;
          }
          if (ele === 'housing_loan') {
            this.housingStatus = true;
          }
          if (ele === 'other') {
            this.otherStatus = true;
          }
          if (ele === 'non_credit_product') {
            this.nonCreditStatus = true;
          }
          if (ele === 'other_non_creadit') {
            this.otherNonCreditStatus = true;
          }
        });
      });
    }
  }

  ngOnInit() {
    console.log('alertSaveStatusalertSaveStatusalertSaveStatus', this.alertSaveStatus);
    this.service.status_state = false;
    if (localStorage) {
      this.userRole = localStorage.getItem('role');
      if (this.userRole === 'PO') {
        this.reviewStatusShow = false;
        this.reviewStatus = false;
        this.sendWorkStatus = true;
        this.detailValidate_1 = true;
        this.detailValidate_2 = true;
        this.detailValidate_3 = true;
        this.detailValidate_4_1 = true;
        this.detailValidate_4_2 = true;
        this.detailValidate_4_3 = true;
        this.detailValidate_4_4 = true;
        this.detailValidate_4_5 = true;
        this.detailValidate_4_6 = true;
        this.detailValidate_5 = true;
        this.detailValidate_6 = true;
        this.detailValidate_7 = true;
        this.detailValidate_8 = true;
        this.detailValidate_9 = true;
        this.detailValidate_10 = true;
        this.detailValidate_11 = true;
        this.detailValidate_12 = true;
        this.detailValidate_20 = true;
        this.detailValidate_21 = true;
        this.detailValidate_22 = true;
        this.detailValidate_23 = true;
        // set show
        this.information_1 = true;
        this.information_2 = true;
        this.information_4_1 = true;
        this.information_4_2 = true;
        this.information_4_3 = true;
        this.information_4_4 = true;
        this.information_4_5 = true;
        this.information_4_6 = true;
        this.information_10 = true;
        this.information_21 = true;
        // this.information_13 = true;

        this.checkDocument();
      } else if (this.userRole === 'PC') {
        this.reviewStatusShow = true;
        this.reviewStatus = true;
        this.sendWorkStatus = true;
        this.detailValidate_1 = false;
        this.detailValidate_2 = false;
        this.detailValidate_3 = false;
        this.detailValidate_4_1 = false;
        this.detailValidate_4_2 = false;
        this.detailValidate_4_3 = false;
        this.detailValidate_4_4 = false;
        this.detailValidate_4_5 = false;
        this.detailValidate_4_6 = false;
        this.detailValidate_5 = false;
        this.detailValidate_6 = false;
        this.detailValidate_7 = false;
        this.detailValidate_8 = false;
        this.detailValidate_9 = false;
        this.detailValidate_10 = false;
        this.detailValidate_11 = false;
        this.detailValidate_12 = false;
        this.detailValidate_20 = false;
        this.detailValidate_21 = false;
        this.detailValidate_22 = false;
        this.detailValidate_23 = false;
        // set show
        this.information_1 = true;
        this.information_2 = true;
        this.information_4_1 = true;
        this.information_4_2 = true;
        this.information_4_3 = true;
        this.information_4_4 = true;
        this.information_4_5 = true;
        this.information_4_6 = true;
        this.information_10 = true;
        this.information_21 = true;
        this.information_13 = true;

        this.checkDocument();
      } else {
        this.reviewStatusShow = true;
        this.reviewStatus = true;
        this.sendWorkStatus = false;
        this.detailValidate_1 = false;
        this.detailValidate_2 = false;
        this.detailValidate_3 = false;
        this.detailValidate_4_1 = false;
        this.detailValidate_4_2 = false;
        this.detailValidate_4_3 = false;
        this.detailValidate_4_4 = false;
        this.detailValidate_4_5 = false;
        this.detailValidate_4_6 = false;
        this.detailValidate_5 = false;
        this.detailValidate_6 = false;
        this.detailValidate_7 = false;
        this.detailValidate_8 = false;
        this.detailValidate_9 = false;
        this.detailValidate_10 = false;
        this.detailValidate_11 = false;
        this.detailValidate_12 = false;
        this.detailValidate_20 = false;
        this.detailValidate_21 = false;
        this.detailValidate_22 = false;
        this.detailValidate_23 = false;
        this.checkDocument();
      }
      const requestId = Number(localStorage.getItem('requestId'));
      this.productDefultFirstStep.getDefultFirstStep(requestId, 'summary').subscribe(resss => {
        if (resss['data'] !== null) {
          const type = resss['data'].objective;
          if (type === 'ทบทวนผลิตภัณฑ์ระหว่างปี') {
            this.typeSumary = true;
          } else if (type === 'ทบทวนผลิตภัณฑ์ประจำปี') {
            this.typeSumary = true;
          } else if (type === 'ผลิตภัณฑ์ใหม่') {
            this.typeSumary = false;
          }
        }
      });

      this.stepTwoService.getPage2({ id: localStorage.getItem('requestId') }).subscribe(ress => {
        console.log('000007>> ', ress);
        const ressData = ress['data'];
        if (ressData.is_update === 'ไม่ใช่') {
          this.is_updateBypass = true;
          this.page7show = false;
        } else {
          this.is_updateBypass = false;
          this.page7show = true;
        }
        console.log('000008>> ', this.is_updateBypass);
      });

    }
    this.CheckPage();
    this.getReview();
  }

  back() {
    // localStorage.setItem('requestId', '');
    this.router.navigate(['/dashboard1']);
  }

  checkDocument() {
    if (localStorage.getItem('requestId')) {
      const requestId = Number(localStorage.getItem('requestId'));
      this.productDefultFirstStep.getDefultFirstStep(requestId, 'summary').subscribe(res => {
        if (res['data'] !== null) {
          const productDefaultStep1 = res['data'];


          if (productDefaultStep1.validate === true) {
            this.reviewStatus = false;
          } else {
            this.reviewStatus = true;
          }
          console.log('this.reviewStatus 00000=>', this.reviewStatus);
          this.objective = productDefaultStep1.objective;
          console.log('this.objective 00000=>', this.objective);
        }
      });
    }
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('summary-of-consideration');
    const subject = new Subject<boolean>();
    const status = this.checkSave();
    const statusSevice = this.sidebarService.outPageStatus();
    console.log('002 statusSevice status', statusSevice);
    console.log('002 summary-of-consideration status', status);
    console.log('002 saveStatus_13 status', this.saveStatus_13);
    if (status === false || this.saveStatus_13 === false) {
      // -----
      const dialogRef = this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        if (data.returnStatus === true) {
          this.hideAlert = true;
          if (this.saveStatus_1 === false) {
            this.saveInformation_1();
          }

          if (this.saveStatus_2 === false) {
            this.saveInformation_2();
          }

          if (this.saveStatus_3 === false) {
            this.saveInformation_3();
          }

          if (this.saveStatus_4_1 === false) {
            this.saveInformation_4_1();
          }

          if (this.saveStatus_4_2 === false) {
            this.saveInformation_4_2();
          }

          if (this.saveStatus_4_3 === false) {
            this.saveInformation_4_3();
          }

          if (this.saveStatus_4_4 === false) {
            this.saveInformation_4_4();
          }

          if (this.saveStatus_4_5 === false) {
            this.saveInformation_4_5();
          }

          if (this.saveStatus_4_6 === false) {
            this.saveInformation_4_6();
          }

          if (this.saveStatus_5 === false) {
            this.saveInformation_5();
          }

          if (this.saveStatus_6 === false) {
            this.saveInformation_6();
          }

          if (this.saveStatus_7 === false) {
            this.saveInformation_7();
          }

          if (this.saveStatus_8 === false) {
            this.saveInformation_8();
          }

          if (this.saveStatus_9 === false) {
            this.saveInformation_9();
          }

          if (this.saveStatus_10 === false) {
            this.saveInformation_10();
          }

          if (this.saveStatus_11 === false) {
            this.saveInformation_11();
          }

          if (this.saveStatus_12 === false) {
            this.saveInformation_12();
          }

          if (this.saveStatus_20 === false) {
            this.saveInformation_20();
          }

          if (this.saveStatus_21 === false) {
            this.saveInformation_21();
          }

          if (this.saveStatus_22 === false) {
            this.saveInformation_22();
          }

          if (this.saveStatus_23 === false) {
            this.saveInformation_23();
          }
          if (this.saveStatus_13 === false) {
            this.saveInformation_13();
          }
          this.status_loading = true;
          this.Timeout = setTimeout(() => {
            if (this.checkSave() === true && this.saveStatus_13 === true) {
              console.log('raturnStatus T', this.saveDraftstatus);
              subject.next(true);
            } else if (this.alertSaveStatus === false) {
              console.log('raturnStatus alertSaveStatus F ', this.saveDraftstatus);
              subject.next(false);
            } else {
              console.log('raturnStatus F', this.saveDraftstatus);
              subject.next(false);
            }
            this.status_loading = false;
          }, 5000);

        } else if (data.returnStatus === false) {
          this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
          // return false;
        }
      });
      return subject;
    } else {
      return true;
    }
  }

}
