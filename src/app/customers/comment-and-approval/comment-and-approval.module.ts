import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommentAndApprovalComponent } from './comment-and-approval.component';
import { SummaryOfConsiderationComponent } from './summary-of-consideration/summary-of-consideration.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { RequestDetailsComponent } from './summary-of-consideration/request-details/request-details.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule,
  MatRadioModule,
  MatCheckboxModule,
  MatTableModule,
  MatInputModule,
  MatDialogModule } from '@angular/material';
import { AngularMyDatePickerModule } from 'angular-mydatepicker';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ClickOutsideModule } from 'ng-click-outside';
import { RequestModule } from '../request/request.module';
import { CommentAndApprovalRoutingModule } from './comment-and-approval-routing.module';
import { DialogSummaryComponent } from './summary-of-consideration/dialog-summary/dialog-summary.component';
import { DialogSummarySaveComponent } from './summary-of-consideration/dialog-summary-save/dialog-summary-save.component';
import { DateModule } from '../datepicker/date/date.module';
import { RequestSummaryModule } from '../request-summary/request-summary.module';
import { CanDeactivateGuard } from '../../services/configGuard/config-guard.service';



@NgModule({
  declarations: [
    CommentAndApprovalComponent,
    SummaryOfConsiderationComponent,
    RequestDetailsComponent,
    DialogSummaryComponent,
    DialogSummarySaveComponent,
  ],
  imports: [
    CommonModule,
    RequestModule,
    RequestSummaryModule,
    CommentAndApprovalRoutingModule,
    MatExpansionModule,
    FormsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatRadioModule,
    MatCheckboxModule,
    AngularMyDatePickerModule,
    MatTableModule,
    MatInputModule,
    DragDropModule,
    ClickOutsideModule,
    MatDialogModule,
    DateModule
  ],
  providers: [ CanDeactivateGuard ],
  entryComponents: [DialogSummaryComponent, DialogSummarySaveComponent],
  exports: [RequestDetailsComponent]
})
export class CommentAndApprovalModule { }
