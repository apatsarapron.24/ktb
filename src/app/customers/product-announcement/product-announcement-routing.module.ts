import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductAnnouncementComponent } from './product-announcement.component';
import { CanDeactivateGuard } from './../../services/configGuard/config-guard.service';

const routes: Routes = [
  {
    path: '',
    component: ProductAnnouncementComponent,
    canDeactivate: [CanDeactivateGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductAnnouncementRoutingModule { }
