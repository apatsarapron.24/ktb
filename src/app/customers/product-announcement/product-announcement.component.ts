import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularMyDatePickerDirective, IAngularMyDpOptions, IMyDateModel } from 'angular-mydatepicker';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { ProductAnnouncementService } from '../../services/product-announcement/product-announcement.service';
import * as moment from 'moment';
import Swal from 'sweetalert2';
import { DateComponent } from '../datepicker/date/date.component';
import { Router } from '@angular/router';
import { saveAs } from 'file-saver';
import { type } from 'os';
import { CanDeactivateGuard } from './../../services/configGuard/config-guard.service';
import { Observable } from 'rxjs/Observable';
import { DialogSaveStatusComponent } from '../product-announcement/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
declare var $: any;

@Component({
    selector: 'app-product-announcement',
    templateUrl: './product-announcement.component.html',
    styleUrls: ['./product-announcement.component.scss']
})
export class ProductAnnouncementComponent implements OnInit {
    constructor(
        private productsersice: ProductAnnouncementService,
        private router: Router,
        public dialog: MatDialog,
        private guardService: CanDeactivateGuard
    ) { }

    @ViewChild(DateComponent) Date: DateComponent;
    saveDraftstatus: any = '';
    displayedColumns_file: string[] = ['fileName', 'fileSize', 'delete'];
    dataSource_file: MatTableDataSource<FileList>;

    // private model: Object = {};
    model: IMyDateModel = null;
    @ViewChild('dp') mydp: AngularMyDatePickerDirective;

    model_date1: any = null;
    model_date2: any = null;

    role = '';
    // data get

    product = {
        presentDate: null,
        promulgateModel: {
            requestId: null,
            promulgateDate: null,
            circularNo: null,
            circularDate: null,
            dept: null,
            sctr: null,
            grp: null,
            manager: null,
            managerTel: null,
            coManager: null,
            coManagerTel: null,
        },
        businessSignModel: [{
            id: null,
            business: null,
            signDate: null,
            comment: null,
        }],
        fileUpload: {
            file: [
                {
                    id: null,
                    path: null,
                    fileName: null,
                    base64File: null,
                    fileSize: null,
                }
            ]
        },
        validate: null,
    };
    testfile = [{ filename: 'เอกสารแนบ1.pptx', filesize: '174KB' }];

    fileToUpload: any = [];
    sendfile = '';
    files: any = {
        file: [],
        fileDelete: [],
    };

    presentdate: any;
    promulgate: any;
    filedata: any;
    showfile: any;
    // businessSignModel: any;
    departmenttext = [];
    commenttext = [];
    signdatetext = [];
    listConsider: any = [];
    confirmState = 'confirm';
    page = '';
    businessmode = '';
    FileOverSize: any;
    status_loading = false;
    // ลบข้อมูล
    dataid = [];
    data = null;

    downloadfile_by = [];
    saveDraftstatus_State = false;

    ngOnInit() {
        this.role = localStorage.getItem('role');
        console.log('role', this.role);
        if (this.role === 'PO') {
            this.getProductannouncementPO();
        } else {
            this.getProductannouncementPC();
        }


    }
    // get role PC fontend <== backend
    getProductannouncementPC() {
        const requestid = {
            'requestId': localStorage.getItem('requestId'),
        };
        this.status_loading = true;
        this.productsersice.getpromulgate(requestid).subscribe(res => {
            console.log('>>>>>>>', res);
            if (res['status'] === 'success' && res['data']) {
                const getdata = res['data'];
                this.status_loading = false;
                this.product.validate = getdata['validate'];
                if (getdata['presentDate'] === null) {
                    this.product.presentDate = '-';
                } else {
                    this.product.presentDate = getdata['presentDate'];
                }
                this.product.businessSignModel = getdata['businessSignModel'];
                this.product.promulgateModel = getdata['promulgateModel'];
                this.showfile = getdata['fileUpload'];
                // console.log(getdata);
            }
        });
    }
    // get role PO fontend <== backend
    changeDate(date: any) {
        const SendI = this.Date.changeDate(date);
        this.changeSaveDraft();
        return SendI;
    }
    getProductannouncementPO() {
        const requestid = {
            'requestId': localStorage.getItem('requestId'),
        };
        this.productsersice.getpromulgate(requestid).subscribe(res => {
            if (res['status'] === 'success' && res['data']) {
                const getdata = res['data'];
                if (getdata['presentDate'] === null) {
                    this.product.presentDate = '-';
                } else {
                    this.product.presentDate = getdata['presentDate'];
                }
                // console.log(getdata);

                if (getdata['businessSignModel'].length === 0) {
                    this.departmenttext.push('สายงานบริหารความเสี่ยง', 'ฝ่ายบริหารความเสี่ยงด้านเทคโนโลยีสารสนเทศ',
                        'สายงานกำกับกฎเกณฑ์และกฎหมาย', 'สายงานปฏิบัติงาน', 'สายงานบริหารการเงิน'
                    );
                    // console.log(this.departmenttext);
                    // for (let index = 0; index < this.departmenttext.length; index++) {
                    //     const element = this.departmenttext[index];
                    this.product.businessSignModel = [{
                        id: '',
                        business: 'สายงานบริหารความเสี่ยง',
                        signDate: null,
                        comment: '',
                    },
                    {
                        id: '',
                        business: 'ฝ่ายบริหารความเสี่ยงด้านเทคโนโลยีสารสนเทศ',
                        signDate: null,
                        comment: '',
                    },
                    {
                        id: '',
                        business: 'สายงานกำกับกฎเกณฑ์และกฎหมาย',
                        signDate: null,
                        comment: '',
                    },
                    {
                        id: '',
                        business: 'สายงานปฏิบัติงาน',
                        signDate: null,
                        comment: '',
                    },
                    {
                        id: '',
                        business: 'สายงานบริหารการเงิน',
                        signDate: null,
                        comment: '',
                    },
                    ];
                    // }
                } else {
                    this.listConsider = getdata['businessSignModel'];
                    this.product.businessSignModel = [];
                    for (let index = 0; index < this.listConsider.length; index++) {
                        const element = this.listConsider[index];
                        this.departmenttext.push(element.business);
                        // console.log(this.departmenttext);
                        this.commenttext.push(element.comment);
                        // this.signdatetext.push(element.signDate);
                        const datecircusign = moment(element.signDate).format('MM/DD/YYYY');
                        // console.log(covert);
                        let date3 = element.signDate;
                        this.signdatetext.push(date3);
                        // console.log('signdate',this.commenttext);
                        this.product.businessSignModel.push({
                            id: element.id,
                            business: element.business,
                            signDate: element.signDate,
                            comment: element.comment,
                        }
                        );
                    }
                    this.listConsider = [];
                }
                this.product.validate = getdata['validate'];
                this.promulgate = getdata['promulgateModel'];
                // this.model_date = this.promulgate.circularDate;

                // let date = new Date(datecircu).toISOString();
                this.model_date1 = this.promulgate.promulgateDate;
                console.log(this.model_date1);
                console.log('type', typeof (this.model_date1));

                this.product.promulgateModel.circularNo = this.promulgate.circularNo;
                // const datepromu = moment(this.promulgate.promulgateDate).format('MM/DD/YYYY');
                // let date1 = new Date(datepromu).toISOString();
                this.model_date2 = this.promulgate.circularDate;
                this.product.promulgateModel.requestId = this.promulgate.requestId;
                this.product.promulgateModel.promulgateDate = this.promulgate.promulgateDate;
                this.product.promulgateModel.circularDate = this.promulgate.circularDate;
                this.product.promulgateModel.dept = this.promulgate.dept;
                this.product.promulgateModel.sctr = this.promulgate.sctr;
                this.product.promulgateModel.grp = this.promulgate.grp;
                this.product.promulgateModel.manager = this.promulgate.manager;
                this.product.promulgateModel.managerTel = this.promulgate.managerTel;
                this.product.promulgateModel.coManager = this.promulgate.coManager;
                this.product.promulgateModel.coManagerTel = this.promulgate.coManagerTel;
                this.showfile = getdata['fileUpload'];
                this.dataSource_file = new MatTableDataSource(this.showfile.file);
                this.hideDownloadFile();
            }
            this.status_loading = false;
        });

        console.log('this.product', this.product);
    }




    // save send = false fontend ==> backend
    savepromulgate() {
        this.listConsider = [];
        for (let index = 0; index < this.product.businessSignModel.length; index++) {
            this.listConsider.push({
                id: '',
                business: '',
                signDate: '',
                comment: '',
            });
            if (this.product.businessSignModel[index].id !== null) {
                this.listConsider[index].id = this.product.businessSignModel[index].id;
            }
        }
        for (let index = 0; index < this.product.businessSignModel.length; index++) {
            if(this.departmenttext[index] === undefined){
                this.listConsider[index].business = '';
            }else{
                           this.listConsider[index].business = this.departmenttext[index]; 
            }

        }
        for (let index = 0; index < this.commenttext.length; index++) {
            this.listConsider[index].comment = this.commenttext[index];
        }
        // for (let index = 0; index < this.product.businessSignModel.length; index++) {
        //     if (this.listConsider.length !== 0) {
        //         this.listConsider[index].id = this.product.businessSignModel[index].id;
        //     }
        // }

        for (let index = 0; index < this.signdatetext.length; index++) {
            this.listConsider[index].signDate = this.signdatetext[index];
        }

        // // date circulardate
        // if (this.model_date == null) {
        //     this.product.promulgateModel.circularDate = null;
        // } else {
        //     this.product.promulgateModel.circularDate = this.model_date;
        // }

        // date circulardate
        // console.log('type:', typeof (this.model_date.singleDate.jsDate));

        // const datecircukk = moment(this.model_date.singleDate.jsDate).format('MM/DD/YYYY');
        // const datepro = moment(this.model_date2.singleDate.jsDate).format('MM/DD/YYYY');
        // console.log(datecircukk);

        // if (datecircukk === 'Invalid date') {
        // this.product.promulgateModel.circularDate = null;
        // } else {
        this.product.promulgateModel.circularDate = this.model_date2;
        // }
        console.log('date>>>>', this.model_date1);
        // date circulardate
        // if (datepro === 'Invalid date') {
        //     this.product.promulgateModel.promulgateDate = null;
        // } else {
        this.product.promulgateModel.promulgateDate = this.model_date1;
        // }

        // console.log('list', this.listConsider);
        this.data = {
            'requestId': localStorage.getItem('requestId'),
            'send': false,
            'deletebusinessSign': this.dataid,
            'businessSign': this.listConsider,
            'promulgateModel': {
                promulgateDate: null,
                circularNo: this.product.promulgateModel.circularNo,
                circularDate: null,
                dept: this.product.promulgateModel.dept,
                sctr: this.product.promulgateModel.sctr,
                grp: this.product.promulgateModel.grp,
                manager: this.product.promulgateModel.manager,
                managerTel: this.product.promulgateModel.managerTel,
                coManager: this.product.promulgateModel.coManager,
                coManagerTel: this.product.promulgateModel.coManagerTel
            },
            'fileUpload': {
                'file': this.showfile.file,
                'fileDelete': this.files.fileDelete,
            },
        };
        // console.log(data['promulgateModel'].circularDate);
        // console.log('>>>>>>>test',this.signdatetext);
        // Date circularDate

        if (this.product.promulgateModel.circularDate) {
            // tslint:disable-next-line: max-line-length
            // data.promulgateModel.circularDate = moment(this.product.promulgateModel.circularDate.singleDate.jsDate).format().substring(0, 10);
            this.data.promulgateModel.circularDate = this.product.promulgateModel.circularDate;
        } else { this.data.promulgateModel.circularDate = null; }

        // Date promulgateDate
        if (this.product.promulgateModel.promulgateDate) {
            // tslint:disable-next-line:max-line-length
            // data.promulgateModel.promulgateDate = moment(this.product.promulgateModel.promulgateDate.singleDate.jsDate).format().substring(0, 10);
            this.data.promulgateModel.promulgateDate = this.product.promulgateModel.promulgateDate;
        } else { this.data.promulgateModel.promulgateDate = null; }

        // API POST
        console.log('data to save', this.data);
        this.status_loading = true;
        this.productsersice.postpromulgate(this.data).subscribe(res => {
            console.log('sen save', res);
            if (res['status'] === 'success') {
                this.status_loading = false;
                this.saveDraftstatus_State = false;
                this.saveDraftstatus = res['status'];
                setTimeout(() => {
                    this.saveDraftstatus = 'fail';
                }, 3000);
                this.listConsider = [];
                this.getProductannouncementPC();
                this.dataid = [];
            } else {
                Swal.fire({
                    title: 'error',
                    text: res['message'],
                    icon: 'error',
                });
                this.status_loading = false;
                this.data = null;
                this.listConsider = [];
                this.dataid = [];
            }
        }, err => {
            this.status_loading = false;
            console.log(err);
          });
    }

    // save send = true fontend ==> backend
    sendpromulgate() {
        if (this.departmenttext.length > 0 && this.signdatetext.length > 0 && this.commenttext.length > 0
            && this.model_date1 !== null && this.product.promulgateModel.circularNo && this.model_date2 !== null) {
            for (let index = 0; index < this.departmenttext.length; index++) {
                this.listConsider.push({
                    id: '',
                    business: this.departmenttext[index],
                    signDate: null,
                    comment: '',
                });
            }
            for (let index = 0; index < this.commenttext.length; index++) {
                this.listConsider[index].comment = this.commenttext[index];
            }
            for (let index = 0; index < this.product.businessSignModel.length; index++) {
                if (this.listConsider.length !== 0) {
                    this.listConsider[index].id = this.product.businessSignModel[index].id;
                }
            }

            for (let index = 0; index < this.signdatetext.length; index++) {
                this.listConsider[index].signDate = this.signdatetext[index];
            }

            // // date circulardate
            // if (this.model_date == null) {
            //     this.product.promulgateModel.circularDate = null;
            // } else {
            //     this.product.promulgateModel.circularDate = this.model_date;
            // }

            // date circulardate
            // console.log('type:', typeof (this.model_date.singleDate.jsDate));

            // const datecircukk = moment(this.model_date.singleDate.jsDate).format('MM/DD/YYYY');
            // const datepro = moment(this.model_date2.singleDate.jsDate).format('MM/DD/YYYY');
            // console.log(datecircukk);

            // if (datecircukk === 'Invalid date') {
            //     this.product.promulgateModel.circularDate = null;
            // } else {
            this.product.promulgateModel.circularDate = this.model_date1;
            // }
            console.log('date>>>>', this.model_date1);
            // date circulardate
            // if (datepro === 'Invalid date') {
            //     this.product.promulgateModel.promulgateDate = null;
            // } else {
            this.product.promulgateModel.promulgateDate = this.model_date2;
            // }

            // console.log('list', this.listConsider);
            const data = {
                'requestId': localStorage.getItem('requestId'),
                'send': true,
                'deletebusinessSign': this.dataid,
                'businessSign': this.listConsider,
                'promulgateModel': {
                    promulgateDate: null,
                    circularNo: this.product.promulgateModel.circularNo,
                    circularDate: null,
                    dept: this.product.promulgateModel.dept,
                    sctr: this.product.promulgateModel.sctr,
                    grp: this.product.promulgateModel.grp,
                    manager: this.product.promulgateModel.manager,
                    managerTel: this.product.promulgateModel.managerTel,
                    coManager: this.product.promulgateModel.coManager,
                    coManagerTel: this.product.promulgateModel.coManagerTel
                },
                'fileUpload': {
                    'file': this.showfile.file,
                    'fileDelete': this.files.fileDelete,
                },
            };
            // console.log(data['promulgateModel'].circularDate);
            // console.log('>>>>>>>test',this.signdatetext);
            // Date circularDate

            if (this.product.promulgateModel.circularDate) {
                // tslint:disable-next-line: max-line-length
                // data.promulgateModel.circularDate = moment(this.product.promulgateModel.circularDate.singleDate.jsDate).format().substring(0, 10);
                data.promulgateModel.circularDate = this.product.promulgateModel.circularDate;
            } else { data.promulgateModel.circularDate = null; }

            // Date promulgateDate
            if (this.product.promulgateModel.promulgateDate) {
                // tslint:disable-next-line:max-line-length
                // data.promulgateModel.promulgateDate = moment(this.product.promulgateModel.promulgateDate.singleDate.jsDate).format().substring(0, 10);
                data.promulgateModel.promulgateDate = this.product.promulgateModel.promulgateDate;
            } else { data.promulgateModel.promulgateDate = null; }
            // API POST

            console.log('data to save', data);
            this.status_loading = true;
            this.productsersice.postpromulgate(data).subscribe(res => {
                console.log('sen save', res);
                if (res['status'] === 'success') {
                    this.status_loading = false;
                    this.saveDraftstatus_State = false;
                    this.confirmState = 'success';
                    // setTimeout(() => {
                    $('#confirmModal').modal('hide');
                    this.confirmState = 'confirm';
                    this.listConsider = [];
                    this.getProductannouncementPC();
                    this.dataid = [];
                    this.page = 'true';
                    // }, 3000);
                    Swal.fire({
                        title: 'success',
                        text: 'ประกาศใช้ผลิตภัณฑ์เรียบร้อย',
                        icon: 'success',
                        showConfirmButton: false,
                        timer: 3000,
                    });
                } else {
                    Swal.fire({
                        title: 'error',
                        text: res['message'],
                        icon: 'error',
                    });
                    this.status_loading = false;
                    this.listConsider = [];
                    this.getProductannouncementPC();
                }
            }, err => {
                this.status_loading = false;
                console.log(err);
            });
        } else {
            alert('กรุณากรอกข้อมูลให้ครบ');
        }
    }

    // add row signoff
    addData() {
        const templateaddrow = {
            id: null,
            business: '',
            signDate: null,
            comment: '',
        };
        this.product.businessSignModel.push(templateaddrow);
        console.log('this.product.businessSignModel',this.product.businessSignModel);
        this.changeSaveDraft();
    }
    removedata(indexdata, dataid: number) {
        this.dataid.push(dataid);
        console.log(this.dataid);
        console.log(indexdata);
        console.log(this.signdatetext);
        if (this.product.businessSignModel.length !== 1) {
            this.product.businessSignModel.splice(indexdata, 1);
            this.departmenttext.splice(indexdata, 1);
            this.commenttext.splice(indexdata, 1);
            this.signdatetext.splice(indexdata, 1);
        } else {
            this.departmenttext = [];
            // this.Date.datepicker_input.dateData = null;
            this.commenttext = [];
            this.signdatetext = [];
            this.Date.datepicker_input['dateData'] = null;
            this.Date.endDate = null;
            // console.log(this.Date.datepicker_input);
            // this.product.businessSignModel.push({
            //     id: null,
            //     business: null,
            //     signDate: null,
            //     comment: null,
            // })
        }
        this.changeSaveDraft();
    }

    // file
    onSelectFile(event) {
        this.FileOverSize = [];
        console.log('dataSource_file', this.dataSource_file);
        if (event.target.files && event.target.files[0]) {
            const filesAmount = event.target.files.length;
            const file = event.target.files;

            for (let index = 0; index < file.length; index++) {
                // const element = file[index];
                this.fileToUpload.push(file[index]);
                console.log(this.fileToUpload);
            }
            // this.fileToUpload = file;
            this.sendfile = 'true';

            for (let i = 0; i < filesAmount; i++) {
                if (
                    // file[i].type === 'image/svg+xml' ||
                    file[i].type === 'image/jpeg' ||
                    // file[i].type === 'image/raw' ||
                    // file[i].type === 'image/svg' ||
                    // file[i].type === 'image/tif' ||
                    // file[i].type === 'image/gif' ||
                    file[i].type === 'image/jpg' ||
                    file[i].type === 'image/png' ||
                    file[i].type === 'application/doc' ||
                    file[i].type === 'application/pdf' ||
                    file[i].type ===
                    'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
                    file[i].type ===
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
                    file[i].type ===
                    'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
                    file[i].type === 'application/pptx' ||
                    file[i].type === 'application/docx' ||
                    (file[i].type === 'application/ppt' && file[i].size)
                ) {
                    if (file[i].size <= 5000000) {
                        // tslint:disable-next-line: prefer-const
                        let select = {
                            id: '',
                            file: file[i],
                        };
                        // console.log('file:', select);
                        this.multiple_file(select, i);
                    } else {
                        this.FileOverSize.push(file[i].name);
                    }
                } else {

                    alert('File Not Support');
                }
            }
            if (this.FileOverSize.length !== 0) {
                console.log('open modal');
                document.getElementById('testttt').click();
            }
        }
    }

    multiple_file(file, index) {
        const reader = new FileReader();
        if (file.file) {
            reader.readAsDataURL(file.file);
            reader.onload = (_event) => {
                const rawReadFile = reader.result as string;
                const splitFile = rawReadFile.split(',');
                // console.log('>>>>>', splitFile);
                const templateFile = {
                    id: null,
                    fileName: file.file.name,
                    fileSize: file.file.size,
                    base64File: splitFile[1],
                };
                this.showfile.file.push(templateFile);
                this.dataSource_file = new MatTableDataSource(this.showfile.file);
                this.hideDownloadFile();

                console.log('this.showfile', this.showfile);
                this.changeSaveDraft();
            };
        }
    }

    delete_mutifile(data: any, index: any) {
        console.log('data:', data, 'index::', index);
        this.showfile.file.splice(index, 1);
        console.log('urls:', this.files);
        this.fileToUpload.splice(index, 1);

        // this.urls.splice(index, 1);
        // this.muti_file.splice(index, 1);
        if (data.id !== '') {
            this.files.fileDelete.push(data.id);
        }
        this.dataSource_file = new MatTableDataSource(this.showfile.file);
        this.files.file = this.showfile.file;
        console.log('form dee:', this.files);
        console.log('id delete', this.files.fileDelete);
    }

    downloadFile(pathdata: any) {
        console.log(pathdata)
        const contentType = '';
        const sendpath = pathdata;
        console.log('path', sendpath);
        this.productsersice.getfile(sendpath).subscribe(res => {
            if (res['status'] === 'success' && res['data']) {
                const datagetfile = res['data'];
                const b64Data = datagetfile['data'];
                const filename = datagetfile['fileName'];
                console.log('base64', b64Data);
                const config = this.b64toBlob(b64Data, contentType);
                saveAs(config, filename);
            }

        });
    }

    b64toBlob(b64Data, contentType = '', sliceSize = 512) {
        const convertbyte = atob(b64Data);
        const byteCharacters = atob(convertbyte);
        const byteArrays = [];
        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            const slice = byteCharacters.slice(offset, offset + sliceSize);

            const byteNumbers = new Array(slice.length);
            for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            const byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }

        const blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }

    downloadFile64(data: any) {
        this.status_loading = true;
        const contentType = '';
        this.status_loading = false;
        const b64Data = data.base64File;
        const filename = data.fileName;
        console.log('base64', b64Data);

        const config = this.base64(b64Data, contentType);
        saveAs(config, filename);
    }

    base64(b64Data, contentType = '', sliceSize = 512) {
        const byteCharacters = atob(b64Data);
        const byteArrays = [];
        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            const slice = byteCharacters.slice(offset, offset + sliceSize);

            const byteNumbers = new Array(slice.length);
            for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            const byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }

        const blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }

    hideDownloadFile() {
        if (this.dataSource_file.data.length > 0) {
            this.downloadfile_by = [];

            for (let index = 0; index < this.dataSource_file.data.length; index++) {
                const element = this.dataSource_file.data[index];
                console.log('ele', element);

                if ('path' in element) {
                    this.downloadfile_by.push('path');
                } else {
                    this.downloadfile_by.push('base64');
                }
            }
        }
        console.log('by:', this.downloadfile_by);
    }

    backToDashboardPage() {
        this.router.navigate(['/dashboard1']);
    }

    autoGrowTextZone(e) {
        e.target.style.height = '0px';
        e.target.style.overflow = 'hidden';
        e.target.style.height = (e.target.scrollHeight + 0) + 'px';
    }

    changeSaveDraft() {
        this.saveDraftstatus_State = true;
        this.productsersice.status_state = true;
        console.log('saveDraftstatus:', this.saveDraftstatus_State);
    }

    canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
        console.log('canDeactivate');
        // return confirm();
        const subject = new Subject<boolean>();
        // const status = this.sidebarService.outPageStatus();
        // console.log('000 sidebarService status:', status);
        if (this.saveDraftstatus_State === true) {
            // console.log('000 sidebarService status:', status);
            const dialogRef = this.dialog.open(DialogSaveStatusComponent, {
                disableClose: true,
                data: {
                    headerDetail: 'ยืนยันการดำเนินการ',
                    bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
                    bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
                    returnStatus: false,
                    icon: 'assets/img/Alarm-blue.svg',
                }
            });
            dialogRef.afterClosed().subscribe((data) => {
                console.log('data::', data);
                if (data.returnStatus === true) {
                    subject.next(true);
                    this.savepromulgate();
                    this.saveDraftstatus_State = false;
                } else if (data.returnStatus === false) {
                    console.log('000000 data::', data.returnStatus);
                    // this.sidebarService.inPageStatus(false);
                    subject.next(true);
                    return true;
                } else {
                    subject.next(false);
                }
            });
            console.log('=====', subject);
            return subject;
        } else {
            return true;
        }
    }
}
