import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductAnnouncementComponent } from './product-announcement.component';

describe('ProductAnnouncementComponent', () => {
  let component: ProductAnnouncementComponent;
  let fixture: ComponentFixture<ProductAnnouncementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductAnnouncementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductAnnouncementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
