import { MatDialogModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductAnnouncementRoutingModule } from './product-announcement-routing.module';
import { ProductAnnouncementComponent } from './product-announcement.component';
import { FormsModule } from '@angular/forms';
import { AngularMyDatePickerModule } from 'angular-mydatepicker';
import { MatTableModule } from '@angular/material';

import { DateModule } from '../datepicker/date/date.module';
import { TextareaAutosizeModule } from 'ngx-textarea-autosize';
import { TextareaAutoresizeDirectiveModule } from '../../textarea-autoresize.directive/textarea-autoresize.directive.module';
import { DialogSaveStatusComponent } from './dialog-save-status/dialog-save-status.component';
import { CanDeactivateGuard } from './../../services/configGuard/config-guard.service';
;
@NgModule({
  declarations: [
    ProductAnnouncementComponent,
    DialogSaveStatusComponent,
  ],
  imports: [
    CommonModule,
    ProductAnnouncementRoutingModule,
    FormsModule,
    AngularMyDatePickerModule,
    MatTableModule,
    DateModule,
    TextareaAutosizeModule,
    TextareaAutoresizeDirectiveModule,
    MatDialogModule
  ],
  providers: [CanDeactivateGuard],
  entryComponents: [DialogSaveStatusComponent]
})
export class ProductAnnouncementModule { }
