import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProductProcessSystemComponent} from './product-process-system/product-process-system.component';
import {CommonModule} from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: ProductProcessSystemComponent,
  }
];

@NgModule({
  declarations: [ProductProcessSystemComponent],
    imports: [RouterModule.forChild(routes), CommonModule],
  exports: [RouterModule]
})
export class ProductProcessSystemRoutingModule { }
