import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductProcessSystemComponent } from './product-process-system.component';

describe('ProductProcessSystemComponent', () => {
  let component: ProductProcessSystemComponent;
  let fixture: ComponentFixture<ProductProcessSystemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductProcessSystemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductProcessSystemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
