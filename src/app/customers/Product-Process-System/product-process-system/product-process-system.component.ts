import { Component, OnInit } from '@angular/core';
import { RequestService } from '../../../services/request/request.service';
import { saveAs } from 'file-saver';
import { ProductProcessSystemService } from '../../../services/product-process-system/product-process-system.service';
import { split } from 'ts-node';
import { MatDialog } from '@angular/material';
import { NewsEditComponent } from '../../news-announcement/news-edit/news-edit.component';
import { ProductProcessSystemDialogComponent } from './product-process-system-dialog/product-process-system-dialog.component';

@Component({
    selector: 'app-product-process-system',
    templateUrl: './product-process-system.component.html',
    styleUrls: ['./product-process-system.component.scss']
})
export class ProductProcessSystemComponent implements OnInit {
    product_data = [];
    file_data = [];
    filename = [];
    types = [];
    types2 = [];

    constructor(private sidebarService: RequestService, private _service: ProductProcessSystemService,
        private dialog: MatDialog) {
    }

    showDialog(i) {
        const dialogRef = this.dialog.open(ProductProcessSystemDialogComponent, { width: '1300px', disableClose: true, autoFocus: false });
        console.log(this.product_data[i].id);
        // localStorage.removeItem('confirmSavePermissionData');
        sessionStorage.setItem('showId', JSON.stringify(this.product_data[i].id));
        dialogRef.afterClosed().subscribe(result => {
            // sessionStorage.clear();
            sessionStorage.removeItem('showId');
        });
    }

    ngOnInit() {
        this._service.Getproduct_process_system().subscribe(data => {
            // console.log('ข่าว>>>>', data['data'])
            if (data['message'] === 'success') {
                this.product_data = data['data'];
                // console.log(this.product_data)
                for (let i = 0; i < this.product_data.length; i++) {
                    const row = [];
                    // let merge;
                    for (let o = 0; o < this.product_data[i]['attachments']['file'].length; o++) {
                        // console.log(this.product_data[i]['attachments']['file'].length);
                        const result = this.product_data[i]['attachments']['file'][o];
                        row.push(result);
                        // if (o > 0) {
                        //     merge += '  ,  ' + row[o];
                        // } else {
                        //     merge = row[o];
                        // }
                        // console.log(merge);
                    }

                    this.file_data.push(row);
                }
                const types = this.file_data;
                for (let a = 0; a < types.length; a++) {
                    this.types = [];
                    for (let b = 0; b < types[a].length; b++) {
                        const row = types[a][b]['fileName'];
                        const res = row.toLowerCase().split('.');
                        this.types.push(res[res.length - 1]);
                    }
                    this.types2.push(this.types);
                }
                console.log(this.types2);
                // console.log(this.file_data);
                // console.log(this.filename);
            }
        });
    }


    downloadFile(pathdata: any) {
        const contentType = '';
        const sendpath = pathdata;
        console.log('path', sendpath);
        this.sidebarService.getfile(sendpath).subscribe(res => {
            if (res['status'] === 'success' && res['data']) {
                const datagetfile = res['data'];
                const b64Data = datagetfile['data'];
                const filename = datagetfile['fileName'];
                console.log('base64', b64Data);
                const config = this.b64toBlob(b64Data, contentType);
                saveAs(config, filename);
            }

        });
    }

    b64toBlob(b64Data, contentType = '', sliceSize = 512) {
        const convertbyte = atob(b64Data);
        const byteCharacters = atob(convertbyte);
        const byteArrays = [];
        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            const slice = byteCharacters.slice(offset, offset + sliceSize);

            const byteNumbers = new Array(slice.length);
            for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            const byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }

        const blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }
}
