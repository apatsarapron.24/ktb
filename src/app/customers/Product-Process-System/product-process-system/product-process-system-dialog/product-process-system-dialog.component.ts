import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {ProductProcessSystemComponent} from '../product-process-system.component';
import {ProductProcessSystemService} from '../../../../services/product-process-system/product-process-system.service';

@Component({
    selector: 'app-product-process-system-dialog',
    templateUrl: './product-process-system-dialog.component.html',
    styleUrls: ['./product-process-system-dialog.component.scss']
})
export class ProductProcessSystemDialogComponent implements OnInit {
    datadetail = [];

    constructor(private dialogRef: MatDialogRef<ProductProcessSystemComponent>, private _service: ProductProcessSystemService) {
    }

    showModel = false;
    modalmainImageIndex: number;
    modalmainImagedetailIndex: number;
    indexSelect: number;
    modalImage = [];
    modalImagedetail = [];
    fileValue: number;
    image1 = [];
    message = null;
    file1_1: any;
    file1_2: any;
    result: any;
    data_edit = {
        id: null,
        topic: '',
        informationDate: '',
        status: false,
        informationStartDate: '',
        informationEndDate: '',
        detail: '',
        picture: {
            file: [],
            fileDelete: []
        },
        attachments: {
            file: [],
            fileDelete: []
        },
    };

    ngOnInit() {
        // console.log('id', sessionStorage.getItem('showId'));
        const Id = sessionStorage.getItem('showId');
        if (Id) {
            this._service.Getdetailnews(+Id).subscribe(data => {
                if (data['status'] === 'success') {
                    console.log(data['data']);
                    this.datadetail = data['data'];
                    for (let i = 0; i < this.datadetail['picture']['file'].length; i++) {
                        // this.image1[i] =  this.datadetail['picture']['file'][i]['fileName'];
                        this.preview(1, this.datadetail['picture']['file'][i], i);
                        this.data_edit['picture']['file'].push(this.datadetail['picture']['file'][i]);
                    }
                }
            });
        }
    }

    preview(fileIndex, file, index) {

        const name = file.fileName;
        const typeFiles = name.match(/\.\w+$/g);
        const typeFile = typeFiles[0].replace('.', '');

        const tempPicture = `data:image/${typeFile};base64,${file.base64File}`;

        if (fileIndex === 1) {
            if (file) {
                this.image1[index] = tempPicture;
            }
        }
    }

    image_click(colId, id) {
        this.showModel = true;
        document.getElementById('myModal').style.display = 'block';
        this.imageModal(colId, id);
    }

    imageModal(fileIndex, index) {
        if (fileIndex === 1) {
            this.modalmainImageIndex = index;
            this.modalImage = this.image1;
            this.modalImagedetail = this.data_edit.picture.file;
            this.indexSelect = index;
            this.fileValue = fileIndex;
        }
    }

    closeModal() {
        this.showModel = false;
        document.getElementById('myModal').style.display = 'none';
    }

    close() {
        sessionStorage.removeItem('showId');
        this.dialogRef.close();
    }
}
