import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductProcessSystemDialogComponent } from './product-process-system-dialog.component';

describe('ProductProcessSystemDialogComponent', () => {
  let component: ProductProcessSystemDialogComponent;
  let fixture: ComponentFixture<ProductProcessSystemDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductProcessSystemDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductProcessSystemDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
