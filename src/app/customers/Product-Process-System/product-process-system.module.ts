import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductProcessSystemRoutingModule } from './product-process-system-routing.module';
import { ProductProcessSystemDialogComponent } from './product-process-system/product-process-system-dialog/product-process-system-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [ProductProcessSystemDialogComponent],
  imports: [
    CommonModule,
    ProductProcessSystemRoutingModule,
    MatDialogModule,
    FormsModule,
  ],
  entryComponents: [ProductProcessSystemDialogComponent]
})
export class ProductProcessSystemModule { }
