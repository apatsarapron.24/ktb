import { dateFormat } from 'highcharts';
import {
  Component,
  OnInit,
  ViewChild,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';
import * as moment from 'moment';
import {
  AngularMyDatePickerDirective,
  IAngularMyDpOptions,
  IMyDateModel,
} from 'angular-mydatepicker';
import { Router } from '@angular/router';
// tslint:disable-next-line: max-line-length
import { DateAdapter } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { Location, DatePipe } from '@angular/common';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { dateToBuddha, dateToBuddhaSend } from './utility/utils';

@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.scss'],
})
export class DateComponent implements OnInit {
  toDay = new Date();
  endDate: any;
  minEndDate = null;

  @Input() datepicker_input = {
    role: null,
    dateData: null,
    index: null,
    page: null,
  };

  @Output() DateChange = new EventEmitter<any>();

  placeholder_input = 'วว/ดด/ปปปป';
  @Input() announced = null;

  constructor(
    private adapter: DateAdapter<any>,
    private http: HttpClient,
    private datePipe: DatePipe,
    private _location: Location
  ) {
    this.adapter.setLocale('th-TH');
  }

  ngOnInit() {
    // console.log('DateInput:', this.datepicker_input);
    this.endDate = this.datepicker_input.dateData;
    // console.log('endDate:', this.endDate);
    if (this.endDate !== null) {
      this.endDate = new Date(this.endDate);
    } else {
      this.endDate = null;
    }
  
  }

  dateChangeEnd(event: MatDatepickerInputEvent<Date>) {
    // console.log('date end : ', event);
    if (event !== null) {
      const value = moment(event.value).format('DD/MM/YYYY');

      this.endDate = event.value;
      this.DateChange.emit(this.endDate);
      // this.datepicker_input.dateData = this.endDate;
      return this.endDate;
    } else {
      return null;
    }

  }

  changeDate(date: any) {
    if (date !== null) {
      // console.log(date);
      const d = new Date(date);
      let month = '' + (d.getMonth() + 1);
      let day = '' + d.getDate();
      const year = d.getFullYear();
      if (month.length < 2) {
        month = '0' + month;
      }
      if (day.length < 2) {
        day = '0' + day;
      }
      const Send = [year, month, day].join('-');
      const SendI = new Date(Send).toISOString();
      // console.log('Send:', SendI);
      return SendI;
    } else {
      return null;
    }

  }
  covertDate() {
    // console.log(this.datepicker_input.dateData);
    // tslint:disable-next-line: no-non-null-assertion
    if (this.datepicker_input.dateData !== null) {
      const year = Number(
        this.datepicker_input.dateData[0] +
        this.datepicker_input.dateData[1] +
        this.datepicker_input.dateData[2] +
        this.datepicker_input.dateData[3]
      ) + 543;

      const Date = this.datepicker_input.dateData[8] +
        this.datepicker_input.dateData[9] + '/' + this.datepicker_input.dateData[5] +
        this.datepicker_input.dateData[6] + '/' + year;
      return Date;
    } else {
      return null;
    }
  }

}
