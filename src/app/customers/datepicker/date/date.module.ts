import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DateRoutingModule } from './date-routing.module';

import { DatePipe } from '@angular/common';
import {
  MatMenuModule,
  MatIconModule,
  MatDatepickerModule,
  MatNativeDateModule,
} from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { DateRangePickerModule } from '@syncfusion/ej2-angular-calendars';
import { DateComponent } from './date.component';

@NgModule({
  declarations: [ DateComponent ],
  imports: [
    CommonModule,
    DateRoutingModule,
    MatNativeDateModule,
    MatDatepickerModule
  ],
  providers: [DatePipe, MatDatepickerModule, MatNativeDateModule],
  exports: [ DateComponent ]
})
export class DateModule { }
