import * as moment from 'moment';

// js date to DD/MM/YYYY (Buddha)
const dateToBuddha = (dateNormal): string => {
    const now = moment(dateNormal);
    now.locale('th');
    const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();

    return now.format('DD/MM') + '/' + buddhishYear;
  };

  // '2563-12-30T17:00:00.000+0000' to DD/MM/YYYY
const buddhaConvert = (dateBuddha): string => {
  return dateBuddha.split('T')[0].split('-').reverse().join('/');
};

  // '2563-12-30T17:00:00.000+0000' to DD/MM/YYYY
const buddhaConvertCalendar = (dateBuddha): string => {
    // 30-12-2563
    const dateReverse = dateBuddha.split('T')[0].split('-').reverse();
    const yearChange = +dateReverse.split('-')[2] - 543;
    return dateReverse[0] + '-' + dateReverse[1] + '-' + yearChange;
  };

// js date to buddha 2563-01-09 (YYYY-MM-DD)
const dateToBuddhaSend =  (dateNormal): string => {
  const now = moment(dateNormal);
  now.locale('th');
  const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();

  return buddhishYear + '-' + now.format('MM-DD') ;
};

// 2563-01-09 to 2020-01-09
const buddhaToChrist = (dateBuddha): string => {
  const splitDate = dateBuddha.split('-');
  return (+splitDate[0] - 543) + '-' + splitDate[1] + '-' + splitDate[2];
};

// 9/08/2563 to 2020-08-9
const buddhaSlashToChrist = (dateBuddha): string => {
  const splitDate = dateBuddha.split('/');
  return (+splitDate[2] - 543) + '-' + splitDate[1] + '-' + splitDate[0];
};

// 09/08/2563 to 2563-08-09
const buddhaSlashToHyphen = (dateBuddha): string => {
  const splitDate = dateBuddha.split('/').reverse().join('-');
  return splitDate;
};


export { dateToBuddha , buddhaConvert , dateToBuddhaSend , buddhaConvertCalendar, buddhaToChrist, buddhaSlashToChrist, buddhaSlashToHyphen};
