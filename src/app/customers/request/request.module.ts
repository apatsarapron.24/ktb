import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RequestRoutingModule } from './request-routing.module';
import { RequestComponent } from './request.component';

import { informationBaseFirstStepComponent } from './information-base/first-step/first-step.component';
import { informationBaseStepTwoComponent } from './information-base/step-two/step-two.component';
import { informationBaseStepThreeComponent } from './information-base/step-three/step-three.component';
import { informationBaseStepFourComponent } from './information-base/step-four/step-four.component';

import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatRadioModule } from '@angular/material/radio';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { AngularMyDatePickerModule } from 'angular-mydatepicker';
import { BrowserModule } from '@angular/platform-browser';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { DragDropModule } from '@angular/cdk/drag-drop';

import { StepOneComponent } from './product-detail/step-one/step-one.component';
import { StepTwoComponent } from './product-detail/step-two/step-two.component';
import { StepThreeComponent } from './product-detail/step-three/step-three.component';
import { StepFourComponent } from './product-detail/step-four/step-four.component';
import { StepFiveComponent } from './product-detail/step-five/step-five.component';
import { StepSixComponent } from './product-detail/step-six/step-six.component';
import { StepEightComponent } from './product-detail/step-eight/step-eight.component';
import { StepNineComponent } from './product-detail/step-nine/step-nine.component';
import { StepTenComponent } from './product-detail/step-ten/step-ten.component';
import { StepElevenComponent } from './product-detail/step-eleven/step-eleven.component';
import { StepTwelveComponent } from './product-detail/step-twelve/step-twelve.component';
import { ClickOutsideModule } from 'ng-click-outside';
import { StepSevenComponent } from './product-detail/step-seven/step-seven.component';
import { PersonalLoanComponent } from './product-detail/step-three/personal-loan/personal-loan.component';
import { OtherComponent } from './product-detail/step-three/other/other.component';
import { HousingLoanComponent } from './product-detail/step-three/housing-loan/housing-loan.component';
import { NonCreditComponent } from './product-detail/step-three/non-credit/non-credit.component';
import { BusinessLoanComponent } from './product-detail/step-three/business-loan/business-loan.component';
import { OtherNonCreaditComponent } from './product-detail/step-three/other-non-creadit/other-non-creadit.component';

import { TextareaAutoresizeDirectiveModule } from '../../textarea-autoresize.directive/textarea-autoresize.directive.module'
import { DatePipe } from '@angular/common';
import {
  MatMenuModule,
  MatIconModule,
  MatDatepickerModule,
  MatNativeDateModule,
} from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { DateRangePickerModule } from '@syncfusion/ej2-angular-calendars';

import { DateModule } from '../datepicker/date/date.module';
import { ConfirmRequestSummaryModelsComponent } from './information-base/first-step/confirm-request-summary-models/confirm-request-summary-models.component';
import { TextareaAutosizeModule } from 'ngx-textarea-autosize';
import { DialogSaveStatusComponent } from './dialog/dialog-save-status/dialog-save-status.component';
import { CanDeactivateGuard } from '../../services/configGuard/config-guard.service';
@NgModule({
  declarations: [
    RequestComponent,
    informationBaseFirstStepComponent,
    informationBaseStepTwoComponent,
    informationBaseStepThreeComponent,
    informationBaseStepFourComponent,
    StepOneComponent,
    StepTwoComponent,
    StepThreeComponent,
    StepFourComponent,
    StepFiveComponent,
    StepSixComponent,
    StepEightComponent,
    StepNineComponent,
    StepTenComponent,
    StepElevenComponent,
    StepTwelveComponent,
    StepSevenComponent,
    PersonalLoanComponent,
    OtherComponent,
    HousingLoanComponent,
    NonCreditComponent,
    OtherComponent,
    BusinessLoanComponent,
    OtherNonCreaditComponent,
    ConfirmRequestSummaryModelsComponent,
    DialogSaveStatusComponent,
  ],
  imports: [
    CommonModule,
    RequestRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatRadioModule,
    MatCheckboxModule,
    AngularMyDatePickerModule,
    MatTableModule,
    MatInputModule,
    DragDropModule,
    ClickOutsideModule,
    BsDatepickerModule.forRoot(),
    DateRangePickerModule,
    AngularMyDatePickerModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatNativeDateModule,
    MatDatepickerModule,
    DateModule,
    TextareaAutosizeModule,
    TextareaAutoresizeDirectiveModule
  ],
  exports: [
    informationBaseFirstStepComponent,
    informationBaseStepTwoComponent,
    informationBaseStepThreeComponent,
    informationBaseStepFourComponent,
    StepOneComponent,
    StepTwoComponent,
    StepThreeComponent,
    StepFourComponent,
    StepFiveComponent,
    StepSixComponent,
    StepEightComponent,
    StepNineComponent,
    StepTenComponent,
    StepElevenComponent,
    StepTwelveComponent,
    StepSevenComponent,
    PersonalLoanComponent,
    OtherComponent,
    HousingLoanComponent,
    NonCreditComponent,
    OtherComponent,
    BusinessLoanComponent,
    OtherNonCreaditComponent,
    DialogSaveStatusComponent
  ],
  providers: [DatePipe, MatDatepickerModule, MatNativeDateModule, CanDeactivateGuard],

  entryComponents: [ConfirmRequestSummaryModelsComponent, DialogSaveStatusComponent]
})
export class RequestModule { }
