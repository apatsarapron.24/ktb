import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { informationBaseStepTwoComponent } from './step-two.component';

describe('informationBaseStepTwoComponent', () => {
  let component: informationBaseStepTwoComponent;
  let fixture: ComponentFixture<informationBaseStepTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ informationBaseStepTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(informationBaseStepTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
