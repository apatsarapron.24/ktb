import { Component, OnInit, Input } from '@angular/core';
import { CanComponentDeactivate } from './../../../../services/configGuard/config-guard.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { count } from 'rxjs/operators';
import { Router } from '@angular/router';
import { StepTwoService } from '../../../../services/request/information-base/step-two/step-two.service';
import { RequestService } from '../../../../services/request/request.service';
import { MatTableDataSource } from '@angular/material';
import { DialogSaveStatusComponent } from '../../dialog/dialog-save-status/dialog-save-status.component';

export interface PeriodicElement {
  quarter: string;
  name: string;
  code: string;
  yearPlan: string;
  planStatus: string;
  planStatusDetail: string;
}

@Component({
  selector: 'app-step-two-base',
  templateUrl: './step-two.component.html',
  styleUrls: ['./step-two.component.scss']
})
// tslint:disable-next-line:class-name
export class informationBaseStepTwoComponent implements OnInit, CanComponentDeactivate {

  displayedColumns: string[] = ['select', 'quarter', 'name', 'code', 'yearPlan', 'planStatus', 'planStatusDetail'];
  dataSource: MatTableDataSource<PeriodicElement>;

  showtable = false;
  res_save: any;
  res_get: any;
  status_loading = true;
  // policy: any;

  // stepTwoData: any = {};
  stepTwoData = {
    id: null,
    validate: null,
    policy: [
      {
        topic: 'นโยบายและแผนการประกอบธุรกิจสินเชื่อบุคคลภายใต้การกำกับ',
        flag: false
      },
      {
        topic: 'นโยบายการประกอบธุรกิจสินเชื่อไมโครไฟแนนซ์',
        flag: false
      },
      {
        topic: 'นโยบายการเป็นตัวแทนรับคำขอและเรียกเก็บเบี้ยประกันหรือค่าบริการการประกันการส่งออกและการค้ำประกันสินเชื่อแก่ลูกค้า',
        flag: false
      },
      {
        topic: 'นโยบายการประกอบธุรกิจแฟ็กเตอริง',
        flag: false
      },
      {
        topic: 'นโยบายและกลยุทธ์การลงทุนใน Collateralized Debt Obligation (CDO)',
        flag: false
      },
      {
        topic: 'นโยบายและกลยุทธ์การทำธุรกรรม Credit Derivatives',
        flag: false
      },
      {
        topic: 'นโยบายและกลยุทธ์การทำธุรกรรมอนุพันธ์ที่อ้างอิงตัวแปรด้านตลาด',
        flag: false
      },
      {
        topic: 'นโยบายและกลยุทธ์การทำธุรกรรมเงินกู้ยืมที่มีอนุพันธ์แฝง',
        flag: false
      },
      {
        topic: 'นโยบายการดูแลลูกค้าสำหรับการทำธุรกรรมอนุพันธ์',
        flag: false
      },
      {
        topic: 'นโยบายการประกอบธุรกิจการเป็นที่ปรึกษาทางการเงิน',
        flag: false
      },
      {
        topic: 'นโยบายและกรอบวิธีปฏิบัติ การทำธุรกิจที่ปรึกษาการลงทุนส่วนบุคคล',
        flag: false
      },
      {
        topic: 'นโยบายการประกอบธุรกิจการจัดจำหน่ายหลักทรัพย์อันเป็นตราสารแห่งหนี้และหน่วยลงทุน (Underwriting Policy)',
        flag: false
      },
      {
        topic: 'นโยบายการประกอบธุรกิจนายทะเบียนหลักทรัพย์และตัวแทนชำระเงิน',
        flag: false
      },
      {
        topic: 'นโยบายการประกอบธุรกิจผู้รับฝากทรัพย์สิน และผู้ดูแลประโยชน์กองทุนรวม',
        flag: false
      },
      {
        topic: 'นโยบายการประกอบธุรกิจผู้แทนผู้ถือหุ้นกู้',
        flag: false
      },
      {
        topic: 'นโยบายการเป็นนายหน้าประกันวินาศภัยและนายหน้าประกันภัย',
        flag: false
      },
      {
        topic: 'แผนงานและวิธีปฏิบัติรองรับการประกอบธุรกิจหลักทรัพย์ประเภทการเป็นนายหน้าซื้อขายหลักทรัพย์ที่เป็นหน่วยลงทุน',
        flag: false
      },
      {
        topic: 'แผนงานและวิธีปฏิบัติรองรับการประกอบธุรกิจการค้าหลักทรัพย์อันเป็นตราสารแห่งหนี้',
        flag: false
      },
      {
        topic: 'นโยบายการให้บริการด้านผลิตภัณฑ์บัตรและแผนรองรับการประกอบธุรกิจ',
        flag: false
      },
      {
        topic: 'นโยบายด้านช่องทางการให้บริการและแผนกลยุทธ์ช่องทางให้บริการ',
        flag: false
      },
      {
        topic: 'นโยบายการแต่งตั้งตัวแทนจ่ายเงิน',
        flag: false
      },
      {
        topic: 'นโยบายการแต่งตั้งตัวแทนธนาคาร',
        flag: false
      },
      {
        topic: 'นโยบายการบริหารจัดการด้านการให้บริการแก่ลูกค้าอย่างเป็นธรรม (Market Conduct)',
        flag: false
      }
    ]
    ,
    requestyearPlan:
    {
      id: null,
      planStatusDetail: null
    },
    strategic: [
      {
        topic: 'Bank\'s Net Profit',
        flag: false
      },
      {
        topic: 'ROE',
        flag: false
      },
      {
        topic: 'Loan Growth',
        flag: false
      },
      {
        topic: 'NPLs',
        flag: false
      },
      {
        topic: 'Coverage Ratio',
        flag: false
      },
      {
        topic: 'L/D Ratio',
        flag: false
      },
      {
        topic: 'Capital Adequacy Ratio',
        flag: false
      },
      {
        topic: 'Operational Loss',
        flag: false
      },
      {
        topic: 'Fraud',
        flag: false
      },
      {
        topic: 'Liquidity Coverage Ratio',
        flag: false
      },
      {
        topic: 'Value At Risk',
        flag: false
      },
      {
        topic: 'BU specific compliance issues',
        flag: false
      },
      {
        topic: 'Compliance',
        flag: false
      },
      {
        topic: 'อื่นๆ',
        flag: false
      }
    ],
    status: false,
    strategic_detail: null,
    year_plan: null,
    year_plan_detail: null,
    is_update: null,
  };

  requestId = null;
  res_id: any;
  @Input() template_manage = { summary: null };

  saveDraftstatus = null;
  outOfPageSave = false;
  linkTopage = null;
  pageChange = null;
  openDoCheckc = false;

  selplanyear = null;
  requestid: any;
  ckplandetail: any;
  pageStatus = false;
  checkDataSuccess = false;
  pageAction = 'save';
  alerts = false;

  constructor(private router: Router,
    public dialog: MatDialog,
    private saveData: StepTwoService,
    private sidebarService: RequestService) {
    this.dataSource = new MatTableDataSource();
  }

  checkId() {
    this.requestId = localStorage.getItem('requestId');
    const body = { id: this.requestId };
    if (localStorage.getItem('requestId') !== null
      && localStorage.getItem('requestId') !== ''
      && Number(localStorage.getItem('requestId')) !== 0) {
      this.saveData.getPage2(body).subscribe(res => {
        console.log('res : ', res);
        if (res['status'] === 'success') {
          this.status_loading = false;
          if (res['data'].requestyearPlan === null) {
            res['data'].requestyearPlan = {
              id: null,
              planStatusDetail: null
            }
          }
        }
        this.res_get = res;
        if (this.res_get.data !== null) {
          console.log(this.res_get.data.policy, this.res_get.data.strategic);
          if (this.res_get.data.policy.length === 0) {
            this.res_get.data.policy = this.stepTwoData.policy;
          }
          if (this.res_get.data.strategic.length === 0) {
            this.res_get.data.strategic = this.stepTwoData.strategic;
          }
          this.stepTwoData = this.res_get.data;
        }
      });
    }
  }
  viewNext() {
    this.router.navigate(['request/information-base/step3']);
  }
  ngOnInit() {
    window.scrollTo(0, 0);
    console.log('template_manage', this.template_manage);
    this.openDoCheckc = true;
    // tslint:disable-next-line: radix
    this.requestid = parseInt(localStorage.getItem('requestId'));
    this.checkId();
    this.sidebarService.checkDefault(localStorage.getItem('requestId')).subscribe(page => {
      this.pageStatus = page['data']
    })
    if (localStorage.getItem('requestId')) {
      this.sidebarService.sendEvent();
    } else {
      this.status_loading = false;
    }
  }

  check_require() {
    // const check_policy = Object.values(this.stepTwoData.policy).some(value => value.flag === true);
    // const check_strategic = Object.values(this.stepTwoData.strategic).some(value => value.flag === true);

    // console.log('check_policy : ', check_policy);
    // console.log('check_strategic : ', check_strategic);

    // set status in stepTwoData
    let policyStatus = false;
    let strategicStatus = false;
    this.stepTwoData.policy.forEach(ele => {
      if (ele.flag === true) {
        policyStatus = true;
      }
    });
    this.stepTwoData.strategic.forEach(ele => {
      if (ele.flag === true) {
        strategicStatus = true;
      }
    });
    console.log('strategicStatus : ', policyStatus);

    if (strategicStatus
      && this.stepTwoData.year_plan != null
      && this.stepTwoData.is_update != null) {
      this.stepTwoData.status = true;

      if (this.stepTwoData.year_plan === 'มี'
        && this.stepTwoData.requestyearPlan.id === null) {
        this.stepTwoData.status = false;
      }
      console.log('status : ', this.stepTwoData.status);
    } else {
      this.stepTwoData.status = false;
      console.log('status : ', this.stepTwoData.status);
    }
    return  this.stepTwoData.status;


  }


  clearsectionstrategicPlan() {
    this.stepTwoData.strategic_detail = null;
    this.changeSaveDraft();
  }

  clearannualPlanDetail(number: any) {
    if (number === 'มี') {
      this.stepTwoData.year_plan = 'มี';
      this.stepTwoData.year_plan_detail = null;
    } else if (number === 'ไม่มี') {
      this.stepTwoData.year_plan = 'ไม่มี';
      this.showtable = false;
    }
    // console.log('stepTwoData.year_plan', this.stepTwoData.year_plan);
    this.changeSaveDraft();
  }

  saveDraft(page?, action?) {
    this.check_require();
    if (localStorage.getItem('requestId')) {
      this.stepTwoData.id = localStorage.getItem('requestId');
      console.log('this.stepTwoData[strategic][13].flag', this.stepTwoData['strategic'][13].flag);
      console.log('this.stepTwoData.year_plan', this.stepTwoData.year_plan);
    }
    if (this.stepTwoData['strategic'][13].flag === true
      && (this.stepTwoData.strategic_detail === null || this.stepTwoData.strategic_detail === '')) {
      alert('กรุณากรอกเหตุผลประกอบ');
      return false;
    } else if (this.stepTwoData.year_plan === 'ไม่มี'
      && (this.stepTwoData.year_plan_detail === null || this.stepTwoData.year_plan_detail === '')) {
      alert('กรุณากรอกเหตุผลประกอบ');
      return false;
    } else {
      return this.save(page, action);
    }
  }

  save(page?, action?) {
    const returnUpdateData = new Subject<any>();
    console.log('===== stepTwoData', this.stepTwoData);
    // this.checkDataSuccess = this.stepTwoData.status;
    this.saveData.savePage2(this.stepTwoData).subscribe(res => {
      console.log('===== stepTwoData res', res);
      if (res['status'] === 'success') {
        this.res_save = res;
        this.stepTwoData = this.res_save.data;
        localStorage.setItem('requestId', this.res_save.data.id);
        this.sidebarService.sendEvent();
        this.saveDraftstatus = this.res_save.status;
        returnUpdateData.next(true);
        setTimeout(() => {
          this.saveDraftstatus = null;
        }, 3000);
        console.log('save', this.res_save.status);
        // if (page === 'summary') {
        //   if (action === 'save') {
        //     console.log('summary save');
        //   } else if (action === 'next') {
        //     this.router.navigate(['/request-summary/information-base/step3']);
        //     if (localStorage.getItem('requestId')) {
        //       this.sidebarService.sendEvent();
        //     }
        //   }
        // } else if (page === 'navbar') {
        //   this.router.navigate([this.linkTopage]);
        //   if (localStorage.getItem('requestId')) {
        //     this.sidebarService.sendEvent();
        //   }
        // } else if (page !== 'summary' && action === 'next') {
        //   this.router.navigate(['/request/information-base/step3']);
        //   if (localStorage.getItem('requestId')) {
        //     this.sidebarService.sendEvent();
        //   }
        // }
        this.checkId();
        this.sidebarService.inPageStatus(false);
      } else {
        alert(res['message']);
        returnUpdateData.next(true);
      }
    });
    return returnUpdateData;
  }

  next_step(page?, action?) {
    // console.log('page_page', page);
    // console.log('action_action', action);
    // if (this.sidebarService.outPageStatus() === true) {
    //   const dialogRef = this.dialog.open(DialogSaveStatusComponent, {
    //     disableClose: true,
    //     data: {
    //       headerDetail: 'ยืนยันการดำเนินการ',
    //       bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
    //       bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
    //       returnStatus: false,
    //       icon: 'assets/img/Alarm-blue.svg',
    //     }
    //   });
    //   dialogRef.afterClosed().subscribe((data) => {
    //     console.log('data::', data);
    //     if (data.returnStatus === true) {
    //       this.check_require();
    //       if (localStorage.getItem('requestId')) {
    //         this.stepTwoData.id = localStorage.getItem('requestId');
    //       }
    //       console.log('this.stepTwoData.policy : ', this.stepTwoData.policy);
    //       console.log('stepTwoData : ', this.stepTwoData);

    //       if (this.stepTwoData['strategic'][13].flag === true
    //         && (this.stepTwoData.strategic_detail === null || this.stepTwoData.strategic_detail === '')) {
    //           alert('กรุณากรอกเหตุผลประกอบ');
    //         } else if (this.stepTwoData.year_plan === 'ไม่มี'
    //         && (this.stepTwoData.year_plan_detail === null || this.stepTwoData.year_plan_detail === '')) {
    //           alert('กรุณากรอกเหตุผลประกอบ');
    //         } else {
    //           this.save(page, 'next');
    //         }
    //     } else if (data.returnStatus === false) {
    //       this.router.navigate(['/request/information-base/step3']);
    //       if (localStorage.getItem('requestId')) {
    //         this.sidebarService.sendEvent();
    //       }
    //     }
    //   });
    // } else {
    //   if (page === 'summary') {
    //     if (action === 'next') {
    //       this.router.navigate(['/request-summary/information-base/step3']);
    //       if (localStorage.getItem('requestId')) {
    //         this.sidebarService.sendEvent();
    //       }
    //     }
    //   } else {
    //     this.router.navigate(['/request/information-base/step3']);
    //     if (localStorage.getItem('requestId')) {
    //       this.sidebarService.sendEvent();
    //     }
    //   }
    // }
    this.pageAction = 'next'
    this.router.navigate(['/request/information-base/step3']);
  }


  autogrow(index: any) {
    if (index === 1) {
      const textother1 = document.getElementById('textother1');
      textother1.style.overflow = 'hidden';
      textother1.style.height = 'auto';
      textother1.style.height = textother1.scrollHeight + 'px';
    }

    if (index === 2) {
      const textother2 = document.getElementById('textother2');
      textother2.style.overflow = 'hidden';
      textother2.style.height = 'auto';
      textother2.style.height = textother2.scrollHeight + 'px';
    }

    this.changeSaveDraft();
  }

  openTablePlan() {
    if (this.stepTwoData.year_plan === 'มี') {
      this.showtable = true;
      this.getFilterSystemManagement();
    } else {
      this.showtable = false;
    }
  }

  getFilterSystemManagement() {
    this.status_loading = true;
    this.saveData.getFilterSystemManagement().subscribe(res => {
      if (res['data']) {
        this.dataSource.data = res['data'];
        this.status_loading = false;

        for (let index = 0; index < this.dataSource.data.length; index++) {
          const _data: any = this.dataSource.data[index];

          if (_data.requestId !== null) {
            if (this.requestid === _data.requestId) {
              this.selplanyear = _data;
              this.ckplandetail = this.selplanyear.id;
            }
          } else {
            console.log('???', this.ckplandetail);
          }
        }
      }
    });
    this.changeSaveDraft();
  }

  planYear(_element) {
    this.selplanyear = null;
    this.selplanyear = _element;

    console.log('this.selplanyear.planStatus', _element)
    if (this.selplanyear.planStatus === 'ล่าช้า') {
      this.ckplandetail = this.selplanyear.id;
      this.stepTwoData.requestyearPlan.id = this.selplanyear.id;
      this.stepTwoData.requestyearPlan.planStatusDetail = this.selplanyear.planStatusDetail;
    } else {
      console.log('this.selplanyear.id', this.selplanyear.id)
      this.ckplandetail = this.selplanyear.id;
      this.stepTwoData.requestyearPlan.id = this.selplanyear.id;
      this.stepTwoData.requestyearPlan.planStatusDetail = null;
    }


    // this.stepTwoData.requestyearPlan.id = this.selplanyear.id;
    // console.log('this.selplanyear.id', this.selplanyear.id)
  }

  changeSaveDraft() {
    console.log('changeSaveDraft');
    this.saveDraftstatus = false;
    this.sidebarService.inPageStatus(true);
  }

  checkDataStatus() {

  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate request');
    console.log('pageAction', this.pageAction);
    let gotolink = this.sidebarService.goToLink;
    if (this.pageAction === 'next') {
      gotolink = '/request/information-base/step3';
    }
    console.log('LINK:::', gotolink);
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef = this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        console.log('gotolink:::', gotolink)
        const search = gotolink.search('request');
        this.checkDataSuccess = this.check_require();
        console.log('checkDataSuccess', this.checkDataSuccess);
        if (data.returnStatus === true) {
          console.log('true', gotolink)
          const saveReturn = this.saveDraft();
          if (saveReturn) {
            if (search === -1) {
              console.log('header')
              subject.next(true);
            } else {
              if (
                gotolink === '/request/information-base/step1' ||
                gotolink === '/request/information-base/step3' ||
                gotolink === '/request/information-base/step4') {
                subject.next(true);
              } else {
                if (this.pageStatus && this.checkDataSuccess) {
                  subject.next(true);
                } else if (this.pageStatus && this.checkDataSuccess === false) {
                  alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                  subject.next(false);
                } else {
                  alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                  subject.next(false);
                }
              }
            }
          } else {
            subject.next(false);
          }
          
        } else if (data.returnStatus === false) {
          console.log('false', gotolink);
          if (search === -1) {
            console.log('header');
            subject.next(true);
          } else {
            if (
              gotolink === '/request/information-base/step1' ||
              gotolink === '/request/information-base/step3' ||
              gotolink === '/request/information-base/step4') {
              this.checkId();
              this.sidebarService.resetSaveStatusInfo();
              this.saveDraftstatus = null;
              subject.next(true);
              return true;
            } else {
              console.log('this.pageStatus', this.pageStatus);
              console.log('this.checkDataSuccess', this.checkDataSuccess);
              if (this.pageStatus) {
                subject.next(true);
              } else {
                if (this.checkDataSuccess === false) {
                  alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                  subject.next(false);
                } else {
                  this.sidebarService.resetSaveStatusInfo();
                  this.saveDraftstatus = null;
                  this.checkId();
                  // alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                  subject.next(true);
                }
              }

            }
          }
        } else {
          // this.sidebarService.inPageStatus(false)
          console.log('test(2)')
          subject.next(false);
        }
      });
      return subject;
    } else {
      console.log('test(3)')
      return true;
    }
  }




}
