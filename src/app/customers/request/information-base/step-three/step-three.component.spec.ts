import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { informationBaseStepThreeComponent } from './step-three.component';

describe('informationBaseStepThreeComponent', () => {
  let component: informationBaseStepThreeComponent;
  let fixture: ComponentFixture<informationBaseStepThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ informationBaseStepThreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(informationBaseStepThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
