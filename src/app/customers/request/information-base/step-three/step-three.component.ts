import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { StepThreeService } from '../../../../services/request/information-base/step-three/step-three.service';
import { RequestService } from '../../../../services/request/request.service';
import { DialogSaveStatusComponent } from '../../dialog/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from './../../../../services/configGuard/config-guard.service';


@Component({
    selector: 'app-step-three-base',
    templateUrl: './step-three.component.html',
    styleUrls: ['./step-three.component.scss']
})
// tslint:disable-next-line:class-name
export class informationBaseStepThreeComponent implements OnInit, CanComponentDeactivate {
    favoriteSeason: string;
    seasons: string[] = ['ไม่ใช่', 'ใช่'];
    testdata = {
        DSC: false,
        it: false,
        other: false
    };
    stepThreeData = {
        validate: null,
        id: null,
        status: false,
        approvers: {
            product: false,
            other: false,
            manage: false
        },
        approversOther: null,
        commenters: null,
        commentersOther: null,
        outsiderDetail: null,
        outsider: {
            no: false,
            yes: false,
            outhside: {
                BOT: false,
                IC: false,
                SEC: false,
                PON: false,
                other: false,
            }
        },
    };

    approvalDirectors = null;
    saveDraftstatus = null;
    outOfPageSave = false;
    linkTopage = null;
    openDoCheckc = false;
    pageChange = null;

    approvalAuthority = null;
    res_get: any;
    status_loading = true;
    pageAction = 'save'
    pageStatus = false;
    checkDataSuccess = false;
    requestId: any;
    res_id: any;
    res_save: any;
    @Input() template_manage = { summary: null };

    constructor(private router: Router,
        public dialog: MatDialog,
        private saveData: StepThreeService,
        private sidebarService: RequestService) {
    }

    ngOnInit() {
        // console.log('template_manage', this.template_manage);
        window.scrollTo(0, 0);
        this.openDoCheckc = true;
        this.checkId();
        if (localStorage.getItem('requestId')) {
            this.sidebarService.checkDefault(localStorage.getItem('requestId')).subscribe(page => {
                this.pageStatus = page['data']
            })
            this.sidebarService.sendEvent();
        } else {
            this.status_loading = false;
        }
    }
    viewNext() {
        this.router.navigate(['request/information-base/step4']);
    }
    changeApprovalOutsideList_1() {
        this.stepThreeData.outsider.yes = false;
    }

    changeApprovalOutsideList_2() {
        this.stepThreeData.outsider.no = false;
    }

    changeApprovalOutsideList_1_checkbox() {
        this.stepThreeData.outsider.no = true;
        this.stepThreeData.outsider.yes = false;
        this.stepThreeData.outsider.outhside.BOT = false;
        this.stepThreeData.outsider.outhside.IC = false;
        this.stepThreeData.outsider.outhside.SEC = false;
        this.stepThreeData.outsider.outhside.PON = false;
        this.stepThreeData.outsider.outhside.other = false;
        this.stepThreeData.outsiderDetail = null;

    }

    changeApprovalOutsideList_2_checkbox() {
        this.stepThreeData.outsider.no = false;
        this.stepThreeData.outsider.yes = true;
    }

    checkradio() {
        if (this.favoriteSeason === 'ไม่ใช่') {
            this.stepThreeData.outsider.no = true;
            this.stepThreeData.outsider.yes = false;
            this.stepThreeData.outsider.outhside.BOT = false;
            this.stepThreeData.outsider.outhside.IC = false;
            this.stepThreeData.outsider.outhside.other = false;
            this.stepThreeData.outsider.outhside.PON = false;
            this.stepThreeData.outsider.outhside.SEC = false;
            if (this.stepThreeData.outsider.outhside.other === false) {
                this.clearList_5_detail();
            }
        }
        if (this.favoriteSeason === 'ใช่') {
            this.stepThreeData.outsider.no = false;
            this.stepThreeData.outsider.yes = true;
        }
    }

    clearApprovalAuthority(number) {
        // this.stepThreeData.approvalAuthority = !this.stepThreeData.approvalAuthority;
        if (number === '1') {
            this.approvalAuthority = '1';
            this.stepThreeData.approvers = {
                product: true,
                other: false,
                manage: false
            };
            this.stepThreeData.approversOther = null;
        } else if (number === '2') {
            this.approvalAuthority = '2';
            this.stepThreeData.approvers = {
                product: false,
                other: false,
                manage: true
            };
            this.stepThreeData.approversOther = null;
        } else if (number === '3') {
            this.approvalAuthority = '3';
            this.stepThreeData.approvers = {
                product: false,
                other: true,
                manage: false
            };
        } else {
            this.stepThreeData.approvers = {
                product: false,
                other: false,
                manage: false
            };
        }
        this.changeSaveDraft();
    }

    commentator(values) {
        // console.log(values);
        if (this.testdata.other === false) {
            this.stepThreeData.commentersOther = null;
        }
        // console.log(this.testdata);
        // console.log(this.stepThreeData.commentersOther);
        this.changeSaveDraft();
    }

    clearList_5_detail() {
        if (this.stepThreeData.outsider.outhside.other === false) {
            this.stepThreeData.outsiderDetail = null;
        }
    }

    clearList_5_detail_div() {
        if (this.stepThreeData.outsider.yes === true) {
            this.stepThreeData.outsider.outhside.other = !this.stepThreeData.outsider.outhside.other;
            this.stepThreeData.outsiderDetail = null;
        }
    }

    check_require() {
        if (this.stepThreeData.approvers && this.stepThreeData.commenters && this.stepThreeData.outsider) {
            const check_approvers = Object.values(this.stepThreeData.approvers).some(value => value === true);
            const check_outsider = Object.values(this.stepThreeData.outsider).some(value => value === true);

            if (check_approvers === true && check_outsider === true) {
                this.stepThreeData.status = true;
            } else {
                this.stepThreeData.status = false;
            }
        }
        return this.stepThreeData.status;
    }

    checkId() {
        this.requestId = localStorage.getItem('requestId');
        const body = { id: this.requestId };
        if (localStorage.getItem('requestId') !== null
            && localStorage.getItem('requestId') !== ''
            && Number(localStorage.getItem('requestId')) !== 0) {
            this.saveData.getPage3(body).subscribe(res => {
                console.log('?????', res);
                this.res_get = res;
                if (res['status'] === 'success') {
                    this.status_loading = false;
                }

                // console.log(this.res_get.approvers, this.res_get.commenters, this.res_get.outsider)
                if (this.res_get.data.approvers === {}) {
                    this.res_get.data.approvers = {
                        product: false,
                        other: false,
                        manage: false
                    };
                } else if (this.res_get.data.approvers.product === true) {
                    this.approvalAuthority = '1';
                } else if (this.res_get.data.approvers.manage === true) {
                    this.approvalAuthority = '2';
                } else if (this.res_get.data.approvers.other === true) {
                    this.approvalAuthority = '3';
                }

                if (this.res_get.data.commenters === {}) {
                    this.res_get.data.commenters = {
                        DSC: false,
                        it: false,
                        other: false
                    };
                }
                if (this.res_get.data.commenters.DSC === true) {
                    this.testdata.DSC = true;
                }
                if (this.res_get.data.commenters.it === true) {
                    this.testdata.it = true;
                }
                if (this.res_get.data.commenters.other === true) {
                    this.testdata.other = true;
                }

                if (this.res_get.data.outsider === {}) {
                    this.res_get.data.outsider = {
                        no: false,
                        yes: false,
                        outhside: {
                            BOT: false,
                            IC: false,
                            SEC: false,
                            PON: false,
                            other: false,
                        }
                    };
                }
                if (this.res_get.data.outsider.yes === true) {
                    this.favoriteSeason = 'ใช่';
                }
                if (this.res_get.data.outsider.no === true) {
                    this.favoriteSeason = 'ไม่ใช่';
                }
                this.stepThreeData = this.res_get.data;
                console.log('hhhhhh', this.stepThreeData);
            });


        }
    }

    saveDraft(nextPage?) {
        this.status_loading = true;
        this.check_require();
        let _q1 = '';
        let _q2 = '';
        let _q3 = '';
        let _q4 = '';
        if (this.favoriteSeason === 'ใช่') {
            if (this.stepThreeData.outsider.outhside.BOT === false
                && this.stepThreeData.outsider.outhside.IC === false
                && this.stepThreeData.outsider.outhside.SEC === false
                && this.stepThreeData.outsider.outhside.PON === false
                && this.stepThreeData.outsider.outhside.other === false
            ) {
                _q4 = 'false';
            } else {
                _q4 = 'true';
            }
        } else {
            _q4 = 'true';
        }

        if (this.approvalAuthority === '3' || this.testdata.other === true || this.stepThreeData.outsider.outhside.other === true) {
            // tslint:disable-next-line: max-line-length
            if (this.approvalAuthority === '3' && (this.stepThreeData.approversOther === null || this.stepThreeData.approversOther === '')) {
                console.log('ข้อ 1 ว่าง');
                _q1 = 'false';
            } else {
                _q1 = 'true';
            }
            // tslint:disable-next-line: max-line-length
            if (this.testdata.other === true && (this.stepThreeData.commentersOther === null || this.stepThreeData.commentersOther === '')) {
                console.log('ข้อ 2 ว่าง');
                _q2 = 'false';
            } else {
                _q2 = 'true';
            }
            // tslint:disable-next-line: max-line-length
            if (this.stepThreeData.outsider.outhside.other === true && (this.stepThreeData.outsiderDetail === null || this.stepThreeData.outsiderDetail === '')) {
                console.log('ข้อ 3 ว่าง');
                _q3 = 'false';
            } else {
                _q3 = 'true';
            }
        }

        if (_q1 === 'false' || _q2 === 'false' || _q3 === 'false') {
            this.status_loading = false;
            alert('กรุณากรอกเหตุผลประกอบ');
            return false;
        } else if (_q4 === 'false') {
            this.status_loading = false;
            alert('หัวข้อการขออนุญาตจากหน่วยงานภายนอก: กรุณาเลือกตัวเลือกในกรอบสีเทา');
            return false;
        } else {
            return this.save(null, nextPage);
        }
    }

    save(page?, nextPage?) {
        const returnUpdateData = new Subject<any>();
        if (localStorage.getItem('requestId')) {
            this.stepThreeData.id = localStorage.getItem('requestId');
        }
        this.stepThreeData.commenters = {
            DSC: this.testdata.DSC,
            it: this.testdata.it,
            other: this.testdata.other
        };
        this.check_require();
        console.log('firstStepData3333:', this.stepThreeData);
        this.saveDraftstatus = 'success';
        this.saveData.savePage3(this.stepThreeData).subscribe(res => {
            // console.log('savePage3 res', res);
            if (res['status'] === 'success') {
                this.res_save = res;
                this.status_loading = false;
                this.stepThreeData = this.res_save.data;
                localStorage.setItem('requestId', this.res_save.data.id);
                this.saveDraftstatus = this.res_save.status;
                returnUpdateData.next(true);
                // if ( nextPage === 'nextInfo') {
                //     this.router.navigate([this.linkTopage]);
                // } if (nextPage === 'nextSummary') {
                //     if (page === 'next') {
                //         this.router.navigate(['/request-summary/information-base/step4']);
                //     }
                // } else {
                //     if (page === 'next') {
                //         this.router.navigate(['request/information-base/step4']);
                //     }
                // }
                setTimeout(() => {
                    this.saveDraftstatus = null;
                }, 3000);
                this.sidebarService.sendEvent();
                this.sidebarService.resetSaveStatusInfo();
            } else {
                returnUpdateData.next(false);
                this.status_loading = false;
                alert(res['message']);
                this.sidebarService.resetPageInfo();
            }
        });
        return returnUpdateData;
    }


    next_step(nextPage?) {
        this.pageAction = 'next'
        this.router.navigate(['/request/information-base/step4']);
    }

    autogrow(index: any) {
        if (index === 1) {
            const textother1 = document.getElementById('textother1');
            textother1.style.overflow = 'hidden';
            textother1.style.height = 'auto';
            textother1.style.height = textother1.scrollHeight + 'px';
        }

        if (index === 2) {
            const textother2 = document.getElementById('textother2');
            textother2.style.overflow = 'hidden';
            textother2.style.height = 'auto';
            textother2.style.height = textother2.scrollHeight + 'px';
        }

        if (index === 3) {
            const textother3 = document.getElementById('textother3');
            textother3.style.overflow = 'hidden';
            textother3.style.height = 'auto';
            textother3.style.height = textother3.scrollHeight + 'px';
        }
    }

    changeSaveDraft() {
        this.saveDraftstatus = false;
        this.sidebarService.inPageStatus(true);
    }


    canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
        console.log('canDeactivate request');
        console.log('pageAction', this.pageAction);
        let gotolink = this.sidebarService.goToLink;
        if (this.pageAction === 'next') {
            gotolink = '/request/information-base/step4';
        }
        console.log('LINK:::', gotolink);
        const subject = new Subject<boolean>();
        const status = this.sidebarService.outPageStatus();
        if (status === true) {
            // -----
            const dialogRef = this.dialog.open(DialogSaveStatusComponent, {
                disableClose: true,
                data: {
                    headerDetail: 'ยืนยันการดำเนินการ',
                    bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
                    bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
                    returnDetail: 'null',
                    returnStatus: false,
                    icon: 'assets/img/Alarm-blue.svg',
                }
            });
            dialogRef.afterClosed().subscribe((data) => {
                console.log('gotolink:::', gotolink)
                const search = gotolink.search('request');
                this.checkDataSuccess = this.check_require();
                if (data.returnStatus === true) {
                    console.log('true', gotolink);
                    const saveReturn = this.saveDraft();
                    if (saveReturn) {
                        if (search === -1) {
                            console.log('header');
                            subject.next(true);
                        } else {
                            if (
                                gotolink === '/request/information-base/step2' ||
                                gotolink === '/request/information-base/step1' ||
                                gotolink === '/request/information-base/step4') {
                                subject.next(true);
                            } else {
                                if (this.pageStatus && this.checkDataSuccess) {
                                    subject.next(true);
                                } else if (this.pageStatus && this.checkDataSuccess === false) {
                                    alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                                    subject.next(false);
                                } else {
                                    // alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                                    subject.next(true);
                                }
                            }
                        }
                    } else {
                        subject.next(false);
                    }
                } else if (data.returnStatus === false) {
                    console.log('false', gotolink);
                    if (search === -1) {
                        console.log('header');
                        subject.next(true);
                    } else {
                        if (
                            gotolink === '/request/information-base/step2' ||
                            gotolink === '/request/information-base/step1' ||
                            gotolink === '/request/information-base/step4') {
                            this.checkId();
                            this.sidebarService.resetSaveStatusInfo();
                            this.saveDraftstatus = null;
                            subject.next(true);
                            return true;
                        } else {
                            console.log('this.pageStatus', this.pageStatus)
                            console.log('this.checkDataSuccess', this.checkDataSuccess)
                            if (this.pageStatus) {
                                subject.next(true);
                            } else {
                                if (this.checkDataSuccess === false) {
                                    alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                                    subject.next(false);
                                } else {
                                    this.checkId()
                                    this.sidebarService.resetSaveStatusInfo();
                                    this.saveDraftstatus = null;
                                    // alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                                    subject.next(true);
                                }
                            }
                        }
                    }
                } else {
                    this.sidebarService.inPageStatus(false)
                    console.log('test(2)')
                    subject.next(false);
                }
            });
            return subject;
        } else {
            console.log('test(3)')
            return true;
        }
    }
}
