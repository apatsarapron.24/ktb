import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { informationBaseStepFourComponent } from './step-four.component';

describe('informationBaseStepFourComponent', () => {
  let component: informationBaseStepFourComponent;
  let fixture: ComponentFixture<informationBaseStepFourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ informationBaseStepFourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(informationBaseStepFourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
