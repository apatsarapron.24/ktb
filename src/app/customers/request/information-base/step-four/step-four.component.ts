import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
// tslint:disable-next-line: max-line-length
import { ProductDefultStepFourService } from '../../../../services/request/information-base/step-four/product-defult-step-four.service';
import { Router } from '@angular/router';
import { RequestService } from '../../../../services/request/request.service';
import { DialogSaveStatusComponent } from '../../dialog/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from './../../../../services/configGuard/config-guard.service';
import { truncate } from 'fs';

@Component({
  selector: 'app-step-four-base',
  templateUrl: './step-four.component.html',
  styleUrls: ['./step-four.component.scss']
})
// tslint:disable-next-line:class-name
export class informationBaseStepFourComponent implements OnInit, CanComponentDeactivate {
  stepFourData = {
    validate: null,
    executiveName: null,
    executiveName_phoneNumber: null,
    coordinatorName: null,
    coordinatorName_phoneNumber: null,
  };
  saveDraftStepFourData: any;
  requestId: null;
  saveDraftstatus = null;
  outOfPageSave = false;
  linkTopage = null;
  pageChange = null;
  openDoCheckc = false;

  status_loading = true;
  pageAction = 'save'
  pageStatus = false;
  page4Step: any;
  checkDataSuccess = false;

  @Input() template_manage = { summary: null };

  saveDraft(nextPage?) {
    console.log('firstStepData:', this.stepFourData);
    const data = this.upDateDataFirstStep(nextPage);
    this.saveDraftStepFourData = this.stepFourData;
    return data;
  }
  viewNext() {
    this.router.navigate(['request/product-detail/step1']);
  }
  next_step() {
    this.pageAction = 'next';

    let statusExecutiveName = false;
    let statusExecutiveName_phone = false;
    let statusCoordinatorName = false;
    let statusCoordinatorName_phone = false;
    let statusSuccess = false;

    if (this.stepFourData.executiveName) {
      statusExecutiveName = true;
    }
    if (this.stepFourData.executiveName_phoneNumber) {
      statusExecutiveName_phone = true;
    }
    if (this.stepFourData.coordinatorName) {
      statusCoordinatorName = true;
    }
    if (this.stepFourData.coordinatorName_phoneNumber) {
      statusCoordinatorName_phone = true;
    }

    console.log(statusExecutiveName, statusExecutiveName_phone, statusCoordinatorName, statusCoordinatorName_phone);

    if (statusExecutiveName &&
      statusExecutiveName_phone &&
      statusCoordinatorName &&
      statusCoordinatorName_phone
    ) {
      statusSuccess = true;
    }
    //  +++++++++ send API ++++++++++++++++
    // backend <-- frontend
    let requestId = null;
    if (localStorage.getItem('requestId')) {
      requestId = Number(localStorage.getItem('requestId'));
    }


    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef = this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        if (data.returnStatus === true) {

          const dataInfo = {
            'id': requestId,
            'teamManager': this.stepFourData.executiveName,
            'teamManagerPhone': this.stepFourData.executiveName_phoneNumber,
            'coordinator': this.stepFourData.coordinatorName,
            'coordinatorPhone': this.stepFourData.coordinatorName_phoneNumber,
            'status': statusSuccess
          };
          this.checkDataSuccess = statusSuccess;

          console.log(this.pageStatus);
          console.log(this.saveDraftstatus);

          this.productDefultStepFour.upDateDefultFirstStep(dataInfo).subscribe(res => {
            console.log('res send API', res['status']);
            if (res['status'] === 'success') {
              this.status_loading = false;
              this.saveDraftstatus = 'success';
              localStorage.setItem('requestId', res['data'].id);
              this.sidebarService.inPageStatus(false);
              setTimeout(() => {
                this.saveDraftstatus = null;
              }, 3000);
              this.sidebarService.sendEvent();
              this.GetDataStepFour(localStorage.getItem('requestId'));

              this.sidebarService.statusSibar(localStorage.getItem('requestId')).subscribe(bar => {
                console.log('bar 2::', bar['data']['pageDefault']);
                this.page4Step = bar['data']['pageDefault'];
                console.log('page 1::', this.page4Step['page1']);
                console.log('page 2::', this.page4Step['page2']);
                console.log('page 3::', this.page4Step['page3']);
                console.log('page 4::', this.page4Step['page4']);
                if (this.page4Step['page1'] === true
                && this.page4Step['page2'] === true
                && this.page4Step['page3'] === true
                && this.page4Step['page4'] === true) {
                  this.router.navigate(['request/product-detail/step1']);
                } else {
                  alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                }
              });

            } else {
              alert(res['message']);
            }

          }, err => {
            this.status_loading = false;
            alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
            console.log(err);
          });

        } else if (data.returnStatus === false) {
          this.sidebarService.inPageStatus(false);
          if (localStorage.getItem('requestId')) {
            this.GetDataStepFour(localStorage.getItem('requestId'));
            this.sidebarService.statusSibar(localStorage.getItem('requestId')).subscribe(bar => {
              console.log('bar 2::', bar['data']['pageDefault']);
              this.page4Step = bar['data']['pageDefault'];
              console.log('page 1::', this.page4Step['page1']);
              console.log('page 2::', this.page4Step['page2']);
              console.log('page 3::', this.page4Step['page3']);
              console.log('page 4::', this.page4Step['page4']);
              if (this.page4Step['page1'] === true
              && this.page4Step['page2'] === true
              && this.page4Step['page3'] === true
              && this.page4Step['page4'] === true) {
                this.router.navigate(['request/product-detail/step1']);
              } else {
                alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
              }
            });
          } else {
            this.stepFourData.executiveName = null;
            this.stepFourData.executiveName_phoneNumber = null;
            this.stepFourData.coordinatorName = null;
            this.stepFourData.coordinatorName_phoneNumber = null;
            alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
          }
        } else {
          console.log('test(2)');
        }
      });
    } else {
      console.log('test(3)');
      if (localStorage.getItem('requestId')) {
        this.sidebarService.statusSibar(localStorage.getItem('requestId')).subscribe(bar => {
          console.log('bar 2::', bar['data']['pageDefault']);
          this.page4Step = bar['data']['pageDefault'];
          console.log('page 1::', this.page4Step['page1']);
          console.log('page 2::', this.page4Step['page2']);
          console.log('page 3::', this.page4Step['page3']);
          console.log('page 4::', this.page4Step['page4']);
          if (this.page4Step['page1'] === true
          && this.page4Step['page2'] === true
          && this.page4Step['page3'] === true
          && this.page4Step['page4'] === true) {
            this.router.navigate(['request/product-detail/step1']);
          } else {
            alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
          }
        });
      } else {
        alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
      }

    }



  }
  changeSaveDraft() {
    this.saveDraftstatus = 'none';
    this.sidebarService.inPageStatus(true);
  }


  // ++++++++++  API +++++++++++++
  GetDataStepFour(documentId) {
    this.productDefultStepFour.getDefultFirstStep(documentId).subscribe(res => {
      console.log('res', res);
      const productDefaultStep4 = res['data'];
      if (res['status'] === 'success') {
        this.status_loading = false;
      }
      if (productDefaultStep4 !== null) {
        // frontend <-- backend
        this.stepFourData = {
          validate: productDefaultStep4['validate'],
          executiveName: productDefaultStep4['teamManager'],
          executiveName_phoneNumber: productDefaultStep4['teamManagerPhone'],
          coordinatorName: productDefaultStep4['coordinator'],
          coordinatorName_phoneNumber: productDefaultStep4['coordinatorPhone'],
        };
      }
    }, err => {
      this.status_loading = false;
      alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
      console.log(err);
    });
  }

  upDateDataFirstStep(nextPage?) {
    let statusExecutiveName = false;
    let statusExecutiveName_phone = false;
    let statusCoordinatorName = false;
    let statusCoordinatorName_phone = false;
    let statusSuccess = false;

    if (this.stepFourData.executiveName) {
      statusExecutiveName = true;
    }
    if (this.stepFourData.executiveName_phoneNumber) {
      statusExecutiveName_phone = true;
    }
    if (this.stepFourData.coordinatorName) {
      statusCoordinatorName = true;
    }
    if (this.stepFourData.coordinatorName_phoneNumber) {
      statusCoordinatorName_phone = true;
    }

    console.log(statusExecutiveName, statusExecutiveName_phone, statusCoordinatorName, statusCoordinatorName_phone);

    if (statusExecutiveName &&
      statusExecutiveName_phone &&
      statusCoordinatorName &&
      statusCoordinatorName_phone
    ) {
      statusSuccess = true;
    }
    //  +++++++++ send API ++++++++++++++++
    // backend <-- frontend
    let requestId = null;
    if (localStorage.getItem('requestId')) {
      requestId = Number(localStorage.getItem('requestId'));
    }

    const data = {
      'id': requestId,
      'teamManager': this.stepFourData.executiveName,
      'teamManagerPhone': this.stepFourData.executiveName_phoneNumber,
      'coordinator': this.stepFourData.coordinatorName,
      'coordinatorPhone': this.stepFourData.coordinatorName_phoneNumber,
      'status': statusSuccess
    };
    this.checkDataSuccess = statusSuccess
    console.log('data : ', data);
    const returnUpdateData = new Subject<any>();
    this.productDefultStepFour.upDateDefultFirstStep(data).subscribe(res => {
      console.log('res send API', res['status']);
      if (res['status'] === 'success') {
        this.status_loading = false;
        this.saveDraftstatus = 'success';
        returnUpdateData.next(true);
        this.sidebarService.inPageStatus(false);
        setTimeout(() => {
          this.saveDraftstatus = null;
        }, 3000);
        localStorage.setItem('requestId', res['data'].id);
        this.sidebarService.sendEvent();
        if (nextPage === 'nextInfo') {
          this.router.navigate([this.linkTopage]);
        }

        this.GetDataStepFour(localStorage.getItem('requestId'));
      } else {
        alert(res['message']);
        returnUpdateData.next(false);
      }

    }, err => {
      this.status_loading = false;
      alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
      console.log(err);
      returnUpdateData.next(false);
    });
    return returnUpdateData;
  }


  constructor(private router: Router,
    public dialog: MatDialog,
    private productDefultStepFour: ProductDefultStepFourService,
    private sidebarService: RequestService) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    console.log('template_manage', this.template_manage);
    this.openDoCheckc = true;
    // localStorage.removeItem('requestId');
    console.log('localStorage', localStorage.getItem('requestId'));
    if (localStorage) {
      if (localStorage.getItem('requestId') !== null
        && localStorage.getItem('requestId') !== ''
        && Number(localStorage.getItem('requestId')) !== 0) {
        this.GetDataStepFour(localStorage.getItem('requestId'));
        this.sidebarService.sendEvent();
        this.sidebarService.checkDefault(localStorage.getItem('requestId')).subscribe(page => {
          console.log('page::', page);
          this.pageStatus = page['data'];
        });
        this.sidebarService.statusSibar(localStorage.getItem('requestId')).subscribe(bar => {
          console.log('bar::', bar['data']['pageDefault']);
          this.page4Step = bar['data']['pageDefault'];
        });
      } else {
        this.status_loading = false;
      }
    } else {
      console.log('Brownser not support');
    }
  }

  // ngDoCheck() {
  //   if (this.openDoCheckc === true) {
  //     this.pageChange = this.sidebarService.changePageGoToLink();
  //     if (this.pageChange) {
  //       const data = this.sidebarService.getsavePage();
  //       let saveInFoStatus = false;
  //       saveInFoStatus = data.saveStatusInfo;
  //       if (saveInFoStatus === true) {
  //         this.outOfPageSave = true;
  //         this.linkTopage = data.goToLink;
  //         this.saveDraft('nextInfo');
  //         this.sidebarService.resetSaveStatu();
  //       }
  //     }
  //   }
  // }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate request');
    console.log('pageAction', this.pageAction);
    var gotolink = this.sidebarService.goToLink
    if (this.pageAction == 'next') {
      gotolink = '/request/product-detail/step1'
    }
    console.log('LINK:::', gotolink)
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef = this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        console.log('gotolink:::', gotolink)
        const search = gotolink.search('request');
        if (data.returnStatus === true) {
          console.log('true', gotolink)
          this.saveDraft();
          // this.saveDraftstatus = null;
          if (search == -1) {
            console.log('header')
            subject.next(true);
          } else {
            if (
              gotolink == '/request/information-base/step1' ||
              gotolink == '/request/information-base/step2' ||
              gotolink == '/request/information-base/step3') {
              subject.next(true);
            }
            else {
              if (this.page4Step.page1 && this.page4Step.page2 &&
                this.page4Step.page3 && this.checkDataSuccess) {
                subject.next(true);
              }
              else if (this.checkDataSuccess == false) {
                alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                subject.next(false);
              } else {
                alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                subject.next(false);
              }
            }
          }
        } else if (data.returnStatus === false) {
          console.log('false', gotolink)
          if (search == -1) {
            console.log('header')
            subject.next(true);
          } else {
            if (
              gotolink == '/request/information-base/step1' ||
              gotolink == '/request/information-base/step2' ||
              gotolink == '/request/information-base/step3') {
              this.GetDataStepFour(localStorage.getItem('requestId'));
              this.sidebarService.resetSaveStatusInfo();
              this.saveDraftstatus = null;
              subject.next(true);
              return true;
            }
            else {
              console.log('this.pageStatus', this.pageStatus)
              console.log('this.checkDataSuccess', this.checkDataSuccess)
              if (this.pageStatus) {
                if (this.checkDataSuccess) {
                  subject.next(true);
                } else {
                  alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                  subject.next(false);
                }
              } else {
                if (this.checkDataSuccess == false) {
                  alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                  subject.next(false);
                } else {
                  this.sidebarService.resetSaveStatusInfo();
                  this.saveDraftstatus = null;
                  this.GetDataStepFour(localStorage.getItem('requestId'));
                  // alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                  subject.next(true);
                }
              }
            }
          }
        } else {
          // this.sidebarService.inPageStatus(false)
          console.log('test(2)')
          subject.next(false);
        }
      });
      return subject;
    } else {
      console.log('test(3)')
      return true;
    }
  }

}
