import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface DialogData {
  headerDetail: any;
  bodyDetail1: any;
  bodyDetail2: any;
  returnStatus: any;
  icon: any;
}

@Component({
  selector: 'app-confirm-request-summary-models',
  templateUrl: './confirm-request-summary-models.component.html',
  styleUrls: ['./confirm-request-summary-models.component.scss']
})
export class ConfirmRequestSummaryModelsComponent implements OnInit {

  close() {
    this.data.returnStatus = false;
    this.dialogRef.close(this.data);
  }

  back() {
    this.data.returnStatus = false;
    this.dialogRef.close(this.data);
  }

  save() {
    this.data.returnStatus = true;
    this.dialogRef.close(this.data);
  }

  constructor(
    private dialogRef: MatDialogRef<ConfirmRequestSummaryModelsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.dialogRef.disableClose = true;
  }

  ngOnInit() {
  }

}
