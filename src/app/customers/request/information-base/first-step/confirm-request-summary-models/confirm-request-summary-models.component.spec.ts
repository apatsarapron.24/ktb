import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmRequestSummaryModelsComponent } from './confirm-request-summary-models.component';

describe('ConfirmRequestSummaryModelsComponent', () => {
  let component: ConfirmRequestSummaryModelsComponent;
  let fixture: ComponentFixture<ConfirmRequestSummaryModelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmRequestSummaryModelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmRequestSummaryModelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
