import { Result } from './../../../dashboard-cus/dashboard-cus.component';
import { CanComponentDeactivate } from './../../../../services/configGuard/config-guard.service';
import { forEach } from '@angular/router/src/utils/collection';
import { dateFormat } from 'highcharts';
import { Component, OnInit, ViewChild, Input } from '@angular/core';
import * as moment from 'moment';
import {
  AngularMyDatePickerDirective,
  IAngularMyDpOptions,
  IMyDateModel,
} from 'angular-mydatepicker';
import { Router } from '@angular/router';
// tslint:disable-next-line: max-line-length
import { ProductDefultFirstStepService } from '../../../../services/request/information-base/first-step/product-defult-first-step.service';
import { RequestService } from '../../../../services/request/request.service';
import { DateAdapter, MatAutocompleteTrigger, MatDatepickerInputEvent } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { startWith, map, count } from 'rxjs/operators';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ConfirmRequestSummaryModelsComponent } from './confirm-request-summary-models/confirm-request-summary-models.component';
import { DateComponent } from '../../../datepicker/date/date.component';
import { listenToElementOutputs } from '@angular/core/src/view/element';
import { DialogSaveStatusComponent } from '../../dialog/dialog-save-status/dialog-save-status.component';

export interface StateSummary {
  requestId: number;
  productName: string;
}

@Component({
  selector: 'app-first-step',
  templateUrl: './first-step.component.html',
  styleUrls: ['./first-step.component.scss'],
})



// tslint:disable-next-line:class-name
export class informationBaseFirstStepComponent implements OnInit, CanComponentDeactivate {

  favoriteSeason: string;
  status_loading = true;
  seasons: string[] = ['Winter', 'Spring', 'Summer', 'Autumn'];
  canDeactivateOpen = true;
  // private model: Object = {};
  model: any = null;
  model2: any = null;
  model3: any = null;
  @ViewChild('dp') mydp: AngularMyDatePickerDirective;
  @ViewChild(MatAutocompleteTrigger) autocomplete: MatAutocompleteTrigger;
  // All data in page +++++++++++++
  firstStepData = {
    validate: null,
    applicationNumber: null,
    PresentType: null,
    agendaType: null,
    subAgendaType: null,
    presentPurpose: null,
    productInfor: null,
    productsOffered: {
      productProgram: false,
      nonCredit: false,
      pricing: false,
      outsourcing: false,
      insourcing: false,
    },
    productName: null,
    productCode: null,
    productGroup: {
      creditProduct: false,
      CP_BusinessLoan: false,
      CP_PersonalLoan: false,
      CP_HousingLoan: false,
      CP_Etc: false,
      CP_Etc_digital: null,
      nonCreditProduct: false,
      NCP_nonCreditProduct: false,
      NCP_Etc: false,
      NCP_Etc_digital: null,
      insourcing: false,
      outsourcing: false,
    },
    serviceDelivery: {
      generalBranch: false,
      electronics: false,
      businessOffice: false,
      CBC: false,
      blockChain: false,
      callCenter: false,
      ATM: false,
      digitalChannels: false,
      detailDigitalChannels: null,
      nonBank: false,
      detailNonBank: null,
      etc: false,
      detailEtc: null,
    },
    expectedDate: null,
    lastReviewDate: null,
    nextReviewDate: null,
    requestRepeatName: null,
    requestRepeatId: null
  };
  newProduct = true;
  saveDraftstatus = false;
  outOfPageSave = false;
  linkTopage = null;

  dateRange = [];
  start_date: any;
  end_date: any;

  // alert
  alerts = false;

  requestId = null;

  dataChange = false;
  changeChange = null;

  getSummaryListstart = true;

  // date
  releaseDate: string;
  lastReviewDate: string;
  nextReviewDate: string;

  // summary
  control = new FormControl();
  summaryList: StateSummary[] = [
    {
      requestId: 1,
      productName: 'aaa'
    }
  ];
  requestRepeatList: Observable<StateSummary[]>;
  getSummary: any;

  listDate: any = [];
  placeholder_input = 'วันที่';

  pageChange = null;
  openDoCheckc = false;
  changeRequestRepeatId = true; // สามารถเเก้ไขวัตถุประสงค์การนำเสนอได้

  // save--
  pageAction = 'save';
  nextPage = null;
  checkDataSuccess = false;
  pageStatus: boolean;
  @Input() template_manage = { summary: null };

  @ViewChild(DateComponent) Date: DateComponent;


  saveDraft(nextPage?) {
    return this.upDateDataFirstStep(localStorage.getItem('requestId'), this.pageAction);
  }
  next_step() {
    this.pageAction = 'next';
    this.router.navigate(['request/information-base/step2']);
  }

  changeSaveDraft() {
    this.saveDraftstatus = false;
    this.dataChange = true;
    this.sidebarService.inPageStatus(true);
  }

  productGroup(numder) {
    // console.log('numder', numder);
    if (numder === 1) {
      // console.log('zzz', this.firstStepData.productGroup.creditProduct);
      this.firstStepData.productGroup.creditProduct = !this.firstStepData
        .productGroup.creditProduct;
      this.firstStepData.productGroup.nonCreditProduct = false;
      this.firstStepData.productGroup.insourcing = false;
      this.firstStepData.productGroup.outsourcing = false;
      this.clearCreditProductSub();
      this.clearnonCreditProductSub();
    } else if (numder === 2) {
      this.firstStepData.productGroup.creditProduct = false;
      this.firstStepData.productGroup.nonCreditProduct = !this.firstStepData
        .productGroup.nonCreditProduct;
      this.firstStepData.productGroup.insourcing = false;
      this.firstStepData.productGroup.outsourcing = false;
      this.clearCreditProductSub();
      this.clearnonCreditProductSub();
    } else if (numder === 3) {
      this.firstStepData.productGroup.creditProduct = false;
      this.firstStepData.productGroup.nonCreditProduct = false;
      this.firstStepData.productGroup.insourcing = !this.firstStepData
        .productGroup.insourcing;
      this.firstStepData.productGroup.outsourcing = false;
      this.clearCreditProductSub();
      this.clearnonCreditProductSub();
    } else if (numder === 4) {
      this.firstStepData.productGroup.creditProduct = false;
      this.firstStepData.productGroup.nonCreditProduct = false;
      this.firstStepData.productGroup.insourcing = false;
      this.firstStepData.productGroup.outsourcing = !this.firstStepData
        .productGroup.outsourcing;
      this.clearCreditProductSub();
      this.clearnonCreditProductSub();
    }
  }

  clearCreditProductSub() {
    this.firstStepData.productGroup.CP_BusinessLoan = false;
    this.firstStepData.productGroup.CP_PersonalLoan = false;
    this.firstStepData.productGroup.CP_HousingLoan = false;
    this.firstStepData.productGroup.CP_Etc = false;
    this.firstStepData.productGroup.CP_Etc_digital = null;
  }

  clearAgendaType() {
    this.firstStepData.subAgendaType = null;
  }

  creditProductSub(numder) {
    if (this.firstStepData.productGroup.creditProduct) {
      if (numder === 1) {
        this.firstStepData.productGroup.CP_BusinessLoan = !this.firstStepData
          .productGroup.CP_BusinessLoan;
      } else if (numder === 2) {
        this.firstStepData.productGroup.CP_PersonalLoan = !this.firstStepData
          .productGroup.CP_PersonalLoan;
      } else if (numder === 3) {
        this.firstStepData.productGroup.CP_HousingLoan = !this.firstStepData
          .productGroup.CP_HousingLoan;
      } else if (numder === 4) {
        this.firstStepData.productGroup.CP_Etc = !this.firstStepData
          .productGroup.CP_Etc;
        this.firstStepData.productGroup.CP_Etc_digital = null;
      }
    }
    this.changeSaveDraft();
  }
  nonCreditProductSub(numder) {
    if (this.firstStepData.productGroup.nonCreditProduct) {
      if (numder === 1) {
        this.firstStepData.productGroup.NCP_nonCreditProduct = !this
          .firstStepData.productGroup.NCP_nonCreditProduct;
      } else if (numder === 2) {
        this.firstStepData.productGroup.NCP_Etc = !this.firstStepData
          .productGroup.NCP_Etc;
        this.firstStepData.productGroup.NCP_Etc_digital = null;
      }
    }
  }
  clearnonCreditProductSub() {
    this.firstStepData.productGroup.NCP_nonCreditProduct = false;
    this.firstStepData.productGroup.NCP_Etc = false;
    this.firstStepData.productGroup.NCP_Etc_digital = null;
  }
  clearServiceDeliveryDetailDigitalChannels() {
    this.firstStepData.serviceDelivery.digitalChannels = !this.firstStepData
      .serviceDelivery.digitalChannels;
    this.firstStepData.serviceDelivery.detailDigitalChannels = null;
  }
  clearServiceDeliveryDetailNonBank() {
    this.firstStepData.serviceDelivery.nonBank = !this.firstStepData
      .serviceDelivery.nonBank;
    this.firstStepData.serviceDelivery.detailNonBank = null;
  }
  clearServiceDeliveryDetailEtc() {
    this.firstStepData.serviceDelivery.etc = !this.firstStepData.serviceDelivery
      .etc;
    this.firstStepData.serviceDelivery.detailEtc = null;
  }

  // ====================  API =============================

  GetDataFirstStep(documentId) {
    this.saveDraftstatus = null;
    this.productDefultFirstStep
      .getDefultFirstStep(documentId)
      .subscribe((res) => {
        console.log('get data res', res);
        if (res['status'] === 'success') {
          this.status_loading = false;
          console.log('get data res', res['data'].objective);
        }
        if (res['data'] !== null) {
          const productDefaultStep1 = res['data'];
          console.log('999999999=>', productDefaultStep1.requestRepeatId);
          if (productDefaultStep1.requestRepeatId !== null) {
            // เช็ค requestRepeatId ? เเก้ไขวัตถุประสงค์การนำเสนอได้ : เเก้ไขวัตถุประสงค์การนำเสนอไม่ได้
            this.changeRequestRepeatId = false;
          } else {
            this.changeRequestRepeatId = true;
          }
          console.log('10101010=>', this.changeRequestRepeatId);
          let agenda: string;
          let subAgenda: string;
          if (productDefaultStep1.agenda !== null) {
            if (
              productDefaultStep1.agenda['normal'] === true &&
              productDefaultStep1.agenda['additional'] === false
            ) {
              agenda = 'true';
            } else if (
              productDefaultStep1.agenda['normal'] === false &&
              productDefaultStep1.agenda['additional'] === true
            ) {
              agenda = 'false';
            } else {
              agenda = null;
            }

            if (agenda === 'false') {
              if (
                productDefaultStep1.agenda.additional_detail['befor'] === true &&
                productDefaultStep1.agenda.additional_detail['after'] === false
              ) {
                subAgenda = 'befor';
              } else if (
                productDefaultStep1.agenda.additional_detail['befor'] === false &&
                productDefaultStep1.agenda.additional_detail['after'] === true
              ) {
                subAgenda = 'after';
              } else if (
                productDefaultStep1.agenda.additional_detail['befor'] === false &&
                productDefaultStep1.agenda.additional_detail['after'] === false
              ) {
                subAgenda = 'non';
              }
            }
          } else {
            agenda = null;
            subAgenda = null;
          }
          // console.log(productDefaultStep1['name']);
          if (productDefaultStep1['name']) {
            this.newProduct = false;
          } else {
            this.newProduct = true;
          }

          // frontend <-- backend
          this.firstStepData = {
            validate: productDefaultStep1['validate'],
            applicationNumber: productDefaultStep1['no'],
            PresentType: productDefaultStep1['presentation'],
            agendaType: agenda,
            subAgendaType: subAgenda,
            presentPurpose: productDefaultStep1['objective'],
            productInfor: null,
            productsOffered: {
              productProgram: false,
              nonCredit: false,
              pricing: false,
              outsourcing: false,
              insourcing: false,
            },
            productName: productDefaultStep1['name'],
            productCode: productDefaultStep1['code'],
            productGroup: {
              creditProduct: false,
              CP_BusinessLoan: false,
              CP_PersonalLoan: false,
              CP_HousingLoan: false,
              CP_Etc: false,
              CP_Etc_digital: null,
              nonCreditProduct: false,
              NCP_nonCreditProduct: false,
              NCP_Etc: false,
              NCP_Etc_digital: null,
              insourcing: false,
              outsourcing: false,
            },
            serviceDelivery: {
              generalBranch: false,
              electronics: false,
              businessOffice: false,
              CBC: false,
              blockChain: false,
              callCenter: false,
              ATM: false,
              digitalChannels: false,
              detailDigitalChannels: null,
              nonBank: false,
              detailNonBank: null,
              etc: false,
              detailEtc: null,
            },
            expectedDate: productDefaultStep1['releaseDate'],
            lastReviewDate: productDefaultStep1['lastReviewDate'],
            nextReviewDate: productDefaultStep1['nextReviewDate'],
            requestRepeatName: productDefaultStep1['requestRepeatName'],
            requestRepeatId: productDefaultStep1['requestRepeatId']
          };
          // date
          this.releaseDate = productDefaultStep1['releaseDate'];
          this.lastReviewDate = productDefaultStep1['lastReviewDate'];
          this.nextReviewDate = productDefaultStep1['nextReviewDate'];

          // set productsOffered
          if (productDefaultStep1['productProgram'].length > 0) {
            this.firstStepData.productsOffered.productProgram =
              productDefaultStep1['productProgram'][0].flag;
            this.firstStepData.productsOffered.nonCredit =
              productDefaultStep1['productProgram'][1].flag;
            this.firstStepData.productsOffered.pricing =
              productDefaultStep1['productProgram'][2].flag;
            this.firstStepData.productsOffered.outsourcing =
              productDefaultStep1['productProgram'][3].flag;
            this.firstStepData.productsOffered.insourcing =
              productDefaultStep1['productProgram'][4].flag;
          }

          // set productGroup
          if (productDefaultStep1['group'] !== null) {
            this.firstStepData.productGroup.creditProduct =
              productDefaultStep1['group'].credit_product['flag'];
            this.firstStepData.productGroup.CP_BusinessLoan =
              productDefaultStep1['group'].credit_product[
                'child'
              ].business_loan;
            this.firstStepData.productGroup.CP_PersonalLoan =
              productDefaultStep1['group'].credit_product[
                'child'
              ].persenal_loan;
            this.firstStepData.productGroup.CP_HousingLoan =
              productDefaultStep1['group'].credit_product['child'].housing_loan;
            this.firstStepData.productGroup.CP_Etc =
              productDefaultStep1['group'].credit_product['child'].other;
            this.firstStepData.productGroup.CP_Etc_digital =
              productDefaultStep1['group'].credit_product['child'].other_detail;
            this.firstStepData.productGroup.nonCreditProduct =
              productDefaultStep1['group'].non_credit_product['flag'];
            // tslint:disable-next-line:max-line-length
            this.firstStepData.productGroup.NCP_nonCreditProduct =
              productDefaultStep1['group'].non_credit_product[
                'child'
              ].non_credit_product;
            this.firstStepData.productGroup.NCP_Etc =
              productDefaultStep1['group'].non_credit_product['child'].other;
            this.firstStepData.productGroup.NCP_Etc_digital =
              productDefaultStep1['group'].non_credit_product[
                'child'
              ].other_detail;
            this.firstStepData.productGroup.insourcing =
              productDefaultStep1['group'].insousecing['flag'];
            this.firstStepData.productGroup.outsourcing =
              productDefaultStep1['group'].outsoucing['flag'];
          }

          // set serviceDelivery
          if (productDefaultStep1['channel'].length > 0) {
            this.firstStepData.serviceDelivery.generalBranch =
              productDefaultStep1['channel'][0].flag;
            this.firstStepData.serviceDelivery.electronics =
              productDefaultStep1['channel'][1].flag;
            this.firstStepData.serviceDelivery.businessOffice =
              productDefaultStep1['channel'][2].flag;
            this.firstStepData.serviceDelivery.CBC =
              productDefaultStep1['channel'][3].flag;
            this.firstStepData.serviceDelivery.blockChain =
              productDefaultStep1['channel'][4].flag;
            this.firstStepData.serviceDelivery.callCenter =
              productDefaultStep1['channel'][5].flag;
            this.firstStepData.serviceDelivery.ATM =
              productDefaultStep1['channel'][6].flag;
            this.firstStepData.serviceDelivery.digitalChannels =
              productDefaultStep1['channel'][7].flag;
            this.firstStepData.serviceDelivery.detailDigitalChannels =
              productDefaultStep1['digitalDetail'];
            this.firstStepData.serviceDelivery.nonBank =
              productDefaultStep1['channel'][8].flag;
            this.firstStepData.serviceDelivery.detailNonBank =
              productDefaultStep1['anotherDetail'];
            this.firstStepData.serviceDelivery.etc =
              productDefaultStep1['channel'][9].flag;
            this.firstStepData.serviceDelivery.detailEtc =
              productDefaultStep1['otherDetail'];
          }

          // set productsOffered
          console.log('productDefaultStep1', productDefaultStep1['productProgram'].length);
          if (productDefaultStep1['productProgram'].length > 0) {
          }
          this.listDate = [];
          this.dataChange = false;
        }
      });
    this.getSummaryList(this.getSummaryListstart);
  }

  changeDate(date: any) {
    const SendI = this.Date.changeDate(date);
    this.changeSaveDraft();
    return SendI;
  }


  // changeDate(date: any, id) {
  //   if (id === 1) {
  //     const SendI = this.Date.changeDate(date);
  //     this.firstStepData.expectedDate = SendI;
  //     console.log('id1 =>',  this.firstStepData.expectedDate);
  //     return SendI;
  //   } else if (id === 2) {
  //     const SendI = this.Date.changeDate(date);
  //     this.firstStepData.lastReviewDate = SendI;
  //     console.log('id2 =>',  this.firstStepData.lastReviewDate);
  //     return SendI;
  //   } else if (id === 3) {
  //     const SendI = this.Date.changeDate(date);
  //     this.firstStepData.nextReviewDate = SendI;
  //     console.log('id3 =>',  this.firstStepData.nextReviewDate);
  //     return SendI;
  //   }
  // }

  upDateDataFirstStep(documentId, action, nextPage?) {
    console.log('upDateDataFirstStep', documentId);
    this.status_loading = true;
    const countSuccess = 0;
    let statusSuccess = false;
    let statusPresentType = false;
    let statusAgendaType = false;
    let statusAgendaType_select = false;
    let statusPresentPurpose = false;
    let statusProductsOffered = false;
    let statusProductGroup = false;
    let statusProductGroup_creditselect = false;
    let statusProductGroup_creditProduct = true;
    let statusProductGroup_noncreditselect = false;
    let statusProductGroup_nonCreditProduct = true;
    let statusServiceDelivery = false;
    let statusServiceDelivery_etailDigitalChannels = true;
    let statusServiceDelivery_detailNonBank = true;
    let statusServiceDelivery_detailEtc = true;
    let statusExpectedDate = false;
    let statusNameProduct = false;
    let statusGroup = false;


    // สอบทาน
    let statusLastReviewDate = false;
    let statusNextReviewDate = false;

    let agendaType_normal = null;
    let agendaType_additional = null;
    let agendaType_sub_befor = null;
    let agendaType_sub_after = null;

    this.alerts = false;

    // = PresentType (ประเภทการนำเสนอ)
    if (this.firstStepData.PresentType) {
      statusPresentType = true;
    }

    // = agendaType (ประเภทวาระ)
    if (this.firstStepData.agendaType === 'true' || this.firstStepData.agendaType === 'false') {
      statusAgendaType = true;
      if (this.firstStepData.agendaType === 'true') {
        agendaType_normal = true;
        agendaType_additional = false;
        agendaType_sub_befor = false;
        agendaType_sub_after = false;
        statusAgendaType_select = true;
      } else if (this.firstStepData.agendaType === 'false') {
        statusAgendaType = true;
        agendaType_normal = false;
        agendaType_additional = true;
        if (
          this.firstStepData.subAgendaType === 'after' ||
          this.firstStepData.subAgendaType === 'befor' ||
          this.firstStepData.subAgendaType === 'non'
        ) {
          if (this.firstStepData.subAgendaType === 'after') {
            agendaType_sub_befor = false;
            agendaType_sub_after = true;
            statusAgendaType_select = true;
          } else if (this.firstStepData.subAgendaType === 'befor') {
            agendaType_sub_befor = true;
            agendaType_sub_after = false;
            statusAgendaType_select = true;
          } else if (this.firstStepData.subAgendaType === 'non') {
            agendaType_sub_befor = false;
            agendaType_sub_after = false;
            statusAgendaType_select = true;
          }
        }
      } else {
        agendaType_normal = false;
        agendaType_additional = false;
        agendaType_sub_befor = false;
        agendaType_sub_after = false;
      }
    }

    if (
      this.firstStepData.agendaType === 'false' &&
      statusAgendaType_select === false
    ) {
      this.status_loading = false;
      this.alerts = true;
      alert('หัวข้อประเภทวาระ กรณีที่เลือก "วาระเพิ่มเติม" : กรุณาเลือกตัวเลือกในกรอบสีเทา');
    }

    // = presentPurpose (วัตถุประสงค์การนำเสนอ) ** ยังไมเสร็จดี ขาดตรวจสอบ เรียกดูข้อมูลผลิตภัณฑ์
    if (this.template_manage.summary === 'show') {
      this.firstStepData.productInfor = this.firstStepData.requestRepeatName;
    }
    if (this.firstStepData.presentPurpose) {
      statusPresentPurpose = true;
    }

    // = productsOffered (ผลิตภัณฑ์ที่นำเสนออยู่ภายใต้การกำกับของคณะกรรมการผลิตภัณฑ์)
    if (this.firstStepData.productsOffered.productProgram === true) {
      statusProductsOffered = true;
    }
    if (this.firstStepData.productsOffered.nonCredit === true) {
      statusProductsOffered = true;
    }
    if (this.firstStepData.productsOffered.pricing === true) {
      statusProductsOffered = true;
    }
    if (this.firstStepData.productsOffered.outsourcing === true) {
      statusProductsOffered = true;
    }
    if (this.firstStepData.productsOffered.insourcing === true) {
      statusProductsOffered = true;
    }

    // = productGroup (กลุ่มผลิตภัณฑ์)
    // - creditProduct
    if (this.firstStepData.productGroup.creditProduct === true) {
      statusProductGroup = true;
      if (this.firstStepData.productGroup.CP_BusinessLoan === true) {
        statusProductGroup_creditselect = true;
      }
      if (this.firstStepData.productGroup.CP_PersonalLoan === true) {
        statusProductGroup_creditselect = true;
      }
      if (this.firstStepData.productGroup.CP_HousingLoan === true) {
        statusProductGroup_creditselect = true;
      }
      if (this.firstStepData.productGroup.CP_Etc === true) {
        statusProductGroup_creditselect = true;
        if (!this.firstStepData.productGroup.CP_Etc_digital) {
          statusProductGroup_creditProduct = false;
        }
      }
      // + Alert Credit Product
      if (statusProductGroup_creditselect === false) {
        this.status_loading = false;
        this.alerts = true;
        alert('หัวข้อกลุ่มผลิตภัณฑ์ กรณีที่เลือก "Credit Product" : กรุณาเลือกตัวเลือกในกรอบสีเทา');
      } else if (
        statusProductGroup_creditselect === true &&
        statusProductGroup_creditProduct === false
      ) {
        this.status_loading = false;
        this.alerts = true;
        alert(
          'หัวข้อกลุ่มผลิตภัณฑ์ กรณีที่เลือก "Credit Product -> อื่นๆ" : กรุณาระบุรายละเอียดเพิ่มเติม'
        );
      }
    } else if (this.firstStepData.productGroup.creditProduct === false) {
      statusProductGroup_creditselect = true;
    }
    // - creditProduct
    if (this.firstStepData.productGroup.nonCreditProduct === true) {
      statusProductGroup = true;
      if (this.firstStepData.productGroup.NCP_nonCreditProduct === true) {
        statusProductGroup_noncreditselect = true;
      }
      if (this.firstStepData.productGroup.NCP_Etc === true) {
        statusProductGroup_noncreditselect = true;
        if (!this.firstStepData.productGroup.NCP_Etc_digital) {
          statusProductGroup_nonCreditProduct = false;
        }
      }
      // + Alert Non-Credit Product
      if (statusProductGroup_noncreditselect === false) {
        this.status_loading = false;
        this.alerts = true;
        alert(
          'หัวข้อกลุ่มผลิตภัณฑ์ กรณีที่เลือก "Non-Credit Product" : กรุณาเลือกตัวเลือกในกรอบสีเทา'
        );
      } else if (
        statusProductGroup_noncreditselect === true &&
        statusProductGroup_nonCreditProduct === false
      ) {
        this.status_loading = false;
        this.alerts = true;
        alert(
          'หัวข้อกลุ่มผลิตภัณฑ์ กรณีที่เลือก "Non-Credit Product -> อื่นๆ" : กรุณาระบุรายละเอียดเพิ่มเติม'
        );
      }
    } else if (this.firstStepData.productGroup.nonCreditProduct === false) {
      statusProductGroup_noncreditselect = true;
    }
    // - insourcing
    if (this.firstStepData.productGroup.insourcing === true) {
      statusProductGroup = true;
    }
    // - outsourcing
    if (this.firstStepData.productGroup.outsourcing === true) {
      statusProductGroup = true;
    }

    // = serviceDelivery (ช่องทางนำเสนอบริการ)
    if (this.firstStepData.serviceDelivery.generalBranch === true) {
      statusServiceDelivery = true;
    }
    if (this.firstStepData.serviceDelivery.electronics === true) {
      statusServiceDelivery = true;
    }
    if (this.firstStepData.serviceDelivery.businessOffice === true) {
      statusServiceDelivery = true;
    }
    if (this.firstStepData.serviceDelivery.CBC === true) {
      statusServiceDelivery = true;
    }
    if (this.firstStepData.serviceDelivery.blockChain === true) {
      statusServiceDelivery = true;
    }
    if (this.firstStepData.serviceDelivery.callCenter === true) {
      statusServiceDelivery = true;
    }
    if (this.firstStepData.serviceDelivery.ATM === true) {
      statusServiceDelivery = true;
    }
    if (this.firstStepData.serviceDelivery.digitalChannels === true) {
      statusServiceDelivery = true;
      if (!this.firstStepData.serviceDelivery.detailDigitalChannels) {
        statusServiceDelivery_etailDigitalChannels = false;
      }
    }
    if (this.firstStepData.serviceDelivery.nonBank === true) {
      statusServiceDelivery = true;
      if (!this.firstStepData.serviceDelivery.detailNonBank) {
        statusServiceDelivery_detailNonBank = false;
      }
    }
    if (this.firstStepData.serviceDelivery.etc === true) {
      statusServiceDelivery = true;
      if (!this.firstStepData.serviceDelivery.detailEtc) {
        statusServiceDelivery_detailEtc = false;
      }
    }

    // + Alert ServiceDelivery
    if (
      this.firstStepData.serviceDelivery.digitalChannels === true &&
      statusServiceDelivery_etailDigitalChannels === false
    ) {
      this.status_loading = false;
      this.alerts = true;
      alert(
        'หัวข้อช่องทางนำเสนอ: กรุณาระบุรายละเอียดเพิ่มเติมกรณีเลือกตัวเลือก "ช่องทางดิจิทัล"'
      );
    }
    if (
      this.firstStepData.serviceDelivery.nonBank === true &&
      statusServiceDelivery_detailNonBank === false
    ) {
      this.status_loading = false;
      this.alerts = true;
      alert(
        'หัวข้อช่องทางนำเสนอ: กรุณาระบุรายละเอียดเพิ่มเติมกรณีเลือกตัวเลือก "ช่องทางอื่นที่ไม่ใช่ของธนาคาร"'
      );
    }
    if (
      this.firstStepData.serviceDelivery.etc === true &&
      statusServiceDelivery_detailEtc === false
    ) {
      this.status_loading = false;
      this.alerts = true;
      alert(
        'หัวข้อช่องทางนำเสนอ: กรุณาระบุรายละเอียดเพิ่มเติมกรณีเลือกตัวเลือก "อื่นๆ"'
      );
    }

    // = expectedDate (วันที่คาดว่าจะประกาศใช้)
    console.log('this.firstStepData.expectedDate 0001 5555', this.firstStepData.expectedDate);
    if (this.firstStepData.expectedDate) {
      statusExpectedDate = true;
    } else {
      this.firstStepData.expectedDate = null;
      statusExpectedDate = false;
    }

    // if (this.template_manage.summary === 'show') {
    if (this.firstStepData.lastReviewDate) {
      statusLastReviewDate = true;
    } else {
      statusLastReviewDate = true;
    }
    if (this.firstStepData.nextReviewDate) {
      statusNextReviewDate = true;
    } else {
      statusNextReviewDate = false;
    }

    // } else {
    //   statusLastReviewDate = true;
    //   statusNextReviewDate = true;
    // }


    if (this.firstStepData.productName) {
      statusNameProduct = true;
    }



    // backend <-- frontend
    const data = {
      id: null,
      no: this.firstStepData.applicationNumber,
      presentation: this.firstStepData.PresentType,
      agenda: {
        normal: agendaType_normal,
        additional_detail: {
          befor: agendaType_sub_befor,
          after: agendaType_sub_after,
        },
        additional: agendaType_additional,
      },
      objective: this.firstStepData.presentPurpose,
      productProgram: [
        {
          topic: 'ออก/ทบทวนผลิตภัณฑ์สินเชื่อที่เป็น Product Program',
          flag: this.firstStepData.productsOffered.productProgram,
        },
        {
          topic:
            'ออก/ทบทวนผลิตภัณฑ์ทางอิเล็กทรอนิกส์และผลิตภัณฑ์อื่นๆ (Non Credit)',
          flag: this.firstStepData.productsOffered.nonCredit,
        },
        {
          topic: 'ออก/ทบทวนหลักเกณฑ์......... Pricingแก่ลูกค้า',
          flag: this.firstStepData.productsOffered.pricing,
        },
        {
          topic: 'การใช้บริการจากผู้ให้บริการภายนอก (Outsourcing)',
          flag: this.firstStepData.productsOffered.outsourcing,
        },
        {
          topic: 'การใช้บริการทางการเงินหรือทางการ (Insourcing)',
          flag: this.firstStepData.productsOffered.insourcing,
        },
      ],
      name: this.firstStepData.productName,
      code: this.firstStepData.productCode,
      group: {
        credit_product: {
          flag: this.firstStepData.productGroup.creditProduct,
          child: {
            business_loan: this.firstStepData.productGroup.CP_BusinessLoan,
            persenal_loan: this.firstStepData.productGroup.CP_PersonalLoan,
            housing_loan: this.firstStepData.productGroup.CP_HousingLoan,
            other: this.firstStepData.productGroup.CP_Etc,
            other_detail: this.firstStepData.productGroup.CP_Etc_digital,
          },
        },
        non_credit_product: {
          flag: this.firstStepData.productGroup.nonCreditProduct,
          child: {
            non_credit_product: this.firstStepData.productGroup
              .NCP_nonCreditProduct,
            other: this.firstStepData.productGroup.NCP_Etc,
            other_detail: this.firstStepData.productGroup.NCP_Etc_digital,
          },
        },
        insousecing: {
          flag: this.firstStepData.productGroup.insourcing,
        },
        outsoucing: {
          flag: this.firstStepData.productGroup.outsourcing,
        },
      },
      channel: [
        {
          topic: 'สาขาทั่วไป',
          flag: this.firstStepData.serviceDelivery.generalBranch,
        },
        {
          topic: 'สาขาอิเล็กทรอนิกส์',
          flag: this.firstStepData.serviceDelivery.electronics,
        },
        {
          topic: 'สำนักงานธุรกิจ',
          flag: this.firstStepData.serviceDelivery.businessOffice,
        },
        {
          topic: 'CBC',
          flag: this.firstStepData.serviceDelivery.CBC,
        },
        {
          topic: 'Blockchain',
          flag: this.firstStepData.serviceDelivery.blockChain,
        },
        {
          topic: 'Call Center',
          flag: this.firstStepData.serviceDelivery.callCenter,
        },
        {
          topic: 'ATM',
          flag: this.firstStepData.serviceDelivery.ATM,
        },
        {
          topic: 'ช่องทางดิจิทัล',
          flag: this.firstStepData.serviceDelivery.digitalChannels,
        },
        {
          topic: 'ช่องทางอื่นที่ไม่ใช่ของธนาคาร',
          flag: this.firstStepData.serviceDelivery.nonBank,
        },
        {
          topic: 'อื่นๆ',
          flag: this.firstStepData.serviceDelivery.etc,
        },
      ],
      digitalDetail: this.firstStepData.serviceDelivery.detailDigitalChannels,
      anotherDetail: this.firstStepData.serviceDelivery.detailNonBank,
      otherDetail: this.firstStepData.serviceDelivery.detailEtc,
      releaseDate: this.firstStepData.expectedDate,
      lastReviewDate: this.firstStepData.lastReviewDate,
      nextReviewDate: this.firstStepData.nextReviewDate,
      status: statusSuccess,
    };
    if (localStorage.getItem('requestId')) {
      data.id = localStorage.getItem('requestId');
    }

    // console.log('this.firstStepData.expectedDate 0001=>', this.firstStepData.expectedDate);
    // console.log('this.firstStepData.lastReviewDate 0002=>', this.firstStepData.lastReviewDate);
    // console.log('this.firstStepData.nextReviewDate 0003=>', this.firstStepData.nextReviewDate);

    // if (this.template_manage.summary === 'show') {
    //   if (this.firstStepData.lastReviewDate) {
    //     data.lastReviewDate = moment(
    //       this.firstStepData.lastReviewDate
    //     )
    //       .format()
    //       .substring(0, 10);
    //   } else {
    //     data.lastReviewDate = null;
    //   }
    //   if (this.firstStepData.nextReviewDate) {
    //     data.nextReviewDate = moment(
    //       this.firstStepData.nextReviewDate
    //     )
    //       .format()
    //       .substring(0, 10);
    //   } else {
    //     data.nextReviewDate = null;
    //   }
    // }

    // ===================  Send API =============================
    console.log('data 0005=>', data);
    // --------check credit/non-credit--------
    if (data.group.credit_product.flag === false && data.group.non_credit_product.flag === false) {
      statusGroup = true;
    } else {
      if (data.group.credit_product.child.business_loan === false &&
        data.group.credit_product.child.persenal_loan === false &&
        data.group.credit_product.child.housing_loan === false &&
        data.group.credit_product.child.other === false) {
        if (data.group.non_credit_product.child.non_credit_product === false &&
          data.group.non_credit_product.child.other === false) {
          statusGroup = false;
        } else {
          statusGroup = true;
        }
      } else {
        statusGroup = true;
      }

    }

    // console.log('0001 statusPresentType', statusPresentType);
    // console.log('0002 statusAgendaType', statusAgendaType);
    // console.log('0003 statusAgendaType_select', statusAgendaType_select);
    // console.log('0004 statusPresentPurpose', statusPresentPurpose);
    // console.log('0005 statusProductsOffered', statusProductsOffered);
    // console.log('0006 statusProductGroup', statusProductGroup);
    // console.log('0007 statusProductGroup_creditProduct', statusProductGroup_creditProduct);
    // console.log('0008 statusProductGroup_creditselect', statusProductGroup_creditselect);
    // console.log('0009 statusProductGroup_nonCreditProduct', statusProductGroup_nonCreditProduct);
    // console.log('0010 statusProductGroup_noncreditselect', statusProductGroup_noncreditselect);
    // console.log('0011 statusServiceDelivery', statusServiceDelivery);
    // console.log('0012 statusServiceDelivery_etailDigitalChannels', statusServiceDelivery_etailDigitalChannels);
    // console.log('0013 statusServiceDelivery_detailNonBank', statusServiceDelivery_detailNonBank);
    // console.log('0014 statusServiceDelivery_detailEtc', statusServiceDelivery_detailEtc);
    // console.log('0015 statusExpectedDate', statusExpectedDate);
    // console.log('0016 statusLastReviewDate', statusLastReviewDate);
    // console.log('0017 statusNextReviewDate', statusNextReviewDate);
    // console.log('0018 statusNameProduct', statusNameProduct);
    // console.log('0019 statusGroup', statusGroup);



    if (
      statusPresentType === true &&
      statusAgendaType === true &&
      statusAgendaType_select === true &&
      statusPresentPurpose === true &&
      statusProductsOffered === true &&
      statusProductGroup === true &&
      statusProductGroup_creditProduct === true &&
      statusProductGroup_creditselect === true &&
      statusProductGroup_nonCreditProduct === true &&
      statusProductGroup_noncreditselect === true &&
      statusServiceDelivery === true &&
      statusServiceDelivery_etailDigitalChannels === true &&
      statusServiceDelivery_detailNonBank === true &&
      statusServiceDelivery_detailEtc === true &&
      statusExpectedDate === true &&
      statusLastReviewDate === true &&
      statusNextReviewDate === true &&
      statusNameProduct === true &&
      statusGroup === true
    ) {
      statusSuccess = true;
      data.status = true;
    } else {
      statusSuccess = false;
      data.status = false;
    }
    this.checkDataSuccess = statusSuccess;
    console.log('statusdata.group=>', data.status);
    const returnUpdateData = new Subject<any>();
    let returnData = null;
    if (this.alerts === false) {
      this.productDefultFirstStep.upDateDefultFirstStep(data).subscribe(
        (res) => {
          if (res['status'] === 'success') {
            this.status_loading = false;
            localStorage.setItem('requestId', res['data'].id);
            if (localStorage.getItem('requestId')) {
              this.requestId = localStorage.getItem('requestId');
              this.GetDataFirstStep(localStorage.getItem('requestId'));
              this.sidebarService.sendEvent();
              this.sidebarService.inPageStatus(false);
              this.saveDraftstatus = true;
              const dataOut = this.sidebarService.getsavePage();
              this.linkTopage = dataOut.goToLink;
              returnUpdateData.next(true);
              returnData = true;
              console.log('444 action', action);
              console.log('555 this.pageAction', this.pageAction);
              if (action === 'save') {
                  console.log('000000000000000255', this.outOfPageSave);
                  console.log('000000000000000256', this.linkTopage);
                if (res['data'].objective === 'ทบทวนผลิตภัณฑ์ระหว่างปี' || res['data'].objective === 'ทบทวนผลิตภัณฑ์ประจำปี') {
                  // if (atrout === '/request/information-base/step1') {
                    if (this.linkTopage === '/request/information-base/step1') {
                      this.router.navigate(['/request-summary/information-base/step1']);
                    } else if (this.linkTopage === '/request/information-base/step2') {
                      this.router.navigate(['/request-summary/information-base/step2']);
                    } else if (this.linkTopage === '/request/information-base/step3') {
                      this.router.navigate(['/request-summary/information-base/step3']);
                    } else if (this.linkTopage === '/request/information-base/step4') {
                      this.router.navigate(['/request-summary/information-base/step4']);
                    } else {
                      if (this.linkTopage != null) {
                        const search_At_summary  = this.linkTopage.slice(0, 17);
                        if (search_At_summary === '/request-summary/') {
                          this.router.navigate([this.linkTopage]);
                        } else {
                          const search = this.linkTopage.search('request');
                          const linkSlice = this.linkTopage.slice(8);
                          const link = '/request-summary' + linkSlice;
                          if (search === 1) {
                            this.router.navigate([link]);
                          } else {
                            this.router.navigate([this.linkTopage]);
                          }
                        }
                      } else {
                        this.router.navigate(['/request-summary/information-base/step1']);
                      }
                    }

                  // }
                } else {
                  // if (atrout === '/request-summary/information-base/step1') {
                    if (this.linkTopage === '/request-summary/information-base/step1') {
                      this.router.navigate(['/request/information-base/step1']);
                    } else if (this.linkTopage === '/request-summary/information-base/step2') {
                      this.router.navigate(['/request/information-base/step2']);
                    } else if (this.linkTopage === '/request-summary/information-base/step3') {
                      this.router.navigate(['/request/information-base/step3']);
                    } else if (this.linkTopage === '/request-summary/information-base/step4') {
                      this.router.navigate(['/request/information-base/step4']);
                    } else {
                      if (this.linkTopage != null) {
                        const search_At_request  = this.linkTopage.slice(0, 9);
                        if (search_At_request === '/request/') {
                          this.router.navigate([this.linkTopage]);
                        } else {
                          const search = this.linkTopage.search('request');
                          if (this.linkTopage !== '/request-summary/product-detail/step13'
                          && this.linkTopage !== '/request-summary/product-detail/step14') {
                            const linkSlice = this.linkTopage.slice(16);
                            const link = '/request' + linkSlice;
                            if (search === 1) {
                              this.router.navigate([link]);
                            } else {
                              this.router.navigate([this.linkTopage]);
                            }
                          } else {
                            this.router.navigate(['/request/information-base/step1']);
                          }
                        }
                      } else {
                        if (this.pageAction === 'next') {
                          this.router.navigate(['/request/information-base/step2']);
                        } else {
                          this.router.navigate(['/request/information-base/step1']);
                        }
                      }
                    }
                  //   console.log('0000000000000002542');
                  // }

                }
              } else if (action === 'next') {
                if (res['data'].objective === 'ทบทวนผลิตภัณฑ์ระหว่างปี' || res['data'].objective === 'ทบทวนผลิตภัณฑ์ประจำปี') {
                  this.router.navigate(['request-summary/information-base/step2']);
                } else {
                  this.router.navigate(['/request/information-base/step2']);
                }
              }
            }
            console.log('999', this.sidebarService.outPageStatus());
            setTimeout(() => {
              this.saveDraftstatus = null;
            }, 3000);
          } else {
            this.status_loading = false;
            alert(res['message']);
            returnUpdateData.next(false);
          }
        },
        (err) => {
          this.status_loading = false;
          alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
          console.log('send API err', err);
          returnUpdateData.next(false);
        });
      console.log('returnUpdateData', returnUpdateData);
      console.log('returnData', returnData);
      return returnUpdateData;
      // return true;
    } else {
      this.sidebarService.resetPageInfo();
      return false;
    }

  }

  constructor(
    private router: Router,
    public dialog: MatDialog,
    private productDefultFirstStep: ProductDefultFirstStepService,
    private sidebarService: RequestService,
    private adapter: DateAdapter<any>,
    private http: HttpClient,
  ) {
    this.adapter.setLocale('th-TH');
  }
  async ngOnInit() {
    window.scrollTo(0, 0);
    console.log('55555555555555555555555555555555555555555555555555');
    this.openDoCheckc = true;
    console.log('55555 this.sidebarService.outPageStatus();', this.sidebarService.outPageStatus());
    console.log('template_manage', this.template_manage);
    this.setupCalendar(0);
    this.setupCalendar(1);
    this.setupCalendar(2);
    if (localStorage) {
      if (localStorage.getItem('requestId') !== null
        && localStorage.getItem('requestId') !== ''
        && Number(localStorage.getItem('requestId')) !== 0) {
        this.requestId = localStorage.getItem('requestId');
        this.GetDataFirstStep(this.requestId);
        this.sidebarService.sendEvent();
      } else {
        this.requestId = null;
        this.status_loading = false;
      }
      this.sidebarService.checkDefault(localStorage.getItem('requestId')).subscribe(page => {
        this.pageStatus = page['data']
      })
    } else {
      console.log('Brownser not support');
      this.status_loading = false;
    }
  }

  viewNext() {
    this.router.navigate(['request/information-base/step2']);
  }


  onSelectStart(event, calendarIndex) {
    console.log(event);
    this.dateRange[calendarIndex].selectedStartDate = event;
    this.dateRange[calendarIndex].modifiedStartDate = this.dateToBuddha(event);
    console.log(this.dateRange[calendarIndex].modifiedStartDate);

    this.dateRange[calendarIndex].minEndDate = this.dateRange[calendarIndex].selectedStartDate;


    console.log('dd', this.dateRange[calendarIndex].minEndDate);

    if (calendarIndex === 0) {
      this.model = {
        isRange: false,
        singleDate: {
          jsDate: new Date(this.dateRange[calendarIndex].minEndDate),
        },
      };
    } else if (calendarIndex === 1) {
      this.model2 = {
        isRange: false,
        singleDate: {
          jsDate: new Date(this.dateRange[calendarIndex].minEndDate),
        },
      };
    } else if (calendarIndex === 2) {
      this.model3 = {
        isRange: false,
        singleDate: {
          jsDate: new Date(this.dateRange[calendarIndex].minEndDate),
        },
      };
    }

  }
  input_date(calendarIndex) {
    console.log('date:', this.dateRange[calendarIndex].selectedDate);
  }
  openCalendar(calendarIndex) {
    if (calendarIndex === 0) {
      this.dateRange[0].calendarState = true;
      this.dateRange[1].calendarState = false;
      this.dateRange[2].calendarState = false;
    }
    if (calendarIndex === 1) {
      this.dateRange[0].calendarState = false;
      this.dateRange[1].calendarState = true;
      this.dateRange[2].calendarState = false;
    }
    if (calendarIndex === 2) {
      this.dateRange[0].calendarState = false;
      this.dateRange[1].calendarState = false;
      this.dateRange[2].calendarState = true;
    }

    this.dateRange[calendarIndex].calendarState = true;
    // check if already have value selected.Need to transform to normal format
    if (this.dateRange[calendarIndex].selectedDate) {
      // dd/mm/yyyy -> yyyy-mm-dd
      const splitData = this.dateRange[calendarIndex].selectedDate.replace(/\s/g, '').split('-');
      const convertYear = splitData.map(d => {
        return `${(+d.split('/')[2] - 543)}-${d.split('/')[1]}-${d.split('/')[0]}`;
      });

      // set current date when open calendar
      this.dateRange[calendarIndex].selectedStartDate = new Date(convertYear[0]);
      // this.dateRange[calendarIndex].selectedEndDate = new Date(convertYear[1]);
      this.dateRange[calendarIndex].modifiedStartDate = splitData[0];
      // this.dateRange[calendarIndex].modifiedEndDate = splitData[1];
    }
  }
  setupCalendar(calendarIndex, option?) {
    // set min and max date : 'minStartDate' , 'maxStartDate' , 'minEndDate' , 'maxEndDate'
    // set defalut ngModule date : 'selectedDate' , 'selectedStartDate' , 'selectedEndDate'
    // buddha year : 'modifiedStartDate' 'modifiedEndDate'
    // set state calendar show or not : 'calendarState'
    // 'DKKs' Author
    // *** example option ***
    const templateCalendar = {
      minStartDate: new Date(),
      maxStartDate: undefined,
      // minEndDate: new Date(1900, 0, 1),
      // maxEndDate: undefined,
      selectedDate: '',
      selectedStartDate: new Date(),
      // selectedEndDate: new Date(),
      modifiedStartDate: this.dateToBuddha(new Date()),
      // modifiedEndDate: this.dateToBuddha(new Date()),
      calendarState: false
    };

    this.dateRange[calendarIndex] = option ? option : templateCalendar;

  }
  getLogUserSearchData(calendarIndex) {
    if (this.dateRange[calendarIndex].selectedDate !== '') {
      this.start_date = moment(this.dateRange[calendarIndex].selectedStartDate).format('YYYY-MM-DD');

    } else {
      this.start_date = '';
      console.log('null');
    }
  }
  closeCalendar(calendarIndex) {


    if (calendarIndex === 0) {
      this.model = null;
      this.dateRange[calendarIndex].calendarState = false;
      // this.dateRange[calendarIndex].selectedDate = null;

    } else if (calendarIndex === 1) {
      this.model2 = null;
      this.dateRange[calendarIndex].calendarState = false;
      // this.dateRange[calendarIndex].selectedDate = null;

    } else if (calendarIndex === 2) {
      this.model3 = null;
      this.dateRange[calendarIndex].calendarState = false;
      // this.dateRange[calendarIndex].selectedDate = null;

    }

  }

  clearCalendar(calendarIndex) {
    this.setupCalendar(calendarIndex);
  }

  applyCalendar(calendarIndex) {
    // this.dateRange[calendarIndex].selectedDate
    // = `${this.dateRange[calendarIndex].modifiedStartDate} - ${this.dateRange[calendarIndex].modifiedEndDate}`;

    this.dateRange[calendarIndex].selectedDate = `${this.dateRange[calendarIndex].modifiedStartDate}`;

    this.dateRange[calendarIndex].calendarState = false;
    this.getLogUserSearchData(calendarIndex);
  }

  dateToBuddha(dateNormal) {
    console.log('dateNormal', dateNormal);
    const now = moment(dateNormal);
    now.locale('th');
    const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();
    return now.format('DD/MM') + '/' + buddhishYear;
  }


  getSummaryList(start?) {
    console.log('getSummaryList');
    this.productDefultFirstStep.getSummaryRequestList().subscribe(res => {
      console.log('getSummaryList => ', res['data']);
      if (res['status'] === 'success') {
        this.getSummary = res['data'].requestRepeatList;
        this.summaryList = this.getSummary;
        this.requestRepeatList = this.control.valueChanges.pipe(startWith(''),
          map(state => state ? this._filterStates(state) : this.summaryList.slice())
        );
      }
    });
    if (this.template_manage.summary !== 'show') {
      if (start === true) {
        this.getSummaryListstart = false;
      } else {
        this.changeSaveDraft();
      }
    }
  }

  fillSearch() {
    console.log('fillSearch:', this.control.value);
    console.log('fillSearch:', this.requestRepeatList);
  }

  private _filterStates(value: string): StateSummary[] {
    const filterValue = this._normalizeValue(value);
    const data = this.summaryList.filter(state => this._normalizeValue(state.productName).includes(filterValue));
    console.log('00000', data);
    return data;
  }

  private _normalizeValue(value: string): string {
    return value.toLowerCase().replace(/\s/g, '');
  }

  summaryGenerate(requestId) {
    console.log('getSummaryList requestId ', requestId);
    if (requestId) {
      let confirmStatus = false;
      const dialogRef = this.dialog.open(ConfirmRequestSummaryModelsComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการทบทวนผลิตภัณฑ์',
          bodyDetail1: 'คุณแน่ใจหรือไม่? ว่าต้องการทบทวนผลิตภัณฑ์',
          bodyDetail2: this.firstStepData.productInfor,
          returnStatus: false
        }
      });
      dialogRef.afterClosed().subscribe(result => {
        confirmStatus = result.returnStatus;
        if (confirmStatus) {
          this.canDeactivateOpen = false;
          console.log('summaryGenerate');
          this.autocomplete.closePanel();
          this.status_loading = true;
          const data = {
            baseRequest: requestId,
            newRequest: null,
            objective: this.firstStepData.presentPurpose
          };
          if (localStorage.getItem('requestId') === null || localStorage.getItem('requestId') === '') {
            data.newRequest = null;
          } else {
            data.newRequest = localStorage.getItem('requestId');
          }
          this.productDefultFirstStep.repeatProduct(data).subscribe(res => {
            if (res['status'] === 'success') {
              localStorage.setItem('requestId', res['requestId']);
              this.productDefultFirstStep.getDefultFirstStep(res['requestId']).subscribe((ress) => {
                console.log('0000008', ress['data'].requestRepeatName);
                if (ress['data'].requestRepeatName !== null) {
                  this.firstStepData.requestRepeatName = ress['data'].requestRepeatName;
                  this.router.navigate(['request-summary/information-base/step1']);
                  const atrout = window.location.pathname;
                  if (atrout === '/request-summary/information-base/step1') {
                    this.GetDataFirstStep(this.requestId);
                  }
                  this.sidebarService.sendEvent();
                  this.status_loading = false;
                  this.sidebarService.inPageStatus(false);
                }
              });
              // this.firstStepData.requestRepeatName = 111;
            } else {
              this.status_loading = false;
              alert(res['message']);
            }
          });
        } else {
          this.autocomplete.closePanel();
          this.firstStepData.productInfor = null;
        }
      });
    }
  }

  checkListSummary() {
    console.log('checkListSummary');
    this.firstStepData.productInfor = null;
  }

  changePresentPurpose(id) {
    if (this.changeRequestRepeatId === true) {
      if (id === 1) {
        this.firstStepData.presentPurpose = 'ทบทวนผลิตภัณฑ์ระหว่างปี';
        this.firstStepData.productInfor = null;
        this.changeSaveDraft();
        this.getSummaryList(this.getSummaryListstart);
      } else if (id === 2) {
        this.firstStepData.presentPurpose = 'ทบทวนผลิตภัณฑ์ประจำปี';
        this.firstStepData.productInfor = null;
        this.changeSaveDraft();
        this.getSummaryList(this.getSummaryListstart);
      } else if (id === 3) {
        this.firstStepData.presentPurpose = 'ผลิตภัณฑ์ใหม่';
        this.firstStepData.productInfor = null;
        this.changeSaveDraft();
      }
    }
  }

  autogrow(index: any) {
    if (index === 1) {
      const textother1 = document.getElementById('textother1');
      textother1.style.overflow = 'hidden';
      textother1.style.height = 'auto';
      textother1.style.height = textother1.scrollHeight + 'px';
    }

    if (index === 2) {
      const textother2 = document.getElementById('textother2');
      textother2.style.overflow = 'hidden';
      textother2.style.height = 'auto';
      textother2.style.height = textother2.scrollHeight + 'px';
    }

    if (index === 3) {
      const textother3 = document.getElementById('textother3');
      textother3.style.overflow = 'hidden';
      textother3.style.height = 'auto';
      textother3.style.height = textother3.scrollHeight + 'px';
    }
    if (index === 4) {
      const textother4 = document.getElementById('textother4');
      textother4.style.overflow = 'hidden';
      textother4.style.height = 'auto';
      textother4.style.height = textother4.scrollHeight + 'px';
    }
    if (index === 5) {
      const textother5 = document.getElementById('textother5');
      textother5.style.overflow = 'hidden';
      textother5.style.height = 'auto';
      textother5.style.height = textother5.scrollHeight + 'px';
    }
  }

  dateFormats(date: any) {
    let dateForm = '';
    // if (Object.keys(date).includes('singleDate')) {
    dateForm = moment(date).format('DD/MM') + '/' + Number(Number(moment(date).format('YYYY')) + 543);
    // }
    return dateForm;
  }

  runNextpage() {
    console.log('runNextpage');
  }



  checkDataStatus() {
    let statusPresentType = false; //
    let statusAgendaType = false;
    let statusAgendaType_select = false;
    let statusPresentPurpose = false;
    let statusProductsOffered = false;
    let statusProductGroup = false;
    let statusProductGroup_creditselect = false;
    let statusProductGroup_creditProduct = true;
    let statusProductGroup_noncreditselect = false;
    let statusProductGroup_nonCreditProduct = true;
    let statusServiceDelivery = false;
    let statusServiceDelivery_etailDigitalChannels = true;
    let statusServiceDelivery_detailNonBank = true;
    let statusServiceDelivery_detailEtc = true;
    let statusExpectedDate = false;
    let statusNameProduct = false;
    let statusGroup = false;
    let statusGroupDetail = true;

    // สอบทาน
    let statusLastReviewDate = false;
    let statusNextReviewDate = false;

    let agendaType_normal = null;
    let agendaType_additional = null;
    let agendaType_sub_befor = null;
    let agendaType_sub_after = null;

    this.alerts = false;

    // = PresentType (ประเภทการนำเสนอ)
    if (this.firstStepData.PresentType) {
      statusPresentType = true;
    }

    // = agendaType (ประเภทวาระ)
    if (this.firstStepData.agendaType === 'true' || this.firstStepData.agendaType === 'false') {
      statusAgendaType = true;
      if (this.firstStepData.agendaType === 'true') {
        agendaType_normal = true;
        agendaType_additional = false;
        agendaType_sub_befor = false;
        agendaType_sub_after = false;
        statusAgendaType_select = true;
      } else if (this.firstStepData.agendaType === 'false') {
        statusAgendaType = true;
        agendaType_normal = false;
        agendaType_additional = true;
        if (
          this.firstStepData.subAgendaType === 'after' ||
          this.firstStepData.subAgendaType === 'befor' ||
          this.firstStepData.subAgendaType === 'non'
        ) {
          if (this.firstStepData.subAgendaType === 'after') {
            agendaType_sub_befor = false;
            agendaType_sub_after = true;
            statusAgendaType_select = true;
          } else if (this.firstStepData.subAgendaType === 'befor') {
            agendaType_sub_befor = true;
            agendaType_sub_after = false;
            statusAgendaType_select = true;
          } else if (this.firstStepData.subAgendaType === 'non') {
            agendaType_sub_befor = false;
            agendaType_sub_after = false;
            statusAgendaType_select = true;
          }
        }
      } else {
        agendaType_normal = false;
        agendaType_additional = false;
        agendaType_sub_befor = false;
        agendaType_sub_after = false;
      }
    }


    // = presentPurpose (วัตถุประสงค์การนำเสนอ) ** ยังไมเสร็จดี ขาดตรวจสอบ เรียกดูข้อมูลผลิตภัณฑ์
    if (this.template_manage.summary === 'show') {
      this.firstStepData.productInfor = this.firstStepData.requestRepeatName;
    }
    if (this.firstStepData.presentPurpose) {
      statusPresentPurpose = true;
    }

    // = productsOffered (ผลิตภัณฑ์ที่นำเสนออยู่ภายใต้การกำกับของคณะกรรมการผลิตภัณฑ์)
    if (this.firstStepData.productsOffered.productProgram === true) {
      statusProductsOffered = true;
    }
    if (this.firstStepData.productsOffered.nonCredit === true) {
      statusProductsOffered = true;
    }
    if (this.firstStepData.productsOffered.pricing === true) {
      statusProductsOffered = true;
    }
    if (this.firstStepData.productsOffered.outsourcing === true) {
      statusProductsOffered = true;
    }
    if (this.firstStepData.productsOffered.insourcing === true) {
      statusProductsOffered = true;
    }

    // = productGroup (กลุ่มผลิตภัณฑ์)
    // - creditProduct
    if (this.firstStepData.productGroup.creditProduct === true) {
      statusProductGroup = true;
      if (this.firstStepData.productGroup.CP_BusinessLoan === true) {
        statusProductGroup_creditselect = true;
      }
      if (this.firstStepData.productGroup.CP_PersonalLoan === true) {
        statusProductGroup_creditselect = true;
      }
      if (this.firstStepData.productGroup.CP_HousingLoan === true) {
        statusProductGroup_creditselect = true;
      }
      if (this.firstStepData.productGroup.CP_Etc === true) {
        statusProductGroup_creditselect = true;
        if (!this.firstStepData.productGroup.CP_Etc_digital) {
          statusProductGroup_creditProduct = false;
        }
      }

    } else if (this.firstStepData.productGroup.creditProduct === false) {
      statusProductGroup_creditselect = true;
    }

    // - creditProduct
    if (this.firstStepData.productGroup.nonCreditProduct === true) {
      statusProductGroup = true;
      if (this.firstStepData.productGroup.NCP_nonCreditProduct === true) {
        statusProductGroup_noncreditselect = true;
      }
      if (this.firstStepData.productGroup.NCP_Etc === true) {
        statusProductGroup_noncreditselect = true;
        if (!this.firstStepData.productGroup.NCP_Etc_digital) {
          statusProductGroup_nonCreditProduct = false;
        }
      }

    } else if (this.firstStepData.productGroup.nonCreditProduct === false) {
      statusProductGroup_noncreditselect = true;
    }
    // - insourcing
    if (this.firstStepData.productGroup.insourcing === true) {
      statusProductGroup = true;
    }
    // - outsourcing
    if (this.firstStepData.productGroup.outsourcing === true) {
      statusProductGroup = true;
    }

    // = serviceDelivery (ช่องทางนำเสนอบริการ)
    if (this.firstStepData.serviceDelivery.generalBranch === true) {
      statusServiceDelivery = true;
    }
    if (this.firstStepData.serviceDelivery.electronics === true) {
      statusServiceDelivery = true;
    }
    if (this.firstStepData.serviceDelivery.businessOffice === true) {
      statusServiceDelivery = true;
    }
    if (this.firstStepData.serviceDelivery.CBC === true) {
      statusServiceDelivery = true;
    }
    if (this.firstStepData.serviceDelivery.blockChain === true) {
      statusServiceDelivery = true;
    }
    if (this.firstStepData.serviceDelivery.callCenter === true) {
      statusServiceDelivery = true;
    }
    if (this.firstStepData.serviceDelivery.ATM === true) {
      statusServiceDelivery = true;
    }
    if (this.firstStepData.serviceDelivery.digitalChannels === true) {
      statusServiceDelivery = true;
      if (!this.firstStepData.serviceDelivery.detailDigitalChannels) {
        statusServiceDelivery_etailDigitalChannels = false;
      }
    }
    if (this.firstStepData.serviceDelivery.nonBank === true) {
      statusServiceDelivery = true;
      if (!this.firstStepData.serviceDelivery.detailNonBank) {
        statusServiceDelivery_detailNonBank = false;
      }
    }
    if (this.firstStepData.serviceDelivery.etc === true) {
      statusServiceDelivery = true;
      if (!this.firstStepData.serviceDelivery.detailEtc) {
        statusServiceDelivery_detailEtc = false;
      }
    }

    // = expectedDate (วันที่คาดว่าจะประกาศใช้)
    console.log('this.firstStepData.expectedDate 0001 5555', this.firstStepData.expectedDate);
    if (this.firstStepData.expectedDate) {
      statusExpectedDate = true;
    } else {
      this.firstStepData.expectedDate = null;
      statusExpectedDate = false;
    }

    // --------- lastReviewDate
    if (this.firstStepData.lastReviewDate) {
      statusLastReviewDate = true;
    } else {
      statusLastReviewDate = true;
    }
    if (this.firstStepData.nextReviewDate) {
      statusNextReviewDate = true;
    } else {
      statusNextReviewDate = false;
    }

    if (this.firstStepData.productName) {
      statusNameProduct = true;
    }
    // ----statusGroup
    if (this.firstStepData.productGroup.creditProduct === false && this.firstStepData.productGroup.nonCreditProduct === false) {
      statusGroup = true;
    } else {
      if (this.firstStepData.productGroup.CP_BusinessLoan === false &&
        this.firstStepData.productGroup.CP_PersonalLoan === false &&
        this.firstStepData.productGroup.CP_HousingLoan === false &&
        this.firstStepData.productGroup.CP_Etc === false) {
        if (this.firstStepData.productGroup.NCP_nonCreditProduct === false &&
          this.firstStepData.productGroup.NCP_Etc === false) {
          statusGroup = false;
        } else {
          statusGroup = true;
        }
      } else {
        statusGroup = true;
      }
    }
    // subStatusGroup
    if (this.firstStepData.productGroup.CP_Etc === true) {
      if (this.firstStepData.productGroup.CP_Etc_digital === null
        || this.firstStepData.productGroup.CP_Etc_digital === '') {
          statusGroupDetail = false;
      }
    }

    if (this.firstStepData.productGroup.NCP_Etc === true) {
      if (this.firstStepData.productGroup.NCP_Etc_digital === null
        || this.firstStepData.productGroup.NCP_Etc_digital === '') {
          statusGroupDetail = false;
      }
    }

    // getDataStatus
    if (
      statusPresentType === true &&
      statusAgendaType === true &&
      statusAgendaType_select === true &&
      statusPresentPurpose === true &&
      statusProductsOffered === true &&
      statusProductGroup === true &&
      statusProductGroup_creditProduct === true &&
      statusProductGroup_creditselect === true &&
      statusProductGroup_nonCreditProduct === true &&
      statusProductGroup_noncreditselect === true &&
      statusServiceDelivery === true &&
      statusServiceDelivery_etailDigitalChannels === true &&
      statusServiceDelivery_detailNonBank === true &&
      statusServiceDelivery_detailEtc === true &&
      statusExpectedDate === true &&
      statusLastReviewDate === true &&
      statusNextReviewDate === true &&
      statusNameProduct === true &&
      statusGroup === true &&
      statusGroupDetail === true
    ) {
      return true;
    } else {
      return false;
    }

  }



  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate request');
    console.log('pageAction', this.pageAction);
    let gotolink = this.sidebarService.goToLink;
    console.log('true', gotolink);

    if (this.pageAction === 'next') {
      gotolink = '/request/information-base/step2';
    }
    console.log('LINK:::', gotolink);
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef = this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        console.log('gotolink:::', gotolink);
        const search = gotolink.search('request');
        if (data.returnStatus === true) {
          console.log('true', gotolink);
          const saveReturn = this.saveDraft();
          if (saveReturn) {
            if (search === -1) {
              console.log('header');
              subject.next(true);
            } else {
              if (
                gotolink === '/request/information-base/step2' ||
                gotolink === '/request/information-base/step3' ||
                gotolink === '/request/information-base/step4') {
                subject.next(true);
              } else {
                if (this.pageStatus && this.checkDataSuccess) {
                  subject.next(true);
                } else if (this.pageStatus && this.checkDataSuccess === false) {
                  alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                  subject.next(false);
                } else {
                  alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                  subject.next(false);
                }
              }
            }
          } else {
            subject.next(false);
          }

        } else if (data.returnStatus === false) {
          console.log('false', gotolink);
          if (search === -1) {
            console.log('header');
            subject.next(true);
          } else {
            if (
              gotolink === '/request/information-base/step2' ||
              gotolink === '/request/information-base/step3' ||
              gotolink === '/request/information-base/step4') {
              this.GetDataFirstStep(this.requestId);
              subject.next(true);
              return true;
            } else {
              console.log('this.pageStatus', this.pageStatus);
              console.log('this.checkDataSuccess', this.checkDataSuccess);
              if (this.pageStatus) {
                subject.next(true);
              } else {
                if (this.checkDataSuccess === false) {
                  alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                  subject.next(false);
                } else {
                  this.GetDataFirstStep(this.requestId);
                  // alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                  subject.next(true);
                }
              }

            }

          }
        } else {
          // this.sidebarService.inPageStatus(false);
          console.log('test(2)')
          subject.next(false);
        }
      });
      return subject;
    } else {
      console.log('test(3)');
      return true;
    }
  }
}
