import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { informationBaseFirstStepComponent } from './first-step.component';

describe('informationBaseFirstStepComponent', () => {
  let component: informationBaseFirstStepComponent;
  let fixture: ComponentFixture<informationBaseFirstStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ informationBaseFirstStepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(informationBaseFirstStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
