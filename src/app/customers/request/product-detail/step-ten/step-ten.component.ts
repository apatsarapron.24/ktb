import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { StepTenService } from './../../../../services/request/product-detail/step-ten/step-ten.service';
import { RequestService } from './../../../../services/request/request.service';
import { saveAs } from 'file-saver';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogSaveStatusComponent } from '../../dialog/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from '../../../../services/configGuard/config-guard.service';


export interface FileList {
  fileName: string;
  fileSize: string;
  base64File: string;
}

const File_data: FileList[] = [];


@Component({
  selector: 'app-step-ten',
  templateUrl: './step-ten.component.html',
  styleUrls: ['./step-ten.component.scss'],
})
export class StepTenComponent implements OnInit, CanComponentDeactivate {
  @Input() template_manage = { role: null, validate: null, caaStatus: null, pageShow: null };
  @Output() saveStatus = new EventEmitter<boolean>();

  saveDraftstatus: any = null;
  outOfPageSave = false;
  linkTopage = null;
  pageChange = null;
  openDoCheckc = false;

  finance_1: any;
  finance_2: any;
  finance_3: any;
  finance_4: any;
  finance_5: any = '';
  input_finance_5: any = '';
  finance_5_sub: any = '';
  status_loading = true;

  list_finance_1: string[] = ['ตามหลักเกณฑ์ธนาคาร', 'กำหนดใหม่'];
  list_finance_2: string[] = ['ชุด Field เดิม', 'เพิ่ม Field'];
  list_finance_3: string[] = [
    'ได้รับความเห็นชอบจากสายงานบริหารการเงิน / MIS',
    'ไม่กระทบต่อการจัดสรรรายได้และค่าใช้จ่าย',
  ];
  list_finance_4: string[] = ['มี', 'ไม่มี'];
  list_finance_5: string[] = [
    'ไม่จัดทำ IFRS 9 Chectlist',
    'จัดทำ IFRS 9 Chectlist',
  ];
  list_finance_5_sub: string[] = [
    'ไม่ใช้ทางเลือก (จัดทำเฉพาะแบบฟอร์ม TFRS9)',
    'ใช้สิทธิเลือก การวัดมูลค่ายุติธรรมผ่านกำไรหรือขาดทุน (จัดทำแบบฟอร์ม FVTPL Option เพิ่มเติม)',
    'ใช้สิทธิเลือก การวัดมูลค่ายุติธรรมผ่านกำไรหรือขาดทุนเบ็ดเสร็จอื่น (จัดทำแบบฟอร์ม FVTOCI Option เพิ่มเติม)',
  ];

  keep_list_select: any = {
    radio_1: '',
    radio_2: '',
    radio_3: '',
    radio_4: '',
    radio_5: {
      check: '',
      input: '',
      select: '',
    },
  };

  topic5: any = {
    'NotIFRS9': {
      'flag': false,
      'reason': ''
    },
    'CreateIFRS9': {
      'flag': false,
      'TFRS9': false,
      'FVTPL': false,
      'FVTOCI': false
    }
  };
  fileUpload: any = {
    'file': [],
    'fileDelete': []
  };

  urls: any = [];
  displayedColumns_file: string[] = ['name', 'size', 'delete'];
  dataSource_file: MatTableDataSource<FileList>;

  results: any;
  api_response: any;

  FileOverSize: any;
  validate = null;
  downloadfile_by = [];

  constructor(private router: Router,
    public stepTenService: StepTenService,
    private sidebarService: RequestService,
    public dialog: MatDialog
  ) { }

  sendSaveStatus() {
    if (this.saveDraftstatus === 'success') {
      this.saveStatus.emit(true);
    } else {
      this.saveStatus.emit(false);
    }
  }

  changeSaveDraft() {
    this.saveDraftstatus = null;
    this.sendSaveStatus();
    this.sidebarService.inPageStatus(true);
  }

  next_step() {
    this.router.navigate(['/request/product-detail/step11']);
  }
  viewNext() {
    this.router.navigate(['request/product-detail/step11']);
  }
  ngOnInit() {
    window.scrollTo(0, 0);
    this.openDoCheckc = true;
    if (localStorage.getItem('requestId')) {
      this.sidebarService.sendEvent();
    }
    if (localStorage) {
      if (localStorage.getItem('requestId')) {
        this.getDetail(localStorage.getItem('requestId'));
      } else {
        this.status_loading = false;
      }
    } else {
      console.log('Brownser not support');
    }
  }

  getDetail(requestId) {
    this.stepTenService.getDetail(requestId, this.template_manage.pageShow).subscribe(res => {
      console.log('res step10', res);
      this.results = res;
      if (res['status'] === 'success') {
        this.status_loading = false;
      }
      if (this.results.data !== null) {
        this.validate = this.results.data.validate;
        console.log(this.validate);
        this.finance_1 = this.results.data.topic1;
        this.finance_2 = this.results.data.topic2;
        this.finance_3 = this.results.data.topic3;
        this.finance_4 = this.results.data.topic4;
        this.topic5 = this.results.data.topic5;
        for (let i = 0; i < this.results.data.fileUplode.file.length; i++) {
          this.urls.push(this.results.data.fileUplode.file[i]);
        }
        this.fileUpload.file = this.urls;
        console.log('file>>', this.fileUpload);
        if (this.results.data.topic5.NotIFRS9.flag === true) {
          this.finance_5 = 'sub_1';
          this.input_finance_5 = this.results.data.topic5.NotIFRS9.reason;
        }
        if (this.results.data.topic5.CreateIFRS9.flag === true) {
          this.finance_5 = 'sub_2';
          if (this.results.data.topic5.CreateIFRS9.TFRS9 === true) {
            this.finance_5_sub = 'ไม่ใช้ทางเลือก (จัดทำเฉพาะแบบฟอร์ม TFRS9)';
          }
          if (this.results.data.topic5.CreateIFRS9.FVTPL === true) {
            this.finance_5_sub = 'ใช้สิทธิเลือก การวัดมูลค่ายุติธรรมผ่านกำไรหรือขาดทุน (จัดทำแบบฟอร์ม FVTPL Option เพิ่มเติม)';
          }
          if (this.results.data.topic5.CreateIFRS9.FVTOCI === true) {
            // tslint:disable-next-line:max-line-length
            this.finance_5_sub = 'ใช้สิทธิเลือก การวัดมูลค่ายุติธรรมผ่านกำไรหรือขาดทุนเบ็ดเสร็จอื่น (จัดทำแบบฟอร์ม FVTOCI Option เพิ่มเติม)';
          }
        }
        console.log('url', this.urls);
        this.dataSource_file = new MatTableDataSource(this.urls);
        this.hideDownloadFile();
        console.log('datasource', this.dataSource_file);
      } else {
        this.validate = res['validate'];
      }
    });
  }

  validateStatus() {
    let status = false;
    if (this.finance_1 &&
      this.finance_2 &&
      this.finance_3 &&
      this.finance_4 &&
      (this.finance_5 && (this.input_finance_5 || this.finance_5_sub))) {
      status = true;
    } else {
      status = false;
    }
    console.log('status : ', status);
    return status;
  }

  saveDraft(page?, action?) {
    this.status_loading = true;
    let alertStatus = false;
    // console.log('firstStepData:', this.stepTwoData);
    const dataSend = {
      requestId: localStorage.getItem('requestId'),
      topic1: this.finance_1 ? this.finance_1 : '',
      topic2: this.finance_2 ? this.finance_2 : '',
      topic3: this.finance_3 ? this.finance_3 : '',
      topic4: this.finance_4 ? this.finance_4 : '',
      topic5: this.topic5 ? this.topic5 : '',
      fileUplode: this.fileUpload ? this.fileUpload : '',
      status: this.validateStatus(),
      conclusions: null,
      // parentRequestId: this.results.data.parentRequestId,
      // validate: this.results.data.validate
    };

    if (this.template_manage.pageShow === 'summary') {
      dataSend.conclusions = true;
    } else {
      dataSend.conclusions = null;
    }

    console.log('dataSend', dataSend);
    if (this.finance_5 === 'sub_1') {
      if (this.input_finance_5 === null || this.input_finance_5 === '') {
        alertStatus = true;
        this.status_loading = false;
        alert('กรุณาระบุเหตุผลเพิ่มเติม กรณีที่ไม่จัดทำ IFRS 9 Chectlist');
      }
    } else if (this.finance_5 === 'sub_2') {
      if (this.finance_5_sub === null || this.finance_5_sub === '') {
        alertStatus = true;
        this.status_loading = false;
        alert('กรุณาเลือกตัวเลือก กรณีที่จัดทำ IFRS 9 Chectlist');
      }
    }

    if (alertStatus === false) {
      const returnUpdateData = new Subject<any>();
      this.stepTenService.updateDetail(dataSend).subscribe(res => {
        console.log('response : ', res);
        this.api_response = res;
        if (this.api_response.status === 'success') {
          this.status_loading = false;
          this.downloadfile_by = [];
          this.saveDraftstatus = 'success';
          returnUpdateData.next(true);
          this.sidebarService.inPageStatus(false);
          setTimeout(() => {
            this.saveDraftstatus = 'fail';
          }, 3000);
          this.fileUpload.file = [];
          this.urls = [];
          this.sidebarService.sendEvent();
          this.sendSaveStatus();
          this.getDetail(localStorage.getItem('requestId'));
        } else {
          this.status_loading = false;
          alert(res['message']);
          returnUpdateData.next(false);
          // alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
        }
      }, err => {
        this.status_loading = false;
        console.log(err);
        alert(err['message']);
        returnUpdateData.next(false);
        // alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
      });
      return returnUpdateData;
    } else {
      return false;
    }
  }


  select_finance(index: any, event) {
    console.log('index:', index, 'select:', event.value);

    if (index === 1) {
      this.finance_1 = event.value;
      this.keep_list_select.radio_1 = this.finance_1;
    } else if (index === 2) {
      this.finance_2 = event.value;
      this.keep_list_select.radio_2 = this.finance_2;
    } else if (index === 3) {
      this.finance_3 = event.value;
      this.keep_list_select.radio_3 = this.finance_3;
    } else if (index === 4) {
      this.finance_4 = event.value;
      this.keep_list_select.radio_4 = this.finance_4;
    } else if (index === 5) {
      this.finance_5 = event.value;
      console.log('5:', this.finance_5);
      // this.keep_list_select.radio_5.check =this.finance_5
      if (this.finance_5 === 'sub_1') {
        this.keep_list_select.radio_5.check = this.list_finance_5[0];
        this.keep_list_select.radio_5.select = '';
        this.finance_5_sub = '';
        this.topic5.NotIFRS9.flag = true;
        this.topic5.CreateIFRS9.flag = !this.topic5.NotIFRS9.flag;
      } else {
        this.keep_list_select.radio_5.check = this.list_finance_5[1];
        this.keep_list_select.radio_5.input = '';
        this.input_finance_5 = '';
        this.topic5.CreateIFRS9.flag = true;
        this.topic5.NotIFRS9.flag = !this.topic5.CreateIFRS9.flag;
        // this.sub_select(event);
      }
    }

    console.log('list_radio:', this.keep_list_select);
  }

  sub_select(event) {
    // this.finance_5_sub = event.value;
    // console.log("e:", event);
    console.log('55555:', this.finance_5);
    console.log('dkdkdk', event);
    if (this.finance_5 === 'sub_2') {
      this.keep_list_select.radio_5.select = '';
      this.topic5.NotIFRS9.reason = '';
      if (event.value === 'ไม่ใช้ทางเลือก (จัดทำเฉพาะแบบฟอร์ม TFRS9)') {
        this.topic5.CreateIFRS9.TFRS9 = true;
        this.topic5.CreateIFRS9.FVTPL = !this.topic5.CreateIFRS9.TFRS9;
        this.topic5.CreateIFRS9.FVTOCI = !this.topic5.CreateIFRS9.TFRS9;
      }
      if (event.value === 'ใช้สิทธิเลือก การวัดมูลค่ายุติธรรมผ่านกำไรหรือขาดทุน (จัดทำแบบฟอร์ม FVTPL Option เพิ่มเติม)') {
        this.topic5.CreateIFRS9.FVTPL = true;
        this.topic5.CreateIFRS9.TFRS9 = !this.topic5.CreateIFRS9.FVTPL;
        this.topic5.CreateIFRS9.FVTOCI = !this.topic5.CreateIFRS9.FVTPL;
      }
      if (event.value === 'ใช้สิทธิเลือก การวัดมูลค่ายุติธรรมผ่านกำไรหรือขาดทุนเบ็ดเสร็จอื่น (จัดทำแบบฟอร์ม FVTOCI Option เพิ่มเติม)') {
        this.topic5.CreateIFRS9.FVTOCI = true;
        this.topic5.CreateIFRS9.TFRS9 = !this.topic5.CreateIFRS9.FVTOCI;
        this.topic5.CreateIFRS9.FVTPL = !this.topic5.CreateIFRS9.FVTOCI;
      }
    } else {
      this.keep_list_select.radio_5.select = event.value;
      this.keep_list_select.radio_5.input = this.input_finance_5;
      this.topic5.NotIFRS9.reason = this.input_finance_5;
      this.topic5.CreateIFRS9.TFRS9 = this.topic5.CreateIFRS9.FVTPL = this.topic5.CreateIFRS9.FVTOCI = false;
    }

    console.log('list_radio_222:', this.keep_list_select);
  }

  onSelectFile(event) {
    this.FileOverSize = [];
    if (event.target.files && event.target.files[0]) {
      // tslint:disable-next-line: prefer-const
      let filesAmount = event.target.files.length;
      const file = event.target.files;
      for (let i = 0; i < filesAmount; i++) {
        console.log('type:', file[i]);
        if (
          // file[i].type === 'image/svg+xml' ||
          file[i].type === 'image/jpeg' ||
          // file[i].type === 'image/raw' ||
          // file[i].type === 'image/svg' ||
          // file[i].type === 'image/tif' ||
          // file[i].type === 'image/gif' ||
          file[i].type === 'image/jpg' ||
          file[i].type === 'image/png' ||
          file[i].type === 'application/doc' ||
          file[i].type === 'application/pdf' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
          file[i].type === 'application/pptx' ||
          file[i].type === 'application/docx' ||
          (file[i].type === 'application/ppt' && file[i].size)
        ) {
          if (file[i].size <= 5000000) {
            this.multiple_file(file[i], i);
          } else {
            this.FileOverSize.push(file[i].name);
          }

        } else {
          this.status_loading = false;
          alert('File Not Support');
        }
      }

    }
    if (this.FileOverSize.length !== 0) {
      console.log('open modal');
      document.getElementById('testttt').click();
    }
  }

  multiple_file(file, index) {
    console.log('download');
    const reader = new FileReader();
    if (file) {
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');
        // this.urls.push({
        const x = {
          fileName: file.name,
          fileSize: file.size,
          base64File: splitFile[1],
        };
        // });
        this.urls.push(x);
        this.fileUpload.file = this.urls;
        console.log('this.urls', this.urls);
        this.dataSource_file = new MatTableDataSource(this.urls);
        this.hideDownloadFile();
      };
    }
    this.changeSaveDraft();
  }
  // delete_mutifile(data: any, index: any) {
  //   console.log('data:', data, 'index::', index);
  //   this.dataSource_file.data.splice(index, 1)
  //   this.dataSource_file = new MatTableDataSource(this.dataSource_file.data);
  // }
  delete_mutifile(data: any, index: any) {
    console.log('data:', data, 'index::', index);
    if ('id' in this.urls[index]) {
      const id = this.fileUpload.file[index].id;
      console.log('found id');
      if ('fileDelete' in this.fileUpload) {
        this.fileUpload.fileDelete.push(id);
      }
    }
    console.log('fileDelete attachments : ', this.fileUpload.fileDelete);
    this.urls.splice(index, 1);
    this.dataSource_file = new MatTableDataSource(this.urls);
    this.changeSaveDraft();
  }
  // ===============================================================================

  downloadFile(pathdata: any) {
    this.status_loading = true;
    const contentType = '';
    const sendpath = pathdata;
    console.log('path', sendpath);
    this.sidebarService.getfile(sendpath).subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        this.status_loading = false;
        const datagetfile = res['data'];
        const b64Data = datagetfile['data'];
        const filename = datagetfile['fileName'];
        console.log('base64', b64Data);
        const config = this.b64toBlob(b64Data, contentType);
        saveAs(config, filename);
      }

    });
  }

  b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const convertbyte = atob(b64Data);
    const byteCharacters = atob(convertbyte);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  downloadFile64(data: any) {
    this.status_loading = true;
    const contentType = '';
    this.status_loading = false;
    const b64Data = data.base64File;
    const filename = data.fileName;
    console.log('base64', b64Data);

    const config = this.base64(b64Data, contentType);
    saveAs(config, filename);
  }

  base64(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  autoother(index: any) {
    if (index === 1) {
      const textother = document.getElementById('textother');
      textother.style.overflow = 'hidden';
      textother.style.height = 'auto';
      textother.style.height = textother.scrollHeight + 'px';
    }
  }

  // ngOnDestroy(): void {
  //   const data = this.sidebarService.getsavePage();
  //   let saveInFoStatus = false;
  //   saveInFoStatus = data.saveStatusInfo;
  //   if (saveInFoStatus === true) {
  //     this.outOfPageSave = true;
  //     this.linkTopage = data.goToLink;
  //     console.log('ngOnDestroy', this.linkTopage);
  //     this.saveDraft();
  //     this.sidebarService.resetSaveStatusInfo();
  //   }
  // }

  hideDownloadFile() {
    if (this.dataSource_file.data.length > 0) {
      this.downloadfile_by = [];

      for (let index = 0; index < this.dataSource_file.data.length; index++) {
        const element = this.dataSource_file.data[index];
        console.log('ele', element);

        if ('id' in element) {
          this.downloadfile_by.push('path');
        } else {
          this.downloadfile_by.push('base64');
        }
      }
    }
    console.log('by:', this.downloadfile_by);
  }

  ngDoCheck() {
    if (this.openDoCheckc === true) {
      if (this.template_manage.caaStatus !== 'show') {
        this.pageChange = this.sidebarService.changePageGoToLink();
        if (this.pageChange) {
          // this.getNavbar();
          const data = this.sidebarService.getsavePage();
          let saveInFoStatus = false;
          saveInFoStatus = data.saveStatusInfo;
          if (saveInFoStatus === true) {
            this.outOfPageSave = true;
            this.linkTopage = data.goToLink;
            this.saveDraft('navbar', null);
            this.sidebarService.resetSaveStatu();
          }
        }
      }
    }
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate request');
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef =  this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        if (data.returnStatus === true) {
          const raturnStatus = this.saveDraft();
          console.log('request raturnStatus', raturnStatus);
            if (raturnStatus) {
              console.log('raturnStatus T', this.saveDraftstatus);
              subject.next(true);
            } else {
              console.log('raturnStatus F', this.saveDraftstatus);
              subject.next(false);
            }
        } else if (data.returnStatus === false) {
          this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
        }
      });
      return subject;
    } else {
      return true;
    }
  }


}
