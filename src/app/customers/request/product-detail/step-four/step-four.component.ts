import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import {
  AngularMyDatePickerDirective,
  IAngularMyDpOptions,
  IMyDateModel,
} from 'angular-mydatepicker';
import { Router } from '@angular/router';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
  CdkDragExit,
  CdkDragEnter,
  CdkDragStart,
  CdkDragEnd,
} from '@angular/cdk/drag-drop';
import { template } from '@angular/core/src/render3';
import * as moment from 'moment';
import { RequestService } from '../../../../services/request/request.service';
import { StepFourService } from './../../../../services/request/product-detail/step-four/step-four.service';
import { saveAs } from 'file-saver';
import { FormGroup, FormControl } from '@angular/forms';
import { DateComponent } from '../../../datepicker/date/date.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogSaveStatusComponent } from '../../dialog/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from '../../../../services/configGuard/config-guard.service';

export interface PeriodicElement {
  loanCriteriaId: number;
  mainProcess: string;
  subProcess: string;
  in_out: string;
  ItRelated: string;
  theRisk: string;
  riskControl_formerly: string;
  riskControl_more: string;
}

const loanCriteria_DATA: PeriodicElement[] = [
  {
    loanCriteriaId: 1,
    mainProcess: 'ขั้นตอน A',
    subProcess: 'ขั้นตอน A-1',
    in_out: 'no',
    ItRelated: 'yes',
    theRisk: null,
    riskControl_formerly: 'Hydrogen',
    riskControl_more: 'Hydrogen',
  },
  {
    loanCriteriaId: 2,
    mainProcess: 'ขั้นตอน B',
    subProcess: 'ขั้นตอน B-1',
    in_out: 'no',
    ItRelated: 'yes',
    theRisk: null,
    riskControl_formerly: 'Hydrogen',
    riskControl_more: 'Hydrogen',
  },
  {
    loanCriteriaId: 3,
    mainProcess: null,
    subProcess: 'ขั้นตอน B-1',
    in_out: 'no',
    ItRelated: 'yes',
    theRisk: null,
    riskControl_formerly: 'Hydrogen',
    riskControl_more: 'Hydrogen',
  },
];

@Component({
  selector: 'app-step-four',
  templateUrl: './step-four.component.html',
  styleUrls: ['./step-four.component.scss'],
})
export class StepFourComponent implements OnInit, CanComponentDeactivate {
  @Input() template_manage = { role: null, validate: null, caaStatus: null, pageShow: null };
  @Output() saveStatus = new EventEmitter<boolean>();
  displayedColumns_file_sub: string[] = ['name', 'size', 'delete'];
  dataSource_file_sub: MatTableDataSource<FileList>;
  urls_sub: any = [];

  @ViewChild(DateComponent) Date: DateComponent;

  displayedColumns_file: string[] = ['name', 'size', 'delete'];
  dataSource_file: MatTableDataSource<FileList>;
  urls: any = [];

  saveDraftstatus = null;
  outOfPageSave = false;
  linkTopage = null;
  pageChange = null;
  openDoCheckc = false;

  testfile: any;
  nonSOP_document: any;
  nonSOP_file_name: any;
  nonSOP_file_size: number;
  message: string;

  document: any;
  file_name: any;
  file_size: number;

  datetime: any;

  model: any = null;
  @ViewChild('dp') mydp: AngularMyDatePickerDirective;
  modelReset = this.model;
  image1 = [];
  file1_1: any;
  file1_2: any;
  // Model
  showModel = false;
  modalmainImageIndex: number;
  modalmainImagedetailIndex: number;
  indexSelect: number;
  modalImage = [];
  modalImagedetail = [];
  fileValue: number;
  add_mainProcess: null;

  SOP = '';

  // new table
  dropIndex: number;
  staticTitleIndex = []; // !important ex [0,1,2] mean set fix row index 0 1 2

  // Risk sub_table
  Risk_dropIndex = 0;
  staticTitleIndexRisk = [];

  stepFour = {
    basicInfo: {
      haveSOP: false,
      noSOP: false,
      newProceeses: false,
    },
    responseProcessSop: {
      refNo: '',
      effectiveDate: '',
      fileUpload: {
        file: [],
        fileDelete: [],
      },
    },
    workFlowText: '',
    workFlow: {
      file: [],
      fileDelete: [],
    },
    requestProcessConsiders: [
      {
        id: '',
        mainProcess: 'ขั้นตอน A',
        subProcess: [
          {
            id: '1',
            subTitle: '',
            inOut: null,
            itRelate: null,
            subProcessRisk: [
              {
                id: null,
                risk: null,
                riskControl: '',
                riskControlOther: null,
              },
            ],
          },
        ],
      },
      {
        id: '',
        mainProcess: 'ขั้นตอน B',
        subProcess: [
          {
            id: '2',
            subTitle: '',
            inOut: null,
            itRelate: null,
            subProcessRisk: [
              {
                id: null,
                risk: null,
                riskControl: '',
                riskControlOther: null,
              },
            ],
          },
        ],
      },
      {
        id: '',
        mainProcess: 'ขั้นตอน C',
        subProcess: [
          {
            id: '3',
            subTitle: '',
            inOut: null,
            itRelate: null,
            subProcessRisk: [
              {
                id: null,
                risk: null,
                riskControl: '',
                riskControlOther: null,
              },
            ],
          },
        ],
      },
      {
        id: '',
        mainProcess: '',
        subProcess: [
          {
            id: '4',
            subTitle: '',
            inOut: null,
            itRelate: null,
            subProcessRisk: [
              {
                id: null,
                risk: null,
                riskControl: '',
                riskControlOther: null,
              },
            ],
          },
        ],
      },
    ],
    requestProcessConsidersDelete: [],
    requestProcessConsidersSubDelete: [],
    requestProcessConsidersSubRiskDelete: [],
    fileUpload: {
      file: [],
      fileDelete: [],
    },
    status: false,
    validate: null,
    conclusions: null
  };
  status_loading = true;

  // textareaForm = new FormGroup({
  //   workFlowText: new FormControl(this.stepFour.workFlowText),
  // });
  OverSize: any;
  validate = null;
  nextItem: boolean;
  statusprocessConsiders = true;

  statusprocessconsiders: boolean;
  datastatustitle = [];
  datastatussub = [];
  downloadfile_by = [];
  downloadfile_by_sub = [];

  constructor(
    private router: Router,
    private stepFourService: StepFourService,
    private sidebarService: RequestService,
    public dialog: MatDialog,

  ) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.openDoCheckc = true;
    if (localStorage) {
      if (localStorage.getItem('requestId')) {
        this.getDetail(localStorage.getItem('requestId'));
        this.sidebarService.sendEvent();
      } else {
        this.status_loading = false;
      }
    } else {
      console.log('Brownser not support');
    }
  }

  sendSaveStatus() {
    if (this.saveDraftstatus === 'success') {
      this.saveStatus.emit(true);
    } else {
      this.saveStatus.emit(false);
    }
  }

  autogrow() {
    const workFlowText = document.getElementById('workFlowText');
    workFlowText.style.overflow = 'hidden';
    workFlowText.style.height = 'auto';
    workFlowText.style.height = workFlowText.scrollHeight + 'px';
  }

  autoprocess(index: any, test: any) {
    if (test === 1) {
      const textprocess1 = document.getElementById('textprocess1' + String(index));
      textprocess1.style.overflow = 'hidden';
      textprocess1.style.height = 'auto';
      textprocess1.style.height = textprocess1.scrollHeight + 'px';
    }

    if (test === 2) {
      const textprocess2 = document.getElementById('textprocess2' + String(index));
      textprocess2.style.overflow = 'hidden';
      textprocess2.style.height = 'auto';
      textprocess2.style.height = textprocess2.scrollHeight + 'px';
    }

    if (test === 3) {
      const textprocess3 = document.getElementById('textprocess3' + String(index));
      textprocess3.style.overflow = 'hidden';
      textprocess3.style.height = 'auto';
      textprocess3.style.height = textprocess3.scrollHeight + 'px';
    }
    if (test === 4) {
      const textprocess4 = document.getElementById('textprocess4' + String(index));
      textprocess4.style.overflow = 'hidden';
      textprocess4.style.height = 'auto';
      textprocess4.style.height = textprocess4.scrollHeight + 'px';
    }
    if (test === 5) {
      const textprocess4 = document.getElementById('textprocess5' + String(index));
      textprocess4.style.overflow = 'hidden';
      textprocess4.style.height = 'auto';
      textprocess4.style.height = textprocess4.scrollHeight + 'px';
    }
  }

  changeSaveDraft() {
    this.saveDraftstatus = null;
    this.sendSaveStatus();
    this.sidebarService.inPageStatus(true);
  }

  getDetail(requestId) {
    this.stepFourService.getPage4(requestId, this.template_manage.pageShow).subscribe((res) => {
      console.log(res);


      if (res['status'] === 'success') {
        this.status_loading = false;
      }
      if (res['status'] === 'success' && res['data'] !== null) {
        // console.log(res['data']);
        this.stepFour = res['data'];
        this.validate = this.stepFour.validate;
        console.log('validate: ', this.validate);
        this.stepFour.requestProcessConsidersSubRiskDelete = [];
        // set fileDelete to Object stepFour
        this.stepFour.responseProcessSop.fileUpload.fileDelete = [];
        this.stepFour.workFlow.fileDelete = [];
        this.stepFour.fileUpload.fileDelete = [];
        this.stepFour.requestProcessConsidersSubDelete = [];
        this.stepFour.requestProcessConsidersDelete = [];

        // this.textareaForm = new FormGroup({
        //   workFlowText: new FormControl(this.stepFour.workFlowText)
        // });

        // set datepicker
        if (this.stepFour.responseProcessSop.effectiveDate) {
          const dateString = this.stepFour.responseProcessSop.effectiveDate;
          this.model = dateString;
          this.Date.datepicker_input.dateData = this.model;
          this.Date.ngOnInit();
          // this.Date.datepicker_input.page = this.model ;
          // this.model = {
          //   isRange: false,
          //   singleDate: {
          //     jsDate: new Date(dateString),
          //   },
          // };
          // this.datetime = moment(this.model.singleDate.jsDate).format('DD/MM/YY');
        } else {
          this.Date.datepicker_input.dateData = null;
          this.model = null;
        }

        // set hardcode if child length = 0
        this.stepFour.requestProcessConsiders.forEach((mainProcess) => {
          if (mainProcess.subProcess.length === 0) {
            const templateSubProcess = {
              id: '',
              subTitle: '',
              inOut: null,
              itRelate: null,
              subProcessRisk: [
                {
                  id: null,
                  risk: null,
                  riskControl: '',
                  riskControlOther: null,
                },
              ],
            };
            mainProcess.subProcess.push(templateSubProcess);

            // formatted textarea
            // document.getElementById('workFlowText').addEventListener('paste', function (e: ClipboardEvent) {
            //   e.preventDefault();
            //   const text = e.clipboardData.getData('text/plain');
            //   document.execCommand('insertHTML', false, text);
            // });
          }

          mainProcess.subProcess.forEach((subProcessRisk) => {
            // console.log('subb',subProcessRisk);
            if (subProcessRisk.subProcessRisk.length === 0) {
              const subProcessRiskTemplete = {
                id: null,
                risk: null,
                riskControl: '',
                riskControlOther: null,
              };
              subProcessRisk.subProcessRisk.push(subProcessRiskTemplete);
            }
          });
        });

        // set picture preview to all input
        for (let i = 0; i < this.stepFour.workFlow.file.length; i++) {
          this.preview(1, this.stepFour.workFlow.file[i], i);
        }

        // console.log('SOP : ', this.SOP);
        // console.log('basicInfo : ', this.stepFour.basicInfo);
        // set first time ratio
        if (this.stepFour.basicInfo.haveSOP === true) {
          this.SOP = '1';
        }
        if (this.stepFour.basicInfo.noSOP === true) {
          this.SOP = '2';
        }
        if (this.stepFour.basicInfo.newProceeses === true) {
          this.SOP = '3';
        }
        // console.log('SOP after : ', this.SOP);
        this.Date.datepicker_input.page = this.SOP;
        // set file attachments
        this.dataSource_file_sub = new MatTableDataSource(
          this.stepFour.responseProcessSop.fileUpload.file
        );
        this.hideDownloadFile_Sub();
        // console.log('getdata', this.dataSource_file_sub);
        this.dataSource_file = new MatTableDataSource(
          this.stepFour.fileUpload.file
        );
        this.hideDownloadFile();
        // console.log('getdata', this.dataSource_file);

        // set status to stepData
        this.stepFour.status = false;
      } else {
        this.validate = res['validate'];
      }

    });
  }
  viewNext() {
    this.router.navigate(['request/product-detail/step5']);
  }

  next_step() {
    this.router.navigate(['/request/product-detail/step5']);
  }


  saveDraft(page?, action?) {
    // this.stepFour.workFlowText = document.getElementById('workFlowText').innerHTML;
    // console.log('data:>>>>>', this.stepFour);
    this.status_loading = true;
    if (this.stepFour.responseProcessSop.effectiveDate === 'Invalid date') {
      this.stepFour.responseProcessSop.effectiveDate = null;
    }



    let noSOP_Status = false;
    if (this.stepFour.basicInfo.noSOP || this.stepFour.basicInfo.haveSOP) {
      if (this.stepFour.responseProcessSop.refNo === '' || this.stepFour.responseProcessSop.effectiveDate === null) {
        this.status_loading = false;
        alert('กรุณากรอกกรอกเลขหนังสือ และวันที่มีผลบังคับใช้');
        noSOP_Status = false;
      } else {

        noSOP_Status = true;
      }
    } else {
      noSOP_Status = true;
    }


    if (noSOP_Status === true) {
      let _statustitle = '';
      let _statussub = '';
      this.datastatustitle = [];
      this.datastatussub = [];
      for (
        let index = 0; index < this.stepFour.requestProcessConsiders.length; index++) {
        const Group = this.stepFour.requestProcessConsiders[index].subProcess;

        if (Group.length > 0) {
          for (let j = 0; j < Group.length; j++) {
            const element = Group[j];

            if (element.subTitle === '') {
              _statustitle = 'false';
            } else {
              _statustitle = 'true';
            }

            for (let k = 0; k < element.subProcessRisk.length; k++) {
              const subpro = element.subProcessRisk[k];
              if (subpro.risk === '' || subpro.riskControl === '' || subpro.riskControlOther === '') {
                _statussub = 'false';
              } else {
                _statussub = 'true';
              }
              this.datastatussub.push(_statussub);
            }
            this.datastatustitle.push(_statustitle);
          }
        }

        for (let n = 0; n < Group.length; n++) {
          const subProcess = Group[n].id;
          if (typeof (subProcess) === 'string') {
            // console.log('subProcess', subProcess);
            this.stepFour.requestProcessConsiders[index].subProcess[n].id = '';
          }
        }
      }
      // console.log('datastatustitle', this.datastatustitle);
      // console.log('datastatussub', this.datastatussub);

      let _checktitle = false;
      let _checksub = false;
      if (this.datastatustitle.length > 0) {
        for (let i = 0; i < this.datastatustitle.length; i++) {
          const element = this.datastatustitle[i];
          if (element === 'false') {
            _checktitle = false;
            break;
          } else {
            _checktitle = true;
          }
        }
      }
      if (this.datastatussub.length > 0) {
        for (let i = 0; i < this.datastatussub.length; i++) {
          const element = this.datastatussub[i];
          if (element === 'false') {
            _checksub = false;
            break;
          } else {
            _checksub = true;
          }
        }
      }

      if (_checktitle === false || _checksub === false) {
        // alert('กรุณากรอกรายละเอียดกระบวนการทำงาน');
        this.statusprocessconsiders = false;
      } else {
        this.statusprocessconsiders = true;
      }

      console.log('statusprocessconsiders', this.statusprocessconsiders);

      if (this.validateStatus() === true) {
        this.stepFour.status = true;
      } else {
        this.stepFour.status = false;
      }
      console.log('stepFour status', this.stepFour.status);

      if (localStorage.getItem('requestId')) {
        this.stepFour['requestId'] = localStorage.getItem('requestId');
      }
      // ======check type dragable if have not id

      let statusDrag = true;

      for (
        let index = 0;
        index < this.stepFour.requestProcessConsiders.length;
        index++
      ) {
        if (
          this.stepFour.requestProcessConsiders[index].id === '' &&
          this.stepFour.requestProcessConsiders[index].mainProcess === ''
        ) {
          this.stepFour.requestProcessConsiders.splice(index, 1);
        }
      }


      for (
        let index = 0;
        index < this.stepFour.requestProcessConsiders.length;
        index++
      ) {
        if (
          this.stepFour.requestProcessConsiders[index].id === '' &&
          this.stepFour.requestProcessConsiders[index].mainProcess === ''
        ) {
          this.stepFour.requestProcessConsiders.splice(index, 1);
          statusDrag = true;
        }
      }


      for (let i = 0; i < this.stepFour.requestProcessConsidersSubDelete.length; i++) {
        if (typeof (this.stepFour.requestProcessConsidersSubDelete[i]) === 'string') {
          this.stepFour.requestProcessConsidersSubDelete.splice(i, 1);
        }
      }
      for (let i = 0; i < this.stepFour.requestProcessConsidersSubDelete.length; i++) {
        if (typeof (this.stepFour.requestProcessConsidersSubDelete[i]) === 'string') {
          this.stepFour.requestProcessConsidersSubDelete.splice(i, 1);
        }
      }

      // console.log('Delete', this.stepFour.requestProcessConsidersDelete);
      // console.log('Delete Sub', this.stepFour.requestProcessConsidersSubDelete);
      // console.log('Delete SubRisk',this.stepFour.requestProcessConsidersSubRiskDelete);

      console.log('table:',this.stepFour.requestProcessConsiders)
      this.statusprocessConsiders = true
      for (let r = 0; r < this.stepFour.requestProcessConsiders.length; r++) {
        if (this.stepFour.requestProcessConsiders[r].mainProcess == '' || this.stepFour.requestProcessConsiders[r].mainProcess == null) {
          this.statusprocessConsiders = false
        } else {
          for (let k = 0; k < this.stepFour.requestProcessConsiders[r].subProcess.length; k++) {
            const subProcess = this.stepFour.requestProcessConsiders[r].subProcess[k]
            if ((subProcess.subTitle === '' || subProcess.subTitle === null) ||
              (subProcess.inOut === '' || subProcess.inOut === null) ||
              (subProcess.itRelate === '' || subProcess.itRelate === null)) {
                this.statusprocessConsiders = false;
            }
            else {
              subProcess.subProcessRisk.forEach(risk => {
                if ((risk.risk === '' || risk.risk === null) ||
                  (risk.riskControl === '' || risk.riskControl === null) ||
                  (risk.riskControlOther === '' || risk.riskControlOther === null)) {
                  console.log('false');
                  this.statusprocessConsiders = false;
                }
              })
            }
          }
        }
      }
      if(this.statusprocessConsiders == false) {
        this.stepFour.status = false;
      }
      console.log('000005', statusDrag);
      this.nextItem = statusDrag;

      if (this.template_manage.pageShow === 'summary') {
        this.stepFour.conclusions = true;
      } else {
        this.stepFour.conclusions = null;
      }

      const returnUpdateData = new Subject<any>();
      if (statusDrag === true) {
        console.log('data:>>>>>', this.stepFour);
        this.stepFourService.savePage4(this.stepFour).subscribe(
          (res) => {
            if (res['status'] === 'success') {
              this.status_loading = false;
              this.downloadfile_by = [];
              this.downloadfile_by_sub = [];
              this.saveDraftstatus = 'success';
              returnUpdateData.next(true);
              this.sidebarService.inPageStatus(false);
              setTimeout(() => {
                this.saveDraftstatus = 'fail';
              }, 3000);
              this.stepFour.fileUpload.file = [];
              this.sidebarService.sendEvent();
              this.sendSaveStatus();
              this.getDetail(localStorage.getItem('requestId'));
            } else {
              this.status_loading = false;
              alert(res['message']);
              returnUpdateData.next(false);
            }
          },
          (err) => {
            console.log(err);
            this.status_loading = false;
            alert(err['message']);
            returnUpdateData.next(false);
          }
        );
      }
      return returnUpdateData;
    } else {
      return false;
    }
  }

  validateStatus() {
    let status = false;
    const checkForm =
      this.stepFour.workFlowText &&
      (this.stepFour.basicInfo.haveSOP === true ||
        this.stepFour.basicInfo.noSOP === true ||
        this.stepFour.basicInfo.newProceeses === true)
      && (this.stepFour.requestProcessConsiders.length > 0 && this.statusprocessconsiders === true);

    console.log('checkForm : ', checkForm);

    status = checkForm;

    console.log('status : ', status);
    return status;
  }

  changeDatepicker(e) {
    console.log('change datepicker : ', e);
    console.log('date single : ', e.singleDate.jsDate);
    const date = moment(e.singleDate.jsDate).format('YYYY-MM-DD');
    console.log(date);
    this.stepFour.responseProcessSop.effectiveDate = date;
  }
  changeDate(date: any) {
    const SendI = this.Date.changeDate(date);
    this.model = SendI;
    this.stepFour.responseProcessSop.effectiveDate = SendI;
    return SendI;
  }
  // ratio sop
  clearWorkFlow() {
    console.log(this.stepFour);
    if (this.SOP === '1') {
      this.stepFour.basicInfo.haveSOP = true;
      this.stepFour.basicInfo.noSOP = false;
      this.stepFour.basicInfo.newProceeses = false;
      // this.stepFour.responseProcessSop.refNo = null;
      // this.stepFour.responseProcessSop.effectiveDate = null;
      // this.stepFour.responseProcessSop.fileUpload.file = [];
      // this.stepFour.responseProcessSop.fileUpload.fileDelete = [];
      this.Date.datepicker_input.page = '1';
      // this.model = this.Date.ngOnInit();
      // this.dataSource_file_sub = new MatTableDataSource();
      console.log(' this.stepFour', this.stepFour);
    } else if (this.SOP === '2') {
      this.stepFour.basicInfo.haveSOP = false;
      this.stepFour.basicInfo.noSOP = true;
      this.stepFour.basicInfo.newProceeses = false;
      // this.Date.datepicker_input.dateData = 'step4';
      this.Date.datepicker_input.page = '2';
      // this.model = this.Date.ngOnInit();
      console.log('check:', this.model);
    } else if (this.SOP === '3') {
      this.stepFour.basicInfo.haveSOP = false;
      this.stepFour.basicInfo.noSOP = false;
      this.stepFour.basicInfo.newProceeses = true;
      this.stepFour.responseProcessSop.refNo = null;
      this.stepFour.responseProcessSop.effectiveDate = null;
      this.stepFour.responseProcessSop.fileUpload.file = [];
      this.stepFour.responseProcessSop.fileUpload.fileDelete = [];
      this.dataSource_file_sub = new MatTableDataSource();
      this.Date.datepicker_input.dateData = null;
      this.Date.datepicker_input.page = '3';
      this.model = this.Date.ngOnInit();
    }
    this.changeSaveDraft();
  }

  delete_image(fileIndex, index) {
    console.log('fileIndex:', fileIndex);
    console.log('index:', index);
    if (fileIndex === 1) {
      if ('id' in this.stepFour.workFlow.file[index]) {
        const id = this.stepFour.workFlow.file[index].id;
        console.log('found id');
        if ('fileDelete' in this.stepFour.workFlow) {
          this.stepFour.workFlow.fileDelete.push(id);
        }
      }
      console.log('fileDelete objective : ', this.stepFour.workFlow.fileDelete);
      this.stepFour.workFlow.file.splice(index, 1);
      this.image1.splice(index, 1);

      console.log(
        'this.stepFour.objective.file.length2:',
        this.stepFour.workFlow.file.length
      );
      for (let i = 0; i < this.stepFour.workFlow.file.length; i++) {
        this.preview(fileIndex, this.stepFour.workFlow.file[i], i);
      }
    }
    this.changeSaveDraft();
  }

  image_click(colId, id) {
    this.showModel = true;
    console.log('colId:', colId);
    console.log('id:', id);
    // this.showCircle_colId = colId;
    // this.showCircle_id = id;
    document.getElementById('myModal').style.display = 'block';
    this.imageModal(colId, id);
  }

  image_bdclick() {
    document.getElementById('myModal').style.display = 'block';
    console.log('2222');
  }

  // ============================== Modal ==========================================
  imageModal(fileIndex, index) {
    if (fileIndex === 1) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image1;
      this.modalImagedetail = this.stepFour.workFlow.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    }
  }
  // ===============================================================================

  closeModal() {
    this.showModel = false;
    document.getElementById('myModal').style.display = 'none';
  }

  // getFileDetails ================================================================
  getFileDetails(fileIndex, event) {
    const files = event.target.files;

    for (let i = 0; i < files.length; i++) {
      const mimeType = files[i].type;
      console.log(mimeType);
      if (mimeType.match('image/jpeg|image/png') == null) {
        this.message = 'Only images are supported.';
        this.status_loading = false;
        alert(this.message);
        return;
      }
      const file = files[i];

      console.log('file:', file);
      const reader = new FileReader();
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');

        const templateFile = {
          fileName: file.name,
          base64File: splitFile[1],
        };
        console.log(templateFile);

        if (fileIndex === 1) {
          console.log('00000000000000000000000000000001');
          this.stepFour.workFlow.file.push(templateFile);
          // console.log('this.stepFour.objective.file : ' , this.stepFour.objective.file)

          for (
            let index = 0;
            index < this.stepFour.workFlow.file.length;
            index++
          ) {
            this.preview(fileIndex, this.stepFour.workFlow.file[index], index);
          }
        }
      };
      reader.readAsDataURL(file);
    }
    this.changeSaveDraft();
  }

  preview(fileIndex, file, index) {
    //  console.log('file : ', file);

    const name = file.fileName;
    const typeFiles = name.match(/\.\w+$/g);
    const typeFile = typeFiles[0].replace('.', '');

    const tempPicture = `data:image/${typeFile};base64,${file.base64File}`;

    if (fileIndex === 1) {
      if (file) {
        this.image1[index] = tempPicture;
      }
    }
  }

  // ======================= Table ==========================

  deleteTable(index) {
    // this.stepFour.loanCriteria.splice(index, 1);
  }

  onSelectFile(fileIndex, event) {

    this.OverSize = [];

    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      const file = event.target.files;
      for (let i = 0; i < filesAmount; i++) {
        console.log('type:', file[i]);
        if (
          // file[i].type === 'image/svg+xml' ||
          file[i].type === 'image/jpeg' ||
          // file[i].type === 'image/raw' ||
          // file[i].type === 'image/svg' ||
          // file[i].type === 'image/tif' ||
          // file[i].type === 'image/gif' ||
          file[i].type === 'image/jpg' ||
          file[i].type === 'image/png' ||
          file[i].type === 'application/doc' ||
          file[i].type === 'application/pdf' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
          file[i].type === 'application/pptx' ||
          file[i].type === 'application/docx' ||
          (file[i].type === 'application/ppt' && file[i].size)
        ) {
          if (file[i].size <= 5000000) {
            this.multiple_file(fileIndex, file[i], i);
          } else {
            this.OverSize.push(file[i].name);
          }
        } else {
          this.status_loading = false;
          alert('File Not Support');
        }
      }
    }
    if (this.OverSize.length !== 0) {
      console.log('open modal');
      document.getElementById('testttt').click();
    }
    this.changeSaveDraft();
  }

  multiple_file(fileIndex, file, index) {
    console.log('download');
    const reader = new FileReader();
    if (file) {
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');

        const templateFile = {
          fileName: file.name,
          fileSize: file.size,
          base64File: splitFile[1],
        };

        if (fileIndex === 1) {
          this.stepFour.responseProcessSop.fileUpload.file.push(templateFile);
          this.dataSource_file_sub = new MatTableDataSource(
            this.stepFour.responseProcessSop.fileUpload.file
          );
          this.hideDownloadFile_Sub();
        }
        if (fileIndex === 2) {
          this.stepFour.fileUpload.file.push(templateFile);
          this.dataSource_file = new MatTableDataSource(
            this.stepFour.fileUpload.file
          );
          this.hideDownloadFile();
        }
      };
    }
  }
  delete_mutifile(fileIndex: any, data: any, index: any) {
    console.log('data:', data, 'index::', index);
    if (fileIndex === 1) {
      // check if id exiting
      if ('id' in this.stepFour.responseProcessSop.fileUpload.file[index]) {
        const id = this.stepFour.responseProcessSop.fileUpload.file[index].id;
        console.log('found id');
        if ('fileDelete' in this.stepFour.responseProcessSop.fileUpload) {
          this.stepFour.responseProcessSop.fileUpload.fileDelete.push(id);
        }
      }
      this.stepFour.responseProcessSop.fileUpload.file.splice(index, 1);
      this.dataSource_file_sub = new MatTableDataSource(
        this.stepFour.responseProcessSop.fileUpload.file
      );
    }
    if (fileIndex === 2) {
      // check if id exiting
      if ('id' in this.stepFour.fileUpload.file[index]) {
        const id = this.stepFour.fileUpload.file[index].id;
        console.log('found id');
        if ('fileDelete' in this.stepFour.fileUpload) {
          this.stepFour.fileUpload.fileDelete.push(id);
        }
      }
      this.stepFour.fileUpload.file.splice(index, 1);
      this.dataSource_file = new MatTableDataSource(
        this.stepFour.fileUpload.file
      );
    }
    this.changeSaveDraft();
  }

  // drag and drop ===================================================================== //
  checkStaticTitle(index) {
    return this.staticTitleIndex.includes(index);
  }

  drop(event: CdkDragDrop<string[]>) {
    const fixedBody = this.staticTitleIndex;
    console.log('event Group:', event);
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (event.previousContainer.data.length > 1) {
      // check if only one left child
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (fixedBody.includes(this.dropIndex)) {
      // check only 1 child left and static type move only not delete
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      const templatesubTitleRisk = {
        id: this.makeid(5),
        subTitle: '',
        inOut: null,
        itRelate: null,
        subProcessRisk: [
          {
            id: null,
            risk: '',
            riskControl: '',
            riskControlOther: '',
            remark: '',
            subject: '',
            management: '',
          },
        ],
      };

      this.stepFour.requestProcessConsiders[this.dropIndex].subProcess.push(
        templatesubTitleRisk
      );
    } else {
      // check only 1 child left and not a static type move and delete
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      // remove current row
      this.removeAddItem(this.dropIndex);
      this.dropIndex = null;
    }
    this.changeSaveDraft();
  }

  dragExited(event: CdkDragExit<string[]>) {
    console.log('Exited', event);
  }

  entered(event: CdkDragEnter<string[]>) {
    console.log('Entered', event);
  }

  startDrag(event: CdkDragStart<string[]>, istep) {
    // console.log('startDrag', event);
    // console.log('startDrag index', istep);
    this.dropIndex = istep;
  }

  removeAddItem(index) {
    if (index != null) {
      this.stepFour.requestProcessConsidersDelete.push(
        this.stepFour.requestProcessConsiders[index].id
      );
      this.stepFour.requestProcessConsiders.splice(index, 1);
    }
  }

  // ==================================================================
  dropItem(event: CdkDragDrop<string[]>) {
    console.log('event item :', event);
    console.log('previousContainer :', event.container.data.length);
    console.log('index group', this.dropIndex);
    console.log('index item:', this.Risk_dropIndex);
    const fixedBody = this.staticTitleIndexRisk;

    // if (
    //   event.previousContainer.data.length === 1 &&
    //   event.container.data.length !== 1
    // ) {
    //   console.log('index:', this.dropIndex);
    //   this.removeAddItem(this.dropIndex);
    //   this.dropIndex = null;
    // }
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (event.previousContainer.data.length > 1) {
      // check if only one left child
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      // check only 1 child left and not a static type move and delete
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      // remove current row
      this.removeAddItemRisk(this.dropIndex, this.Risk_dropIndex);
      this.dropIndex = null;
      this.Risk_dropIndex = null;
    }
    this.changeSaveDraft();
  }

  connectItem(): any[] {
    const mapping = [];
    for (
      let index = 0;
      index < this.stepFour.requestProcessConsiders.length;
      index++
    ) {
      const Group = this.stepFour.requestProcessConsiders[index].subProcess;

      // tslint:disable-next-line: no-shadowed-variable
      // this.stepFour.operations[index].subProcess.forEach(
      //   ( item, i) =>
      //    mapping.push(`${this.stepFour.operations[index].subProcess[i].id}`)
      // );

      for (let j = 0; j < Group.length; j++) {
        mapping.push(String(Group[j].id));
      }
    }

    // console.log('map', mapping);
    return mapping;
  }

  removeAddItemRisk(subIndex, Riskkindex) {
    if (this.stepFour.requestProcessConsiders[subIndex].subProcess.length === 1) {
      this.stepFour.requestProcessConsidersSubDelete.push(
        this.stepFour.requestProcessConsiders[subIndex].id
      );
      this.stepFour.requestProcessConsiders.splice(subIndex, 1);
    } else if (
      this.stepFour.requestProcessConsiders[subIndex].subProcess.length > 1 &&
      Riskkindex != null
    ) {
      this.stepFour.requestProcessConsidersSubDelete.push(
        this.stepFour.requestProcessConsiders[subIndex].subProcess[Riskkindex].id
      );
      this.stepFour.requestProcessConsiders[subIndex].subProcess.splice(Riskkindex, 1);
    }
  }

  startDrag_Risk(event: CdkDragStart<string[]>, isub, istep) {
    console.log('Risks tartDrag', event);
    console.log('Risk startDrag index', isub);
    this.Risk_dropIndex = isub;
    this.dropIndex = istep;
  }
  enteredRisk(event: CdkDragEnter<string[]>) {
    console.log('Entered Risk', event);
  }
  dragExitedRisk(event: CdkDragExit<string[]>) {
    console.log('Exited Risk', event);
  }

  dragEnded(event: CdkDragEnd) {
    console.log('dragEnded Event > item', event.source.data);
  }

  checkStaticTitle_Risk(index) {
    return this.staticTitleIndexRisk.includes(index);
  }
  // ===============================================================================================

  addRow() {
    const tempateAddRow = {
      id: '',
      mainProcess: '',
      subProcess: [
        {
          id: this.makeid(5),
          subTitle: '',
          inOut: null,
          itRelate: null,
          subProcessRisk: [
            {
              id: null,
              risk: '',
              riskControl: '',
              riskControlOther: '',
              remark: '',
              subject: '',
              management: '',
            },
          ],
        },
      ],
    };
    this.stepFour.requestProcessConsiders.push(tempateAddRow);
  }

  removeDataItem(stepindex, subindex, riskIndex) {
    console.log('stepindex', stepindex);
    console.log('subindex', subindex);
    console.log('riskIndex', riskIndex);
    const fixedBody = [];
    // tslint:disable-next-line: max-line-length
    if (
      fixedBody.includes(stepindex) &&
      this.stepFour.requestProcessConsiders[stepindex].subProcess[subindex]
        .subProcessRisk.length > 1 &&
      this.Risk_dropIndex !== 0
    ) {
      console.log('delete fixed row only have many risk[]');
      // this.stepFour.operations[stepindex].detailDelete.push(this.stepFour.operations[stepindex].subProcess[subindex]['id']);
      // console.log(
      //   this.stepFour.processConsiders[stepindex].subProcess[subindex]
      //     .subProcessRisk[riskIndex].id
      // );
      if (
        this.stepFour.requestProcessConsiders[stepindex].subProcess[subindex]
          .subProcessRisk[riskIndex].id !== ''
      ) {
        this.stepFour.requestProcessConsidersSubRiskDelete.push(
          this.stepFour.requestProcessConsiders[stepindex].subProcess[subindex]
            .subProcessRisk[riskIndex].id
        );
      }

      this.stepFour.requestProcessConsiders[stepindex].subProcess[
        subindex
      ].subProcessRisk.splice(riskIndex, 1);
    } else if (
      fixedBody.includes(stepindex) &&
      this.stepFour.requestProcessConsiders[stepindex].subProcess[subindex]
        .subProcessRisk.length === 1
    ) {
      console.log('3 clear fixed row only');
      if (subindex !== 0) {
        if (
          this.stepFour.requestProcessConsiders[stepindex].subProcess[subindex].id !==
          ''
        ) {
          this.stepFour.requestProcessConsidersSubDelete.push(
            this.stepFour.requestProcessConsiders[stepindex].subProcess[subindex].id
          );
        }
        this.stepFour.requestProcessConsiders[stepindex].subProcess.splice(
          subindex,
          1
        );
      } else {
        this.stepFour.requestProcessConsidersSubDelete.push(
          this.stepFour.requestProcessConsiders[stepindex].subProcess[subindex].id
        );

        Object.keys(
          this.stepFour.requestProcessConsiders[stepindex].subProcess[subindex]
        ).forEach((key, index) => {
          if (key === 'subProcessRisk') {
            // tslint:disable-next-line: no-shadowed-variable
            for (
              // tslint:disable-next-line: no-shadowed-variable
              let index = 0;
              index <
              this.stepFour.requestProcessConsiders[stepindex].subProcess[subindex][
                key
              ].length;
              index++
            ) {
              Object.keys(
                this.stepFour.requestProcessConsiders[stepindex].subProcess[subindex][
                key
                ][riskIndex]
              ).forEach((sub, i) => {
                this.stepFour.requestProcessConsiders[stepindex].subProcess[subindex][
                  key
                ][index][sub] = '';
              });
            }
          } else {
            this.stepFour.requestProcessConsiders[stepindex].subProcess[subindex][
              key
            ] = '';
          }
        });
      }

      // console.log(
      //   'tt',
      //   this.stepFour.operations[stepindex].subProcess[subindex]
      // );

      // this.stepFour.operations[stepindex].detailDelete.push(
      //   this.stepFour.operations[stepindex].subProcess[subindex]['id']
      // );
    } else if (
      this.stepFour.requestProcessConsiders[stepindex].subProcess.length === 1 &&
      this.stepFour.requestProcessConsiders[stepindex].subProcess[subindex]
        .subProcessRisk.length > 1
    ) {
      console.log(' delete risk row only have 1 row');
      if (
        this.stepFour.requestProcessConsiders[stepindex].subProcess[subindex]
          .subProcessRisk[riskIndex].id !== ''
      ) {
        this.stepFour.requestProcessConsidersSubRiskDelete.push(
          this.stepFour.requestProcessConsiders[stepindex].subProcess[subindex]
            .subProcessRisk[riskIndex].id
        );
      }
      this.stepFour.requestProcessConsiders[stepindex].subProcess[
        subindex
      ].subProcessRisk.splice(riskIndex, 1);
    } else if (
      this.stepFour.requestProcessConsiders[stepindex].subProcess.length > 1 &&
      this.stepFour.requestProcessConsiders[stepindex].subProcess[subindex]
        .subProcessRisk.length > 1
    ) {
      console.log(' delete risk row only have many row');
      // this.stepFour.operations[stepindex].detailDelete.push(
      //   this.stepFour.operations[stepindex].subProcess[subindex]['id']
      // );
      if (
        this.stepFour.requestProcessConsiders[stepindex].subProcess[subindex]
          .subProcessRisk[riskIndex].id !== ''
      ) {
        this.stepFour.requestProcessConsidersSubRiskDelete.push(
          this.stepFour.requestProcessConsiders[stepindex].subProcess[subindex]
            .subProcessRisk[riskIndex].id
        );
      }
      this.stepFour.requestProcessConsiders[stepindex].subProcess[
        subindex
      ].subProcessRisk.splice(riskIndex, 1);
    } else if (
      this.stepFour.requestProcessConsiders[stepindex].subProcess[subindex]
        .subProcessRisk.length === 0
    ) {
      console.log(
        'delete row permanent',
        this.stepFour.requestProcessConsiders[stepindex].id
      );
      // this.stepFour.operationsDelete.push(
      //   this.stepFour.operations[stepindex].id
      // );
      if (this.stepFour.requestProcessConsiders[stepindex].id !== '') {
        this.stepFour.requestProcessConsidersDelete.push(
          this.stepFour.requestProcessConsiders[stepindex].id
        );
        this.stepFour.requestProcessConsiders.splice(stepindex, 1);
      }
    } else if (
      this.stepFour.requestProcessConsiders[stepindex].subProcess[subindex]
        .subProcessRisk.length === 1 &&
      subindex !== 0 &&
      this.stepFour.requestProcessConsiders[stepindex].subProcess.length > 0
    ) {
      console.log('delete row have 1 sub , 1 risk');
      if (
        this.stepFour.requestProcessConsiders[stepindex].subProcess[subindex].id !== ''
      ) {
        this.stepFour.requestProcessConsidersSubDelete.push(
          this.stepFour.requestProcessConsiders[stepindex].subProcess[subindex].id
        );
        this.stepFour.requestProcessConsiders[stepindex].subProcess.splice(
          subindex,
          1
        );
      }
    } else if (
      this.stepFour.requestProcessConsiders[stepindex].subProcess[subindex]
        .subProcessRisk.length === 1 &&
      subindex === 0 && this.stepFour.requestProcessConsiders[stepindex].subProcess.length === 1
    ) {
      console.log('delete row have 1 sub , 1 risk and delete main row');
      if (this.stepFour.requestProcessConsiders[stepindex].id !== '') {
        this.stepFour.requestProcessConsidersDelete.push(
          this.stepFour.requestProcessConsiders[stepindex].id
        );
      }
      this.stepFour.requestProcessConsiders.splice(stepindex, 1);

    } else if (this.stepFour.requestProcessConsiders[stepindex].subProcess[subindex]
      .subProcessRisk.length === 1 &&
      subindex === 0 && this.stepFour.requestProcessConsiders[stepindex].subProcess.length > 1) {
      console.log('delete row have many sub , 1 risk and delete 1 sub ');
      if (this.stepFour.requestProcessConsiders[stepindex].subProcess[subindex].id !== '') {
        this.stepFour.requestProcessConsidersSubDelete.push(
          this.stepFour.requestProcessConsiders[stepindex].subProcess[subindex].id
        );

      }
      this.stepFour.requestProcessConsiders[stepindex].subProcess.splice(subindex, 1);
    } else {
      console.log('not in case');
    }
    this.changeSaveDraft();
  }
  makeid(length) {
    let result = '';
    const characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
  // ============================================================================================> drag and drop


  // downloadFile
  downloadFile(pathdata: any) {
    this.status_loading = true;
    const contentType = '';
    const sendpath = pathdata;
    console.log('path', sendpath);
    this.sidebarService.getfile(sendpath).subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        this.status_loading = false;
        const datagetfile = res['data'];
        const b64Data = datagetfile['data'];
        const filename = datagetfile['fileName'];
        console.log('base64', b64Data);
        const config = this.b64toBlob(b64Data, contentType);
        saveAs(config, filename);
      }

    });
  }

  b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const convertbyte = atob(b64Data);
    const byteCharacters = atob(convertbyte);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  // downloadFile
  downloadFilesub(base64FileData: any, fileNameData: any) {
    // console.log('testfile', this.testfile);
    const contentType = '';
    const b64Data = base64FileData;
    const filename = fileNameData;
    console.log('b64Data', b64Data);
    console.log('filename', filename);
    const config = this.b64toBlobsub(b64Data, contentType);
    console.log('config', config);
    saveAs(config, filename);
  }

  b64toBlobsub(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  downloadFile64(data: any) {
    this.status_loading = true;
    const contentType = '';
    this.status_loading = false;
    const b64Data = data.base64File;
    const filename = data.fileName;
    console.log('base64', b64Data);

    const config = this.base64(b64Data, contentType);
    saveAs(config, filename);
  }

  base64(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }


  ReviewConvertDate(date) {
    let dateForm = null;
    const year = Number(moment(date).format('YYYY')) + Number(543);
    dateForm = moment(date).format('DD/MM') + '/' + year;
    return dateForm;

  }

  // ngOnDestroy(): void {
  //   const data = this.sidebarService.getsavePage();
  //   let saveInFoStatus = false;
  //   saveInFoStatus = data.saveStatusInfo;
  //   if (saveInFoStatus === true) {
  //     this.outOfPageSave = true;
  //     this.linkTopage = data.goToLink;
  //     console.log('ngOnDestroy', this.linkTopage);
  //     this.saveDraft();
  //     this.sidebarService.resetSaveStatusInfo();
  //   }
  // }

  hideDownloadFile() {
    if (this.dataSource_file.data.length > 0) {
      this.downloadfile_by = [];

      for (let index = 0; index < this.dataSource_file.data.length; index++) {
        const element = this.dataSource_file.data[index];
        console.log('ele', element);

        if ('id' in element) {
          this.downloadfile_by.push('path');
        } else {
          this.downloadfile_by.push('base64');
        }
      }
    }
    console.log('by:', this.downloadfile_by);
  }

  hideDownloadFile_Sub() {
    if (this.dataSource_file_sub.data.length > 0) {
      this.downloadfile_by_sub = [];

      for (let index = 0; index < this.dataSource_file_sub.data.length; index++) {
        const element = this.dataSource_file_sub.data[index];
        console.log('ele', element);

        if ('id' in element) {
          this.downloadfile_by_sub.push('path');
        } else {
          this.downloadfile_by_sub.push('base64');
        }
      }
    }
    console.log('by:', this.downloadfile_by_sub);
  }

  // ngDoCheck() {
  //   if (this.openDoCheckc === true) {
  //     if (this.template_manage.caaStatus !== 'show') {
  //       this.pageChange = this.sidebarService.changePageGoToLink();
  //       if (this.pageChange) {
  //         // this.getNavbar();
  //         const data = this.sidebarService.getsavePage();
  //         let saveInFoStatus = false;
  //         saveInFoStatus = data.saveStatusInfo;
  //         if (saveInFoStatus === true) {
  //           this.outOfPageSave = true;
  //           this.linkTopage = data.goToLink;
  //           this.saveDraft('navbar', null);
  //           this.sidebarService.resetSaveStatu();
  //         }
  //       }
  //     }
  //   }
  // }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate request');
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef =  this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        if (data.returnStatus === true) {
          const raturnStatus = this.saveDraft();
          console.log('request raturnStatus', raturnStatus);
            if (raturnStatus) {
              console.log('raturnStatus T', this.saveDraftstatus);
              subject.next(true);
            } else {
              console.log('raturnStatus F', this.saveDraftstatus);
              subject.next(false);
            }
        } else if (data.returnStatus === false) {
          this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
        }
      });
      return subject;
    } else {
      return true;
    }
  }

}
