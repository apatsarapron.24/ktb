import { Component, OnInit, ViewChild, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { dateFormat } from 'highcharts';
import { Router } from '@angular/router';
import { StepEightService } from '../../../../services/request/product-detail/step-eight/step-eight.service';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { RequestService } from '../../../../services/request/request.service';
import * as moment from 'moment';
import { FormGroup, FormControl, AbstractControl } from '@angular/forms';
import { saveAs } from 'file-saver';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogSaveStatusComponent } from '../../dialog/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from '../../../../services/configGuard/config-guard.service';

@Component({
  selector: 'app-step-eight',
  templateUrl: './step-eight.component.html',
  styleUrls: ['./step-eight.component.scss']
})
export class StepEightComponent implements OnInit, CanComponentDeactivate {
  @Input() template_manage = { role: null, validate: null, caaStatus: null, pageShow: null };
  @Output() saveStatus = new EventEmitter<boolean>();
  financialYear: number;

  saveDraftstatus = null;
  outOfPageSave = false;
  linkTopage = null;
  pageChange = null;
  openDoCheckc = false;

  dataSource_file: MatTableDataSource<FileList>;
  displayedColumns_file: string[] = ['name', 'size', 'delete'];
  status_loading = true;
  validate = null;
  stepEight = {
    validate: null,
    year: [
      this.formatDate(new Date()),
      0,
      0
    ],
    costBenefit: {
      income: [
        {
          title: '',
          tranx: [0, 0, 0]
        },
        {
          title: '',
          tranx: [0, 0, 0]
        },
        {
          title: '',
          tranx: [0, 0, 0]
        }
      ],
      outlay: [
        {
          title: '',
          tranx: [0, 0, 0]
        },
        {
          title: '',
          tranx: [0, 0, 0]
        }
      ],
      profit: {
        title: '',
        tranx: [0, 0, 0]
      }
    },
    costRoi: '',
    costRaroc: '',
    costBreakEventPoint: '',
    hypothesis: '',
    costBenefitText: '',
    costBenefitOutsource: '',
    nonFinanceText: '',
    costBenefitDelete: [],
    nonFinance: {
      file: [],
      fileDelete: []
    },
    fileUpload: {
      file: [],
      fileDelete: []
    },
    status: false,
    // validate: null,
    conclusions: null
  };

  // modal only
  file1: any;
  file2: any;

  document: any;
  Agreement = null;
  file_name: any;
  file_size: number;

  imgURL: any;
  image1 = [];

  showImage1 = false;
  message = null;

  // Model
  showModel = false;
  modalmainImageIndex: number;
  modalmainImagedetailIndex: number;
  indexSelect: number;
  modalImage = [];
  modalImagedetail = [];
  fileValue: number;
  getDetail: any;
  requestId: any;

  textareaForm = new FormGroup({
    nonFinanceText: new FormControl(this.stepEight.nonFinanceText),
  });
  // coppy_paste_status: number = 0;

  OverSize: any;
  downloadfile_by = [];

  constructor(
    private router: Router,
    private callService: StepEightService,
    private sidebarService: RequestService,
    public dialog: MatDialog,
  ) { }

  isNumber(val): boolean { return typeof val === 'number'; }

  formatDate(dateIn: any) {
    console.log('dateIn', dateIn);
    const now = moment(dateIn);
    now.locale('th');
    const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();
    return buddhishYear;
  }

  changeYear(e) {
    console.log(e);
    if (this.stepEight['year'][0]) {
      this.stepEight['year'][1] = +this.stepEight['year'][0] + 1;
      this.stepEight['year'][2] = +this.stepEight['year'][0] + 2;
    }
  }


  dropIncome(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.stepEight['costBenefit'].income, event.previousIndex, event.currentIndex);
  }

  dropOutcome(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.stepEight['costBenefit'].outlay, event.previousIndex, event.currentIndex);
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.openDoCheckc = true;
    this.sidebarService.sendEvent();

    if (this.stepEight['year'][0]) {
      this.stepEight['year'][1] = +this.stepEight['year'][0] + 1;
      this.stepEight['year'][2] = +this.stepEight['year'][0] + 2;
    }

    this.requestId = localStorage.getItem('requestId');
    console.log(this.requestId);
    this.getDetailStep();
  }

  sendSaveStatus() {
    if (this.saveDraftstatus === 'success') {
      this.saveStatus.emit(true);
    } else {
      this.saveStatus.emit(false);
    }
  }

  changeSaveDraft() {
    this.saveDraftstatus = null;
    this.sendSaveStatus();
    this.sidebarService.inPageStatus(true);
  }


  getDetailStep() {
    this.callService.getDetail(this.requestId, this.template_manage.pageShow).subscribe(res => {
      console.log('res step8', res);
      if (res['status'] === 'success') {
        this.status_loading = false;
      }
      if (res['data'] !== null) {
        this.stepEight = res['data'];
        this.validate = this.stepEight.validate;
        console.log(this.validate);
        this.stepEight.costBenefitDelete = [];
        this.stepEight.nonFinance.fileDelete = [];
        this.stepEight.fileUpload.fileDelete = [];

        this.textareaForm = new FormGroup({
          nonFinanceText: new FormControl(this.stepEight.nonFinanceText),
        });

        // if (this.coppy_paste_status === 0) {
        //   document.getElementById('nonFinanceText').addEventListener('paste', function (e: ClipboardEvent) {
        //     e.preventDefault();
        //     const text = e.clipboardData.getData('text/plain');
        //     document.execCommand('insertHTML', false, text);
        //   });
        // }
        for (let index = 0; index < this.stepEight['nonFinance'].file.length; index++) {
          this.preview(1, this.stepEight['nonFinance'].file[index], index);
        }

        this.dataSource_file = new MatTableDataSource(this.stepEight['fileUpload'].file);
        this.hideDownloadFile();
      } else {
        this.validate = res['validate'];
        this.status_loading = false;
        console.log('data null');
      }
    });

  }

  autogrow() {
    const textArea = document.getElementById('description');
    textArea.style.overflow = 'hidden';
    textArea.style.height = 'auto';
    textArea.style.height = textArea.scrollHeight + 'px';
  }
  addRow(tableIndex) {
    console.log('add row func');

    const incomeTemplate = {
      title: '',
      tranx: [0, 0, 0]
    };

    if (tableIndex === 1) {
      this.stepEight['costBenefit'].income.push(incomeTemplate);
    } else if (tableIndex === 2) {
      this.stepEight['costBenefit'].outlay.push(incomeTemplate);
    }
  }

  removeItem(tableIndex, index) {
    console.log('removeItem', tableIndex, index);
    if (tableIndex === 1) {
      // check if have id
      console.log('income delete index : ', this.stepEight['costBenefit'].income[index]);
      if ('id' in this.stepEight['costBenefit'].income[index]) {
        const id = this.stepEight['costBenefit'].income[index]['id'];
        this.stepEight['costBenefitDelete'].push(id);
      }

      if (this.stepEight['costBenefit'].income.length > 1) {
        this.stepEight['costBenefit'].income.splice(index, 1);
      } else {
        // check if table have one left child we emptry not remove this row
        const incomeTemplate = {
          title: '',
          tranx: [0, 0, 0]
        };
        this.stepEight['costBenefit'].income[0] = incomeTemplate;
      }
    } else if (tableIndex === 2) {
      // check if have id
      if ('id' in this.stepEight['costBenefit'].outlay[index]) {
        const id = this.stepEight['costBenefit'].outlay[index]['id'];
        this.stepEight['costBenefitDelete'].push(id);
      }

      if (this.stepEight['costBenefit'].outlay.length > 1) {
        this.stepEight['costBenefit'].outlay.splice(index, 1);
      } else {
        // check if table have one left child we emptry not remove this row
        const incomeTemplate = {
          title: '',
          tranx: [0, 0, 0]
        };
        this.stepEight['costBenefit'].outlay[0] = incomeTemplate;
      }
    }
    console.log('image delete costBenefitDelete : ', this.stepEight['costBenefitDelete']);
    this.changeSaveDraft();
  }

  delete_image(fileIndex, index) {
    console.log('fileIndex:', fileIndex);
    console.log('index:', index);
    if (fileIndex === 1) {
      // check image have id to remove
      if (this.stepEight.nonFinance.file[index]) {
        const id = this.stepEight.nonFinance.file[index].id;
        this.stepEight.nonFinance.fileDelete.push(id);
      }
      this.stepEight['nonFinance'].file.splice(index, 1);
      this.image1.splice(index, 1);

      console.log('this.stepEight.objective.uploadFile.length2:', this.stepEight['nonFinance'].file.length);
      for (let i = 0; i < this.stepEight['nonFinance'].file.length; i++) {
        this.preview(fileIndex, this.stepEight['nonFinance'].file[i], i);
      }
    }
    this.changeSaveDraft();
  }

  image_click(colId, id) {
    this.showModel = true;
    console.log('colId:', colId);
    console.log('id:', id);
    // this.showCircle_colId = colId;
    // this.showCircle_id = id;
    document.getElementById('myModal8').style.display = 'block';
    this.imageModal(colId, id);
  }
  image_bdclick() {
    document.getElementById('myModal8').style.display = 'block';
    console.log('2222');
  }

  closeModal() {
    this.showModel = false;
    document.getElementById('myModal8').style.display = 'none';
  }
  viewNext() {
    this.router.navigate(['request/product-detail/step9']);
  }
  // getFileDetails ================================================================
  getFileDetails(fileIndex, event) {

    const files = event.target.files;

    for (let i = 0; i < files.length; i++) {
      const mimeType = files[i].type;
      console.log(mimeType);
      if (mimeType.match('image/jpeg|image/png') == null) {
        this.message = 'Only images are supported.';
        this.status_loading = false;
        alert(this.message);
        return;
      }
      const file = files[i];
      // this.file_name = file.name;
      // this.file_type = file.type;
      // this.file_size = file.size;
      // this.modifiedDate = file.lastModifiedDate;
      console.log('file:', file);
      const reader = new FileReader();
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');

        const templateFile = {
          fileName: file.name,
          base64File: splitFile[1],
        };
        console.log(templateFile);

        if (fileIndex === 1) {
          console.log('00000000000000000000000000000001');
          this.stepEight['nonFinance'].file.push(templateFile);
          console.log('this.stepOne.objective.file : ', this.stepEight['nonFinance'].file);
          for (let index = 0; index < this.stepEight['nonFinance'].file.length; index++) {
            this.preview(fileIndex, this.stepEight['nonFinance'].file[index], index);
          }
          this.file1 = undefined;
          this.file2 = undefined;
        }
      };
      reader.readAsDataURL(file);
      this.changeSaveDraft();
    }
  }

  preview(fileIndex, file, index) {
    //  console.log('file : ', file);

    const name = file.fileName;
    const typeFiles = name.match(/\.\w+$/g);
    const typeFile = typeFiles[0].replace('.', '');

    const tempPicture = `data:image/${typeFile};base64,${file.base64File}`;

    if (fileIndex === 1) {
      if (file) {
        this.image1[index] = tempPicture;
      }
    }
  }

  // getFileDocument ================================================================
  getFileDocument(event) {
    const file = event.target.files[0];
    const mimeType = file.type;
    if (mimeType.match(/pdf\/*/) == null) {
      this.message = 'Only PDF are supported.';
      this.status_loading = false;
      alert(this.message);
      return;
    }
    console.log('11111', file);
    this.file_name = file.name;
    // this.file_type = file.type;
    this.file_size = file.size / 1024;
    // this.modifiedDate = file.lastModifiedDate;
    // console.log('file:', file);

    this.stepEight['fileUpload'] = file;
    // this.document = undefined;
  }

  onSelectFile(event) {

    this.OverSize = [];

    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      const file = event.target.files;
      for (let i = 0; i < filesAmount; i++) {
        console.log('type:', file[i]);
        if (
          // file[i].type === 'image/svg+xml' ||
          file[i].type === 'image/jpeg' ||
          // file[i].type === 'image/raw' ||
          // file[i].type === 'image/svg' ||
          // file[i].type === 'image/tif' ||
          // file[i].type === 'image/gif' ||
          file[i].type === 'image/jpg' ||
          file[i].type === 'image/png' ||
          file[i].type === 'application/doc' ||
          file[i].type === 'application/pdf' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
          file[i].type === 'application/pptx' ||
          file[i].type === 'application/docx' ||
          (file[i].type === 'application/ppt' && file[i].size)
        ) {
          if (file[i].size <= 5000000) {
            this.multiple_file(file[i], i);
          } else {
            this.OverSize.push(file[i].name);
          }
        } else {
          this.status_loading = false;
          alert('File Not Support');
        }

      }
    }
    if (this.OverSize.length !== 0) {
      console.log('open modal');
      document.getElementById('testttt').click();
    }
  }

  multiple_file(file, index) {
    // console.log('download');
    const reader = new FileReader();
    if (file) {
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');

        const templateFile = {
          fileName: file.name,
          fileSize: file.size,
          base64File: splitFile[1],
        };
        this.stepEight['fileUpload'].file.push(templateFile);
        this.dataSource_file = new MatTableDataSource(this.stepEight['fileUpload'].file);
        this.hideDownloadFile();
      };
    }
    this.changeSaveDraft();
  }

  delete_mutifile(data: any, index: any) {
    console.log('data:', data, 'index::', index);
    if ('id' in this.stepEight['fileUpload'].file[index]) {
      const id = this.stepEight['fileUpload'].file[index].id;
      console.log('found id');
      if ('fileDelete' in this.stepEight['fileUpload']) {
        this.stepEight['fileUpload'].fileDelete.push(id);
      }
    }
    console.log('fileDelete fileUpload : ', this.stepEight['fileUpload'].fileDelete);
    this.stepEight['fileUpload'].file.splice(index, 1);
    this.dataSource_file = new MatTableDataSource(this.stepEight['fileUpload'].file);
    this.changeSaveDraft();
  }



  // ============================== Modal ==========================================
  imageModal(fileIndex, index) {
    if (fileIndex === 1) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image1;
      this.modalImagedetail = this.stepEight['nonFinance'].file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    }
  }
  // ===============================================================================

  validateStatus() {
    let status = false;

    if (
      this.stepEight.year[0] &&
      this.stepEight.costBenefit &&
      this.stepEight.costRoi &&
      this.stepEight.costRaroc &&
      this.stepEight.costBreakEventPoint &&
      this.stepEight.hypothesis &&
      (this.stepEight.costBenefitText &&
        this.stepEight.costBenefitOutsource) &&
      this.stepEight.nonFinanceText) {
      status = true;
    }

    console.log('status : ', status);
    return status;
  }

  saveDraft(page?, action?) {
    this.status_loading = true;
    // this.coppy_paste_status = this.coppy_paste_status + 1;

    // this.stepEight.nonFinanceText = document.getElementById('nonFinanceText').innerHTML;
    if (this.validateStatus()) {
      this.stepEight['status'] = true;
    } else {
      this.stepEight['status'] = false;
    }

    console.log('data send : ', this.stepEight);

    let _year: any;
    let _temp: any;
    _temp = this.stepEight.year;
    this.stepEight.year = [];

    for (let i = 0; i < _temp.length; i++) {
      const element = _temp[i];
      if (element) {
        _year = element.toString();
        this.stepEight.year.push(_year);
      }
    }

    if (localStorage.getItem('requestId')) {
      this.stepEight['requestId'] = localStorage.getItem('requestId');
    }

    if (this.template_manage.pageShow === 'summary') {
      this.stepEight.conclusions = true;
    } else {
      this.stepEight.conclusions = null;
    }

    const returnUpdateData = new Subject<any>();
    this.callService.updateDetail(this.stepEight).subscribe(res => {
      console.log(res['status']);
      if (res['status'] === 'success') {
        this.status_loading = false;
        this.downloadfile_by = [];
        this.saveDraftstatus = res['status'];
        returnUpdateData.next(true);
        this.sidebarService.inPageStatus(false);
        setTimeout(() => {
          this.saveDraftstatus = 'fail';
        }, 3000);

        this.stepEight['fileUpload'].file = [];
        this.sendSaveStatus();
        this.getDetailStep();
      } else {
        this.status_loading = false;
        alert(res['message']);
        returnUpdateData.next(false);
      }
      this.sidebarService.sendEvent();
    });
    return returnUpdateData;
  }

  next() {
    this.router.navigate(['/request/product-detail/step9']);
  }

  // ===============================================================================

  downloadFile(pathdata: any) {
    this.status_loading = true;
    const contentType = '';
    const sendpath = pathdata;
    console.log('path', sendpath);
    this.sidebarService.getfile(sendpath).subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        this.status_loading = false;
        const datagetfile = res['data'];
        const b64Data = datagetfile['data'];
        const filename = datagetfile['fileName'];
        console.log('base64', b64Data);
        const config = this.b64toBlob(b64Data, contentType);
        saveAs(config, filename);
      }

    });
  }

  b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const convertbyte = atob(b64Data);
    const byteCharacters = atob(convertbyte);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  downloadFile64(data: any) {
    this.status_loading = true;
    const contentType = '';
    this.status_loading = false;
    const b64Data = data.base64File;
    const filename = data.fileName;
    console.log('base64', b64Data);

    const config = this.base64(b64Data, contentType);
    saveAs(config, filename);
  }

  base64(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  autoprocess(index: any, test: any) {
    if (test === 1) {
      const textprocess1 = document.getElementById('textprocess1' + String(index));
      textprocess1.style.overflow = 'hidden';
      textprocess1.style.height = 'auto';
      textprocess1.style.height = textprocess1.scrollHeight + 'px';
    }

    if (test === 2) {
      const textprocess2 = document.getElementById('textprocess2' + String(index));
      textprocess2.style.overflow = 'hidden';
      textprocess2.style.height = 'auto';
      textprocess2.style.height = textprocess2.scrollHeight + 'px';
    }
  }

  autotext(index: any) {
    if (index === 1) {
      const textprofit = document.getElementById('textprofit');
      textprofit.style.overflow = 'hidden';
      textprofit.style.height = 'auto';
      textprofit.style.height = textprofit.scrollHeight + 'px';
    }

    if (index === 2) {
      const textrou = document.getElementById('textrou');
      textrou.style.overflow = 'hidden';
      textrou.style.height = 'auto';
      textrou.style.height = textrou.scrollHeight + 'px';
    }

    if (index === 3) {
      const textroroc = document.getElementById('textroroc');
      textroroc.style.overflow = 'hidden';
      textroroc.style.height = 'auto';
      textroroc.style.height = textroroc.scrollHeight + 'px';
    }
    if (index === 4) {
      const textbreack = document.getElementById('textbreack');
      textbreack.style.overflow = 'hidden';
      textbreack.style.height = 'auto';
      textbreack.style.height = textbreack.scrollHeight + 'px';
    }
  }

  // ngOnDestroy(): void {
  //   const data = this.sidebarService.getsavePage();
  //   let saveInFoStatus = false;
  //   saveInFoStatus = data.saveStatusInfo;
  //   if (saveInFoStatus === true) {
  //     this.outOfPageSave = true;
  //     this.linkTopage = data.goToLink;
  //     console.log('ngOnDestroy', this.linkTopage);
  //     this.saveDraft();
  //     this.sidebarService.resetSaveStatusInfo();
  //   }
  // }

  hideDownloadFile() {
    if (this.dataSource_file.data.length > 0) {
      this.downloadfile_by = [];

      for (let index = 0; index < this.dataSource_file.data.length; index++) {
        const element = this.dataSource_file.data[index];
        console.log('ele', element);

        if ('id' in element) {
          this.downloadfile_by.push('path');
        } else {
          this.downloadfile_by.push('base64');
        }
      }
    }
    console.log('by:', this.downloadfile_by);
  }

  // ngDoCheck() {
  //   if (this.openDoCheckc === true) {
  //     if (this.template_manage.caaStatus !== 'show') {
  //       this.pageChange = this.sidebarService.changePageGoToLink();
  //       if (this.pageChange) {
  //         // this.getNavbar();
  //         const data = this.sidebarService.getsavePage();
  //         let saveInFoStatus = false;
  //         saveInFoStatus = data.saveStatusInfo;
  //         if (saveInFoStatus === true) {
  //           this.outOfPageSave = true;
  //           this.linkTopage = data.goToLink;
  //           this.saveDraft('navbar', null);
  //           this.sidebarService.resetSaveStatu();
  //         }
  //       }
  //     }
  //   }
  // }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate request');
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef =  this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        if (data.returnStatus === true) {
          const raturnStatus = this.saveDraft();
          console.log('request raturnStatus', raturnStatus);
            if (raturnStatus) {
              console.log('raturnStatus T', this.saveDraftstatus);
              subject.next(true);
            } else {
              console.log('raturnStatus F', this.saveDraftstatus);
              subject.next(false);
            }
        } else if (data.returnStatus === false) {
          this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
        }
      });
      return subject;
    } else {
      return true;
    }
  }

}
