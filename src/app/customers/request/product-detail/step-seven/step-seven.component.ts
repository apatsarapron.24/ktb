import { RequestService } from './../../../../services/request/request.service';
import { ReplaySubject } from 'rxjs';
import { StepSevenService } from '../../../../services/request/product-detail/step-seven/step-seven.service';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { CdkDragDrop, moveItemInArray, transferArrayItem, CdkDrag, CdkDragExit, CdkDragEnter, CdkDragStart } from '@angular/cdk/drag-drop';
import {
    AngularMyDatePickerDirective,
    IAngularMyDpOptions,
    IMyDateModel
} from 'angular-mydatepicker';
import { FormGroup, FormControl } from '@angular/forms';
import { DateComponent } from '../../../datepicker/date/date.component';
import { saveAs } from 'file-saver';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from '../../../../services/configGuard/config-guard.service';
import { DialogSaveStatusComponent } from '../../../request/dialog/dialog-save-status/dialog-save-status.component';
import { MatDialog } from '@angular/material';


@Component({
    selector: 'app-step-seven',
    templateUrl: './step-seven.component.html',
    styleUrls: ['./step-seven.component.scss']
})
export class StepSevenComponent implements OnInit, CanComponentDeactivate {
    @Input() template_manage = { role: null, validate: null, caaStatus: null, pageShow: null };
    @Output() saveStatus = new EventEmitter<boolean>();
    stepSeven: any = {
        validate: null,
        attachments: {
            file: [],
            fileDelete: []
        },
        dataFlowDiagram: {
            file: [],
            fileDelete: []
        }
        ,
        dataFlowDiagramText: null,
        impactAnalysis: {
            file: [],
            fileDelete: []
        },
        impactAnalysisText: null,
        networkDiagram: {
            file: [],
            fileDelete: []
        },
        networkDiagramText: null,
        parentRequestId: null,
        readiness: [
            {
                id: null,
                process: 'ความพร้อมของอุปกรณ์/กระบวนการ',
                detail: [
                    {
                        dueDate: '',
                        id: null,
                        management: '',
                        responsible: '',
                        subProcess: 'ความมั่นคงปลอดภัยด้านไซเบอร์ (Security & Cyber Risk)'
                    },
                    {
                        dueDate: '',
                        id: null,
                        management: '',
                        responsible: '',
                        subProcess: 'ความถูกต้องเชื่อถือได้ (Integrity)'
                    },
                    {
                        dueDate: '',
                        id: null,
                        management: '',
                        responsible: '',
                        subProcess: 'ความพร้อมใช้ของบริการ (Availability)'
                    },
                ],
                detailDelete: []
            },
            {
                id: null,
                process: 'การคุ้มครองข้อมูลส่วนบุคคล (Data Privacy)',
                detail: [
                    {
                        dueDate: '',
                        id: null,
                        management: '',
                        responsible: '',
                        subProcess: ''
                    },
                ],
                detailDelete: []
            },
        ],
        readinessDelete: [],
        requestId: null,
        status: false,
        ux: {
            file: [],
            fileDelete: []
        },
        uxText: null,
        conclusions: null
    };

    addData = {
        readiness: []
    };

    dropIndex: number;
    staticTitleIndex = [];

    name: any;
    status_loading = true;


    displayedColumns_file: string[] = ['name', 'size', 'delete'];
    dataSource_file: MatTableDataSource<FileList>;
    saveDraftstatus = null;

    outOfPageSave = false;
    linkTopage = null;
    pageChange = null;
    openDoCheckc = false;

    showCircle_colId = null;
    showCircle_id = null;
    enter = 3;



    agreement: any;
    file1_1: any;
    file1_2: any;
    file2_1: any;
    file2_2: any;
    file3_1: any;
    file3_2: any;
    file4_1: any;
    file4_2: any;
    document: any;
    Agreement = null;
    file_name: any;
    file_size: number;

    imgURL: any;
    image1 = [];
    image2 = [];
    image3 = [];
    image4 = [];
    showImage1 = false;
    message = null;

    urls: any = [];
    validate = null;
    // Model
    showModel = false;
    modalmainImageIndex: number;
    modalmainImagedetailIndex: number;
    indexSelect: number;
    modalImage = [];
    modalImagedetail = [];
    fileValue: number;

    // textareaForm = new FormGroup({
    //     uxText: new FormControl(this.stepSeven.uxText),
    //     impactAnalysisText: new FormControl(this.stepSeven.impactAnalysisText),
    //     networkDiagramText: new FormControl(this.stepSeven.networkDiagramText),
    //     dataFlowDiagramText: new FormControl(this.stepSeven.dataFlowDiagramText),

    // });
    // coppy_paste_status: number = 0;
    FileOverSize: any;

    status_isupdate = false;
    downloadfile_by = [];

    @ViewChild(DateComponent) Date: DateComponent;

    // แบบ form ตาราง step7
    readiness: any = [
        {
            id: null,
            process: 'ความพร้อมของอุปกรณ์/กระบวนการ',
            detail: [
                {
                    dueDate: '',
                    id: null,
                    management: '',
                    responsible: '',
                    subProcess: 'ความมั่นคงปลอดภัยด้านไซเบอร์ (Security & Cyber Risk)'
                },
                {
                    dueDate: '',
                    id: null,
                    management: '',
                    responsible: '',
                    subProcess: 'ความถูกต้องเชื่อถือได้ (Integrity)'
                },
                {
                    dueDate: '',
                    id: null,
                    management: '',
                    responsible: '',
                    subProcess: 'ความพร้อมใช้ของบริการ (Availability)'
                },
            ],
            detailDelete: []
        },
        {
            id: null,
            process: 'การคุ้มครองข้อมูลส่วนบุคคล (Data Privacy)',
            detail: [
                {
                    dueDate: '',
                    id: null,
                    management: '',
                    responsible: '',
                    subProcess: ''
                },
            ],
            detailDelete: []
        },
    ];
    constructor(
        private router: Router,
        public dialog: MatDialog,
        private stepSevenService: StepSevenService,
        private sidebarService: RequestService
    ) {
    }

    ngOnInit() {
        window.scrollTo(0, 0);
        this.openDoCheckc = true;
        if (localStorage) {
            if (localStorage.getItem('requestId')) {
                this.sidebarService.sendEvent();
                this.getDetail(localStorage.getItem('requestId'));
            } else {
                this.status_loading = false;
            }
        } else {
            // console.log('Brownser not support');
        }
        // this.addRow();
    }
    viewNext() {
        this.router.navigate(['request/product-detail/step8']);
    }
    changeDate(date: any) {
        const SendI = this.Date.changeDate(date);
        this.changeSaveDraft();
        return SendI;
    }

    autogrow(number: any) {
        if (number === 1) {
            const uxText = document.getElementById('uxText');
            uxText.style.overflow = 'hidden';
            uxText.style.height = 'auto';
            uxText.style.height = uxText.scrollHeight + 'px';
        }

        if (number === 2) {
            const impactAnalysisText = document.getElementById('impactAnalysisText');
            impactAnalysisText.style.overflow = 'hidden';
            impactAnalysisText.style.height = 'auto';
            impactAnalysisText.style.height = impactAnalysisText.scrollHeight + 'px';
        }
        if (number === 3) {
            const networkDiagramText = document.getElementById('networkDiagramText');
            networkDiagramText.style.overflow = 'hidden';
            networkDiagramText.style.height = 'auto';
            networkDiagramText.style.height = networkDiagramText.scrollHeight + 'px';
        }
        if (number === 4) {
            const dataFlowDiagramText = document.getElementById('dataFlowDiagramText');
            dataFlowDiagramText.style.overflow = 'hidden';
            dataFlowDiagramText.style.height = 'auto';
            dataFlowDiagramText.style.height = dataFlowDiagramText.scrollHeight + 'px';
        }
    }
    sendSaveStatus() {
        if (this.saveDraftstatus === 'success') {
            this.saveStatus.emit(true);
        } else {
            this.saveStatus.emit(false);
        }
    }

    // ====================== function table ============================
    checkStaticTitle(index) {
        return this.staticTitleIndex.includes(index);
    }

    drop(event: CdkDragDrop<string[]>) {
        const fixedBody = this.staticTitleIndex;

        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
        } else if (event.previousContainer.data.length > 1) {
            // console.log(event.previousContainer);
            // check if only one left child
            transferArrayItem(event.previousContainer.data,
                event.container.data,
                event.previousIndex,
                event.currentIndex);
        } else if (fixedBody.includes(this.dropIndex)) {
            // check only 1 child left and static type move only not delete
            // console.log(event.previousContainer);
            // console.log('check only 1 child left and static type move only not delete');
            transferArrayItem(event.previousContainer.data,
                event.container.data,
                event.previousIndex,
                event.currentIndex);

            const templateSubprocess = {
                subProcess: '',
                management: '',
                responsible: '',
                dueDate: null
            };
            this.stepSeven.readiness[this.dropIndex].detail.push(templateSubprocess);
        } else {
            // check only 1 child left and not a static type move and delete
            // console.log('check only 1 child left and not a static type move and delete');
            transferArrayItem(event.previousContainer.data,
                event.container.data,
                event.previousIndex,
                event.currentIndex);

            // remove current row
            this.removeAddItem(this.dropIndex);
            this.dropIndex = null;
        }
        this.changeSaveDraft();
    }

    dragExited(event: CdkDragExit<string[]>) {
        // console.log('Exited', event);
    }

    entered(event: CdkDragEnter<string[]>) {
        // console.log('Entered', event);
    }

    startDrag(event: CdkDragStart<string[]>, istep) {
        // console.log('startDrag', event);
        // console.log('startDrag index', istep);
        this.dropIndex = istep;
    }

    removeAddItem(index) {
        if (index != null) {
            this.stepSeven.readiness.splice(index, 1);
        }
    }


    addRow() {
        const tempateAddRow = {
            process: '',
            detail: [
                {
                    subProcess: '',
                    management: '',
                    responsible: '',
                    dueDate: null
                }
            ],
            detailDelete: []
        };
        this.stepSeven.readiness.push(tempateAddRow);
        // console.log('this.stepSeven.loanCriteria', this.stepSeven.readiness);
    }

    removeDataItem(stepindex, subindex) {
        // console.log('stepindex', stepindex);
        // console.log('subindex', subindex);
        const fixedBody = [0];
        if (fixedBody.includes(stepindex) && this.stepSeven.readiness[stepindex].detail.length > 1) {
            // console.log('1 delete fixed row only');
            // console.log('delete fixed row only', this.stepSeven.readiness[stepindex].detail[subindex]['id']);
            this.stepSeven.readiness[stepindex].detailDelete.push(this.stepSeven.readiness[stepindex].detail[subindex]['id']);
            this.stepSeven.readiness[stepindex].detail.splice(subindex, 1);
        } else if (fixedBody.includes(stepindex)) {
            // console.log('3 clear fixed row only');
            this.Date.datepicker_input['dateData'] = null;
            this.Date.endDate = null;
            this.stepSeven.readiness[stepindex].process = null;
            // console.log(this.stepSeven.readiness[stepindex].process);
            Object.keys(this.stepSeven.readiness[stepindex].detail[subindex]).forEach((key, index) => {
                this.stepSeven.readiness[stepindex].detail[subindex][key] = '';
            });
            this.stepSeven.readiness[stepindex].detailDelete.push(this.stepSeven.readiness[stepindex].detail[subindex]['id']);
        } else if (this.stepSeven.readiness[stepindex].detail.length > 1) {
            // console.log(' delete sub row only');
            this.stepSeven.readiness[stepindex].detailDelete.push(this.stepSeven.readiness[stepindex].detail[subindex]['id']);
            this.stepSeven.readiness[stepindex].detail.splice(subindex, 1);
        } else if (this.stepSeven.readiness[stepindex].detail.length === 1) {
            // console.log('delete row permanent', this.stepSeven.readiness[stepindex].id);
            this.stepSeven.readinessDelete.push(this.stepSeven.readiness[stepindex].id);
            this.stepSeven.readiness.splice(stepindex, 1);
        } else {
            // console.log('not in case');
        }
        this.changeSaveDraft();
    }

    changeSaveDraft() {
        this.saveDraftstatus = null;
        this.sendSaveStatus();
        this.sidebarService.inPageStatus(true);
    }


    // ====================== function text image ============================

    getDetail(requestId) {
        let _readiness: any;
        // console.log('getDetail id', requestId);
        this.stepSevenService.getDetail(requestId, this.template_manage.pageShow).subscribe(res => {
            console.log('res data : ', res);
            _readiness = this.stepSeven.readiness;

            if (res['status'] === 'success') {
                // console.log('res data : ', res['data']);
                this.status_loading = false;
                this.status_isupdate = res['isUpdate'];

                if (res['data'] !== null) {
                    console.log('pppppppppp', res['data']);
                    this.stepSeven = res['data'];
                    this.validate = this.stepSeven.validate;
                    console.log(this.validate);
                    // if (this.status_isupdate === true) {
                    // if (this.stepSeven.readiness.length > 0) {
                    //     for (let i = 0; i < this.stepSeven.readiness.length; i++) {
                    //         if (this.stepSeven.readiness[i].detail.length === 0) {
                    //             this.stepSeven.readiness[i].detail.push({
                    //                 dueDate: '',
                    //                 id: null,
                    //                 management: '',
                    //                 responsible: '',
                    //                 subProcess: ''
                    //             });
                    //         }
                    //     }
                    // } else {
                    //     this.stepSeven.readiness = _readiness;
                    // }

                    // }
                    // set fileDelete to Object stepSeven
                    this.stepSeven.ux.fileDelete = [];
                    this.stepSeven.impactAnalysis.fileDelete = [];
                    this.stepSeven.networkDiagram.fileDelete = [];
                    this.stepSeven.dataFlowDiagram.fileDelete = [];
                    this.stepSeven.attachments.fileDelete = [];

                    // this.textareaForm = new FormGroup({
                    //     uxText: new FormControl(this.stepSeven.uxText),
                    //     impactAnalysisText: new FormControl(this.stepSeven.impactAnalysisText),
                    //     networkDiagramText: new FormControl(this.stepSeven.networkDiagramText),
                    //     dataFlowDiagramText: new FormControl(this.stepSeven.dataFlowDiagramText),
                    // });

                    // if (this.coppy_paste_status === 0) {
                    //     document.getElementById('uxText').addEventListener('paste', function (e: ClipboardEvent) {
                    //         e.preventDefault();
                    //         const text = e.clipboardData.getData('text/plain');
                    //         document.execCommand('insertHTML', false, text);
                    //     });

                    //     document.getElementById('impactAnalysisText').addEventListener('paste', function (e: ClipboardEvent) {
                    //         e.preventDefault();
                    //         const text = e.clipboardData.getData('text/plain');
                    //         document.execCommand('insertHTML', false, text);
                    //     });

                    //     document.getElementById('networkDiagramText').addEventListener('paste', function (e: ClipboardEvent) {
                    //         e.preventDefault();
                    //         const text = e.clipboardData.getData('text/plain');
                    //         document.execCommand('insertHTML', false, text);
                    //     });

                    //     document.getElementById('dataFlowDiagramText').addEventListener('paste', function (e: ClipboardEvent) {
                    //         e.preventDefault();
                    //         const text = e.clipboardData.getData('text/plain');
                    //         document.execCommand('insertHTML', false, text);
                    //     });
                    // }

                    // set picture preview to all input
                    for (let i = 0; i < this.stepSeven.ux.file.length; i++) {
                        this.preview(1, this.stepSeven.ux.file[i], i);
                    }

                    for (let i = 0; i < this.stepSeven.impactAnalysis.file.length; i++) {
                        this.preview(2, this.stepSeven.impactAnalysis.file[i], i);
                    }

                    for (let i = 0; i < this.stepSeven.networkDiagram.file.length; i++) {
                        this.preview(3, this.stepSeven.networkDiagram.file[i], i);
                    }

                    for (let i = 0; i < this.stepSeven.dataFlowDiagram.file.length; i++) {
                        this.preview(4, this.stepSeven.dataFlowDiagram.file[i], i);
                    }

                    // set detailDelete Object stepSeven
                    // console.log('detailDelete', this.stepSeven.readiness);
                    if (this.status_isupdate === true) {
                        this.stepSeven.readiness.forEach((e, index) => {
                            this.stepSeven.readiness[index].detailDelete = [];
                        });
                    }
                    // set readinessDeleteto Object stepSeven
                    this.stepSeven.readinessDelete = [];

                    // set dateFormat
                    if (this.status_isupdate === true) {
                        this.stepSeven.readiness.forEach((elements, readinessIndex) => {
                            elements.detail.forEach((e, detailIndex) => {
                                // console.log('dueDate', e);
                                const dateIso = this.stepSeven.readiness[readinessIndex].detail[detailIndex]['dueDate'];
                                // console.log('dateIso', dateIso);
                                // const dateStamp = {isRange: false, singleDate: {jsDate: new Date(dateIso)}};
                                // console.log('dateStamp', dateStamp);
                                this.stepSeven.readiness[readinessIndex].detail[detailIndex].dueDate = dateIso;
                            });
                        });
                    }


                    // set file attachments
                    this.dataSource_file = new MatTableDataSource(this.stepSeven.attachments.file);
                    this.hideDownloadFile();
                    // set status to stepData
                    this.stepSeven.status = false;
                } else {
                    this.validate = res['validate'];
                    // console.log('data null');
                    // this.stepSeven.readiness
                }
            }
        }, err => {
            this.status_loading = false;
            // console.log(err);
        });
    }

    next_step() {
        this.router.navigate(['/request/product-detail/step8']);
    }

    saveDraft(page?, action?) {
        this.status_loading = true;
        if (localStorage.getItem('requestId')) {
            this.stepSeven.requestId = localStorage.getItem('requestId');

        }
        if (this.status_isupdate === true) {
            this.stepSeven.readiness.forEach((elements, operationsIndex) => {
                elements.detail.forEach((e, detailIndex) => {
                    if (Object.keys(elements.process).length === 0
                        && Object.keys(e.subProcess).length === 0
                        && Object.keys(e.management).length === 0
                        && Object.keys(e.responsible).length === 0
                        // && Object.keys(e.dueDate).length === 0
                    ) {
                        this.stepSeven.readinessDelete.push(this.stepSeven.readiness[operationsIndex].id);
                        this.stepSeven.readiness.splice(operationsIndex, 1);
                    } else {
                        if (Object.keys(elements.process).length === 0
                            || Object.keys(e.subProcess).length === 0
                            || Object.keys(e.management).length === 0
                            || Object.keys(e.responsible).length === 0
                            || (this.stepSeven.uxText === '' || this.stepSeven.uxText === null)
                            || (this.stepSeven.impactAnalysisText === '' || this.stepSeven.impactAnalysisText === null)
                            || (this.stepSeven.networkDiagramText === '' || this.stepSeven.networkDiagramText === null)
                            || (this.stepSeven.dataFlowDiagramText === '' || this.stepSeven.dataFlowDiagramText === null)
                        ) {
                            this.saveDraftstatus = false;
                        } else {
                            this.saveDraftstatus = true;
                        }
                        this.stepSeven.status = this.saveDraftstatus;
                    }
                });
            });
        } else if (this.status_isupdate === false) {
            this.stepSeven.readiness = [];
            this.stepSeven.status = true;
        }

        if (this.template_manage.pageShow === 'summary') {
            this.stepSeven.conclusions = true;
        } else {
            this.stepSeven.conclusions = null;
        }

        console.log(this.stepSeven);
        const returnUpdateData = new Subject<any>();
        this.stepSevenService.updatePage(this.stepSeven).subscribe(res => {
            console.log(res);
            // console.log(res['status']);
            if (res['status'] === 'success') {
                this.status_loading = false;
                this.downloadfile_by = [];
                this.sidebarService.sendEvent();
                this.saveDraftstatus = 'success';
                returnUpdateData.next(true);
                this.sidebarService.inPageStatus(false);
                setTimeout(() => {
                    this.saveDraftstatus = 'fail';
                }, 3000);
                this.sidebarService.resetSaveStatusInfo();

                this.stepSeven.attachments.file = [];
                this.stepSeven.dataFlowDiagram.file = [];
                this.stepSeven.impactAnalysis.file = [];
                this.stepSeven.networkDiagram.file = [];
                this.stepSeven.ux.file = [];
                this.image1 = [];
                this.image2 = [];
                this.image3 = [];
                this.image4 = [];
                this.status_loading = false;
                this.sendSaveStatus();
                this.getDetail(localStorage.getItem('requestId'));
            } else {
                this.sendSaveStatus();
                this.status_loading = false;
                alert(res['message']);
                returnUpdateData.next(false);
                // alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
            }
        }, err => {
            this.status_loading = false;
            console.log(err);
            this.sendSaveStatus();
            alert(err['message']);
            returnUpdateData.next(false);
            // alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
        }
        );
        return returnUpdateData;
    }

    delete_image(fileIndex, index) {
        // console.log('fileIndex:', fileIndex);
        // console.log('index:', index);
        if (fileIndex === 1) {
            if ('id' in this.stepSeven.ux.file[index]) {
                const id = this.stepSeven.ux.file[index].id;
                // console.log('found id');
                if ('fileDelete' in this.stepSeven.ux) {
                    this.stepSeven.ux.fileDelete.push(id);
                }
            }
            // console.log('fileDelete ux : ', this.stepSeven.ux.fileDelete);
            this.stepSeven.ux.file.splice(index, 1);
            this.image1.splice(index, 1);

            // console.log('this.stepSeven.ux.file.length2:', this.stepSeven.ux.file.length);
            for (let i = 0; i < this.stepSeven.ux.file.length; i++) {
                this.preview(fileIndex, this.stepSeven.ux.file[i], i);
            }
        } else if (fileIndex === 2) {
            if ('id' in this.stepSeven.impactAnalysis.file[index]) {
                const id = this.stepSeven.impactAnalysis.file[index].id;
                // console.log('found id');
                if ('fileDelete' in this.stepSeven.impactAnalysis) {
                    this.stepSeven.impactAnalysis.fileDelete.push(id);
                }
            }
            // console.log('fileDelete impactAnalysis : ', this.stepSeven.impactAnalysis.fileDelete);
            this.stepSeven.impactAnalysis.file.splice(index, 1);
            this.image2.splice(index, 1);

            // console.log('this.stepSeven.impactAnalysis.file.length2:', this.stepSeven.impactAnalysis.file.length);
            for (let i = 0; i < this.stepSeven.impactAnalysis.file.length; i++) {
                this.preview(fileIndex, this.stepSeven.impactAnalysis.file[i], i);
            }
        } else if (fileIndex === 3) {
            if ('id' in this.stepSeven.networkDiagram.file[index]) {
                const id = this.stepSeven.networkDiagram.file[index].id;
                // console.log('found id');
                if ('fileDelete' in this.stepSeven.networkDiagram) {
                    this.stepSeven.networkDiagram.fileDelete.push(id);
                }
            }
            // console.log('fileDelete networkDiagram : ', this.stepSeven.networkDiagram.fileDelete);
            this.stepSeven.networkDiagram.file.splice(index, 1);
            this.image3.splice(index, 1);

            // console.log('this.file.networkDiagram.uploadFile.length2:', this.stepSeven.networkDiagram.file.length);
            for (let i = 0; i < this.stepSeven.networkDiagram.file.length; i++) {
                this.preview(fileIndex, this.stepSeven.networkDiagram.file[i], i);
            }
        } else if (fileIndex === 4) {
            if ('id' in this.stepSeven.dataFlowDiagram.file[index]) {
                const id = this.stepSeven.dataFlowDiagram.file[index].id;
                // console.log('found id');
                if ('fileDelete' in this.stepSeven.dataFlowDiagram) {
                    this.stepSeven.dataFlowDiagram.fileDelete.push(id);
                }
            }
            // console.log('fileDelete dataFlowDiagram : ', this.stepSeven.dataFlowDiagram.fileDelete);
            this.stepSeven.dataFlowDiagram.file.splice(index, 1);
            this.image4.splice(index, 1);

            // console.log('this.file.dataFlowDiagram.uploadFile.length2:', this.stepSeven.dataFlowDiagram.file.length);
            for (let i = 0; i < this.stepSeven.dataFlowDiagram.file.length; i++) {
                this.preview(fileIndex, this.stepSeven.dataFlowDiagram.file[i], i);
            }
        }
        this.changeSaveDraft();
    }

    image_click2(colId, id) {
        this.showModel = true;
        // console.log('colId:', colId);
        // console.log('id:', id);
        // this.showCircle_colId = colId;
        // this.showCircle_id = id;
        document.getElementById('myModal7').style.display = 'block';
        this.imageModal(colId, id);
    }

    image_bdclick2() {
        document.getElementById('myModal7').style.display = 'block';
        // console.log('2222');
    }

    closeModal2() {
        this.showModel = false;
        document.getElementById('myModal7').style.display = 'none';
    }

    image_click(colId, id) {
        this.showModel = true;
        // console.log('colId:', colId);
        // console.log('id:', id);
        // this.showCircle_colId = colId;
        // this.showCircle_id = id;
        document.getElementById('myModal').style.display = 'block';
        this.imageModal(colId, id);
    }

    image_bdclick() {
        document.getElementById('myModal').style.display = 'block';
        // console.log('2222');
    }

    closeModal() {
        this.showModel = false;
        document.getElementById('myModal').style.display = 'none';
    }

    // getFileDetails ================================================================
    getFileDetails(fileIndex, event) {
        const files = event.target.files;

        for (let i = 0; i < files.length; i++) {
            const mimeType = files[i].type;
            // console.log(mimeType);
            if (mimeType.match('image/jpeg|image/png') == null) {
                this.message = 'Only images are supported.';
                this.status_loading = false;
                alert(this.message);
                return;
            }
            const file = files[i];
            // this.file_name = file.name;
            // this.file_type = file.type;
            // this.file_size = file.size;
            // this.modifiedDate = file.lastModifiedDate;
            // console.log('file:', file);
            const reader = new FileReader();
            reader.onload = (_event) => {
                const rawReadFile = reader.result as string;
                const splitFile = rawReadFile.split(',');

                const templateFile = {
                    fileName: file.name,
                    base64File: splitFile[1],
                };
                // console.log(templateFile);

                if (fileIndex === 1) {
                    // console.log('00000000000000000000000000000001');
                    this.stepSeven.ux.file.push(templateFile);
                    // console.log('this.stepSeven.ux.file : ' , this.stepSeven.ux.file)

                    for (let index = 0; index < this.stepSeven.ux.file.length; index++) {
                        this.preview(fileIndex, this.stepSeven.ux.file[index], index);
                    }
                    this.file1_1 = undefined;
                    this.file1_2 = undefined;
                } else if (fileIndex === 2) {
                    // console.log('00000000000000000000000000000002');
                    // console.log('templateFile : ', templateFile);
                    this.stepSeven.impactAnalysis.file.push(templateFile);

                    for (let index = 0; index < this.stepSeven.impactAnalysis.file.length; index++) {
                        this.preview(fileIndex, this.stepSeven.impactAnalysis.file[index], index);
                        this.file2_1 = undefined;
                        this.file2_2 = undefined;
                    }
                } else if (fileIndex === 3) {
                    // console.log('00000000000000000000000000000003');
                    this.stepSeven.networkDiagram.file.push(templateFile);

                    for (let index = 0; index < this.stepSeven.networkDiagram.file.length; index++) {
                        this.preview(fileIndex, this.stepSeven.networkDiagram.file[index], index);
                    }
                    this.file3_1 = undefined;
                    this.file3_2 = undefined;
                } else if (fileIndex === 4) {
                    // console.log('00000000000000000000000000000004');
                    this.stepSeven.dataFlowDiagram.file.push(templateFile);

                    for (let index = 0; index < this.stepSeven.dataFlowDiagram.file.length; index++) {
                        this.preview(fileIndex, this.stepSeven.dataFlowDiagram.file[index], index);
                    }
                    this.file4_1 = undefined;
                    this.file4_2 = undefined;
                }

            };
            reader.readAsDataURL(file);

        }
        this.changeSaveDraft();
    }

    preview(fileIndex, file, index) {
        //  console.log('file : ', file);

        const name = file.fileName;
        const typeFiles = name.match(/\.\w+$/g);
        const typeFile = typeFiles[0].replace('.', '');

        const tempPicture = `data:image/${typeFile};base64,${file.base64File}`;

        if (fileIndex === 1) {
            if (file) {
                this.image1[index] = tempPicture;
            }
        } else if (fileIndex === 2) {
            if (file) {
                this.image2[index] = tempPicture;
            }
        } else if (fileIndex === 3) {
            if (file) {
                this.image3[index] = tempPicture;
            }
        } else if (fileIndex === 4) {
            if (file) {
                this.image4[index] = tempPicture;
            }
        }
    }

    // getFileDocument ================================================================
    getFileDocument(event) {
        const file = event.target.files[0];
        const mimeType = file.type;
        if (mimeType.match(/pdf\/*/) == null) {
            this.message = 'Only PDF are supported.';
            this.status_loading = false;
            alert(this.message);
            return;
        }
        // console.log('11111', file);
        this.file_name = file.name;
        // this.file_type = file.type;
        this.file_size = file.size / 1024;
        // this.modifiedDate = file.lastModifiedDate;
        // console.log('file:', file);

        this.stepSeven.attachments = file;
        // this.document = undefined;
        this.changeSaveDraft();
    }

    onSelectFile(event) {
        this.FileOverSize = [];
        if (event.target.files && event.target.files[0]) {
            const filesAmount = event.target.files.length;
            const file = event.target.files;
            for (let i = 0; i < filesAmount; i++) {
                // console.log('type:', file[i]);
                if (
                    // file[i].type === 'image/svg+xml' ||
                    file[i].type === 'image/jpeg' ||
                    // file[i].type === 'image/raw' ||
                    // file[i].type === 'image/svg' ||
                    // file[i].type === 'image/tif' ||
                    // file[i].type === 'image/gif' ||
                    file[i].type === 'image/jpg' ||
                    file[i].type === 'image/png' ||
                    file[i].type === 'application/doc' ||
                    file[i].type === 'application/pdf' ||
                    file[i].type ===
                    'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
                    file[i].type ===
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
                    file[i].type ===
                    'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
                    file[i].type === 'application/pptx' ||
                    file[i].type === 'application/docx' ||
                    (file[i].type === 'application/ppt' && file[i].size)
                ) {
                    if (file[i].size <= 5000000) {
                        this.multiple_file(file[i], i);
                    } else {
                        this.FileOverSize.push(file[i].name);
                    }
                } else {
                    this.status_loading = false;
                    alert('File Not Support');
                }
            }

            if (this.FileOverSize.length !== 0) {
                // console.log('open modal');
                document.getElementById('testttt').click();
            }
        }
    }

    multiple_file(file, index) {
        // console.log('download');
        const reader = new FileReader();
        if (file) {
            reader.readAsDataURL(file);
            reader.onload = (_event) => {
                const rawReadFile = reader.result as string;
                const splitFile = rawReadFile.split(',');

                const templateFile = {
                    fileName: file.name,
                    fileSize: file.size,
                    base64File: splitFile[1],
                };
                this.stepSeven.attachments.file.push(templateFile);
                this.dataSource_file = new MatTableDataSource(this.stepSeven.attachments.file);
                this.hideDownloadFile();
            };
        }
        this.changeSaveDraft();
    }

    delete_mutifile(data: any, index: any) {
        // console.log('data:', data, 'index::', index);
        if ('id' in this.stepSeven.attachments.file[index]) {
            const id = this.stepSeven.attachments.file[index].id;
            // console.log('found id');
            if ('fileDelete' in this.stepSeven.attachments) {
                this.stepSeven.attachments.fileDelete.push(id);
            }
        }
        // console.log('fileDelete attachments : ', this.stepSeven.attachments.fileDelete);
        this.stepSeven.attachments.file.splice(index, 1);
        this.dataSource_file = new MatTableDataSource(this.stepSeven.attachments.file);
        this.changeSaveDraft();
    }


    // ============================== Modal ==========================================
    imageModal(fileIndex, index) {
        if (fileIndex === 1) {
            this.modalmainImageIndex = index;
            this.modalImage = this.image1;
            this.modalImagedetail = this.stepSeven.ux.file;
            this.indexSelect = index;
            this.fileValue = fileIndex;
        } else if (fileIndex === 2) {
            this.modalmainImageIndex = index;
            this.modalImage = this.image2;
            this.modalImagedetail = this.stepSeven.impactAnalysis.file;
            this.indexSelect = index;
            this.fileValue = fileIndex;
        } else if (fileIndex === 3) {
            this.modalmainImageIndex = index;
            this.modalImage = this.image3;
            this.modalImagedetail = this.stepSeven.networkDiagram.file;
            this.indexSelect = index;
            this.fileValue = fileIndex;
        } else if (fileIndex === 4) {
            this.modalmainImageIndex = index;
            this.modalImage = this.image4;
            this.modalImagedetail = this.stepSeven.dataFlowDiagram.file;
            this.indexSelect = index;
            this.fileValue = fileIndex;
        }
    }

    // ===============================================================================

    covertDate(datePC: any) {
        if (datePC != null) {
            const year = Number(datePC[0] + datePC[1] + datePC[2] + datePC[3]) + 543;
            const Date =
                datePC[8] + datePC[9] + '/' + datePC[5] + datePC[6] + '/' + year;
            return Date;
        } else {
            return null;
        }
    }
    downloadFile(pathdata: any) {
        this.status_loading = true;
        const contentType = '';
        const sendpath = pathdata;
        console.log('path', sendpath);
        this.sidebarService.getfile(sendpath).subscribe(res => {
            if (res['status'] === 'success' && res['data']) {
                this.status_loading = false;
                const datagetfile = res['data'];
                const b64Data = datagetfile['data'];
                const filename = datagetfile['fileName'];
                console.log('base64', b64Data);
                const config = this.b64toBlob(b64Data, contentType);
                saveAs(config, filename);
            }

        });
    }

    b64toBlob(b64Data, contentType = '', sliceSize = 512) {
        const convertbyte = atob(b64Data);
        const byteCharacters = atob(convertbyte);
        const byteArrays = [];
        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            const slice = byteCharacters.slice(offset, offset + sliceSize);

            const byteNumbers = new Array(slice.length);
            for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            const byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }

        const blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }

    downloadFile64(data: any) {
        this.status_loading = true;
        const contentType = '';
        this.status_loading = false;
        const b64Data = data.base64File;
        const filename = data.fileName;
        console.log('base64', b64Data);

        const config = this.base64(b64Data, contentType);
        saveAs(config, filename);
    }

    base64(b64Data, contentType = '', sliceSize = 512) {
        const byteCharacters = atob(b64Data);
        const byteArrays = [];
        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            const slice = byteCharacters.slice(offset, offset + sliceSize);

            const byteNumbers = new Array(slice.length);
            for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            const byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }

        const blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }

    autoGrowTextZone(e) {
        e.target.style.height = '0px';
        e.target.style.height = (e.target.scrollHeight + 5) + 'px';
    }


    //   ngOnDestroy(): void {
    //     const data = this.sidebarService.getsavePage();
    //     let saveInFoStatus = false;
    //     saveInFoStatus = data.saveStatusInfo;
    //     if (saveInFoStatus === true) {
    //       this.outOfPageSave = true;
    //       this.linkTopage = data.goToLink;
    //       console.log('ngOnDestroy', this.linkTopage);
    //       this.saveDraft();
    //       this.sidebarService.resetSaveStatusInfo();
    //     }
    //   }

    hideDownloadFile() {
        if (this.dataSource_file.data.length > 0) {
            this.downloadfile_by = [];

            for (let index = 0; index < this.dataSource_file.data.length; index++) {
                const element = this.dataSource_file.data[index];
                console.log('ele', element);

                if ('id' in element) {
                    this.downloadfile_by.push('path');
                } else {
                    this.downloadfile_by.push('base64');
                }
            }
        }
        console.log('by:', this.downloadfile_by);
    }

    // ngDoCheck() {
    //     if (this.openDoCheckc === true) {
    //         if (this.template_manage.caaStatus !== 'show') {
    //             this.pageChange = this.sidebarService.changePageGoToLink();
    //             if (this.pageChange) {
    //                 // this.getNavbar();
    //                 const data = this.sidebarService.getsavePage();
    //                 let saveInFoStatus = false;
    //                 saveInFoStatus = data.saveStatusInfo;
    //                 if (saveInFoStatus === true) {
    //                     this.outOfPageSave = true;
    //                     this.linkTopage = data.goToLink;
    //                     this.saveDraft('navbar', null);
    //                     this.sidebarService.resetSaveStatu();
    //                 }
    //             }
    //         }
    //     }
    // }

    canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
        console.log('canDeactivate request');
        const subject = new Subject<boolean>();
        const status = this.sidebarService.outPageStatus();
        if (status === true) {
            // -----
            const dialogRef = this.dialog.open(DialogSaveStatusComponent, {
                disableClose: true,
                data: {
                    headerDetail: 'ยืนยันการดำเนินการ',
                    bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
                    bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
                    returnDetail: 'null',
                    returnStatus: false,
                    icon: 'assets/img/Alarm-blue.svg',
                }
            });
            dialogRef.afterClosed().subscribe((data) => {
                if (data.returnStatus === true) {
                    const raturnStatus = this.saveDraft();
                    console.log('request raturnStatus', raturnStatus);
                    if (raturnStatus) {
                        console.log('raturnStatus T', this.saveDraftstatus);
                        subject.next(true);
                    } else {
                        console.log('raturnStatus F', this.saveDraftstatus);
                        subject.next(false);
                    }
                } else if (data.returnStatus === false) {
                    this.sidebarService.inPageStatus(false);
                    subject.next(true);
                    return true;
                } else {
                    subject.next(false);
                }
            });
            return subject;
        } else {
            return true;
        }
    }



}
