import { forEach } from '@angular/router/src/utils/collection';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { StepElevenService } from './../../../../services/request/product-detail/step-eleven/step-eleven.service';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { RequestService } from './../../../../services/request/request.service';
import { saveAs } from 'file-saver';
import { nullSafeIsEquivalent } from '@angular/compiler/src/output/output_ast';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogSaveStatusComponent } from '../../dialog/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from '../../../../services/configGuard/config-guard.service';

export interface FileList {
  fileName: string;
  fileSize: string;
  base64File: string;
}

@Component({
  selector: 'app-step-eleven',
  templateUrl: './step-eleven.component.html',
  styleUrls: ['./step-eleven.component.scss']
})
export class StepElevenComponent implements OnInit, CanComponentDeactivate {
  @Input() template_manage = { role: null, validate: null, caaStatus: null, pageShow: null };
  @Output() saveStatus = new EventEmitter<boolean>();
  allowSave = false;
  stepEleven = {
    validate: null,
    complianceItems: [],
    onProcess: false,
    attachments: {
      file: [],
      fileDelete: []
    }
  };

  caseAllowList = [
    {
      title: 'ต้องขออนุญาต',
      value: 'ต้องขออนุญาต'
    },
    {
      title: 'ไม่ต้องขออนุญาต',
      value: 'ไม่ต้องขออนุญาต'
    },
    {
      title: 'ต้องหารือ ธปท.',
      value: 'ต้องหารือ ธปท.'
    }
  ];

  caseNotAllowList = [
    {
      title: 'อนุญาตเป็นการทั่วไป',
      value: 'อนุญาตเป็นการทั่วไป'
    },
    {
      title: 'อนุญาตเป็นการทั่วไป และมีเงื่อนไขให้ปฏิบัติก่อนเริ่มดำเนินการ',
      value: 'อนุญาตเป็นการทั่วไป และมีเงื่อนไขให้ปฏิบัติก่อนเริ่มดำเนินการ'
    },
    {
      title: 'อนุญาตเป็นการทั่วไป แต่ต้องมีหนังสือแจ้งให้ทราบก่อนเริ่มดำเนินการ',
      value: 'อนุญาตเป็นการทั่วไป แต่ต้องมีหนังสือแจ้งให้ทราบก่อนเริ่มดำเนินการ'
    },
    {
      title: 'เคยได้รับอนุญาตแล้ว',
      value: 'เคยได้รับอนุญาตแล้ว'
    }
  ];

  saveDraftstatus = '';
  outOfPageSave = false;
  linkTopage = null;
  pageChange = null;
  openDoCheckc = false;

  message: string;

  document: any;
  file_name: any;
  file_size: number;

  results: any;
  attachments: any = {
    file: [],
    fileDelete: []
  };
  complianceItemsDelete: any = [];
  status_loading = true;

  file: any;
  displayedColumns_file: string[] = ['name', 'size', 'delete'];
  dataSource_file: MatTableDataSource<FileList>;
  api_response: any;

  FileOverSize: any;
  validate = null;
  downloadfile_by = [];

  constructor(private router: Router,
    public stepElevenService: StepElevenService,
    private sidebarService: RequestService,
    public dialog: MatDialog
  ) { }

  sendSaveStatus() {
    if (this.saveDraftstatus === 'success') {
      this.saveStatus.emit(true);
    } else {
      this.saveStatus.emit(false);
    }
  }

  changeSaveDraft() {
    this.saveDraftstatus = null;
    this.sendSaveStatus();
    this.sidebarService.inPageStatus(true);
  }

  next_step() {
    this.router.navigate(['/request/product-detail/step12']);
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.openDoCheckc = true;
    if (localStorage.getItem('requestId')) {
      this.sidebarService.sendEvent();
    }

    if (localStorage) {
      if (localStorage.getItem('requestId')) {
        this.getDetail(localStorage.getItem('requestId'));
      } else {
        this.status_loading = false;
      }
    } else {
      console.log('Brownser not support');
    }
  }
  viewNext() {
    this.router.navigate(['request/product-detail/step12']);
  }
  getDetail(id) {
    this.status_loading = true;
    this.stepElevenService.getDetail(localStorage.getItem('requestId'), this.template_manage.pageShow).subscribe(res => {
      console.log(res);
      if (res['status'] === 'success') {
        this.status_loading = false;
        this.validate = res['validate'];
      } else {
        this.status_loading = false;
      }
      if (res['data'] !== null) {
        this.stepEleven = res['data'];
        console.log(this.validate);
        console.log('444', this.stepEleven);
        if ((this.stepEleven.complianceItems === null
          || this.stepEleven.complianceItems.length === 0)
          && this.validate === true) {
          this.addRow();
        }
        for (let i = 0; i < this.stepEleven.attachments.file.length; i++) {
          this.attachments.file.push(this.stepEleven.attachments.file[i]);
        }
        this.dataSource_file = new MatTableDataSource(this.stepEleven['attachments'].file);
        this.hideDownloadFile();
      }
      if (res['data'] === null) {
        this.validate = res['validate'];
        this.stepEleven = this.stepEleven;
        if (this.validate === true) {
          this.addRow();
        }
        this.dataSource_file = new MatTableDataSource([]);
      }
      this.complianceItemsDelete = [];

    }, err => {
      this.status_loading = false;
      console.log(err);
    });
  }

  changeValueCheckbox() {
    console.log('');
  }

  addRow() {
    const template = {
      unit: '',
      bookNumber: '',
      topic: '',
      allow: null,
      other: null
    };

    this.stepEleven.complianceItems.push(template);

  }

  removeDataItem(index) {
    console.log(index);
    const template = {
      unit: '',
      bookNumber: '',
      topic: '',
      allow: null,
      other: null
    };
    const id = this.stepEleven.complianceItems[index].id;
    console.log('removeDataItem id: ', id);
    if (this.stepEleven.complianceItems.length === 1) {
      this.complianceItemsDelete.push(id);
      this.stepEleven.complianceItems[0] = template;
      console.log('1');
    } else {
      this.stepEleven.complianceItems.splice(index, 1);
      this.complianceItemsDelete.push(id);
      console.log(this.complianceItemsDelete);
    }
    this.changeSaveDraft();
  }

  onSelectFile(event) {
    this.FileOverSize = [];
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      const file = event.target.files;
      for (let i = 0; i < filesAmount; i++) {
        console.log('type:', file[i]);
        if (
          // file[i].type === 'image/svg+xml' ||
          file[i].type === 'image/jpeg' ||
          // file[i].type === 'image/raw' ||
          // file[i].type === 'image/svg' ||
          // file[i].type === 'image/tif' ||
          // file[i].type === 'image/gif' ||
          file[i].type === 'image/jpg' ||
          file[i].type === 'image/png' ||
          file[i].type === 'application/doc' ||
          file[i].type === 'application/pdf' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
          file[i].type === 'application/pptx' ||
          file[i].type === 'application/docx' ||
          (file[i].type === 'application/ppt' && file[i].size)
        ) {
          if (file[i].size <= 5000000) {
            this.multiple_file(file[i], i);
          } else {
            this.FileOverSize.push(file[i].name);
          }

        } else {
          this.status_loading = false;
          alert('File Not Support');
        }
      }
      if (this.FileOverSize.length !== 0) {
        console.log('open modal');
        document.getElementById('testttt').click();
      }
    }
  }

  multiple_file(file, index) {
    console.log('download');
    const reader = new FileReader();
    if (file) {
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');
        // this.urls.push({
        const x = {
          fileName: file.name,
          fileSize: file.size,
          base64File: splitFile[1],
        };
        // });
        this.attachments.file.push(x);
        // console.log('this.urls', this.attachments.file);
        this.dataSource_file = new MatTableDataSource(this.attachments.file);
        this.hideDownloadFile();
        console.log(this.attachments.file);
      };
    }
  }

  delete_mutifile(data: any, index: any) {
    console.log('data:', data, 'index::', index);
    if ('id' in this.attachments.file[index]) {
      const id = this.attachments.file[index].id;
      console.log('found id');
      if ('fileDelete' in this.attachments) {
        this.attachments.fileDelete.push(id);
      }
    }
    console.log('fileDelete attachments : ', this.attachments.fileDelete);
    this.attachments.file.splice(index, 1);
    this.dataSource_file = new MatTableDataSource(this.attachments.file);
    console.log(this.attachments);
    this.changeSaveDraft();
  }

  saveDraft(page?, action?) {
    this.status_loading = true;
    console.log(this.stepEleven);
    console.log('222', this.stepEleven.complianceItems);

    const dataSend = {
      requestId: localStorage.getItem('requestId'),
      complianceItems: this.stepEleven.complianceItems ? this.stepEleven.complianceItems : null,
      complianceItemsDelete: this.complianceItemsDelete ? this.complianceItemsDelete : '',
      onProcess: this.stepEleven.onProcess ? this.stepEleven.onProcess : '',
      attachments: this.attachments ? this.attachments : '',
      status: this.validateStatus(),
      conclusions: null
    };

    if (this.template_manage.pageShow === 'summary') {
      dataSend.conclusions = true;
    } else {
      dataSend.conclusions = null;
    }

    console.log('dataSend :', dataSend);
    // console.log('dd :', this.checkItem(dataSend.complianceItems));
    if (this.checkItem(dataSend.complianceItems)) {
      this.stepEleven.complianceItems.forEach((ele, index) => {
        if ((ele.unit === '' || ele.unit === null)
          && (ele.bookNumber === '' || ele.bookNumber === null)
          && (ele.topic === '' || ele.topic === null)
          && (ele.allow === '' || ele.allow === null)
          && (ele.other === '' || ele.other === null)
        ) {
          if (ele.id !== null) {
            this.complianceItemsDelete.push(ele.id);
          }
          dataSend.complianceItems.splice(index, 1);
        }
      });

      const returnUpdateData = new Subject<any>();
      this.status_loading = true;
      this.stepElevenService.updateDetail(dataSend).subscribe(res => {
        this.api_response = res;
        if (this.api_response.status === 'success') {
          this.status_loading = false;
          this.downloadfile_by = [];
          this.saveDraftstatus = 'success';
          returnUpdateData.next(true);
          this.sidebarService.inPageStatus(false);
          setTimeout(() => {
            this.saveDraftstatus = 'fail';
          }, 3000);
          this.attachments.file = [];
          this.stepEleven['attachments'].file = [];
          this.sidebarService.sendEvent();
          this.sendSaveStatus();
          this.getDetail(localStorage.getItem('requestId'));
        } else {
          this.status_loading = false;
          alert(res['message']);
          returnUpdateData.next(true);
        }
      }, err => {
        this.status_loading = false;
        console.log(err);
      });
      return returnUpdateData;
    } else {
      this.status_loading = false;
      return false;
    }
  }

  checkItem(complianceItems) {
    this.allowSave = true;
    for (let i = 0; i < complianceItems.length; i++) {
      if (complianceItems[i].allow === 'ไม่ต้องขออนุญาต') {
        if (complianceItems[i].other === '' || complianceItems[i].other === null) {
          this.allowSave = false;
          break;
        }
      }
    }

    if (this.allowSave == false) {
      alert('ระบุเหตุผล กรณีไม่ต้องขออนุญาต');
    }
    return this.allowSave;
  }

  validateStatus() {
    let status = true;
    const listkey = [];
    const complianceItems = this.stepEleven.complianceItems;
    const Items = [];
    // check all key is not null and empty string
    const keys = Object.keys(complianceItems[0]);
    keys.forEach(key => {
      if (key !== 'createdAt' && key !== 'updatedAt' && key !== 'deletedAt' && key !== 'id' && key !== 'other') {
        listkey.push(key);
      }
    });
    console.log('000 validateStatus=====', complianceItems);
    for (let index = 0; index < complianceItems.length; index++) {
      console.log('000 validateStatus >>>', index);
      if ((complianceItems[index].unit !== '' && complianceItems[index].unit !== null)
        || (complianceItems[index].bookNumber !== '' && complianceItems[index].bookNumber !== null)
        || (complianceItems[index].topic !== '' && complianceItems[index].topic !== null)
        || (complianceItems[index].allow !== '' && complianceItems[index].allow !== null)
        || (complianceItems[index].other !== '' && complianceItems[index].other !== null)
      ) {
        // ตัวไดตัวหนึ่งมีค่า
        console.log('000 validateStatus >>> 01');
        if ((complianceItems[index].unit !== '' && complianceItems[index].unit != null)
          && (complianceItems[index].bookNumber !== '' && complianceItems[index].bookNumber !== null)
          && (complianceItems[index].topic !== '' && complianceItems[index].topic !== null)
          && (complianceItems[index].allow !== '' && complianceItems[index].allow !== null)
        ) {
          // มีค่าถุกตัว
        } else {
          status = false;
        }
      }
      // for (let k = 0; k < listkey.length; k++) {
      //   console.log('778', complianceItems[index][listkey[k]] );
      //   if (complianceItems[index][listkey[k]] === '' || complianceItems[index][listkey[k]] === null) {
      //     status = false;
      //   } else {

      //   }
      // }
    }

    console.log('000 validateStatus >>> status', status);
    if (
      complianceItems && status) {
      return status = true;
    } else {
      return status = false;
    }

  }
  // ===============================================================================

  downloadFile(pathdata: any) {
    const contentType = '';
    const sendpath = pathdata;
    console.log('path', sendpath);
    this.sidebarService.getfile(sendpath).subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        this.status_loading = false;
        const datagetfile = res['data'];
        const b64Data = datagetfile['data'];
        const filename = datagetfile['fileName'];
        console.log('base64', b64Data);
        const config = this.b64toBlob(b64Data, contentType);
        saveAs(config, filename);
      }

    }, err => {
      this.status_loading = false;
      console.log(err);
    });
  }

  b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const convertbyte = atob(b64Data);
    const byteCharacters = atob(convertbyte);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  downloadFile64(data: any) {
    this.status_loading = true;
    const contentType = '';
    this.status_loading = false;
    const b64Data = data.base64File;
    const filename = data.fileName;
    console.log('base64', b64Data);

    const config = this.base64(b64Data, contentType);
    saveAs(config, filename);
  }

  base64(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }


  autotext(index: any, text: any) {
    if (text === 1) {
      const textgency = document.getElementById('textgency' + String(index));
      textgency.style.overflow = 'hidden';
      textgency.style.height = 'auto';
      textgency.style.height = textgency.scrollHeight + 'px';
    }
    if (text === 2) {
      const testsubject = document.getElementById('testsubject' + String(index));
      testsubject.style.overflow = 'hidden';
      testsubject.style.height = 'auto';
      testsubject.style.height = testsubject.scrollHeight + 'px';
    }
  }

  // ngOnDestroy(): void {
  //   const data = this.sidebarService.getsavePage();
  //   let saveInFoStatus = false;
  //   saveInFoStatus = data.saveStatusInfo;
  //   if (saveInFoStatus === true) {
  //     this.outOfPageSave = true;
  //     this.linkTopage = data.goToLink;
  //     console.log('ngOnDestroy', this.linkTopage);
  //     this.saveDraft();
  //     this.sidebarService.resetSaveStatusInfo();
  //   }
  // }

  hideDownloadFile() {
    if (this.dataSource_file.data.length > 0) {
      this.downloadfile_by = [];

      for (let index = 0; index < this.dataSource_file.data.length; index++) {
        const element = this.dataSource_file.data[index];
        console.log('ele', element);

        if ('id' in element) {
          this.downloadfile_by.push('path');
        } else {
          this.downloadfile_by.push('base64');
        }
      }
    }
    console.log('by:', this.downloadfile_by);
  }

  // ngDoCheck() {
  //   if (this.openDoCheckc === true) {
  //     if (this.template_manage.caaStatus !== 'show') {
  //       this.pageChange = this.sidebarService.changePageGoToLink();
  //       if (this.pageChange) {
  //         // this.getNavbar();
  //         const data = this.sidebarService.getsavePage();
  //         let saveInFoStatus = false;
  //         saveInFoStatus = data.saveStatusInfo;
  //         if (saveInFoStatus === true) {
  //           this.outOfPageSave = true;
  //           this.linkTopage = data.goToLink;
  //           this.saveDraft('navbar', null);
  //           this.sidebarService.resetSaveStatu();
  //         }
  //       }
  //     }
  //   }
  // }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate request');
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef = this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        if (data.returnStatus === true) {
          const raturnStatus = this.saveDraft();
          console.log('request raturnStatus', raturnStatus);
          if (raturnStatus) {
            console.log('raturnStatus T', this.saveDraftstatus);
            subject.next(true);
          } else {
            console.log('raturnStatus F', this.saveDraftstatus);
            subject.next(false);
          }
        } else if (data.returnStatus === false) {
          this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
        }
      });
      return subject;
    } else {
      return true;
    }
  }
}
