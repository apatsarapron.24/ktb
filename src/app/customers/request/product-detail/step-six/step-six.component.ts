import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {
    AngularMyDatePickerDirective,
    IAngularMyDpOptions,
    IMyDateModel,
} from 'angular-mydatepicker';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { StepSixService } from './../../../../services/request/product-detail/step-six/step-six.service';
import { RequestService } from '../../../../services/request/request.service';
import { DateComponent } from '../../../datepicker/date/date.component';
import { saveAs } from 'file-saver';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogSaveStatusComponent } from '../../dialog/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from '../../../../services/configGuard/config-guard.service';


export interface FileList {
    fileName: string;
    fileSize: string;
    base64File: string;
}


@Component({
    selector: 'app-step-six',
    templateUrl: './step-six.component.html',
    styleUrls: ['./step-six.component.scss']
})

export class StepSixComponent implements OnInit, CanComponentDeactivate {
    @Input() template_manage = { role: null, validate: null, caaStatus: null, pageShow: null };
    @Output() saveStatus = new EventEmitter<boolean>();
    displayedColumns: string[] = [
        'title1',
        'dont_create',
        'new_create',
        'old_doc',
        'explain1',
        'due_date1',
        'delete1'
    ];
    _validatecheck: any;
    status_loading = true;
    _nextstep: boolean;

    // MatPaginator Inputs

    description = '';

    requestServiceDetailModel = [{
        id: null,
        title: 'Master sales process',
        serviceType: '',
        reason: '',
        dueDate: null
    },
    {
        id: null,
        title: 'Term & Condition',
        serviceType: '',
        reason: '',
        dueDate: null
    },
    {
        id: null,
        title: 'Product Catalog',
        serviceType: '',
        reason: '',
        dueDate: null
    },
    {
        id: null,
        title: 'Sales Sheet',
        serviceType: '',
        reason: '',
        dueDate: null
    }];

    step_six = [];

    mock_up = [{
        id: null,
        title: 'Master sales process',
        dont_create: false,
        new_doc: false,
        old_doc: false,
        reason: '',
        dueDate: null
    },
    {
        id: null,
        title: 'Term & Condition',
        dont_create: false,
        new_doc: false,
        old_doc: false,
        reason: '',
        dueDate: null
    },
    {
        id: null,
        title: 'Product Catalog',
        dont_create: false,
        new_doc: false,
        old_doc: false,
        reason: '',
        dueDate: null
    },
    {
        id: null,
        title: 'Sales Sheet',
        dont_create: false,
        new_doc: false,
        old_doc: false,
        reason: '',
        dueDate: null
    }
    ];

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    dont_create: boolean;
    new_doc: boolean;
    old_doc: boolean;
    OverSize: any;
    isUpdate: boolean;

    attachments: any = {
        file: [],
        fileDelete: []
    };

    validate = null;
    status = [false, false, false, false];
    deleteDetail: [];
    displayedColumns_file: string[] = ['name', 'size', 'delete'];
    dataSource_file: MatTableDataSource<FileList>;
    serviceType: any = '';


    model_date: IMyDateModel = null;
    start: any;
    end_date: any;
    results: any;
    api_response: any;
    saveDraftstatus = null;
    outOfPageSave = false;
    linkTopage = null;
    pageChange = null;
    openDoCheckc = false;
    status_description = false;
    downloadfile_by = [];

    @ViewChild(DateComponent) Date: DateComponent;

    constructor(private router: Router,
        public stepSixService: StepSixService,
        private sidebarService: RequestService,
        public dialog: MatDialog
    ) {
    }


    next_step() {
        if (this.isUpdate === true) {
            this.router.navigate(['/request/product-detail/step7']);
        } else if (this.isUpdate === false) {
            this.router.navigate(['/request/product-detail/step8']);
        }
    }
    viewNext() {
        if (this.isUpdate === true) {
            this.router.navigate(['/request/product-detail/step7']);
        } else if (this.isUpdate === false) {
            this.router.navigate(['/request/product-detail/step8']);
        }
        // this.router.navigate(['request/product-detail/step7']);
    }
    ngOnInit() {
        window.scrollTo(0, 0);
        this.openDoCheckc = true;
        if (localStorage) {
            if (localStorage.getItem('requestId')) {
                this.sidebarService.sendEvent();
                this.getDetail(localStorage.getItem('requestId'));
            } else {
                this.status_loading = false;
            }
        } else {
            // console.log('Brownser not support');
        }
    }

    changeDate(date: any) {
        const SendI = this.Date.changeDate(date);
        this.changeSaveDraft();
        return SendI;
    }

    sendSaveStatus() {
        if (this.saveDraftstatus === 'success') {
            this.saveStatus.emit(true);
        } else {
            this.saveStatus.emit(false);
        }
    }

    changeSaveDraft() {
        this.saveDraftstatus = null;
        this.sendSaveStatus();
        this.sidebarService.inPageStatus(true);
    }

    getDetail(id) {
        this.stepSixService.getDetail(id, this.template_manage.pageShow).subscribe(res => {
            // console.log('res : ', res);
            this.results = res;
            if (res['status'] === 'success') {
                this.status_loading = false;
                this.isUpdate = res['isUpdate'];
            }
            if (res['data'] !== null) {
                this.validate = this.results.data[0].validate;
                console.log(this.validate);
                if (this.results.data !== null && this.results.data[0].requestServiceDetailModel !== null) {
                    // console.log(res['data'][0]['validate']);
                    this._validatecheck = res['data'][0]['validate'];
                    console.log(this._validatecheck);
                    this.step_six = [];
                    this.requestServiceDetailModel = [];
                    this.description = this.results.data[0].compliance;
                    for (let i = 0; i < this.results.data[0].requestServiceDetailModel.length; i++) {
                        if (this.results.data[0].requestServiceDetailModel[i].serviceType) {
                            if (this.results.data[0].requestServiceDetailModel[i].serviceType === 'ไม่ต้องจัดทำ') {
                                this.dont_create = true;
                                this.new_doc = false;
                                this.old_doc = false;
                            }
                            if (this.results.data[0].requestServiceDetailModel[i].serviceType === 'จัดทำใหม่') {
                                this.dont_create = false;
                                this.new_doc = true;
                                this.old_doc = false;
                            }
                            if (this.results.data[0].requestServiceDetailModel[i].serviceType === 'ใช้เอกสารชุดเดิม') {
                                this.dont_create = false;
                                this.new_doc = false;
                                this.old_doc = true;
                            }
                        } else {
                            if (!this.results.data[0].requestServiceDetailModel[i].serviceType) {
                                this.dont_create = false;
                                this.new_doc = false;
                                this.old_doc = false;
                            }
                        }
                        if (this.results.data[0].requestServiceDetailModel[i].dueDate !== null) {
                            const dateIso = this.results.data[0].requestServiceDetailModel[i].dueDate;
                            // const dateStamp = {isRange: false, singleDate: {jsDate: new Date(dateIso.toString())}};
                            const dateStamp = dateIso;
                            const template = {
                                id: this.results.data[0].requestServiceDetailModel[i].id,
                                title: this.results.data[0].requestServiceDetailModel[i].title,
                                dont_create: this.dont_create,
                                new_doc: this.new_doc,
                                old_doc: this.old_doc,
                                reason: this.results.data[0].requestServiceDetailModel[i].reason,
                                dueDate: dateStamp
                            };
                            this.step_six.push(template);
                        } else {
                            const template = {
                                id: this.results.data[0].requestServiceDetailModel[i].id,
                                title: this.results.data[0].requestServiceDetailModel[i].title,
                                dont_create: this.dont_create,
                                new_doc: this.new_doc,
                                old_doc: this.old_doc,
                                reason: this.results.data[0].requestServiceDetailModel[i].reason,
                                dueDate: null
                            };
                            this.step_six.push(template);
                        }
                        const requestServiceDetail = {
                            id: this.results.data[0].requestServiceDetailModel[i].id,
                            title: this.results.data[0].requestServiceDetailModel[i].title,
                            serviceType: this.results.data[0].requestServiceDetailModel[i].serviceType,
                            reason: this.results.data[0].requestServiceDetailModel[i].reason,
                            dueDate: this.results.data[0].requestServiceDetailModel[i].dueDate,
                        };
                        this.requestServiceDetailModel.push(requestServiceDetail);
                    }
                    for (let i = 0; i < this.results.data[0].fileUplode.file.length; i++) {
                        this.attachments.file.push(this.results.data[0].fileUplode.file[i]);
                    }
                    this.dataSource_file = new MatTableDataSource(this.attachments.file);
                    this.hideDownloadFile();
                } else {
                    // console.log('data null');
                }
                console.log('0000000002', this.template_manage);
            } else {
                this.step_six = this.mock_up;
                this.validate =  res['validate'];
            }
        });
    }

    checkbox(data, i) {
        console.log('six:', this.step_six[i].dueDate);
        console.log('datettt:', this.requestServiceDetailModel[i].dueDate);
        if (data === 'ไม่ต้องจัดทำ') {

            if (this.step_six[i].dont_create === true) {
                this.requestServiceDetailModel[i].serviceType = data;
                this.step_six[i].new_doc = false;
                this.step_six[i].old_doc = false;
                this.requestServiceDetailModel[i].dueDate = this.step_six[i].dueDate;
            } else {
                this.requestServiceDetailModel[i].serviceType = '';
                this.requestServiceDetailModel[i].reason = '';
                this.requestServiceDetailModel[i].dueDate = null;
                this.step_six[i].new_doc = false;
                this.step_six[i].old_doc = false;
                this.step_six[i].reason = '';
                this.step_six[i].dueDate = null;
            }
        }
        if (data === 'จัดทำใหม่') {
            if (this.step_six[i].new_doc === true) {
                this.requestServiceDetailModel[i].serviceType = data;
                this.step_six[i].dont_create = false;
                this.step_six[i].old_doc = false;
                this.requestServiceDetailModel[i].dueDate = this.step_six[i].dueDate;
            } else {
                this.requestServiceDetailModel[i].serviceType = '';
                this.requestServiceDetailModel[i].reason = '';
                this.requestServiceDetailModel[i].dueDate = null;
                this.step_six[i].dont_create = false;
                this.step_six[i].old_doc = false;
                this.step_six[i].reason = '';
                this.step_six[i].dueDate = null;
            }
        }
        if (data === 'ใช้เอกสารชุดเดิม') {
            if (this.step_six[i].old_doc === true) {
                this.requestServiceDetailModel[i].serviceType = data;
                this.step_six[i].dont_create = false;
                this.step_six[i].new_doc = false;
                this.requestServiceDetailModel[i].dueDate = this.step_six[i].dueDate;

            } else {
                this.requestServiceDetailModel[i].serviceType = '';
                this.requestServiceDetailModel[i].reason = '';
                this.requestServiceDetailModel[i].dueDate = null;
                this.step_six[i].dont_create = false;
                this.step_six[i].new_doc = false;
                this.step_six[i].reason = '';
                this.step_six[i].dueDate = null;
            }
        }
        this.changeSaveDraft();
    }

    // checkStatus(i) {
    //     if (this.requestServiceDetailModel[0].serviceType ||
    //         this.requestServiceDetailModel[1].serviceType ||
    //         this.requestServiceDetailModel[2].serviceType ||
    //         this.requestServiceDetailModel[3].serviceType !== '') {
    //         if (this.requestServiceDetailModel[i].serviceType === 'ไม่ต้องจัดทำ') {
    //             if (this.requestServiceDetailModel[i].reason !== '') {
    //                 this.status[i] = true;
    //             } else {
    //                 this.status[i] = false;
    //                 alert('กรุณากรอกข้อมูลให้ถูกต้อง');
    //             }
    //         }
    //         if (this.requestServiceDetailModel[i].serviceType === 'จัดทำใหม่') {
    //             if (this.requestServiceDetailModel[i].reason && this.requestServiceDetailModel[i].reason !== '') {
    //                 this.status[i] = true;
    //             } else {
    //                 this.status[i] = false;
    //             }
    //         }
    //         if (this.requestServiceDetailModel[i].serviceType === 'ใช้เอกสารชุดเดิม') {
    //             this.status[i] = true;
    //         }
    //     } else {
    //         this.status[i] = false;
    //     }
    // }

    connectExplain(i) {
        this.requestServiceDetailModel[i].reason = this.step_six[i].reason;
    }

    onDateChanged(event: IMyDateModel, i): void {
        const start_date = moment(event.singleDate.jsDate);
        this.requestServiceDetailModel[i].dueDate = start_date.format('YYYY-MM-DD');
    }


    onSelectFile(event) {
        this.OverSize = [];
        if (event.target.files && event.target.files[0]) {
            const filesAmount = event.target.files.length;
            const file = event.target.files;
            for (let i = 0; i < filesAmount; i++) {
                if (
                    // file[i].type === 'image/svg+xml' ||
                    file[i].type === 'image/jpeg' ||
                    // file[i].type === 'image/raw' ||
                    // file[i].type === 'image/svg' ||
                    // file[i].type === 'image/tif' ||
                    // file[i].type === 'image/gif' ||
                    file[i].type === 'image/jpg' ||
                    file[i].type === 'image/png' ||
                    file[i].type === 'application/doc' ||
                    file[i].type === 'application/pdf' ||
                    file[i].type ===
                    'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
                    file[i].type ===
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
                    file[i].type ===
                    'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
                    file[i].type === 'application/pptx' ||
                    file[i].type === 'application/docx' ||
                    (file[i].type === 'application/ppt' && file[i].size)
                ) {
                    if (file[i].size <= 5000000) {
                        this.multiple_file(file[i], i);
                    } else {
                        this.OverSize.push(file[i].name);
                    }

                } else {
                    this.status_loading = false;
                    alert('File Not Support');
                }
            }
        }
        if (this.OverSize.length !== 0) {
            console.log('open modal');
            document.getElementById('testttt').click();
        }
    }

    multiple_file(file, index) {
        // console.log('download');
        const reader = new FileReader();
        if (file) {
            reader.readAsDataURL(file);
            reader.onload = (_event) => {
                const rawReadFile = reader.result as string;
                const splitFile = rawReadFile.split(',');
                // this.urls.push({
                const x = {
                    fileName: file.name,
                    fileSize: file.size,
                    base64File: splitFile[1],
                };
                // });
                this.attachments.file.push(x);
                // console.
                // ('this.urls', this.attachments.file);
                this.dataSource_file = new MatTableDataSource(this.attachments.file);
                this.hideDownloadFile();
                // console.log(this.attachments.file);
            };
        }
        this.changeSaveDraft();
    }

    delete_mutifile(data: any, index: any) {
        // console.log('data:', data, 'index::', index);
        if ('id' in this.attachments.file[index]) {
            const id = this.attachments.file[index].id;
            // console.log('found id');
            if ('fileDelete' in this.attachments) {
                this.attachments.fileDelete.push(id);
            }
        }
        // console.log('fileDelete attachments : ', this.attachments.fileDelete);
        this.attachments.file.splice(index, 1);
        this.dataSource_file = new MatTableDataSource(this.attachments.file);
        // console.log(this.attachments);
        this.changeSaveDraft();
    }

    saveDraft(page?, action?) {
        const returnUpdateData = new Subject<any>();
        let alertStatus = false;
        this.status_loading = true;
        let checkedStatus = true;
        console.log('data:', this.step_six);
        console.log('service:', this.requestServiceDetailModel);
        for (let i = 0; i < this.step_six.length; i++) {
            this.requestServiceDetailModel[i].dueDate = this.step_six[i].dueDate;
        }
        for (let i = 0; i < this.requestServiceDetailModel.length; i++) {
            // this.checkStatus(i);
            if (this.requestServiceDetailModel[i].serviceType === '') {
                this.status_description = true;
                checkedStatus = false;
                this._nextstep = false;
                this.status_loading = false;
                // return alert('กรุณากรอกข้อมูลให้ถูกต้อง');
            }
            if (this.requestServiceDetailModel[i].serviceType === 'ไม่ต้องจัดทำ' && this.requestServiceDetailModel[i].reason === '') {
                this.status_description = false;
                this._nextstep = false;
                this.status_loading = false;
                alertStatus = true;
                // alert('กรุณากรอกข้อมูลให้ถูกต้อง');
            }
            if (this.requestServiceDetailModel[i].serviceType === 'จัดทำใหม่') {
                if (this.requestServiceDetailModel[i].reason === '' || this.requestServiceDetailModel[i].dueDate === null) {
                    this.status_description = false;
                    this._nextstep = false;
                    this.status_loading = false;
                    alertStatus = true;
                    // alert('กรุณากรอกข้อมูลให้ถูกต้อง');
                }
            }
        }
        if (this.description === null || this.description === '') {
            // this.status_description = false;
            checkedStatus = false;
            this._nextstep = false;
            this.status_loading = false;
            // alert('กรุณาระบุช่องทางการร้องเรียน');
        }
        // else {
        //     this.status_description = true;
        // }
        // if (this.description !== null && this.description !== '') {
        //     this.status_description = true;
        //     this.checkedStatus = true;
        // } else {
        //     this.status_description = true;
        //     this.checkedStatus = false;
        // }

        console.log('this.status_description', this.status_description)

        // if (this.status_description === true) {
        this._nextstep = true;
        // console.log(this.status);
        const dataSend = {
            requestId: localStorage.getItem('requestId'),
            status: checkedStatus,
            compliance: this.description ? this.description : '',
            deleteDetail: this.deleteDetail ? this.deleteDetail : [],
            requestServiceDetailModel: this.requestServiceDetailModel ? this.requestServiceDetailModel : '',
            fileUplode: this.attachments ? this.attachments : '',
            conclusions: null
        };

        if (this.template_manage.pageShow === 'summary') {
            dataSend.conclusions = true;
        } else {
            dataSend.conclusions = null;
        }
        if (alertStatus === false) {
            console.log('dataSend', dataSend);
            this.stepSixService.updateDetail(dataSend).subscribe(res => {
                this.api_response = res;
                // console.log(this.api_response);
                if (this.api_response.status === 'success') {
                    this.status_loading = false;
                    this.attachments.file = [];
                    this.downloadfile_by = [];
                    returnUpdateData.next(true);
                    this.sidebarService.inPageStatus(false);
                    this.sidebarService.sendEvent();
                    this.saveDraftstatus = this.api_response.status;
                    setTimeout(() => {
                        this.saveDraftstatus = 'fail';
                    }, 3000);
                    console.log('page_page', page);
                    console.log('action_action', action);
                    this.sendSaveStatus();
                } else {
                    this.sendSaveStatus();
                    this.status_loading = false;
                    alert(res['message']);
                    returnUpdateData.next(false);
                }
                this.status_loading = true;
            },
                (error) => {
                    this.status_loading = false;
                    console.log(error);
                    returnUpdateData.next(false);
                }, () => {this.getDetail(localStorage.getItem('requestId'));
                    this.status_loading = false;
                    returnUpdateData.next(false);
                });
               return returnUpdateData;
        } else {
            alert('กรุณากรอกข้อมูลให้ถูกต้อง');
            return false;
        }
    }

    dateFormat(date: any) {
        let dateForm = null;
        const year = Number(moment(date).format('YYYY')) + Number(543);
        dateForm = moment(date).format('DD/MM') + '/' + year;
        return dateForm;
    }
    downloadFile(pathdata: any) {
        this.status_loading = true;
        const contentType = '';
        const sendpath = pathdata;
        console.log('path', sendpath);
        this.sidebarService.getfile(sendpath).subscribe(res => {
            if (res['status'] === 'success' && res['data']) {
                this.status_loading = false;
                const datagetfile = res['data'];
                const b64Data = datagetfile['data'];
                const filename = datagetfile['fileName'];
                console.log('base64', b64Data);
                const config = this.b64toBlob(b64Data, contentType);
                saveAs(config, filename);
            }

        });
    }

    b64toBlob(b64Data, contentType = '', sliceSize = 512) {
        const convertbyte = atob(b64Data);
        const byteCharacters = atob(convertbyte);
        const byteArrays = [];
        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            const slice = byteCharacters.slice(offset, offset + sliceSize);

            const byteNumbers = new Array(slice.length);
            for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            const byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }

        const blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }

    downloadFile64(data: any) {
        this.status_loading = true;
        const contentType = '';
        this.status_loading = false;
        const b64Data = data.base64File;
        const filename = data.fileName;
        console.log('base64', b64Data);

        const config = this.base64(b64Data, contentType);
        saveAs(config, filename);
    }

    base64(b64Data, contentType = '', sliceSize = 512) {
        const byteCharacters = atob(b64Data);
        const byteArrays = [];
        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            const slice = byteCharacters.slice(offset, offset + sliceSize);

            const byteNumbers = new Array(slice.length);
            for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            const byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }

        const blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }

    autoprocess(index: any, test: any) {
        if (test === 1) {
            const textprocess1 = document.getElementById('textprocess1' + String(index));
            textprocess1.style.overflow = 'hidden';
            textprocess1.style.height = 'auto';
            textprocess1.style.height = textprocess1.scrollHeight + 'px';
        }
    }

    // ngOnDestroy(): void {
    //     const data = this.sidebarService.getsavePage();
    //     let saveInFoStatus = false;
    //     saveInFoStatus = data.saveStatusInfo;
    //     if (saveInFoStatus === true) {
    //       this.outOfPageSave = true;
    //       this.linkTopage = data.goToLink;
    //       console.log('ngOnDestroy', this.linkTopage);
    //       this.saveDraft();
    //       this.sidebarService.resetSaveStatusInfo();
    //     }
    //   }

    // ngDoCheck() {
    //     if (this.openDoCheckc === true) {
    //         if (this.template_manage.caaStatus !== 'show') {
    //             this.pageChange = this.sidebarService.changePageGoToLink();
    //             if (this.pageChange) {
    //                 // this.getNavbar();
    //                 const data = this.sidebarService.getsavePage();
    //                 let saveInFoStatus = false;
    //                 saveInFoStatus = data.saveStatusInfo;
    //                 if (saveInFoStatus === true) {
    //                     this.outOfPageSave = true;
    //                     this.linkTopage = data.goToLink;
    //                     this.saveDraft('navbar', null);
    //                     this.sidebarService.resetSaveStatu();
    //                 }
    //             }
    //         }
    //     }
    // }

    hideDownloadFile() {
        if (this.dataSource_file.data.length > 0) {
            this.downloadfile_by = [];

            for (let index = 0; index < this.dataSource_file.data.length; index++) {
                const element = this.dataSource_file.data[index];
                console.log('ele', element);

                if ('id' in element) {
                    this.downloadfile_by.push('path');
                } else {
                    this.downloadfile_by.push('base64');
                }
            }
        }
        console.log('by:', this.downloadfile_by);
    }


    canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
        console.log('canDeactivate request');
        const subject = new Subject<boolean>();
        const status = this.sidebarService.outPageStatus();
        if (status === true) {
          // -----
          const dialogRef =  this.dialog.open(DialogSaveStatusComponent, {
            disableClose: true,
            data: {
              headerDetail: 'ยืนยันการดำเนินการ',
              bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
              bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
              returnDetail: 'null',
              returnStatus: false,
              icon: 'assets/img/Alarm-blue.svg',
            }
          });
          dialogRef.afterClosed().subscribe((data) => {
            if (data.returnStatus === true) {
              const raturnStatus = this.saveDraft();
              console.log('request raturnStatus', raturnStatus);
                if (raturnStatus) {
                  console.log('raturnStatus T', this.saveDraftstatus);
                  subject.next(true);
                } else {
                  console.log('raturnStatus F', this.saveDraftstatus);
                  subject.next(false);
                }
            } else if (data.returnStatus === false) {
              this.sidebarService.inPageStatus(false);
              subject.next(true);
              return true;
            } else {
              subject.next(false);
            }
          });
          return subject;
        } else {
          return true;
        }
      }

}
