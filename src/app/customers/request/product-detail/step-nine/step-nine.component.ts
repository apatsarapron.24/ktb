import { RequestService } from './../../../../services/request/request.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validator } from '@angular/forms';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
  CdkDrag,
  CdkDragExit,
  CdkDragEnter,
} from '@angular/cdk/drag-drop';
import { element } from 'protractor';
import { Router } from '@angular/router';
import { StepNineService } from '../../../../services/request/product-detail/step-nine/step-nine.service';
import { ThrowStmt } from '@angular/compiler';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogSaveStatusComponent } from '../../dialog/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from '../../../../services/configGuard/config-guard.service';


@Component({
  selector: 'app-step-nine',
  templateUrl: './step-nine.component.html',
  styleUrls: ['./step-nine.component.scss'],
})
export class StepNineComponent implements OnInit, CanComponentDeactivate {
  @Input() template_manage = { role: null, validate: null, caaStatus: null, pageShow: null };
  @Output() saveStatus = new EventEmitter<boolean>();

  staticTitleIndex1 = []; // !important ex [0,1,2] mean set fix row index 0, 1, 2, 3, 4, 5, 6
  staticTitleIndex2 = []; // !important ex [0,1,2] mean set fix row index 0, 1, 2, 3, 4
  staticTitleIndex3 = []; // !important ex [0,1,2] mean set fix row index 0, 1, 2, 3

  status_loading = true;

  saveDraftstatus = null;
  outOfPageSave = false;
  linkTopage = null;
  pageChange = null;
  openDoCheckc = false;
  step9_DATA = [
    {
      tableName: 'ปริมาณธุรกรรม',
      data: [
        {
          KPI_id: null,
          KPI_dataDraft: true,
          KPI_disabledCol: true,
          KPI_name: 'วงเงินโครงการ',
          KPI_year: [{
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          }],
        },
        {
          KPI_id: null,
          KPI_dataDraft: true,
          KPI_disabledCol: true,
          KPI_name: 'Limit',
          KPI_year: [{
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          }],
        },
        {
          KPI_id: null,
          KPI_dataDraft: true,
          KPI_disabledCol: true,
          KPI_name: 'Outstanding',
          KPI_year: [{
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          }],
        },
        {
          KPI_id: null,
          KPI_dataDraft: true,
          KPI_disabledCol: true,
          KPI_name: 'A/R turnover',
          KPI_year: [{
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          }],
        },
        {
          KPI_id: null,
          KPI_dataDraft: true,
          KPI_disabledCol: true,
          KPI_name: 'Advance Payment',
          KPI_year: [{
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          }],
        },
        {
          KPI_id: null,
          KPI_dataDraft: true,
          KPI_disabledCol: true,
          KPI_name: 'Utilization/Turnover',
          KPI_year: [{
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          }],
        },
        {
          KPI_id: null,
          KPI_dataDraft: true,
          KPI_disabledCol: true,
          KPI_name: 'จำนวนลูกค้า',
          KPI_year: [{
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          }],
        },
      ],
    },
    {
      tableName: 'ผลตอบแทน',
      data: [
        {
          KPI_id: null,
          KPI_dataDraft: true,
          KPI_disabledCol: true,
          KPI_name: 'NII',
          KPI_year: [{
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          }],
        },
        {
          KPI_id: null,
          KPI_dataDraft: true,
          KPI_disabledCol: true,
          KPI_name: 'Non-NII',
          KPI_year: [{
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          }],
        },
        {
          KPI_id: null,
          KPI_dataDraft: true,
          KPI_disabledCol: true,
          KPI_name: 'RAROC',
          KPI_year: [{
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          }],
        },
        {
          KPI_id: null,
          KPI_dataDraft: true,
          KPI_disabledCol: true,
          KPI_name: 'ROI',
          KPI_year: [{
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          }],
        },
        {
          KPI_id: null,
          KPI_dataDraft: true,
          KPI_disabledCol: true,
          KPI_name: 'EP',
          KPI_year: [{
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          }],
        },
      ],
    },
    {
      tableName: 'การควบคุม',
      data: [
        {
          KPI_id: null,
          KPI_dataDraft: true,
          KPI_disabledCol: true,
          KPI_name: 'NPL',
          KPI_year: [{
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          }],
        },
        {
          KPI_id: null,
          KPI_dataDraft: true,
          KPI_disabledCol: true,
          KPI_name: 'Loss(Writeoff)',
          KPI_year: [{
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          }],
        },
        {
          KPI_id: null,
          KPI_dataDraft: true,
          KPI_disabledCol: true,
          KPI_name: '1st Year Default rate',
          KPI_year: [{
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          }],
        },
        {
          KPI_id: null,
          KPI_dataDraft: true,
          KPI_disabledCol: true,
          KPI_name: 'Override of Total port',
          KPI_year: [{
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          },
          {
            year: '',
            detail: '',
          }],
        },
      ],
    },
  ];
  stepNine = null;
  colZise = 3;
  rowYear = [];
  popUpShowRow = null;
  popUpShowCol = [{ indextable: null, indexCol: null }];
  attachOutsideOnClick = false;
  enabled = false;
  enabledCount = false;
  clickedOutsideCount: number;
  clickedOutsideDetalCount: number;
  disabledRowAll = false;
  disabledRow = true;
  disabledCol = [];
  // dataDraft = [{
  //   tableindex: null,
  //   dataDteil: null,
  // }];
  requestId: any;
  res: any = {
    data: {
      goal: {
        year: []
      }
    }
  };
  get_data: any;
  sendData: any = {
    requestId: '',
    status: false,
    goal: {
      year: [],
      transaction: [],
      compensation: [],
      control: [],
    },
    goalDelete: [],
    conclusions: null
  };
  check_year = true;
  check_transaction = true;
  check_compensation = true;
  check_control = true;
  validate = null;
  sendSaveStatus() {
    if (this.saveDraftstatus === 'success') {
      this.saveStatus.emit(true);
    } else {
      this.saveStatus.emit(false);
    }
  }

  // ============== next page ==========================
  saveDraft(page?, action?) {
    this.status_loading = true;
    this.yearInput();
    // console.log('stepNine', this.stepNine);
    this.sendData.goal.transaction = [];
    this.sendData.goal.compensation = [];
    this.sendData.goal.control = [];
    this.sendData.goal.year = [];

    console.log('year aaa:', this.rowYear);
    console.log('year aaa[0]:', this.res['data'].goal.year[0]);
    for (
      let index = 0;
      index < this.res['data'].goal.year.length;
      index++
    ) {
      this.sendData.goal.year.push(
        Number(this.rowYear[0]) + index
      );
    }
    console.log('year:', this.sendData.goal.year);
    // **********************************************transaction*************************************************
    for (let index = 0; index < this.stepNine[0].data.length; index++) {
      this.sendData.goal.transaction.push({
        id: this.stepNine[0].data[index].KPI_id,
        title: this.stepNine[0].data[index].KPI_name,
        tranx: [],
      });
    }

    for (let index = 0; index < this.stepNine[0].data.length; index++) {
      for (let i = 0; i < this.stepNine[0].data[index].KPI_year.length; i++) {
        this.sendData.goal.transaction[index].tranx.push(
          this.stepNine[0].data[index].KPI_year[i].detail
        );
      }
    }

    // **********************************************compensation*************************************************
    for (let index = 0; index < this.stepNine[1].data.length; index++) {
      this.sendData.goal.compensation.push({
        id: this.stepNine[1].data[index].KPI_id,
        title: this.stepNine[1].data[index].KPI_name,
        tranx: [],
      });
    }

    for (let index = 0; index < this.stepNine[1].data.length; index++) {
      for (let i = 0; i < this.stepNine[1].data[index].KPI_year.length; i++) {
        this.sendData.goal.compensation[index].tranx.push(
          this.stepNine[1].data[index].KPI_year[i].detail
        );
      }
    }

    // **********************************************control*************************************************
    for (let index = 0; index < this.stepNine[2].data.length; index++) {
      this.sendData.goal.control.push({
        id: this.stepNine[2].data[index].KPI_id,
        title: this.stepNine[2].data[index].KPI_name,
        tranx: [],
      });
    }

    for (let index = 0; index < this.stepNine[2].data.length; index++) {
      for (let i = 0; i < this.stepNine[2].data[index].KPI_year.length; i++) {
        this.sendData.goal.control[index].tranx.push(
          this.stepNine[2].data[index].KPI_year[i].detail
        );
      }
    }
    console.log('send data step9', this.sendData);

    const result = this.check_status();
    console.log('result', result);
    if (result === true) {
      if (
        this.sendData.goal.transaction.length != 0 &&
        this.sendData.goal.compensation.length != 0 &&
        this.sendData.goal.control.length != 0 &&
        this.rowYear.length == 0) {
        this.sendData.status = false;
      }
      // console.log('year', this.check_year);
      // console.log('transaction', this.check_transaction);
      // console.log('compensation', this.check_compensation);
      // console.log('control', this.check_control);
      // console.log('send:', this.sendData);
      this.sendData.requestId = localStorage.getItem('requestId');

      if (this.template_manage.pageShow === 'summary') {
        this.sendData.conclusions = true;
      } else {
        this.sendData.conclusions = null;
      }
      const returnUpdateData = new Subject<any>();
      this.Step_nine.Update_step_nine(this.sendData).subscribe(res => {
        console.log(res);
        if (res['status'] === 'success') {
          this.status_loading = false;
          this.sidebarService.sendEvent();
          this.saveDraftstatus = 'success';
          returnUpdateData.next(true);
          this.sidebarService.inPageStatus(false);
          setTimeout(() => {
            this.saveDraftstatus = 'fail';
          }, 3000);
          this.sendSaveStatus();
          this.ngOnInit();
        } else {
          this.status_loading = false;
          alert(res['message']);
          returnUpdateData.next(false);
          // alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
        }
      }, err => {
        this.status_loading = false;
        console.log(err);
        alert(err['message']);
        returnUpdateData.next(false);
        // alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
      });
      return returnUpdateData;
    } else {
      // this.saveDraftstatus = null;
      this.sendData.requestId = localStorage.getItem('requestId');

      if (this.template_manage.pageShow === 'summary') {
        this.sendData.conclusions = true;
      } else {
        this.sendData.conclusions = null;
      }
      const returnUpdateData = new Subject<any>();
      this.Step_nine.Update_step_nine(this.sendData).subscribe(res => {
        console.log(res);
        if (res['status'] === 'success') {
          this.status_loading = false;
          this.sidebarService.sendEvent();
          this.saveDraftstatus = 'success';
          returnUpdateData.next(true);
          this.sidebarService.inPageStatus(false);
          setTimeout(() => {
            this.saveDraftstatus = 'fail';
          }, 3000);
          if (page === 'navbar') {
            this.router.navigate([this.linkTopage]);
          }
          this.sidebarService.resetSaveStatusInfo();

          this.sendSaveStatus();
          this.ngOnInit();
        } else {
          this.status_loading = false;
          alert(res['message']);
          returnUpdateData.next(false);
          // alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
        }
      }, err => {
        this.status_loading = false;
        console.log(err);
        alert(err['message']);
        returnUpdateData.next(false);
        // alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
      });
      return returnUpdateData;
    }
  }
  next_step() {
    this.router.navigate(['/request/product-detail/step10']);
  }

  check_status() {
    console.log('data:', this.sendData);

    this.check_year = true;
    this.check_transaction = true;
    this.check_compensation = true;
    this.check_control = true;

    for (let index = 0; index < this.sendData.goal.year.length; index++) {
      if (
        this.sendData.goal.year[index] === null ||
        this.sendData.goal.year[index] === ''
      ) {
        this.check_year = false;
        break;
      }
    }

    for (
      let index = 0;
      index < this.sendData.goal.transaction.length;
      index++
    ) {
      for (let p = 0; p < this.sendData.goal.transaction[index].tranx.length; p++) {
        if (
          this.sendData.goal.transaction[index].tranx[p] === null ||
          this.sendData.goal.transaction[index].tranx[p] === ''
        ) {
          this.check_transaction = false;
          break;
        }
      }

    }

    for (
      let index = 0;
      index < this.sendData.goal.compensation.length;
      index++
    ) {
      for (let p = 0; p < this.sendData.goal.compensation[index].tranx.length; p++) {
        if (

          this.sendData.goal.compensation[index].tranx[p] === null ||
          this.sendData.goal.compensation[index].tranx[p] === ''
        ) {
          this.check_compensation = false;
          break;
        }
      }

    }

    for (let index = 0; index < this.sendData.goal.control.length; index++) {
      for (let p = 0; p < this.sendData.goal.control[index].tranx.length; p++) {
        if (
          this.sendData.goal.control[index].tranx[p] === null ||
          this.sendData.goal.control[index].tranx[p] === ''
        ) {
          this.check_control = false;
          break;
        }
      }

    }
    for (let index = 0; index < this.sendData.goal.transaction.length; index++) {
      if (this.sendData.goal.transaction[index].title === null ||
        this.sendData.goal.transaction[index].title === '') {
        this.check_transaction = false;
        break;
      }
    }
    for (let index = 0; index < this.sendData.goal.compensation.length; index++) {
      if (this.sendData.goal.compensation[index].title === null ||
        this.sendData.goal.compensation[index].title === '') {
        this.check_compensation = false;
        break;
      }
    }

    for (let index = 0; index < this.sendData.goal.control.length; index++) {
      if (this.sendData.goal.control[index].title === null ||
        this.sendData.goal.control[index].title === '') {
        this.check_control = false;
        break;
      }
    }

    if (
      this.check_year === true &&
      this.check_transaction === true &&
      this.check_compensation === true &&
      this.check_control === true
    ) {
      this.sendData.status = true;
      return true;
    } else {
      this.sendData.status = false;
      return false;
    }
  }
  // ================= disabled ==========================
  checkDisabledYear() {
    // console.log('checkDisabledYear');
    let checkValue = 0;
    this.rowYear.forEach((elements) => {
      console.log('elementYear00000=>', elements);
      if (elements) {
        checkValue = checkValue + 1;
      }
    });
    this.stepNine.forEach((elements) => {
      elements.data.forEach((sudElements) => {
        sudElements.KPI_year.forEach((elementYear, index) => {
          elementYear.year = this.rowYear[0] + index;
          // console.log('rowYear', this.rowYear[0]);
          // console.log('elementYear', elementYear);
          // console.log('index', index);
          if (elementYear.detail) {
            checkValue = checkValue + 1;
          }
        });
      });
    });

    console.log('00007=>', this.stepNine);
    // console.log('checkValue', checkValue);
    // if (checkValue === 0) {
    //   this.disabledRowAll = true;
    // } else {
    //   this.disabledRowAll = false;
    // }
    // console.log('dddddddd', this.disabledRowAll);
  }
  checkDisabledRow(indextable, indexCol) {
    let checkValue = 0;
    this.stepNine[indextable].data[indexCol].KPI_year.forEach((elements) => {
      // console.log('elements', elements);
      if (elements.detail) {
        checkValue = checkValue + 1;
      }
    });
    if (this.stepNine[indextable].data[indexCol].KPI_name !== null) {
      checkValue = checkValue + 1;
    }
    if (checkValue === 0) {
      this.stepNine[indextable].data[indexCol].KPI_disabledCol = true;
    } else {
      this.stepNine[indextable].data[indexCol].KPI_disabledCol = false;
    }
    // console.log('checkValue', checkValue);
    // console.log('dddddddd', this.disabledRowAll);
    this.changeSaveDraft();
  }

  // ============== row function ==========================
  addYear() {
    this.changeSaveDraft();
    this.stepNine.forEach((elements) => {
      elements.data.forEach((sudElements) => {
        sudElements.KPI_year.push({
          year: null,
          detail: null,
        });
        // console.log('addYear', sudElements.KPI_year);
      });
    });
    this.res.data.goal.year.push(null)
    // console.log(this.res['data'].goal.year)
    this.rowYear.push(null);
    this.colZise = this.colZise + 1;
  }

  deleteRow(index) {
    this.stepNine.forEach((elements) => {
      elements.data.forEach((sudElements) => {
        sudElements.KPI_year.splice(index, 1);
        // console.log('deletRow', sudElements.KPI_year);
      });
    });
    this.res.data.goal.year.splice(index, 1);
    this.rowYear.splice(index, 1);
    this.colZise = this.colZise - 1;
    // this.stepFour.workFlow.uploadFile.splice(index, 1);
    // console.log('deletRow', index);
    this.popUpShowRow = null;
    this.checkDisabledYear();
    this.changeSaveDraft();
  }
  deleteRowDetail(index) {
    this.stepNine.forEach((elements) => {
      elements.data.forEach((sudElements) => {
        sudElements.KPI_year[index].year = null;
        sudElements.KPI_year[index].detail = null;
        // console.log('deletRow', sudElements.KPI_year);
      });
    });
    // this.stepFour.workFlow.uploadFile.splice(index, 1);
    // console.log('deletRow', index);
    this.rowYear[index] = null;
    this.popUpShowRow = null;
    this.checkDisabledYear();
    this.changeSaveDraft();
  }

  yearInput() {
    // console.log('aaaaaaa', this.colZise - 1);
    // console.log('aaaaaaa this.rowYear', this.rowYear);
    this.stepNine.forEach((elements) => {
      elements.data.forEach((sudElements) => {
        // console.log('aaaaaaa sudElements', sudElements);
        this.rowYear.forEach((year, index) => {
          // console.log('aaaaaaa', year);
          sudElements.KPI_year[index].year = year;
          // sudElements.KPI_year[index].detail = null;
        });
        // console.log('deletRow', sudElements.KPI_year);
      });
    });
  }

  // ============== col function ==========================
  addTable(tableIndx) {
    this.changeSaveDraft();
    //   console.log('tableIndx', tableIndx);
    const KPI_year_col = [];
    for (let index = 0; index < this.colZise - 1; index++) {
      KPI_year_col.push({
        year: null,
        detail: null,
      });
    }
    this.stepNine[tableIndx].data.push({
      KPI_id: null,
      KPI_dataDraft: true,
      KPI_disabledCol: true,
      KPI_name: null,
      KPI_year: KPI_year_col,
    });
    // console.log('addTable', this.stepNine[tableIndx]);
  }
  deleteCol(indextable, indexCol) {
    console.log(this.stepNine[indextable].data[indexCol]);
    if (this.stepNine[indextable].data[indexCol].KPI_id !== null) {
      this.sendData.goalDelete.push(
        this.stepNine[indextable].data[indexCol].KPI_id
      );
    }
    this.stepNine[indextable].data.splice(indexCol, 1);
    this.popUpShowCol[0].indexCol = null;
    this.popUpShowCol[0].indextable = null;
    this.checkDisabledYear();
    this.changeSaveDraft();
  }

  deleteColDetail(indextable, indexCol) {
    for (
      let index = 0;
      index < this.stepNine[indextable].data[indexCol].KPI_year.length;
      index++
    ) {
      this.stepNine[indextable].data[indexCol].KPI_year[index].detail = null;
    }
    if (this.stepNine[indextable].data[indexCol].KPI_dataDraft) {
      this.stepNine[indextable].data[indexCol].KPI_name = null;
      this.stepNine[indextable].data[indexCol].KPI_disabledCol = true;
    }
    this.popUpShowCol[0].indexCol = null;
    this.popUpShowCol[0].indextable = null;
    this.checkDisabledYear();
    this.changeSaveDraft();
  }

  placeholder(text: string) {
    if (text[0].search(/[^a-zA-Z]+/) === -1) {
      return ' ' + text;
    } else {
      return text;
    }
  }
  // validationNumber (text: any, i: number) {
  //   if (text.search(/[^0-9]+/g, '') === -1 ) {

  //   } else {
  //     alert('กรุณากรอกเป็นตัวเลข');
  //   }

  //   this.changeSaveDraft();
  // }

  // =========================== Modal popup ==========================
  popUpRow(index) {
    //   console.log('popUpRow', index);
    if (this.popUpShowRow === index) {
      this.popUpShowRow = null;
    } else {
      this.popUpShowRow = index;
    }
    this.enabled = true;
    this.enabledCount = true;
    this.clickedOutsideCount = this.colZise - 2;
  }

  popUpCol(indextable, indexCol) {
    //   console.log('popUpCol');
    if (
      this.popUpShowCol[0].indextable === indextable &&
      this.popUpShowCol[0].indexCol === indexCol
    ) {
      this.popUpShowCol[0].indextable = null;
      this.popUpShowCol[0].indexCol = null;
    } else {
      this.popUpShowCol[0].indextable = indextable;
      this.popUpShowCol[0].indexCol = indexCol;
    }
    //   console.log('popUpShowCol', this.popUpShowCol);
    this.enabled = true;
    this.clickedOutsideDetalCount = 0;
    this.stepNine.forEach((elements) => {
      elements.data.forEach((sudElements) => {
        this.clickedOutsideDetalCount = this.clickedOutsideDetalCount + 1;
      });
    });
  }

  onClickedOutsideYear(indextable) {
    // console.log('indextableYear', indextable);
    // console.log('indextableYear - enabled', this.enabled);
    // console.log('indextableYear - clickedOutsideCount - 2', this.clickedOutsideCount);
    if (this.clickedOutsideCount > 0) {
      this.clickedOutsideCount = this.clickedOutsideCount - 1;
    }
    else if (this.clickedOutsideCount === 0) {
      this.popUpShowRow = null;
    }
    // console.log(' this.clickedOutsideCount',  this.clickedOutsideCount);
  }
  onClickedOutsideDetal(indextable, indexCol) {
    // console.log('Detalindextable', indextable);
    // console.log('DetalindexCol', indexCol);
    if (this.clickedOutsideDetalCount > 0) {
      this.clickedOutsideDetalCount = this.clickedOutsideDetalCount - 1;
    } else if (this.clickedOutsideDetalCount === 0) {
      this.popUpShowCol[0].indexCol = null;
      this.popUpShowCol[0].indextable = null;
    }
  }

  // ============================ drop table ==========================
  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
    this.changeSaveDraft();
  }

  constructor(private router: Router,
    private Step_nine: StepNineService,
    private sidebarService: RequestService,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.stepNine = this.step9_DATA;
    this.openDoCheckc = true;
    if (localStorage.getItem('requestId')) {
      this.sidebarService.sendEvent();
    } else {
      this.status_loading = false;
    }
    this.requestId = localStorage.getItem('requestId');
    this.Step_nine.getData_step_nine(this.requestId, this.template_manage.pageShow).subscribe((res) => {
      console.log('res step 9', res);
      if (res['data'] !== null) {
        this.res.data = res['data'];

        this.GetData();
        if (res['status'] === 'success') {
          this.status_loading = false;
        }
        if (this.stepNine) {
          this.rowYear = [];
          this.res.data.goal.year.forEach((year) => {
            this.rowYear.push(Number(year));
          });
        }
        this.colZise = this.res['data'].goal.year.length + 1;
      } else {
        console.log(this.res)
        this.colZise = this.colZise + 1;
        this.status_loading = false;
        this.res.data.goal.year = [null, null, null]
        this.stepNine = this.step9_DATA

      }
      if (this.template_manage.pageShow === 'executive') {
        this.validate = false;
      }
    });
    if (this.template_manage.pageShow === 'executive') {
      this.validate = false;
    }
  }
  GetData() {
    this.get_data = this.res.data;
    this.validate = this.get_data.validate;
    console.log(this.validate);
    this.sendData.requestId = Number(this.get_data.requestId);
    // this.sendData.goal.transaction = this.get_data.goal.transaction;
    // this.sendData.goal.compensation = this.get_data.goal.compensation;
    // this.sendData.goal.control = this.get_data.goal.control;
    // ***************************************transaction******************************************
    // console.log('get_data', this.get_data.goal.transaction);
    // console.log('stepNine:', this.stepNine);
    this.stepNine = [
      {
        tableName: 'ปริมาณธุรกรรม',
        data: [],
      },
      {
        tableName: 'ผลตอบแทน',
        data: [],
      },
      {
        tableName: 'การควบคุม',
        data: [],
      }
    ];
    for (
      let index = 0;
      index < this.get_data.goal.transaction.length;
      index++
    ) {
      this.stepNine[0].data.push({
        KPI_id: this.get_data.goal.transaction[index].id,
        KPI_dataDraft: false,
        KPI_disabledCol: false,
        KPI_name: this.get_data.goal.transaction[index].title,
        KPI_year: [],
      });
    }
    for (let i = 0; i < this.stepNine[0].data.length; i++) {
      for (let index = 0; index < this.get_data.goal.year.length; index++) {
        this.stepNine[0].data[i].KPI_year.push({
          year: this.get_data.goal.year[index],
          detail: this.get_data.goal.transaction[i].tranx[index],
        });
      }
    }

    // ***************************************compensation******************************************
    for (
      let index = 0;
      index < this.get_data.goal.compensation.length;
      index++
    ) {
      this.stepNine[1].data.push({
        KPI_id: this.get_data.goal.compensation[index].id,
        KPI_dataDraft: false,
        KPI_disabledCol: false,
        KPI_name: this.get_data.goal.compensation[index].title,
        KPI_year: [],
      });
    }
    for (let i = 0; i < this.stepNine[1].data.length; i++) {
      for (let index = 0; index < this.get_data.goal.year.length; index++) {
        this.stepNine[1].data[i].KPI_year.push({
          year: this.get_data.goal.year[index],
          detail: this.get_data.goal.compensation[i].tranx[index],
        });
      }
    }

    // ****************************************control******************************************
    for (let index = 0; index < this.get_data.goal.control.length; index++) {
      this.stepNine[2].data.push({
        KPI_id: this.get_data.goal.control[index].id,
        KPI_dataDraft: false,
        KPI_disabledCol: false,
        KPI_name: this.get_data.goal.control[index].title,
        KPI_year: [],
      });
    }
    for (let i = 0; i < this.stepNine[2].data.length; i++) {
      for (let index = 0; index < this.get_data.goal.year.length; index++) {
        this.stepNine[2].data[i].KPI_year.push({
          year: this.get_data.goal.year[index],
          detail: this.get_data.goal.control[i].tranx[index],
        });
      }
    }
  }
  viewNext() {
    this.router.navigate(['request/product-detail/step10']);
  }
  changeSaveDraft() {
    this.saveDraftstatus = null;
    this.sendSaveStatus();
    this.sidebarService.inPageStatus(true);
  }

  checkStaticTitle(iTabld, index) {
    if (iTabld === 0) {
      return this.staticTitleIndex1.includes(index);
    } else if (iTabld === 1) {
      return this.staticTitleIndex2.includes(index);
    } else if (iTabld === 2) {
      return this.staticTitleIndex3.includes(index);
    }
  }
  // autoprocess(index: any, test: any) {
  //   if (test === 1) {
  //     const textvolume = document.getElementById('textvolume' + String(index));
  //     textvolume.style.overflow = 'hidden';
  //     textvolume.style.height = 'auto';
  //     textvolume.style.height = textvolume.scrollHeight + 'px';
  //   }

  //   if (test === 2) {
  //     const textprocess2 = document.getElementById('textprocess2' + String(index));
  //     textprocess2.style.overflow = 'hidden';
  //     textprocess2.style.height = 'auto';
  //     textprocess2.style.height = textprocess2.scrollHeight + 'px';
  //   }
  // }

  autoGrowTextZone(e) {
    e.target.style.height = '0px';
    e.target.style.height = (e.target.scrollHeight + 0) + 'px';
  }

  // ngOnDestroy(): void {
  //   const data = this.sidebarService.getsavePage();
  //   let saveInFoStatus = false;
  //   saveInFoStatus = data.saveStatusInfo;
  //   if (saveInFoStatus === true) {
  //     this.outOfPageSave = true;
  //     this.linkTopage = data.goToLink;
  //     console.log('ngOnDestroy', this.linkTopage);
  //     this.saveDraft();
  //     this.sidebarService.resetSaveStatusInfo();
  //   }
  // }

  // ngDoCheck() {
  //   if (this.openDoCheckc === true) {
  //     if  (this.template_manage.caaStatus !== 'show') {
  //     this.pageChange = this.sidebarService.changePageGoToLink();
  //     if (this.pageChange) {
  //       // this.getNavbar();
  //       const data = this.sidebarService.getsavePage();
  //       let saveInFoStatus = false;
  //       saveInFoStatus = data.saveStatusInfo;
  //       if (saveInFoStatus === true) {
  //         this.outOfPageSave = true;
  //         this.linkTopage = data.goToLink;
  //           this.saveDraft('navbar', null);
  //           this.sidebarService.resetSaveStatu();
  //         }
  //       }
  //     }
  //   }
  // }


  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate request');
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef = this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        if (data.returnStatus === true) {
          const raturnStatus = this.saveDraft();
          console.log('request raturnStatus', raturnStatus);
          if (raturnStatus) {
            console.log('raturnStatus T', this.saveDraftstatus);
            subject.next(true);
          } else {
            console.log('raturnStatus F', this.saveDraftstatus);
            subject.next(false);
          }
        } else if (data.returnStatus === false) {
          this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
        }
      });
      return subject;
    } else {
      return true;
    }
  }
}
