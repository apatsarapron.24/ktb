import { Key } from 'protractor';
import { ReplaySubject } from 'rxjs';
import { StepOneService } from './../../../../services/request/product-detail/step-one/step-one.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { RequestService } from './../../../../services/request/request.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import { saveAs } from 'file-saver';
import { DialogSaveStatusComponent } from '../../dialog/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from './../../../../services/configGuard/config-guard.service';
import { fstat } from 'fs';

@Component({
    selector: 'app-step-one',
    templateUrl: './step-one.component.html',
    styleUrls: ['./step-one.component.scss']
})
export class StepOneComponent implements OnInit, CanComponentDeactivate {
    @Input() template_manage = { role: null, validate: null, caaStatus: null, pageShow: null };
    @Output() saveStatus = new EventEmitter<boolean>();
    displayedColumns_file: string[] = ['name', 'size', 'delete'];
    dataSource_file: MatTableDataSource<FileList>;
    saveDraftstatus = null;
    outOfPageSave = false;
    linkTopage = null;
    pageChange = null;
    openDoCheckc = false;

    showCircle_colId = null;
    showCircle_id = null;
    status_loading = true;
    enter = 3;
    stepOne: any = {
        objective: {
            file: [],
            fileDelete: []
        },
        objectiveText: null,
        businessModel: {
            file: [],
            fileDelete: []
        },
        businessModelText: null,
        detail: {
            file: [],
            fileDelete: []
        },
        detailText: null,
        externalCommit: {
            no: false,
            yesDetail: {
                KTB: '',
                external: ''
            },
            yes: false
        },
        attachments: {
            file: [],
            fileDelete: []
        },
        validate: null,
        conclusions: null
    };

    agreement: any;
    file1_1: any;
    file1_2: any;
    file2_1: any;
    file2_2: any;
    file3_1: any;
    file3_2: any;
    document: any;
    Agreement = null;
    file_name: any;
    file_size: number;

    imgURL: any;
    image1 = [];
    image2 = [];
    image3 = [];
    showImage1 = false;
    message = null;

    urls: any = [];
    // sendbase64 : string;
    // Model
    showModel = false;
    modalmainImageIndex: number;
    modalmainImagedetailIndex: number;
    indexSelect: number;
    modalImage = [];
    modalImagedetail = [];
    fileValue: number;

    // textareaForm = new FormGroup({
    //     objectiveText: new FormControl(this.stepOne.objectiveText),
    //     businessModelText: new FormControl(this.stepOne.businessModelText),
    //     detailText: new FormControl(this.stepOne.detailText),
    // });
    OverSize: any;
    repeat = false;
    downloadfile_by = [];

    constructor(
        private router: Router,
        private stepOneService: StepOneService,
        private sidebarService: RequestService,
        public dialog: MatDialog,
    ) {
    }
    viewNext() {
        this.router.navigate(['request/product-detail/step2']);
    }
    ngOnInit() {
        window.scrollTo(0, 0);
        console.log('template_manage', this.template_manage);
        this.openDoCheckc = true;
        this.sidebarService.sendEvent();
        if (localStorage) {
            if (localStorage.getItem('requestId')) {
                this.getDetail(localStorage.getItem('requestId'));
            } else {
                this.status_loading = false;
            }
        } else {
            console.log('Brownser not support');
        }
    }
    autogrow(index: any) {
        if (index === 1) {
            const objectiveText = document.getElementById('objectiveText');
            objectiveText.style.overflow = 'hidden';
            objectiveText.style.height = 'auto';
            objectiveText.style.height = objectiveText.scrollHeight + 'px';
        }

        if (index === 2) {
            const businessModelText = document.getElementById('businessModelText');
            businessModelText.style.overflow = 'hidden';
            businessModelText.style.height = 'auto';
            businessModelText.style.height = businessModelText.scrollHeight + 'px';
        }

        if (index === 3) {
            const detailText = document.getElementById('detailText');
            detailText.style.overflow = 'hidden';
            detailText.style.height = 'auto';
            detailText.style.height = detailText.scrollHeight + 'px';
        }
    }
    sendSaveStatus() {
        if (this.saveDraftstatus === 'success') {
            this.saveStatus.emit(true);
        } else {
            this.saveStatus.emit(false);
        }
    }

    getDetail(requestId) {
        this.stepOneService.getDetail(requestId, this.template_manage.pageShow).subscribe(res => {
            if (res['status'] === 'success') {
                console.log('res', res['data']);

                this.status_loading = false;

                if (res['data']) {


                    this.stepOne = res['data'];
                    console.log(this.template_manage.pageShow);
                    console.log(this.stepOne);
                    console.log(this.stepOne['validate']);
                    console.log(this.template_manage.validate);
                    // set fileDelete to Object stepOne
                    this.stepOne.objective.fileDelete = [];
                    this.stepOne.businessModel.fileDelete = [];
                    this.stepOne.detail.fileDelete = [];
                    this.stepOne.attachments.fileDelete = [];


                    // this.textareaForm = new FormGroup({
                    //     objectiveText: new FormControl(this.stepOne.objectiveText),
                    //     businessModelText: new FormControl(this.stepOne.businessModelText),
                    //     detailText: new FormControl(this.stepOne.detailText),
                    // });

                    // set picture preview to all input
                    for (let i = 0; i < this.stepOne.objective.file.length; i++) {
                        this.preview(1, this.stepOne.objective.file[i], i);
                    }

                    for (let i = 0; i < this.stepOne.businessModel.file.length; i++) {
                        this.preview(2, this.stepOne.businessModel.file[i], i);
                    }

                    for (let i = 0; i < this.stepOne.detail.file.length; i++) {
                        this.preview(3, this.stepOne.detail.file[i], i);
                    }


                    // set first time ratio
                    if (this.stepOne.externalCommit.yes === true) {
                        this.agreement = '1';
                    }
                    if (this.stepOne.externalCommit.no === true) {
                        this.agreement = '2';
                    }

                    // set file attachments
                    this.dataSource_file = new MatTableDataSource(this.stepOne.attachments.file);
                    this.hideDownloadFile();

                    // set status to stepData
                    this.stepOne.status = false;

                } else {
                    this.stepOne = this.stepOne;
                    this.stepOne['validate'] = res['validate'];
                }
            }
        }, err => {
            console.log(err);
        });
    }

    next_step() {
        this.router.navigate(['/request/product-detail/step2']);
    }

    saveDraft(page?, action?) {
        this.status_loading = true;
        if (this.validateStatus()) {
            this.stepOne.status = true;
        } else {
            this.stepOne.status = false;
        }
        console.log('status tosave', this.stepOne.status);

        console.log('data to save', this.stepOne);
        if (localStorage.getItem('requestId')) {
            this.stepOne.requestId = localStorage.getItem('requestId');
        }
        if (this.template_manage.pageShow === 'summary') {
            this.stepOne.conclusions = true;
        } else {
            this.stepOne.conclusions = null;
        }
        console.log('stepOne =>>>', this.stepOne);
        const subject = new Subject<boolean>();
        return this.stepOneService.updatePage(this.stepOne).subscribe(res => {
            console.log(res);
            if (res['status'] === 'success') {
                console.log('22225555', page);
                // if (page !== 'outpage') {
                //     this.status_loading = false;
                // }
                this.status_loading = false;
                this.stepOne.attachments.file = [];
                this.sidebarService.sendEvent();
                this.downloadfile_by = [];
                this.saveDraftstatus = 'success';
                this.sidebarService.inPageStatus(false);
                setTimeout(() => {
                    this.saveDraftstatus = 'fail';
                }, 3000);
                this.sendSaveStatus();
                this.getDetail(localStorage.getItem('requestId'));
                subject.next(true);
                return true;
            } else {
                this.sendSaveStatus();
                this.status_loading = false;
                subject.next(true);
                alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
                return false;
            }
        }, err => {
            console.log(err);
            this.sendSaveStatus();
            this.status_loading = false;
            subject.next(true);
            alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
            return false;
        });

        // return false;
    }

    changeSaveDraft() {
        this.saveDraftstatus = null;
        this.sendSaveStatus();
        this.sidebarService.inPageStatus(true);
    }

    validateStatus() {
        let status = false;
        const checkForm =
            this.stepOne.objectiveText &&
            this.stepOne.businessModelText &&
            this.stepOne.detailText &&
            (this.stepOne.externalCommit.yes === true || this.stepOne.externalCommit.no === true);


        let checkRatioForm = false;
        if (this.stepOne.externalCommit.yes) {
            if (this.stepOne.externalCommit.yesDetail.KTB || this.stepOne.externalCommit.yesDetail.external) {
                checkRatioForm = true;
            }
        } else {
            checkRatioForm = true;
        }

        status = checkForm && checkRatioForm;

        return status;
    }

    delete_image(fileIndex, index) {

        if (fileIndex === 1) {
            if ('id' in this.stepOne.objective.file[index]) {
                const id = this.stepOne.objective.file[index].id;
                if ('fileDelete' in this.stepOne.objective) {
                    this.stepOne.objective.fileDelete.push(id);
                }
            }
            this.stepOne.objective.file.splice(index, 1);
            this.image1.splice(index, 1);

            for (let i = 0; i < this.stepOne.objective.file.length; i++) {
                this.preview(fileIndex, this.stepOne.objective.file[i], i);
            }
        } else if (fileIndex === 2) {
            if ('id' in this.stepOne.businessModel.file[index]) {
                const id = this.stepOne.businessModel.file[index].id;
                if ('fileDelete' in this.stepOne.businessModel) {
                    this.stepOne.businessModel.fileDelete.push(id);
                }
            }
            this.stepOne.businessModel.file.splice(index, 1);
            this.image2.splice(index, 1);

            for (let i = 0; i < this.stepOne.businessModel.file.length; i++) {
                this.preview(fileIndex, this.stepOne.businessModel.file[i], i);
            }
        } else if (fileIndex === 3) {
            if ('id' in this.stepOne.detail.file[index]) {
                const id = this.stepOne.detail.file[index].id;
                if ('fileDelete' in this.stepOne.detail) {
                    this.stepOne.detail.fileDelete.push(id);
                }
            }
            this.stepOne.detail.file.splice(index, 1);
            this.image3.splice(index, 1);

            for (let i = 0; i < this.stepOne.detail.file.length; i++) {
                this.preview(fileIndex, this.stepOne.detail.file[i], i);
            }
        }
        this.changeSaveDraft();
    }

    image_click(colId, id) {
        this.showModel = true;
        document.getElementById('myModal1').style.display = 'block';
        this.imageModal(colId, id);
    }

    image_bdclick() {
        document.getElementById('myModal1').style.display = 'block';
    }

    closeModal() {
        this.showModel = false;
        document.getElementById('myModal1').style.display = 'none';
    }

    ratioChange() {
        if (this.agreement === '1') {
            this.stepOne.externalCommit.yes = true;
            this.stepOne.externalCommit.no = false;
        } else if (this.agreement === '2') {
            this.stepOne.externalCommit.yes = false;
            this.stepOne.externalCommit.no = true;
            this.stepOne.externalCommit.yesDetail.KTB = '';
            this.stepOne.externalCommit.yesDetail.external = '';
        }
    }

    // getFileDetails ================================================================
    getFileDetails(fileIndex, event) {
        const files = event.target.files;

        for (let i = 0; i < files.length; i++) {
            const mimeType = files[i].type;
            console.log(mimeType);
            if (mimeType.match('image/jpeg|image/png') == null) {
                this.message = 'Only images are supported.';
                this.status_loading = false;
                alert(this.message);
                return;
            }
            const file = files[i];
            console.log('file:', file);
            const reader = new FileReader();
            reader.onload = (_event) => {
                const rawReadFile = reader.result as string;
                const splitFile = rawReadFile.split(',');

                const templateFile = {
                    fileName: file.name,
                    base64File: splitFile[1],
                };

                if (fileIndex === 1) {
                    console.log('00000000000000000000000000000001');
                    this.stepOne.objective.file.push(templateFile);
                    console.log(this.stepOne);
                    for (let index = 0; index < this.stepOne.objective.file.length; index++) {
                        this.preview(fileIndex, this.stepOne.objective.file[index], index);
                    }
                    this.file1_1 = undefined;
                    this.file1_2 = undefined;
                } else if (fileIndex === 2) {
                    console.log('00000000000000000000000000000002');
                    this.stepOne.businessModel.file.push(templateFile);

                    for (let index = 0; index < this.stepOne.businessModel.file.length; index++) {
                        this.preview(fileIndex, this.stepOne.businessModel.file[index], index);
                        this.file2_1 = undefined;
                        this.file2_2 = undefined;
                    }
                } else if (fileIndex === 3) {
                    console.log('00000000000000000000000000000003');
                    this.stepOne.detail.file.push(templateFile);

                    for (let index = 0; index < this.stepOne.detail.file.length; index++) {
                        this.preview(fileIndex, this.stepOne.detail.file[index], index);
                    }
                    this.file3_1 = undefined;
                    this.file3_2 = undefined;
                }

            };
            reader.readAsDataURL(file);

        }
        this.changeSaveDraft();
    }

    preview(fileIndex, file, index) {

        const name = file.fileName;
        const typeFiles = name.match(/\.\w+$/g);
        const typeFile = typeFiles[0].replace('.', '');

        const tempPicture = `data:image/${typeFile};base64,${file.base64File}`;

        if (fileIndex === 1) {
            if (file) {
                this.image1[index] = tempPicture;
            }
        } else if (fileIndex === 2) {
            if (file) {
                this.image2[index] = tempPicture;
            }
        } else if (fileIndex === 3) {
            if (file) {
                this.image3[index] = tempPicture;
            }
        }
    }

    // getFileDocument ================================================================

    onSelectFile(event) {

        this.OverSize = [];

        if (event.target.files && event.target.files[0]) {
            const filesAmount = event.target.files.length;
            const file = event.target.files;
            for (let i = 0; i < filesAmount; i++) {
                // console.log('type:', file[i]);
                if (
                    // file[i].type === 'image/svg+xml' ||
                    file[i].type === 'image/jpeg' ||
                    // file[i].type === 'image/raw' ||
                    // file[i].type === 'image/svg' ||
                    // file[i].type === 'image/tif' ||
                    // file[i].type === 'image/gif' ||
                    file[i].type === 'image/jpg' ||
                    file[i].type === 'image/png' ||
                    file[i].type === 'application/doc' ||
                    file[i].type === 'application/pdf' ||
                    file[i].type ===
                    'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
                    file[i].type ===
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
                    file[i].type ===
                    'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
                    file[i].type === 'application/pptx' ||
                    file[i].type === 'application/docx' ||
                    (file[i].type === 'application/ppt' && file[i].size)
                ) {
                    if (file[i].size <= 5000000) {
                        this.multiple_file(file[i], i);
                    } else {
                        this.OverSize.push(file[i].name);
                    }
                } else {
                    this.status_loading = false;
                    alert('File Not Support');
                }
            }


        }

        if (this.OverSize.length !== 0) {
            console.log('open modal');
            document.getElementById('testttt').click();
        }
    }

    multiple_file(file, index) {
        // console.log('download');
        const reader = new FileReader();
        if (file) {
            reader.readAsDataURL(file);
            reader.onload = (_event) => {
                const rawReadFile = reader.result as string;
                const splitFile = rawReadFile.split(',');

                const templateFile = {
                    fileName: file.name,
                    fileSize: file.size,
                    base64File: splitFile[1],
                };
                this.stepOne.attachments.file.push(templateFile);
                this.dataSource_file = new MatTableDataSource(this.stepOne.attachments.file);
                this.hideDownloadFile();
            };
        }
        this.changeSaveDraft();
    }

    delete_mutifile(data: any, index: any) {
        // console.log('data:', data, 'index::', index);
        if ('id' in this.stepOne.attachments.file[index]) {
            const id = this.stepOne.attachments.file[index].id;
            if ('fileDelete' in this.stepOne.attachments) {
                this.stepOne.attachments.fileDelete.push(id);
            }
        }
        // console.log('fileDelete attachments : ', this.stepOne.attachments.fileDelete);
        this.stepOne.attachments.file.splice(index, 1);
        this.dataSource_file = new MatTableDataSource(this.stepOne.attachments.file);
        this.changeSaveDraft();
    }

    hideDownloadFile() {
        if (this.dataSource_file.data.length > 0) {
            this.downloadfile_by = [];

            for (let index = 0; index < this.dataSource_file.data.length; index++) {
                const element = this.dataSource_file.data[index];
                console.log('ele', element);

                if ('id' in element) {
                    this.downloadfile_by.push('path');
                } else {
                    this.downloadfile_by.push('base64');
                }
            }
        }
        console.log('by:', this.downloadfile_by);
    }




    // ============================== Modal ==========================================
    imageModal(fileIndex, index) {
        if (fileIndex === 1) {
            this.modalmainImageIndex = index;
            this.modalImage = this.image1;
            this.modalImagedetail = this.stepOne.objective.file;
            this.indexSelect = index;
            this.fileValue = fileIndex;
        } else if (fileIndex === 2) {
            this.modalmainImageIndex = index;
            this.modalImage = this.image2;
            this.modalImagedetail = this.stepOne.businessModel.file;
            this.indexSelect = index;
            this.fileValue = fileIndex;
        } else if (fileIndex === 3) {
            this.modalmainImageIndex = index;
            this.modalImage = this.image3;
            this.modalImagedetail = this.stepOne.detail.file;
            this.indexSelect = index;
            this.fileValue = fileIndex;
        }
    }

    // ===============================================================================

    downloadFile(pathdata: any) {
        this.status_loading = true;
        const contentType = '';
        const sendpath = pathdata;
        console.log('path', sendpath);
        this.sidebarService.getfile(sendpath).subscribe(res => {
            if (res['status'] === 'success' && res['data']) {
                this.status_loading = false;
                const datagetfile = res['data'];
                const b64Data = datagetfile['data'];
                const filename = datagetfile['fileName'];
                console.log('base64', b64Data);
                const config = this.b64toBlob(b64Data, contentType);
                saveAs(config, filename);
            }

        });
    }

    b64toBlob(b64Data, contentType = '', sliceSize = 512) {
        const convertbyte = atob(b64Data);
        const byteCharacters = atob(convertbyte);
        const byteArrays = [];
        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            const slice = byteCharacters.slice(offset, offset + sliceSize);

            const byteNumbers = new Array(slice.length);
            for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            const byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }

        const blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }

    downloadFile64(data: any) {
        this.status_loading = true;
        const contentType = '';
        this.status_loading = false;
        const b64Data = data.base64File;
        const filename = data.fileName;
        console.log('base64', b64Data);

        const config = this.base64(b64Data, contentType);
        saveAs(config, filename);
    }

    base64(b64Data, contentType = '', sliceSize = 512) {
        const byteCharacters = atob(b64Data);
        const byteArrays = [];
        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            const slice = byteCharacters.slice(offset, offset + sliceSize);

            const byteNumbers = new Array(slice.length);
            for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            const byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }

        const blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }

    // ngDoCheck() {
    //     if (this.openDoCheckc === true) {
    //         this.pageChange = this.sidebarService.changePageGoToLink();
    //         if (this.pageChange) {
    //             // this.getNavbar();
    //             const data = this.sidebarService.getsavePage();
    //             let saveInFoStatus = false;
    //             saveInFoStatus = data.saveStatusInfo;
    //             if (saveInFoStatus === true) {
    //                 this.outOfPageSave = true;
    //                 this.linkTopage = data.goToLink;
    //                 this.saveDraft('navbar', null);
    //                 this.sidebarService.resetSaveStatu();
    //             }
    //         }
    //     }
    // }

    canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
        console.log('canDeactivate request');
        const subject = new Subject<boolean>();
        const status = this.sidebarService.outPageStatus();
        if (status === true) {
          // -----
          const dialogRef =  this.dialog.open(DialogSaveStatusComponent, {
            disableClose: true,
            data: {
              headerDetail: 'ยืนยันการดำเนินการ',
              bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
              bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
              returnDetail: 'null',
              returnStatus: false,
              icon: 'assets/img/Alarm-blue.svg',
            }
          });
          dialogRef.afterClosed().subscribe((data) => {
            if (data.returnStatus === true) {
              const raturnStatus = this.saveDraft('outpage');
              console.log('request raturnStatus', raturnStatus);
                if (raturnStatus) {
                  console.log('raturnStatus T', this.saveDraftstatus);
                  subject.next(true);
                } else {
                  console.log('raturnStatus F', this.saveDraftstatus);
                  subject.next(false);
                }
            } else if (data.returnStatus === false) {
              this.sidebarService.inPageStatus(false);
              subject.next(true);
              return true;
            } else {
              subject.next(false);
            }
          });
          return subject;
        } else {
          return true;
        }
      }

}


