import { ReplaySubject } from 'rxjs';
import { RequestService } from './../../../../services/request/request.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
// tslint:disable-next-line: max-line-length
import { StepTwoService } from '../../../../services/request/product-detail/step-two/step-two.service';
import { Router } from '@angular/router';
import { saveAs } from 'file-saver';
import { FormGroup, FormControl } from '@angular/forms';
import { DialogSaveStatusComponent } from '../../dialog/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from './../../../../services/configGuard/config-guard.service';

@Component({
    selector: 'app-step-two',
    templateUrl: './step-two.component.html',
    styleUrls: ['./step-two.component.scss']
})
export class StepTwoComponent implements OnInit, CanComponentDeactivate {
    @Input() template_manage = { role: null, validate: null, caaStatus: null, pageShow: null };
    @Output() saveStatus = new EventEmitter<boolean>();
    displayedColumns_file: string[] = ['name', 'size', 'delete'];
    dataSource_file: MatTableDataSource<FileList>;
    urls: any = [];
    status_loading = true;

    saveDraftstatus = null;
    outOfPageSave = false;
    linkTopage = null;
    pageChange = null;
    openDoCheckc = false;
    showCircle_colId = null;
    showCircle_id = null;
    enter = 3;
    stepTwo = {
        requestId: null,
        markting: {
            file: [],
            fileDelete: []
        },
        marktingText: null,
        competitor: {
            file: [],
            fileDelete: []
        },
        competitorText: null,
        similar: {
            file: [],
            fileDelete: []
        },
        similarText: null,
        compensate: {
            file: [],
            fileDelete: []
        },
        compensateText: null,
        target: {
            file: [],
            fileDelete: []
        },
        targetText: null,
        attachments: {
            file: [],
            fileDelete: []
        },
        status: false,
        validate: null,
        conclusions: null
    };

    file1_1: any;
    file1_2: any;
    file2_1: any;
    file2_2: any;
    file3_1: any;
    file3_2: any;
    file4_1: any;
    file4_2: any;
    file5_1: any;
    file5_2: any;
    document: any;
    Agreement = null;
    file_name: any;
    file_size: number;

    imgURL: any;
    image1 = [];
    image2 = [];
    image3 = [];
    image4 = [];
    image5 = [];
    showImage1 = false;
    message = null;


    // Model
    showModel = false;
    modalmainImageIndex: number;
    modalmainImagedetailIndex: number;
    indexSelect: number;
    modalImage = [];
    modalImagedetail = [];
    fileValue: number;

    testfile: any;

    //   textareaForm = new FormGroup({
    //     marktingText: new FormControl(this.stepTwo.marktingText),
    //     competitorText: new FormControl(this.stepTwo.competitorText),
    //     similarText: new FormControl(this.stepTwo.similarText),
    //     compensateText: new FormControl(this.stepTwo.compensateText),
    //     targetText: new FormControl(this.stepTwo.targetText),
    //   });

    //   coppy_paste_status: number = 0;
    FileOverSize: any;
    downloadfile_by = [];

    constructor(
        private productDetailStepTwo: StepTwoService,
        private router: Router,
        private sidebarService: RequestService,
        public dialog: MatDialog,
    ) { }

    ngOnInit() {
        window.scrollTo(0, 0);
        this.openDoCheckc = true;
        if (localStorage.getItem('requestId')) {
            this.sidebarService.sendEvent();
        }
        if (localStorage) {
            if (localStorage.getItem('requestId')) {
                console.log('localStorage', localStorage.getItem('requestId'));
                const requestId = Number(localStorage.getItem('requestId'));
                this.getDataProductDetailStepTwo(requestId);
            } else {
                this.status_loading = false;
            }
        } else {
            console.log('Brownser not support');
        }
    }
    viewNext() {
        this.router.navigate(['request/product-detail/step3']);
    }
    autogrow(index: any) {

        if (index === 1) {
            const marktingText = document.getElementById('marktingText');
            marktingText.style.overflow = 'hidden';
            marktingText.style.height = 'auto';
            marktingText.style.height = marktingText.scrollHeight + 'px';
        }

        if (index === 2) {
            const competitorText = document.getElementById('competitorText');
            competitorText.style.overflow = 'hidden';
            competitorText.style.height = 'auto';
            competitorText.style.height = competitorText.scrollHeight + 'px';
        }
        if (index === 3) {
            const similarText = document.getElementById('similarText');
            similarText.style.overflow = 'hidden';
            similarText.style.height = 'auto';
            similarText.style.height = similarText.scrollHeight + 'px';
        }
        if (index === 4) {
            const compensateText = document.getElementById('compensateText');
            compensateText.style.overflow = 'hidden';
            compensateText.style.height = 'auto';
            compensateText.style.height = compensateText.scrollHeight + 'px';
        }
        if (index === 5) {
            const targetText = document.getElementById('targetText');
            targetText.style.overflow = 'hidden';
            targetText.style.height = 'auto';
            targetText.style.height = targetText.scrollHeight + 'px';
        }
    }
    sendSaveStatus() {
        if (this.saveDraftstatus === 'success') {
            this.saveStatus.emit(true);
        } else {
            this.saveStatus.emit(false);
        }
    }

    saveDraft(page?, action?) {
        const returnUpdateData = new Subject<any>();
        this.status_loading = true;
        console.log(this.stepTwo);
        if (localStorage.getItem('requestId')) {
            this.stepTwo.requestId = localStorage.getItem('requestId');
        }
        if (((((this.stepTwo.marktingText
            && this.stepTwo.competitorText)
            && this.stepTwo.similarText)
            && this.stepTwo.compensateText)
            && this.stepTwo.targetText)
        ) {
            this.stepTwo.status = true;
        } else {
            this.stepTwo.status = false;
        }

        if (this.template_manage.pageShow === 'summary') {
            this.stepTwo.conclusions = true;
        } else {
            this.stepTwo.conclusions = null;
        }

        this.productDetailStepTwo.updateProductDetailStepTwo(this.stepTwo).subscribe(res => {
            if (res['status'] === 'success') {
                this.status_loading = false;
                this.saveDraftstatus = 'success';
                returnUpdateData.next(true);
                this.sidebarService.inPageStatus(false);
                setTimeout(() => {
                    this.saveDraftstatus = 'fail';
                }, 3000);
                this.sendSaveStatus();
                this.stepTwo.attachments.file = [];
                this.downloadfile_by = [];
                this.sidebarService.sendEvent();
                this.getDataProductDetailStepTwo(localStorage.getItem('requestId'));
            } else {
                this.status_loading = false;
                alert(res['message']);
                returnUpdateData.next(false);
            }
        }, err => {
            console.log(err);
            this.sendSaveStatus();
            this.status_loading = false;
            alert(err['message']);
            returnUpdateData.next(false);
        });
        return returnUpdateData;
    }

    next_step() {
        this.router.navigate(['/request/product-detail/step3']);
    }

    changeSaveDraft() {
        this.saveDraftstatus = null;
        this.sendSaveStatus();
        this.sidebarService.inPageStatus(true);
    }

    // ================================================================================

    delete_image(fileIndex, index) {
        console.log('fileIndex:', fileIndex);
        console.log('index:', index);
        if (fileIndex === 1) {
            if ('id' in this.stepTwo.markting.file[index]) {
                const id = this.stepTwo.markting.file[index].id;
                console.log('found id');
                if ('fileDelete' in this.stepTwo.markting) {
                    this.stepTwo.markting.fileDelete.push(id);
                }
            }
            this.stepTwo.markting.file.splice(index, 1);
            this.image1.splice(index, 1);

            for (let i = 0; i < this.stepTwo.markting.file.length; i++) {
                this.preview(fileIndex, this.stepTwo.markting.file[i], i);
            }
        } else if (fileIndex === 2) {
            if ('id' in this.stepTwo.competitor.file[index]) {
                const id = this.stepTwo.competitor.file[index].id;
                console.log('found id');
                if ('fileDelete' in this.stepTwo.competitor) {
                    this.stepTwo.competitor.fileDelete.push(id);
                }
            }
            this.stepTwo.competitor.file.splice(index, 1);
            this.image2.splice(index, 1);

            for (let i = 0; i < this.stepTwo.competitor.file.length; i++) {
                this.preview(fileIndex, this.stepTwo.competitor.file[i], i);
            }
        } else if (fileIndex === 3) {
            if ('id' in this.stepTwo.similar.file[index]) {
                const id = this.stepTwo.similar.file[index].id;
                console.log('found id');
                if ('fileDelete' in this.stepTwo.similar) {
                    this.stepTwo.similar.fileDelete.push(id);
                }
            }
            this.stepTwo.similar.file.splice(index, 1);
            this.image3.splice(index, 1);

            for (let i = 0; i < this.stepTwo.similar.file.length; i++) {
                this.preview(fileIndex, this.stepTwo.similar.file[i], i);
            }
        } else if (fileIndex === 4) {
            if ('id' in this.stepTwo.compensate.file[index]) {
                const id = this.stepTwo.compensate.file[index].id;
                console.log('found id');
                if ('fileDelete' in this.stepTwo.compensate) {
                    this.stepTwo.compensate.fileDelete.push(id);
                }
            }
            this.stepTwo.compensate.file.splice(index, 1);
            this.image4.splice(index, 1);

            for (let i = 0; i < this.stepTwo.compensate.file.length; i++) {
                this.preview(fileIndex, this.stepTwo.compensate.file[i], i);
            }
        } else if (fileIndex === 5) {
            if ('id' in this.stepTwo.target.file[index]) {
                const id = this.stepTwo.target.file[index].id;
                console.log('found id');
                if ('fileDelete' in this.stepTwo.target) {
                    this.stepTwo.target.fileDelete.push(id);
                }
            }
            this.stepTwo.target.file.splice(index, 1);
            this.image5.splice(index, 1);

            for (let i = 0; i < this.stepTwo.target.file.length; i++) {
                this.preview(fileIndex, this.stepTwo.target.file[i], i);
            }
        }
        this.changeSaveDraft();

        if (this.FileOverSize.length !== 0) {
            console.log('open modal');
            document.getElementById('testttt').click();
        }
    }

    image_bdclick() {
        document.getElementById('myModal2').style.display = 'block';
    }

    closeModal() {
        this.showModel = false;
        document.getElementById('myModal2').style.display = 'none';
    }

    // getFileDetails ================================================================
    getFileDetails(fileIndex, event) {

        const files = event.target.files;
        for (let i = 0; i < files.length; i++) {
            const mimeType = files[i].type;
            if (mimeType.match('image/jpeg|image/png') == null) {
                this.message = 'Only images are supported.';
                this.status_loading = false;
                alert(this.message);
                return;
            }
            const file = files[i];
            // this.file_name = file.name;
            // this.file_type = file.type;
            // this.file_size = file.size;
            // this.modifiedDate = file.lastModifiedDate;
            console.log('file:', file);
            const reader = new FileReader();
            reader.onload = (_event) => {
                const rawReadFile = reader.result as string;
                const splitFile = rawReadFile.split(',');

                const templateFile = {
                    fileName: file.name,
                    base64File: splitFile[1],
                };
                console.log('templateFile123456', templateFile);

                if (fileIndex === 1) {
                    console.log('00000000000000000000000000000001');
                    this.stepTwo.markting.file.push(templateFile);

                    for (let index = 0; index < this.stepTwo.markting.file.length; index++) {
                        this.preview(fileIndex, this.stepTwo.markting.file[index], index);
                    }
                    this.file1_1 = undefined;
                    this.file1_2 = undefined;
                } else if (fileIndex === 2) {
                    console.log('00000000000000000000000000000002');
                    this.stepTwo.competitor.file.push(templateFile);


                    for (let index = 0; index < this.stepTwo.competitor.file.length; index++) {
                        this.preview(fileIndex, this.stepTwo.competitor.file[index], index);
                        this.file2_1 = undefined;
                        this.file2_2 = undefined;
                    }
                } else if (fileIndex === 3) {
                    console.log('00000000000000000000000000000003');
                    this.stepTwo.similar.file.push(templateFile);

                    for (let index = 0; index < this.stepTwo.similar.file.length; index++) {
                        this.preview(fileIndex, this.stepTwo.similar.file[index], index);
                    }
                    this.file3_1 = undefined;
                    this.file3_2 = undefined;
                } else if (fileIndex === 4) {
                    console.log('00000000000000000000000000000004');
                    this.stepTwo.compensate.file.push(templateFile);

                    for (let index = 0; index < this.stepTwo.compensate.file.length; index++) {
                        this.preview(fileIndex, this.stepTwo.compensate.file[index], index);
                    }
                    this.file4_1 = undefined;
                    this.file4_2 = undefined;
                } else if (fileIndex === 5) {
                    console.log('00000000000000000000000000000005');
                    this.stepTwo.target.file.push(templateFile);

                    for (let index = 0; index < this.stepTwo.target.file.length; index++) {
                        console.log('ffff', this.stepTwo.target.file[index]);
                        this.preview(fileIndex, this.stepTwo.target.file[index], index);
                    }
                    this.file5_1 = undefined;
                    this.file5_2 = undefined;
                }
            };
            reader.readAsDataURL(file);
        }
        this.changeSaveDraft();
    }

    preview(fileIndex, file, index) {
        const name = file.fileName;
        const typeFiles = name.match(/\.\w+$/g);
        const typeFile = typeFiles[0].replace('.', '');

        const tempPicture = `data:image/${typeFile};base64,${file.base64File}`;

        if (fileIndex === 1) {
            if (file) {
                this.image1[index] = tempPicture;
                // console.log('preview image1', this.image1[0]);
            }
        } else if (fileIndex === 2) {
            if (file) {
                this.image2[index] = tempPicture;
            }
        } else if (fileIndex === 3) {
            if (file) {
                this.image3[index] = tempPicture;
            }
        } else if (fileIndex === 4) {
            if (file) {
                this.image4[index] = tempPicture;
            }
        } else if (fileIndex === 5) {
            if (file) {
                this.image5[index] = tempPicture;
            }
        }

    }

    // getFileDocument ================================================================
    // getFileDocument(event) {
    //   const file = event.target.files[0];
    //   const mimeType = file.type;
    //   if (mimeType.match(/pdf\/*/) == null) {
    //     this.message = 'Only PDF are supported.';
    //     alert(this.message);
    //     return;
    //   }
    //   console.log('11111', file);
    //   this.file_name = file.name;
    //   // this.file_type = file.type;
    //   this.file_size = file.size / 1024;
    //   // this.modifiedDate = file.lastModifiedDate;
    //   // console.log('file:', file);

    //   this.stepTwo.attachments = file;
    //   // this.document = undefined;
    // }

    onSelectFile(event) {

        this.FileOverSize = [];

        if (event.target.files && event.target.files[0]) {
            const filesAmount = event.target.files.length;
            const file = event.target.files;
            for (let i = 0; i < filesAmount; i++) {
                console.log('type:', file[i]);
                if (
                    // file[i].type === 'image/svg+xml' ||
                    file[i].type === 'image/jpeg' ||
                    // file[i].type === 'image/raw' ||
                    // file[i].type === 'image/svg' ||
                    // file[i].type === 'image/tif' ||
                    // file[i].type === 'image/gif' ||
                    file[i].type === 'image/jpg' ||
                    file[i].type === 'image/png' ||
                    file[i].type === 'application/doc' ||
                    file[i].type === 'application/pdf' ||
                    file[i].type ===
                    'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
                    file[i].type ===
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
                    file[i].type ===
                    'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
                    file[i].type === 'application/pptx' ||
                    file[i].type === 'application/docx' ||
                    (file[i].type === 'application/ppt' && file[i].size)
                ) {
                    if (file[i].size <= 5000000) {
                        this.multiple_file(file[i], i);
                    } else {
                        this.FileOverSize.push(file[i].name);
                    }
                } else {
                    this.status_loading = false;
                    alert('File Not Support');
                }
            }

            if (this.FileOverSize.length !== 0) {
                console.log('open modal');
                document.getElementById('testttt').click();
            }
        }
        this.changeSaveDraft();
    }

    multiple_file(file, index) {
        // console.log('download');
        const reader = new FileReader();
        if (file) {
            reader.readAsDataURL(file);
            reader.onload = (_event) => {
                const rawReadFile = reader.result as string;
                const splitFile = rawReadFile.split(',');

                const templateFile = {
                    fileName: file.name,
                    fileSize: file.size,
                    base64File: splitFile[1],
                };
                this.stepTwo.attachments.file.push(templateFile);
                this.dataSource_file = new MatTableDataSource(this.stepTwo.attachments.file);
                this.hideDownloadFile();
            };
        }
    }

    delete_mutifile(data: any, index: any) {
        console.log('data:', data, 'index::', index);
        if ('id' in this.stepTwo.attachments.file[index]) {
            const id = this.stepTwo.attachments.file[index].id;
            console.log('found id');
            if ('fileDelete' in this.stepTwo.attachments) {
                this.stepTwo.attachments.fileDelete.push(id);
            }
        }
        console.log('fileDelete attachments : ', this.stepTwo.attachments.fileDelete);
        this.stepTwo.attachments.file.splice(index, 1);
        this.dataSource_file = new MatTableDataSource(this.stepTwo.attachments.file);
        this.changeSaveDraft();
    }


    // ============================== Modal ==========================================
    imageModal(fileIndex, index) {
        if (fileIndex === 1) {
            this.modalmainImageIndex = index;
            this.modalImage = this.image1;
            this.modalImagedetail = this.stepTwo.markting.file;
            this.indexSelect = index;
            this.fileValue = fileIndex;
        } else if (fileIndex === 2) {
            this.modalmainImageIndex = index;
            this.modalImage = this.image2;
            this.modalImagedetail = this.stepTwo.competitor.file;
            this.indexSelect = index;
            this.fileValue = fileIndex;
        } else if (fileIndex === 3) {
            this.modalmainImageIndex = index;
            this.modalImage = this.image3;
            this.modalImagedetail = this.stepTwo.similar.file;
            this.indexSelect = index;
            this.fileValue = fileIndex;
        } else if (fileIndex === 4) {
            this.modalmainImageIndex = index;
            this.modalImage = this.image4;
            this.modalImagedetail = this.stepTwo.compensate.file;
            this.indexSelect = index;
            this.fileValue = fileIndex;
        } else if (fileIndex === 5) {
            this.modalmainImageIndex = index;
            this.modalImage = this.image5;
            this.modalImagedetail = this.stepTwo.target.file;
            this.indexSelect = index;
            this.fileValue = fileIndex;
        }
    }
    image_click(colId, id) {
        this.showModel = true;
        document.getElementById('myModal2').style.display = 'block';
        this.imageModal(colId, id);
    }

    // ===============================================================================
    // ======================================= API ===================================
    getDataProductDetailStepTwo(documentID) {
        this.productDetailStepTwo.getProductDetailStepTwo(documentID, this.template_manage.pageShow).subscribe(res => {
            console.log('res', res);
            if (res['status'] === 'success') {
                this.status_loading = false;
            }
            if (res['data'] !== null) {
                this.stepTwo = res['data'];

                // set fileDelete to Object stepOne
                this.stepTwo.markting.fileDelete = [];
                this.stepTwo.competitor.fileDelete = [];
                this.stepTwo.similar.fileDelete = [];
                this.stepTwo.compensate.fileDelete = [];
                this.stepTwo.target.fileDelete = [];
                this.stepTwo.attachments.fileDelete = [];


                console.log('set fileDelete', this.stepTwo);

                // set picture preview to all input
                for (let i = 0; i < this.stepTwo.markting.file.length; i++) {
                    this.preview(1, this.stepTwo.markting.file[i], i);
                }

                for (let i = 0; i < this.stepTwo.competitor.file.length; i++) {
                    this.preview(2, this.stepTwo.competitor.file[i], i);
                }

                for (let i = 0; i < this.stepTwo.similar.file.length; i++) {
                    this.preview(3, this.stepTwo.similar.file[i], i);
                }

                for (let i = 0; i < this.stepTwo.compensate.file.length; i++) {
                    this.preview(4, this.stepTwo.compensate.file[i], i);
                }

                for (let i = 0; i < this.stepTwo.target.file.length; i++) {
                    this.preview(5, this.stepTwo.target.file[i], i);
                }

                // set file attachments
                this.dataSource_file = new MatTableDataSource(this.stepTwo.attachments.file);
                this.hideDownloadFile();
            } else {
                this.stepTwo.validate = res['validate'];
            }
        }, err => {
            console.log(err);
        });
    }

    // downloadFile
    downloadFile(pathdata: any) {
        this.status_loading = true;
        const contentType = '';
        const sendpath = pathdata;
        console.log('path', sendpath);
        this.sidebarService.getfile(sendpath).subscribe(res => {
            if (res['status'] === 'success' && res['data']) {
                this.status_loading = false;
                const datagetfile = res['data'];
                const b64Data = datagetfile['data'];
                const filename = datagetfile['fileName'];
                console.log('base64', b64Data);
                const config = this.b64toBlob(b64Data, contentType);
                saveAs(config, filename);
            }

        });
    }

    b64toBlob(b64Data, contentType = '', sliceSize = 512) {
        const convertbyte = atob(b64Data);
        const byteCharacters = atob(convertbyte);
        const byteArrays = [];
        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            const slice = byteCharacters.slice(offset, offset + sliceSize);

            const byteNumbers = new Array(slice.length);
            for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            const byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }

        const blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }

    downloadFile64(data: any) {
        this.status_loading = true;
        const contentType = '';
        this.status_loading = false;
        const b64Data = data.base64File;
        const filename = data.fileName;
        console.log('base64', b64Data);

        const config = this.base64(b64Data, contentType);
        saveAs(config, filename);
    }

    base64(b64Data, contentType = '', sliceSize = 512) {
        const byteCharacters = atob(b64Data);
        const byteArrays = [];
        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            const slice = byteCharacters.slice(offset, offset + sliceSize);

            const byteNumbers = new Array(slice.length);
            for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            const byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }

        const blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }

    // ngDoCheck() {
    //     if (this.openDoCheckc === true) {
    //         this.pageChange = this.sidebarService.changePageGoToLink();
    //         if (this.pageChange) {
    //             const data = this.sidebarService.getsavePage();
    //             let saveInFoStatus = false;
    //             saveInFoStatus = data.saveStatusInfo;
    //             if (saveInFoStatus === true) {
    //                 this.outOfPageSave = true;
    //                 this.linkTopage = data.goToLink;
    //                 console.log('ddddddddddddddd', saveInFoStatus);
    //                 this.saveDraft('navbar', null);
    //                 this.sidebarService.resetSaveStatu();
    //             }
    //         }
    //     }
    // }

    hideDownloadFile() {
        if (this.dataSource_file.data.length > 0) {
            this.downloadfile_by = [];

            for (let index = 0; index < this.dataSource_file.data.length; index++) {
                const element = this.dataSource_file.data[index];
                console.log('ele', element);

                if ('id' in element) {
                    this.downloadfile_by.push('path');
                } else {
                    this.downloadfile_by.push('base64');
                }
            }
        }
        console.log('by:', this.downloadfile_by);

    }

    canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
        console.log('canDeactivate request');
        const subject = new Subject<boolean>();
        const status = this.sidebarService.outPageStatus();
        if (status === true) {
        // -----
        const dialogRef =  this.dialog.open(DialogSaveStatusComponent, {
            disableClose: true,
            data: {
            headerDetail: 'ยืนยันการดำเนินการ',
            bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
            bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
            returnDetail: 'null',
            returnStatus: false,
            icon: 'assets/img/Alarm-blue.svg',
            }
        });
        dialogRef.afterClosed().subscribe((data) => {
            if (data.returnStatus === true) {
            const raturnStatus = this.saveDraft();
            console.log('request raturnStatus', raturnStatus);
                if (raturnStatus) {
                  console.log('raturnStatus T', this.saveDraftstatus);
                  subject.next(true);
                } else {
                  console.log('raturnStatus F', this.saveDraftstatus);
                  subject.next(false);
                }
            } else if (data.returnStatus === false) {
            this.sidebarService.inPageStatus(false);
            subject.next(true);
            return true;
            } else {
            subject.next(false);
            }
        });
        return subject;
        } else {
        return true;
        }
    }
}
