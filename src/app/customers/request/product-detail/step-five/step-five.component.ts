import { dateFormat } from 'highcharts';
import { element } from 'protractor';
import { Data } from './../../../dashboard-cus/dashboard-cus.component';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import {
  AngularMyDatePickerDirective,
  IAngularMyDpOptions,
  IMyDateModel
} from 'angular-mydatepicker';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { CdkDragDrop, moveItemInArray, transferArrayItem, CdkDragExit, CdkDragEnter, CdkDragStart } from '@angular/cdk/drag-drop';
import { StepFiveService } from '../../../../services/request/product-detail/step-five/step-five.service';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { RequestService } from '../../../../services/request/request.service';
import { DateComponent } from '../../../datepicker/date/date.component';
import { saveAs } from 'file-saver';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogSaveStatusComponent } from '../../dialog/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from '../../../../services/configGuard/config-guard.service';

@Component({
  selector: 'app-step-five',
  templateUrl: './step-five.component.html',
  styleUrls: ['./step-five.component.scss']
})
export class StepFiveComponent implements OnInit, CanComponentDeactivate {
  @Input() template_manage = { role: null, validate: null, caaStatus: null, pageShow: null };
  @Output() saveStatus = new EventEmitter<boolean>();

  @ViewChild(DateComponent) Date: DateComponent;

  stepFive = {
    validate: null,
    requestId: null,
    status: false,
    operations: [
      {
        process: 'การพัฒนาระบบ',
        detail: [
          {
            subProcess: '',
            management: '',
            responsible: '',
            dueDate: null
          },
        ],
        detailDelete: []
      },
      {
        process: 'ความพร้อมของอุปกรณ์/กระบวนการ (End to End workflow)',
        detail: [
          {
            subProcess: '',
            management: '',
            responsible: '',
            dueDate: null
          },
        ],
        detailDelete: []
      },
      {
        id: '',
        process: 'ความพร้อมของระบบงาน IT',
        detail: [
          {
            subProcess: '',
            management: '',
            responsible: '',
            dueDate: null
          },
        ],
        detailDelete: []
      },
      {
        id: '',
        process: 'ความพร้อมของ ระเบียบ /คู่มือ /การฝึกอบรม /รายงาน',
        detail: [
          {
            subProcess: '',
            management: '',
            responsible: '',
            dueDate: null
          },
        ],
        detailDelete: []
      },
      {
        id: '',
        process: 'การจัดซื้อจัดจ้าง (ถ้ามี)',
        detail: [
          {
            subProcess: '',
            management: '',
            responsible: '',
            dueDate: null
          },
        ],
        detailDelete: []
      }
    ],
    operationsDelete: [],
    documentFile: [],
    attachments: {
      file: [],
      fileDelete: []
    },
    conclusions: null
  };

  addData = {
    operations: []
  };
  // dropIndex: number;
  status_loading = true;

  saveDraftstatus = null;

  outOfPageSave = false;
  linkTopage = null;
  pageChange = null;
  openDoCheckc = false;

  file1: any;
  file2: any;
  file3: any;
  document: any;
  Agreement = null;
  file_name: any;
  file_size: number;

  imgURL: any;
  image1 = [];
  image2 = [];
  image3 = [];
  showImage1 = false;
  message = null;

  // Model
  showModel = false;
  modalmainImageIndex: number;
  modalmainImagedetailIndex: number;
  indexSelect: number;
  modalImage = [];
  modalImagedetail = [];
  fileValue: number;
  // uploadfile
  displayedColumns_file: string[] = ['name', 'size', 'delete'];
  dataSource_file: MatTableDataSource<FileList>;
  urls: any = [];

  // new table
  dropIndex: number;
  add_Process: null;
  staticTitleIndex = [0, 1, 2, 3, 4]; // !important ex [0,1,2] mean set fix row index 0 1 2

  // status page
  stepFiveStatus = true;

  OverSize: any;

  validate = null;
  downloadfile_by = [];

  constructor(
    private router: Router,
    private stepFiveService: StepFiveService,
    private sidebarService: RequestService,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    if (this.template_manage.caaStatus !== 'show') {
      this.openDoCheckc = true;
    }
    this.sidebarService.sendEvent();
    if (localStorage) {
      this.getData(localStorage.getItem('requestId'));
    } else {
      this.status_loading = false;
    }
  }
  viewNext() {
    this.router.navigate(['request/product-detail/step6']);
  }
  changeDate(date: any) {
    const SendI = this.Date.changeDate(date);
    this.changeSaveDraft();
    return SendI;
  }
  sendSaveStatus() {
    if (this.saveDraftstatus === 'success') {
      this.saveStatus.emit(true);
    } else {
      this.saveStatus.emit(false);
    }
  }

  getData(requestId) {
    // this.dataSource_file = new MatTableDataSource(File_data);
    this.stepFiveService.getProduct_detail_step_five(requestId, this.template_manage.pageShow).subscribe(res => {
      if (res['status'] === 'success') {
        this.status_loading = false;
      }
      if (res['data'] !== null) {
        this.stepFive = res['data'];
        this.validate = this.stepFive.validate;
        console.log('validate step5', this.validate);
        // set detailDelete Object stepFive
        this.stepFive.operations.forEach((e, index) => {
          this.stepFive.operations[index].detailDelete = [];
        });
        // set operationsDeleteto Object stepFive
        this.stepFive.operationsDelete = [];
        this.stepFive.attachments.fileDelete = [];
        // set dateFormat
        this.stepFive.operations.forEach((elements, operationsIndex) => {
          elements.detail.forEach((e, detailIndex) => {

            const dateIso = this.stepFive.operations[operationsIndex].detail[detailIndex]['dueDate'];
            // console.log(dateIso)
            if (dateIso === null) {
              this.stepFive.operations[operationsIndex].detail[detailIndex].dueDate = null;
            } else {
              // const dateStamp = { isRange: false, singleDate: { jsDate: new Date(dateIso.toString()) } };
              this.stepFive.operations[operationsIndex].detail[detailIndex].dueDate = dateIso;
            }
          });
        });

        // set uploadfile
        this.dataSource_file = new MatTableDataSource(this.stepFive.attachments.file);
        this.hideDownloadFile();
        // if ((this.validate === null || this.validate === true) && this.template_manage.pageShow !== 'executive') {
        //   this.addRow();
        // }
      } else {
        if ((this.validate === null || this.validate === true) && this.template_manage.pageShow !== 'executive') {
          // this.addRow();
        }
      }
    });
  }

  saveDraft(page?, action?) {
    const returnUpdateData = new Subject<any>();
    this.stepFiveStatus = true;
    this.status_loading = true;
    this.stepFive.requestId = localStorage.getItem('requestId');
    // set dateFormat

    this.stepFive.operations.forEach((elements, operationsIndex) => {
      elements.detail.forEach((e, detailIndex) => {
        if (e.dueDate === null) {
          e.dueDate = null;
        }
        if ((elements.process === '' || elements.process === null)
          && (e.subProcess === '' || e.subProcess === null)
          && (e.management === '' || e.management === null)
          && (e.responsible === '' || e.responsible === null)
          && (e.dueDate === '' || e.dueDate === null)
        ) {
          this.stepFive.operationsDelete.push(this.stepFive.operations[operationsIndex].id);
          this.stepFive.operations.splice(operationsIndex, 1);
        } else {
          if ((elements.process === '' || elements.process === null)
            || (e.subProcess === '' || e.subProcess === null)
            || (e.management === '' || e.management === null)
            || (e.responsible === '' || e.responsible === null)
          ) {
            if (this.stepFiveStatus = true) {
              this.stepFiveStatus = false;
            }
          }
        }
      });
    });
    this.stepFive.status = this.stepFiveStatus;
    console.log('data:', this.stepFive);

    if (this.template_manage.pageShow === 'summary') {
      this.stepFive.conclusions = true;
    } else {
      this.stepFive.conclusions = null;
    }


    this.stepFiveService.updateProduct_detail_step_five(this.stepFive).subscribe(res => {
      if (res['status'] === 'success') {
        this.status_loading = false;
        this.downloadfile_by = [];
        this.saveDraftstatus = 'success';
        returnUpdateData.next(true);
        this.sidebarService.inPageStatus(false);
        setTimeout(() => {
          this.saveDraftstatus = 'fail';
        }, 3000);
        this.stepFive.attachments.file = [];
        this.sidebarService.sendEvent();
        this.sendSaveStatus();
        this.getData(localStorage.getItem('requestId'));
      } else {
        this.status_loading = false;
        alert(res['message']);
        returnUpdateData.next(false);
      }
    }, err => {
      this.status_loading = false;
      console.log(err);
      this.sendSaveStatus();
      alert(err['message']);
      returnUpdateData.next(false);
    });
    return returnUpdateData;
  }


  next_step() {
    this.router.navigate(['/request/product-detail/step6']);
  }

  changeSaveDraft() {
    this.saveDraftstatus = null;
    this.sendSaveStatus();
    this.sidebarService.inPageStatus(true);
  }

  // new table ================================================================================
  checkStaticTitle(index) {
    return this.staticTitleIndex.includes(index);
  }

  drop(event: CdkDragDrop<string[]>) {
    const fixedBody = this.staticTitleIndex;

    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else if (event.previousContainer.data.length > 1) {
      // check if only one left child
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    } else if (fixedBody.includes(this.dropIndex)) {
      // check only 1 child left and static type move only not delete
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);

      const templateSubprocess = {
        subProcess: '',
        management: '',
        responsible: '',
        dueDate: null
      };
      this.stepFive.operations[this.dropIndex].detail.push(templateSubprocess);
    } else {
      // check only 1 child left and not a static type move and delete
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
      if (this.stepFive.operations[this.dropIndex].id) {
        this.stepFive.operationsDelete.push(this.stepFive.operations[this.dropIndex].id);
      }

      // remove current row
      this.removeAddItem(this.dropIndex);
      this.dropIndex = null;
    }
    this.changeSaveDraft();
  }

  dragExited(event: CdkDragExit<string[]>) {
    console.log('Exited', event);
  }

  entered(event: CdkDragEnter<string[]>) {
    console.log('Entered', event);
  }

  startDrag(event: CdkDragStart<string[]>, istep) {
    console.log('startDrag', event);
    console.log('startDrag index', istep);
    this.dropIndex = istep;
  }

  removeAddItem(index) {
    if (index != null) {
      this.stepFive.operations.splice(index, 1);
    }
  }


  addRow() {
    const tempateAddRow = {
      process: '',
      detail: [
        {
          subProcess: '',
          management: '',
          responsible: '',
          dueDate: null
        }
      ],
      detailDelete: []
    };
    this.stepFive.operations.push(tempateAddRow);
  }

  removeDataItem(stepindex, subindex) {
    // console.log('stepindex', stepindex);
    // console.log('subindex', subindex);
    const fixedBody = [0, 1, 2, 3, 4];
    if (fixedBody.includes(stepindex) && this.stepFive.operations[stepindex].detail.length > 1) {
      console.log('1 delete fixed row only');
      this.stepFive.operations[stepindex].detailDelete.push(this.stepFive.operations[stepindex].detail[subindex]['id']);
      this.stepFive.operations[stepindex].detail.splice(subindex, 1);
    } else if (fixedBody.includes(stepindex)) {
      console.log('3 clear fixed row only');
      this.stepFive.operations[stepindex].detailDelete.push(this.stepFive.operations[stepindex].detail[subindex]['id']);
      Object.keys(this.stepFive.operations[stepindex].detail[subindex]).forEach((key, index) => {
        this.stepFive.operations[stepindex].detail[subindex][key] = '';
      });
    } else if (this.stepFive.operations[stepindex].detail.length > 1) {
      console.log(' delete sub row only');
      this.stepFive.operations[stepindex].detailDelete.push(this.stepFive.operations[stepindex].detail[subindex]['id']);
      this.stepFive.operations[stepindex].detail.splice(subindex, 1);
    } else if (this.stepFive.operations[stepindex].detail.length === 1) {
      console.log('delete row permanent', this.stepFive.operations[stepindex].id);
      this.stepFive.operationsDelete.push(this.stepFive.operations[stepindex].id);
      this.stepFive.operations.splice(stepindex, 1);
    } else {
      console.log('not in case');
    }
    this.changeSaveDraft();
  }

  // removeDataItem all non fix
  // removeDataItem(stepindex, subindex) {
  //   console.log('stepindex', stepindex);
  //   console.log('subindex', subindex);
  //   const fixedBody = [0, 1, 2, 3, 4];
  //   if (fixedBody.includes(stepindex) && this.stepFive.operations[stepindex].detail.length > 1) {
  //     console.log('1 delete fixed row only');
  //     // console.log('delete fixed row only', this.stepFive.operations[stepindex].detail[subindex]['id']);
  //     this.stepFive.operations[stepindex].detailDelete.push(this.stepFive.operations[stepindex].detail[subindex]['id']);
  //     this.stepFive.operations[stepindex].detail.splice(subindex, 1);
  //   } else if ((((fixedBody.includes(stepindex)
  //   && this.stepFive.operations[stepindex].detail[0]['subProcess'] === '')
  //   && this.stepFive.operations[stepindex].detail[0]['management'] === '')
  //   && this.stepFive.operations[stepindex].detail[0]['responsible'] === '')
  //   ) {
  //     console.log('2 delete fixed row only');
  //     this.stepFive.operationsDelete.push(this.stepFive.operations[stepindex].id);
  //     this.stepFive.operations.splice(stepindex, 1);
  // } else if (fixedBody.includes(stepindex)) {
  //     console.log('3 clear fixed row only');
  //     Object.keys(this.stepFive.operations[stepindex].detail[subindex]).forEach((key, index) => {
  //       this.stepFive.operations[stepindex].detail[subindex][key] = '';
  //     });
  //     this.stepFive.operations[stepindex].detailDelete.push(this.stepFive.operations[stepindex].detail[subindex]['id']);
  //   } else if (this.stepFive.operations[stepindex].detail.length > 1) {
  //     console.log(' delete sub row only');
  //     this.stepFive.operations[stepindex].detailDelete.push(this.stepFive.operations[stepindex].detail[subindex]['id']);
  //     this.stepFive.operations[stepindex].detail.splice(subindex, 1);
  //   } else if (this.stepFive.operations[stepindex].detail.length === 1) {
  //     console.log('delete row permanent');
  //     this.stepFive.operationsDelete.push(this.stepFive.operations[stepindex].id);
  //     this.stepFive.operations.splice(stepindex, 1);
  //   } else {
  //     console.log('not in case');
  //   }
  // }

  // Uplode File ================================================================
  onSelectFile(event) {

    this.OverSize = [];

    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      const file = event.target.files;
      for (let i = 0; i < filesAmount; i++) {
        console.log('type:', file[i]);
        if (
          // file[i].type === 'image/svg+xml' ||
          file[i].type === 'image/jpeg' ||
          // file[i].type === 'image/raw' ||
          // file[i].type === 'image/svg' ||
          // file[i].type === 'image/tif' ||
          // file[i].type === 'image/gif' ||
          file[i].type === 'image/jpg' ||
          file[i].type === 'image/png' ||
          file[i].type === 'application/doc' ||
          file[i].type === 'application/pdf' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
          file[i].type === 'application/pptx' ||
          file[i].type === 'application/docx' ||
          (file[i].type === 'application/ppt' && file[i].size)
        ) {
          if (file[i].size <= 5000000) {
            this.multiple_file(file[i], i);
          } else {
            this.OverSize.push(file[i].name);
          }
        } else {
          this.status_loading = false;
          alert('File Not Support');
        }
      }
    }

    if (this.OverSize.length !== 0) {
      console.log('open modal');
      document.getElementById('testttt').click();
    }
  }

  multiple_file(file, index) {
    console.log('download');
    const reader = new FileReader();
    if (file) {
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');

        const templateFile = {
          fileName: file.name,
          fileSize: file.size,
          base64File: splitFile[1],
        };

        this.stepFive.attachments.file.push(templateFile);
        this.dataSource_file = new MatTableDataSource(this.stepFive.attachments.file);
        this.hideDownloadFile();
      };
    }
    this.changeSaveDraft();
  }
  delete_mutifile(data: any, index: any) {
    console.log('data:', data, 'index::', index);
    if ('id' in this.stepFive.attachments.file[index]) {
      const id = this.stepFive.attachments.file[index].id;

      console.log('found id', id);
      if (id !== '' || id !== null) {
        this.stepFive.attachments.fileDelete.push(id);
      }
    }
    console.log('delete:', this.stepFive.attachments);
    this.stepFive.attachments.file.splice(index, 1);
    this.dataSource_file = new MatTableDataSource(this.stepFive.attachments.file);
    this.changeSaveDraft();
  }

  hideDownloadFile() {
    if (this.dataSource_file.data.length > 0) {
      this.downloadfile_by = [];

      for (let index = 0; index < this.dataSource_file.data.length; index++) {
        const element = this.dataSource_file.data[index];
        console.log('ele', element);

        if ('id' in element) {
          this.downloadfile_by.push('path');
        } else {
          this.downloadfile_by.push('base64');
        }
      }
    }
    console.log('by:', this.downloadfile_by);
  }



  meageData() {
    const mergeData = {
      process: []
    };
    mergeData.process = [].concat(this.stepFive.operations, this.addData.operations);
    console.log(mergeData);
  }

  dateFormat(date: any) {
    let dateForm = '';
    // if (Object.keys(date).includes('singleDate')) {
    dateForm = moment(date).format('DD/MM') + '/' + Number(Number(moment(date).format('YYYY')) + 543);
    // }
    return dateForm;
  }
  // ===============================================================================

  downloadFile(pathdata: any) {
    this.status_loading = true;
    const contentType = '';
    const sendpath = pathdata;
    console.log('path', sendpath);
    this.sidebarService.getfile(sendpath).subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        this.status_loading = false;
        const datagetfile = res['data'];
        const b64Data = datagetfile['data'];
        const filename = datagetfile['fileName'];
        console.log('base64', b64Data);
        const config = this.b64toBlob(b64Data, contentType);
        saveAs(config, filename);
      }

    });
  }

  b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const convertbyte = atob(b64Data);
    const byteCharacters = atob(convertbyte);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  downloadFile64(data: any) {
    this.status_loading = true;
    const contentType = '';
    this.status_loading = false;
    const b64Data = data.base64File;
    const filename = data.fileName;
    console.log('base64', b64Data);

    const config = this.base64(b64Data, contentType);
    saveAs(config, filename);
  }

  base64(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  autoprocess(index: any, test: any) {
    if (test === 1) {
      const textprocess1 = document.getElementById('textprocess1' + String(index));
      textprocess1.style.overflow = 'hidden';
      textprocess1.style.height = 'auto';
      textprocess1.style.height = textprocess1.scrollHeight + 'px';
    }

    if (test === 2) {
      const textprocess2 = document.getElementById('textprocess2' + String(index));
      textprocess2.style.overflow = 'hidden';
      textprocess2.style.height = 'auto';
      textprocess2.style.height = textprocess2.scrollHeight + 'px';
    }

    if (test === 3) {
      const textprocess3 = document.getElementById('textprocess3' + String(index));
      textprocess3.style.overflow = 'hidden';
      textprocess3.style.height = 'auto';
      textprocess3.style.height = textprocess3.scrollHeight + 'px';
    }
    if (test === 4) {
      const textprocess4 = document.getElementById('textprocess4' + String(index));
      textprocess4.style.overflow = 'hidden';
      textprocess4.style.height = 'auto';
      textprocess4.style.height = textprocess4.scrollHeight + 'px';
    }
  }

  // ngOnDestroy(): void {
  //   const data = this.sidebarService.getsavePage();
  //   let saveInFoStatus = false;
  //   saveInFoStatus = data.saveStatusInfo;
  //   if (saveInFoStatus === true) {
  //     this.outOfPageSave = true;
  //     this.linkTopage = data.goToLink;
  //     console.log('ngOnDestroy', this.linkTopage);
  //     this.saveDraft();
  //     this.sidebarService.resetSaveStatusInfo();
  //   }
  // }

  ngDoCheck() {
    if (this.openDoCheckc === true) {
      if (this.template_manage.caaStatus !== 'show') {
        this.pageChange = this.sidebarService.changePageGoToLink();
        if (this.pageChange) {
          // this.getNavbar();
          const data = this.sidebarService.getsavePage();
          let saveInFoStatus = false;
          saveInFoStatus = data.saveStatusInfo;
          if (saveInFoStatus === true) {
            this.outOfPageSave = true;
            this.linkTopage = data.goToLink;
            this.saveDraft('navbar', null);
            this.sidebarService.resetSaveStatu();
          }
        }
      }
    }
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate request');
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef =  this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        if (data.returnStatus === true) {
          const raturnStatus = this.saveDraft();
          console.log('request raturnStatus', raturnStatus);
            if (raturnStatus) {
              console.log('raturnStatus T', this.saveDraftstatus);
              subject.next(true);
            } else {
              console.log('raturnStatus F', this.saveDraftstatus);
              subject.next(false);
            }
        } else if (data.returnStatus === false) {
          this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
        }
      });
      return subject;
    } else {
      return true;
    }
  }

}
