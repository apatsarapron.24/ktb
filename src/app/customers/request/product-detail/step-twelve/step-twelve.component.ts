import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StepTwelveService } from './../../../../services/request/product-detail/step-twelve/step-twelve.service';
import { RequestService } from './../../../../services/request/request.service';
import { SendWorkService } from './../../../../services/request/product-detail/send-work/send-work.service';
import Swal from 'sweetalert2';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogSaveStatusComponent } from '../../dialog/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from '../../../../services/configGuard/config-guard.service';

@Component({
  selector: 'app-step-twelve',
  templateUrl: './step-twelve.component.html',
  styleUrls: ['./step-twelve.component.scss']
})
export class StepTwelveComponent implements OnInit, CanComponentDeactivate {

  data_twelve = {
    'validate': '',
    'for_comment': '',
    'for_know': '',
    'for_approval': '',
  };
  constructor(private router: Router,
    public stepTwelveService: StepTwelveService,
    private sidebarService: RequestService,
    private sendWorkService: SendWorkService,
    public dialog: MatDialog
  ) { }

  status = false;
  results: any;
  saveDraftstatus: any;
  outOfPageSave = false;
  linkTopage = null;
  pageChange = null;
  openDoCheckc = false;
  api_response: any;
  sendwork_response: any;
  status_loading = true;
  validate = null;


  ngOnInit() {
    window.scrollTo(0, 0);
    this.openDoCheckc = true;
    if (localStorage.getItem('requestId')) {
      this.sidebarService.sendEvent();
    }
    if (localStorage) {
      if (localStorage.getItem('requestId')) {
        this.getDetail(localStorage.getItem('requestId'));
      } else {
        this.status_loading = false;
      }
    } else {
      console.log('Brownser not support');
    }

  }

  getDetail(id) {
    this.stepTwelveService.getDetail(id).subscribe(res => {
      this.results = res;
      if (res['status'] === 'success') {
        this.status_loading = false;
      }
      console.log('getDetail', this.results.data);
      if (this.results.data !== null) {
        this.validate = this.results.data.validate;
        console.log(this.validate);
        this.data_twelve.for_comment = this.results.data.fyi;
        this.data_twelve.for_know = this.results.data.fyr;
        this.data_twelve.for_approval = this.results.data.fya;
      } else {
        this.validate = res['validate'];
      }
    });
  }

  next() {
    this.check();
    console.log(this.status);

    const dataSend = {
      requestId: localStorage.getItem('requestId'),
      status: this.status ? this.status : false,
      fyi: this.data_twelve.for_comment ? this.data_twelve.for_comment : '',
      fyr: this.data_twelve.for_know ? this.data_twelve.for_know : '',
      fya: this.data_twelve.for_approval ? this.data_twelve.for_approval : '',
    };
    console.log('dataSend', dataSend);
    if (localStorage.getItem('level') === 'พนักงาน') {
      this.stepTwelveService.updateDetail(dataSend).subscribe(res => {
        this.api_response = res;
        if (this.api_response.status === 'success') {
          this.status_loading = false;
          this.sidebarService.sendEvent();
          this.sidebarService.inPageStatus(false);
          // this.router.navigate(['/dashboard1']);
          this.sendWorkService.sendWork(localStorage.getItem('requestId')).subscribe(res_sendwork => {
            this.sendwork_response = res_sendwork;
            if (this.sendwork_response.status === 'success') {
              // this.sidebarService.sendEvent();
              Swal.fire({
                title: 'success',
                text: 'ส่งงานเรียบร้อย',
                icon: 'success',
                showConfirmButton: false,
                timer: 3000
              });
              this.router.navigate(['/comment']);
            } else {
              // alert('กรุณากรอกข้อมูลผู้ดำเนินการ');
              const data = this.sendwork_response.message;
              console.log(data);
              // tslint:disable-next-line: max-line-length
              if (data === 'กรุณากรอกข้อมูลผู้ดำเนินการ') {
                this.status_loading = false;
                Swal.fire({
                  title: 'error',
                  text: data,
                  icon: 'error',
                  showConfirmButton: false,
                  timer: 3000,
                }).then( ()=> {
                  this.router.navigate(['/save-manager']);
                })
              } else {
                this.status_loading = false;
                Swal.fire({
                  title: 'error',
                  text: data,
                  icon: 'error',
                  showConfirmButton: false,
                  timer: 3000,
                });
              }

            }
          });
        } else {
          this.status_loading = false;
          alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
        }
      });
    } else {
      this.status_loading = false;
      this.router.navigate(['/comment']);
    }
    // } else {

    // }
    // ==============================================================================
  }

  saveDraft(page?, action?) {
    console.log('saveDraft func');
    this.check();
    console.log(this.status);

    const dataSend = {
      requestId: localStorage.getItem('requestId'),
      status: this.status ? this.status : false,
      fyi: this.data_twelve.for_comment ? this.data_twelve.for_comment : '',
      fyr: this.data_twelve.for_know ? this.data_twelve.for_know : '',
      fya: this.data_twelve.for_approval ? this.data_twelve.for_approval : '',
    };
    console.log('dataSend', dataSend);
    const returnUpdateData = new Subject<any>();
    this.stepTwelveService.updateDetail(dataSend).subscribe(res => {
      this.api_response = res;
      if (this.api_response.status === 'success') {
        this.status_loading = false;
        this.sidebarService.sendEvent();
        this.saveDraftstatus = 'success';
        returnUpdateData.next(true);
        this.sidebarService.inPageStatus(false);
        setTimeout(() => {
          this.saveDraftstatus = 'fail';
        }, 3000);
        this.getDetail(localStorage.getItem('requestId'));
      } else {
        this.status_loading = false;
        alert(res['message']);
        returnUpdateData.next(false);
      }
    });
    return returnUpdateData;
  }

  check() {
    this.status = true;
    // console.log('in check', this.data_twelve.for_comment);
    // if (this.data_twelve.for_comment && this.data_twelve.for_know && this.data_twelve.for_approval !== '') {
    //   this.status = true;
    // } else {
    //   this.status = false;
    // }
  }

  nexttoComment() {
    console.log('nexttoComment')
    this.router.navigate(['/comment']);
  }

  changeSaveDraft() {
    this.saveDraftstatus = null;
    this.sidebarService.inPageStatus(true);
  }

  // ngOnDestroy(): void {
  //   const data = this.sidebarService.getsavePage();
  //   let saveInFoStatus = false;
  //   saveInFoStatus = data.saveStatusInfo;
  //   if (saveInFoStatus === true) {
  //     this.outOfPageSave = true;
  //     this.linkTopage = data.goToLink;
  //     console.log('ngOnDestroy', this.linkTopage);
  //     this.saveDraft();
  //     this.sidebarService.resetSaveStatusInfo();
  //   }
  // }

  // ngDoCheck() {
  //   if (this.openDoCheckc === true) {
  //     this.pageChange = this.sidebarService.changePageGoToLink();
  //     if (this.pageChange) {
  //       // this.getNavbar();
  //       const data = this.sidebarService.getsavePage();
  //       let saveInFoStatus = false;
  //       saveInFoStatus = data.saveStatusInfo;
  //       if (saveInFoStatus === true) {
  //         this.outOfPageSave = true;
  //         this.linkTopage = data.goToLink;
  //         this.saveDraft('navbar', null);
  //         this.sidebarService.resetSaveStatu();
  //       }
  //     }
  //   }
  // }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate request');
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef =  this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        if (data.returnStatus === true) {
          const raturnStatus = this.saveDraft();
          console.log('request raturnStatus', raturnStatus);
            if (raturnStatus) {
              console.log('raturnStatus T', this.saveDraftstatus);
              subject.next(true);
            } else {
              console.log('raturnStatus F', this.saveDraftstatus);
              subject.next(false);
            }
        } else if (data.returnStatus === false) {
          this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
        }
      });
      return subject;
    } else {
      return true;
    }
  }

}
