import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatTableDataSource, MatTable } from '@angular/material/table';
// tslint:disable-next-line: max-line-length
import { StepThreeService } from '../../../../../services/request/product-detail/step-three/step-three.service';
import { Router } from '@angular/router';
import { RequestService } from '../../../../../services/request/request.service';
// import { StepThreeComponent } from '../step-three.component';
import { FormControl, FormGroup } from '@angular/forms';
import { saveAs } from 'file-saver';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogSaveStatusComponent } from '../../../dialog/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from '../../../../../services/configGuard/config-guard.service';

export interface PeriodicElement {
  // position: number;
  bank: string;
  outsource: string;
}

// const ELEMENT_DATA: PeriodicElement[] = [
//   { position: 1, bank: '-', thirdParty: '-' },
//   { position: 2, bank: '-', thirdParty: '-' },
//   { position: 3, bank: '-', thirdParty: '-' },
//   { position: 4, bank: '-', thirdParty: '-' },
//   { position: 5, bank: '-', thirdParty: '-' },
//   { position: 6, bank: '-', thirdParty: '-' },
//   { position: 7, bank: '-', thirdParty: '-' },
//   { position: 8, bank: '-', thirdParty: '-' },
//   { position: 9, bank: '-', thirdParty: '-' },
//   { position: 10, bank: '-', thirdParty: '-' },
// ];

@Component({
  selector: 'app-non-credit',
  templateUrl: './non-credit.component.html',
  styleUrls: ['./non-credit.component.scss'],
})
export class NonCreditComponent implements OnInit, CanComponentDeactivate {
  @Input() template_manage = {
    role: null,
    validate: null,
    caaStatus: null,
    pageShow: null,
  };
  @Output() saveStatus = new EventEmitter<boolean>();
  displayedColumns_file: string[] = ['name', 'size', 'delete'];
  dataSource_file: MatTableDataSource<FileList>;
  urls: any = [];
  saveDraftstatus = null;
  outOfPageSave = false;
  pageChange = null;
  openDoCheckc = false;
  linkTopage = null;
  presentation_type = null;
  status_loading = true;

  // ระบบงานหลักที่เกี่ยวข้อง
  displayedColumns: string[] = ['bank', 'outsource', 'delete'];
  dataSource: MatTableDataSource<PeriodicElement>;
  datatable_status = true

  serviceDisabled = true;

  // status =============
  status = false;
  statusTargetGroup = false;
  statusPresentationType = false;

  stepThreeNonCerdit = {
    validate: null,
    requestId: null,
    status: true,
    productType: {
      creditCard: true,
      electronicCard: false,
      electronicCash: false,
      deposit: false,
      insurance: false,
      fund: false,
      other: false,
      otherDetail: null,
    },
    productOrther: null,
    userTarget: {
      normalPerson: {
        thai: true,
        foreigner: false,
      },
      corporation: {
        thai: true,
        foreigner: false,
      },
    },
    target: {
      file: [],
      fileDelete: [],
    },
    targetText: 'tttttttttttttttttttt',
    property: {
      file: [],
      fileDelete: [],
    },
    propertyText: 'ppppppppppppppppppp',
    scope: {
      file: [],
      fileDelete: [],
    },
    scopeText: 'ssssssssssssssssssss',
    condition: {
      file: [],
      fileDelete: [],
    },
    conditionText: 'cccccccccccccccccccccccc',
    duty: {
      file: [],
      fileDelete: [],
    },
    dutyText: 'ddddddddddddddddddd',
    fee: {
      file: [],
      fileDelete: [],
    },
    feeText: 'ffffffffffffffffffffffff',
    other: {
      file: [],
      fileDelete: [],
    },
    otherText: 'oooooooooooooooooooooo',
    attachments: {
      file: [],
      fileDelete: [],
    },
    mainWork: null,
    conclusions: null
  };

  serviceChannels = {
    generalBranch: false,
    electronics: false,
    businessOffice: false,
    CBC: false,
    blockChain: false,
    callCenter: false,
    ATM: false,
    digitalChannels: false,
    detailDigitalChannels: null,
    nonBank: false,
    detailNonBank: null,
    etc: false,
    detailEtc: null,
  };

  targetGroup = {
    individual: false,
    individual_thai: false,
    individual_foreigner: false,
    legalEntity: false,
    legalEntity_thai: false,
    legalEntity_foreigner: false,
  };

  // upload image
  file1_1: any;
  file1_2: any;
  file2_1: any;
  file2_2: any;
  file3_1: any;
  file3_2: any;
  file4_1: any;
  file4_2: any;
  file5_1: any;
  file5_2: any;
  file6_1: any;
  file6_2: any;
  file7_1: any;
  file7_2: any;
  image1 = [];
  image2 = [];
  image3 = [];
  image4 = [];
  image5 = [];
  image6 = [];
  image7 = [];
  message = null;
  document: any;
  attachments: any;

  // Model
  showModel = false;
  modalmainImageIndex: number;
  modalmainImagedetailIndex: number;
  indexSelect: number;
  modalImage = [];
  modalImagedetail = [];
  fileValue: number;
  stepPage: any;
  checkLocal: any;
  pageLast: any;

  // textareaForm = new FormGroup({
  //   targetText: new FormControl(this.stepThreeNonCerdit.targetText),
  //   propertyText: new FormControl(this.stepThreeNonCerdit.propertyText),
  //   scopeText: new FormControl(this.stepThreeNonCerdit.scopeText),
  //   conditionText: new FormControl(this.stepThreeNonCerdit.conditionText),
  //   dutyText: new FormControl(this.stepThreeNonCerdit.dutyText),
  //   feeText: new FormControl(this.stepThreeNonCerdit.feeText),
  //   otherText: new FormControl(this.stepThreeNonCerdit.otherText),
  // });

  FileOverSize: any;
  status_productorther: boolean;
  repeat = false;
  downloadfile_by = [];

  sendSaveStatus() {
    if (this.saveDraftstatus === 'success') {
      this.saveStatus.emit(true);
    } else {
      this.saveStatus.emit(false);
    }
  }
  autogrow(number: any) {
    if (number === 1) {
      const targetText = document.getElementById('targetText');
      targetText.style.overflow = 'hidden';
      targetText.style.height = 'auto';
      targetText.style.height = targetText.scrollHeight + 'px';
    }

    if (number === 2) {
      const propertyText = document.getElementById('propertyText');
      propertyText.style.overflow = 'hidden';
      propertyText.style.height = 'auto';
      propertyText.style.height = propertyText.scrollHeight + 'px';
    }
    if (number === 3) {
      const scopeText = document.getElementById('scopeText');
      scopeText.style.overflow = 'hidden';
      scopeText.style.height = 'auto';
      scopeText.style.height = scopeText.scrollHeight + 'px';
    }
    if (number === 4) {
      const conditionText = document.getElementById('conditionText');
      conditionText.style.overflow = 'hidden';
      conditionText.style.height = 'auto';
      conditionText.style.height = conditionText.scrollHeight + 'px';
    }

    if (number === 5) {
      const dutyText = document.getElementById('dutyText');
      dutyText.style.overflow = 'hidden';
      dutyText.style.height = 'auto';
      dutyText.style.height = dutyText.scrollHeight + 'px';
    }

    if (number === 6) {
      const feeText = document.getElementById('feeText');
      feeText.style.overflow = 'hidden';
      feeText.style.height = 'auto';
      feeText.style.height = feeText.scrollHeight + 'px';
    }
    if (number === 7) {
      const otherText = document.getElementById('otherText');
      otherText.style.overflow = 'hidden';
      otherText.style.height = 'auto';
      otherText.style.height = otherText.scrollHeight + 'px';
    }
  }

  addrow() {
    const _databank = {
      bank: null,
      outsource: null,
    };
    this.dataSource.data.push(_databank);
    this.dataSource = new MatTableDataSource(this.dataSource.data);
  }

  removedata(indexdata) {
    // console.log(indexdata);
    this.dataSource = new MatTableDataSource(this.dataSource.data);
    this.dataSource.data.splice(indexdata, 1);
  }
  saveDraft(page?, action?) {
    let statusAlert = false;
    this.status_loading = true;
    // this.stepThreeNonCerdit.targetText = document.getElementById('targetText').innerHTML;
    // this.stepThreeNonCerdit.propertyText = document.getElementById('propertyText').innerHTML;
    // this.stepThreeNonCerdit.scopeText = document.getElementById('scopeText').innerHTML;
    // this.stepThreeNonCerdit.conditionText = document.getElementById('conditionText').innerHTML;
    // this.stepThreeNonCerdit.dutyText = document.getElementById('dutyText').innerHTML;
    // this.stepThreeNonCerdit.feeText = document.getElementById('feeText').innerHTML;
    // this.stepThreeNonCerdit.otherText = document.getElementById('otherText').innerHTML;

    console.log('firstStepData requestId:', localStorage.getItem('requestId'));
    for (let index = 0; index < this.dataSource.data.length; index++) {
      const element = this.dataSource.data[index];
      if (element.bank == null && element.outsource == null) {
        console.log('index>>>>>', index);
        this.dataSource.data.splice(index);
        // this.stepThreeNonCerdit.mainWork = this.dataSource.data;
      }
    }
    this.stepThreeNonCerdit.mainWork = this.dataSource.data;

    if (this.presentation_type) {
      this.stepThreeNonCerdit.productType.creditCard = false;
      this.stepThreeNonCerdit.productType.electronicCard = false;
      this.stepThreeNonCerdit.productType.electronicCash = false;
      this.stepThreeNonCerdit.productType.deposit = false;
      this.stepThreeNonCerdit.productType.insurance = false;
      this.stepThreeNonCerdit.productType.fund = false;
      this.stepThreeNonCerdit.productType.other = false;
      if (this.presentation_type === 'บัตรเครดิต') {
        this.stepThreeNonCerdit.productType.creditCard = true;
        this.statusPresentationType = true;
      } else if (this.presentation_type === 'บัตรอิเล็กทรอนิกส์') {
        this.stepThreeNonCerdit.productType.electronicCard = true;
        this.statusPresentationType = true;
      } else if (this.presentation_type === 'เงินอิเล็กทรอนิกส์') {
        this.stepThreeNonCerdit.productType.electronicCash = true;
        this.statusPresentationType = true;
      } else if (this.presentation_type === 'เงินฝาก') {
        this.stepThreeNonCerdit.productType.deposit = true;
        this.statusPresentationType = true;
      } else if (this.presentation_type === 'ประกันภัย') {
        this.stepThreeNonCerdit.productType.insurance = true;
        this.statusPresentationType = true;
      } else if (this.presentation_type === 'กองทุน') {
        this.stepThreeNonCerdit.productType.fund = true;
        this.statusPresentationType = true;
      } else if (this.presentation_type === 'อื่นๆ') {
        this.stepThreeNonCerdit.productType.other = true;
        if (this.stepThreeNonCerdit.productType.otherDetail) {
          this.statusPresentationType = true;
        } else {
          statusAlert = true;
          this.status_loading = false;
          alert('Presentation type: Please specify more details.');
        }
      }
    }

    if (this.targetGroup.individual) {
      if (
        this.targetGroup.individual_thai ||
        this.targetGroup.individual_foreigner
      ) {
        this.stepThreeNonCerdit.userTarget.corporation.thai = this.targetGroup.individual_thai;
        this.stepThreeNonCerdit.userTarget.corporation.foreigner = this.targetGroup.individual_foreigner;
        this.statusTargetGroup = true;
      } else {
        this.statusTargetGroup = false;
        statusAlert = true;
        this.status_loading = false;
        alert('Target group/User -> Individual: Please select a topic. ');
      }
    } else {
      this.stepThreeNonCerdit.userTarget.corporation.thai = false;
      this.stepThreeNonCerdit.userTarget.corporation.foreigner = false;
    }

    if (this.targetGroup.legalEntity) {
      if (
        this.targetGroup.legalEntity_thai ||
        this.targetGroup.legalEntity_foreigner
      ) {
        this.stepThreeNonCerdit.userTarget.normalPerson.thai = this.targetGroup.legalEntity_thai;
        this.stepThreeNonCerdit.userTarget.normalPerson.foreigner = this.targetGroup.legalEntity_foreigner;
        this.statusTargetGroup = true;
      } else {
        this.statusTargetGroup = false;
        statusAlert = true;
        this.status_loading = false;
        alert('Target group/User -> Individual: Please select a topic. ');
      }
    } else {
      this.stepThreeNonCerdit.userTarget.normalPerson.thai = false;
      this.stepThreeNonCerdit.userTarget.normalPerson.foreigner = false;
    }

    if (this.serviceChannels.detailDigitalChannels !== null) {
      if (this.stepThreeNonCerdit.productOrther === null || this.stepThreeNonCerdit.productOrther === '') {
        this.status_productorther = false;
      } else {
        this.status_productorther = true;
      }
    } else {
      this.status_productorther = true;
    }
    this.datatable_status = true;
    if(this.dataSource.data !== null && this.dataSource.data.length > 0 ) {
      for (let index = 0; index < this.dataSource.data.length; index++) {
       if(this.dataSource.data[index].bank == '' || this.dataSource.data[index].bank == null) {
        this.datatable_status = false;
       }
       if(this.dataSource.data[index].outsource == '' || this.dataSource.data[index].outsource == null) {
        this.datatable_status = false;
       }
      }
    }
    else if(this.dataSource.data == null ||this.dataSource.data.length == 0 ) {
      this.datatable_status = false
    }
    console.log(' this.datatable_status:',this.datatable_status )
    if (
      this.statusPresentationType === true &&
      this.statusTargetGroup === true &&
      this.status_productorther === true &&
      this.stepThreeNonCerdit.targetText &&
      this.stepThreeNonCerdit.propertyText &&
      this.stepThreeNonCerdit.scopeText &&
      this.stepThreeNonCerdit.conditionText &&
      this.stepThreeNonCerdit.dutyText &&
      this.stepThreeNonCerdit.feeText &&
      this.dataSource.data !== null &&
      this.datatable_status
      // this.stepThreeNonCerdit.otherText
    ) {
      this.status = true;
      this.stepThreeNonCerdit.status = true;
    } else { this.stepThreeNonCerdit.status = false; }

    console.log('status:', this.stepThreeNonCerdit.status);


    this.stepThreeNonCerdit.requestId = localStorage.getItem('requestId');
    console.log('data to save non-credit', this.stepThreeNonCerdit);

    if (this.template_manage.pageShow === 'summary') {
      this.stepThreeNonCerdit.conclusions = true;
    } else {
      this.stepThreeNonCerdit.conclusions = null;
    }

    if (statusAlert === false) {
      const returnUpdateData = new Subject<any>();
      this.StepThreeOtherService.update_step_three_non_credit(
        this.stepThreeNonCerdit
      ).subscribe(
        (res) => {
          console.log('send res', res);
          if (res['status'] === 'success') {
            this.saveDraftstatus = 'success';
            this.status_loading = false;
            this.downloadfile_by = [];
            returnUpdateData.next(true);
            this.sidebarService.inPageStatus(false);
            setTimeout(() => {
              this.saveDraftstatus = 'fail';
            }, 3000);
            if (page === 'summary') {
              if (action === 'next' || action === 'back') {
                this.StepThreeOtherService.CheckNextPageSummary(localStorage.getItem('check'), action);
              }
            } else if (page === 'request') {
              if (action === 'next' || action === 'back') {
                this.StepThreeOtherService.CheckNextPage(localStorage.getItem('check'), action);
              }
            } else if (page === 'navbar') {
              this.router.navigate([this.linkTopage]);
            } else if (page === 'navbarSummary') {
              this.router.navigate([action]);
            }
            this.stepThreeNonCerdit.attachments.file = [];
            this.stepThreeNonCerdit.attachments.fileDelete = [];
            this.sendSaveStatus();
            this.sidebarService.sendEvent();
          } else {
            this.status_loading = false;
            alert(res['message']);
            returnUpdateData.next(false);
          }
          this.getDataProductDetailstepThreeNonCerdit(
            localStorage.getItem('requestId')
          );
        },
        (err) => {
          console.log('err', err);
          this.sendSaveStatus();
          this.status_loading = false;
          alert(err['message']);
          returnUpdateData.next(false);
        }
      );
      return returnUpdateData;
    } else {
      return false;
    }
  }

  next_step(action) {
    this.StepThreeOtherService.CheckNextPage(localStorage.getItem('check'), action);
  }

  viewNext() {
    this.StepThreeOtherService.CheckNextPage(localStorage.getItem('check'), 'next');
  }

  changeSaveDraft() {
    this.saveDraftstatus = null;
    this.sendSaveStatus();
    this.sidebarService.inPageStatus(true);
  }

  clearServiceDeliveryDetailDigitalChannels() {
    this.serviceChannels.digitalChannels = !this.serviceChannels
      .digitalChannels;
    this.serviceChannels.detailDigitalChannels = null;
  }
  clearServiceDeliveryDetailNonBank() {
    this.serviceChannels.nonBank = !this.serviceChannels.nonBank;
    this.serviceChannels.detailNonBank = null;
  }
  clearServiceDeliveryDetailEtc() {
    this.serviceChannels.etc = !this.serviceChannels.etc;
    this.serviceChannels.detailEtc = null;
  }

  clearTargetGroupIndividual() {
    if (this.targetGroup.individual === false) {
      this.targetGroup.individual_thai = false;
      this.targetGroup.individual_foreigner = false;
    }
    this.changeSaveDraft();
  }
  clearTargetGroupLegalEntity() {
    if (this.targetGroup.legalEntity === false) {
      this.targetGroup.legalEntity_thai = false;
      this.targetGroup.legalEntity_foreigner = false;
    }
    this.changeSaveDraft();
  }

  changeTargetGroupIndividual(id: any) {
    if (this.targetGroup.individual === true) {
      if (id === 1) {
        this.targetGroup.individual_thai = !this.targetGroup.individual_thai;
      } else if (id === 2) {
        this.targetGroup.individual_foreigner = !this.targetGroup
          .individual_foreigner;
      }
      this.changeSaveDraft();
    }
  }

  changeTargetGroupLegalEntity(id: any) {
    if (this.targetGroup.legalEntity === true) {
      if (id === 1) {
        this.targetGroup.legalEntity_thai = !this.targetGroup.legalEntity_thai;
      } else if (id === 2) {
        this.targetGroup.legalEntity_foreigner = !this.targetGroup
          .legalEntity_foreigner;
      }
      this.changeSaveDraft();
    }
  }

  // getFileDetailsImage ================================================================
  getFileDetails(fileIndex, event) {
    const files = event.target.files;
    for (let i = 0; i < files.length; i++) {
      const mimeType = files[i].type;
      if (mimeType.match('image/jpeg|image/png') == null) {
        this.message = 'Only images are supported.';
        this.status_loading = false;
        alert(this.message);
        return;
      }
      const file = files[i];

      console.log('file:', file);
      const reader = new FileReader();
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');

        const templateFile = {
          fileName: file.name,
          base64File: splitFile[1],
        };
        console.log('templateFile', templateFile);

        if (fileIndex === 1) {
          console.log('00000000000000000000000000000001');
          this.stepThreeNonCerdit.target.file.push(templateFile);

          for (
            let index = 0;
            index < this.stepThreeNonCerdit.target.file.length;
            index++
          ) {
            this.preview(
              fileIndex,
              this.stepThreeNonCerdit.target.file[index],
              index
            );
          }
          this.file1_1 = undefined;
          this.file1_2 = undefined;
        } else if (fileIndex === 2) {
          console.log('00000000000000000000000000000002');
          this.stepThreeNonCerdit.property.file.push(templateFile);

          for (
            let index = 0;
            index < this.stepThreeNonCerdit.property.file.length;
            index++
          ) {
            this.preview(
              fileIndex,
              this.stepThreeNonCerdit.property.file[index],
              index
            );
            this.file2_1 = undefined;
            this.file2_2 = undefined;
          }
        } else if (fileIndex === 3) {
          console.log('00000000000000000000000000000003');
          this.stepThreeNonCerdit.scope.file.push(templateFile);

          for (
            let index = 0;
            index < this.stepThreeNonCerdit.scope.file.length;
            index++
          ) {
            this.preview(
              fileIndex,
              this.stepThreeNonCerdit.scope.file[index],
              index
            );
          }
          this.file3_1 = undefined;
          this.file3_2 = undefined;
        } else if (fileIndex === 4) {
          console.log('00000000000000000000000000000004');
          this.stepThreeNonCerdit.condition.file.push(templateFile);

          for (
            let index = 0;
            index < this.stepThreeNonCerdit.condition.file.length;
            index++
          ) {
            this.preview(
              fileIndex,
              this.stepThreeNonCerdit.condition.file[index],
              index
            );
          }
          this.file4_1 = undefined;
          this.file4_2 = undefined;
        } else if (fileIndex === 5) {
          console.log('00000000000000000000000000000005');
          this.stepThreeNonCerdit.duty.file.push(templateFile);

          for (
            let index = 0;
            index < this.stepThreeNonCerdit.duty.file.length;
            index++
          ) {
            this.preview(
              fileIndex,
              this.stepThreeNonCerdit.duty.file[index],
              index
            );
          }
          this.file5_1 = undefined;
          this.file5_2 = undefined;
        } else if (fileIndex === 6) {
          console.log('00000000000000000000000000000006');
          this.stepThreeNonCerdit.fee.file.push(templateFile);

          for (
            let index = 0;
            index < this.stepThreeNonCerdit.fee.file.length;
            index++
          ) {
            this.preview(
              fileIndex,
              this.stepThreeNonCerdit.fee.file[index],
              index
            );
          }
          this.file6_1 = undefined;
          this.file6_2 = undefined;
        } else if (fileIndex === 7) {
          console.log('00000000000000000000000000000007');
          this.stepThreeNonCerdit.other.file.push(templateFile);

          for (
            let index = 0;
            index < this.stepThreeNonCerdit.other.file.length;
            index++
          ) {
            this.preview(
              fileIndex,
              this.stepThreeNonCerdit.other.file[index],
              index
            );
          }
          this.file7_1 = undefined;
          this.file7_2 = undefined;
        }
      };
      reader.readAsDataURL(file);
    }
    this.changeSaveDraft();
  }

  preview(fileIndex, file, index) {
    const name = file.fileName;
    const typeFiles = name.match(/\.\w+$/g);
    const typeFile = typeFiles[0].replace('.', '');

    const tempPicture = `data:image/${typeFile};base64,${file.base64File}`;

    if (fileIndex === 1) {
      if (file) {
        this.image1[index] = tempPicture;
      }
    } else if (fileIndex === 2) {
      if (file) {
        this.image2[index] = tempPicture;
      }
    } else if (fileIndex === 3) {
      if (file) {
        this.image3[index] = tempPicture;
      }
    } else if (fileIndex === 4) {
      if (file) {
        this.image4[index] = tempPicture;
      }
    } else if (fileIndex === 5) {
      if (file) {
        this.image5[index] = tempPicture;
      }
    } else if (fileIndex === 6) {
      if (file) {
        this.image6[index] = tempPicture;
      }
    } else if (fileIndex === 7) {
      if (file) {
        this.image7[index] = tempPicture;
      }
    }
  }
  // Delete image =====================================
  delete_image(fileIndex, index) {
    console.log('fileIndex:', fileIndex);
    console.log('index:', index);
    if (fileIndex === 1) {
      if ('id' in this.stepThreeNonCerdit.target.file[index]) {
        const id = this.stepThreeNonCerdit.target.file[index].id;
        console.log('found id');
        if ('fileDelete' in this.stepThreeNonCerdit.target) {
          this.stepThreeNonCerdit.target.fileDelete.push(id);
        }
      }
      this.stepThreeNonCerdit.target.file.splice(index, 1);
      this.image1.splice(index, 1);

      for (let i = 0; i < this.stepThreeNonCerdit.target.file.length; i++) {
        this.preview(fileIndex, this.stepThreeNonCerdit.target.file[i], i);
      }
    } else if (fileIndex === 2) {
      if ('id' in this.stepThreeNonCerdit.property.file[index]) {
        const id = this.stepThreeNonCerdit.property.file[index].id;
        console.log('found id');
        if ('fileDelete' in this.stepThreeNonCerdit.property) {
          this.stepThreeNonCerdit.property.fileDelete.push(id);
        }
      }
      this.stepThreeNonCerdit.property.file.splice(index, 1);
      this.image2.splice(index, 1);

      for (let i = 0; i < this.stepThreeNonCerdit.property.file.length; i++) {
        this.preview(fileIndex, this.stepThreeNonCerdit.property.file[i], i);
      }
    } else if (fileIndex === 3) {
      if ('id' in this.stepThreeNonCerdit.scope.file[index]) {
        const id = this.stepThreeNonCerdit.scope.file[index].id;
        console.log('found id');
        if ('fileDelete' in this.stepThreeNonCerdit.scope) {
          this.stepThreeNonCerdit.scope.fileDelete.push(id);
        }
      }
      this.stepThreeNonCerdit.scope.file.splice(index, 1);
      this.image3.splice(index, 1);

      for (let i = 0; i < this.stepThreeNonCerdit.scope.file.length; i++) {
        this.preview(fileIndex, this.stepThreeNonCerdit.scope.file[i], i);
      }
    } else if (fileIndex === 4) {
      if ('id' in this.stepThreeNonCerdit.condition.file[index]) {
        const id = this.stepThreeNonCerdit.condition.file[index].id;
        console.log('found id');
        if ('fileDelete' in this.stepThreeNonCerdit.condition) {
          this.stepThreeNonCerdit.condition.fileDelete.push(id);
        }
      }
      this.stepThreeNonCerdit.condition.file.splice(index, 1);
      this.image4.splice(index, 1);

      for (let i = 0; i < this.stepThreeNonCerdit.condition.file.length; i++) {
        this.preview(fileIndex, this.stepThreeNonCerdit.condition.file[i], i);
      }
    } else if (fileIndex === 5) {
      if ('id' in this.stepThreeNonCerdit.duty.file[index]) {
        const id = this.stepThreeNonCerdit.duty.file[index].id;
        console.log('found id');
        if ('fileDelete' in this.stepThreeNonCerdit.duty) {
          this.stepThreeNonCerdit.duty.fileDelete.push(id);
        }
      }
      this.stepThreeNonCerdit.duty.file.splice(index, 1);
      this.image5.splice(index, 1);

      for (let i = 0; i < this.stepThreeNonCerdit.duty.file.length; i++) {
        this.preview(fileIndex, this.stepThreeNonCerdit.duty.file[i], i);
      }
    } else if (fileIndex === 6) {
      if ('id' in this.stepThreeNonCerdit.fee.file[index]) {
        const id = this.stepThreeNonCerdit.fee.file[index].id;
        console.log('found id');
        if ('fileDelete' in this.stepThreeNonCerdit.fee) {
          this.stepThreeNonCerdit.fee.fileDelete.push(id);
        }
      }
      this.stepThreeNonCerdit.fee.file.splice(index, 1);
      this.image6.splice(index, 1);

      for (let i = 0; i < this.stepThreeNonCerdit.fee.file.length; i++) {
        this.preview(fileIndex, this.stepThreeNonCerdit.fee.file[i], i);
      }
    } else if (fileIndex === 7) {
      if ('id' in this.stepThreeNonCerdit.other.file[index]) {
        const id = this.stepThreeNonCerdit.other.file[index].id;
        console.log('found id');
        if ('fileDelete' in this.stepThreeNonCerdit.other) {
          this.stepThreeNonCerdit.other.fileDelete.push(id);
        }
      }
      this.stepThreeNonCerdit.other.file.splice(index, 1);
      this.image7.splice(index, 1);

      for (let i = 0; i < this.stepThreeNonCerdit.other.file.length; i++) {
        this.preview(fileIndex, this.stepThreeNonCerdit.other.file[i], i);
      }
    }
    this.changeSaveDraft();
  }
  // ========================================================================================

  // ============================== Modal ==========================================
  image_click(colId, id) {
    this.showModel = true;
    console.log('colId:', colId);
    console.log('id:', id);
    // this.showCircle_colId = colId;
    // this.showCircle_id = id;
    document.getElementById('myModal3_4').style.display = 'block';
    this.imageModal(colId, id);
  }

  closeModal() {
    this.showModel = false;
    document.getElementById('myModal3_4').style.display = 'none';
  }

  imageModal(fileIndex, index) {
    if (fileIndex === 1) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image1;
      this.modalImagedetail = this.stepThreeNonCerdit.target.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 2) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image2;
      this.modalImagedetail = this.stepThreeNonCerdit.property.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 3) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image3;
      this.modalImagedetail = this.stepThreeNonCerdit.scope.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 4) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image4;
      this.modalImagedetail = this.stepThreeNonCerdit.condition.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 5) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image5;
      this.modalImagedetail = this.stepThreeNonCerdit.duty.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 6) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image6;
      this.modalImagedetail = this.stepThreeNonCerdit.fee.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 7) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image7;
      this.modalImagedetail = this.stepThreeNonCerdit.other.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    }
  }
  // Upload  attachments =====================
  onSelectFile(event) {
    this.FileOverSize = [];
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      const file = event.target.files;
      for (let i = 0; i < filesAmount; i++) {
        console.log('type:', file[i]);
        if (
          file[i].type === 'image/svg+xml' ||
          file[i].type === 'image/jpeg' ||
          // file[i].type === 'image/raw' ||
          // file[i].type === 'image/svg' ||
          // file[i].type === 'image/tif' ||
          // file[i].type === 'image/gif' ||
          file[i].type === 'image/jpg' ||
          file[i].type === 'image/png' ||
          file[i].type === 'application/doc' ||
          file[i].type === 'application/pdf' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
          file[i].type === 'application/pptx' ||
          file[i].type === 'application/docx' ||
          (file[i].type === 'application/ppt' && file[i].size)
        ) {
          if (file[i].size <= 5000000) {
            this.multiple_file(file[i], i);
          } else {
            this.FileOverSize.push(file[i].name);
          }
        } else {
          this.status_loading = false;
          alert('File Not Support');
        }
      }
      if (this.FileOverSize.length !== 0) {
        console.log('open modal');
        document.getElementById('testttt').click();
      }
    }
    this.attachments = undefined;
    this.changeSaveDraft();
  }

  multiple_file(file, index) {
    // console.log('download');
    const reader = new FileReader();
    if (file) {
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');

        const templateFile = {
          fileName: file.name,
          fileSize: file.size,
          base64File: splitFile[1],
        };
        this.stepThreeNonCerdit.attachments.file.push(templateFile);
        this.dataSource_file = new MatTableDataSource(
          this.stepThreeNonCerdit.attachments.file
        );
        this.hideDownloadFile();
      };
    }
  }

  delete_mutifile(data: any, index: any) {
    console.log('data:', data, 'index::', index);
    if ('id' in this.stepThreeNonCerdit.attachments.file[index]) {
      const id = this.stepThreeNonCerdit.attachments.file[index].id;
      console.log('found id');
      if ('fileDelete' in this.stepThreeNonCerdit.attachments) {
        this.stepThreeNonCerdit.attachments.fileDelete.push(id);
      }
    }
    // console.log('fileDelete attachments : ', this.stepThreeNonCerdit.attachments.fileDelete);
    this.stepThreeNonCerdit.attachments.file.splice(index, 1);
    this.dataSource_file = new MatTableDataSource(
      this.stepThreeNonCerdit.attachments.file
    );
    this.changeSaveDraft();
  }
  // ============================= APi ============================================
  getDataProductDetailstepThreeNonCerdit(documentID) {
    this.StepThreeOtherService.get_step_three_non_credit(documentID, this.template_manage.pageShow).subscribe(
      (res) => {
        console.log('res', res);
        this.stepThreeNonCerdit = res['data'];
        if (res['status'] === 'success') {
          this.status_loading = false;
          this.stepThreeNonCerdit['parentRequestId'] = res['parentRequestId'];
        }

        console.log('555', this.stepThreeNonCerdit.productType);
        if (this.stepThreeNonCerdit.productType !== null) {
          // set  presentation_type
          if (this.stepThreeNonCerdit.productType.creditCard) {
            this.presentation_type = 'บัตรเครดิต';
          } else if (this.stepThreeNonCerdit.productType.electronicCard) {
            this.presentation_type = 'บัตรอิเล็กทรอนิกส์';
          } else if (this.stepThreeNonCerdit.productType.electronicCash) {
            this.presentation_type = 'เงินอิเล็กทรอนิกส์';
          } else if (this.stepThreeNonCerdit.productType.deposit) {
            this.presentation_type = 'เงินฝาก';
          } else if (this.stepThreeNonCerdit.productType.insurance) {
            this.presentation_type = 'ประกันภัย';
          } else if (this.stepThreeNonCerdit.productType.fund) {
            this.presentation_type = 'กองทุน';
          } else if (this.stepThreeNonCerdit.productType.other) {
            this.presentation_type = 'อื่นๆ';
            this.stepThreeNonCerdit.productType.otherDetail = this.stepThreeNonCerdit.productType.otherDetail;
          } else {
            this.presentation_type = null;
          }
        } else {
          this.presentation_type = null;
          this.stepThreeNonCerdit.productType = {
            creditCard: true,
            electronicCard: false,
            electronicCash: false,
            deposit: false,
            insurance: false,
            fund: false,
            other: false,
            otherDetail: null,
          };
        }

        if (this.stepThreeNonCerdit.userTarget !== null) {
          // set targetGroup

          if (
            this.stepThreeNonCerdit.userTarget.corporation.thai ||
            this.stepThreeNonCerdit.userTarget.corporation.foreigner
          ) {
            this.targetGroup.legalEntity = true;
            this.targetGroup.legalEntity_thai = this.stepThreeNonCerdit.userTarget.corporation.thai;
            this.targetGroup.legalEntity_foreigner = this.stepThreeNonCerdit.userTarget.corporation.foreigner;
          } else {
            this.targetGroup.legalEntity = false;
            this.targetGroup.legalEntity_thai = false;
            this.targetGroup.legalEntity_foreigner = false;
          }
          if (
            this.stepThreeNonCerdit.userTarget.normalPerson.thai ||
            this.stepThreeNonCerdit.userTarget.normalPerson.foreigner
          ) {
            this.targetGroup.individual = true;
            this.targetGroup.individual_thai = this.stepThreeNonCerdit.userTarget.normalPerson.thai;
            this.targetGroup.individual_foreigner = this.stepThreeNonCerdit.userTarget.normalPerson.foreigner;
          } else {
            this.targetGroup.individual = false;
            this.targetGroup.individual_thai = false;
            this.targetGroup.individual_foreigner = false;
          }
        } else {
          this.targetGroup.individual = false;
          this.targetGroup.individual_thai = false;
          this.targetGroup.individual_foreigner = false;
          this.targetGroup.legalEntity = false;
          this.targetGroup.legalEntity_thai = false;
          this.targetGroup.legalEntity_foreigner = false;
          this.stepThreeNonCerdit.userTarget = {
            normalPerson: {
              thai: false,
              foreigner: false,
            },
            corporation: {
              thai: false,
              foreigner: false,
            },
          };
        }

        this.dataSource.data = this.stepThreeNonCerdit['mainWork'];
        if (this.dataSource.data === null) {
          this.dataSource.data = [];
        }
        // set service
        this.serviceChannels.generalBranch = this.stepThreeNonCerdit[
          'service'
        ][0].flag;
        this.serviceChannels.electronics = this.stepThreeNonCerdit[
          'service'
        ][1].flag;
        this.serviceChannels.businessOffice = this.stepThreeNonCerdit[
          'service'
        ][2].flag;
        this.serviceChannels.CBC = this.stepThreeNonCerdit['service'][3].flag;
        this.serviceChannels.blockChain = this.stepThreeNonCerdit[
          'service'
        ][4].flag;
        this.serviceChannels.callCenter = this.stepThreeNonCerdit[
          'service'
        ][5].flag;
        this.serviceChannels.ATM = this.stepThreeNonCerdit['service'][6].flag;
        this.serviceChannels.digitalChannels = this.stepThreeNonCerdit[
          'service'
        ][7].flag;
        this.serviceChannels.detailDigitalChannels = this.stepThreeNonCerdit[
          'digitalDetail'
        ];
        this.serviceChannels.nonBank = this.stepThreeNonCerdit[
          'service'
        ][8].flag;
        this.serviceChannels.detailNonBank = this.stepThreeNonCerdit[
          'otherBankDetail'
        ];
        this.serviceChannels.etc = this.stepThreeNonCerdit['service'][9].flag;
        this.serviceChannels.detailEtc = this.stepThreeNonCerdit['otherDetail'];

        console.log('set fileDelete', this.stepThreeNonCerdit);

        // this.textareaForm = new FormGroup({
        //   targetText: new FormControl(this.stepThreeNonCerdit.targetText),
        //   propertyText: new FormControl(this.stepThreeNonCerdit.propertyText),
        //   scopeText: new FormControl(this.stepThreeNonCerdit.scopeText),
        //   conditionText: new FormControl(this.stepThreeNonCerdit.conditionText),
        //   dutyText: new FormControl(this.stepThreeNonCerdit.dutyText),
        //   feeText: new FormControl(this.stepThreeNonCerdit.feeText),
        //   otherText: new FormControl(this.stepThreeNonCerdit.otherText),
        // });

        // set picture preview to all input
        if (
          this.stepThreeNonCerdit.target !== null &&
          this.stepThreeNonCerdit.property !== null &&
          this.stepThreeNonCerdit.scope !== null &&
          this.stepThreeNonCerdit.condition !== null &&
          this.stepThreeNonCerdit.duty !== null &&
          this.stepThreeNonCerdit.fee !== null &&
          this.stepThreeNonCerdit.other !== null
        ) {
          for (let i = 0; i < this.stepThreeNonCerdit.target.file.length; i++) {
            this.preview(1, this.stepThreeNonCerdit.target.file[i], i);
          }

          for (
            let i = 0;
            i < this.stepThreeNonCerdit.property.file.length;
            i++
          ) {
            this.preview(2, this.stepThreeNonCerdit.property.file[i], i);
          }

          for (let i = 0; i < this.stepThreeNonCerdit.scope.file.length; i++) {
            this.preview(3, this.stepThreeNonCerdit.scope.file[i], i);
          }

          for (
            let i = 0;
            i < this.stepThreeNonCerdit.condition.file.length;
            i++
          ) {
            this.preview(4, this.stepThreeNonCerdit.condition.file[i], i);
          }

          for (let i = 0; i < this.stepThreeNonCerdit.duty.file.length; i++) {
            this.preview(5, this.stepThreeNonCerdit.duty.file[i], i);
          }

          for (let i = 0; i < this.stepThreeNonCerdit.fee.file.length; i++) {
            this.preview(6, this.stepThreeNonCerdit.fee.file[i], i);
          }

          for (let i = 0; i < this.stepThreeNonCerdit.other.file.length; i++) {
            this.preview(7, this.stepThreeNonCerdit.other.file[i], i);
          }

          // set fileDelete to Object stepOne
          this.stepThreeNonCerdit.target.fileDelete = [];
          this.stepThreeNonCerdit.property.fileDelete = [];
          this.stepThreeNonCerdit.scope.fileDelete = [];
          this.stepThreeNonCerdit.condition.fileDelete = [];
          this.stepThreeNonCerdit.duty.fileDelete = [];
          this.stepThreeNonCerdit.fee.fileDelete = [];
          this.stepThreeNonCerdit.other.fileDelete = [];
        } else {
          this.stepThreeNonCerdit.target = {
            file: [],
            fileDelete: [],
          };
          this.stepThreeNonCerdit.property = {
            file: [],
            fileDelete: [],
          };
          this.stepThreeNonCerdit.scope = {
            file: [],
            fileDelete: [],
          };
          this.stepThreeNonCerdit.condition = {
            file: [],
            fileDelete: [],
          };
          this.stepThreeNonCerdit.duty = {
            file: [],
            fileDelete: [],
          };
          this.stepThreeNonCerdit.fee = {
            file: [],
            fileDelete: [],
          };
          this.stepThreeNonCerdit.other = {
            file: [],
            fileDelete: [],
          };
        }

        // set file attachments
        if (this.stepThreeNonCerdit.attachments !== null) {
          this.dataSource_file = new MatTableDataSource(
            this.stepThreeNonCerdit.attachments.file
          );
          this.hideDownloadFile();
          this.stepThreeNonCerdit.attachments.fileDelete = [];
        } else {
          this.stepThreeNonCerdit.attachments = {
            file: [],
            fileDelete: [],
          };
        }
      },
      (err) => {
        this.status_loading = false;
        console.log(err);
      }
    );
  }

  // ===============================================================================

  constructor(
    private StepThreeOtherService: StepThreeService,
    private sidebarService: RequestService,
    private router: Router,
    public dialog: MatDialog,
  ) { this.dataSource = new MatTableDataSource(); }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.openDoCheckc = true;
    this.stepPage = localStorage.getItem('check');
    this.checkLocal = localStorage.getItem('check');
    this.pageLast = sessionStorage.getItem('page');
    console.log('pageeee', this.pageLast);

    if (localStorage) {
      if (localStorage.getItem('requestId')) {
        console.log('localStorage', localStorage.getItem('requestId'));
        this.sidebarService.sendEvent();
        this.sidebarService.statusSibar(localStorage.getItem('requestId')).subscribe(check => {
          console.log('ckeck ทบทวน', check);
          if (check['data'] !== null) {
            this.repeat = check['data']['repeat'];
          } else {
            this.repeat = false;
          }
        });
        this.getDataProductDetailstepThreeNonCerdit(
          localStorage.getItem('requestId')
        );
      } else {
        this.status_loading = false;
      }
    } else {
      console.log('Brownser not support');
    }
  }

  // ===============================================================================
  downloadFile(pathdata: any) {
    this.status_loading = true;
    const contentType = '';
    const sendpath = pathdata;
    console.log('path', sendpath);
    this.sidebarService.getfile(sendpath).subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        this.status_loading = false;
        const datagetfile = res['data'];
        const b64Data = datagetfile['data'];
        const filename = datagetfile['fileName'];
        console.log('base64', b64Data);
        const config = this.b64toBlob(b64Data, contentType);
        saveAs(config, filename);
      }

    });
  }

  b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const convertbyte = atob(b64Data);
    const byteCharacters = atob(convertbyte);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  downloadFile64(data: any) {
    this.status_loading = true;
    const contentType = '';
    this.status_loading = false;
    const b64Data = data.base64File;
    const filename = data.fileName;
    console.log('base64', b64Data);

    const config = this.base64(b64Data, contentType);
    saveAs(config, filename);
  }

  base64(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  autoGrowTextZone(e) {
    e.target.style.height = '0px';
    e.target.style.height = e.target.scrollHeight + 0 + 'px';
  }

  // ngOnDestroy(): void {
  //   const data = this.sidebarService.getsavePage();
  //   let saveInFoStatus = false;
  //   saveInFoStatus = data.saveStatusInfo;
  //   if (saveInFoStatus === true) {
  //     this.outOfPageSave = true;
  //     this.linkTopage = data.goToLink;
  //     console.log('ngOnDestroy', this.linkTopage);
  //     this.saveDraft();
  //     this.sidebarService.resetSaveStatusInfo();
  //   }
  // }

  hideDownloadFile() {
    if (this.dataSource_file.data.length > 0) {
      this.downloadfile_by = [];

      for (let index = 0; index < this.dataSource_file.data.length; index++) {
        const element = this.dataSource_file.data[index];
        console.log('ele', element);

        if ('path' in element) {
          this.downloadfile_by.push('path');
        } else {
          this.downloadfile_by.push('base64');
        }
      }
    }
    console.log('by:', this.downloadfile_by);
  }

  ngDoCheck() {
    if (this.openDoCheckc === true) {
      if (this.template_manage.caaStatus !== 'show') {
        this.pageChange = this.sidebarService.changePageGoToLink();
        if (this.pageChange) {
          // this.getNavbar();
          const data = this.sidebarService.getsavePage();
          let saveInFoStatus = false;
          saveInFoStatus = data.saveStatusInfo;
          if (saveInFoStatus === true) {
            this.outOfPageSave = true;
            this.linkTopage = data.goToLink;
            this.saveDraft('navbar', null);
            this.sidebarService.resetSaveStatu();
          }
        }
      }
    }
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate request');
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef =  this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        if (data.returnStatus === true) {
          const raturnStatus = this.saveDraft();
          console.log('request raturnStatus', raturnStatus);
            if (raturnStatus) {
              console.log('raturnStatus T', this.saveDraftstatus);
              subject.next(true);
            } else {
              console.log('raturnStatus F', this.saveDraftstatus);
              localStorage.setItem('check', this.stepPage);
              subject.next(false);
            }
        } else if (data.returnStatus === false) {
          this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          localStorage.setItem('check', this.stepPage);
          subject.next(false);
        }
      });
      return subject;
    } else {
      return true;
    }
  }

}
