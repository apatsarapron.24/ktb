import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NonCreditComponent } from './non-credit.component';

describe('NonCreditComponent', () => {
  let component: NonCreditComponent;
  let fixture: ComponentFixture<NonCreditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NonCreditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NonCreditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
