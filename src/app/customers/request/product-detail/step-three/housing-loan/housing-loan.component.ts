import { Component, OnInit, Input, Output, EventEmitter, DoCheck } from '@angular/core';
import { StepThreeService } from '../../../../../services/request/product-detail/step-three/step-three.service';
import { RequestService } from '../../../../../services/request/request.service';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
// import { StepThreeComponent } from '../step-three.component';
import { saveAs } from 'file-saver';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogSaveStatusComponent } from '../../../dialog/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from '../../../../../services/configGuard/config-guard.service';

export interface FileList {
  fileName: string;
  fileSize: string;
  base64File: string;
}

const File_data: FileList[] = [];

@Component({
  selector: 'app-housing-loan',
  templateUrl: './housing-loan.component.html',
  styleUrls: ['./housing-loan.component.scss'],
})
export class HousingLoanComponent implements OnInit, CanComponentDeactivate {
  @Input() template_manage = { role: null, validate: null, caaStatus: null, pageShow: null };
  @Output() saveStatus = new EventEmitter<boolean>();
  displayedColumns_file: string[] = ['fileName', 'fileSize', 'delete'];
  dataSource_file: MatTableDataSource<FileList>;

  status_loading = true;
  validate = null;
  header = [
    {
      name: 'loandType',
      value: 'ประเภทสินเชื่อ',
    },
    {
      name: 'limitType',
      value: 'ประเภทวงเงิน',
    },
    {
      name: 'creditLimit',
      value: 'วงเงินสินเชื่อสูงสุด (ลบ.)',
    },
    {
      name: 'preriod',
      value: 'ระยะเวลาให้กู้สูงสุด (ปี)',
    },
  ];
  sub_header = [
    {
      name: 'creditMin',
      value: 'Min',
    },
    {
      name: 'creditMax',
      value: 'Max',
    },
    {
      name: 'creditAvg',
      value: 'Average',
    },
  ];
  all_header = [
    {
      name: 'loandType',
      value: 'ประเภทสินเชื่อ',
    },
    {
      name: 'limitType',
      value: 'ประเภทวงเงิน',
    },
    {
      name: 'creditLimit',
      value: 'วงเงินสินเชื่อสูงสุด (ลบ.)',
    },
    {
      name: 'preriod',
      value: 'ระยะเวลาให้กู้สูงสุด (ปี)',
    },
    {
      name: 'creditMin',
      value: 'Min',
    },
    {
      name: 'creditMax',
      value: 'Max',
    },
    {
      name: 'creditAvg',
      value: 'Average',
    },
  ];
  alertStatus = false;
  saveDraftstatus = null;
  outOfPageSave = false;
  linkTopage = null;
  pageChange = null;
  openDoCheckc = false;

  stepThree_3: any = {
    requestId: '',
    status: false,
    requestHousingCredit: [],
    requestHousingCreditDelete: [],
    agreement: [
      {
        topic: 'MOU',
        flag: false,
      },
      {
        topic: 'Non-MOU',
        flag: false,
      },
      {
        topic: 'ไม่มีกำหนด',
        flag: false,
      },
    ],
    target: [
      {
        topic: 'Top Developers',
        flag: false,
        child: [
          {
            topic: 'Top 6Developers',
            flag: false,
          },
          {
            topic: 'Top 9Developers',
            flag: false,
          },
          {
            topic: 'Top Region Developers',
            flag: false,
          },
          {
            topic: 'Top Region Silver Developers',
            flag: false,
          },
        ],
        otherDetail: null,
      },
      {
        topic: 'Big Developers',
        flag: false,
        child: [
          {
            topic: 'AAA',
            flag: false,
          },
          {
            topic: 'AA',
            flag: false,
          },
        ],
        otherDetail: null,
      },
      {
        topic: 'Non-Top Developers',
        flag: false,
        child: null,
        otherDetail: null,
      },
      {
        topic: 'อื่นๆ',
        flag: false,
        child: null,
        otherDetail: null,
      },
    ],
    guaranty: [
      {
        topic: 'เงินฝาก',
        flag: false,
        child: null,
        otherDetail: null,
      },
      {
        topic: 'ตราสารหนี้ ตราสารทุน หน่วยลงทุน',
        flag: false,
        child: [
          {
            topic: 'พันธมิตร หุ้นกู้ หรือตั๋วเงิน',
            flag: false,
          },
          {
            topic: 'หุ้นบุริมสิทธิหรือหุ้นสามัญ',
            flag: false,
          },
          {
            topic: 'หน่วยลงทุนทุกประเภทกองทุนรวม',
            flag: false,
          },
          {
            topic: 'หน่วยลงทุนประเภทกองทุนเปิด',
            flag: false,
          },
        ],
        otherDetail: null,
      },
      {
        topic: 'อสังหาริมทรัพย์',
        flag: false,
        child: [
          {
            topic: 'ที่ดินว่างเปล่า',
            flag: false,
          },
          {
            topic: 'ที่ดินว่างเปล่าที่มีสพาพคล่องต่ำ',
            flag: false,
          },
          {
            topic: 'ที่ดินในนิคมอุตสาหกรรม',
            flag: false,
          },
          {
            topic: 'ที่ดินพร้อมสิ่งปลูกสร้าง',
            flag: false,
          },
          {
            topic: 'กรรมสิทธิอาคารชุด',
            flag: false,
          },
          {
            topic: 'อู่ซ่อมเรือ',
            flag: false,
          },
          {
            topic: 'อสังหาริมทรัพย์อื่นๆ',
            flag: false,
          },
        ],
        otherDetail: null,
      },
      {
        topic: 'สินค้า/เครื่องจักร/เรือ',
        flag: false,
        child: [
          {
            topic: 'เครื่องจักร',
            flag: false,
          },
          {
            topic: 'สต๊อคสินค้าในคลังสินค้า',
            flag: false,
          },
          {
            topic: 'เรือ',
            flag: false,
          },
        ],
        otherDetail: null,
      },
      {
        topic: 'การค้ำประกัน',
        flag: false,
        child: [
          {
            topic: 'หนังสือค้ำประกัน Standby Lettter of Credit',
            flag: false,
          },
          {
            topic: 'การค้ำประกันโดยกระทรวงการคลัง',
            flag: false,
          },
          {
            topic: 'การค้ำประกันโดยสถาบันการเงิน',
            flag: false,
          },
          {
            topic: 'การค้ำประกันโดยบริษัทสินเชื่ออุตสาหกรรมขนาดย่อย',
            flag: false,
          },
        ],
        otherDetail: null,
      },
      {
        topic: 'โอนสิทธิการเช่า',
        flag: false,
        child: null,
        otherDetail: null,
      },
      {
        topic: 'บุคคลค้ำประกัน',
        flag: false,
        child: null,
        otherDetail: null,
      },
      {
        topic: 'อื่นๆ',
        flag: false,
        child: null,
        otherDetail: '',
      },
    ],
    consider: {
      applicant: [],
      finan: [],
      status: [],
      other: [],
    },
    considerDelete: [],
    loan: '',
    interest: '',
    interestApprove: '',
    other: '',
    requestLoanHousingPicture: {
      file: [],
      fileDelete: [],
    },
    requestInterestHousingPicture: {
      file: [],
      fileDelete: [],
    },
    requestInterestApproveHousingPicture: {
      file: [],
      fileDelete: [],
    },
    requestOtherHousingPicture: {
      file: [],
      fileDelete: [],
    },
    requestHousingAttachments: {
      file: [],
      fileDelete: [],
    },
    conclusions: null
  };
  res: any;
  res_2: any;
  get_data: any;

  index_to_create_template: any = [];
  length_header: any = 0;

  add_data: any = {
    id: '',
    loandType: '',
    limitType: '',
    creditLimit: '',
    preriod: '',
    creditMax: '',
    creditMin: '',
    creditAvg: '',
  };
  showCircle_colId = null;
  showCircle_id = null;
  enter = 3;

  data_pic: any = {
    objective: {
      objectiveDetail: null,
      uploadFile: [],
    },
    businessModel: {
      businessModelDetail: null,
      uploadFile: [],
    },
    productCharacteristics: {
      productCharacteristicsDetail: null,
      uploadFile: [],
    },
    another: {
      anotherDetail: null,
      uploadFile: [],
    },
  };
  file1_1: any;
  file1_2: any;
  file2_1: any;
  file2_2: any;
  file3_1: any;
  file3_2: any;
  file4_1: any;
  file4_2: any;
  document: any;
  file_name: any;
  file_size: number;

  imgURL: any;
  image1 = [];
  image2 = [];
  image3 = [];
  image4 = [];
  showImage1 = false;
  message = null;

  // Model
  showModel = false;
  modalmainImageIndex = 0;
  modalmainImagedetailIndex = 0;
  indexSelect = 0;
  modalImage = [];
  modalImagedetail = [];
  fileValue = 0;

  urls: any = [];
  base64Image: any;
  fileName: any;
  file_img1: any = [];
  file_img2: any = [];
  file_img3: any = [];
  file_img4: any = [];

  file_img1_delete: any = [];
  file_img2_delete: any = [];
  file_img3_delete: any = [];
  file_img4_delete: any = [];

  next_page: any;
  data: any;

  loandetail = false;
  agreement = false;
  target = false;
  targetOtherDetail = false;
  guaranty = false;
  loanText = false;
  interestText = false;
  interestApproveText = false;

  // textareaForm = new FormGroup({
  //   loan: new FormControl(this.stepThree_3.loan),
  //   interest: new FormControl(this.stepThree_3.interest),
  //   interestApprove: new FormControl(this.stepThree_3.interestApprove),
  //   other: new FormControl(this.stepThree_3.other),
  // });
  // coppy_paste_status: number = 0;

  FileOverSize: any;

  considerDefault: any = {
    applicant: [
      {
        id: null,
        topic: 'ประสบการณ์',
        detail: {
          'MOU': '',
          'Non-MOU': '',
          'Undefined': ''
        },
      },
      {
        id: null,
        topic: 'อายุผู้กู้',
        detail: {
          'MOU': '',
          'Non-MOU': '',
          'Undefined': ''
        },
      },
      {
        id: null,
        topic: 'QCA Rating/Credit Rating',
        detail: {
          'MOU': '',
          'Non-MOU': '',
          'Undefined': ''
        },
      },
      {
        id: null,
        topic: 'KTB Grade',
        detail: {
          'MOU': '',
          'Non-MOU': '',
          'Undefined': ''
        },
      },
      {
        id: null,
        topic: 'KTB Color',
        detail: {
          'MOU': '',
          'Non-MOU': '',
          'Undefined': ''
        },
      },
    ],
    finan: [
      {
        id: null,
        topic: 'DSCR',
        detail: {
          'MOU': '',
          'Non-MOU': '',
          'Undefined': ''
        },
      },
      {
        id: null,
        topic: 'D/E Ratio',
        detail: {
          'MOU': '',
          'Non-MOU': '',
          'Undefined': ''
        },
      },
      {
        id: null,
        topic: 'กําไรสุทธิ',
        detail: {
          'MOU': '',
          'Non-MOU': '',
          'Undefined': ''
        },
      },
      {
        id: null,
        topic: 'ส่วนทุนของผู้ถือหุ้น',
        detail: {
          'MOU': '',
          'Non-MOU': '',
          'Undefined': ''
        },
      },
      {
        id: null,
        topic: 'ทุนจดทะเบียน',
        detail: {
          'MOU': '',
          'Non-MOU': '',
          'Undefined': ''
        },
      },
    ],
    status: [{
      id: null,
      topic: 'EWS Risk Level (สําหรับลูกค้าเดิม)',
      detail: {
        'MOU': '',
        'Non-MOU': '',
        'Undefined': ''
      },
    },

    {
      id: null,
      topic: 'White List และ AML List',
      detail: {
        'MOU': '',
        'Non-MOU': '',
        'Undefined': ''
      },
    },

    {
      id: null,
      topic: 'การปรับโครงสร้างหนี้',
      detail: {
        'MOU': '',
        'Non-MOU': '',
        'Undefined': ''
      },
    },

    {
      id: null,
      topic: 'การเปลียนผู้ตรวจสอบบัญา',
      detail: {
        'MOU': '',
        'Non-MOU': '',
        'Undefined': ''
      },
    },
    {
      id: null,
      topic: 'การฟ้องร้องเกี่ยวกับการเงิน',
      detail: {
        'MOU': '',
        'Non-MOU': '',
        'Undefined': ''
      },
    }, {
      id: null,
      topic: 'การตรวจสอบเครดิตบูโร',
      detail: {
        'MOU': '',
        'Non-MOU': '',
        'Undefined': ''
      },
    },
    ],
    other: [
      {
        id: null,
        topic: 'Core Asset',
        detail: {
          'MOU': '',
          'Non-MOU': '',
          'Undefined': ''
        },
      },
    ],
  };
  repeat = false;
  downloadfile_by = [];

  constructor(
    private router: Router,
    private StepThree: StepThreeService,
    private sideBarService: RequestService,
    // private CheckPage: StepThreeComponent,
    public dialog: MatDialog,
  ) { }
  stepPage: any;
  checkLocal: any;
  pageLast: any;
  ngOnInit() {
    window.scrollTo(0, 0);
    this.openDoCheckc = true;
    this.stepPage = localStorage.getItem('check');
    this.checkLocal = localStorage.getItem('check');
    this.pageLast = (sessionStorage.getItem('page'));
    console.log('pageeee', this.pageLast);
    this.stepThree_3.requestId = Number(localStorage.getItem('requestId'));
    this.sideBarService.sendEvent();
    this.sideBarService.statusSibar(localStorage.getItem('requestId')).subscribe(check => {
      console.log('ckeck ทบทวน', check);
      if (check['data'] !== null) {
        this.repeat = check['data']['repeat'];
      } else {
        this.repeat = false;
      }
    });
    this.GetREst();
  }

  sendSaveStatus() {
    if (this.saveDraftstatus === 'success') {
      this.saveStatus.emit(true);
    } else {
      this.saveStatus.emit(false);
    }
  }

  changeSaveDraft() {
    this.saveDraftstatus = null;
    this.sendSaveStatus();
    this.sideBarService.inPageStatus(true);
  }

  GetREst() {
    console.log('000055555', this.template_manage.pageShow);
    this.StepThree.get_step_three_housing(this.stepThree_3.requestId, this.template_manage.pageShow).subscribe(
      (res) => {
        if (res['status'] === 'success') {
          this.status_loading = false;
          this.stepThree_3['parentRequestId'] = res['parentRequestId'];
        }
        console.log('start:', res);
        this.res = res;
        this.get_data = this.res.data;

        if (this.get_data == null || this.get_data === []) {
          // this.stepThree_3 = this.stepThree_3;
          this.get_data = this.stepThree_3;
          this.stepThree_3.requestId = Number(localStorage.getItem('requestId'));
          // console.log('form', this.get_data);
          this.validate = res['validate'];
          if (this.stepThree_3.consider.applicant.length === 0) {
            for (let index = 0; index < this.considerDefault.applicant.length; index++) {
              this.stepThree_3.consider.applicant.push(this.considerDefault.applicant[index]);
            }
          }
      
          if (this.stepThree_3.consider.finan.length === 0) {
            for (let index = 0; index < this.considerDefault.finan.length; index++) {
              this.stepThree_3.consider.finan.push(this.considerDefault.finan[index]);
            }
          }
      
          if (this.stepThree_3.consider.status.length === 0) {
            for (let index = 0; index < this.considerDefault.status.length; index++) {
              this.stepThree_3.consider.status.push(this.considerDefault.status[index]);
            }
          }
      
          if (this.stepThree_3.consider.other.length === 0) {
            for (let index = 0; index < this.considerDefault.other.length; index++) {
              this.stepThree_3.consider.other.push(this.considerDefault.other[index]);
            }
          }
        } else {
          this.get_data = this.res.data;
          this.validate = res['validate'];
        }

        this.getData();
      }
    );
  }
  autogrow(index: any) {
    if (index === 1) {
      const loan = document.getElementById('loan');
      loan.style.overflow = 'hidden';
      loan.style.height = 'auto';
      loan.style.height = loan.scrollHeight + 'px';
    }
    if (index === 2) {
      const interest = document.getElementById('interest');
      interest.style.overflow = 'hidden';
      interest.style.height = 'auto';
      interest.style.height = interest.scrollHeight + 'px';
    }
    if (index === 3) {
      const interestApprove = document.getElementById('interestApprove');
      interestApprove.style.overflow = 'hidden';
      interestApprove.style.height = 'auto';
      interestApprove.style.height = interestApprove.scrollHeight + 'px';
    }
    if (index === 4) {
      const other = document.getElementById('other');
      other.style.overflow = 'hidden';
      other.style.height = 'auto';
      other.style.height = other.scrollHeight + 'px';
    }
  }
  saveDraft(page?, action?) {
    this.alertStatus = false;
    const returnUpdateData = new Subject<any>();
    this.status_loading = true;
    const result = this.check_text_before();

    if (this.template_manage.pageShow === 'summary') {
      this.stepThree_3.conclusions = true;
    } else {
      this.stepThree_3.conclusions = null;
    }
    if (this.stepThree_3.guaranty[7].otherDetail === '' && this.stepThree_3.guaranty[7].flag === true) {
      this.status_loading = false;
      this.alertStatus = true;
      console.log('8888888888888888888888888 h');
      alert('กรุณากรอกช่องอื่นๆในประเภทหลักประกัน aaa');
      return false;
    } else if (result === true && this.alertStatus === false) {
      console.log('99999999999999999999999999 h');
      console.log('start::', this.get_data);
      this.StepThree.update_step_three_housing(this.stepThree_3).subscribe(
        (res) => {
          if (res['status'] === 'success') {
            this.status_loading = false;
            this.downloadfile_by = [];
            this.saveDraftstatus = 'success';
            returnUpdateData.next(true);
            this.sideBarService.inPageStatus(false);
            setTimeout(() => {
              this.saveDraftstatus = 'fail';
            }, 3000);
            this.sendSaveStatus();
            this.sideBarService.sendEvent();

            console.log('rs::', res);
            this.stepThree_3.requestHousingAttachments.file = [];
            this.dataSource_file = new MatTableDataSource(File_data);

            this.image1 = [];
            this.image2 = [];
            this.image3 = [];
            this.image4 = [];

            this.data_pic.objective.uploadFile = [];
            this.data_pic.businessModel.uploadFile = [];
            this.data_pic.productCharacteristics.uploadFile = [];
            this.data_pic.another.uploadFile = [];
            this.stepThree_3.requestHousingCredit = [];
            this.stepThree_3.requestHousingCreditDelete = [];
            this.stepThree_3.requestLoanHousingPicture.file = [];
            this.stepThree_3.requestInterestHousingPicture.file = [];
            this.stepThree_3.requestInterestApproveHousingPicture.file = [];
            this.stepThree_3.requestOtherHousingPicture.file = [];

            this.GetREst();
          } else if (res['status'] === 'fail') {
            this.sendSaveStatus();
            this.status_loading = false;
            alert(res['message']);
            console.log('message::', res['message']);
            returnUpdateData.next(false);
          }
        },
        (err) => {
          console.log(err);
          this.status_loading = false;
          alert(err['message']);
          returnUpdateData.next(false);
        }
      );
      return returnUpdateData;
    } else {
      console.log('111111111111111111111 h');
      this.status_loading = false;
      return false;
    }
  }
  nextpage(action) {
    console.log('5555555555 nextpage');
    this.StepThree.CheckNextPage(localStorage.getItem('check'), action);
  }

  viewNext() {
    this.StepThree.CheckNextPage(localStorage.getItem('check'), 'next');
  }

  getData() {
    this.stepThree_3.requestHousingCredit = this.get_data.requestHousingCredit;
    this.stepThree_3.agreement = this.get_data.agreement;
    this.stepThree_3.target = this.get_data.target;
    this.stepThree_3.consider = this.get_data.consider;

    console.log('considerDefault:', this.considerDefault);

   
    console.log('consider:', this.stepThree_3.consider);
    // ================================================================================
    this.stepThree_3.guaranty[0].flag = this.get_data.guaranty[0].flag;

    this.stepThree_3.guaranty[1].flag = this.get_data.guaranty[1].flag;
    this.stepThree_3.guaranty[1].child[0].flag = this.get_data.guaranty[1].child[0].flag;
    this.stepThree_3.guaranty[1].child[1].flag = this.get_data.guaranty[1].child[1].flag;
    this.stepThree_3.guaranty[1].child[2].flag = this.get_data.guaranty[1].child[2].flag;
    this.stepThree_3.guaranty[1].child[3].flag = this.get_data.guaranty[1].child[3].flag;

    this.stepThree_3.guaranty[2].flag = this.get_data.guaranty[2].flag;
    this.stepThree_3.guaranty[2].child[0].flag = this.get_data.guaranty[2].child[0].flag;
    this.stepThree_3.guaranty[2].child[1].flag = this.get_data.guaranty[2].child[1].flag;
    this.stepThree_3.guaranty[2].child[2].flag = this.get_data.guaranty[2].child[2].flag;
    this.stepThree_3.guaranty[2].child[3].flag = this.get_data.guaranty[2].child[3].flag;
    this.stepThree_3.guaranty[2].child[4].flag = this.get_data.guaranty[2].child[4].flag;
    this.stepThree_3.guaranty[2].child[5].flag = this.get_data.guaranty[2].child[5].flag;
    this.stepThree_3.guaranty[2].child[6].flag = this.get_data.guaranty[2].child[6].flag;

    this.stepThree_3.guaranty[3].flag = this.get_data.guaranty[3].flag;
    this.stepThree_3.guaranty[3].child[0].flag = this.get_data.guaranty[3].child[0].flag;
    this.stepThree_3.guaranty[3].child[1].flag = this.get_data.guaranty[3].child[1].flag;
    this.stepThree_3.guaranty[3].child[2].flag = this.get_data.guaranty[3].child[2].flag;

    this.stepThree_3.guaranty[4].flag = this.get_data.guaranty[4].flag;
    this.stepThree_3.guaranty[4].child[0].flag = this.get_data.guaranty[4].child[0].flag;
    this.stepThree_3.guaranty[4].child[1].flag = this.get_data.guaranty[4].child[1].flag;
    this.stepThree_3.guaranty[4].child[2].flag = this.get_data.guaranty[4].child[2].flag;
    this.stepThree_3.guaranty[4].child[3].flag = this.get_data.guaranty[4].child[3].flag;

    this.stepThree_3.guaranty[5].flag = this.get_data.guaranty[5].flag;
    this.stepThree_3.guaranty[6].flag = this.get_data.guaranty[6].flag;
    this.stepThree_3.guaranty[7].flag = this.get_data.guaranty[7].flag;
    this.stepThree_3.guaranty[7].otherDetail = this.get_data.guaranty[7].otherDetail;
    if (this.stepThree_3.guaranty[7].flag === false) {
      this.stepThree_3.guaranty[7].otherDetail = '';
    }
    this.init_value();
    // this.changeCheckbox();
    // ***************************************************************************************************/
    // this.data_pic.objective.objectiveDetail = this.get_data.loan;
    this.stepThree_3.loan = this.get_data.loan;
    for (
      let index = 0;
      index < this.get_data.requestLoanHousingPicture.file.length;
      index++
    ) {
      this.base64Image = this.get_data.requestLoanHousingPicture.file[
        index
      ].base64File;
      this.fileName = this.get_data.requestLoanHousingPicture.file[
        index
      ].fileName;
      const byteString =
        'data:image/jpeg;base64,' + this.base64Image.split(',')[1];
      const ab = new ArrayBuffer(byteString.length);
      const ia = new Uint8Array(ab);
      for (let i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
      }
      fetch('data:image/jpeg;base64,' + this.base64Image)
        .then((res) => res.blob())
        .then((blob) => {
          const file_1 = new File(
            [blob],
            this.get_data.requestLoanHousingPicture.file[index].fileName,
            {
              type: 'image/png',
            }
          );

          // console.log("fff", file_1);
          this.data_pic.objective.uploadFile.push({
            file: file_1,
            id: this.get_data.requestLoanHousingPicture.file[index].id,
          });
          this.stepThree_3.requestLoanHousingPicture.file.push({
            id: this.get_data.requestLoanHousingPicture.file[index].id,
            fileName: this.get_data.requestLoanHousingPicture.file[index]
              .fileName,
            fileSize: file_1.size,
            base64File: this.get_data.requestLoanHousingPicture.file[index]
              .base64File,
          });
        });

      const file = 'data:image/jpeg;base64,' + this.base64Image;

      this.image1[index] = file;
    }
    // console.log("loan 1:",this.file_img1)
    // ***************************************************************************************************/
    // this.data_pic.businessModel.businessModelDetail = this.get_data.interest;
    this.stepThree_3.interest = this.get_data.interest;
    for (
      let index = 0;
      index < this.get_data.requestInterestHousingPicture.file.length;
      index++
    ) {
      this.base64Image = this.get_data.requestInterestHousingPicture.file[
        index
      ].base64File;
      this.fileName = this.get_data.requestInterestHousingPicture.file[
        index
      ].fileName;
      const byteString =
        'data:image/jpeg;base64,' + this.base64Image.split(',')[1];
      const ab = new ArrayBuffer(byteString.length);
      const ia = new Uint8Array(ab);
      for (let i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
      }
      fetch('data:image/jpeg;base64,' + this.base64Image)
        .then((res) => res.blob())
        .then((blob) => {
          const file_1 = new File(
            [blob],
            this.get_data.requestInterestHousingPicture.file[index].fileName,
            {
              type: 'image/png',
            }
          );

          // console.log("fff", file_1);
          this.data_pic.businessModel.uploadFile.push({
            file: file_1,
            id: this.get_data.requestInterestHousingPicture.file[index].id,
          });
          this.stepThree_3.requestInterestHousingPicture.file.push({
            id: this.get_data.requestInterestHousingPicture.file[index].id,
            fileName: this.get_data.requestInterestHousingPicture.file[index]
              .fileName,
            fileSize: file_1.size,
            base64File: this.get_data.requestInterestHousingPicture.file[index]
              .base64File,
          });
        });

      const file = 'data:image/jpeg;base64,' + this.base64Image;

      this.image2[index] = file;
    }
    // ***************************************************************************************************/
    // this.data_pic.productCharacteristics.productCharacteristicsDetail = this.get_data.interestApprove;
    this.stepThree_3.interestApprove = this.get_data.interestApprove;
    for (
      let index = 0;
      index < this.get_data.requestInterestApproveHousingPicture.file.length;
      index++
    ) {
      this.base64Image = this.get_data.requestInterestApproveHousingPicture.file[
        index
      ].base64File;
      this.fileName = this.get_data.requestInterestApproveHousingPicture.file[
        index
      ].fileName;
      const byteString =
        'data:image/jpeg;base64,' + this.base64Image.split(',')[1];
      const ab = new ArrayBuffer(byteString.length);
      const ia = new Uint8Array(ab);
      for (let i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
      }
      fetch('data:image/jpeg;base64,' + this.base64Image)
        .then((res) => res.blob())
        .then((blob) => {
          const file_1 = new File(
            [blob],
            this.get_data.requestInterestApproveHousingPicture.file[
              index
            ].fileName,
            {
              type: 'image/png',
            }
          );

          // console.log("fff", file_1);
          this.data_pic.productCharacteristics.uploadFile.push({
            file: file_1,
            id: this.get_data.requestInterestApproveHousingPicture.file[index]
              .id,
          });
          this.stepThree_3.requestInterestApproveHousingPicture.file.push({
            id: this.get_data.requestInterestApproveHousingPicture.file[index]
              .id,
            fileName: this.get_data.requestInterestApproveHousingPicture.file[
              index
            ].fileName,
            fileSize: file_1.size,
            base64File: this.get_data.requestInterestApproveHousingPicture.file[
              index
            ].base64File,
          });
        });

      const file = 'data:image/jpeg;base64,' + this.base64Image;

      this.image3[index] = file;
    }
    // ***************************************************************************************************/
    // this.data_pic.another.anotherDetail = this.get_data.other;
    this.stepThree_3.other = this.get_data.other;
    for (
      let index = 0;
      index < this.get_data.requestOtherHousingPicture.file.length;
      index++
    ) {
      this.base64Image = this.get_data.requestOtherHousingPicture.file[
        index
      ].base64File;
      this.fileName = this.get_data.requestOtherHousingPicture.file[
        index
      ].fileName;
      const byteString =
        'data:image/jpeg;base64,' + this.base64Image.split(',')[1];
      const ab = new ArrayBuffer(byteString.length);
      const ia = new Uint8Array(ab);
      for (let i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
      }
      fetch('data:image/jpeg;base64,' + this.base64Image)
        .then((res) => res.blob())
        .then((blob) => {
          const file_1 = new File(
            [blob],
            this.get_data.requestOtherHousingPicture.file[index].fileName,
            {
              type: 'image/png',
            }
          );

          // console.log("fff", file_1);
          this.data_pic.another.uploadFile.push({
            file: file_1,
            id: this.get_data.requestOtherHousingPicture.file[index].id,
          });
          this.stepThree_3.requestOtherHousingPicture.file.push({
            id: this.get_data.requestOtherHousingPicture.file[index].id,
            fileName: this.get_data.requestOtherHousingPicture.file[index]
              .fileName,
            fileSize: file_1.size,
            base64File: this.get_data.requestOtherHousingPicture.file[index]
              .base64File,
          });
        });

      const file = 'data:image/jpeg;base64,' + this.base64Image;

      this.image4[index] = file;
    }
    // ***************************************************************************************************/

    // console.log('loan 1:', this.file_img1);
    // console.log('loan 2:', this.file_img2);
    // console.log('loan 3:', this.file_img3);
    // console.log('loan 4:', this.file_img4);
    // console.log("uploadFile::", this.stepThree.objective.uploadFile);

    // this.textareaForm = new FormGroup({
    //   loan: new FormControl(this.stepThree_3.loan),
    //   interest: new FormControl(this.stepThree_3.interest),
    //   interestApprove: new FormControl(this.stepThree_3.interestApprove),
    //   other: new FormControl(this.stepThree_3.other),
    // });

    // if(this.coppy_paste_status === 0) {
    //   document.getElementById('loan').addEventListener('paste', function (e: ClipboardEvent) {
    //     e.preventDefault();
    //     const text = e.clipboardData.getData('text/plain');
    //     document.execCommand('insertHTML', false, text);
    //   });

    //   document.getElementById('interest').addEventListener('paste', function (e: ClipboardEvent) {
    //     e.preventDefault();
    //     const text = e.clipboardData.getData('text/plain');
    //     document.execCommand('insertHTML', false, text);
    //   });

    //   document.getElementById('interestApprove').addEventListener('paste', function (e: ClipboardEvent) {
    //     e.preventDefault();
    //     const text = e.clipboardData.getData('text/plain');
    //     document.execCommand('insertHTML', false, text);
    //   });

    //   document.getElementById('other').addEventListener('paste', function (e: ClipboardEvent) {
    //     e.preventDefault();
    //     const text = e.clipboardData.getData('text/plain');
    //     document.execCommand('insertHTML', false, text);
    //   });

    // }
    // ****************************************muti file*************************************************

    for (
      let index = 0;
      index < this.get_data.requestHousingAttachments.file.length;
      index++
    ) {
      this.stepThree_3.requestHousingAttachments.file.push({
        id: this.get_data.requestHousingAttachments.file[index].id,
        fileName: this.get_data.requestHousingAttachments.file[index].fileName,
        base64File: this.get_data.requestHousingAttachments.file[index]
          .base64File,
        fileSize:
          this.get_data.requestHousingAttachments.file[index].fileSize,
        path: this.get_data.requestHousingAttachments.file[index].path,
      });
    }

    this.dataSource_file = new MatTableDataSource(this.stepThree_3.requestHousingAttachments.file);
    this.hideDownloadFile();
    // this.stepThree_3.requestHousingAttachments.file = this.urls;
    // **************************************muti file****************************************************
    // console.log("URL:::", this.urls);

    // this.stepThree_3.requestLoanHousingPicture.file = this.file_img1;
    // this.stepThree_3.requestLoanHousingPicture.fileDelete = this.file_img1_delete;

    // this.stepThree_3.requestInterestHousingPicture.file = this.file_img2;
    // this.stepThree_3.requestInterestHousingPicture.fileDelete = this.file_img2_delete;

    // this.stepThree_3.requestInterestApproveHousingPicture.file = this.file_img3;
    // this.stepThree_3.requestInterestApproveHousingPicture.fileDelete = this.file_img3_delete;

    // this.stepThree_3.requestOtherHousingPicture.file = this.file_img4;
    // this.stepThree_3.requestOtherHousingPicture.fileDelete = this.file_img4_delete;
  }

  check_text_before() {
    this.loandetail = false;
    this.agreement = false;
    this.target = false;
    this.targetOtherDetail = false;
    this.guaranty = false;
    this.loanText = false;
    this.interestText = false;
    this.interestApproveText = false;

    if (this.stepThree_3.agreement[0].flag === false) {

      for (let k = 0; k < this.stepThree_3.consider.applicant.length; k++) {
        delete this.stepThree_3.consider.applicant[k].detail[
          this.stepThree_3.agreement[0].topic
        ];
      }
      for (let k = 0; k < this.stepThree_3.consider.finan.length; k++) {
        delete this.stepThree_3.consider.finan[k].detail[
          this.stepThree_3.agreement[0].topic
        ];
      }
      for (let k = 0; k < this.stepThree_3.consider.status.length; k++) {
        delete this.stepThree_3.consider.status[k].detail[
          this.stepThree_3.agreement[0].topic
        ];
      }
      for (let k = 0; k < this.stepThree_3.consider.other.length; k++) {
        delete this.stepThree_3.consider.other[k].detail[
          this.stepThree_3.agreement[0].topic
        ];
      }
    }

    if (this.stepThree_3.agreement[1].flag === false) {

      for (let k = 0; k < this.stepThree_3.consider.applicant.length; k++) {
        delete this.stepThree_3.consider.applicant[k].detail[
          'Non-MOU'
        ];
      }
      for (let k = 0; k < this.stepThree_3.consider.finan.length; k++) {
        delete this.stepThree_3.consider.finan[k].detail[
          'Non-MOU'
        ];
      }
      for (let k = 0; k < this.stepThree_3.consider.status.length; k++) {
        delete this.stepThree_3.consider.status[k].detail[
          'Non-MOU'
        ];
      }
      for (let k = 0; k < this.stepThree_3.consider.other.length; k++) {
        delete this.stepThree_3.consider.other[k].detail[
          'Non-MOU'
        ];
      }
    }

    if (this.stepThree_3.agreement[2].flag === false) {

      for (let k = 0; k < this.stepThree_3.consider.applicant.length; k++) {
        delete this.stepThree_3.consider.applicant[k].detail[
          'Undefined'
        ];
      }
      for (let k = 0; k < this.stepThree_3.consider.finan.length; k++) {
        delete this.stepThree_3.consider.finan[k].detail[
          'Undefined'
        ];
      }
      for (let k = 0; k < this.stepThree_3.consider.status.length; k++) {
        delete this.stepThree_3.consider.status[k].detail[
          'Undefined'
        ];
      }
      for (let k = 0; k < this.stepThree_3.consider.other.length; k++) {
        delete this.stepThree_3.consider.other[k].detail[
          'Undefined'
        ];
      }
    }

    for (let index = 0; index < this.stepThree_3.agreement.length; index++) {
      if (this.stepThree_3.agreement[index].flag === true) {
        this.agreement = true;
        break;
      }
    }
    console.log('target', this.stepThree_3.target);
    for (let index = 0; index < this.stepThree_3.target.length; index++) {
      if (
        this.stepThree_3.target[index].flag === true &&
        this.stepThree_3.target[index].child === null
      ) {
        this.target = true;
        break;
      } else if (
        this.stepThree_3.target[index].flag === true &&
        this.stepThree_3.target[index].child.length !== 0
      ) {
        for (let i = 0; i < this.stepThree_3.target[index].child.length; i++) {
          if (this.stepThree_3.target[index].child[i].flag === true) {
            this.target = true;
            break;
          }
        }
      }
    }
    console.log('this.stepThree_3.target[3].flag', this.stepThree_3.target[3]);
    if (this.stepThree_3.target[3].flag === true) {
      if (this.stepThree_3.target[3].otherDetail !== null && this.stepThree_3.target[3].otherDetail !== '') {
        this.targetOtherDetail = true;
      } else {
        this.targetOtherDetail = false;
        this.alertStatus = true;
        alert('กรุณากรอกช่องอื่นๆในกลุ่มเป้าหมาย');
      }
    } else {
      this.targetOtherDetail = true;
    }
    console.log('888888888', this.targetOtherDetail);

    // console.log('this.stepThree_3.guaranty', this.stepThree_3.guaranty);
    for (let index = 0; index < this.stepThree_3.guaranty.length; index++) {
      // console.log('index:', this.stepThree_3.guaranty[index]);
      if (
        this.stepThree_3.guaranty[index].flag === true &&
        this.stepThree_3.guaranty[index].child === null
      ) {
        this.guaranty = true;
        break;
      } else if (
        this.stepThree_3.guaranty[index].flag === true &&
        this.stepThree_3.guaranty[index].child !== null
      ) {
        for (let i = 0; i < this.stepThree_3.guaranty[index].child.length; i++) {
          if (this.stepThree_3.guaranty[index].child[i].flag === true) {
            this.guaranty = true;
            break;
          }
        }
      }
    }

    if (this.stepThree_3.loan !== '') {
      this.loanText = true;
    }
    if (this.stepThree_3.interest !== '') {
      this.interestText = true;
    }

    if (this.stepThree_3.interestApprove !== '') {
      this.interestApproveText = true;
    }

    let _statustable = false;
    if (this.stepThree_3.requestHousingCredit.length > 0) {
      console.log('0000 this.stepThree_3.requestHousingCredit:', this.stepThree_3.requestHousingCredit);
      for (let index = 0; index < this.stepThree_3.requestHousingCredit.length; index++) {
        const element = this.stepThree_3.requestHousingCredit[index];
        if (element) {
          if (element.loandType === ''
            || element.limitType === ''
            || element.creditLimit === null
            || element.preriod === null
            || element.creditMin === null
            || element.creditMax === null
            || element.creditAvg === null
          ) {
            _statustable = false;
            break;
          } else {
            _statustable = true;
          }
        }

      }
      this.loandetail = _statustable;
    } else {
      this.loandetail = false;
    }

    if (
      this.loandetail === true &&
      this.agreement === true &&
      this.target === true &&
      this.targetOtherDetail === true &&
      this.guaranty === true &&
      this.loanText === true &&
      this.interestText === true &&
      this.interestApproveText === true
    ) {
      // this.saveDraftstatus = "success";
      console.log('form:', this.stepThree_3);
      this.stepThree_3.status = true;
      return true;
    } else {
      this.status_loading = false;
      // alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
      console.log('agreement:', this.agreement);
      console.log('target:', this.target);
      console.log('targetOtherDetail:', this.targetOtherDetail);
      console.log('guaranty:', this.guaranty);
      console.log('loanText:', this.loanText);
      console.log('interestText:', this.interestText);
      console.log('interestApproveText:', this.interestApproveText);
      console.log('err:', this.stepThree_3);
      this.stepThree_3.status = false;
      // this.saveDraftstatus = '';
      return true;
    }
  }
  init_value() {

    console.log('considerconsiderconsider:', this.stepThree_3.consider);
    switch (this.stepThree_3.agreement[0].flag) {
      case true:
        const item = this.index_to_create_template.indexOf('MOU');
        if (item === -1) {
          this.index_to_create_template.splice(0, 0, 'MOU');
        }
        break;
      case false:
        for (
          let index = 0;
          index < this.index_to_create_template.length;
          index++
        ) {
          if (this.index_to_create_template[index] === 'MOU') {
            this.index_to_create_template.splice(index, 1);
          }
        }
        // for (let p = 0; p < this.stepThree_3.consider.applicant.length; p++) {
        //   this.stepThree_3.consider.applicant[p].detail['MOU'] = '';
        // }
        // for (let p = 0; p < this.stepThree_3.consider.finan.length; p++) {
        //   this.stepThree_3.consider.finan[p].detail['MOU'] = '';
        // }
        // for (let p = 0; p < this.stepThree_3.consider.status.length; p++) {
        //   this.stepThree_3.consider.status[p].detail['MOU'] = '';
        // }
        // for (let p = 0; p < this.stepThree_3.consider.other.length; p++) {
        //   this.stepThree_3.consider.other[p].detail['MOU'] = '';
        // }
        break;
    }

    switch (this.stepThree_3.agreement[1].flag) {
      case true:
        const item = this.index_to_create_template.indexOf('Non-MOU');
        if (item === -1) {
          // this.index_to_create_template.push('Non-MOU');

          if (this.index_to_create_template.length === 0) {
            this.index_to_create_template.splice(0, 0, 'Non-MOU');
            break;
          } else {
            for (
              let index = 0;
              index < this.index_to_create_template.length;
              index++
            ) {
              if (this.index_to_create_template[index] === 'MOU') {
                this.index_to_create_template.splice(index + 1, 0, 'Non-MOU');
                break;
              } else {
                this.index_to_create_template.splice(index, 0, 'Non-MOU');
                break;
              }
            }
          }

          console.log(
            'this.index_to_create_template',
            this.index_to_create_template
          );
        }
        break;
      case false:
        for (
          let index = 0;
          index < this.index_to_create_template.length;
          index++
        ) {
          if (this.index_to_create_template[index] === 'Non-MOU') {
            this.index_to_create_template.splice(index, 1);
          }
        }
        // for (let p = 0; p < this.stepThree_3.consider.applicant.length; p++) {
        //   this.stepThree_3.consider.applicant[p].detail['Non-MOU'] = '';
        // }
        // for (let p = 0; p < this.stepThree_3.consider.finan.length; p++) {
        //   this.stepThree_3.consider.finan[p].detail['Non-MOU'] = '';
        // }
        // for (let p = 0; p < this.stepThree_3.consider.status.length; p++) {
        //   this.stepThree_3.consider.status[p].detail['Non-MOU'] = '';
        // }
        // for (let p = 0; p < this.stepThree_3.consider.other.length; p++) {
        //   this.stepThree_3.consider.other[p].detail['Non-MOU'] = '';
        // }
        break;
    }

    switch (this.stepThree_3.agreement[2].flag) {
      case true:
        const item = this.index_to_create_template.indexOf('Undefined');
        if (item === -1) {
          // this.index_to_create_template.push('Undefined');
          this.index_to_create_template.splice(2, 0, 'Undefined');
        }
        break;
      case false:
        for (
          let index = 0;
          index < this.index_to_create_template.length;
          index++
        ) {
          if (this.index_to_create_template[index] === 'Undefined') {
            this.index_to_create_template.splice(index, 1);
          }
        }
        // for (let p = 0; p < this.stepThree_3.consider.applicant.length; p++) {
        //   this.stepThree_3.consider.applicant[p].detail['Undefined'] = '';
        // }
        // for (let p = 0; p < this.stepThree_3.consider.finan.length; p++) {
        //   this.stepThree_3.consider.finan[p].detail['Undefined'] = '';
        // }
        // for (let p = 0; p < this.stepThree_3.consider.status.length; p++) {
        //   this.stepThree_3.consider.status[p].detail['Undefined'] = '';
        // }
        // for (let p = 0; p < this.stepThree_3.consider.other.length; p++) {
        //   this.stepThree_3.consider.other[p].detail['Undefined'] = '';
        // }
        break;
    }

    if (this.stepThree_3.agreement[0].flag === false) {

      for (let k = 0; k < this.stepThree_3.consider.applicant.length; k++) {
        delete this.stepThree_3.consider.applicant[k].detail[
          this.stepThree_3.agreement[0].topic
        ];
      }
      for (let k = 0; k < this.stepThree_3.consider.finan.length; k++) {
        delete this.stepThree_3.consider.finan[k].detail[
          this.stepThree_3.agreement[0].topic
        ];
      }
      for (let k = 0; k < this.stepThree_3.consider.status.length; k++) {
        delete this.stepThree_3.consider.status[k].detail[
          this.stepThree_3.agreement[0].topic
        ];
      }
      for (let k = 0; k < this.stepThree_3.consider.other.length; k++) {
        delete this.stepThree_3.consider.other[k].detail[
          this.stepThree_3.agreement[0].topic
        ];
      }
    }

    if (this.stepThree_3.agreement[1].flag === false) {

      for (let k = 0; k < this.stepThree_3.consider.applicant.length; k++) {
        delete this.stepThree_3.consider.applicant[k].detail[
          'Non-MOU'
        ];
      }
      for (let k = 0; k < this.stepThree_3.consider.finan.length; k++) {
        delete this.stepThree_3.consider.finan[k].detail[
          'Non-MOU'
        ];
      }
      for (let k = 0; k < this.stepThree_3.consider.status.length; k++) {
        delete this.stepThree_3.consider.status[k].detail[
          'Non-MOU'
        ];
      }
      for (let k = 0; k < this.stepThree_3.consider.other.length; k++) {
        delete this.stepThree_3.consider.other[k].detail[
          'Non-MOU'
        ];
      }
    }

    if (this.stepThree_3.agreement[2].flag === false) {

      for (let k = 0; k < this.stepThree_3.consider.applicant.length; k++) {
        delete this.stepThree_3.consider.applicant[k].detail[
          'Undefined'
        ];
      }
      for (let k = 0; k < this.stepThree_3.consider.finan.length; k++) {
        delete this.stepThree_3.consider.finan[k].detail[
          'Undefined'
        ];
      }
      for (let k = 0; k < this.stepThree_3.consider.status.length; k++) {
        delete this.stepThree_3.consider.status[k].detail[
          'Undefined'
        ];
      }
      for (let k = 0; k < this.stepThree_3.consider.other.length; k++) {
        delete this.stepThree_3.consider.other[k].detail[
          'Undefined'
        ];
      }
    }


    this.length_header = this.index_to_create_template.length;
    console.log('stepThree_3.consider::', this.stepThree_3.consider);

  }
  changeCheckbox() {
    console.log('checkbox:', this.stepThree_3.agreement);
    console.log('consider', this.stepThree_3.consider);

    switch (this.stepThree_3.agreement[0].flag) {
      case true:
        const item = this.index_to_create_template.indexOf('MOU');
        if (item === -1) {
          // this.index_to_create_template.push('MOU');
          this.index_to_create_template.splice(0, 0, 'MOU');
          this.StepThree.get_step_three_housing(this.stepThree_3.requestId, this.template_manage.pageShow).subscribe(res => {
            this.res_2 = res;
            console.log('res_2:', this.res_2.data);
            if(res['data'] !== null) {
              for (let p = 0; p < this.stepThree_3.consider.applicant.length; p++) {

                if (this.res_2.data.consider.applicant[p] === undefined) {
                  this.stepThree_3.consider.applicant[p].detail['MOU'] = '';
                } else {
                  if (this.res_2.data.consider.applicant[p].detail['MOU'] === undefined) {
                    this.stepThree_3.consider.applicant[p].detail['MOU'] = '';
                  } else {
                    this.stepThree_3.consider.applicant[p].detail['MOU'] = this.res_2.data.consider.applicant[p].detail['MOU'];
  
                  }
                }
              }
              for (let p = 0; p < this.stepThree_3.consider.finan.length; p++) {
                if (this.res_2.data.consider.finan[p] === undefined) {
                  this.stepThree_3.consider.finan[p].detail['MOU'] = '';
                } else {
                  if (this.res_2.data.consider.finan[p].detail['MOU'] === undefined) {
                    this.stepThree_3.consider.finan[p].detail['MOU'] = '';
                  } else {
                    this.stepThree_3.consider.finan[p].detail['MOU'] = this.res_2.data.consider.finan[p].detail['MOU'];
                  }
                }
              }
              for (let p = 0; p < this.stepThree_3.consider.status.length; p++) {
                if (this.res_2.data.consider.status[p] === undefined) {
                  this.stepThree_3.consider.status[p].detail['MOU'] = '';
                } else {
                  if (this.res_2.data.consider.status[p].detail['MOU'] === undefined) {
                    this.stepThree_3.consider.status[p].detail['MOU'] = '';
                  } else {
                    this.stepThree_3.consider.status[p].detail['MOU'] = this.res_2.data.consider.status[p].detail['MOU'];
                  }
                }
              }
              for (let p = 0; p < this.stepThree_3.consider.other.length; p++) {
                if (this.res_2.data.consider.other[p] === undefined) {
                  this.stepThree_3.consider.other[p].detail['MOU'] = '';
                } else {
                  if (this.res_2.data.consider.other[p].detail['MOU'] === undefined) {
                    this.stepThree_3.consider.other[p].detail['MOU'] = '';
                  } else {
                    this.stepThree_3.consider.other[p].detail['MOU'] = this.res_2.data.consider.other[p].detail['MOU'];
                  }
                }
              }
            }
          
          });

        }

        break;

      case false:
        for (
          let index = 0;
          index < this.index_to_create_template.length;
          index++
        ) {
          if (this.index_to_create_template[index] === 'MOU') {
            this.index_to_create_template.splice(index, 1);
          }
        }
        for (let p = 0; p < this.stepThree_3.consider.applicant.length; p++) {
          this.stepThree_3.consider.applicant[p].detail['MOU'] = '';
        }
        for (let p = 0; p < this.stepThree_3.consider.finan.length; p++) {
          this.stepThree_3.consider.finan[p].detail['MOU'] = '';
        }
        for (let p = 0; p < this.stepThree_3.consider.status.length; p++) {
          this.stepThree_3.consider.status[p].detail['MOU'] = '';
        }
        for (let p = 0; p < this.stepThree_3.consider.other.length; p++) {
          this.stepThree_3.consider.other[p].detail['MOU'] = '';
        }
        break;
    }

    switch (this.stepThree_3.agreement[1].flag) {
      case true:
        const item = this.index_to_create_template.indexOf('Non-MOU');
        if (item === -1) {
          // this.index_to_create_template.push('Non-MOU');

          if (this.index_to_create_template.length === 0) {
            this.index_to_create_template.splice(0, 0, 'Non-MOU');
            break;
          } else {
            for (
              let index = 0;
              index < this.index_to_create_template.length;
              index++
            ) {
              if (this.index_to_create_template[index] === 'MOU') {
                this.index_to_create_template.splice(index + 1, 0, 'Non-MOU');
                break;
              } else {
                this.index_to_create_template.splice(index, 0, 'Non-MOU');
                break;
              }
            }
          }

          console.log(
            'this.index_to_create_template',
            this.index_to_create_template
          );
          this.StepThree.get_step_three_housing(this.stepThree_3.requestId, this.template_manage.pageShow).subscribe(res => {
            this.status_loading = false;
            this.res_2 = res;
            console.log('res_2:', this.res_2.data);
            if(res['data'] !== null) {
            for (let p = 0; p < this.stepThree_3.consider.applicant.length; p++) {
              if (this.res_2.data.consider.applicant[p] === undefined) {
                this.stepThree_3.consider.applicant[p].detail['Non-MOU'] = '';
              } else {
                if (this.res_2.data.consider.applicant[p].detail['Non-MOU'] === undefined) {
                  this.stepThree_3.consider.applicant[p].detail['Non-MOU'] = '';
                } else {
                  this.stepThree_3.consider.applicant[p].detail['Non-MOU'] = this.res_2.data.consider.applicant[p].detail['Non-MOU'];
                }
              }
            }
            for (let p = 0; p < this.stepThree_3.consider.finan.length; p++) {
              if (this.res_2.data.consider.finan[p] === undefined) {
                this.stepThree_3.consider.finan[p].detail['Non-MOU'] = '';
              } else {
                if (this.res_2.data.consider.finan[p].detail['Non-MOU'] === undefined) {
                  this.stepThree_3.consider.finan[p].detail['Non-MOU'] = '';
                } else {
                  this.stepThree_3.consider.finan[p].detail['Non-MOU'] = this.res_2.data.consider.finan[p].detail['Non-MOU'];
                }
              }
            }
            for (let p = 0; p < this.stepThree_3.consider.status.length; p++) {
              if (this.res_2.data.consider.status[p] === undefined) {
                this.stepThree_3.consider.status[p].detail['Non-MOU'] = '';
              } else {
                if (this.res_2.data.consider.status[p].detail['Non-MOU'] === undefined) {
                  this.stepThree_3.consider.status[p].detail['Non-MOU'] = '';
                } else {
                  this.stepThree_3.consider.status[p].detail['Non-MOU'] = this.res_2.data.consider.status[p].detail['Non-MOU'];
                }
              }
            }
            for (let p = 0; p < this.stepThree_3.consider.other.length; p++) {
              if (this.res_2.data.consider.other[p] === undefined) {
                this.stepThree_3.consider.other[p].detail['Non-MOU'] = '';
              } else {
                if (this.res_2.data.consider.other[p].detail['Non-MOU'] === undefined) {
                  this.stepThree_3.consider.other[p].detail['Non-MOU'] = '';
                } else {
                  this.stepThree_3.consider.other[p].detail['Non-MOU'] = this.res_2.data.consider.other[p].detail['Non-MOU'];
                }
              }
            }
          }
          });
        }

        break;
      case false:
        for (
          let index = 0;
          index < this.index_to_create_template.length;
          index++
        ) {
          if (this.index_to_create_template[index] === 'Non-MOU') {
            this.index_to_create_template.splice(index, 1);
          }
        }
        for (let p = 0; p < this.stepThree_3.consider.applicant.length; p++) {
          this.stepThree_3.consider.applicant[p].detail['Non-MOU'] = '';
        }
        for (let p = 0; p < this.stepThree_3.consider.finan.length; p++) {
          this.stepThree_3.consider.finan[p].detail['Non-MOU'] = '';
        }
        for (let p = 0; p < this.stepThree_3.consider.status.length; p++) {
          this.stepThree_3.consider.status[p].detail['Non-MOU'] = '';
        }
        for (let p = 0; p < this.stepThree_3.consider.other.length; p++) {
          this.stepThree_3.consider.other[p].detail['Non-MOU'] = '';
        }
        break;
    }

    switch (this.stepThree_3.agreement[2].flag) {
      case true:
        const item = this.index_to_create_template.indexOf('Undefined');
        if (item === -1) {
          // this.index_to_create_template.push('Undefined');
          this.index_to_create_template.splice(3, 0, 'Undefined');
          this.StepThree.get_step_three_housing(this.stepThree_3.requestId, this.template_manage.pageShow).subscribe(res => {
            this.res_2 = res;
            this.status_loading = false;
            console.log('res_2:', this.res_2.data);
            if(res['data'] !== null) {
            for (let p = 0; p < this.stepThree_3.consider.applicant.length; p++) {
              if (this.res_2.data.consider.applicant[p] === undefined) {
                this.stepThree_3.consider.applicant[p].detail['Undefined'] = '';
              } else {
                if (this.res_2.data.consider.applicant[p].detail['Undefined'] === undefined) {
                  this.stepThree_3.consider.applicant[p].detail['Undefined'] = '';
                } else {
                  this.stepThree_3.consider.applicant[p].detail['Undefined'] = this.res_2.data.consider.applicant[p].detail['Undefined'];
                }
              }
            }
            for (let p = 0; p < this.stepThree_3.consider.finan.length; p++) {
              if (this.res_2.data.consider.finan[p] === undefined) {
                this.stepThree_3.consider.finan[p].detail['Undefined'] = '';
              } else {
                if (this.res_2.data.consider.finan[p].detail['Undefined'] === undefined) {
                  this.stepThree_3.consider.finan[p].detail['Undefined'] = '';
                } else {
                  this.stepThree_3.consider.finan[p].detail['Undefined'] = this.res_2.data.consider.finan[p].detail['Undefined'];
                }
              }
            }
            for (let p = 0; p < this.stepThree_3.consider.status.length; p++) {
              if (this.res_2.data.consider.status[p] === undefined) {
                this.stepThree_3.consider.status[p].detail['Undefined'] = '';
              } else {
                if (this.res_2.data.consider.status[p].detail['Undefined'] === undefined) {
                  this.stepThree_3.consider.status[p].detail['Undefined'] = '';
                } else {
                  this.stepThree_3.consider.status[p].detail['Undefined'] = this.res_2.data.consider.status[p].detail['Undefined'];
                }
              }
            }
            for (let p = 0; p < this.stepThree_3.consider.other.length; p++) {
              if (this.res_2.data.consider.other[p] === undefined) {
                this.stepThree_3.consider.other[p].detail['Undefined'] = '';
              } else {
                if (this.res_2.data.consider.other[p].detail['Undefined'] === undefined) {
                  this.stepThree_3.consider.other[p].detail['Undefined'] = '';
                } else {
                  this.stepThree_3.consider.other[p].detail['Undefined'] = this.res_2.data.consider.other[p].detail['Undefined'];
                }
              }
            }
          }
          });
        }

        break;
      case false:
        for (
          let index = 0;
          index < this.index_to_create_template.length;
          index++
        ) {
          if (this.index_to_create_template[index] === 'Undefined') {
            this.index_to_create_template.splice(index, 1);
          }
        }
        for (let p = 0; p < this.stepThree_3.consider.applicant.length; p++) {
          this.stepThree_3.consider.applicant[p].detail['Undefined'] = '';
        }
        for (let p = 0; p < this.stepThree_3.consider.finan.length; p++) {
          this.stepThree_3.consider.finan[p].detail['Undefined'] = '';
        }
        for (let p = 0; p < this.stepThree_3.consider.status.length; p++) {
          this.stepThree_3.consider.status[p].detail['Undefined'] = '';
        }
        for (let p = 0; p < this.stepThree_3.consider.other.length; p++) {
          this.stepThree_3.consider.other[p].detail['Undefined'] = '';
        }
        break;
    }

    console.log('check::', this.index_to_create_template);
    this.length_header = this.index_to_create_template.length;
  }
  autoGrowTextZone(e) {
    e.target.style.height = '0px';
    e.target.style.height = (e.target.scrollHeight + 5) + 'px';
  }
  consider() {
    console.log('consider_consider:', this.stepThree_3.consider);
    // console.log('1::', this.stepThree_3.consider.applicant);
    // console.log('2::', this.stepThree_3.consider.finan);
    // console.log('3::', this.stepThree_3.consider.status);
    // console.log('4::', this.stepThree_3.consider.other);
  }
  changeValue_collateral() {
    if (this.stepThree_3.guaranty[1].flag === false) {
      this.stepThree_3.guaranty[1].child[0].flag = false;
      this.stepThree_3.guaranty[1].child[1].flag = false;
      this.stepThree_3.guaranty[1].child[2].flag = false;
      this.stepThree_3.guaranty[1].child[3].flag = false;
    }

    if (this.stepThree_3.guaranty[2].flag === false) {
      this.stepThree_3.guaranty[2].child[0].flag = false;
      this.stepThree_3.guaranty[2].child[1].flag = false;
      this.stepThree_3.guaranty[2].child[2].flag = false;
      this.stepThree_3.guaranty[2].child[3].flag = false;
      this.stepThree_3.guaranty[2].child[4].flag = false;
      this.stepThree_3.guaranty[2].child[5].flag = false;
      this.stepThree_3.guaranty[2].child[6].flag = false;
    }

    if (this.stepThree_3.guaranty[3].flag === false) {
      this.stepThree_3.guaranty[3].child[0].flag = false;
      this.stepThree_3.guaranty[3].child[1].flag = false;
      this.stepThree_3.guaranty[3].child[2].flag = false;
    }

    if (this.stepThree_3.guaranty[4].flag === false) {
      this.stepThree_3.guaranty[4].child[0].flag = false;
      this.stepThree_3.guaranty[4].child[1].flag = false;
      this.stepThree_3.guaranty[4].child[2].flag = false;
      this.stepThree_3.guaranty[4].child[3].flag = false;
    }

    if (this.stepThree_3.guaranty[7].flag === false) {
      this.stepThree_3.guaranty[7].otherDetail = '';
    }
    this.changeSaveDraft();
  }

  changeValue_Gain() {
    console.log('gain:', this.stepThree_3.target);
    if (this.stepThree_3.target[3].flag === false) {
      this.stepThree_3.target[3].otherDetail = '';
    }
    if (this.stepThree_3.target[0].flag === false) {
      for (
        let index = 0;
        index < this.stepThree_3.target[0].child.length;
        index++
      ) {
        const item = this.stepThree_3.target[0].child[index];
        item.flag = false;
      }
    }
    if (this.stepThree_3.target[1].flag === false) {
      for (
        let index = 0;
        index < this.stepThree_3.target[1].child.length;
        index++
      ) {
        const item = this.stepThree_3.target[1].child[index];
        item.flag = false;
      }
    }
    this.changeSaveDraft();
  }
  change_text_img(index: any) {
    this.stepThree_3.loan = this.stepThree_3.loan;
    this.stepThree_3.interest = this.stepThree_3.interest;
    this.stepThree_3.interestApprove = this.stepThree_3.interestApprove;
    this.stepThree_3.other = this.stepThree_3.other;

    console.log('tfff', this.stepThree_3);
  }
  add_gain_credit(value: any) {
    console.log('value', value);
    if (value === 1) {
      this.stepThree_3.consider.applicant.push({
        id: '',
        topic: '',
        detail: {
          'MOU': '',
          'Non-MOU': '',
          'Undefined': ''
        },
      });
    } else if (value === 2) {
      this.stepThree_3.consider.finan.push({
        id: '',
        topic: '',
        detail: {
          'MOU': '',
          'Non-MOU': '',
          'Undefined': ''
        },
      });
    } else if (value === 3) {
      this.stepThree_3.consider.status.push({
        id: '',
        topic: '',
        detail: {
          'MOU': '',
          'Non-MOU': '',
          'Undefined': ''
        },
      });
    } else if (value === 4) {
      this.stepThree_3.consider.other.push({
        id: '',
        topic: '',
        detail: {
          'MOU': '',
          'Non-MOU': '',
          'Undefined': ''
        },
      });
    }

    console.log('consider:', this.stepThree_3.consider);
  }
  delete_gain(gain: any, data: any, index: any) {
    console.log('del gain::', gain);
    console.log('del data::', data);
    console.log('select::', index);

    switch (gain) {
      case 1:
        this.stepThree_3.consider.applicant.splice(index, 1);
        if (data.id !== '') {
          this.stepThree_3.considerDelete.push(data.id);
        }

        break;
      case 2:
        this.stepThree_3.consider.finan.splice(index, 1);
        if (data.id !== '') {
          this.stepThree_3.considerDelete.push(data.id);
        }

        break;
      case 3:
        this.stepThree_3.consider.status.splice(index, 1);
        if (data.id !== '') {
          this.stepThree_3.considerDelete.push(data.id);
        }

        break;
      case 4:
        this.stepThree_3.consider.other.splice(index, 1);
        if (data.id !== '') {
          this.stepThree_3.considerDelete.push(data.id);
        }

        break;
    }
    console.log('form:', this.stepThree_3.consider);
    console.log('delete::', this.stepThree_3.considerDelete);
    this.changeSaveDraft();
  }

  add(index: any) {
    if (index === 1) {
      // this.data_pic.table_Main.table = this.data;
      this.stepThree_3.requestHousingCredit.push(this.add_data);
      this.add_data = {
        id: '',
        loandType: '',
        limitType: '',
        creditLimit: '',
        preriod: '',
        creditMax: '',
        creditMin: '',
        creditAvg: '',
      };
    }
    console.log(
      'stepThree_2.requestHousingCredit:',
      this.stepThree_3.requestHousingCredit
    );
  }
  delete(index: number, row: any, i: any) {
    console.log('index:', index);
    console.log('row:', row);
    console.log('i:', i);
    // this.stepThree_2.table_Main.table = this.data;
    if (row.id !== '') {
      this.stepThree_3.requestHousingCreditDelete.push(row.id);
    }
    this.stepThree_3.requestHousingCredit.splice(i, 1);

    // for (let k = 20; k < this.stepThree_3.requestHousingCredit.length; k++) {
    //   this.stepThree_3.requestHousingCreditDelete.push(this.stepThree_3.requestHousingCredit[k].id);
    //   this.stepThree_3.requestHousingCredit.splice(k, 1);
    // }
    console.log('stepThree_3.credit:', this.stepThree_3.requestHousingCredit);
    console.log('table.delete:', this.stepThree_3.requestHousingCreditDelete);
    this.changeSaveDraft();
  }

  delete_image(fileIndex, index) {
    console.log('fileIndex:', fileIndex);
    console.log('index:', index);
    if (fileIndex === 1) {
      this.data_pic.objective.uploadFile.splice(index, 1);
      this.image1.splice(index, 1);
      if (this.stepThree_3.requestLoanHousingPicture.file[index].id === '') {
        this.stepThree_3.requestLoanHousingPicture.file.splice(index, 1);
      } else {
        this.stepThree_3.requestLoanHousingPicture.fileDelete.push(this.stepThree_3.requestLoanHousingPicture.file[index].id);
        this.stepThree_3.requestLoanHousingPicture.file.splice(index, 1);
      }
      // console.log("delete",this.file_img1_delete)
      console.log(
        'fileDelete',
        this.stepThree_3.requestLoanHousingPicture.fileDelete
      );
      for (let i = 0; i < this.data_pic.objective.uploadFile.length; i++) {
        this.preview(fileIndex, this.data_pic.objective.uploadFile[i], i);
      }
    } else if (fileIndex === 2) {
      this.data_pic.businessModel.uploadFile.splice(index, 1);
      this.image2.splice(index, 1);
      if (this.stepThree_3.requestInterestHousingPicture.file[index].id === '') {
        this.stepThree_3.requestInterestHousingPicture.file.splice(index, 1);
      } else {
        this.stepThree_3.requestInterestHousingPicture.fileDelete.push(this.stepThree_3.requestInterestHousingPicture.file[index].id);
        this.stepThree_3.requestInterestHousingPicture.file.splice(index, 1);
      }
      console.log(
        'fileDelete',
        this.stepThree_3.requestInterestHousingPicture.fileDelete
      );
      for (let i = 0; i < this.data_pic.businessModel.uploadFile.length; i++) {
        this.preview(fileIndex, this.data_pic.businessModel.uploadFile[i], i);
      }
    } else if (fileIndex === 3) {
      this.data_pic.productCharacteristics.uploadFile.splice(index, 1);
      this.image3.splice(index, 1);
      if (this.stepThree_3.requestInterestApproveHousingPicture.file[index].id === '') {
        this.stepThree_3.requestInterestApproveHousingPicture.file.splice(index, 1);
      } else {
        // tslint:disable-next-line: max-line-length
        this.stepThree_3.requestInterestApproveHousingPicture.fileDelete.push(
          this.stepThree_3.requestInterestApproveHousingPicture.file[index].id);
        this.stepThree_3.requestInterestApproveHousingPicture.file.splice(index, 1);
      }
      console.log(
        'fileDelete',
        this.stepThree_3.requestInterestApproveHousingPicture.fileDelete
      );
      for (
        let i = 0;
        i < this.data_pic.productCharacteristics.uploadFile.length;
        i++
      ) {
        this.preview(
          fileIndex,
          this.data_pic.productCharacteristics.uploadFile[i],
          i
        );
      }
    } else if (fileIndex === 4) {
      this.data_pic.another.uploadFile.splice(index, 1);
      this.image4.splice(index, 1);
      if (this.stepThree_3.requestOtherHousingPicture.file[index].id === '') {
        this.stepThree_3.requestOtherHousingPicture.file.splice(index, 1);
      } else {
        this.stepThree_3.requestOtherHousingPicture.fileDelete.push(this.stepThree_3.requestOtherHousingPicture.file[index].id);
        this.stepThree_3.requestOtherHousingPicture.file.splice(index, 1);
      }
      console.log(
        'fileDelete',
        this.stepThree_3.requestOtherHousingPicture.fileDelete
      );
      for (let i = 0; i < this.data_pic.another.uploadFile.length; i++) {
        this.preview(fileIndex, this.data_pic.another.uploadFile[i], i);
      }
    }

    // console.log("del img1:", this.file_img1);
    // console.log("del img2:", this.file_img2);
    // console.log("del img3:", this.file_img3);
    // console.log("del img4:", this.file_img4);
    this.changeSaveDraft();
  }

  image_click(colId, id) {
    this.showModel = true;
    console.log('colId:', colId);
    console.log('id:', id);
    console.log(document.getElementById('myModal_3_3'));
    document.getElementById('myModal_3_3').style.display = 'block';
    this.imageModal(colId, id);
  }
  image_bdclick() {
    document.getElementById('myModal_3_3').style.display = 'block';
    console.log('2222');
  }

  closeModal() {
    this.showModel = false;
    document.getElementById('myModal_3_3').style.display = 'none';
  }


  image_click3(colId, id) {
    this.showModel = true;
    console.log('colId:', colId);
    console.log('id:', id);
    console.log(document.getElementById('myModal_review'));
    document.getElementById('myModal_review').style.display = 'block';
    this.imageModal(colId, id);
  }
  image_bdclick3() {
    document.getElementById('myModal_review').style.display = 'block';
    console.log('2222');
  }

  closeModal3() {
    this.showModel = false;
    document.getElementById('myModal_review').style.display = 'none';
  }
  // getFileDetails ================================================================
  getFileDetails(fileIndex, event) {
    console.log('fileIndex:', fileIndex);
    console.log('event:', event);
    const file = event.target.files;
    for (let index = 0; index < file.length; index++) {
      const mimeType = file[index].type;
      if (mimeType.match('image/jpeg|image/png') == null) {
        this.message = 'Only images are supported.';
        this.status_loading = false;
        alert(this.message);
        return;
      }
    }
    // this.file_name = file.name;
    // this.file_type = file.type;
    // this.file_size = file.size;
    // this.modifiedDate = file.lastModifiedDate;
    // console.log('file:', file);
    console.log('file.length:', file.length);

    if (fileIndex === 1) {
      console.log('00000000000000000000000000000001');
      for (let index = 0; index < file.length; index++) {
        // this.data_pic.objective.uploadFile.push(event.target.files[index]);
        this.data_pic.objective.uploadFile.push({
          file: event.target.files[index],
          id: '',
        });
      }
      console.log('data_pic:', this.data_pic);
      for (
        let index = 0;
        index < this.data_pic.objective.uploadFile.length;
        index++
      ) {
        this.preview(
          fileIndex,
          this.data_pic.objective.uploadFile[index],
          index
        );
      }
      this.file1_1 = undefined;
      this.file1_2 = undefined;
    } else if (fileIndex === 2) {
      console.log('00000000000000000000000000000002');
      for (let index = 0; index < file.length; index++) {
        // this.data_pic.businessModel.uploadFile.push(event.target.files[index]);
        this.data_pic.businessModel.uploadFile.push({
          file: event.target.files[index],
          id: '',
        });
      }

      for (
        let index = 0;
        index < this.data_pic.businessModel.uploadFile.length;
        index++
      ) {
        this.preview(
          fileIndex,
          this.data_pic.businessModel.uploadFile[index],
          index
        );
        this.file2_1 = undefined;
        this.file2_2 = undefined;
      }
    } else if (fileIndex === 3) {
      console.log('00000000000000000000000000000003');
      for (let index = 0; index < file.length; index++) {
        // this.data_pic.productCharacteristics.uploadFile.push(
        //   event.target.files[index]
        // );
        this.data_pic.productCharacteristics.uploadFile.push({
          file: event.target.files[index],
          id: '',
        });
      }

      for (
        let index = 0;
        index < this.data_pic.productCharacteristics.uploadFile.length;
        index++
      ) {
        this.preview(
          fileIndex,
          this.data_pic.productCharacteristics.uploadFile[index],
          index
        );
      }
      this.file3_1 = undefined;
      this.file3_2 = undefined;
    } else if (fileIndex === 4) {
      console.log('00000000000000000000000000000004');
      for (let index = 0; index < file.length; index++) {
        // this.data_pic.another.uploadFile.push(event.target.files[index]);
        this.data_pic.another.uploadFile.push({
          file: event.target.files[index],
          id: '',
        });
      }

      for (
        let index = 0;
        index < this.data_pic.another.uploadFile.length;
        index++
      ) {
        this.preview(fileIndex, this.data_pic.another.uploadFile[index], index);
      }
      this.file4_1 = undefined;
      this.file4_2 = undefined;
    }
  }

  preview(fileIndex, file, index) {
    const reader = new FileReader();
    // console.log("llll:",this.file_img1)
    if (fileIndex === 1) {
      this.stepThree_3.requestLoanHousingPicture.file = [];
      if (file.file) {
        // console.log("llll:",file)
        reader.readAsDataURL(file.file);
        reader.onload = (_event) => {
          this.image1[index] = reader.result;
          const base64result = this.image1[index].substr(
            this.image1[index].indexOf(',') + 1
          );
          this.stepThree_3.requestLoanHousingPicture.file.push({
            id: file.id,
            fileName: file.file.name,
            fileSize: file.file.size,
            base64File: base64result,
          });
          // console.log('file_img1:', this.file_img1);
        };
      }
    } else if (fileIndex === 2) {
      this.stepThree_3.requestInterestHousingPicture.file = [];
      if (file.file) {
        reader.readAsDataURL(file.file);
        reader.onload = (_event) => {
          this.image2[index] = reader.result;
          const base64result = this.image2[index].substr(
            this.image2[index].indexOf(',') + 1
          );
          this.stepThree_3.requestInterestHousingPicture.file.push({
            id: file.id,
            fileName: file.file.name,
            fileSize: file.file.size,
            base64File: base64result,
          });
        };
      }
    } else if (fileIndex === 3) {
      this.stepThree_3.requestInterestApproveHousingPicture.file = [];
      if (file.file) {
        reader.readAsDataURL(file.file);
        reader.onload = (_event) => {
          this.image3[index] = reader.result;
          const base64result = this.image3[index].substr(
            this.image3[index].indexOf(',') + 1
          );
          this.stepThree_3.requestInterestApproveHousingPicture.file.push({
            id: file.id,
            fileName: file.file.name,
            fileSize: file.file.size,
            base64File: base64result,
          });
        };
      }
    } else if (fileIndex === 4) {
      this.stepThree_3.requestOtherHousingPicture.file = [];
      if (file.file) {
        reader.readAsDataURL(file.file);
        reader.onload = (_event) => {
          this.image4[index] = reader.result;
          const base64result = this.image4[index].substr(
            this.image4[index].indexOf(',') + 1
          );
          this.stepThree_3.requestOtherHousingPicture.file.push({
            id: file.id,
            fileName: file.file.name,
            fileSize: file.file.size,
            base64File: base64result,
          });
        };
      }
    }

    // this.stepThree_3.requestLoanHousingPicture.file = this.file_img1;
    // this.stepThree_3.requestLoanHousingPicture.fileDelete = this.file_img1_delete;

    // this.stepThree_3.requestInterestHousingPicture.file = this.file_img2;
    // this.stepThree_3.requestInterestHousingPicture.fileDelete = this.file_img2_delete;

    // this.stepThree_3.requestInterestApproveHousingPicture.file = this.file_img3;
    // this.stepThree_3.requestInterestApproveHousingPicture.fileDelete = this.file_img3_delete;

    // this.stepThree_3.requestOtherHousingPicture.file = this.file_img4;
    // this.stepThree_3.requestOtherHousingPicture.fileDelete = this.file_img4_delete;

    // console.log('img1:', this.stepThree_2.loan);
    // console.log('img2:', this.stepThree_2.interest);
    // console.log('img3:', this.stepThree_2.interestApprove);
    // console.log('img4:', this.stepThree_2.other);
    this.changeSaveDraft();
  }

  getFileDocument(event) {
    const file = event.target.files[0];
    const mimeType = file.type;
    if (mimeType.match(/pdf\/*/) == null) {
      this.message = 'Only PDF are supported.';
      this.status_loading = false;
      alert(this.message);
      return;
    }
    console.log('11111', file);
    this.file_name = file.name;
    // this.file_type = file.type;
    this.file_size = file.size / 1024;
    // this.modifiedDate = file.lastModifiedDate;
    // console.log('file:', file);

    this.data_pic.documentFile = file;
    // this.document = undefined;
  }

  imageModal(fileIndex, index) {
    console.log(fileIndex);
    console.log(index);
    console.log(this.modalImage);
    console.log(this.modalImagedetail);
    if (fileIndex === 1) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image1;
      this.modalImagedetail = this.data_pic.objective.uploadFile;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 2) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image2;
      this.modalImagedetail = this.data_pic.businessModel.uploadFile;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 3) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image3;
      this.modalImagedetail = this.data_pic.productCharacteristics.uploadFile;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 4) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image4;
      this.modalImagedetail = this.data_pic.another.uploadFile;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    }
  }

  onSelectFile(event) {
    this.FileOverSize = [];
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      const file = event.target.files;
      for (let i = 0; i < filesAmount; i++) {
        console.log('type:', file[i]);
        if (
          // file[i].type === 'image/svg+xml' ||
          file[i].type === 'image/jpeg' ||
          // file[i].type === 'image/raw' ||
          // file[i].type === 'image/svg' ||
          // file[i].type === 'image/tif' ||
          // file[i].type === 'image/gif' ||
          file[i].type === 'image/jpg' ||
          file[i].type === 'image/png' ||
          file[i].type === 'application/doc' ||
          file[i].type === 'application/pdf' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
          file[i].type === 'application/pptx' ||
          file[i].type === 'application/docx' ||
          (file[i].type === 'application/ppt' && file[i].size)
        ) {
          if (file[i].size <= 5000000) {
            // tslint:disable-next-line: prefer-const
            let select = {
              id: '',
              file: file[i],
            };
            this.multiple_file(select, i);
          } else {
            this.FileOverSize.push(file[i].name);
          }
        } else {
          this.status_loading = false;
          alert('File Not Support');
        }

      }

      if (this.FileOverSize.length !== 0) {
        console.log('open modal');
        document.getElementById('openOversize').click();
      }
    }
  }

  multiple_file(file_1, index) {
    // console.log("download");
    const reader = new FileReader();
    if (file_1.file) {
      reader.readAsDataURL(file_1.file);
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');

        this.stepThree_3.requestHousingAttachments.file.push({
          id: file_1.id,
          fileName: file_1.file.name,
          fileSize: file_1.file.size,
          base64File: splitFile[1],
        });
        // console.log("this.urls", this.urls);
        this.dataSource_file = new MatTableDataSource(this.stepThree_3.requestHousingAttachments.file);
        this.hideDownloadFile();
      };
    }
    this.changeSaveDraft();
  }

  delete_mutifile(data: any, index: any): void {
    console.log('data:', data, 'index::', index);
    this.dataSource_file.data.splice(index, 1);
    // console.log('urls:', this.urls);
    // this.urls.splice(index, 1);
    // this.muti_file.splice(index, 1);
    if (data.id !== '') {
      this.stepThree_3.requestHousingAttachments.fileDelete.push(data.id);
    }
    this.dataSource_file = new MatTableDataSource(this.dataSource_file.data);
    this.stepThree_3.requestHousingAttachments.file = this.dataSource_file.data;
    console.log('form dee:', this.stepThree_3.requestHousingAttachments);
    this.changeSaveDraft();
  }
  // ===============================================================================

  downloadFile(pathdata: any) {
    this.status_loading = true;
    const contentType = '';
    const sendpath = pathdata;
    console.log('path', sendpath);
    this.sideBarService.getfile(sendpath).subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        this.status_loading = false;
        const datagetfile = res['data'];
        const b64Data = datagetfile['data'];
        const filename = datagetfile['fileName'];
        console.log('base64', b64Data);
        const config = this.b64toBlob(b64Data, contentType);
        saveAs(config, filename);
      }

    });
  }

  b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const convertbyte = atob(b64Data);
    const byteCharacters = atob(convertbyte);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  downloadFile64(data: any) {
    this.status_loading = true;
    const contentType = '';
    this.status_loading = false;
    const b64Data = data.base64File;
    const filename = data.fileName;
    console.log('base64', b64Data);

    const config = this.base64(b64Data, contentType);
    saveAs(config, filename);
  }

  base64(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  // ngOnDestroy(): void {
  //   const data = this.sideBarService.getsavePage();
  //   let saveInFoStatus = false;
  //   saveInFoStatus = data.saveStatusInfo;
  //   if (saveInFoStatus === true) {
  //     this.outOfPageSave = true;
  //     this.linkTopage = data.goToLink;
  //     console.log('ngOnDestroy', this.linkTopage);
  //     this.saveDraft();
  //     this.sideBarService.resetSaveStatusInfo();
  //   }
  // }

  hideDownloadFile() {
    if (this.dataSource_file.data.length > 0) {
      this.downloadfile_by = [];

      for (let index = 0; index < this.dataSource_file.data.length; index++) {
        const element = this.dataSource_file.data[index];
        console.log('ele', element);

        if ('path' in element) {
          this.downloadfile_by.push('path');
        } else {
          this.downloadfile_by.push('base64');
        }
      }
    }
    console.log('by:', this.downloadfile_by);
  }

  // ngDoCheck() {
  //   if (this.openDoCheckc === true) {
  //     if (this.template_manage.caaStatus !== 'show') {
  //       this.pageChange = this.sideBarService.changePageGoToLink();
  //       if (this.pageChange) {
  //         // this.getNavbar();
  //         const data = this.sideBarService.getsavePage();
  //         let saveInFoStatus = false;
  //         saveInFoStatus = data.saveStatusInfo;
  //         if (saveInFoStatus === true) {
  //           this.outOfPageSave = true;
  //           this.linkTopage = data.goToLink;
  //           this.saveDraft('navbar', null);
  //           this.sideBarService.resetSaveStatu();
  //         }
  //       }
  //     }
  //   }
  // }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate request');
    const subject = new Subject<boolean>();
    const status = this.sideBarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef =  this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        if (data.returnStatus === true) {
          const raturnStatus = this.saveDraft();
          console.log('request raturnStatus', raturnStatus);
            if (raturnStatus) {
              console.log('raturnStatus T', this.saveDraftstatus);
              subject.next(true);
            } else {
              console.log('raturnStatus F', this.saveDraftstatus);
              localStorage.setItem('check', this.stepPage);
              subject.next(false);
            }
        } else if (data.returnStatus === false) {
          this.sideBarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          localStorage.setItem('check', this.stepPage);
          subject.next(false);
        }
      });
      return subject;
    } else {
      return true;
    }
  }
}
