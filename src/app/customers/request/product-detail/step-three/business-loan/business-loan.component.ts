import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { FormControl, FormGroup } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';
import { StepThreeService } from '../../../../../services/request/product-detail/step-three/step-three.service';
import { RequestService } from '../../../../../services/request/request.service';
import { Router, NavigationEnd } from '@angular/router';
// import { StepThreeComponent } from '../step-three.component';
// import { element } from '@angular/core/src/render';
import Swal from 'sweetalert2';
import { saveAs } from 'file-saver';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogSaveStatusComponent } from '../../../dialog/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from '../../../../../services/configGuard/config-guard.service';


export interface FileList {
  fileName: string;
  fileSize: string;
  base64File: string;
}

const File_data: FileList[] = [];


@Component({
  selector: 'app-business-loan',
  templateUrl: './business-loan.component.html',
  styleUrls: ['./business-loan.component.scss'],
})
export class BusinessLoanComponent implements OnInit, CanComponentDeactivate {
  @Input() template_manage = { role: null, validate: null, caaStatus: null, pageShow: null };
  @Output() saveStatus = new EventEmitter<boolean>();

  @ViewChild('autosize') autosize: CdkTextareaAutosize;

  status_loading = true;
  header = [
    {
      name: 'loandType',
      value: 'ประเภทสินเชื่อ',
    },
    {
      name: 'limitType',
      value: 'ประเภทวงเงิน',
    },
    {
      name: 'limit',
      value: 'วงเงินสินเชื่อสูงสุด (ลบ.)',
    },
    {
      name: 'preriod',
      value: 'ระยะเวลาให้กู้สูงสุด (ปี)',
    },
  ];
  sub_header = [
    {
      name: 'min',
      value: 'Min',
    },
    {
      name: 'max',
      value: 'Max',
    },
    {
      name: 'avg',
      value: 'Average',
    },
  ];
  all_header = [
    {
      name: 'loandType',
      value: 'ประเภทสินเชื่อ',
    },
    {
      name: 'limitType',
      value: 'ประเภทวงเงิน',
    },
    {
      name: 'limit',
      value: 'วงเงินสินเชื่อสูงสุด (ลบ.)',
    },
    {
      name: 'preriod',
      value: 'ระยะเวลาให้กู้สูงสุด (ปี)',
    },
    {
      name: 'min',
      value: 'Min',
    },
    {
      name: 'max',
      value: 'Max',
    },
    {
      name: 'avg',
      value: 'Average',
    },
  ];

  add_data: any = {
    id: '',
    loandType: '',
    limitType: '',
    limit: '',
    preriod: '',
    max: '',
    min: '',
    avg: '',
  };
  _validate_check = null;
  results: any;
  modifiedDate: any;

  saveDraftstatus = null;
  outOfPageSave = false;
  linkTopage = null;
  pageChange = null;
  openDoCheckc = false;
  showCircle_colId = null;
  showCircle_id = null;
  enter = 3;



  stepThree: any = {
    targetDetail: [
      {
        id: '',
        targetType: 'sSME',
        detail: '',
        loanDelete: [],
        loan: [],
      },
      {
        id: '',
        targetType: 'SME-M',
        detail: '',
        loanDelete: [],
        loan: [],
      },
      {
        id: '',
        targetType: 'SME-L',
        detail: '',
        loanDelete: [],
        loan: [],
      },
      {
        id: '',
        targetType: 'Corporate',
        detail: '',
        loanDelete: [],
        loan: [],
      },
    ],
    objective: {
      objectiveDetail: null,
      uploadFile: [],
    },
    businessModel: {
      businessModelDetail: null,
      uploadFile: [],
    },
    productCharacteristics: {
      productCharacteristicsDetail: null,
      uploadFile: [],
    },
    another: {
      anotherDetail: null,
      uploadFile: [],
    },

    documentFile: [],
  };
  file1_1: any;
  file1_2: any;
  file2_1: any;
  file2_2: any;
  file3_1: any;
  file3_2: any;
  file4_1: any;
  file4_2: any;
  document: any;
  file_name: any;
  file_size: number;

  imgURL: any;
  image1 = [];
  image2 = [];
  image3 = [];
  image4 = [];
  showImage1 = false;
  message = null;

  // Model
  showModel = false;
  modalmainImageIndex = 0;
  modalmainImagedetailIndex = 0;
  indexSelect = 0;
  modalImage = [];
  modalImagedetail = [];
  fileValue = 0;

  urls: any = [];

  index_to_create_template: any = [];

  sSME = false;
  SME_M = false;
  SME_L = false;
  Corporate = false;
  checkbok_goal: any = {
    sSME: false,
    SME_M: false,
    SME_L: false,
    Corporate: false,
  };

  displayedColumns_file: string[] = ['fileName', 'fileSize', 'delete'];
  dataSource_file: MatTableDataSource<FileList>;

  length_header: any = 0;

  gain_applicant: any = {
    id: '',
    topic: '',
    detail: {},
  };

  get_data: any;
  group_data: any;

  base64Image: any;
  fileName: any;

  muti_file: any;
  multi_fileName: any;

  requestId: any;
  res_2: any;
  file_img1: any = [];
  file_img2: any = [];
  file_img3: any = [];
  file_img4: any = [];

  file_img1_delete: any = [];
  file_img2_delete: any = [];
  file_img3_delete: any = [];
  file_img4_delete: any = [];
  // ********************************************formData************************************************* */
  text = {
    sSME: '',
    SME_M: '',
    SME_L: '',
    Corporate: '',
  };
  formData: any = {
    requestId: '',
    status: false,
    target: [
      {
        topic: 'sSME',
        flag: false,
      },
      {
        topic: 'SME-M',
        flag: false,
      },
      {
        topic: 'SME-L',
        flag: false,
      },
      {
        topic: 'Corporate',
        flag: false,
      },
    ],
    guaranty: [
      {
        topic: 'เงินฝาก ',
        flag: false,
        child: [],
      },
      {
        topic: 'ตราสารหนี้',
        flag: false,
        child: [
          {
            topic: 'พันธบัตร หุ้นกู้ หรือตั๋วเงิน',
            flag: false,
          },
          {
            topic: 'หุ้นบุริมสิทธิหรือหุ้นสามัญ',
            flag: false,
          },
          {
            topic: 'หน่วยลงทุนทุกประเภทกองทุนรวม',
            flag: false,
          },
          {
            topic: 'หน่วยลงทุนประเภทกองทุนเปิด',
            flag: false,
          },
        ],
      },
      {
        topic: 'อสังหาริมทรัพย์',
        flag: false,
        child: [
          {
            topic: 'ที่ดินว่างเปล่า',
            flag: false,
          },
          {
            topic: 'ที่ดินว่างเปล่าที่มีสภาพคล่องต่ำ',
            flag: false,
          },
          {
            topic: 'ที่ดินในนิคมอุตสาหกรรม',
            flag: false,
          },
          {
            topic: 'ที่ดินพร้อมสิ่งปลูกสร้าง',
            flag: false,
          },
          {
            topic: 'รรมสิทธิ์อาคารชุด',
            flag: false,
          },
          {
            topic: 'อู่ซ่อมเรือ',
            flag: false,
          },
          {
            topic: 'อสังหาริมทรัพย์อื่นๆ',
            flag: false,
          },
        ],
      },
      {
        topic: 'สินค้า/เครื่องจักร/เรือ',
        flag: false,
        child: [
          {
            topic: 'เครื่องจักร',
            flag: false,
          },
          {
            topic: 'สต็อคสินค้าในคลังสินค้า',
            flag: false,
          },
          {
            topic: 'เรือ',
            flag: false,
          },
        ],
      },
      {
        topic: 'การค้ำประกัน',
        flag: false,
        child: [
          {
            topic: 'หนังสือค้ำประกัน Standby Letter of Credit',
            flag: false,
          },
          {
            topic: 'การค้ำประกันโดยกระทรวงการคลัง',
            flag: false,
          },
          {
            topic: 'การค้ำประกันโดยสถาบันการเงิน',
            flag: false,
          },
          {
            topic: 'การค้ำประกันโดยบรรษัทสินเชื่ออุตสาหกรรมขนาดย่อม',
            flag: false,
          },
        ],
      },
      {
        topic: 'โอนสิทธิการเช่า',
        flag: false,
        child: [],
      },
      {
        topic: 'บุคคลค้ำประกัน',
        flag: false,
        child: [],
      },
      {
        topic: 'อื่นๆ',
        flag: false,
        child: [],
        otherDetail: 'other text',
      },
    ],
    loan: {
      file: [],
      fileDelete: [],
    },
    loanText: '',
    interest: {
      file: [],
      fileDelete: [],
    },
    interestText: '',
    interest_approve: {
      file: [],
      fileDelete: [],
    },
    interestApproveText: '',
    other: {
      file: [],
      fileDelete: [],
    },
    otherText: '',
    attachments: {
      file: [],
      fileDelete: [],
    },
    targetDetailDelete: [],
    targetDetail: [],
    considerDelete: [],
    consider: {
      applicant: [],
      finan: [],
      status: [],
      other: [],
    },
    conclusions: null
  };

  // ********************************************formData************************************************* */
  consider: any = {
    applicant: [
      {
        id: null,
        topic: 'ประสบการณ์',
        detail: {
          Corporate: '',
          sSME: '',
          'SME-M': '',
          'SME-L': '',
        },
      },
      {
        id: null,
        topic: 'อายุผู้กู้',
        detail: {
          Corporate: '',
          sSME: '',
          'SME-M': '',
          'SME-L': '',
        },
      },
      {
        id: null,
        topic: 'QCA Rating/Credit Rating',
        detail: {
          Corporate: '',
          sSME: '',
          'SME-M': '',
          'SME-L': '',
        },
      },
      {
        id: null,
        topic: 'KTB Grade',
        detail: {
          Corporate: '',
          sSME: '',
          'SME-M': '',
          'SME-L': '',
        },
      },
      {
        id: null,
        topic: 'KTB Color',
        detail: {
          Corporate: '',
          sSME: '',
          'SME-M': '',
          'SME-L': '',
        },
      },
    ],
    finan: [
      {
        id: null,
        topic: 'DSCR',
        detail: {
          Corporate: '',
          sSME: '',
          'SME-M': '',
          'SME-L': '',
        },
      },
      {
        id: null,
        topic: 'D/E Ratio',
        detail: {
          Corporate: '',
          sSME: '',
          'SME-M': '',
          'SME-L': '',
        },
      },
      {
        id: null,
        topic: 'กําไรสุทธิ',
        detail: {
          Corporate: '',
          sSME: '',
          'SME-M': '',
          'SME-L': '',
        },
      },
      {
        id: null,
        topic: 'ส่วนทุนของผู้ถือหุ้น',
        detail: {
          Corporate: '',
          sSME: '',
          'SME-M': '',
          'SME-L': '',
        },
      },
      {
        id: null,
        topic: 'ทุนจดทะเบียน',
        detail: {
          Corporate: '',
          sSME: '',
          'SME-M': '',
          'SME-L': '',
        },
      },
    ],
    status: [{
      id: null,
      topic: 'EWS Risk Level (สําหรับลูกค้าเดิม)',
      detail: {
        Corporate: '',
        sSME: '',
        'SME-M': '',
        'SME-L': '',
      },
    },

    {
      id: null,
      topic: 'White List และ AML List',
      detail: {
        Corporate: '',
        sSME: '',
        'SME-M': '',
        'SME-L': '',
      },
    },

    {
      id: null,
      topic: 'การปรับโครงสร้างหนี้',
      detail: {
        Corporate: '',
        sSME: '',
        'SME-M': '',
        'SME-L': '',
      },
    },

    {
      id: null,
      topic: 'การเปลียนผู้ตรวจสอบบัญา',
      detail: {
        Corporate: '',
        sSME: '',
        'SME-M': '',
        'SME-L': '',
      },
    },
    {
      id: null,
      topic: 'การฟ้องร้องเกี่ยวกับการเงิน',
      detail: {
        Corporate: '',
        sSME: '',
        'SME-M': '',
        'SME-L': '',
      },
    }, {
      id: null,
      topic: 'การตรวจสอบเครดิตบูโร',
      detail: {
        Corporate: '',
        sSME: '',
        'SME-M': '',
        'SME-L': '',
      },
    }
    ],
    other: [
      {
        id: null,
        topic: 'Core Asset',
        detail: {
          Corporate: '',
          sSME: '',
          'SME-M': '',
          'SME-L': '',
        },
      },
    ],
  };

  target = false;
  targetDetail = false;
  guaranty = false;
  loanText = false;
  interestText = false;
  interestApproveText = false;
  otherText = false;

  next_page: any;
  parentRequestId: any = [];
  // textareaForm = new FormGroup({
  //   loanText: new FormControl(this.formData.loanText),
  //   interestText: new FormControl(this.formData.interestText),
  //   interestApproveText: new FormControl(this.formData.interestApproveText),
  //   otherText: new FormControl(this.formData.otherText),
  // });

  FileOverSize: any;
  repeat = false;
  constructor(
    private router: Router,
    private StepThree: StepThreeService,
    private sidebarService: RequestService,
    public dialog: MatDialog,
    // private CheckPage: StepThreeComponent,
  ) {
    this.dataSource_file = new MatTableDataSource(File_data);
    // localStorage.setItem("requestId", "1");
    // this.requestId = localStorage.getItem("requestId");
  }

  stepPage: any;
  checkLocal: any;
  pageLast: any;
  downloadfile_by = [];

  ngOnInit() {
    window.scrollTo(0, 0);
    this.openDoCheckc = true;
    this.stepPage = localStorage.getItem('check');
    this.checkLocal = localStorage.getItem('check');
    this.pageLast = (sessionStorage.getItem('page'));
    console.log('this.stepPage', this.stepPage);
    this.saveDraftstatus = null;
    if (localStorage) {
      this.requestId = localStorage.getItem('requestId');
    } else {
      this.status_loading = false;
    }
    this.sidebarService.sendEvent();
    this.sidebarService.statusSibar(localStorage.getItem('requestId')).subscribe(check => {
      console.log('ckeck ทบทวน', check);
      if (check['data'] !== null) {
        this.repeat = check['data']['repeat'];
      } else {
        this.repeat = false;
      }
    });
    this.dataSource_file = new MatTableDataSource(File_data);
    this.StepThree.getProduct_detail_step_three(this.requestId, this.template_manage.pageShow).subscribe(
      (res) => {
        // this.get_data = res;
        if (res['status'] === 'success') {
          this.status_loading = false;
          this.parentRequestId = res['parentRequestId'];
        }
        if (res['data'] == null || res['data'] === []) {
          // this.formData = this.formData;
          this.group_data = this.formData;
          this.requestId = localStorage.getItem('requestId');
          console.log('new data:', this.group_data);
          this._validate_check = res['validate'];
          // ============================check consider default==============================================
          // ====applicant======
          if (this.formData.consider.applicant.length === 0) {
            for (let index = 0; index < this.consider.applicant.length; index++) {
              this.formData.consider.applicant.push(this.consider.applicant[index]);
            }
          } else {
            this.formData.consider.applicant = this.group_data.consider.applicant;
          }
          // ====finan======
          if (this.formData.consider.finan.length === 0) {
            for (let index = 0; index < this.consider.finan.length; index++) {
              this.formData.consider.finan.push(this.consider.finan[index]);
            }
          } else {
            this.formData.consider.finan = this.group_data.consider.finan;
          }
          // ====status======
          if (this.formData.consider.status.length === 0) {
            for (let index = 0; index < this.consider.status.length; index++) {
              this.formData.consider.status.push(this.consider.status[index]);
            }
          } else {
            this.formData.consider.status = this.group_data.consider.status;
          }
          // ====other======
          if (this.formData.consider.other.length === 0) {
            for (let index = 0; index < this.consider.other.length; index++) {
              this.formData.consider.other.push(this.consider.other[index]);
            }
          } else {
            this.formData.consider.other = this.group_data.consider.other;
          }
        } else {
          this.group_data = res['data'];
          console.log('get data:', this.group_data);
          this._validate_check = res['validate'];
        }

        this.get_data_res();

        // console.log('data:', this.get_data.data);

        // this.changeValue();
      }
    );


  }

  autoGrowTextZone(e) {
    e.target.style.height = '0px';
    e.target.style.height = (e.target.scrollHeight + 5) + 'px';
  }
  sendSaveStatus() {
    if (this.saveDraftstatus === 'success') {
      this.saveStatus.emit(true);
    } else {
      this.saveStatus.emit(false);
    }
    // console.log('00007 =>> ', this.saveDraftstatus);
  }

  changeSaveDraft() {
    this.saveDraftstatus = null;
    this.sendSaveStatus();
    this.sidebarService.inPageStatus(true);
  }
  autogrow(index: any) {
    if (index === 1) {
      const loanText = document.getElementById('loanText');
      loanText.style.overflow = 'hidden';
      loanText.style.height = 'auto';
      loanText.style.height = loanText.scrollHeight + 'px';
    }
    if (index === 2) {
      const interestText = document.getElementById('interestText');
      interestText.style.overflow = 'hidden';
      interestText.style.height = 'auto';
      interestText.style.height = interestText.scrollHeight + 'px';
    }
    if (index === 3) {
      const interestApproveText = document.getElementById('interestApproveText');
      interestApproveText.style.overflow = 'hidden';
      interestApproveText.style.height = 'auto';
      interestApproveText.style.height = interestApproveText.scrollHeight + 'px';
    }
    if (index === 4) {
      const otherText = document.getElementById('otherText');
      otherText.style.overflow = 'hidden';
      otherText.style.height = 'auto';
      otherText.style.height = otherText.scrollHeight + 'px';
    }
  }
  saveDraft(page?, action?) {
    const returnUpdateData = new Subject<boolean>();
    const result = this.check_text_before();
    this.status_loading = true;

    if (this.template_manage.pageShow === 'summary') {
      this.formData.conclusions = true;
    } else {
      this.formData.conclusions = null;
    }

    if ((this.formData.guaranty[7].otherDetail === ''
    || this.formData.guaranty[7].otherDetail === null)
      && this.formData.guaranty[7].flag === true) {
        this.status_loading = false;
        this.saveDraftstatus = 'fail';
        alert('กรุณากรอกช่องอื่นๆในประเภทหลักประกัน');
        return false;
      } else if (result === true) {
      // console.log('start::', this.get_data);
      console.log('data to save bu:', this.formData['guaranty']);
      this.StepThree.update_step_three_business(this.formData).subscribe(
        (res) => {
          console.log(res);
          if (res['status'] === 'success') {
            this.saveDraftstatus = 'success';
            this.downloadfile_by = [];
            this.status_loading = false;
            returnUpdateData.next(false);
            this.sidebarService.inPageStatus(false);
            setTimeout(() => {
              this.saveDraftstatus = 'fail';
            }, 3000);
            this.sidebarService.sendEvent();
            this.sendSaveStatus();

            console.log('rs::', res);
            this.StepThree.getProduct_detail_step_three(this.requestId, this.template_manage.pageShow).subscribe(
              // tslint:disable-next-line: no-shadowed-variable
              (res) => {
                this.get_data = res;
                this.group_data = this.get_data.data;
                if (this.group_data == null || this.group_data === []) {
                  // this.formData = this.formData;
                  this.group_data = this.formData;
                  this.requestId = localStorage.getItem('requestId');
                  console.log('new data:', this.group_data);
                } else {
                  this.group_data = this.get_data.data;
                  console.log('get data:', this.group_data);
                }
                this.dataSource_file = new MatTableDataSource(File_data);
                this.formData.attachments.file = [];

                this.formData.loan.file = [];
                this.formData.interest.file = [];
                this.formData.interest_approve.file = [];
                this.formData.other.file = [];

                this.formData.loan.fileDelete = [];
                this.formData.interest.fileDelete = [];
                this.formData.interest_approve.fileDelete = [];
                this.formData.other.fileDelete = [];

                this.image1 = [];
                this.image2 = [];
                this.image3 = [];
                this.image4 = [];

                this.stepThree.objective.uploadFile = [];
                this.stepThree.businessModel.uploadFile = [];
                this.stepThree.productCharacteristics.uploadFile = [];
                this.stepThree.another.uploadFile = [];

                console.log(' this.dataSource_file::', this.dataSource_file.data);
                console.log('  this.urls::', this.formData.attachments.file);

                this.get_data_res();
              });
          } else if (res['status'] === 'fail') {
            // alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
            this.status_loading = false;
            alert(res['message']);
            console.log('message::', res['message']);
            returnUpdateData.next(false);
          }
        },
        (err) => {
          console.log(err);
          // alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
          this.status_loading = false;
          alert(err['message']);
          returnUpdateData.next(false);
        }
      );
      returnUpdateData.next(false);
      if (returnUpdateData) {
        console.log('333 raturnStatus T');
      } else {
        console.log('333 raturnStatus F');
      }
      return returnUpdateData;
    }
  }
  nextpage(action) {
    this.StepThree.CheckNextPage(localStorage.getItem('check'), action);
  }

  viewNext() {
    this.StepThree.CheckNextPage(localStorage.getItem('check'), 'next');
  }

  check_text_before() {
    this.formData.targetDetail = [];
    for (let index = 0; index < this.stepThree.targetDetail.length; index++) {
      this.formData.targetDetail.push(this.stepThree.targetDetail[index]);
    }

    if (this.formData.target[0].flag === false) {
      // console.log('targetDetail', this.formData.targetDetail);
      for (let index = 0; index < this.formData.targetDetail.length; index++) {
        if (
          this.formData.targetDetail[index].targetType ===
          this.formData.target[0].topic
        ) {
          // console.log('index', this.stepThree.targetDetail);
          this.formData.targetDetail.splice(index, 1);
          // console.log('check :', this.formData.targetDetail);
        }
      }
      for (let k = 0; k < this.formData.consider.applicant.length; k++) {
        delete this.formData.consider.applicant[k].detail[
          this.formData.target[0].topic
        ];
      }
      for (let k = 0; k < this.formData.consider.finan.length; k++) {
        delete this.formData.consider.finan[k].detail[
          this.formData.target[0].topic
        ];
      }
      for (let k = 0; k < this.formData.consider.status.length; k++) {
        delete this.formData.consider.status[k].detail[
          this.formData.target[0].topic
        ];
      }
      for (let k = 0; k < this.formData.consider.other.length; k++) {
        delete this.formData.consider.other[k].detail[
          this.formData.target[0].topic
        ];
      }
    }

    if (this.formData.target[1].flag === false) {
      console.log('targetDetail', this.formData.targetDetail);
      for (let index = 0; index < this.formData.targetDetail.length; index++) {
        if (
          this.formData.targetDetail[index].targetType ===
          this.formData.target[1].topic
        ) {
          // console.log('index', this.formData.targetDetail[index].targetType);
          this.formData.targetDetail.splice(index, 1);
          // console.log('check :', this.formData.targetDetail);
        }
      }
      for (let k = 0; k < this.formData.consider.applicant.length; k++) {
        delete this.formData.consider.applicant[k].detail[
          this.formData.target[1].topic
        ];
      }
      for (let k = 0; k < this.formData.consider.finan.length; k++) {
        delete this.formData.consider.finan[k].detail[
          this.formData.target[1].topic
        ];
      }
      for (let k = 0; k < this.formData.consider.status.length; k++) {
        delete this.formData.consider.status[k].detail[
          this.formData.target[1].topic
        ];
      }
      for (let k = 0; k < this.formData.consider.other.length; k++) {
        delete this.formData.consider.other[k].detail[
          this.formData.target[1].topic
        ];
      }
    }

    if (this.formData.target[2].flag === false) {
      // console.log('targetDetail', this.formData.targetDetail);
      for (let index = 0; index < this.formData.targetDetail.length; index++) {
        if (
          this.formData.targetDetail[index].targetType ===
          this.formData.target[2].topic
        ) {
          // console.log('index', this.formData.targetDetail[index].targetType);
          this.formData.targetDetail.splice(index, 1);
          // console.log('check :', this.formData.targetDetail);
        }
      }
      for (let k = 0; k < this.formData.consider.applicant.length; k++) {
        delete this.formData.consider.applicant[k].detail[
          this.formData.target[2].topic
        ];
      }
      for (let k = 0; k < this.formData.consider.finan.length; k++) {
        delete this.formData.consider.finan[k].detail[
          this.formData.target[2].topic
        ];
      }
      for (let k = 0; k < this.formData.consider.status.length; k++) {
        delete this.formData.consider.status[k].detail[
          this.formData.target[2].topic
        ];
      }
      for (let k = 0; k < this.formData.consider.other.length; k++) {
        delete this.formData.consider.other[k].detail[
          this.formData.target[2].topic
        ];
      }
    }

    if (this.formData.target[3].flag === false) {
      // console.log('targetDetail', this.formData.targetDetail);
      for (let index = 0; index < this.formData.targetDetail.length; index++) {
        if (
          this.formData.targetDetail[index].targetType ===
          this.formData.target[3].topic
        ) {
          // console.log('index', this.formData.targetDetail[index].targetType);
          this.formData.targetDetail.splice(index, 1);
        }
      }
      for (let k = 0; k < this.formData.consider.applicant.length; k++) {
        delete this.formData.consider.applicant[k].detail[
          this.formData.target[3].topic
        ];
      }
      for (let k = 0; k < this.formData.consider.finan.length; k++) {
        delete this.formData.consider.finan[k].detail[
          this.formData.target[3].topic
        ];
      }
      for (let k = 0; k < this.formData.consider.status.length; k++) {
        delete this.formData.consider.status[k].detail[
          this.formData.target[3].topic
        ];
      }
      for (let k = 0; k < this.formData.consider.other.length; k++) {
        delete this.formData.consider.other[k].detail[
          this.formData.target[3].topic
        ];
      }
    }

    this.target = false;
    this.guaranty = false;
    this.loanText = false;
    this.targetDetail = true;
    this.interestText = false;
    this.interestApproveText = false;
    this.otherText = false;

    for (let index = 0; index < this.formData.target.length; index++) {
      if (this.formData.target[index].flag === true) {
        this.target = true;
        break;
      }
    }

    for (let index = 0; index < this.formData.guaranty.length; index++) {
      if (
        this.formData.guaranty[index].flag === true &&
        this.formData.guaranty[index].child.length === 0
      ) {
        this.guaranty = true;
        break;
      } else if (
        this.formData.guaranty[index].flag === true &&
        this.formData.guaranty[index].child.length !== 0
      ) {
        for (let i = 0; i < this.formData.guaranty[index].child.length; i++) {
          if (this.formData.guaranty[index].child[i].flag === true) {
            this.guaranty = true;
            break;
          }
        }
      }
    }

    for (let index = 0; index < this.formData.targetDetail.length; index++) {
      if (this.formData.targetDetail[index].detail === '') {
        console.log('this.formData.targetDetail', index);
        this.targetDetail = false;
        break;
      }
    }

    if (this.formData.loanText !== '') {
      this.loanText = true;
    }
    if (this.formData.interestText !== '') {
      this.interestText = true;
    }

    if (this.formData.interestApproveText !== '') {
      this.interestApproveText = true;
    }

    // if (this.formData.otherText !== '') {
    //   this.otherText = true;
    // }
    console.log('indexxx', this.stepThree.targetDetail);
    if (
      this.target === true &&
      this.targetDetail === true &&
      this.guaranty === true &&
      this.loanText === true &&
      this.interestText === true &&
      this.interestApproveText === true
      //  this.otherText === true
    ) {
      // this.saveDraftstatus = "success";
      console.log('form:', this.formData);
      // this.formData.targetDetail = this.stepThree.targetDetail;
      this.formData.status = true;
      return true;
    } else {
      this.status_loading = false;
      // alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
      console.log('target:', this.target);
      console.log('targetDetail:', this.targetDetail);
      console.log('guaranty:', this.guaranty);
      console.log('loanText:', this.loanText);
      console.log('interestText:', this.interestText);
      console.log('interestApproveText:', this.interestApproveText);
      console.log('otherText:', this.otherText);
      console.log('err:', this.formData);
      this.formData.status = false;
      // this.saveDraftstatus = '';
      return true;
    }
  }

  get_data_res() {
    this.formData.target[0].flag = this.group_data.target[0].flag;
    this.formData.target[1].flag = this.group_data.target[1].flag;
    this.formData.target[2].flag = this.group_data.target[2].flag;
    this.formData.target[3].flag = this.group_data.target[3].flag;

    this.formData.requestId = Number(localStorage.getItem('requestId'));
    this.formData.targetDetail = [];
    this.formData.targetDetailDelete = [];
    // console.log('group_data', this.group_data.targetDetail);
    // console.log('targetDetail', this.formData.targetDetail);
    for (let index = 0; index < this.group_data.targetDetail.length; index++) {
      const traget = this.group_data.targetDetail[index];
      this.formData.targetDetail.push({
        id: traget.id,
        targetType: traget.targetType,
        detail: traget.detail,
        loanDelete: traget.loanDelete,
        loan: traget.loan,
      });
    }
    // console.log('targetDetail', this.formData.targetDetail);
    for (let index = 0; index < this.formData.targetDetail.length; index++) {
      if (
        this.stepThree.targetDetail[0].targetType ===
        this.formData.targetDetail[index].targetType
      ) {
        this.stepThree.targetDetail[0].id = this.formData.targetDetail[
          index
        ].id;
        this.stepThree.targetDetail[0].detail = this.formData.targetDetail[
          index
        ].detail;
        this.stepThree.targetDetail[0].loan = this.formData.targetDetail[
          index
        ].loan;
      }

      if (
        this.stepThree.targetDetail[1].targetType ===
        this.formData.targetDetail[index].targetType
      ) {
        this.stepThree.targetDetail[1].id = this.formData.targetDetail[
          index
        ].id;
        this.stepThree.targetDetail[1].detail = this.formData.targetDetail[
          index
        ].detail;
        this.stepThree.targetDetail[1].loan = this.formData.targetDetail[
          index
        ].loan;
      }

      if (
        this.stepThree.targetDetail[2].targetType ===
        this.formData.targetDetail[index].targetType
      ) {
        this.stepThree.targetDetail[2].id = this.formData.targetDetail[
          index
        ].id;
        this.stepThree.targetDetail[2].detail = this.formData.targetDetail[
          index
        ].detail;
        this.stepThree.targetDetail[2].loan = this.formData.targetDetail[
          index
        ].loan;
      }
      if (
        this.stepThree.targetDetail[3].targetType ===
        this.formData.targetDetail[index].targetType
      ) {
        this.stepThree.targetDetail[3].id = this.formData.targetDetail[
          index
        ].id;
        this.stepThree.targetDetail[3].detail = this.formData.targetDetail[
          index
        ].detail;
        this.stepThree.targetDetail[3].loan = this.formData.targetDetail[
          index
        ].loan;
      }
    }
    this.formData.consider.applicant = this.group_data.consider.applicant;
    this.formData.consider.finan = this.group_data.consider.finan;
    this.formData.consider.status = this.group_data.consider.status;
    this.formData.consider.other = this.group_data.consider.other;


    this.changeValue();
    this.get_type();
    // ***************************************************************************************************/
    this.formData.loanText = this.group_data.loanText;
    for (let index = 0; index < this.group_data.loan.file.length; index++) {
      this.base64Image = this.group_data.loan.file[index].base64File;
      this.fileName = this.group_data.loan.file[index].fileName;
      const byteString =
        'data:image/jpeg;base64,' + this.base64Image.split(',')[1];
      const ab = new ArrayBuffer(byteString.length);
      const ia = new Uint8Array(ab);
      for (let i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
      }
      fetch('data:image/jpeg;base64,' + this.base64Image)
        .then((res) => res.blob())
        .then((blob) => {
          const file_1 = new File(
            [blob],
            this.group_data.loan.file[index].fileName,
            {
              type: 'image/png',
            }
          );

          // console.log("fff", file_1);
          this.stepThree.objective.uploadFile.push({
            file: file_1,
            id: this.group_data.loan.file[index].id,
          });
          this.formData.loan.file.push({
            id: this.group_data.loan.file[index].id,
            fileName: this.group_data.loan.file[index].fileName,
            fileSize: file_1.size,
            base64File: this.group_data.loan.file[index].base64File,
          });
        });

      const file = 'data:image/jpeg;base64,' + this.base64Image;

      this.image1[index] = file;
    }
    // console.log("loan 1:",this.file_img1)
    // ***************************************************************************************************/
    this.formData.interestText = this.group_data.interestText;
    for (let index = 0; index < this.group_data.interest.file.length; index++) {
      this.base64Image = this.group_data.interest.file[index].base64File;
      this.fileName = this.group_data.interest.file[index].fileName;
      const byteString =
        'data:image/jpeg;base64,' + this.base64Image.split(',')[1];
      const ab = new ArrayBuffer(byteString.length);
      const ia = new Uint8Array(ab);
      for (let i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
      }
      fetch('data:image/jpeg;base64,' + this.base64Image)
        .then((res) => res.blob())
        .then((blob) => {
          const file_1 = new File(
            [blob],
            this.group_data.interest.file[index].fileName,
            {
              type: 'image/png',
            }
          );

          // console.log("fff", file_1);
          this.stepThree.businessModel.uploadFile.push({
            file: file_1,
            id: this.group_data.interest.file[index].id,
          });
          this.formData.interest.file.push({
            id: this.group_data.interest.file[index].id,
            fileName: this.group_data.interest.file[index].fileName,
            fileSize: file_1.size,
            base64File: this.group_data.interest.file[index].base64File,
          });
        });

      const file = 'data:image/jpeg;base64,' + this.base64Image;

      this.image2[index] = file;
    }
    // ***************************************************************************************************/
    this.formData.interestApproveText = this.group_data.interestApproveText;
    for (
      let index = 0;
      index < this.group_data.interest_approve.file.length;
      index++
    ) {
      this.base64Image = this.group_data.interest_approve.file[
        index
      ].base64File;
      this.fileName = this.group_data.interest_approve.file[index].fileName;
      const byteString =
        'data:image/jpeg;base64,' + this.base64Image.split(',')[1];
      const ab = new ArrayBuffer(byteString.length);
      const ia = new Uint8Array(ab);
      for (let i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
      }
      fetch('data:image/jpeg;base64,' + this.base64Image)
        .then((res) => res.blob())
        .then((blob) => {
          const file_1 = new File(
            [blob],
            this.group_data.interest_approve.file[index].fileName,
            {
              type: 'image/png',
            }
          );

          // console.log("fff", file_1);
          this.stepThree.productCharacteristics.uploadFile.push({
            file: file_1,
            id: this.group_data.interest_approve.file[index].id,
          });
          this.formData.interest_approve.file.push({
            id: this.group_data.interest_approve.file[index].id,
            fileName: this.group_data.interest_approve.file[index].fileName,
            fileSize: file_1.size,
            base64File: this.group_data.interest_approve.file[index].base64File,
          });
        });

      const file = 'data:image/jpeg;base64,' + this.base64Image;

      this.image3[index] = file;
    }
    // ***************************************************************************************************/
    this.formData.otherText = this.group_data.otherText;
    for (let index = 0; index < this.group_data.other.file.length; index++) {
      this.base64Image = this.group_data.other.file[index].base64File;
      this.fileName = this.group_data.other.file[index].fileName;
      const byteString =
        'data:image/jpeg;base64,' + this.base64Image.split(',')[1];
      const ab = new ArrayBuffer(byteString.length);
      const ia = new Uint8Array(ab);
      for (let i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
      }
      fetch('data:image/jpeg;base64,' + this.base64Image)
        .then((res) => res.blob())
        .then((blob) => {
          const file_1 = new File(
            [blob],
            this.group_data.other.file[index].fileName,
            {
              type: 'image/png',
            }
          );

          // console.log("fff", file_1);
          this.stepThree.another.uploadFile.push({
            file: file_1,
            id: this.group_data.other.file[index].id,
          });
          this.formData.other.file.push({
            id: this.group_data.other.file[index].id,
            fileName: this.group_data.other.file[index].fileName,
            fileSize: file_1.size,
            base64File: this.group_data.other.file[index].base64File,
          });
        });

      const file = 'data:image/jpeg;base64,' + this.base64Image;

      this.image4[index] = file;
    }
    // ***************************************************************************************************/
    // this.textareaForm = new FormGroup({
    //   loanText: new FormControl(this.formData.loanText),
    //   interestText: new FormControl(this.formData.interestText),
    //   interestApproveText: new FormControl(this.formData.interestApproveText),
    //   otherText: new FormControl(this.formData.otherText),
    // });
    // ****************************************muti file*************************************************

    for (
      let index = 0;
      index < this.group_data.attachments.file.length;
      index++
    ) {
      this.formData.attachments.file.push({
        id: this.group_data.attachments.file[index].id,
        fileName: this.group_data.attachments.file[index].fileName,
        base64File: this.group_data.attachments.file[index].base64File,
        fileSize: this.group_data.attachments.file[index].fileSize,
        path: this.group_data.attachments.file[index].path
      });
    }

    this.dataSource_file = new MatTableDataSource(
      this.formData.attachments.file
    );
    this.hideDownloadFile();
    console.log('getfile>>>', this.dataSource_file);
    // this.formData.attachments.file = this.urls;
  }
  add_gain_credit(index: any) {
    // console.log("gain_index:", index);
    // tslint:disable-next-line: no-shadowed-variable
    for (let index = 0; index < this.index_to_create_template.length; index++) {
      this.gain_applicant.detail[this.index_to_create_template[index]] = '';
    }
    // console.log("gain_applicant:", this.gain_applicant);
    if (index === 1) {
      this.formData.consider.applicant.push(this.gain_applicant);
      this.gain_applicant = {
        id: '',
        topic: '',
        detail: {},
      };
    } else if (index === 2) {
      this.formData.consider.finan.push(this.gain_applicant);
      this.gain_applicant = {
        id: '',
        topic: '',
        detail: {},
      };
    } else if (index === 3) {
      this.formData.consider.status.push(this.gain_applicant);
      this.gain_applicant = {
        id: '',
        topic: '',
        detail: {},
      };
    } else if (index === 4) {
      this.formData.consider.other.push(this.gain_applicant);
      this.gain_applicant = {
        id: '',
        topic: '',
        detail: {},
      };
    }
  }

  gain() { }
  get_table() { }
  get_type() {
    this.formData.guaranty[0].flag = this.group_data.guaranty[0].flag;

    this.formData.guaranty[1].flag = this.group_data.guaranty[1].flag;
    this.formData.guaranty[1].child[0].flag = this.group_data.guaranty[1].child[0].flag;
    this.formData.guaranty[1].child[1].flag = this.group_data.guaranty[1].child[1].flag;
    this.formData.guaranty[1].child[2].flag = this.group_data.guaranty[1].child[2].flag;
    this.formData.guaranty[1].child[3].flag = this.group_data.guaranty[1].child[3].flag;

    this.formData.guaranty[2].flag = this.group_data.guaranty[2].flag;
    this.formData.guaranty[2].child[0].flag = this.group_data.guaranty[2].child[0].flag;
    this.formData.guaranty[2].child[1].flag = this.group_data.guaranty[2].child[1].flag;
    this.formData.guaranty[2].child[2].flag = this.group_data.guaranty[2].child[2].flag;
    this.formData.guaranty[2].child[3].flag = this.group_data.guaranty[2].child[3].flag;
    this.formData.guaranty[2].child[4].flag = this.group_data.guaranty[2].child[4].flag;
    this.formData.guaranty[2].child[5].flag = this.group_data.guaranty[2].child[5].flag;
    this.formData.guaranty[2].child[6].flag = this.group_data.guaranty[2].child[6].flag;

    this.formData.guaranty[3].flag = this.group_data.guaranty[3].flag;
    this.formData.guaranty[3].child[0].flag = this.group_data.guaranty[3].child[0].flag;
    this.formData.guaranty[3].child[1].flag = this.group_data.guaranty[3].child[1].flag;
    this.formData.guaranty[3].child[2].flag = this.group_data.guaranty[3].child[2].flag;

    this.formData.guaranty[4].flag = this.group_data.guaranty[4].flag;
    this.formData.guaranty[4].child[0].flag = this.group_data.guaranty[4].child[0].flag;
    this.formData.guaranty[4].child[1].flag = this.group_data.guaranty[4].child[1].flag;
    this.formData.guaranty[4].child[2].flag = this.group_data.guaranty[4].child[2].flag;
    this.formData.guaranty[4].child[3].flag = this.group_data.guaranty[4].child[3].flag;

    this.formData.guaranty[5].flag = this.group_data.guaranty[5].flag;
    this.formData.guaranty[6].flag = this.group_data.guaranty[6].flag;
    this.formData.guaranty[7].flag = this.group_data.guaranty[7].flag;
    this.formData.guaranty[7].otherDetail = this.group_data.guaranty[7].otherDetail;
    if (this.formData.guaranty[7].flag === false) {
      this.formData.guaranty[7].otherDetail = '';
    }
  }

  textarea(num: any) {
    console.log('text:', this.formData.targetDetail);
  }

  changeValue() {
    console.log('consider:',this.formData.consider)
    switch (this.formData.target[0].flag) {
      case true:

        const item = this.index_to_create_template.indexOf('sSME');
        if (item === -1) {
          this.StepThree.getProduct_detail_step_three(this.requestId, this.template_manage.pageShow).subscribe(res => {
            this.res_2 = res;
            // console.log('res_2:', this.res_2.data);
            if(res['data'] !==null) {
              for (let p = 0; p < this.formData.consider.applicant.length; p++) {

                if (this.res_2.data.consider.applicant[p] === undefined) {
                  this.formData.consider.applicant[p].detail['sSME'] = '';
                } else {
                  if (this.res_2.data.consider.applicant[p].detail['sSME'] === undefined) {
                    this.formData.consider.applicant[p].detail['sSME'] = '';
                  } else {
                    this.formData.consider.applicant[p].detail['sSME'] = this.res_2.data.consider.applicant[p].detail['sSME'];
  
                  }
                }
  
              }
              for (let p = 0; p < this.formData.consider.finan.length; p++) {
                if (this.res_2.data.consider.finan[p] === undefined) {
                  this.formData.consider.finan[p].detail['sSME'] = '';
                } else {
                  if (this.res_2.data.consider.finan[p].detail['sSME'] === undefined) {
                    this.formData.consider.finan[p].detail['sSME'] = '';
                  } else {
                    this.formData.consider.finan[p].detail['sSME'] = this.res_2.data.consider.finan[p].detail['sSME'];
  
                  }
                }
              }
              for (let p = 0; p < this.formData.consider.status.length; p++) {
                if (this.res_2.data.consider.status[p] === undefined) {
                  this.formData.consider.status[p].detail['sSME'] = '';
                } else {
                  if (this.res_2.data.consider.status[p].detail['sSME'] === undefined) {
                    this.formData.consider.status[p].detail['sSME'] = '';
                  } else {
                    this.formData.consider.status[p].detail['sSME'] = this.res_2.data.consider.status[p].detail['sSME'];
  
                  }
                }
              }
              for (let p = 0; p < this.formData.consider.other.length; p++) {
                if (this.res_2.data.consider.other[p] === undefined) {
                  this.formData.consider.other[p].detail['sSME'] = '';
                } else {
                  if (this.res_2.data.consider.other[p].detail['sSME'] === undefined) {
                    this.formData.consider.other[p].detail['sSME'] = '';
                  } else {
                    this.formData.consider.other[p].detail['sSME'] = this.res_2.data.consider.other[p].detail['sSME'];
  
                  }
                }
              }
            }
         
          });
          // for (let i = 0; i < this.index_to_create_template.length; i++ ) {
          this.index_to_create_template.splice(0, 0, 'sSME');
          // }
          // this.index_to_create_template.push('sSME');
          // console.log(
          //   'this.index_to_create_template',
          //   this.index_to_create_template
          // );
        }

        for (
          let index = 0;
          index < this.group_data.targetDetail.length;
          index++
        ) {
          const traget = this.group_data.targetDetail[index];

          if (traget.targetType === 'sSME') {
            this.text.sSME = traget.detail;
            this.stepThree.targetDetail[0].id = traget.id;
            this.stepThree.targetDetail[0].detail = traget.detail;
            this.stepThree.targetDetail[0].loan = traget.loan;
          }
        }
        for (
          let index = 0;
          index < this.formData.targetDetailDelete.length;
          index++
        ) {
          if (
            this.stepThree.targetDetail[0].id ===
            this.formData.targetDetailDelete[index]
          ) {
            this.formData.targetDetailDelete.splice(index, 1);
          }
        }

        break;
      case false:
        for (
          let index = 0;
          index < this.index_to_create_template.length;
          index++
        ) {
          if (this.index_to_create_template[index] === 'sSME') {
            this.index_to_create_template.splice(index, 1);
          }
        }
        if (this.stepThree.targetDetail[0].targetType === 'sSME') {
          if (this.stepThree.targetDetail[0].id !== '') {
            this.formData.targetDetailDelete.push(
              this.stepThree.targetDetail[0].id
            );
          }
          this.stepThree.targetDetail[0].loan = [];
        }
        // this.formData.targetDetail.splice(1, 1);
        // console.log('targetDetail', this.formData.targetDetail);
        for (let p = 0; p < this.formData.consider.applicant.length; p++) {
          this.formData.consider.applicant[p].detail['sSME'] = '';
        }
        for (let p = 0; p < this.formData.consider.finan.length; p++) {
          this.formData.consider.finan[p].detail['sSME'] = '';
        }
        for (let p = 0; p < this.formData.consider.status.length; p++) {
          this.formData.consider.status[p].detail['sSME'] = '';
        }
        for (let p = 0; p < this.formData.consider.other.length; p++) {
          this.formData.consider.other[p].detail['sSME'] = '';
        }
        // console.log('consider', this.formData.consider);
        break;
    }

    switch (this.formData.target[1].flag) {
      case true:
        const item = this.index_to_create_template.indexOf('SME-M');

        if (item === -1) {
          // console.log('SME-Mssss');
          // this.index_to_create_template.push('SME-M');
          this.StepThree.getProduct_detail_step_three(this.formData.requestId, this.template_manage.pageShow).subscribe(res => {
            this.res_2 = res;
            // console.log('res_2:', this.res_2.data);
            if(res['data'] !== null) {
              for (let p = 0; p < this.formData.consider.applicant.length; p++) {
                if (this.res_2.data.consider.applicant[p] === undefined) {
                  this.formData.consider.applicant[p].detail['SME-M'] = '';
                } else {
                  if (this.res_2.data.consider.applicant[p].detail['SME-M'] === undefined) {
                    this.formData.consider.applicant[p].detail['SME-M'] = '';
                  } else {
                    this.formData.consider.applicant[p].detail['SME-M'] = this.res_2.data.consider.applicant[p].detail['SME-M'];
  
                  }
                }
              }
              for (let p = 0; p < this.formData.consider.finan.length; p++) {
                if (this.res_2.data.consider.finan[p] === undefined) {
                  this.formData.consider.finan[p].detail['SME-M'] = '';
                } else {
                  if (this.res_2.data.consider.finan[p].detail['SME-M'] === undefined) {
                    this.formData.consider.finan[p].detail['SME-M'] = '';
                  } else {
                    this.formData.consider.finan[p].detail['SME-M'] = this.res_2.data.consider.finan[p].detail['SME-M'];
  
                  }
                }
              }
              for (let p = 0; p < this.formData.consider.status.length; p++) {
                if (this.res_2.data.consider.status[p] === undefined) {
                  this.formData.consider.status[p].detail['SME-M'] = '';
                } else {
                  if (this.res_2.data.consider.status[p].detail['SME-M'] === undefined) {
                    this.formData.consider.status[p].detail['SME-M'] = '';
                  } else {
                    this.formData.consider.status[p].detail['SME-M'] = this.res_2.data.consider.status[p].detail['SME-M'];
  
                  }
                }
              }
              for (let p = 0; p < this.formData.consider.other.length; p++) {
                if (this.res_2.data.consider.other[p] === undefined) {
                  this.formData.consider.other[p].detail['SME-M'] = '';
                } else {
                  if (this.res_2.data.consider.other[p].detail['SME-M'] === undefined) {
                    this.formData.consider.other[p].detail['SME-M'] = '';
                  } else {
                    this.formData.consider.other[p].detail['SME-M'] = this.res_2.data.consider.other[p].detail['SME-M'];
  
                  }
                }
              }
            }
          
          });
          if (this.index_to_create_template.length === 0) {
            this.index_to_create_template.splice(0, 0, 'SME-M');

            break;
          } else {
            for (
              let index = 0;
              index < this.index_to_create_template.length;
              index++
            ) {
              if (this.index_to_create_template[index] === 'sSME') {
                // console.log('SME-M');
                this.index_to_create_template.splice(index + 1, 0, 'SME-M');
                break;
              } else {
                this.index_to_create_template.splice(index, 0, 'SME-M');
                break;
              }
            }
          }

          // console.log(
          //   'this.index_to_create_template',
          //   this.index_to_create_template
          // );
        }
        for (
          let index = 0;
          index < this.group_data.targetDetail.length;
          index++
        ) {
          const traget = this.group_data.targetDetail[index];

          if (traget.targetType === 'SME-M') {
            this.text.SME_M = traget.detail;
            this.stepThree.targetDetail[1].id = traget.id;
            this.stepThree.targetDetail[1].detail = traget.detail;
            this.stepThree.targetDetail[1].loan = traget.loan;
          }
        }
        for (
          let index = 0;
          index < this.formData.targetDetailDelete.length;
          index++
        ) {
          if (
            this.stepThree.targetDetail[1].id ===
            this.formData.targetDetailDelete[index]
          ) {
            this.formData.targetDetailDelete.splice(index, 1);
          }
        }
        // console.log('SME-M');

        break;
      case false:
        for (
          let index = 0;
          index < this.index_to_create_template.length;
          index++
        ) {
          if (this.index_to_create_template[index] === 'SME-M') {
            this.index_to_create_template.splice(index, 1);
          }
        }
        if (this.stepThree.targetDetail[1].targetType === 'SME-M') {
          if (this.stepThree.targetDetail[1].id !== '') {
            this.formData.targetDetailDelete.push(
              this.stepThree.targetDetail[1].id
            );
          }
          this.stepThree.targetDetail[1].loan = [];
        }
        // this.formData.targetDetail.splice(1, 1);
        // console.log('targetDetail', this.formData.targetDetail);
        for (let p = 0; p < this.formData.consider.applicant.length; p++) {
          this.formData.consider.applicant[p].detail['SME-M'] = '';
        }
        for (let p = 0; p < this.formData.consider.finan.length; p++) {
          this.formData.consider.finan[p].detail['SME-M'] = '';
        }
        for (let p = 0; p < this.formData.consider.status.length; p++) {
          this.formData.consider.status[p].detail['SME-M'] = '';
        }
        for (let p = 0; p < this.formData.consider.other.length; p++) {
          this.formData.consider.other[p].detail['SME-M'] = '';
        }
        // console.log('consider', this.formData.consider);
        break;
    }

    switch (this.formData.target[2].flag) {
      case true:

        const item = this.index_to_create_template.indexOf('SME-L');
        if (item === -1) {
          this.StepThree.getProduct_detail_step_three(this.requestId, this.template_manage.pageShow).subscribe(res => {
            this.res_2 = res;
            console.log('res_2:', this.res_2.data);
            if(res['data'] !== null) {
            for (let p = 0; p < this.formData.consider.applicant.length; p++) {
              if (this.res_2.data.consider.applicant[p] === undefined) {
                this.formData.consider.applicant[p].detail['SME-L'] = '';
              } else {
                if (this.res_2.data.consider.applicant[p].detail['SME-L'] === undefined) {
                  this.formData.consider.applicant[p].detail['SME-L'] = '';
                } else {
                  this.formData.consider.applicant[p].detail['SME-L'] = this.res_2.data.consider.applicant[p].detail['SME-L'];

                }
              }
            }
            for (let p = 0; p < this.formData.consider.finan.length; p++) {
              if (this.res_2.data.consider.finan[p] === undefined) {
                this.formData.consider.finan[p].detail['SME-L'] = '';
              } else {
                if (this.res_2.data.consider.finan[p].detail['SME-L'] === undefined) {
                  this.formData.consider.finan[p].detail['SME-L'] = '';
                } else {
                  this.formData.consider.finan[p].detail['SME-L'] = this.res_2.data.consider.finan[p].detail['SME-L'];

                }
              }
            }
            for (let p = 0; p < this.formData.consider.status.length; p++) {
              if (this.res_2.data.consider.status[p] === undefined) {
                this.formData.consider.status[p].detail['SME-L'] = '';
              } else {
                if (this.res_2.data.consider.status[p].detail['SME-L'] === undefined) {
                  this.formData.consider.status[p].detail['SME-L'] = '';
                } else {
                  this.formData.consider.status[p].detail['SME-L'] = this.res_2.data.consider.status[p].detail['SME-L'];

                }
              }
            }
            for (let p = 0; p < this.formData.consider.other.length; p++) {
              if (this.res_2.data.consider.other[p] === undefined) {
                this.formData.consider.other[p].detail['SME-L'] = '';
              } else {
                if (this.res_2.data.consider.other[p].detail['SME-L'] === undefined) {
                  this.formData.consider.other[p].detail['SME-L'] = '';
                } else {
                  this.formData.consider.other[p].detail['SME-L'] = this.res_2.data.consider.other[p].detail['SME-L'];

                }
              }
            }
          }
          });
          // this.index_to_create_template.push('SME-L');
          if (this.index_to_create_template.length === 0) {
            this.index_to_create_template.splice(0, 0, 'SME-L');
            break;
          } else {
            for (
              let index = 0;
              index < this.index_to_create_template.length;
              index++
            ) {
              if (
                this.index_to_create_template[index] === 'sSME' &&
                this.index_to_create_template.length === 1
              ) {
                this.index_to_create_template.splice(index + 1, 0, 'SME-L');
                break;
              } else if (
                this.index_to_create_template[index] === 'sSME' &&
                this.index_to_create_template[index + 1] === 'SME-M'
              ) {
                this.index_to_create_template.splice(index + 2, 0, 'SME-L');
                break;
              } else if (
                this.index_to_create_template[index] === 'sSME' &&
                this.index_to_create_template[index + 1] !== 'SME-M'
              ) {
                this.index_to_create_template.splice(index + 1, 0, 'SME-L');
                break;
              } else if (
                this.index_to_create_template[index] === 'SME-M'
              ) {
                this.index_to_create_template.splice(index + 1, 0, 'SME-L');
                break;
              } else if (this.index_to_create_template.length === 0) {
                this.index_to_create_template.splice(0, 0, 'SME-L');
                break;
              } else {
                this.index_to_create_template.splice(index, 0, 'SME-L');
                break;
              }
            }
          }
          // this.index_to_create_template.splice(2, 0, 'SME-L');
          // console.log(
          //   'this.index_to_create_template',
          //   this.index_to_create_template
          // );
        }
        for (
          let index = 0;
          index < this.group_data.targetDetail.length;
          index++
        ) {
          const traget = this.group_data.targetDetail[index];

          if (traget.targetType === 'SME-L') {
            this.text.SME_L = traget.detail;
            this.stepThree.targetDetail[2].id = traget.id;
            this.stepThree.targetDetail[2].detail = traget.detail;
            this.stepThree.targetDetail[2].loan = traget.loan;
          }
        }
        for (
          let index = 0;
          index < this.formData.targetDetailDelete.length;
          index++
        ) {
          if (
            this.stepThree.targetDetail[2].id ===
            this.formData.targetDetailDelete[index]
          ) {
            this.formData.targetDetailDelete.splice(index, 1);
          }
        }

        break;
      case false:
        for (
          let index = 0;
          index < this.index_to_create_template.length;
          index++
        ) {
          if (this.index_to_create_template[index] === 'SME-L') {
            this.index_to_create_template.splice(index, 1);
          }
        }
        if (this.stepThree.targetDetail[2].targetType === 'SME-L') {
          if (this.stepThree.targetDetail[2].id !== '') {
            this.formData.targetDetailDelete.push(
              this.stepThree.targetDetail[2].id
            );
          }
          this.stepThree.targetDetail[2].loan = [];
        }
        // this.formData.targetDetail.splice(1, 1);
        // console.log('targetDetail', this.formData.targetDetail);
        for (let p = 0; p < this.formData.consider.applicant.length; p++) {
          this.formData.consider.applicant[p].detail['SME-L'] = '';
        }
        for (let p = 0; p < this.formData.consider.finan.length; p++) {
          this.formData.consider.finan[p].detail['SME-L'] = '';
        }
        for (let p = 0; p < this.formData.consider.status.length; p++) {
          this.formData.consider.status[p].detail['SME-L'] = '';
        }
        for (let p = 0; p < this.formData.consider.other.length; p++) {
          this.formData.consider.other[p].detail['SME-L'] = '';
        }
        // console.log('consider', this.formData.consider);
        break;
    }

    switch (this.formData.target[3].flag) {
      case true:

        const item = this.index_to_create_template.indexOf('Corporate');
        if (item === -1) {
          this.StepThree.getProduct_detail_step_three(this.formData.requestId, this.template_manage.pageShow).subscribe(res => {
            this.res_2 = res;
            console.log('res_2:', this.res_2.data);
            if(res['data'] !== null) {
            for (let p = 0; p < this.formData.consider.applicant.length; p++) {
              if (this.res_2.data.consider.applicant[p] === undefined) {
                this.formData.consider.applicant[p].detail['Corporate'] = '';
              } else {
                if (this.res_2.data.consider.applicant[p].detail['Corporate'] === undefined) {
                  this.formData.consider.applicant[p].detail['Corporate'] = '';
                } else {
                  this.formData.consider.applicant[p].detail['Corporate'] = this.res_2.data.consider.applicant[p].detail['Corporate'];

                }
              }
            }
            for (let p = 0; p < this.formData.consider.finan.length; p++) {
              if (this.res_2.data.consider.finan[p] === undefined) {
                this.formData.consider.finan[p].detail['Corporate'] = '';
              } else {
                if (this.res_2.data.consider.finan[p].detail['Corporate'] === undefined) {
                  this.formData.consider.finan[p].detail['Corporate'] = '';
                } else {
                  this.formData.consider.finan[p].detail['Corporate'] = this.res_2.data.consider.finan[p].detail['Corporate'];

                }
              }
            }
            for (let p = 0; p < this.formData.consider.status.length; p++) {
              if (this.res_2.data.consider.status[p] === undefined) {
                this.formData.consider.status[p].detail['Corporate'] = '';
              } else {
                if (this.res_2.data.consider.status[p].detail['Corporate'] === undefined) {
                  this.formData.consider.status[p].detail['Corporate'] = '';
                } else {
                  this.formData.consider.status[p].detail['Corporate'] = this.res_2.data.consider.status[p].detail['Corporate'];

                }
              }
            }
            for (let p = 0; p < this.formData.consider.other.length; p++) {
              if (this.res_2.data.consider.other[p] === undefined) {
                this.formData.consider.other[p].detail['Corporate'] = '';
              } else {
                if (this.res_2.data.consider.other[p].detail['Corporate'] === undefined) {
                  this.formData.consider.other[p].detail['Corporate'] = '';
                } else {
                  this.formData.consider.other[p].detail['Corporate'] = this.res_2.data.consider.other[p].detail['Corporate'];

                }
              }
            }
          }
          });
          // this.index_to_create_template.push('Corporate');
          this.index_to_create_template.splice(3, 0, 'Corporate');
          // console.log(
          //   'this.index_to_create_template',
          //   this.index_to_create_template
          // );
        }
        for (
          let index = 0;
          index < this.group_data.targetDetail.length;
          index++
        ) {
          const traget = this.group_data.targetDetail[index];

          if (traget.targetType === 'Corporate') {
            this.text.Corporate = traget.detail;
            this.stepThree.targetDetail[3].id = traget.id;
            this.stepThree.targetDetail[3].detail = traget.detail;
            this.stepThree.targetDetail[3].loan = traget.loan;
          }
        }
        for (
          let index = 0;
          index < this.formData.targetDetailDelete.length;
          index++
        ) {
          if (
            this.stepThree.targetDetail[3].id ===
            this.formData.targetDetailDelete[index]
          ) {
            this.formData.targetDetailDelete.splice(index, 1);
          }
        }

        break;
      case false:
        for (
          let index = 0;
          index < this.index_to_create_template.length;
          index++
        ) {
          if (this.index_to_create_template[index] === 'Corporate') {
            this.index_to_create_template.splice(index, 1);
          }
        }
        if (this.stepThree.targetDetail[3].targetType === 'Corporate') {
          if (this.stepThree.targetDetail[3].id !== '') {
            this.formData.targetDetailDelete.push(
              this.stepThree.targetDetail[3].id
            );
          }
          this.stepThree.targetDetail[3].loan = [];
        }
        // this.formData.targetDetail.splice(1, 1);
        // console.log('targetDetail', this.formData.targetDetail);
        for (let p = 0; p < this.formData.consider.applicant.length; p++) {
          this.formData.consider.applicant[p].detail['Corporate'] = '';
        }
        for (let p = 0; p < this.formData.consider.finan.length; p++) {
          this.formData.consider.finan[p].detail['Corporate'] = '';
        }
        for (let p = 0; p < this.formData.consider.status.length; p++) {
          this.formData.consider.status[p].detail['Corporate'] = '';
        }
        for (let p = 0; p < this.formData.consider.other.length; p++) {
          this.formData.consider.other[p].detail['Corporate'] = '';
        }
        // console.log('consider', this.formData.consider);
        break;
    }

    this.length_header = this.index_to_create_template.length;
    console.log('click-Check', this.formData.consider);
    // this.changeSaveDraft();
  }

  delete_gain(gain: any, data: any, index: any) {
    console.log('del gain::', gain);
    console.log('del data::', data);
    console.log('select::', index);

    switch (gain) {
      case 1:
        this.formData.considerDelete.push(data.id);
        this.formData.consider.applicant.splice(index, 1);
        break;
      case 2:
        this.formData.considerDelete.push(data.id);
        this.formData.consider.finan.splice(index, 1);
        break;
      case 3:
        this.formData.considerDelete.push(data.id);
        this.formData.consider.status.splice(index, 1);
        break;
      case 4:
        this.formData.considerDelete.push(data.id);
        this.formData.consider.other.splice(index, 1);

        break;
    }
    console.log('form:', this.formData.consider);
    this.changeSaveDraft();
  }
  changeValue_collateral() {
    if (this.formData.guaranty[1].flag === false) {
      this.formData.guaranty[1].child[0].flag = false;
      this.formData.guaranty[1].child[1].flag = false;
      this.formData.guaranty[1].child[2].flag = false;
      this.formData.guaranty[1].child[3].flag = false;
    }

    if (this.formData.guaranty[2].flag === false) {
      this.formData.guaranty[2].child[0].flag = false;
      this.formData.guaranty[2].child[1].flag = false;
      this.formData.guaranty[2].child[2].flag = false;
      this.formData.guaranty[2].child[3].flag = false;
      this.formData.guaranty[2].child[4].flag = false;
      this.formData.guaranty[2].child[5].flag = false;
      this.formData.guaranty[2].child[6].flag = false;
    }

    if (this.formData.guaranty[3].flag === false) {
      this.formData.guaranty[3].child[0].flag = false;
      this.formData.guaranty[3].child[1].flag = false;
      this.formData.guaranty[3].child[2].flag = false;
    }

    if (this.formData.guaranty[4].flag === false) {
      this.formData.guaranty[4].child[0].flag = false;
      this.formData.guaranty[4].child[1].flag = false;
      this.formData.guaranty[4].child[2].flag = false;
      this.formData.guaranty[4].child[3].flag = false;
    }

    if (this.formData.guaranty[7].flag === false) {
      this.formData.guaranty[7].otherDetail = '';
    }

    this.changeSaveDraft();
  }

  add(number: any) {
    console.log('index_add::', number);

    if (number === 1) {
      for (let index = 0; index < this.stepThree.targetDetail.length; index++) {
        if (this.stepThree.targetDetail[index].targetType === 'sSME') {
          this.stepThree.targetDetail[index].loan.push(this.add_data);
        }
      }
    } else if (number === 2) {
      for (let index = 0; index < this.stepThree.targetDetail.length; index++) {
        if (this.stepThree.targetDetail[index].targetType === 'SME-M') {
          this.stepThree.targetDetail[index].loan.push(this.add_data);
        }
      }
    } else if (number === 3) {
      for (let index = 0; index < this.stepThree.targetDetail.length; index++) {
        if (this.stepThree.targetDetail[index].targetType === 'SME-L') {
          this.stepThree.targetDetail[index].loan.push(this.add_data);
        }
      }
    } else if (number === 4) {
      for (let index = 0; index < this.stepThree.targetDetail.length; index++) {
        if (this.stepThree.targetDetail[index].targetType === 'Corporate') {
          this.stepThree.targetDetail[index].loan.push(this.add_data);
        }
      }
    }

    this.add_data = {
      id: '',
      loandType: '',
      limitType: '',
      limit: '',
      preriod: '',
      max: '',
      min: '',
      avg: '',
    };
  }
  delete(index: any, row: any, i: any) {
    switch (index) {
      case 1:
        for (let k = 0; k < this.stepThree.targetDetail.length; k++) {
          if (this.stepThree.targetDetail[k].targetType === 'sSME') {
            if (this.stepThree.targetDetail[k].loan[i].id !== '') {
              this.stepThree.targetDetail[k].loanDelete.push(
                this.stepThree.targetDetail[k].loan[i].id
              );
            }
            this.stepThree.targetDetail[k].loan.splice(i, 1);
          }
        }
        break;
      case 2:
        for (let k = 0; k < this.stepThree.targetDetail.length; k++) {
          if (this.stepThree.targetDetail[k].targetType === 'SME-M') {
            if (this.stepThree.targetDetail[k].loan[i].id !== '') {
              this.stepThree.targetDetail[k].loanDelete.push(
                this.stepThree.targetDetail[k].loan[i].id
              );
            }
            this.stepThree.targetDetail[k].loan.splice(i, 1);
          }
        }
        break;
      case 3:
        for (let k = 0; k < this.stepThree.targetDetail.length; k++) {
          if (this.stepThree.targetDetail[k].targetType === 'SME-L') {
            if (this.stepThree.targetDetail[k].loan[i].id !== '') {
              this.stepThree.targetDetail[k].loanDelete.push(
                this.stepThree.targetDetail[k].loan[i].id
              );
            }
            this.stepThree.targetDetail[k].loan.splice(i, 1);
          }
        }
        break;
      case 4:
        for (let k = 0; k < this.stepThree.targetDetail.length; k++) {
          if (this.stepThree.targetDetail[k].targetType === 'Corporate') {
            if (this.stepThree.targetDetail[k].loan[i].id !== '') {
              this.stepThree.targetDetail[k].loanDelete.push(
                this.stepThree.targetDetail[k].loan[i].id
              );
            }
            this.stepThree.targetDetail[k].loan.splice(i, 1);
          }
        }
        // this.stepThree.Corporate.table.splice(i, 1);
        break;
    }
    console.log('delete', this.formData.targetDetail);
    this.changeSaveDraft();
  }

  delete_image(fileIndex, index) {
    console.log('fileIndex:', fileIndex);
    console.log('index:', index);
    if (fileIndex === 1) {
      this.stepThree.objective.uploadFile.splice(index, 1);
      this.image1.splice(index, 1);
      if (this.formData.loan.file[index].id === '') {
        this.formData.loan.file.splice(index, 1);
      } else {
        this.formData.loan.fileDelete.push(this.formData.loan.file[index].id);
        this.formData.loan.file.splice(index, 1);
      }
      // console.log("delete",this.file_img1_delete)
      console.log('formData.loan:', this.stepThree.objective.uploadFile.length);
      for (let i = 0; i < this.stepThree.objective.uploadFile.length; i++) {
        this.preview(fileIndex, this.stepThree.objective.uploadFile[i], i);
      }
    } else if (fileIndex === 2) {
      this.stepThree.businessModel.uploadFile.splice(index, 1);
      this.image2.splice(index, 1);
      if (this.formData.interest.file[index].id === '') {
        this.formData.interest.file.splice(index, 1);
      } else {
        this.formData.interest.fileDelete.push(
          this.formData.interest.file[index].id
        );
        this.formData.interest.file.splice(index, 1);
      }
      console.log(
        'formData.interest:',
        this.stepThree.businessModel.uploadFile.length
      );
      for (let i = 0; i < this.stepThree.businessModel.uploadFile.length; i++) {
        this.preview(fileIndex, this.stepThree.businessModel.uploadFile[i], i);
      }
    } else if (fileIndex === 3) {
      this.stepThree.productCharacteristics.uploadFile.splice(index, 1);
      this.image3.splice(index, 1);
      if (this.formData.interest_approve.file[index].id === '') {
        this.formData.interest_approve.file.splice(index, 1);
      } else {
        this.formData.interest_approve.fileDelete.push(
          this.formData.interest_approve.file[index].id
        );
        this.formData.interest_approve.file.splice(index, 1);
      }
      console.log(
        'formData.interest_approve:',
        this.stepThree.productCharacteristics.uploadFile.length
      );
      for (
        let i = 0;
        i < this.stepThree.productCharacteristics.uploadFile.length;
        i++
      ) {
        this.preview(
          fileIndex,
          this.stepThree.productCharacteristics.uploadFile[i],
          i
        );
      }
    } else if (fileIndex === 4) {
      this.stepThree.another.uploadFile.splice(index, 1);
      this.image4.splice(index, 1);
      if (this.formData.other.file[index].id === '') {
        this.formData.other.file.splice(index, 1);
      } else {
        this.formData.other.fileDelete.push(this.formData.other.file[index].id);
        this.formData.other.file.splice(index, 1);
      }
      console.log(
        'formData.other.file:',
        this.stepThree.another.uploadFile.length
      );
      for (let i = 0; i < this.stepThree.another.uploadFile.length; i++) {
        this.preview(fileIndex, this.stepThree.another.uploadFile[i], i);
      }
    }

    // console.log("del img1:", this.file_img1);
    // console.log("del img2:", this.file_img2);
    // console.log("del img3:", this.file_img3);
    // console.log("del img4:", this.file_img4);
    this.changeSaveDraft();
  }

  image_click(colId, id) {
    this.showModel = true;
    console.log('colId:', colId);
    console.log('id:', id);

    document.getElementById('myModal3_1').style.display = 'block';
    this.imageModal(colId, id);
  }
  image_bdclick() {
    document.getElementById('myModal3_1').style.display = 'block';
    console.log('2222');
  }

  closeModal() {
    this.showModel = false;
    document.getElementById('myModal3_1').style.display = 'none';
  }
  change_text_img(index: any) { }
  // getFileDetails ================================================================
  getFileDetails(fileIndex, event: any) {
    console.log('fileIndex:', fileIndex);
    console.log('event:', event.target.files[0]);

    const file = event.target.files;
    for (let index = 0; index < file.length; index++) {
      const mimeType = file[index].type;
      if (mimeType.match('image/jpeg|image/png') == null) {
        this.message = 'Only images are supported.';
        this.status_loading = false;
        alert(this.message);
        return;
      }
    }
    // this.file_name = file.name;
    // this.file_type = file.type;
    // this.file_size = file.size;
    // this.modifiedDate = file.lastModifiedDate;
    // console.log('file:', file);
    console.log('file.length:', file.length);
    if (fileIndex === 1) {
      console.log('00000000000000000000000000000001');
      for (let index = 0; index < file.length; index++) {
        this.stepThree.objective.uploadFile.push({
          file: event.target.files[index],
          id: '',
        });
      }
      console.log('fff:', this.stepThree.objective.uploadFile);

      for (
        let index = 0;
        index < this.stepThree.objective.uploadFile.length;
        index++
      ) {
        this.preview(
          fileIndex,
          this.stepThree.objective.uploadFile[index],
          index
        );
      }
      this.file1_1 = undefined;
      this.file1_2 = undefined;
    } else if (fileIndex === 2) {
      console.log('00000000000000000000000000000002');
      // console.log("image2:",this.image2)
      for (let index = 0; index < file.length; index++) {
        this.stepThree.businessModel.uploadFile.push({
          file: event.target.files[index],
          id: '',
        });
      }

      for (
        let index = 0;
        index < this.stepThree.businessModel.uploadFile.length;
        index++
      ) {
        this.preview(
          fileIndex,
          this.stepThree.businessModel.uploadFile[index],
          index
        );
        this.file2_1 = undefined;
        this.file2_2 = undefined;
      }
    } else if (fileIndex === 3) {
      console.log('00000000000000000000000000000003');
      for (let index = 0; index < file.length; index++) {
        this.stepThree.productCharacteristics.uploadFile.push({
          file: event.target.files[index],
          id: '',
        });
      }

      for (
        let index = 0;
        index < this.stepThree.productCharacteristics.uploadFile.length;
        index++
      ) {
        this.preview(
          fileIndex,
          this.stepThree.productCharacteristics.uploadFile[index],
          index
        );
      }
      this.file3_1 = undefined;
      this.file3_2 = undefined;
    } else if (fileIndex === 4) {
      console.log('00000000000000000000000000000004');
      for (let index = 0; index < file.length; index++) {
        this.stepThree.another.uploadFile.push({
          file: event.target.files[index],
          id: '',
        });
      }

      for (
        let index = 0;
        index < this.stepThree.another.uploadFile.length;
        index++
      ) {
        this.preview(
          fileIndex,
          this.stepThree.another.uploadFile[index],
          index
        );
      }
      this.file4_1 = undefined;
      this.file4_2 = undefined;
    }
  }

  preview(fileIndex, file, index) {
    const reader = new FileReader();
    this.formData.loan.file = [];
    // console.log("llll:",this.file_img1)
    if (fileIndex === 1) {
      if (file.file) {
        // console.log("llll:",file)
        reader.readAsDataURL(file.file);
        reader.onload = (_event) => {
          this.image1[index] = reader.result;
          const base64result = this.image1[index].substr(
            this.image1[index].indexOf(',') + 1
          );
          this.formData.loan.file.push({
            id: file.id,
            fileName: file.file.name,
            fileSize: file.file.size,
            base64File: base64result,
          });
          // console.log("file_img1:", this.file_img1);
        };
      }
    } else if (fileIndex === 2) {
      this.formData.interest.file = [];
      if (file.file) {
        reader.readAsDataURL(file.file);
        reader.onload = (_event) => {
          this.image2[index] = reader.result;
          const base64result = this.image2[index].substr(
            this.image2[index].indexOf(',') + 1
          );
          this.formData.interest.file.push({
            id: file.id,
            fileName: file.file.name,
            fileSize: file.file.size,
            base64File: base64result,
          });
        };
      }
    } else if (fileIndex === 3) {
      this.formData.interest_approve.file = [];
      if (file.file) {
        reader.readAsDataURL(file.file);
        reader.onload = (_event) => {
          this.image3[index] = reader.result;
          const base64result = this.image3[index].substr(
            this.image3[index].indexOf(',') + 1
          );
          this.formData.interest_approve.file.push({
            id: file.id,
            fileName: file.file.name,
            fileSize: file.file.size,
            base64File: base64result,
          });
        };
      }
    } else if (fileIndex === 4) {
      this.formData.other.file = [];
      if (file.file) {
        reader.readAsDataURL(file.file);
        reader.onload = (_event) => {
          this.image4[index] = reader.result;
          const base64result = this.image4[index].substr(
            this.image4[index].indexOf(',') + 1
          );
          this.formData.other.file.push({
            id: file.id,
            fileName: file.file.name,
            fileSize: file.file.size,
            base64File: base64result,
          });
        };
      }
    }
    // this.formData.loan.file = this.file_img1;
    // this.formData.loan.fileDelete = this.file_img1_delete;

    // this.formData.interest.file = this.file_img2;
    // this.formData.interest.fileDelete = this.file_img2_delete;

    // this.formData.interest_approve.file = this.file_img3;
    // this.formData.interest_approve.fileDelete = this.file_img3_delete;

    // this.formData.other.file = this.file_img4;
    // this.formData.other.fileDelete = this.file_img4_delete;

    // console.log('img1:', this.formData.loan);
    // console.log('img2:', this.formData.interest);
    // console.log('img3:', this.formData.interest_approve);
    // console.log('img4:', this.formData.other);
    this.changeSaveDraft();
  }

  getFileDocument(event) {
    const file = event.target.files[0];
    const mimeType = file.type;
    if (mimeType.match(/pdf\/*/) == null) {
      this.message = 'Only PDF are supported.';
      this.status_loading = false;
      alert(this.message);
      return;
    }
    console.log('11111', file);
    this.file_name = file.name;
    // this.file_type = file.type;
    this.file_size = file.size / 1024;
    // this.modifiedDate = file.lastModifiedDate;
    // console.log('file:', file);

    this.stepThree.documentFile = file;
    // this.document = undefined;
  }

  imageModal(fileIndex, index) {
    if (fileIndex === 1) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image1;
      this.modalImagedetail = this.stepThree.objective.uploadFile;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 2) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image2;
      this.modalImagedetail = this.stepThree.businessModel.uploadFile;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 3) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image3;
      this.modalImagedetail = this.stepThree.productCharacteristics.uploadFile;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 4) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image4;
      this.modalImagedetail = this.stepThree.another.uploadFile;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    }
  }

  onSelectFile(event) {
    this.FileOverSize = [];
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      const file = event.target.files;
      // console.log(event)
      for (let i = 0; i < filesAmount; i++) {
        // console.log("type:", file[i]);
        if (
          // file[i].type === 'image/svg+xml' ||
          file[i].type === 'image/jpeg' ||
          // file[i].type === 'image/raw' ||
          // file[i].type === 'image/svg' ||
          // file[i].type === 'image/tif' ||
          // file[i].type === 'image/gif' ||
          file[i].type === 'image/jpg' ||
          file[i].type === 'image/png' ||
          file[i].type === 'application/doc' ||
          file[i].type === 'application/pdf' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
          file[i].type === 'application/pptx' ||
          file[i].type === 'application/docx' ||
          (file[i].type === 'application/ppt' && file[i].size)
        ) {
          if (file[i].size <= 5000000) {
            // tslint:disable-next-line: prefer-const
            let select = {
              id: '',
              file: file[i],
            };
            this.multiple_file(select, i);
            // }
            // }
          } else {
            this.FileOverSize.push(file[i].name);
          }
        } else {
          this.status_loading = false;
          alert('File Not Support');
        }
      }

      if (this.FileOverSize.length !== 0) {
        console.log('open modal');
        document.getElementById('testttt').click();
      }

    }
  }

  multiple_file(file, index) {
    // console.log("download");
    const reader = new FileReader();
    if (file.file) {
      reader.readAsDataURL(file.file);
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');

        this.formData.attachments.file.push({
          id: file.id,
          fileName: file.file.name,
          fileSize: file.file.size,
          base64File: splitFile[1],
        });
        // console.log('this.urls', this.urls);
        this.dataSource_file = new MatTableDataSource(
          this.formData.attachments.file
        );
        this.hideDownloadFile();
        // this.formData.attachments.file = this.urls;
        // console.log('form add:', this.formData.attachments.file);
      };
    }
    this.changeSaveDraft();
  }

  delete_mutifile(data: any, index: any) {
    console.log('data:', data, 'index::', index);
    this.dataSource_file.data.splice(index, 1);
    console.log('urls:', this.urls);
    // this.urls.splice(index, 1);
    // this.muti_file.splice(index, 1);
    if (data.id !== '') {
      this.formData.attachments.fileDelete.push(data.id);
    }
    this.dataSource_file = new MatTableDataSource(this.dataSource_file.data);
    this.formData.attachments.file = this.dataSource_file.data;
    console.log('form dee:', this.formData.attachments);
    this.changeSaveDraft();
  }

  // downloadFile
  downloadFile(pathdata: any) {
    this.status_loading = true;
    const contentType = '';
    const sendpath = pathdata;
    console.log('path', sendpath);
    this.sidebarService.getfile(sendpath).subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        this.status_loading = false;
        const datagetfile = res['data'];
        const b64Data = datagetfile['data'];
        const filename = datagetfile['fileName'];
        console.log('base64', b64Data);
        const config = this.b64toBlob(b64Data, contentType);
        saveAs(config, filename);
      }

    });
  }

  b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const convertbyte = atob(b64Data);
    const byteCharacters = atob(convertbyte);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  downloadFile64(data: any) {
    this.status_loading = true;
    const contentType = '';
    this.status_loading = false;
    const b64Data = data.base64File;
    const filename = data.fileName;
    console.log('base64', b64Data);

    const config = this.base64(b64Data, contentType);
    saveAs(config, filename);
  }

  base64(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  hideDownloadFile() {
    if (this.dataSource_file.data.length > 0) {
      this.downloadfile_by = [];

      for (let index = 0; index < this.dataSource_file.data.length; index++) {
        const element = this.dataSource_file.data[index];
        console.log('ele', element);

        if ('path' in element) {
          this.downloadfile_by.push('path');
        } else {
          this.downloadfile_by.push('base64');
        }
      }
    }
    console.log('by:', this.downloadfile_by);
  }

  // ngOnDestroy(): void {
  //   const data = this.sidebarService.getsavePage();
  //   let saveInFoStatus = false;
  //   saveInFoStatus = data.saveStatusInfo;
  //   if (saveInFoStatus === true) {
  //     this.outOfPageSave = true;
  //     this.linkTopage = data.goToLink;
  //     console.log('ngOnDestroy', this.linkTopage);
  //     this.saveDraft();
  //     this.sidebarService.resetSaveStatusInfo();
  //   }
  // }

  // ngDoCheck() {
  //   if (this.openDoCheckc === true) {
  //     if (this.template_manage.caaStatus !== 'show') {
  //       this.pageChange = this.sidebarService.changePageGoToLink();
  //       if (this.pageChange) {
  //         // this.getNavbar();
  //         const data = this.sidebarService.getsavePage();
  //         let saveInFoStatus = false;
  //         saveInFoStatus = data.saveStatusInfo;
  //         if (saveInFoStatus === true) {
  //           this.outOfPageSave = true;
  //           this.linkTopage = data.goToLink;
  //           this.saveDraft('navbar', null);
  //           this.sidebarService.resetSaveStatu();
  //         }
  //       }
  //     }
  //   }
  // }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate request');
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef =  this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        if (data.returnStatus === true) {
          const raturnStatus = this.saveDraft();
          console.log('request raturnStatus', raturnStatus);
            if (raturnStatus) {
              console.log('raturnStatus T', this.saveDraftstatus);
              subject.next(true);
            } else {
              console.log('raturnStatus F', this.saveDraftstatus);
              localStorage.setItem('check', this.stepPage);
              subject.next(false);
            }
        } else if (data.returnStatus === false) {
          this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          localStorage.setItem('check', this.stepPage);
          subject.next(false);
        }
      });
      return subject;
    } else {
      return true;
    }
  }
}
