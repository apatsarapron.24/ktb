const dataRow = [
  {
      'key': '00',
      'value': ''
  },
  {
      'key': '10',
      'value': ''
  },
  {
      'key': '20',
      'value': ''
  },
  {
      'key': '01',
      'value': ''
  },
  {
      'key': '11',
      'value': ''
  },
  {
      'key': '21',
      'value': ''
  },
  {
      'key': '02',
      'value': ''
  },
  {
      'key': '12',
      'value': ''
  },
  {
      'key': '22',
      'value': ''
  },
  {
      'key': '03',
      'value': ''
  },
  {
      'key': '13',
      'value': ''
  },
  {
      'key': '23',
      'value': ''
  },
  {
      'key': '04',
      'value': ''
  },
  {
      'key': '14',
      'value': ''
  },
  {
      'key': '24',
      'value': ''
  },
  {
      'key': '05',
      'value': ''
  },
  {
      'key': '15',
      'value': ''
  },
  {
      'key': '25',
      'value': ''
  },
  {
      'key': '06',
      'value': ''
  },
  {
      'key': '16',
      'value': ''
  },
  {
      'key': '26',
      'value': ''
  }
];

export let defaultData: any = {
  applicant: [
    {
      id: null,
      topic: 'ประสบการณ์',
      detail: [
        {
            'key': '00',
            'value': ''
        },
        {
            'key': '10',
            'value': ''
        },
        {
            'key': '20',
            'value': ''
        },
        {
            'key': '01',
            'value': ''
        },
        {
            'key': '11',
            'value': ''
        },
        {
            'key': '21',
            'value': ''
        },
        {
            'key': '02',
            'value': ''
        },
        {
            'key': '12',
            'value': ''
        },
        {
            'key': '22',
            'value': ''
        },
        {
            'key': '03',
            'value': ''
        },
        {
            'key': '13',
            'value': ''
        },
        {
            'key': '23',
            'value': ''
        },
        {
            'key': '04',
            'value': ''
        },
        {
            'key': '14',
            'value': ''
        },
        {
            'key': '24',
            'value': ''
        },
        {
            'key': '05',
            'value': ''
        },
        {
            'key': '15',
            'value': ''
        },
        {
            'key': '25',
            'value': ''
        },
        {
            'key': '06',
            'value': ''
        },
        {
            'key': '16',
            'value': ''
        },
        {
            'key': '26',
            'value': ''
        }
    ],
    },

    {
      id: null,
      topic: 'อายุผู้กู้',
      detail: [
        {
            'key': '00',
            'value': ''
        },
        {
            'key': '10',
            'value': ''
        },
        {
            'key': '20',
            'value': ''
        },
        {
            'key': '01',
            'value': ''
        },
        {
            'key': '11',
            'value': ''
        },
        {
            'key': '21',
            'value': ''
        },
        {
            'key': '02',
            'value': ''
        },
        {
            'key': '12',
            'value': ''
        },
        {
            'key': '22',
            'value': ''
        },
        {
            'key': '03',
            'value': ''
        },
        {
            'key': '13',
            'value': ''
        },
        {
            'key': '23',
            'value': ''
        },
        {
            'key': '04',
            'value': ''
        },
        {
            'key': '14',
            'value': ''
        },
        {
            'key': '24',
            'value': ''
        },
        {
            'key': '05',
            'value': ''
        },
        {
            'key': '15',
            'value': ''
        },
        {
            'key': '25',
            'value': ''
        },
        {
            'key': '06',
            'value': ''
        },
        {
            'key': '16',
            'value': ''
        },
        {
            'key': '26',
            'value': ''
        }
    ],
    },
    {
      id: null,
      topic: 'QCA Rating/Credit Rating',
      detail: [
        {
            'key': '00',
            'value': ''
        },
        {
            'key': '10',
            'value': ''
        },
        {
            'key': '20',
            'value': ''
        },
        {
            'key': '01',
            'value': ''
        },
        {
            'key': '11',
            'value': ''
        },
        {
            'key': '21',
            'value': ''
        },
        {
            'key': '02',
            'value': ''
        },
        {
            'key': '12',
            'value': ''
        },
        {
            'key': '22',
            'value': ''
        },
        {
            'key': '03',
            'value': ''
        },
        {
            'key': '13',
            'value': ''
        },
        {
            'key': '23',
            'value': ''
        },
        {
            'key': '04',
            'value': ''
        },
        {
            'key': '14',
            'value': ''
        },
        {
            'key': '24',
            'value': ''
        },
        {
            'key': '05',
            'value': ''
        },
        {
            'key': '15',
            'value': ''
        },
        {
            'key': '25',
            'value': ''
        },
        {
            'key': '06',
            'value': ''
        },
        {
            'key': '16',
            'value': ''
        },
        {
            'key': '26',
            'value': ''
        }
    ],
    },
    {
      id: null,
      topic: 'KTB Grade',
      detail: [
        {
            'key': '00',
            'value': ''
        },
        {
            'key': '10',
            'value': ''
        },
        {
            'key': '20',
            'value': ''
        },
        {
            'key': '01',
            'value': ''
        },
        {
            'key': '11',
            'value': ''
        },
        {
            'key': '21',
            'value': ''
        },
        {
            'key': '02',
            'value': ''
        },
        {
            'key': '12',
            'value': ''
        },
        {
            'key': '22',
            'value': ''
        },
        {
            'key': '03',
            'value': ''
        },
        {
            'key': '13',
            'value': ''
        },
        {
            'key': '23',
            'value': ''
        },
        {
            'key': '04',
            'value': ''
        },
        {
            'key': '14',
            'value': ''
        },
        {
            'key': '24',
            'value': ''
        },
        {
            'key': '05',
            'value': ''
        },
        {
            'key': '15',
            'value': ''
        },
        {
            'key': '25',
            'value': ''
        },
        {
            'key': '06',
            'value': ''
        },
        {
            'key': '16',
            'value': ''
        },
        {
            'key': '26',
            'value': ''
        }
    ],
    },
    {
      id: null,
      topic: 'KTB Color',
      detail: [
        {
            'key': '00',
            'value': ''
        },
        {
            'key': '10',
            'value': ''
        },
        {
            'key': '20',
            'value': ''
        },
        {
            'key': '01',
            'value': ''
        },
        {
            'key': '11',
            'value': ''
        },
        {
            'key': '21',
            'value': ''
        },
        {
            'key': '02',
            'value': ''
        },
        {
            'key': '12',
            'value': ''
        },
        {
            'key': '22',
            'value': ''
        },
        {
            'key': '03',
            'value': ''
        },
        {
            'key': '13',
            'value': ''
        },
        {
            'key': '23',
            'value': ''
        },
        {
            'key': '04',
            'value': ''
        },
        {
            'key': '14',
            'value': ''
        },
        {
            'key': '24',
            'value': ''
        },
        {
            'key': '05',
            'value': ''
        },
        {
            'key': '15',
            'value': ''
        },
        {
            'key': '25',
            'value': ''
        },
        {
            'key': '06',
            'value': ''
        },
        {
            'key': '16',
            'value': ''
        },
        {
            'key': '26',
            'value': ''
        }
    ],
    },
  ],

  finan: [
    {
      id: null,
      topic: 'DSCR',
      detail: [
        {
            'key': '00',
            'value': ''
        },
        {
            'key': '10',
            'value': ''
        },
        {
            'key': '20',
            'value': ''
        },
        {
            'key': '01',
            'value': ''
        },
        {
            'key': '11',
            'value': ''
        },
        {
            'key': '21',
            'value': ''
        },
        {
            'key': '02',
            'value': ''
        },
        {
            'key': '12',
            'value': ''
        },
        {
            'key': '22',
            'value': ''
        },
        {
            'key': '03',
            'value': ''
        },
        {
            'key': '13',
            'value': ''
        },
        {
            'key': '23',
            'value': ''
        },
        {
            'key': '04',
            'value': ''
        },
        {
            'key': '14',
            'value': ''
        },
        {
            'key': '24',
            'value': ''
        },
        {
            'key': '05',
            'value': ''
        },
        {
            'key': '15',
            'value': ''
        },
        {
            'key': '25',
            'value': ''
        },
        {
            'key': '06',
            'value': ''
        },
        {
            'key': '16',
            'value': ''
        },
        {
            'key': '26',
            'value': ''
        }
    ],
    },

    {
      id: null,
      topic: 'D/E Ratio',
      detail: [
        {
            'key': '00',
            'value': ''
        },
        {
            'key': '10',
            'value': ''
        },
        {
            'key': '20',
            'value': ''
        },
        {
            'key': '01',
            'value': ''
        },
        {
            'key': '11',
            'value': ''
        },
        {
            'key': '21',
            'value': ''
        },
        {
            'key': '02',
            'value': ''
        },
        {
            'key': '12',
            'value': ''
        },
        {
            'key': '22',
            'value': ''
        },
        {
            'key': '03',
            'value': ''
        },
        {
            'key': '13',
            'value': ''
        },
        {
            'key': '23',
            'value': ''
        },
        {
            'key': '04',
            'value': ''
        },
        {
            'key': '14',
            'value': ''
        },
        {
            'key': '24',
            'value': ''
        },
        {
            'key': '05',
            'value': ''
        },
        {
            'key': '15',
            'value': ''
        },
        {
            'key': '25',
            'value': ''
        },
        {
            'key': '06',
            'value': ''
        },
        {
            'key': '16',
            'value': ''
        },
        {
            'key': '26',
            'value': ''
        }
    ],
    },
    {
      id: null,
      topic: 'กําไรสุทธ',
      detail: [
        {
            'key': '00',
            'value': ''
        },
        {
            'key': '10',
            'value': ''
        },
        {
            'key': '20',
            'value': ''
        },
        {
            'key': '01',
            'value': ''
        },
        {
            'key': '11',
            'value': ''
        },
        {
            'key': '21',
            'value': ''
        },
        {
            'key': '02',
            'value': ''
        },
        {
            'key': '12',
            'value': ''
        },
        {
            'key': '22',
            'value': ''
        },
        {
            'key': '03',
            'value': ''
        },
        {
            'key': '13',
            'value': ''
        },
        {
            'key': '23',
            'value': ''
        },
        {
            'key': '04',
            'value': ''
        },
        {
            'key': '14',
            'value': ''
        },
        {
            'key': '24',
            'value': ''
        },
        {
            'key': '05',
            'value': ''
        },
        {
            'key': '15',
            'value': ''
        },
        {
            'key': '25',
            'value': ''
        },
        {
            'key': '06',
            'value': ''
        },
        {
            'key': '16',
            'value': ''
        },
        {
            'key': '26',
            'value': ''
        }
    ],
    },
    {
      id: null,
      topic: 'ส่วนทุนของผู้ถือหุ้น',
      detail: [
        {
            'key': '00',
            'value': ''
        },
        {
            'key': '10',
            'value': ''
        },
        {
            'key': '20',
            'value': ''
        },
        {
            'key': '01',
            'value': ''
        },
        {
            'key': '11',
            'value': ''
        },
        {
            'key': '21',
            'value': ''
        },
        {
            'key': '02',
            'value': ''
        },
        {
            'key': '12',
            'value': ''
        },
        {
            'key': '22',
            'value': ''
        },
        {
            'key': '03',
            'value': ''
        },
        {
            'key': '13',
            'value': ''
        },
        {
            'key': '23',
            'value': ''
        },
        {
            'key': '04',
            'value': ''
        },
        {
            'key': '14',
            'value': ''
        },
        {
            'key': '24',
            'value': ''
        },
        {
            'key': '05',
            'value': ''
        },
        {
            'key': '15',
            'value': ''
        },
        {
            'key': '25',
            'value': ''
        },
        {
            'key': '06',
            'value': ''
        },
        {
            'key': '16',
            'value': ''
        },
        {
            'key': '26',
            'value': ''
        }
    ],
    },
    {
      id: null,
      topic: 'ทุนจดทะเบียน',
      detail: [
        {
            'key': '00',
            'value': ''
        },
        {
            'key': '10',
            'value': ''
        },
        {
            'key': '20',
            'value': ''
        },
        {
            'key': '01',
            'value': ''
        },
        {
            'key': '11',
            'value': ''
        },
        {
            'key': '21',
            'value': ''
        },
        {
            'key': '02',
            'value': ''
        },
        {
            'key': '12',
            'value': ''
        },
        {
            'key': '22',
            'value': ''
        },
        {
            'key': '03',
            'value': ''
        },
        {
            'key': '13',
            'value': ''
        },
        {
            'key': '23',
            'value': ''
        },
        {
            'key': '04',
            'value': ''
        },
        {
            'key': '14',
            'value': ''
        },
        {
            'key': '24',
            'value': ''
        },
        {
            'key': '05',
            'value': ''
        },
        {
            'key': '15',
            'value': ''
        },
        {
            'key': '25',
            'value': ''
        },
        {
            'key': '06',
            'value': ''
        },
        {
            'key': '16',
            'value': ''
        },
        {
            'key': '26',
            'value': ''
        }
    ],
    },
  ],
  status: [
    {
      id: null,
      topic: 'EWS Risk Level (สําหรับลูกค้าเดิม)',
      detail: [
        {
            'key': '00',
            'value': ''
        },
        {
            'key': '10',
            'value': ''
        },
        {
            'key': '20',
            'value': ''
        },
        {
            'key': '01',
            'value': ''
        },
        {
            'key': '11',
            'value': ''
        },
        {
            'key': '21',
            'value': ''
        },
        {
            'key': '02',
            'value': ''
        },
        {
            'key': '12',
            'value': ''
        },
        {
            'key': '22',
            'value': ''
        },
        {
            'key': '03',
            'value': ''
        },
        {
            'key': '13',
            'value': ''
        },
        {
            'key': '23',
            'value': ''
        },
        {
            'key': '04',
            'value': ''
        },
        {
            'key': '14',
            'value': ''
        },
        {
            'key': '24',
            'value': ''
        },
        {
            'key': '05',
            'value': ''
        },
        {
            'key': '15',
            'value': ''
        },
        {
            'key': '25',
            'value': ''
        },
        {
            'key': '06',
            'value': ''
        },
        {
            'key': '16',
            'value': ''
        },
        {
            'key': '26',
            'value': ''
        }
    ],
    },

    {
      id: null,
      topic: 'White List และ AML List',
      detail: [
        {
            'key': '00',
            'value': ''
        },
        {
            'key': '10',
            'value': ''
        },
        {
            'key': '20',
            'value': ''
        },
        {
            'key': '01',
            'value': ''
        },
        {
            'key': '11',
            'value': ''
        },
        {
            'key': '21',
            'value': ''
        },
        {
            'key': '02',
            'value': ''
        },
        {
            'key': '12',
            'value': ''
        },
        {
            'key': '22',
            'value': ''
        },
        {
            'key': '03',
            'value': ''
        },
        {
            'key': '13',
            'value': ''
        },
        {
            'key': '23',
            'value': ''
        },
        {
            'key': '04',
            'value': ''
        },
        {
            'key': '14',
            'value': ''
        },
        {
            'key': '24',
            'value': ''
        },
        {
            'key': '05',
            'value': ''
        },
        {
            'key': '15',
            'value': ''
        },
        {
            'key': '25',
            'value': ''
        },
        {
            'key': '06',
            'value': ''
        },
        {
            'key': '16',
            'value': ''
        },
        {
            'key': '26',
            'value': ''
        }
    ],
    },

    {
      id: null,
      topic: 'การปรับโครงสร้างหนี้',
      detail: [
        {
            'key': '00',
            'value': ''
        },
        {
            'key': '10',
            'value': ''
        },
        {
            'key': '20',
            'value': ''
        },
        {
            'key': '01',
            'value': ''
        },
        {
            'key': '11',
            'value': ''
        },
        {
            'key': '21',
            'value': ''
        },
        {
            'key': '02',
            'value': ''
        },
        {
            'key': '12',
            'value': ''
        },
        {
            'key': '22',
            'value': ''
        },
        {
            'key': '03',
            'value': ''
        },
        {
            'key': '13',
            'value': ''
        },
        {
            'key': '23',
            'value': ''
        },
        {
            'key': '04',
            'value': ''
        },
        {
            'key': '14',
            'value': ''
        },
        {
            'key': '24',
            'value': ''
        },
        {
            'key': '05',
            'value': ''
        },
        {
            'key': '15',
            'value': ''
        },
        {
            'key': '25',
            'value': ''
        },
        {
            'key': '06',
            'value': ''
        },
        {
            'key': '16',
            'value': ''
        },
        {
            'key': '26',
            'value': ''
        }
    ],
    },

    {
      id: null,
      topic: 'การเปลียนผู้ตรวจสอบบัญา',
      detail: [
        {
            'key': '00',
            'value': ''
        },
        {
            'key': '10',
            'value': ''
        },
        {
            'key': '20',
            'value': ''
        },
        {
            'key': '01',
            'value': ''
        },
        {
            'key': '11',
            'value': ''
        },
        {
            'key': '21',
            'value': ''
        },
        {
            'key': '02',
            'value': ''
        },
        {
            'key': '12',
            'value': ''
        },
        {
            'key': '22',
            'value': ''
        },
        {
            'key': '03',
            'value': ''
        },
        {
            'key': '13',
            'value': ''
        },
        {
            'key': '23',
            'value': ''
        },
        {
            'key': '04',
            'value': ''
        },
        {
            'key': '14',
            'value': ''
        },
        {
            'key': '24',
            'value': ''
        },
        {
            'key': '05',
            'value': ''
        },
        {
            'key': '15',
            'value': ''
        },
        {
            'key': '25',
            'value': ''
        },
        {
            'key': '06',
            'value': ''
        },
        {
            'key': '16',
            'value': ''
        },
        {
            'key': '26',
            'value': ''
        }
    ],
    },
    {
      id: null,
      topic: 'การฟ้องร้องเกี่ยวกับการเงิน',
      detail: [
        {
            'key': '00',
            'value': ''
        },
        {
            'key': '10',
            'value': ''
        },
        {
            'key': '20',
            'value': ''
        },
        {
            'key': '01',
            'value': ''
        },
        {
            'key': '11',
            'value': ''
        },
        {
            'key': '21',
            'value': ''
        },
        {
            'key': '02',
            'value': ''
        },
        {
            'key': '12',
            'value': ''
        },
        {
            'key': '22',
            'value': ''
        },
        {
            'key': '03',
            'value': ''
        },
        {
            'key': '13',
            'value': ''
        },
        {
            'key': '23',
            'value': ''
        },
        {
            'key': '04',
            'value': ''
        },
        {
            'key': '14',
            'value': ''
        },
        {
            'key': '24',
            'value': ''
        },
        {
            'key': '05',
            'value': ''
        },
        {
            'key': '15',
            'value': ''
        },
        {
            'key': '25',
            'value': ''
        },
        {
            'key': '06',
            'value': ''
        },
        {
            'key': '16',
            'value': ''
        },
        {
            'key': '26',
            'value': ''
        }
    ],
    },
    {
      id: null,
      topic: 'การตรวจสอบเครดิตบูโร',
      detail: [
        {
            'key': '00',
            'value': ''
        },
        {
            'key': '10',
            'value': ''
        },
        {
            'key': '20',
            'value': ''
        },
        {
            'key': '01',
            'value': ''
        },
        {
            'key': '11',
            'value': ''
        },
        {
            'key': '21',
            'value': ''
        },
        {
            'key': '02',
            'value': ''
        },
        {
            'key': '12',
            'value': ''
        },
        {
            'key': '22',
            'value': ''
        },
        {
            'key': '03',
            'value': ''
        },
        {
            'key': '13',
            'value': ''
        },
        {
            'key': '23',
            'value': ''
        },
        {
            'key': '04',
            'value': ''
        },
        {
            'key': '14',
            'value': ''
        },
        {
            'key': '24',
            'value': ''
        },
        {
            'key': '05',
            'value': ''
        },
        {
            'key': '15',
            'value': ''
        },
        {
            'key': '25',
            'value': ''
        },
        {
            'key': '06',
            'value': ''
        },
        {
            'key': '16',
            'value': ''
        },
        {
            'key': '26',
            'value': ''
        }
    ],
    },
  ],
  other: [
    {
      id: null,
      topic: 'Core Asset',
      detail: [
        {
            'key': '00',
            'value': ''
        },
        {
            'key': '10',
            'value': ''
        },
        {
            'key': '20',
            'value': ''
        },
        {
            'key': '01',
            'value': ''
        },
        {
            'key': '11',
            'value': ''
        },
        {
            'key': '21',
            'value': ''
        },
        {
            'key': '02',
            'value': ''
        },
        {
            'key': '12',
            'value': ''
        },
        {
            'key': '22',
            'value': ''
        },
        {
            'key': '03',
            'value': ''
        },
        {
            'key': '13',
            'value': ''
        },
        {
            'key': '23',
            'value': ''
        },
        {
            'key': '04',
            'value': ''
        },
        {
            'key': '14',
            'value': ''
        },
        {
            'key': '24',
            'value': ''
        },
        {
            'key': '05',
            'value': ''
        },
        {
            'key': '15',
            'value': ''
        },
        {
            'key': '25',
            'value': ''
        },
        {
            'key': '06',
            'value': ''
        },
        {
            'key': '16',
            'value': ''
        },
        {
            'key': '26',
            'value': ''
        }
    ],
    },
  ],
};
