import {
  Component,
  OnInit,
  ViewChild,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { FormControl, FormGroup } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';
import { StepThreeService } from '../../../../../services/request/product-detail/step-three/step-three.service';
import { RequestService } from '../../../../../services/request/request.service';
// import { StepThreeComponent } from '../step-three.component';
import { Router, NavigationEnd } from '@angular/router';
import { defaultData } from './default';
import { saveAs } from 'file-saver';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogSaveStatusComponent } from '../../../dialog/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from '../../../../../services/configGuard/config-guard.service';


const sSME = [];

export interface FileList {
  fileName: string;
  fileSize: string;
  base64File: string;
}

const File_data: FileList[] = [];

@Component({
  selector: 'app-personal-loan',
  templateUrl: './personal-loan.component.html',
  styleUrls: ['./personal-loan.component.scss'],
})
export class PersonalLoanComponent implements OnInit, CanComponentDeactivate {

  constructor(
    private router: Router,
    private StepThree: StepThreeService,
    private SideBarService: RequestService,
    public dialog: MatDialog,
  ) { }
  @Input() template_manage = { role: null, validate: null, caaStatus: null, pageShow: null };
  @Output() saveStatus = new EventEmitter<boolean>();
  displayedColumns_file: string[] = ['fileName', 'fileSize', 'delete'];
  dataSource_file: MatTableDataSource<FileList>;
  checkLocal: any;
  pageLast: any;

  status_loading = true;
  validate = null;
  header = [
    {
      name: 'loandType',
      value: 'ประเภทสินเชื่อ',
    },
    {
      name: 'limitType',
      value: 'ประเภทวงเงิน',
    },
    {
      name: 'limit',
      value: 'วงเงินสินเชื่อสูงสุด (ลบ.)',
    },
    {
      name: 'preriod',
      value: 'ระยะเวลาให้กู้สูงสุด (ปี)',
    },
  ];
  sub_header = [
    {
      name: 'min',
      value: 'Min',
    },
    {
      name: 'max',
      value: 'Max',
    },
    {
      name: 'avg',
      value: 'Average',
    },
  ];
  all_header = [
    {
      name: 'loandType',
      value: 'ประเภทสินเชื่อ',
    },
    {
      name: 'limitType',
      value: 'ประเภทวงเงิน',
    },
    {
      name: 'limit',
      value: 'วงเงินสินเชื่อสูงสุด (ลบ.)',
    },
    {
      name: 'preriod',
      value: 'ระยะเวลาให้กู้สูงสุด (ปี)',
    },
    {
      name: 'min',
      value: 'Min',
    },
    {
      name: 'max',
      value: 'Max',
    },
    {
      name: 'avg',
      value: 'Average',
    },
  ];
  stepThree_2: any = {
    requestId: '',
    status: false,
    agreement: [
      {
        topic: 'MOU',
        flag: false,
      },
      {
        topic: 'Non-MOU',
        flag: false,
      },
      {
        topic: 'ไม่กำหนด',
        flag: false,
      },
    ],
    career: [
      {
        topic: 'ข้าราชการ',
        flag: false,
      },
      {
        topic: 'พนักงานรัฐวิสาหกิจ',
        flag: false,
      },
      {
        topic: 'พนักงานราชการ',
        flag: false,
      },
      {
        topic: 'พนักงานหน่วยงานเอกชน',
        flag: false,
      },
      {
        topic: 'กลุ่มพนักงานบริษัทในเครือ Krungthai',
        flag: false,
      },
      {
        topic: 'บุคคลทั่วไปที่มีรายได้ประจำ',
        flag: false,
      },
      {
        topic: 'ผู้ประกอบการร้านค้าย่อยทั่วไป',
        flag: false,
      },
    ],
    guaranty: [
      {
        topic: 'เงินฝาก ',
        flag: false,
        child: [],
      },
      {
        topic: 'ตราสารหนี้',
        flag: false,
        child: [
          {
            topic: 'พันธบัตร หุ้นกู้ หรือตั๋วเงิน',
            flag: false,
          },
          {
            topic: 'หุ้นบุริมสิทธิหรือหุ้นสามัญ',
            flag: false,
          },
          {
            topic: 'หน่วยลงทุนทุกประเภทกองทุนรวม',
            flag: false,
          },
          {
            topic: 'หน่วยลงทุนประเภทกองทุนเปิด',
            flag: false,
          },
        ],
      },
      {
        topic: 'อสังหาริมทรัพย์',
        flag: false,
        child: [
          {
            topic: 'ที่ดินว่างเปล่า',
            flag: false,
          },
          {
            topic: 'ที่ดินว่างเปล่าที่มีสภาพคล่องต่ำ',
            flag: false,
          },
          {
            topic: 'ที่ดินในนิคมอุตสาหกรรม',
            flag: false,
          },
          {
            topic: 'ที่ดินพร้อมสิ่งปลูกสร้าง',
            flag: false,
          },
          {
            topic: 'รรมสิทธิ์อาคารชุด',
            flag: false,
          },
          {
            topic: 'อู่ซ่อมเรือ',
            flag: false,
          },
          {
            topic: 'อสังหาริมทรัพย์อื่นๆ',
            flag: false,
          },
        ],
      },
      {
        topic: 'สินค้า/เครื่องจักร/เรือ',
        flag: false,
        child: [
          {
            topic: 'เครื่องจักร',
            flag: false,
          },
          {
            topic: 'สต็อคสินค้าในคลังสินค้า',
            flag: false,
          },
          {
            topic: 'เรือ',
            flag: false,
          },
        ],
      },
      {
        topic: 'การค้ำประกัน',
        flag: false,
        child: [
          {
            topic: 'หนังสือค้ำประกัน Standby Letter of Credit',
            flag: false,
          },
          {
            topic: 'การค้ำประกันโดยกระทรวงการคลัง',
            flag: false,
          },
          {
            topic: 'การค้ำประกันโดยสถาบันการเงิน',
            flag: false,
          },
          {
            topic: 'การค้ำประกันโดยบรรษัทสินเชื่ออุตสาหกรรมขนาดย่อม',
            flag: false,
          },
        ],
      },
      {
        topic: 'โอนสิทธิการเช่า',
        flag: false,
        child: [],
      },
      {
        topic: 'บุคคลค้ำประกัน',
        flag: false,
        child: [],
      },
      {
        topic: 'อื่นๆ',
        flag: false,
        child: [],
        otherDetail: '',
      },
    ],
    loan: {
      file: [],
      fileDelete: [],
    },
    loanText: '',
    interest: {
      file: [],
      fileDelete: [],
    },
    interestText: '',
    interestApprove: {
      file: [],
      fileDelete: [],
    },
    interestApproveText: '',
    other: {
      file: [],
      fileDelete: [],
    },
    otherText: '',
    attachments: {
      file: [],
      fileDelete: [],
    },
    considerDelete: [],
    credit: [],
    creditDelete: [],
    consider: {
      applicant: [],
      finan: [],
      status: [],
      other: [],
    },
    conclusions: null
  };

  add_data: any = {
    id: '',
    loandType: '',
    limitType: '',
    limit: '',
    preriod: '',
    max: '',
    min: '',
    avg: '',
  };

  data = sSME;

  checkbok_goal_l: any = {
    MOU: false,
    Non_MOU: false,
    No_gain: false,
  };

  check_box: any = [
    {
      MOU: false,
      job: [],
    },
    {
      'Non-MOU': false,
      job: [],
    },
    {
      'No-gain': false,
      job: [],
    },
  ];
  career: any = [
    {
      topic: 'ข้าราชการ',
      flag: true,
    },
    {
      topic: 'พนักงานรัฐวิสาหกิจ',
      flag: false,
    },
    {
      topic: 'พนักงานราชการ',
      flag: false,
    },
    {
      topic: 'พนักงานหน่วยงานเอกชน',
      flag: false,
    },
    {
      topic: 'กลุ่มพนักงานบริษัทในเครือ Krungthai',
      flag: false,
    },
    {
      topic: 'บุคคลทั่วไปที่มีรายได้ประจำ',
      flag: false,
    },
    {
      topic: 'ผู้ประกอบการร้านค้าย่อยทั่วไป',
      flag: false,
    },
  ];

  credit_data: any = {
    loandType: '',
    limitType: '',
    limit: '',
    preriod: '',
    max: '',
    min: '',
    avg: '',
  };

  dataRow: any = [
    {
      'key': '00',
      'value': ''
    },
    {
      'key': '10',
      'value': ''
    },
    {
      'key': '20',
      'value': ''
    },
    {
      'key': '01',
      'value': ''
    },
    {
      'key': '11',
      'value': ''
    },
    {
      'key': '21',
      'value': ''
    },
    {
      'key': '02',
      'value': ''
    },
    {
      'key': '12',
      'value': ''
    },
    {
      'key': '22',
      'value': ''
    },
    {
      'key': '03',
      'value': ''
    },
    {
      'key': '13',
      'value': ''
    },
    {
      'key': '23',
      'value': ''
    },
    {
      'key': '04',
      'value': ''
    },
    {
      'key': '14',
      'value': ''
    },
    {
      'key': '24',
      'value': ''
    },
    {
      'key': '05',
      'value': ''
    },
    {
      'key': '15',
      'value': ''
    },
    {
      'key': '25',
      'value': ''
    },
    {
      'key': '06',
      'value': ''
    },
    {
      'key': '16',
      'value': ''
    },
    {
      'key': '26',
      'value': ''
    }
  ];

  columnTitle = [];
  keyItem = [];

  results: any;
  modifiedDate: any;

  saveDraftstatus = null;
  outOfPageSave = false;
  linkTopage = null;
  pageChange = null;
  openDoCheckc = false;

  showCircle_colId = null;
  showCircle_id = null;
  enter = 3;

  data_pic: any = {
    table_Main: {
      table: [],
    },
    objective: {
      objectiveDetail: null,
      uploadFile: [],
    },
    businessModel: {
      businessModelDetail: null,
      uploadFile: [],
    },
    productCharacteristics: {
      productCharacteristicsDetail: null,
      uploadFile: [],
    },
    another: {
      anotherDetail: null,
      uploadFile: [],
    },
  };
  file1_1: any;
  file1_2: any;
  file2_1: any;
  file2_2: any;
  file3_1: any;
  file3_2: any;
  file4_1: any;
  file4_2: any;
  document: any;
  file_name: any;
  file_size: number;

  imgURL: any;
  image1 = [];
  image2 = [];
  image3 = [];
  image4 = [];
  showImage1 = false;
  message = null;

  // Model
  showModel = false;
  modalmainImageIndex = 0;
  modalmainImagedetailIndex = 0;
  indexSelect = 0;
  modalImage = [];
  modalImagedetail = [];
  fileValue = 0;

  urls: any = [];

  get_data: any;
  group_data: any;

  base64Image: any;
  fileName: any;

  requestId: any;

  file_img1: any = [];
  file_img2: any = [];
  file_img3: any = [];
  file_img4: any = [];

  file_img1_delete: any = [];
  file_img2_delete: any = [];
  file_img3_delete: any = [];
  file_img4_delete: any = [];

  loandetail = false;
  agreement = false;
  career_1 = false;
  guaranty = false;
  loanText = false;
  interestText = false;
  interestApproveText = false;
  otherText = false;

  next_page: any;

  // textareaForm = new FormGroup({
  //   loanText: new FormControl(this.stepThree_2.loanText),
  //   interestText: new FormControl(this.stepThree_2.interestText),
  //   interestApproveText: new FormControl(this.stepThree_2.interestApproveText),
  //   otherText: new FormControl(this.stepThree_2.otherText),
  // });

  public default = defaultData;

  FileOverSize: any;
  stepPage: any;
  testTable = [];
  cNameee = [];
  colTable: any = 0;
  repeat = false;
  downloadfile_by = [];

  sendSaveStatus() {
    if (this.saveDraftstatus === 'success') {
      this.saveStatus.emit(true);
      this.status_loading = false;
    } else {
      this.saveStatus.emit(false);
      this.status_loading = false;
    }
  }

  changeSaveDraft() {
    this.saveDraftstatus = null;
    this.sendSaveStatus();
    this.SideBarService.inPageStatus(true);
  }

  checkKey(rowKey): boolean {
    // console.log('rowKey', rowKey);
    return this.columnTitle.some((k) => rowKey === k.key);
  }
  findColumn() {
    this.columnTitle = [];
    console.log(this.stepThree_2);
    this.stepThree_2.career.forEach((itemCareer, index) => {
      // console.log('itemCareer', itemCareer);
      this.stepThree_2.agreement.forEach((itemAgree, i) => {
        // console.log('itemAgree', itemAgree)
        if (itemAgree.flag === true && itemCareer.flag === true) {
          const template = {
            key: i.toString() + index.toString(),
            Agree: itemAgree.topic,
            Carrer: itemCareer.topic,
          };
          this.columnTitle.push(template);
        }
      });
    });

    // console.log('this.columnTitle>>>', this.columnTitle)


    this.testTable = [];
    this.cNameee = [];
    this.colTable = 0;
    this.stepThree_2.agreement.forEach((itemAgree, i) => {
      if (itemAgree.flag === true) {
        this.cNameee.push(itemAgree);
      }
    });
    this.stepThree_2.career.forEach((itemCareer, index) => {
      if (itemCareer.flag === true) {
        const hTable = {
          mName: itemCareer.topic,
          cName: this.cNameee
        };
        this.testTable.push(hTable);
      }
    });
    this.colTable = this.cNameee.length;
    console.log('this.testTable', this.testTable);
  }

  changeCheckbox() {
    this.findColumn();
    console.log(this.columnTitle);

    // this.genkey();
  }
  ngOnInit() {
    this.openDoCheckc = true;
    this.stepPage = localStorage.getItem('check');
    this.checkLocal = localStorage.getItem('check');
    this.pageLast = (sessionStorage.getItem('page'));
    console.log('pageeee', this.pageLast);
    this.saveDraftstatus = null;
    if (localStorage) {
      this.requestId = Number(localStorage.getItem('requestId'));
    } else {
      this.status_loading = false;
    }
    // console.log('this.columnTitle', this.columnTitle);

    this.SideBarService.sendEvent();
    this.SideBarService.statusSibar(localStorage.getItem('requestId')).subscribe(check => {
      console.log('ckeck ทบทวน', check);
      if (check['data'] !== null) {
        this.repeat = check['data']['repeat'];
      } else {
        this.repeat = false;
      }
    });
    this.StepThree.get_step_three_personal(this.requestId, this.template_manage.pageShow).subscribe((res) => {
      this.get_data = res;
      this.group_data = this.get_data.data;
      if (res['status'] === 'success') {
        this.status_loading = false;
        this.stepThree_2['parentRequestId'] = res['parentRequestId'];
      }
      if (this.group_data == null || this.group_data === []) {
        // this.formData = this.formData;
        this.group_data = this.stepThree_2;
        this.requestId = Number(localStorage.getItem('requestId'));
        console.log('ddddddddd', this.group_data);
        this.validate = res['validate'];

        if (this.stepThree_2.consider.applicant.length === 0) {
          for (let index = 0; index < this.default.applicant.length; index++) {
            this.stepThree_2.consider.applicant.push(this.default.applicant[index]);
          }
        }

        if (this.stepThree_2.consider.finan.length === 0) {
          for (let index = 0; index < this.default.finan.length; index++) {
            this.stepThree_2.consider.finan.push(this.default.finan[index]);
          }
        }

        if (this.stepThree_2.consider.status.length === 0) {
          for (let index = 0; index < this.default.status.length; index++) {
            this.stepThree_2.consider.status.push(this.default.status[index]);
          }
        }

        if (this.stepThree_2.consider.other.length === 0) {
          for (let index = 0; index < this.default.other.length; index++) {
            this.stepThree_2.consider.other.push(this.default.other[index]);
          }
        }
      } else {
        this.group_data = this.get_data.data;
        this.validate = res['validate'];

        // formatted textarea
        // document.getElementById('loanText').addEventListener('paste', function (e: ClipboardEvent) {
        //   e.preventDefault();
        //   const text = e.clipboardData.getData('text/plain');
        //   document.execCommand('insertHTML', false, text);
        // });
        // document.getElementById('interestText').addEventListener('paste', function (e: ClipboardEvent) {
        //   e.preventDefault();
        //   const text = e.clipboardData.getData('text/plain');
        //   document.execCommand('insertHTML', false, text);
        // });
        // document.getElementById('interestApproveText').addEventListener('paste', function (e: ClipboardEvent) {
        //   e.preventDefault();
        //   const text = e.clipboardData.getData('text/plain');
        //   document.execCommand('insertHTML', false, text);
        // });
        // document.getElementById('otherText').addEventListener('paste', function (e: ClipboardEvent) {
        //   e.preventDefault();
        //   const text = e.clipboardData.getData('text/plain');
        //   document.execCommand('insertHTML', false, text);
        // });

      }
      this.dataSource_file = new MatTableDataSource(File_data);
      this.hideDownloadFile();
      this.urls = [];
      this.get_data_res();
    });

  }

  autoGrowTextZone(e) {
    e.target.style.height = '0px';
    e.target.style.height = (e.target.scrollHeight + 5) + 'px';
  }
  autogrow(index: any) {

    if (index === 1) {
      const loanText = document.getElementById('loanText');
      loanText.style.overflow = 'hidden';
      loanText.style.height = 'auto';
      loanText.style.height = loanText.scrollHeight + 'px';
    }
    if (index === 2) {
      const interestText = document.getElementById('interestText');
      interestText.style.overflow = 'hidden';
      interestText.style.height = 'auto';
      interestText.style.height = interestText.scrollHeight + 'px';
    }
    if (index === 3) {
      const interestApproveText = document.getElementById('interestApproveText');
      interestApproveText.style.overflow = 'hidden';
      interestApproveText.style.height = 'auto';
      interestApproveText.style.height = interestApproveText.scrollHeight + 'px';
    }
    if (index === 4) {
      const otherText = document.getElementById('otherText');
      otherText.style.overflow = 'hidden';
      otherText.style.height = 'auto';
      otherText.style.height = otherText.scrollHeight + 'px';
    }
  }


  saveDraft(page?, action?) {
    // this.saveDraftstatus = "success";
    console.log('request saveDraft');
    
    const result = this.check_text_before();
    this.status_loading = true;

    if (this.template_manage.pageShow === 'summary') {
      this.stepThree_2.conclusions = true;
    } else {
      this.stepThree_2.conclusions = null;
    }
    if (this.stepThree_2.guaranty[7].otherDetail === '' && this.stepThree_2.guaranty[7].flag === true) {
      this.status_loading = false;
      this.saveDraftstatus = 'fail';
      console.log('888888888888888888');
      alert('กรุณากรอกช่องอื่นๆ ในประเภทหลักประกัน');
      return false;
    } else if (result === true) {
      console.log('start::', this.get_data);
      console.log('start:: data.status', this.stepThree_2.status);
      // this.status_loading = false;
      const returnUpdateData = new Subject<any>();
      this.StepThree.update_step_three_personal(this.stepThree_2).subscribe(
        (res) => {
          if (res['status'] === 'success') {
            this.downloadfile_by = [];
            this.saveDraftstatus = 'success';
            returnUpdateData.next(true);
            this.SideBarService.inPageStatus(false);
            // this.status_loading = false;
            setTimeout(() => {
              this.saveDraftstatus = 'fail';
            }, 2000);
            this.sendSaveStatus();
            console.log('rs::', res);

            // this.stepThree_2.consider.applicant = [];
            // this.stepThree_2.consider.finan = [];
            // this.stepThree_2.consider.status = [];
            // this.stepThree_2.consider.other = [];
            this.dataSource_file = new MatTableDataSource(File_data);
            this.urls = [];
            this.file_img1 = [];
            this.file_img2 = [];
            this.file_img3 = [];
            this.file_img4 = [];

            this.image1 = [];
            this.image2 = [];
            this.image3 = [];
            this.image4 = [];

            this.file_img1_delete = [];
            this.file_img2_delete = [];
            this.file_img3_delete = [];
            this.file_img4_delete = [];



            this.data_pic.objective.uploadFile = [];
            this.data_pic.businessModel.uploadFile = [];
            this.data_pic.productCharacteristics.uploadFile = [];
            this.data_pic.another.uploadFile = [];

            console.log(' this.dataSource_file::', this.dataSource_file);
            console.log('  this.urls::', this.urls);

            // tslint:disable-next-line: no-shadowed-variable


            this.stepThree_2.credit = [];
            this.stepThree_2.creditDelete = [];
            this.StepThree.get_step_three_personal(this.requestId).subscribe(
              // tslint:disable-next-line: no-shadowed-variable
              resss => {
                if (resss['status'] === 'success') {
                  // this.status_loading = false;
                  this.get_data = resss;
                  this.group_data = this.get_data.data;

                  if (this.group_data == null || this.group_data === []) {
                    // this.formData = this.formData;
                    this.group_data = this.stepThree_2;
                    this.requestId = Number(localStorage.getItem('requestId'));
                    console.log('ddddddddd', this.group_data);
                  } else {
                    this.group_data = this.get_data.data;
                  }
                  if (this.saveDraftstatus === 'success') {
                    this.status_loading = false;
                    this.get_data_res();
                  }
                }
              }
            );
            this.SideBarService.sendEvent();
            // tslint:disable-next-line: no-shadowed-variable
          } else if (res['status'] === 'fail') {
            this.status_loading = false;
            this.sendSaveStatus();

            // alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
            alert(res['message']);
            console.log('message::', res['message']);
            returnUpdateData.next(false);
          }
        },
        (err) => {
          console.log(err);
          this.status_loading = false;
          alert(err['message']);
          // alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
          returnUpdateData.next(false);
        }
      );
      return returnUpdateData;
    } else {
      this.status_loading = false;
      return false;
    }
  }

  nextpage(action) {
    this.StepThree.CheckNextPage(localStorage.getItem('check'), action);
  }

  viewNext() {
    this.StepThree.CheckNextPage(localStorage.getItem('check'), 'next');
  }

  check_text_before() {
    this.loandetail = false;
    this.agreement = false;
    this.career_1 = false;
    this.guaranty = false;
    this.loanText = false;
    this.interestText = false;
    this.interestApproveText = false;
    // this.otherText = false;
    for (let index = 0; index < this.stepThree_2.career.length; index++) {
      if (this.stepThree_2.career[index].flag === true) {
        this.career_1 = true;
        break;
      }
    }
    for (let index = 0; index < this.stepThree_2.agreement.length; index++) {
      if (this.stepThree_2.agreement[index].flag === true) {
        this.agreement = true;
        break;
      }
    }

    for (let index = 0; index < this.stepThree_2.guaranty.length; index++) {
      if (
        this.stepThree_2.guaranty[index].flag === true &&
        this.stepThree_2.guaranty[index].child.length === 0
      ) {
        this.guaranty = true;
        break;
      } else if (
        this.stepThree_2.guaranty[index].flag === true &&
        this.stepThree_2.guaranty[index].child.length !== 0
      ) {
        for (
          let i = 0;
          i < this.stepThree_2.guaranty[index].child.length;
          i++
        ) {
          if (this.stepThree_2.guaranty[index].child[i].flag === true) {
            this.guaranty = true;
            break;
          }
        }
      }
    }

    if (this.stepThree_2.loanText !== '') {
      this.loanText = true;
    }
    if (this.stepThree_2.interestText !== '') {
      this.interestText = true;
    }

    if (this.stepThree_2.interestApproveText !== '') {
      this.interestApproveText = true;
    }

    // if (this.stepThree_2.otherText !== '') {
    //   this.otherText = true;
    // }

    let _statustable = false;
    console.log('0000 this.stepThree_2.credit:', this.stepThree_2.credit);
    if (this.stepThree_2.credit.length > 0) {
      for (let index = 0; index < this.stepThree_2.credit.length; index++) {
        const element = this.stepThree_2.credit[index];
        if (element) {
          if (element.loandType === ''
            || element.limitType === ''
            || element.limit === null
            || element.preriod === null
            || element.max === null
            || element.min === null
            || element.avg === null
          ) {
            _statustable = false;
            break;
          } else {
            _statustable = true;
          }
        }

      }
      this.loandetail = _statustable;
    } else {
      this.loandetail = false;
    }

    if (
      this.loandetail === true &&
      this.agreement === true &&
      this.career_1 === true &&
      this.guaranty === true &&
      this.loanText === true &&
      this.interestText === true &&
      this.interestApproveText === true
      // this.otherText === true
    ) {
      // this.saveDraftstatus = "success";
      console.log('form:', this.stepThree_2);
      this.stepThree_2.status = true;
      return true;
    } else {
      this.status_loading = false;
      // alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
      console.log('agreement:', this.agreement);
      console.log('career_1:', this.career_1);
      console.log('guaranty:', this.guaranty);
      console.log('loanText:', this.loanText);
      console.log('interestText:', this.interestText);
      console.log('interestApproveText:', this.interestApproveText);
      console.log('otherText:', this.otherText);
      console.log('err:', this.stepThree_2);
      this.stepThree_2.status = false;
      // this.saveDraftstatus = '';
      return true;
    }
  }
  get_data_res() {
    this.stepThree_2.requestId = Number(this.requestId);

    console.log('group_data:', this.group_data);
    console.log('stepThree_2:', this.stepThree_2);

    // console.log(this.stepThree_2.agreement, this.columnTitle, this.career);
    // console.log('get this.columnTitle', this.columnTitle);
    // console.log('career', this.group_data.career);
    // console.log('consider:', this.group_data.consider);
    this.data = this.group_data.credit;
    this.data_pic.table_Main.table = this.data;
    this.stepThree_2.credit = this.data;
    // if (this.group_data.agreement[0].flag === true) {
    //   this.group_data.agreement[1].flag = false;
    // }
    this.stepThree_2.agreement = [
      {
        topic: 'MOU',
        flag: this.group_data.agreement[0].flag,
      },
      {
        topic: 'Non-MOU',
        flag: this.group_data.agreement[1].flag,
      },
      {
        topic: 'ไม่กำหนด',
        flag: this.group_data.agreement[2].flag,
      },
    ];

    this.stepThree_2.career = [
      {
        topic: 'ข้าราชการ',
        flag: this.group_data.career[0].flag,
      },
      {
        topic: 'พนักงานรัฐวิสาหกิจ',
        flag: this.group_data.career[1].flag,
      },
      {
        topic: 'พนักงานราชการ',
        flag: this.group_data.career[2].flag,
      },
      {
        topic: 'พนักงานหน่วยงานเอกชน',
        flag: this.group_data.career[3].flag,
      },
      {
        topic: 'กลุ่มพนักงานบริษัทในเครือ Krungthai',
        flag: this.group_data.career[4].flag,
      },
      {
        topic: 'บุคคลทั่วไปที่มีรายได้ประจำ',
        flag: this.group_data.career[5].flag,
      },
      {
        topic: 'ผู้ประกอบการร้านค้าย่อยทั่วไป',
        flag: this.group_data.career[6].flag,
      },
    ];

    this.stepThree_2.guaranty[0].flag = this.group_data.guaranty[0].flag;

    this.stepThree_2.guaranty[1].flag = this.group_data.guaranty[1].flag;
    this.stepThree_2.guaranty[1].child[0].flag = this.group_data.guaranty[1].child[0].flag;
    this.stepThree_2.guaranty[1].child[1].flag = this.group_data.guaranty[1].child[1].flag;
    this.stepThree_2.guaranty[1].child[2].flag = this.group_data.guaranty[1].child[2].flag;
    this.stepThree_2.guaranty[1].child[3].flag = this.group_data.guaranty[1].child[3].flag;

    this.stepThree_2.guaranty[2].flag = this.group_data.guaranty[2].flag;
    this.stepThree_2.guaranty[2].child[0].flag = this.group_data.guaranty[2].child[0].flag;
    this.stepThree_2.guaranty[2].child[1].flag = this.group_data.guaranty[2].child[1].flag;
    this.stepThree_2.guaranty[2].child[2].flag = this.group_data.guaranty[2].child[2].flag;
    this.stepThree_2.guaranty[2].child[3].flag = this.group_data.guaranty[2].child[3].flag;
    this.stepThree_2.guaranty[2].child[4].flag = this.group_data.guaranty[2].child[4].flag;
    this.stepThree_2.guaranty[2].child[5].flag = this.group_data.guaranty[2].child[5].flag;
    this.stepThree_2.guaranty[2].child[6].flag = this.group_data.guaranty[2].child[6].flag;

    this.stepThree_2.guaranty[3].flag = this.group_data.guaranty[3].flag;
    this.stepThree_2.guaranty[3].child[0].flag = this.group_data.guaranty[3].child[0].flag;
    this.stepThree_2.guaranty[3].child[1].flag = this.group_data.guaranty[3].child[1].flag;
    this.stepThree_2.guaranty[3].child[2].flag = this.group_data.guaranty[3].child[2].flag;

    this.stepThree_2.guaranty[4].flag = this.group_data.guaranty[4].flag;
    this.stepThree_2.guaranty[4].child[0].flag = this.group_data.guaranty[4].child[0].flag;
    this.stepThree_2.guaranty[4].child[1].flag = this.group_data.guaranty[4].child[1].flag;
    this.stepThree_2.guaranty[4].child[2].flag = this.group_data.guaranty[4].child[2].flag;
    this.stepThree_2.guaranty[4].child[3].flag = this.group_data.guaranty[4].child[3].flag;

    this.stepThree_2.guaranty[5].flag = this.group_data.guaranty[5].flag;
    this.stepThree_2.guaranty[6].flag = this.group_data.guaranty[6].flag;
    this.stepThree_2.guaranty[7].flag = this.group_data.guaranty[7].flag;
    this.stepThree_2.guaranty[7].otherDetail = this.group_data.guaranty[7].otherDetail;
    if (this.stepThree_2.guaranty[7].flag === false) {
      this.stepThree_2.guaranty[7].otherDetail = '';
    }
    // -----------------------------set Default---------------------------------------
    this.stepThree_2.consider.applicant = this.group_data.consider.applicant;
    this.stepThree_2.consider.finan = this.group_data.consider.finan;
    this.stepThree_2.consider.status = this.group_data.consider.status;
    this.stepThree_2.consider.other = this.group_data.consider.other;

    console.log('default:', this.default);

    console.log('consider:', this.stepThree_2.consider);

    this.findColumn();

    // ***************************************************************************************************/
    this.data_pic.objective.objectiveDetail = this.group_data.loanText;
    this.stepThree_2.loanText = this.data_pic.objective.objectiveDetail;
    for (let index = 0; index < this.group_data.loan.file.length; index++) {
      this.base64Image = this.group_data.loan.file[index].base64File;
      this.fileName = this.group_data.loan.file[index].fileName;
      const byteString =
        'data:image/jpeg;base64,' + this.base64Image.split(',')[1];
      const ab = new ArrayBuffer(byteString.length);
      const ia = new Uint8Array(ab);
      for (let i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
      }
      fetch('data:image/jpeg;base64,' + this.base64Image)
        .then((res) => res.blob())
        .then((blob) => {
          const file_1 = new File(
            [blob],
            this.group_data.loan.file[index].fileName,
            {
              type: 'image/png',
            }
          );

          // console.log("fff", file_1);
          this.data_pic.objective.uploadFile.push({
            file: file_1,
            id: this.group_data.loan.file[index].id,
          });
          this.file_img1.push({
            id: this.group_data.loan.file[index].id,
            fileName: this.group_data.loan.file[index].fileName,
            fileSize: file_1.size,
            base64File: this.group_data.loan.file[index].base64File,
          });
        });

      const file = 'data:image/jpeg;base64,' + this.base64Image;

      this.image1[index] = file;
    }
    // console.log("loan 1:",this.file_img1)
    // ***************************************************************************************************/
    this.data_pic.businessModel.businessModelDetail = this.group_data.interestText;
    this.stepThree_2.interestText = this.data_pic.businessModel.businessModelDetail;
    for (let index = 0; index < this.group_data.interest.file.length; index++) {
      this.base64Image = this.group_data.interest.file[index].base64File;
      this.fileName = this.group_data.interest.file[index].fileName;
      const byteString =
        'data:image/jpeg;base64,' + this.base64Image.split(',')[1];
      const ab = new ArrayBuffer(byteString.length);
      const ia = new Uint8Array(ab);
      for (let i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
      }
      fetch('data:image/jpeg;base64,' + this.base64Image)
        .then((res) => res.blob())
        .then((blob) => {
          const file_1 = new File(
            [blob],
            this.group_data.interest.file[index].fileName,
            {
              type: 'image/png',
            }
          );

          // console.log("fff", file_1);
          this.data_pic.businessModel.uploadFile.push({
            file: file_1,
            id: this.group_data.interest.file[index].id,
          });
          this.file_img2.push({
            id: this.group_data.interest.file[index].id,
            fileName: this.group_data.interest.file[index].fileName,
            fileSize: file_1.size,
            base64File: this.group_data.interest.file[index].base64File,
          });
        });

      const file = 'data:image/jpeg;base64,' + this.base64Image;

      this.image2[index] = file;
    }
    // ***************************************************************************************************/
    this.data_pic.productCharacteristics.productCharacteristicsDetail = this.group_data.interestApproveText;
    this.stepThree_2.interestApproveText = this.data_pic.productCharacteristics.productCharacteristicsDetail;
    for (
      let index = 0;
      index < this.group_data.interestApprove.file.length;
      index++
    ) {
      this.base64Image = this.group_data.interestApprove.file[index].base64File;
      this.fileName = this.group_data.interestApprove.file[index].fileName;
      const byteString =
        'data:image/jpeg;base64,' + this.base64Image.split(',')[1];
      const ab = new ArrayBuffer(byteString.length);
      const ia = new Uint8Array(ab);
      for (let i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
      }
      fetch('data:image/jpeg;base64,' + this.base64Image)
        .then((res) => res.blob())
        .then((blob) => {
          const file_1 = new File(
            [blob],
            this.group_data.interestApprove.file[index].fileName,
            {
              type: 'image/png',
            }
          );

          // console.log("fff", file_1);
          this.data_pic.productCharacteristics.uploadFile.push({
            file: file_1,
            id: this.group_data.interestApprove.file[index].id,
          });
          this.file_img3.push({
            id: this.group_data.interestApprove.file[index].id,
            fileName: this.group_data.interestApprove.file[index].fileName,
            fileSize: file_1.size,
            base64File: this.group_data.interestApprove.file[index].base64File,
          });
        });

      const file = 'data:image/jpeg;base64,' + this.base64Image;

      this.image3[index] = file;
    }
    // ***************************************************************************************************/
    this.data_pic.another.anotherDetail = this.group_data.otherText;
    this.stepThree_2.otherText = this.data_pic.another.anotherDetail;
    for (let index = 0; index < this.group_data.other.file.length; index++) {
      this.base64Image = this.group_data.other.file[index].base64File;
      this.fileName = this.group_data.other.file[index].fileName;
      const byteString =
        'data:image/jpeg;base64,' + this.base64Image.split(',')[1];
      const ab = new ArrayBuffer(byteString.length);
      const ia = new Uint8Array(ab);
      for (let i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
      }
      fetch('data:image/jpeg;base64,' + this.base64Image)
        .then((res) => res.blob())
        .then((blob) => {
          const file_1 = new File(
            [blob],
            this.group_data.other.file[index].fileName,
            {
              type: 'image/png',
            }
          );

          // console.log("fff", file_1);
          this.data_pic.another.uploadFile.push({
            file: file_1,
            id: this.group_data.other.file[index].id,
          });
          this.file_img4.push({
            id: this.group_data.other.file[index].id,
            fileName: this.group_data.other.file[index].fileName,
            fileSize: file_1.size,
            base64File: this.group_data.other.file[index].base64File,
          });
        });

      const file = 'data:image/jpeg;base64,' + this.base64Image;

      this.image4[index] = file;
    }
    // ***************************************************************************************************/

    // console.log('loan 1:', this.file_img1);
    // console.log('loan 2:', this.file_img2);
    // console.log('loan 3:', this.file_img3);
    // console.log('loan 4:', this.file_img4);

    // this.textareaForm = new FormGroup({
    //   loanText: new FormControl(this.stepThree_2.loanText),
    //   interestText: new FormControl(this.stepThree_2.interestText),
    //   interestApproveText: new FormControl(
    //     this.stepThree_2.interestApproveText
    //   ),
    //   otherText: new FormControl(this.stepThree_2.otherText),
    // });

    // console.log("uploadFile::", this.stepThree.objective.uploadFile);
    // ****************************************muti file*************************************************

    for (
      let index = 0;
      index < this.group_data.attachments.file.length;
      index++
    ) {
      this.urls.push({
        id: this.group_data.attachments.file[index].id,
        fileName: this.group_data.attachments.file[index].fileName,
        base64File: this.group_data.attachments.file[index].base64File,
        fileSize: this.group_data.attachments.file[index].fileSize,
        path: this.group_data.attachments.file[index].path,
      });
    }

    this.dataSource_file = new MatTableDataSource(this.urls);
    this.hideDownloadFile();
    this.stepThree_2.attachments.file = this.urls;
    // **************************************muti file****************************************************
    // console.log("URL:::", this.urls);

    this.stepThree_2.loan.file = this.file_img1;
    this.stepThree_2.loan.fileDelete = this.file_img1_delete;

    this.stepThree_2.interest.file = this.file_img2;
    this.stepThree_2.interest.fileDelete = this.file_img2_delete;

    this.stepThree_2.interestApprove.file = this.file_img3;
    this.stepThree_2.interestApprove.fileDelete = this.file_img3_delete;

    this.stepThree_2.other.file = this.file_img4;
    this.stepThree_2.other.fileDelete = this.file_img4_delete;
  }

  add_gain_credit(value) {
    this.findColumn();
    console.log('column:', this.columnTitle);
    console.log('add applicant:', this.stepThree_2.consider.applicant);

    if (value === 1) {
      this.stepThree_2.consider.applicant.push({
        id: '',
        topic: '',
        detail: this.dataRow,
      });
    } else if (value === 2) {
      this.stepThree_2.consider.finan.push({
        id: '',
        topic: '',
        detail: this.dataRow,
      });
    } else if (value === 3) {
      this.stepThree_2.consider.status.push({
        id: '',
        topic: '',
        detail: this.dataRow,
      });
    } else if (value === 4) {
      this.stepThree_2.consider.other.push({
        id: '',
        topic: '',
        detail: this.dataRow,
      });
    }

    this.dataRow = [
      {
        'key': '00',
        'value': ''
      },
      {
        'key': '10',
        'value': ''
      },
      {
        'key': '20',
        'value': ''
      },
      {
        'key': '01',
        'value': ''
      },
      {
        'key': '11',
        'value': ''
      },
      {
        'key': '21',
        'value': ''
      },
      {
        'key': '02',
        'value': ''
      },
      {
        'key': '12',
        'value': ''
      },
      {
        'key': '22',
        'value': ''
      },
      {
        'key': '03',
        'value': ''
      },
      {
        'key': '13',
        'value': ''
      },
      {
        'key': '23',
        'value': ''
      },
      {
        'key': '04',
        'value': ''
      },
      {
        'key': '14',
        'value': ''
      },
      {
        'key': '24',
        'value': ''
      },
      {
        'key': '05',
        'value': ''
      },
      {
        'key': '15',
        'value': ''
      },
      {
        'key': '25',
        'value': ''
      },
      {
        'key': '06',
        'value': ''
      },
      {
        'key': '16',
        'value': ''
      },
      {
        'key': '26',
        'value': ''
      }
    ];
    console.log('after add:', this.stepThree_2.consider);
  }
  delete_consider(value: any, rows: any, index: any) {
    console.log('index:', value);
    console.log('rows;', rows);
    console.log('number:', index);
    if (value === 1) {
      if (rows.id !== '') {
        this.stepThree_2.considerDelete.push(rows.id);
        this.stepThree_2.consider.applicant.splice(index, 1);
      } else {
        this.stepThree_2.consider.applicant.splice(index, 1);
      }
    } else if (value === 2) {
      if (rows.id !== '') {
        this.stepThree_2.considerDelete.push(rows.id);
        this.stepThree_2.consider.finan.splice(index, 1);
      } else {
        this.stepThree_2.consider.finan.splice(index, 1);
      }
    } else if (value === 3) {
      if (rows.id !== '') {
        this.stepThree_2.considerDelete.push(rows.id);
        this.stepThree_2.consider.status.splice(index, 1);
      } else {
        this.stepThree_2.consider.status.splice(index, 1);
      }
    } else if (value === 4) {
      if (rows.id !== '') {
        this.stepThree_2.considerDelete.push(rows.id);
        this.stepThree_2.consider.other.splice(index, 1);
      } else {
        this.stepThree_2.consider.other.splice(index, 1);
      }
    }
    this.changeSaveDraft();
    console.log('after delete:', this.stepThree_2.consider);
    console.log('delete:', this.stepThree_2.considerDelete);
  }

  changeValue() {
    this.check_box[0].MOU = this.checkbok_goal_l.MOU;
    this.check_box[1]['Non-MOU'] = this.checkbok_goal_l.Non_MOU;
    this.check_box[2]['No-gain'] = this.checkbok_goal_l.No_gain;

    this.stepThree_2.agreement = [
      {
        topic: 'MOU',
        flag: this.checkbok_goal_l.MOU,
      },
      {
        topic: 'Non-MOU',
        flag: this.checkbok_goal_l.Non_MOU,
      },
      {
        topic: 'ไม่กำหนด',
        flag: this.checkbok_goal_l.No_gain,
      },
    ];

    this.stepThree_2.career = [
      {
        topic: 'ข้าราชการ',
        flag: this.career[0].flag,
      },
      {
        topic: 'พนักงานรัฐวิสาหกิจ',
        flag: this.career[1].flag,
      },
      {
        topic: 'พนักงานราชการ',
        flag: this.career[2].flag,
      },
      {
        topic: 'พนักงานหน่วยงานเอกชน',
        flag: this.career[3].flag,
      },
      {
        topic: 'กลุ่มพนักงานบริษัทในเครือ Krungthai',
        flag: this.career[4].flag,
      },
      {
        topic: 'บุคคลทั่วไปที่มีรายได้ประจำ',
        flag: this.career[5].flag,
      },
      {
        topic: 'ผู้ประกอบการร้านค้าย่อยทั่วไป',
        flag: this.career[6].flag,
      },
    ];
    console.log('career:', this.stepThree_2.career);
    console.log('22222', this.columnTitle);
    console.log('row:', this.dataRow);
  }

  changeValue_collateral() {
    if (this.stepThree_2.guaranty[1].flag === false) {
      this.stepThree_2.guaranty[1].child[0].flag = false;
      this.stepThree_2.guaranty[1].child[1].flag = false;
      this.stepThree_2.guaranty[1].child[2].flag = false;
      this.stepThree_2.guaranty[1].child[3].flag = false;
    }

    if (this.stepThree_2.guaranty[2].flag === false) {
      this.stepThree_2.guaranty[2].child[0].flag = false;
      this.stepThree_2.guaranty[2].child[1].flag = false;
      this.stepThree_2.guaranty[2].child[2].flag = false;
      this.stepThree_2.guaranty[2].child[3].flag = false;
      this.stepThree_2.guaranty[2].child[4].flag = false;
      this.stepThree_2.guaranty[2].child[5].flag = false;
      this.stepThree_2.guaranty[2].child[6].flag = false;
    }

    if (this.stepThree_2.guaranty[3].flag === false) {
      this.stepThree_2.guaranty[3].child[0].flag = false;
      this.stepThree_2.guaranty[3].child[1].flag = false;
      this.stepThree_2.guaranty[3].child[2].flag = false;
    }

    if (this.stepThree_2.guaranty[4].flag === false) {
      this.stepThree_2.guaranty[4].child[0].flag = false;
      this.stepThree_2.guaranty[4].child[1].flag = false;
      this.stepThree_2.guaranty[4].child[2].flag = false;
      this.stepThree_2.guaranty[4].child[3].flag = false;
    }

    if (this.stepThree_2.guaranty[7].flag === false) {
      this.stepThree_2.guaranty[7].otherDetail = '';
    }
    console.log('guaranty:', this.stepThree_2.guaranty);
    this.changeSaveDraft();
  }

  change_text_img(index: any) {
    this.stepThree_2.loanText = this.data_pic.objective.objectiveDetail;
    this.stepThree_2.interestText = this.data_pic.businessModel.businessModelDetail;
    this.stepThree_2.interestApproveText = this.data_pic.productCharacteristics.productCharacteristicsDetail;
    this.stepThree_2.otherText = this.data_pic.another.anotherDetail;

    console.log('tfff', this.stepThree_2);
  }
  add(index): void {
    if (index === 1) {
      // this.data_pic.table_Main.table = this.data;
      this.stepThree_2.credit.push(this.add_data);
      this.add_data = {
        id: '',
        loandType: '',
        limitType: '',
        limit: '',
        preriod: '',
        max: '',
        min: '',
        avg: '',
      };
    }
    console.log('stepThree_2.credit:', this.stepThree_2.credit);
  }
  delete(index: number, row: any, i: any) {
    console.log('index:', index);
    console.log('row:', row);
    console.log('i:', i);
    // this.stepThree_2.table_Main.table = this.data;
    if (row.id !== '') {
      this.stepThree_2.creditDelete.push(row.id);
    }
    this.stepThree_2.credit.splice(i, 1);
    console.log('stepThree_2.credit:', this.stepThree_2.credit);
    console.log('table.delete:', this.stepThree_2.creditDelete);
    this.changeSaveDraft();
  }

  delete_image(fileIndex, index) {
    console.log('fileIndex:', fileIndex);
    console.log('index:', index);
    if (fileIndex === 1) {
      this.data_pic.objective.uploadFile.splice(index, 1);
      this.image1.splice(index, 1);
      if (this.file_img1[index].id === '') {
        this.file_img1.splice(index, 1);
      } else {
        this.file_img1_delete.push(this.file_img1[index].id);
        this.file_img1.splice(index, 1);
      }
      // console.log("delete",this.file_img1_delete)
      console.log(
        'this.stepOne.objective.uploadFile.length2:',
        this.data_pic.objective.uploadFile.length
      );
      for (let i = 0; i < this.data_pic.objective.uploadFile.length; i++) {
        this.preview(fileIndex, this.data_pic.objective.uploadFile[i], i);
      }
    } else if (fileIndex === 2) {
      this.data_pic.businessModel.uploadFile.splice(index, 1);
      this.image2.splice(index, 1);
      if (this.file_img2[index].id === '') {
        this.file_img2.splice(index, 1);
      } else {
        this.file_img2_delete.push(this.file_img2[index].id);
        this.file_img2.splice(index, 1);
      }
      console.log(
        'this.stepOne.businessModel.uploadFile.length2:',
        this.data_pic.businessModel.uploadFile.length
      );
      for (let i = 0; i < this.data_pic.businessModel.uploadFile.length; i++) {
        this.preview(fileIndex, this.data_pic.businessModel.uploadFile[i], i);
      }
    } else if (fileIndex === 3) {
      this.data_pic.productCharacteristics.uploadFile.splice(index, 1);
      this.image3.splice(index, 1);
      if (this.file_img3[index].id === '') {
        this.file_img3.splice(index, 1);
      } else {
        this.file_img3_delete.push(this.file_img3[index].id);
        this.file_img3.splice(index, 1);
      }
      console.log(
        'this.uploadFile.productCharacteristics.uploadFile.length2:',
        this.data_pic.productCharacteristics.uploadFile.length
      );
      for (
        let i = 0;
        i < this.data_pic.productCharacteristics.uploadFile.length;
        i++
      ) {
        this.preview(
          fileIndex,
          this.data_pic.productCharacteristics.uploadFile[i],
          i
        );
      }
    } else if (fileIndex === 4) {
      this.data_pic.another.uploadFile.splice(index, 1);
      this.image4.splice(index, 1);
      if (this.file_img4[index].id === '') {
        this.file_img4.splice(index, 1);
      } else {
        this.file_img4_delete.push(this.file_img4[index].id);
        this.file_img4.splice(index, 1);
      }
      console.log(
        'this.uploadFile.another.uploadFile.length2:',
        this.data_pic.another.uploadFile.length
      );
      for (let i = 0; i < this.data_pic.another.uploadFile.length; i++) {
        this.preview(fileIndex, this.data_pic.another.uploadFile[i], i);
      }
    }

    // console.log("del img1:", this.file_img1);
    // console.log("del img2:", this.file_img2);
    // console.log("del img3:", this.file_img3);
    // console.log("del img4:", this.file_img4);
    this.changeSaveDraft();
  }

  image_click(colId, id) {
    this.showModel = true;
    console.log('colId:', colId);
    console.log('id:', id);

    document.getElementById('modal3_2').style.display = 'block';
    this.imageModal(colId, id);
  }
  image_bdclick() {
    document.getElementById('modal3_2').style.display = 'block';
    console.log('2222');
  }

  closeModal() {
    this.showModel = false;
    document.getElementById('modal3_2').style.display = 'none';
  }

  // getFileDetails ================================================================
  getFileDetails(fileIndex, event) {
    console.log('fileIndex:', fileIndex);
    console.log('event:', event);
    const file = event.target.files;
    for (let index = 0; index < file.length; index++) {
      const mimeType = file[index].type;
      if (mimeType.match('image/jpeg|image/png') == null) {
        this.message = 'Only images are supported.';
        this.status_loading = false;
        alert(this.message);
        return;
      }
    }
    // this.file_name = file.name;
    // this.file_type = file.type;
    // this.file_size = file.size;
    // this.modifiedDate = file.lastModifiedDate;
    // console.log('file:', file);
    console.log('file.length:', file.length);

    if (fileIndex === 1) {
      console.log('00000000000000000000000000000001');
      for (let index = 0; index < file.length; index++) {
        // this.data_pic.objective.uploadFile.push(event.target.files[index]);
        this.data_pic.objective.uploadFile.push({
          file: event.target.files[index],
          id: '',
        });
      }
      console.log('data_pic:', this.data_pic);
      for (
        let index = 0;
        index < this.data_pic.objective.uploadFile.length;
        index++
      ) {
        this.preview(
          fileIndex,
          this.data_pic.objective.uploadFile[index],
          index
        );
      }
      this.file1_1 = undefined;
      this.file1_2 = undefined;
    } else if (fileIndex === 2) {
      console.log('00000000000000000000000000000002');
      for (let index = 0; index < file.length; index++) {
        // this.data_pic.businessModel.uploadFile.push(event.target.files[index]);
        this.data_pic.businessModel.uploadFile.push({
          file: event.target.files[index],
          id: '',
        });
      }

      for (
        let index = 0;
        index < this.data_pic.businessModel.uploadFile.length;
        index++
      ) {
        this.preview(
          fileIndex,
          this.data_pic.businessModel.uploadFile[index],
          index
        );
        this.file2_1 = undefined;
        this.file2_2 = undefined;
      }
    } else if (fileIndex === 3) {
      console.log('00000000000000000000000000000003');
      for (let index = 0; index < file.length; index++) {
        // this.data_pic.productCharacteristics.uploadFile.push(
        //   event.target.files[index]
        // );
        this.data_pic.productCharacteristics.uploadFile.push({
          file: event.target.files[index],
          id: '',
        });
      }

      for (
        let index = 0;
        index < this.data_pic.productCharacteristics.uploadFile.length;
        index++
      ) {
        this.preview(
          fileIndex,
          this.data_pic.productCharacteristics.uploadFile[index],
          index
        );
      }
      this.file3_1 = undefined;
      this.file3_2 = undefined;
    } else if (fileIndex === 4) {
      console.log('00000000000000000000000000000004');
      for (let index = 0; index < file.length; index++) {
        // this.data_pic.another.uploadFile.push(event.target.files[index]);
        this.data_pic.another.uploadFile.push({
          file: event.target.files[index],
          id: '',
        });
      }

      for (
        let index = 0;
        index < this.data_pic.another.uploadFile.length;
        index++
      ) {
        this.preview(fileIndex, this.data_pic.another.uploadFile[index], index);
      }
      this.file4_1 = undefined;
      this.file4_2 = undefined;
    }
  }

  preview(fileIndex, file, index) {
    const reader = new FileReader();
    // console.log("llll:",this.file_img1)
    if (fileIndex === 1) {
      this.file_img1 = [];
      if (file.file) {
        // console.log("llll:",file)
        reader.readAsDataURL(file.file);
        reader.onload = (_event) => {
          this.image1[index] = reader.result;
          const base64result = this.image1[index].substr(
            this.image1[index].indexOf(',') + 1
          );
          this.file_img1.push({
            id: file.id,
            fileName: file.file.name,
            fileSize: file.file.size,
            base64File: base64result,
          });
          // console.log('file_img1:', this.file_img1);
        };
      }
    } else if (fileIndex === 2) {
      this.file_img2 = [];
      if (file.file) {
        reader.readAsDataURL(file.file);
        reader.onload = (_event) => {
          this.image2[index] = reader.result;
          const base64result = this.image2[index].substr(
            this.image2[index].indexOf(',') + 1
          );
          this.file_img2.push({
            id: file.id,
            fileName: file.file.name,
            fileSize: file.file.size,
            base64File: base64result,
          });
        };
      }
    } else if (fileIndex === 3) {
      this.file_img3 = [];
      if (file.file) {
        reader.readAsDataURL(file.file);
        reader.onload = (_event) => {
          this.image3[index] = reader.result;
          const base64result = this.image3[index].substr(
            this.image3[index].indexOf(',') + 1
          );
          this.file_img3.push({
            id: file.id,
            fileName: file.file.name,
            fileSize: file.file.size,
            base64File: base64result,
          });
        };
      }
    } else if (fileIndex === 4) {
      this.file_img4 = [];
      if (file.file) {
        reader.readAsDataURL(file.file);
        reader.onload = (_event) => {
          this.image4[index] = reader.result;
          const base64result = this.image4[index].substr(
            this.image4[index].indexOf(',') + 1
          );
          this.file_img4.push({
            id: file.id,
            fileName: file.file.name,
            fileSize: file.file.size,
            base64File: base64result,
          });
        };
      }
    }
    this.stepThree_2.loan.file = this.file_img1;
    this.stepThree_2.loan.fileDelete = this.file_img1_delete;

    this.stepThree_2.interest.file = this.file_img2;
    this.stepThree_2.interest.fileDelete = this.file_img2_delete;

    this.stepThree_2.interestApprove.file = this.file_img3;
    this.stepThree_2.interestApprove.fileDelete = this.file_img3_delete;

    this.stepThree_2.other.file = this.file_img4;
    this.stepThree_2.other.fileDelete = this.file_img4_delete;

    // console.log('img1:', this.stepThree_2.loan);
    // console.log('img2:', this.stepThree_2.interest);
    // console.log('img3:', this.stepThree_2.interestApprove);
    // console.log('img4:', this.stepThree_2.other);
    this.changeSaveDraft();
  }

  getFileDocument(event) {
    const file = event.target.files[0];
    const mimeType = file.type;
    if (mimeType.match(/pdf\/*/) == null) {
      this.message = 'Only PDF are supported.';
      this.status_loading = false;
      alert(this.message);
      return;
    }
    console.log('11111', file);
    this.file_name = file.name;
    // this.file_type = file.type;
    this.file_size = file.size / 1024;
    // this.modifiedDate = file.lastModifiedDate;
    // console.log('file:', file);

    this.data_pic.documentFile = file;
    // this.document = undefined;
  }

  imageModal(fileIndex, index) {
    if (fileIndex === 1) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image1;
      this.modalImagedetail = this.data_pic.objective.uploadFile;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 2) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image2;
      this.modalImagedetail = this.data_pic.businessModel.uploadFile;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 3) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image3;
      this.modalImagedetail = this.data_pic.productCharacteristics.uploadFile;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 4) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image4;
      this.modalImagedetail = this.data_pic.another.uploadFile;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    }
  }

  onSelectFile(event) {
    this.FileOverSize = [];

    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      const file = event.target.files;
      for (let i = 0; i < filesAmount; i++) {
        console.log('type:', file[i]);
        if (
          // file[i].type === 'image/svg+xml' ||
          file[i].type === 'image/jpeg' ||
          // file[i].type === 'image/raw' ||
          // file[i].type === 'image/svg' ||
          // file[i].type === 'image/tif' ||
          // file[i].type === 'image/gif' ||
          file[i].type === 'image/jpg' ||
          file[i].type === 'image/png' ||
          file[i].type === 'application/doc' ||
          file[i].type === 'application/pdf' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
          file[i].type === 'application/pptx' ||
          file[i].type === 'application/docx' ||
          (file[i].type === 'application/ppt' && file[i].size)
        ) {
          if (file[i].size <= 5000000) {
            // tslint:disable-next-line: prefer-const
            let select = {
              id: '',
              file: file[i],
            };
            this.multiple_file(select, i);
          } else {
            this.FileOverSize.push(file[i].name);
          }
        } else {
          this.status_loading = false;
          alert('File Not Support');
        }

        if (this.FileOverSize.length !== 0) {
          console.log('open modal');
          document.getElementById('testttt').click();
        }
      }
    }
  }

  multiple_file(file_1, index) {
    // console.log("download");
    const reader = new FileReader();
    if (file_1.file) {
      reader.readAsDataURL(file_1.file);
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');

        this.urls.push({
          id: file_1.id,
          fileName: file_1.file.name,
          fileSize: file_1.file.size,
          base64File: splitFile[1],
        });
        // console.log("this.urls", this.urls);
        this.dataSource_file = new MatTableDataSource(this.urls);
        this.hideDownloadFile();
      };
    }
    this.changeSaveDraft();
  }
  delete_mutifile(data: any, index: any): void {
    console.log('data:', data, 'index::', index);
    this.dataSource_file.data.splice(index, 1);
    console.log('urls:', this.urls);
    // this.urls.splice(index, 1);
    // this.muti_file.splice(index, 1);
    if (data.id !== '') {
      this.stepThree_2.attachments.fileDelete.push(data.id);
    }
    this.dataSource_file = new MatTableDataSource(this.dataSource_file.data);
    this.stepThree_2.attachments.file = this.dataSource_file.data;
    console.log('form dee:', this.stepThree_2.attachments);
    this.changeSaveDraft();
  }
  // ===============================================================================

  downloadFile(pathdata: any) {
    const contentType = '';
    const sendpath = pathdata;
    console.log('path', sendpath);
    this.SideBarService.getfile(sendpath).subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        this.status_loading = false;
        const datagetfile = res['data'];
        const b64Data = datagetfile['data'];
        const filename = datagetfile['fileName'];
        console.log('base64', b64Data);
        const config = this.b64toBlob(b64Data, contentType);
        saveAs(config, filename);
      }

    });
  }

  b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const convertbyte = atob(b64Data);
    const byteCharacters = atob(convertbyte);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  downloadFile64(data: any) {
    this.status_loading = true;
    const contentType = '';
    this.status_loading = false;
    const b64Data = data.base64File;
    const filename = data.fileName;
    console.log('base64', b64Data);

    const config = this.base64(b64Data, contentType);
    saveAs(config, filename);
  }

  base64(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  // ngOnDestroy(): void {
  //   const data = this.SideBarService.getsavePage();
  //   let saveInFoStatus = false;
  //   saveInFoStatus = data.saveStatusInfo;
  //   if (saveInFoStatus === true) {
  //     this.outOfPageSave = true;
  //     this.linkTopage = data.goToLink;
  //     console.log('ngOnDestroy', this.linkTopage);
  //     this.saveDraft();
  //     this.SideBarService.resetSaveStatusInfo();
  //   }
  // }

  hideDownloadFile() {
    if (this.dataSource_file.data.length > 0) {
      this.downloadfile_by = [];

      for (let index = 0; index < this.dataSource_file.data.length; index++) {
        const element = this.dataSource_file.data[index];
        console.log('ele', element);

        if ('path' in element) {
          this.downloadfile_by.push('path');
        } else {
          this.downloadfile_by.push('base64');
        }
      }
    }
    console.log('by:', this.downloadfile_by);
  }

  // ngDoCheck() {
  //   if (this.openDoCheckc === true) {
  //     if (this.template_manage.caaStatus !== 'show') {
  //       this.pageChange = this.SideBarService.changePageGoToLink();
  //       if (this.pageChange) {
  //         // this.getNavbar();
  //         const data = this.SideBarService.getsavePage();
  //         let saveInFoStatus = false;
  //         saveInFoStatus = data.saveStatusInfo;
  //         if (saveInFoStatus === true) {
  //           this.outOfPageSave = true;
  //           this.linkTopage = data.goToLink;
  //           this.saveDraft('navbar', null);
  //           this.SideBarService.resetSaveStatu();
  //         }
  //       }
  //     }
  //   }
  // }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate request');
    const subject = new Subject<boolean>();
    const status = this.SideBarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef =  this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        if (data.returnStatus === true) {
          const raturnStatus = this.saveDraft();
          console.log('request raturnStatus', raturnStatus);
            if (raturnStatus) {
              console.log('raturnStatus T', this.saveDraftstatus);
              subject.next(true);
            } else {
              console.log('raturnStatus F', this.saveDraftstatus);
              localStorage.setItem('check', this.stepPage);
              subject.next(false);
            }
        } else if (data.returnStatus === false) {
          this.SideBarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          localStorage.setItem('check', this.stepPage);
          subject.next(false);
        }
      });
      return subject;
    } else {
      return true;
    }
  }


}
