import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherNonCreaditComponent } from './other-non-creadit.component';

describe('OtherNonCreaditComponent', () => {
  let component: OtherNonCreaditComponent;
  let fixture: ComponentFixture<OtherNonCreaditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherNonCreaditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherNonCreaditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
