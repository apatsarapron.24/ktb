import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatTableDataSource, MatTable } from '@angular/material/table';
// tslint:disable-next-line: max-line-length
import { StepThreeService } from '../../../../../services/request/product-detail/step-three/step-three.service';
import { Router } from '@angular/router';
import { RequestService } from '../../../../../services/request/request.service';
// import { StepThreeComponent } from '../step-three.component';
import { FormControl, FormGroup } from '@angular/forms';
import { saveAs } from 'file-saver';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogSaveStatusComponent } from '../../../dialog/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from '../../../../../services/configGuard/config-guard.service';

@Component({
  selector: 'app-other-non-creadit',
  templateUrl: './other-non-creadit.component.html',
  styleUrls: ['./other-non-creadit.component.scss']
})
export class OtherNonCreaditComponent implements OnInit, CanComponentDeactivate {
  @Input() template_manage = { role: null, validate: null, caaStatus: null, checkNon: null, pageShow: null };
  @Output() saveStatus = new EventEmitter<boolean>();
  displayedColumns_file: string[] = ['name', 'size', 'delete'];
  dataSource_file: MatTableDataSource<FileList>;
  urls: any = [];
  status_loading = true;
  pageLast: any;


  saveDraftstatus = null;
  outOfPageSave = false;
  linkTopage = null;
  pageChange = null;
  openDoCheckc = false;
  showCircle_colId = null;
  showCircle_id = null;
  enter = 3;
  stepThreeOther = {
    validate: null,
    requestId: null,
    type: 'NonCredit',
    product: {
      file: [],
      fileDelete: []
    },
    productText: null,
    target: {
      file: [],
      fileDelete: []
    },
    targetText: null,
    property: {
      file: [],
      fileDelete: []
    },
    propertyText: null,
    other: {
      file: [],
      fileDelete: []
    },
    otherText: null,
    attachments: {
      file: [],
      fileDelete: []
    },
    status: false,
    conclusions: null
  };

  file1_1: any;
  file1_2: any;
  file2_1: any;
  file2_2: any;
  file3_1: any;
  file3_2: any;
  file4_1: any;
  file4_2: any;
  file5_1: any;
  file5_2: any;
  fileAttachments: any;
  document: any;
  Agreement = null;
  file_name: any;
  file_size: number;

  imgURL: any;
  image1 = [];
  image2 = [];
  image3 = [];
  image4 = [];
  image5 = [];

  showImage1 = false;
  message = null;
  FileOverSize: any;

  // Model
  showModel = false;
  modalmainImageIndex: number;
  modalmainImagedetailIndex: number;
  indexSelect: number;
  modalImage = [];
  modalImagedetail = [];
  fileValue: number;
  checkLocal: any;
  downloadfile_by = [];

  textareaForm = new FormGroup({
    productText: new FormControl(this.stepThreeOther.productText),
    targetText: new FormControl(this.stepThreeOther.targetText),
    propertyText: new FormControl(this.stepThreeOther.propertyText),
    otherText: new FormControl(this.stepThreeOther.otherText),
  });


  constructor(
    private StepThreeOtherService: StepThreeService,
    private sidebarService: RequestService,
    private router: Router,
    public dialog: MatDialog,
    // private CheckPage: StepThreeComponent,
  ) { }
  stepPage: any;
  checkPageNon: any;
  pageShow: any;
  repeat = false;

  ngOnInit() {
    window.scrollTo(0, 0);
    this.openDoCheckc = true;
    this.stepPage = localStorage.getItem('check');
    this.pageLast = (sessionStorage.getItem('page'));
    console.log('pageeee', this.pageLast);

    if (this.template_manage.checkNon === 'Credit' || this.template_manage.checkNon === 'NonCredit') {
      if (this.template_manage.checkNon === 'Credit') {
        this.checkPageNon = '';
      } else if (this.template_manage.checkNon === 'NonCredit') {
        this.checkPageNon = 'NonCredit';
      }
      // console.log('this.checkPageNon', this.checkPageNon);
      if (this.checkPageNon === '') {
        this.pageShow = 'Credit';
      } else if (this.checkPageNon === 'NonCredit') {
        this.pageShow = 'Non-Credit Product';
      }
    } else if (this.template_manage.checkNon === null) {
      this.checkPageNon = sessionStorage.getItem('check_non');
      // console.log('this.checkPageNon', this.checkPageNon);
      if (this.checkPageNon === '') {
        this.pageShow = 'Credit';
      } else if (this.checkPageNon === 'NonCredit') {
        this.pageShow = 'Non-Credit Product';
      }
    }

    this.checkLocal = localStorage.getItem('check');
    // localStorage.setItem('requestId', '1');
    if (localStorage) {
      if (localStorage.getItem('requestId')) {
        console.log('localStorage', localStorage.getItem('requestId'));
        this.sidebarService.sendEvent();
        this.sidebarService.statusSibar(localStorage.getItem('requestId')).subscribe(check => {
          console.log('ckeck ทบทวน', check);
          if (check['data'] !== null) {
            this.repeat = check['data']['repeat'];
          } else {
            this.repeat = false;
          }
        });
        this.getDataProductDetailstepThreeOther(localStorage.getItem('requestId'));
      } else {
        this.status_loading = false;
      }
    } else {
      console.log('Brownser not support');
    }
  }

  sendSaveStatus() {
    if (this.saveDraftstatus === 'success') {
      this.saveStatus.emit(true);
    } else {
      this.saveStatus.emit(false);
    }
    console.log('aaaaaa =>>', this.saveStatus);
  }
  autogrow(number: any) {
    if (number === 1) {
      const productText = document.getElementById('productText');
      productText.style.overflow = 'hidden';
      productText.style.height = 'auto';
      productText.style.height = productText.scrollHeight + 'px';
    }

    if (number === 2) {
      const targetText = document.getElementById('targetText');
      targetText.style.overflow = 'hidden';
      targetText.style.height = 'auto';
      targetText.style.height = targetText.scrollHeight + 'px';
    }
    if (number === 3) {
      const propertyText = document.getElementById('propertyText');
      propertyText.style.overflow = 'hidden';
      propertyText.style.height = 'auto';
      propertyText.style.height = propertyText.scrollHeight + 'px';
    }
    if (number === 4) {
      const otherText = document.getElementById('otherText');
      otherText.style.overflow = 'hidden';
      otherText.style.height = 'auto';
      otherText.style.height = otherText.scrollHeight + 'px';
    }
  }
  saveDraft(page?, action?) {
    const returnUpdateData = new Subject<any>();
    this.status_loading = true;

    console.log('data other to save', this.stepThreeOther);
    if (localStorage.getItem('requestId')) {
      this.stepThreeOther.requestId = localStorage.getItem('requestId');
      this.stepThreeOther.type = 'NonCredit';
    }
    this.stepThreeOther.status = true;

    if (this.template_manage.pageShow === 'summary') {
      this.stepThreeOther.conclusions = true;
    } else {
      this.stepThreeOther.conclusions = null;
    }

    this.StepThreeOtherService.update_step_three_other(this.stepThreeOther).subscribe(res => {
      if (res['status'] === 'success') {
        this.status_loading = false;
        this.downloadfile_by = [];
        this.saveDraftstatus = 'success';
        returnUpdateData.next(true);
        this.sidebarService.inPageStatus(false);
        setTimeout(() => {
          this.saveDraftstatus = 'fail';
        }, 3000);
        this.stepThreeOther.attachments.file = [];
        this.stepThreeOther.attachments.fileDelete = [];
        this.sendSaveStatus();
        if (localStorage.getItem('requestId')) {
          this.sidebarService.sendEvent();
        }
        this.getDataProductDetailstepThreeOther(localStorage.getItem('requestId'));
      } else {
        this.status_loading = false;
        alert(res['message']);
        returnUpdateData.next(false);
      }
    }, err => {
      this.status_loading = false;
      console.log(err);
      this.sendSaveStatus();
      alert(err['message']);
      returnUpdateData.next(false);
    });
    console.log('this.stepThreeOther', this.stepThreeOther);
    return returnUpdateData;
  }

  next_step(action) {
    this.StepThreeOtherService.CheckNextPage(localStorage.getItem('check'), action);
  }

  viewNext() {
    this.StepThreeOtherService.CheckNextPage(localStorage.getItem('check'), 'next');
  }

  changeSaveDraft() {
    this.saveDraftstatus = null;
    this.sendSaveStatus();
    this.sidebarService.inPageStatus(true);
  }

  // ================================================================================

  delete_image(fileIndex, index) {
    console.log('fileIndex:', fileIndex);
    console.log('index:', index);
    if (fileIndex === 1) {
      if ('id' in this.stepThreeOther.product.file[index]) {
        const id = this.stepThreeOther.product.file[index].id;
        console.log('found id');
        if ('fileDelete' in this.stepThreeOther.product) {
          this.stepThreeOther.product.fileDelete.push(id);
        }
      }
      this.stepThreeOther.product.file.splice(index, 1);
      this.image1.splice(index, 1);

      for (let i = 0; i < this.stepThreeOther.product.file.length; i++) {
        this.preview(fileIndex, this.stepThreeOther.product.file[i], i);
      }
    } else if (fileIndex === 2) {
      if ('id' in this.stepThreeOther.target.file[index]) {
        const id = this.stepThreeOther.target.file[index].id;
        console.log('found id');
        if ('fileDelete' in this.stepThreeOther.target) {
          this.stepThreeOther.target.fileDelete.push(id);
        }
      }
      this.stepThreeOther.target.file.splice(index, 1);
      this.image2.splice(index, 1);

      for (let i = 0; i < this.stepThreeOther.target.file.length; i++) {
        this.preview(fileIndex, this.stepThreeOther.target.file[i], i);
      }
    } else if (fileIndex === 3) {
      if ('id' in this.stepThreeOther.property.file[index]) {
        const id = this.stepThreeOther.property.file[index].id;
        console.log('found id');
        if ('fileDelete' in this.stepThreeOther.property) {
          this.stepThreeOther.property.fileDelete.push(id);
        }
      }
      this.stepThreeOther.property.file.splice(index, 1);
      this.image3.splice(index, 1);

      for (let i = 0; i < this.stepThreeOther.property.file.length; i++) {
        this.preview(fileIndex, this.stepThreeOther.property.file[i], i);
      }
    } else if (fileIndex === 4) {
      if ('id' in this.stepThreeOther.other.file[index]) {
        const id = this.stepThreeOther.other.file[index].id;
        console.log('found id');
        if ('fileDelete' in this.stepThreeOther.other) {
          this.stepThreeOther.other.fileDelete.push(id);
        }
      }
      this.stepThreeOther.other.file.splice(index, 1);
      this.image4.splice(index, 1);

      for (let i = 0; i < this.stepThreeOther.other.file.length; i++) {
        this.preview(fileIndex, this.stepThreeOther.other.file[i], i);
      }
    }
    this.changeSaveDraft();
  }

  image_click(colId, id) {
    this.showModel = true;
    console.log('colId:', colId);
    console.log('id:', id);
    document.getElementById('myModal3_6').style.display = 'block';
    this.imageModal(colId, id);
  }
  image_bdclick() {
    document.getElementById('myModal3_6').style.display = 'block';
  }

  closeModal() {
    this.showModel = false;
    document.getElementById('myModal3_6').style.display = 'none';
  }

  // getFileDetails ================================================================
  getFileDetails(fileIndex, event) {

    const files = event.target.files;
    for (let i = 0; i < files.length; i++) {
      const mimeType = files[i].type;
      if (mimeType.match('image/jpeg|image/png') == null) {
        this.message = 'Only images are supported.';
        this.status_loading = false;
        alert(this.message);
        return;
      }
      const file = files[i];

      console.log('file:', file);
      const reader = new FileReader();
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');

        const templateFile = {
          fileName: file.name,
          base64File: splitFile[1],
        };
        console.log('templateFile123456', templateFile);

        if (fileIndex === 1) {
          console.log('00000000000000000000000000000001');
          this.stepThreeOther.product.file.push(templateFile);

          for (let index = 0; index < this.stepThreeOther.product.file.length; index++) {
            this.preview(fileIndex, this.stepThreeOther.product.file[index], index);
          }
          this.file1_1 = undefined;
          this.file1_2 = undefined;
        } else if (fileIndex === 2) {
          console.log('00000000000000000000000000000002');
          this.stepThreeOther.target.file.push(templateFile);


          for (let index = 0; index < this.stepThreeOther.target.file.length; index++) {
            this.preview(fileIndex, this.stepThreeOther.target.file[index], index);
            this.file2_1 = undefined;
            this.file2_2 = undefined;
          }
        } else if (fileIndex === 3) {
          console.log('00000000000000000000000000000003');
          this.stepThreeOther.property.file.push(templateFile);

          for (let index = 0; index < this.stepThreeOther.property.file.length; index++) {
            this.preview(fileIndex, this.stepThreeOther.property.file[index], index);
          }
          this.file3_1 = undefined;
          this.file3_2 = undefined;
        } else if (fileIndex === 4) {
          console.log('00000000000000000000000000000004');
          this.stepThreeOther.other.file.push(templateFile);

          for (let index = 0; index < this.stepThreeOther.other.file.length; index++) {
            this.preview(fileIndex, this.stepThreeOther.other.file[index], index);
          }
          this.file4_1 = undefined;
          this.file4_2 = undefined;
        }
      };
      reader.readAsDataURL(file);
    }
    this.changeSaveDraft();
  }

  preview(fileIndex, file, index) {
    const name = file.fileName;
    const typeFiles = name.match(/\.\w+$/g);
    const typeFile = typeFiles[0].replace('.', '');

    const tempPicture = `data:image/${typeFile};base64,${file.base64File}`;

    if (fileIndex === 1) {
      if (file) {
        this.image1[index] = tempPicture;
        // console.log('preview image1', this.image1[0]);
      }
    } else if (fileIndex === 2) {
      if (file) {
        this.image2[index] = tempPicture;
      }
    } else if (fileIndex === 3) {
      if (file) {
        this.image3[index] = tempPicture;
      }
    } else if (fileIndex === 4) {
      if (file) {
        this.image4[index] = tempPicture;
      }
    } else if (fileIndex === 5) {
      if (file) {
        this.image5[index] = tempPicture;
      }
    }

  }

  // getFileDocument ================================================================
  // รอ API
  // getFileDocument(event) {
  //   const file = event.target.files[0];
  //   const mimeType = file.type;
  //   if (mimeType.match(/pdf\/*/) == null) {
  //     this.message = 'Only PDF are supported.';
  //     alert(this.message);
  //     return;
  //   }
  //   console.log('11111', file);
  //   this.file_name = file.name;
  //   // this.file_type = file.type;
  //   this.file_size = file.size / 1024;
  //   // this.modifiedDate = file.lastModifiedDate;
  //   // console.log('file:', file);

  //   this.stepThreeOther.attachments = file;
  //   // this.document = undefined;
  // }

  onSelectFile(event) {
    this.FileOverSize = [];
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      const file = event.target.files;
      for (let i = 0; i < filesAmount; i++) {
        console.log('type:', file[i]);
        if (
          // file[i].type === 'image/svg+xml' ||
          file[i].type === 'image/jpeg' ||
          // file[i].type === 'image/raw' ||
          // file[i].type === 'image/svg' ||
          // file[i].type === 'image/tif' ||
          // file[i].type === 'image/gif' ||
          file[i].type === 'image/jpg' ||
          file[i].type === 'image/png' ||
          file[i].type === 'application/doc' ||
          file[i].type === 'application/pdf' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
          file[i].type === 'application/pptx' ||
          file[i].type === 'application/docx' ||
          (file[i].type === 'application/ppt' && file[i].size)
        ) {
          if (file[i].size <= 5000000) {
            this.multiple_file(file[i], i);
          } else {
            this.FileOverSize.push(file[i].name);
          }
        } else {
          this.status_loading = false;
          alert('File Not Support');
        }
      }
      if (this.FileOverSize.length !== 0) {
        console.log('open modal');
        document.getElementById('testttt').click();
      }
    }
    this.changeSaveDraft();
  }

  multiple_file(file, index) {
    // console.log('download');
    const reader = new FileReader();
    if (file) {
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');

        const templateFile = {
          fileName: file.name,
          fileSize: file.size,
          base64File: splitFile[1],
        };
        this.stepThreeOther.attachments.file.push(templateFile);
        this.dataSource_file = new MatTableDataSource(this.stepThreeOther.attachments.file);
        this.hideDownloadFile();
      };
    }
    this.fileAttachments = undefined;
  }

  delete_mutifile(data: any, index: any) {
    console.log('data:', data, 'index::', index);
    if ('id' in this.stepThreeOther.attachments.file[index]) {
      const id = this.stepThreeOther.attachments.file[index].id;
      console.log('found id');
      if ('fileDelete' in this.stepThreeOther.attachments) {
        this.stepThreeOther.attachments.fileDelete.push(id);
      }
    }
    console.log('fileDelete attachments : ', this.stepThreeOther.attachments.fileDelete);
    this.stepThreeOther.attachments.file.splice(index, 1);
    this.dataSource_file = new MatTableDataSource(this.stepThreeOther.attachments.file);
    this.changeSaveDraft();
  }



  // ============================== Modal ==========================================
  imageModal(fileIndex, index) {
    if (fileIndex === 1) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image1;
      this.modalImagedetail = this.stepThreeOther.product.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 2) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image2;
      this.modalImagedetail = this.stepThreeOther.target.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 3) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image3;
      this.modalImagedetail = this.stepThreeOther.property.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 4) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image4;
      this.modalImagedetail = this.stepThreeOther.other.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    }
  }
  // ===============================================================================
  // ======================================= API ===================================
  getDataProductDetailstepThreeOther(documentID) {

    console.log('this.checkPageNon', this.checkPageNon);
    this.StepThreeOtherService.get_step_three_other(documentID, 'NonCredit', this.template_manage.pageShow).subscribe(res => {
      console.log('res', res);
      if (res['status'] === 'success') {
        this.status_loading = false;
        this.stepThreeOther['parentRequestId'] = res['parentRequestId'];
      }
      if (res['data'] !== null) {
        this.stepThreeOther = res['data'];
        // set fileDelete to Object stepOne
        this.stepThreeOther.product.fileDelete = [];
        this.stepThreeOther.target.fileDelete = [];
        this.stepThreeOther.property.fileDelete = [];
        this.stepThreeOther.other.fileDelete = [];
        this.stepThreeOther.attachments.fileDelete = [];


        this.textareaForm = new FormGroup({
          productText: new FormControl(this.stepThreeOther.productText),
          targetText: new FormControl(this.stepThreeOther.targetText),
          propertyText: new FormControl(this.stepThreeOther.propertyText),
          otherText: new FormControl(this.stepThreeOther.otherText),
        });

        console.log('set fileDelete', this.stepThreeOther);

        // set picture preview to all input
        for (let i = 0; i < this.stepThreeOther.product.file.length; i++) {
          this.preview(1, this.stepThreeOther.product.file[i], i);
        }

        for (let i = 0; i < this.stepThreeOther.target.file.length; i++) {
          this.preview(2, this.stepThreeOther.target.file[i], i);
        }

        for (let i = 0; i < this.stepThreeOther.property.file.length; i++) {
          this.preview(3, this.stepThreeOther.property.file[i], i);
        }

        for (let i = 0; i < this.stepThreeOther.other.file.length; i++) {
          this.preview(4, this.stepThreeOther.other.file[i], i);
        }

        // set file attachments
        this.dataSource_file = new MatTableDataSource(this.stepThreeOther.attachments.file);
        this.hideDownloadFile();
      } else {
        this.stepThreeOther['validate'] = res['validate'];
      }

    }, err => {
      this.status_loading = false;
      console.log(err);
    });
  }

  // ===============================================================================
  downloadFile(pathdata: any) {
    this.status_loading = true;
    const contentType = '';
    const sendpath = pathdata;
    console.log('path', sendpath);
    this.sidebarService.getfile(sendpath).subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        this.status_loading = false;
        const datagetfile = res['data'];
        const b64Data = datagetfile['data'];
        const filename = datagetfile['fileName'];
        console.log('base64', b64Data);
        const config = this.b64toBlob(b64Data, contentType);
        saveAs(config, filename);
      }

    });
  }

  b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const convertbyte = atob(b64Data);
    const byteCharacters = atob(convertbyte);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  downloadFile64(data: any) {
    this.status_loading = true;
    const contentType = '';
    this.status_loading = false;
    const b64Data = data.base64File;
    const filename = data.fileName;
    console.log('base64', b64Data);

    const config = this.base64(b64Data, contentType);
    saveAs(config, filename);
  }

  base64(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }


  // ngOnDestroy(): void {
  //   const data = this.sidebarService.getsavePage();
  //   let saveInFoStatus = false;
  //   saveInFoStatus = data.saveStatusInfo;
  //   if (saveInFoStatus === true) {
  //     this.outOfPageSave = true;
  //     this.linkTopage = data.goToLink;
  //     console.log('ngOnDestroy', this.linkTopage);
  //     this.saveDraft();
  //     this.sidebarService.resetSaveStatusInfo();
  //   }
  // }

  hideDownloadFile() {
    if (this.dataSource_file.data.length > 0) {
      this.downloadfile_by = [];

      for (let index = 0; index < this.dataSource_file.data.length; index++) {
        const element = this.dataSource_file.data[index];
        console.log('ele', element);

        if ('path' in element) {
          this.downloadfile_by.push('path');
        } else {
          this.downloadfile_by.push('base64');
        }
      }
    }
    console.log('by:', this.downloadfile_by);
  }

  // ngDoCheck() {
  //   if (this.openDoCheckc === true) {
  //     if (this.template_manage.caaStatus !== 'show') {
  //       this.pageChange = this.sidebarService.changePageGoToLink();
  //       if (this.pageChange) {
  //         // this.getNavbar();
  //         const data = this.sidebarService.getsavePage();
  //         let saveInFoStatus = false;
  //         saveInFoStatus = data.saveStatusInfo;
  //         if (saveInFoStatus === true) {
  //           this.outOfPageSave = true;
  //           this.linkTopage = data.goToLink;
  //           this.saveDraft('navbar', null);
  //           this.sidebarService.resetSaveStatu();
  //         }
  //       }
  //     }
  //   }
  // }
  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate request');
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef =  this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        if (data.returnStatus === true) {
          const raturnStatus = this.saveDraft();
          console.log('request raturnStatus', raturnStatus);
            if (raturnStatus) {
              console.log('raturnStatus T', this.saveDraftstatus);
              subject.next(true);
            } else {
              console.log('raturnStatus F', this.saveDraftstatus);
              localStorage.setItem('check', this.stepPage);
              subject.next(false);
            }
        } else if (data.returnStatus === false) {
          this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          localStorage.setItem('check', this.stepPage);
          subject.next(false);
        }
      });
      return subject;
    } else {
      return true;
    }
  }
}
