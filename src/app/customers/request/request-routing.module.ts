import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RequestComponent } from '../request/request.component';

import { informationBaseFirstStepComponent } from './information-base/first-step/first-step.component';
import { informationBaseStepTwoComponent } from './information-base/step-two/step-two.component';
import { informationBaseStepThreeComponent } from './information-base/step-three/step-three.component';
import { informationBaseStepFourComponent } from './information-base/step-four/step-four.component';
import { StepOneComponent } from './product-detail/step-one/step-one.component';
import { StepTwoComponent } from './product-detail/step-two/step-two.component';
import { StepThreeComponent } from './product-detail/step-three/step-three.component';
import { BusinessLoanComponent } from './product-detail/step-three/business-loan/business-loan.component';
import { PersonalLoanComponent } from './product-detail/step-three/personal-loan/personal-loan.component';
import { HousingLoanComponent } from './product-detail/step-three/housing-loan/housing-loan.component';
import { NonCreditComponent } from './product-detail/step-three/non-credit/non-credit.component';
import { OtherComponent } from './product-detail/step-three/other/other.component';

import { StepFourComponent } from './product-detail/step-four/step-four.component';
import { StepFiveComponent } from './product-detail/step-five/step-five.component';
import { StepSixComponent } from './product-detail/step-six/step-six.component';
import { StepSevenComponent } from './product-detail/step-seven/step-seven.component';
import { StepEightComponent } from './product-detail/step-eight/step-eight.component';
import { StepNineComponent } from './product-detail/step-nine/step-nine.component';
import { StepTenComponent } from './product-detail/step-ten/step-ten.component';
import { StepElevenComponent } from './product-detail/step-eleven/step-eleven.component';
import { StepTwelveComponent } from './product-detail/step-twelve/step-twelve.component';
import { from } from 'rxjs';
import { OtherNonCreaditComponent } from './product-detail/step-three/other-non-creadit/other-non-creadit.component';
import { CanDeactivateGuard } from '../../services/configGuard/config-guard.service';


const routes: Routes = [
  {
    path: '',
    // component: RequestComponent,
  },
  {
    path: 'information-base',
    children: [
      {
        path: 'step1',
        component: informationBaseFirstStepComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'step2',
        component: informationBaseStepTwoComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'step3',
        component: informationBaseStepThreeComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'step4',
        component: informationBaseStepFourComponent,
        canDeactivate: [CanDeactivateGuard]
      }
    ],
  },
  {
    path: 'product-detail',
    children: [
      {
        path: 'step1',
        component: StepOneComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'step2',
        component: StepTwoComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'step3',
        component: StepThreeComponent,
        children: [
          {
            path: '',
            component: StepThreeComponent,
          },
          {
            path: 'business_loan',
            component: BusinessLoanComponent,
            canDeactivate: [CanDeactivateGuard]
          },
          {
            path: 'persenal_loan',
            component: PersonalLoanComponent,
            canDeactivate: [CanDeactivateGuard]
          },
          {
            path: 'housing_loan',
            component: HousingLoanComponent,
            canDeactivate: [CanDeactivateGuard]
          },
          {
            path: 'other',
            component: OtherComponent,
            canDeactivate: [CanDeactivateGuard]
          },
          {
            path: 'non_credit_product',
            component: NonCreditComponent,
            canDeactivate: [CanDeactivateGuard]
          },
          {
            path: 'other_non_creadit',
            component: OtherNonCreaditComponent,
            canDeactivate: [CanDeactivateGuard]
          },
        ]
      },
      {
        path: 'step4',
        component: StepFourComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'step5',
        component: StepFiveComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'step6',
        component: StepSixComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'step7',
        component: StepSevenComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'step8',
        component: StepEightComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'step9',
        component: StepNineComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'step10',
        component: StepTenComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'step11',
        component: StepElevenComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'step12',
        component: StepTwelveComponent,
        canDeactivate: [CanDeactivateGuard]
      },
    ]
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [CanDeactivateGuard]
})
export class RequestRoutingModule { }
