import { template } from '@angular/core/src/render3';
import { NotificationMgmtService } from './../../services/notification-mgmt/notification-mgmt.service';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import * as inlineEditor from '@ckeditor/ckeditor5-build-inline';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
declare var $: any;


export class UploadAdapter {
  private loader;
  constructor(loader) {
    this.loader = loader;
  }

  upload() {
    return this.loader.file
      .then(file => new Promise((resolve, reject) => {
        var myReader = new FileReader();
        myReader.onloadend = (e) => {
          resolve({ default: myReader.result });
        }

        myReader.readAsDataURL(file);
      }));
  };
}
@Component({
  selector: 'app-notification-mgmt',
  templateUrl: './notification-mgmt.component.html',
  styleUrls: ['./notification-mgmt.component.scss']
})
export class NotificationMgmtComponent implements OnInit {

  process = [
    {
      title: 'แผนออก/ทบทวนผลิตภัณฑ์',
      value: 'PLAN_REVIEW'
    },
    {
      title: 'การส่งงานในขั้นตอนที่มีการกำหนด SLA สถานะ หน่วยงานที่เกี่ยวข้องให้ความเห็น',
      value: 'LSA'
    },
    {
      title: 'การส่งงานในขั้นตอนที่มีการกำหนด SLA สถานะ PO ให้ความเห็นเพิ่มเติม',
      value: 'LSA_PO'
    },
    {
      title: 'การส่งงานในขั้นตอนที่ไม่มีการกำหนด SLA',
      value: 'NONE_LSA'
    },
    {
      title: 'การประกาศใช้ผลิตภัณฑ์',
      value: 'PUBLISH'
    },
    {
      title: 'การติดตามประเด็นคงค้าง',
      value: 'TRACK'
    },
    {
      title: 'Product Performance',
      value: 'PERFORMANCE'
    },
    {
      title: 'การให้ความเห็น',
      value: 'COMMENT'
    },
    {
      title: 'ข้อมูลเพิ่มเติมเพื่อประกอบการให้ความเห็น',
      value: 'QUETION'
    }
  ];

  processSelected = null;

  condition = [
    {
      title: 'แจ้งเตือนก่อนครบกำหนด',
      value: 'Before'
    },
    {
      title: 'แจ้งเตือนเมื่อครบกำหนด',
      value: 'Present'
    },
    {
      title: 'แจ้งเตือนเมื่อเกินกำหนดครั้งที่ 1',
      value: 'After'
    },
    {
      title: 'แจ้งเตือนเมื่อเกินกำหนดครั้งที่ 2',
      value: 'After2'
    },
    {
      title: 'แจ้งเตือนเมื่อมีการส่งงาน',
      value: 'Finish'
    }
  ];

  conditionSelected = null;

  notiData = {
    process: null,
    condition: null,
    detail: {
      to:
      {
        ops: false,
        dept: false,
        grp: false,
        sctr: false
      },
      cc: {
        ops: false,
        dept: false,
        grp: false,
        sctr: false
      },
    },
    dateAmount: 0,
    title: '',
    message: ''
  };

  btnSaveState = true;
  emailState = false;
  confirmState = 'confirm';
  DataClear = null;
  ClearStatus = false;
  status_loading = false;
  editor = inlineEditor;
  @ViewChild('callAPIDialog') callAPIDialog: TemplateRef<any>;

  constructor(
    private router: Router,
    private notiEmailService: NotificationMgmtService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    // this.selectedValue1 = 'default';
  }

  getConfigEmail() {
    console.log(this.processSelected, this.conditionSelected)
    this.notiEmailService.findEmailConfig(this.processSelected, this.conditionSelected).subscribe(res => {
      console.log(res);
      this.DataClear = res['data'];
      if (res['status'] === 'success' && res['data']) {
        console.log(res['data']);
        this.notiData = res['data'];
        this.btnSaveState = false;
        this.emailState = true;
        this.ClearStatus = true;
      } else {
        console.log('data null');
        const templateNoti = {
          process: null,
          condition: null,
          detail: {
            to:
            {
              ops: false,
              dept: false,
              grp: false,
              sctr: false
            },
            cc: {
              ops: false,
              dept: false,
              grp: false,
              sctr: false
            },
          },
          dateAmount: 0,
          title: '',
          message: ''
        };
        this.notiData = templateNoti;
        this.notiData.process = this.processSelected;
        this.notiData.condition = this.conditionSelected;
        this.emailState = true;
        this.btnSaveState = false;
        this.ClearStatus = false;
      }
    }, err => {
      console.log(err);
    });
  }

  backpage() {
    localStorage.setItem('requestId', '');
    this.router.navigate(['/dashboard1']);
  }

  save() {
    console.log('datasend :', this.notiData);
    if (this.validateInput()) {
      console.log(this.notiData);
      this.notiEmailService.addConfigEmail(this.notiData).subscribe(res => {
        console.log(res);
        if (res['status'] === 'success') {
          this.confirmState = 'success';
          this.getConfigEmail();
          setTimeout(() => {
            $('#confirmModal').modal('hide');
            this.confirmState = 'confirm';
          }, 3000);

        }
      }, err => {
        console.log(err);
      });
    } else {
      console.log('please add all field');
    }
  }

  validateInput() {
    if (this.conditionSelected.length && this.processSelected.length && this.notiData) {
      return true;
    }
    return false;
  }

  onReady(eventData) {
    eventData.plugins.get('FileRepository').createUploadAdapter = function (loader) {
      console.log('loader : ', loader)
      console.log(btoa(loader.file));
      return new UploadAdapter(loader);
    };
  }

  backToDashboardPage() {
    localStorage.setItem('requestId', '');
    this.router.navigate(['dashboard1']);
  }

  clearDataAfterSearch() {
    if (this.DataClear !== null) {
      console.log('clear', this.DataClear);
      console.log('process', this.DataClear.process);
      console.log('condition', this.DataClear.condition);
      this.status_loading = true;
      this.notiEmailService.clearData(this.DataClear.process, this.DataClear.condition).subscribe(res => {
        if (res['status'] === 'success') {
          this.ClearStatus = false;
          this.status_loading = false;
          this.getConfigEmail();
          setTimeout(() => {
            $('#ClearModal').modal('hide');
          }, 3000);
        }
      });
    }
  }
}


