import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationMgmtComponent } from './notification-mgmt.component';

describe('NotificationMgmtComponent', () => {
  let component: NotificationMgmtComponent;
  let fixture: ComponentFixture<NotificationMgmtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationMgmtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationMgmtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
