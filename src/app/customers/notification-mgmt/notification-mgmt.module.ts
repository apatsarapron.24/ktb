import { NotificationMgmtRoutingModule } from './notification-mgmt-routing.module';
import { NotificationMgmtComponent } from './notification-mgmt.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatSelectModule, MatCheckboxModule, MatDialogModule, MatButtonModule} from '@angular/material';
import { FormsModule } from '@angular/forms';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

@NgModule({
  declarations: [NotificationMgmtComponent],
  imports: [
    CommonModule,
    NotificationMgmtRoutingModule,
    MatSelectModule,
    FormsModule,
    MatCheckboxModule,
    CKEditorModule,
    MatDialogModule,
    MatButtonModule
  ]
})
export class NotificationMgmtModule { }
