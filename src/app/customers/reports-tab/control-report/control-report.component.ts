import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { IAngularMyDpOptions } from 'angular-mydatepicker';
import { ReportsTabService } from '../reports-tab.service';
import { ProductTurnaroundService } from '../../../services/product-turnaround/product-turnaround.service';
import * as moment from 'moment';
import { DateAdapter } from '@angular/material';
import Swal from 'sweetalert2';
import { MatDialog } from '@angular/material';
import { ControlReportDialogComponent } from './control-report-dialog/control-report-dialog.component';

@Component({
  selector: 'app-control-report',
  templateUrl: './control-report.component.html',
  styleUrls: ['./control-report.component.scss']
})
export class ControlReportComponent implements OnInit {
  selected1 = new FormControl('', [Validators.required]);
  tabs1 = true;
  department: any;
  start_date: any;
  end_date: any;
  myDpOptions: IAngularMyDpOptions = {
    dateRange: false,
    dateFormat: 'dd/mm/yyyy',
    // other options are here...
    stylesData: {
      selector: 'dp',
      styles: `
           .dp .top:auto
           .dp .myDpIconLeftArrow,
           .dp .myDpIconRightArrow,
           .dp .myDpHeaderBtn {
               color: #3855c1;
            }
           .dp .myDpHeaderBtn:focus,
           .dp .myDpMonthLabel:focus,
           .dp .myDpYearLabel:focus {
               color: #3855c1;
            }
           .dp .myDpDaycell:focus,
           .dp .myDpMonthcell:focus,
           .dp .myDpYearcell:focus {
               box-shadow: inset 0 0 0 1px #66afe9;
            }
           .dp .myDpSelector:focus {
               border: 1px solid #ADD8E6;
            }
           .dp .myDpSelectorArrow:focus:before {
               border-bottom-color: #ADD8E6;
            }
           .dp .myDpCurrMonth,
           .dp .myDpMonthcell,
           .dp .myDpYearcell {
               color: #00a6e6;
            }
           .dp .myDpDaycellWeekNbr {
               color: #3855c1;
            }
           .dp .myDpPrevMonth,
           .dp .myDpNextMonth {
               color: #0098D2;
            }
           .dp .myDpWeekDayTitle {
               background-color: #0098D2;
               color: #ffffff;
            }
           .dp .myDpHeaderBtnEnabled:hover,
           .dp .myDpMonthLabel:hover,
           .dp .myDpYearLabel:hover {
               color:#0098D2;
            }
           .dp .myDpMarkCurrDay,
           .dp .myDpMarkCurrMonth,
           .dp .myDpMarkCurrYear {
               border-bottom: 2px solid #3855c1;
            }
           .dp .myDpDisabled {
               color: #999;
            }
           .dp .myDpHighlight {
               color: #74CBEC;
            }
           .dp .myDpTableSingleDay:hover,
           .dp .myDpTableSingleMonth:hover,
           .dp .myDpTableSingleYear:hover {
               background-color: #add8e6;
               color: #0098D2;
            }
           .dp .myDpRangeColor {
               background-color: #dbeaff;
            }
           .dp .myDpSelectedDay,
           .dp .myDpSelectedMonth,
           .dp .myDpSelectedYear {
               background-color: #0098D2;
               color: #ffffff;
            }
            `,
    },
  };
  _control_report = [];
  _control_report2 = [];
  _control_report3 = [];
  _control_report_sub = [];
  _control_report2_sub = [];
  _control_report3_sub = [];
  _controlSLA_report = [];
  _controlSLA_report2 = [];
  _controlSLA_report3 = [];
  _controlSLA_report_sub = [];
  _controlSLA_report2_sub = [];
  _controlSLA_report3_sub = [];
  mocup = {
    grp: null,  // สายงาน
    sctr: null, // กลุ่มงาน
    dept: null, // ฝ่ายขาย
    objective: null, // วัตถุประสงค์
    productGroup: null, // กลุ่มผลิตภัณฑ์
    startDate: null, // ช่วงเวลาที่สร้างใบคำขอ
    endDate: null // ช่วงเวลาที่สร้างใบคำขอ
  };
  _data_reports = [];
  _data_reportsSLA = [];

  divisionList_data: any;
  workgroupList_data: any;
  workgroupList: any;
  departmentList: any;
  departmentList_data: any;
  workgroup: any;
  division: any;
  divisionList: any;
  objectiveSelected = null;
  objective = [
    {
      title: 'ทั้งหมด',
      value: 'ทั้งหมด'
    },
    {
      title: 'ผลิตภัณฑ์ใหม่',
      value: 'ผลิตภัณฑ์ใหม่'
    },
    {
      title: 'ทบทวนผลิตภัณฑ์ระหว่างปี',
      value: 'ทบทวนผลิตภัณฑ์ระหว่างปี'
    },
    {
      title: 'ทบทวนผลิตภัณฑ์ประจำปี',
      value: 'ทบทวนผลิตภัณฑ์ประจำปี'
    },
  ];
  productgroup = [
    {
      title: 'Credit Product',
      value: 'Credit Product'
    },
    {
      title: 'Non-Credit Product',
      value: 'Non-Credit Product'
    },
    {
      title: 'รวมหลายกลุ่มผลิตภัณฑ์',
      value: 'รวมหลายกลุ่มผลิตภัณฑ์'
    },
  ];
  productgroupSelected = null;
  dateRange = [];
  showsearch = true;

  fillterStatus = false;
  department_ouCode = '';
  workgroup_ouCode = '';
  changResetStatus = true;
  divisionList_fill: any;
  workgroupList_fill: any;
  departmentList_fill: any;
  constructor(private _service: ReportsTabService, private dialog: MatDialog, private productTurnaroundService: ProductTurnaroundService
    , private adapter: DateAdapter<any>
  ) {
    this.adapter.setLocale('th-TH');
  }

  ngOnInit() {
    this.setupCalendar(0);
    this.productTurnaroundService.divisionList().subscribe(res => {
      this.divisionList = res['data'];
      this.divisionList_data = res['data'];
      this.divisionList_fill = JSON.parse(JSON.stringify(res['data']));
    });
    this.productTurnaroundService.workgroupList().subscribe(res => {
      this.workgroupList = res['data'];
      this.workgroupList_data = res['data'];
      this.workgroupList_fill = JSON.parse(JSON.stringify(res['data']));
    });
    this.productTurnaroundService.departmentList().subscribe(res => {
      this.departmentList = res['data'];
      this.departmentList_data = res['data'];
      this.departmentList_fill = JSON.parse(JSON.stringify(res['data']));
    });
  }

  ShowSearch() {
    if (this.showsearch === true) {
      this.showsearch = false;
    } else {
      this.showsearch = true;
    }
  }

  filterSaechWorkgroup(data) {
    const arr = this.divisionList_data.filter(ele => {
      return ele.ouName === data;
    });
    const arr_workgroupList = this.workgroupList_data.filter(ele => {
      return ele.groupOuCode === arr[0].ouCode;
    });
    this.workgroupList = arr_workgroupList;
    this.workgroup = '';
    this.division = '';
    this.departmentList = [];
  }

  filterSaechDepartment(data) {
    const arr = this.workgroupList_data.filter(ele => {
      return ele.ouName === data;
    });
    const arr_Department = this.departmentList_data.filter(ele => {
      return ele.sctrOuCode === arr[0].ouCode;
    });
    this.departmentList = arr_Department;
    this.division = '';
  }

  setupCalendar(calendarIndex, option?) {
    // set min and max date : 'minStartDate' , 'maxStartDate' , 'minEndDate' , 'maxEndDate'
    // set defalut ngModule date : 'selectedDate' , 'selectedStartDate' , 'selectedEndDate'
    // buddha year : 'modifiedStartDate' 'modifiedEndDate'
    // set state calendar show or not : 'calendarState'
    // 'DKKs' Author
    // *** example option ***
    const templateCalendar = {
      minStartDate: new Date(1900, 0, 1),
      maxStartDate: undefined,
      minEndDate: new Date(1900, 0, 1),
      maxEndDate: undefined,
      selectedDate: '',
      selectedStartDate: new Date(),
      selectedEndDate: new Date(),
      modifiedStartDate: this.dateToBuddha(new Date()),
      modifiedEndDate: this.dateToBuddha(new Date()),
      calendarState: false
    };

    this.dateRange[calendarIndex] = option ? option : templateCalendar;

  }

  onSelectStart(event, calendarIndex) {
    // console.log(event);
    this.dateRange[calendarIndex].selectedStartDate = event;
    this.dateRange[calendarIndex].modifiedStartDate = this.dateToBuddha(event);
    console.log('start', this.dateRange[calendarIndex].modifiedStartDate);

    this.dateRange[calendarIndex].minEndDate = this.dateRange[calendarIndex].selectedStartDate;
  }

  onSelectEnd(event, calendarIndex) {
    // console.log(event);
    this.dateRange[calendarIndex].selectedEndDate = event;
    this.dateRange[calendarIndex].modifiedEndDate = this.dateToBuddha(event);

    this.dateRange[calendarIndex].maxStartDate = this.dateRange[calendarIndex].selectedEndDate;
    console.log('end', this.dateRange[calendarIndex].modifiedEndDate);
  }

  openCalendar(calendarIndex) {
    this.dateRange[calendarIndex].calendarState = true;

    // check if already have value selected.Need to transform to normal format
    if (this.dateRange[calendarIndex].selectedDate) {
      // dd/mm/yyyy -> yyyy-mm-dd
      const splitData = this.dateRange[calendarIndex].selectedDate.replace(/\s/g, '').split('-');
      const convertYear = splitData.map(d => {
        return `${(+d.split('/')[2] - 543)}-${d.split('/')[1]}-${d.split('/')[0]}`;
      });

      // set current date when open calendar
      this.dateRange[calendarIndex].selectedStartDate = new Date(convertYear[0]);
      this.dateRange[calendarIndex].selectedEndDate = new Date(convertYear[1]);
      this.dateRange[calendarIndex].modifiedStartDate = splitData[0];
      this.dateRange[calendarIndex].modifiedEndDate = splitData[1];
    }
  }

  getLogUserSearchData() {
    if (this.dateRange[0].selectedDate !== '') {
      this.start_date = moment(this.dateRange[0].selectedStartDate).format('YYYY-MM-DD');
      this.end_date = moment(this.dateRange[0].selectedEndDate).format('YYYY-MM-DD');
    } else {
      this.start_date = '';
      this.end_date = '';
      console.log('null');
    }
  }

  closeCalendar(calendarIndex) {
    this.dateRange[calendarIndex].calendarState = false;
  }

  clearCalendar(calendarIndex) {
    this.setupCalendar(calendarIndex);
  }

  applyCalendar(calendarIndex) {
    this.dateRange[calendarIndex].selectedDate
      = `${this.dateRange[calendarIndex].modifiedStartDate} - ${this.dateRange[calendarIndex].modifiedEndDate}`;
    this.dateRange[calendarIndex].calendarState = false;
    this.getLogUserSearchData();
  }

  dateToBuddha(dateNormal) {
    // console.log('dateNormal>>>', dateNormal);
    const now = moment(dateNormal);
    now.locale('th');
    const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();
    return now.format('DD/MM') + '/' + buddhishYear;
  }

  Onsearch() {
    const dataselected = {
      grp: this.division ? this.division['ouName'] : '',
      sctr: this.workgroup ? this.workgroup['ouName'] : '',
      dept: this.department ? this.department['ouName'] : '',
      objective: this.objectiveSelected ? this.objectiveSelected : null,
      productGroup: this.productgroupSelected ? this.productgroupSelected : null,
      startDate: this.start_date ? this.start_date : null,
      endDate: this.end_date ? this.end_date : null,
    };
    console.log('dataselected', dataselected);
    if (this.tabs1 === true) {
      this._data_reports = [];
      this._service.Getcontrolreport(dataselected).subscribe(data => {
        console.log(data);
        if (data['data']) {
          const result = data['data'];
          // console.log(result);
          // if (result['draft'] === null || result['verifly'] === null || result['buVerify'] === null
          //     || result['poCommentOther'] === null || result['verify2'] === null || result['pcAgenda'] === null
          //     || result['pcResults'] === null || result['poConclusions'] === null || result['pcConclusions'] === null
          //     || result['poIssue'] === null || result['waitingAnnoucement'] === null) {
          //     // console.log(data['message']);
          //     return Swal.fire({
          //         title: 'error',
          //         text: data['message'],
          //         icon: 'error',
          //     });
          // }
          // console.log(result.draft);
          if (result['draft'] !== null) {
            this._data_reports[0] = result['draft'];
          }
          if (result['verifly'] !== null) {
            this._data_reports[1] = result['verifly'];
          }
          if (result['buVerify'] !== null) {
            this._data_reports[2] = result['buVerify'];
          }
          if (result['poCommentOther'] !== null) {
            this._data_reports[3] = result['poCommentOther'];
          }
          if (result['verify2'] !== null) {
            this._data_reports[4] = result['verify2'];
          }
          if (result['pcAgenda'] !== null) {
            this._data_reports[5] = result['pcAgenda'];
          }
          if (result['pcResults'] !== null) {
            this._data_reports[6] = result['pcResults'];
          }
          if (result['poConclusions'] !== null) {
            this._data_reports[7] = result['poConclusions'];
          }
          if (result['pcConclusions'] !== null) {
            this._data_reports[8] = result['pcConclusions'];
          }
          if (result['poIssue'] !== null) {
            this._data_reports[9] = result['poIssue'];
          }
          if (result['waitingAnnoucement'] !== null) {
            this._data_reports[10] = result['waitingAnnoucement'];
          }
          const maxvalue = Math.max.apply(Math, this._data_reports.map(function (item) {
            return item.allRequest;
          }));
          for (let i = 0; i < this._data_reports.length; i++) {
            const cal = (this._data_reports[i]['allRequest'] * 95) / +maxvalue;
            const myStyles = {
              'width': cal + '%'
            };
            this._control_report[i] = myStyles;
            const cal2 = (this._data_reports[i]['moreThan60Days'] * 95) / +maxvalue;
            const myStyles2 = {
              'width': cal2 + '%'
            };
            this._control_report2[i] = myStyles2;
            const cal3 = (this._data_reports[i]['between31and60Days'] * 95) / +maxvalue;
            const myStyles3 = {
              'width': cal3 + '%'
            };
            this._control_report3[i] = myStyles3;
          }
          if (this._data_reports[2] && this._data_reports[2] !== null) {
            if (this._data_reports[2]['risk'] !== null &&
              this._data_reports[2]['compliance'] !== null &&
              this._data_reports[2]['operation'] !== null &&
              this._data_reports[2]['finance'] !== null &&
              this._data_reports[2]['retail'] !== null) {
              const x = [];
              x[0] = this._data_reports[2]['risk'];
              x[1] = this._data_reports[2]['compliance'];
              x[2] = this._data_reports[2]['operation'];
              x[3] = this._data_reports[2]['finance'];
              x[4] = this._data_reports[2]['retail'];
              for (let j = 0; j < x.length; j++) {
                const cal = (x[j]['allRequest'] * 85) / +maxvalue;
                const myStyles = {
                  'width': cal + '%'
                };
                this._control_report_sub[j] = myStyles;
                const cal2 = (x[j]['moreThan60Days'] * 85) / +maxvalue;
                const myStyles2 = {
                  'width': cal2 + '%'
                };
                this._control_report2_sub[j] = myStyles2;
                const cal3 = (x[j]['between31and60Days'] * 85) / +maxvalue;
                const myStyles3 = {
                  'width': cal3 + '%'
                };
                this._control_report3_sub[j] = myStyles3;
              }
            }
          }
        }
      });
    }
    if (this.tabs1 === false) {
      this._data_reportsSLA = [];
      this._service.GetcontrolreportSLA(dataselected).subscribe(data => {
        // console.log(dataselected);
        if (data['status'] === 'success') {
          if (data['data']) {
            const result = data['data'];
            // if (result['draft'] === null || result['verifly'] === null || result['buVerify'] === null
            //     || result['poCommentOther'] === null || result['verify2'] === null || result['pcAgenda'] === null
            //     || result['pcResults'] === null || result['poConclusions'] === null || result['pcConclusions'] === null
            //     || result['poIssue'] === null || result['waitingAnnoucement'] === null) {
            //     // console.log(data['message']);
            //     return Swal.fire({
            //         title: 'error',
            //         text: data['message'],
            //         icon: 'error',
            //     });
            // }
            if (result['draft'] !== null) {
              this._data_reportsSLA[0] = result['draft'];
            }
            if (result['verifly'] !== null) {
              this._data_reportsSLA[1] = result['verifly'];
            }
            if (result['buVerify'] !== null) {
              this._data_reportsSLA[2] = result['buVerify'];
            }
            if (result['poCommentOther'] !== null) {
              this._data_reportsSLA[3] = result['poCommentOther'];
            }
            if (result['verify2'] !== null) {
              this._data_reportsSLA[4] = result['verify2'];
            }
            if (result['pcAgenda'] !== null) {
              this._data_reportsSLA[5] = result['pcAgenda'];
            }
            if (result['pcResults'] !== null) {
              this._data_reportsSLA[6] = result['pcResults'];
            }
            if (result['poConclusions'] !== null) {
              this._data_reportsSLA[7] = result['poConclusions'];
            }
            if (result['pcConclusions'] !== null) {
              this._data_reportsSLA[8] = result['pcConclusions'];
            }
            if (result['poIssue'] !== null) {
              this._data_reportsSLA[9] = result['poIssue'];
            }
            if (result['waitingAnnoucement'] !== null) {
              this._data_reportsSLA[10] = result['waitingAnnoucement'];
            }
            const maxvalue = Math.max.apply(Math, this._data_reportsSLA.map(function (item) {
              return item.allRequest;
            }));
            for (let i = 0; i < this._data_reportsSLA.length; i++) {
              const cal = (this._data_reportsSLA[i]['allRequest'] * 95) / +maxvalue;
              const myStyles = {
                'width': cal + '%'
              };
              this._controlSLA_report[i] = myStyles;
              const cal2 = (this._data_reportsSLA[i]['overDue'] * 95) / +maxvalue;
              const myStyles2 = {
                'width': cal2 + '%'
              };
              this._controlSLA_report2[i] = myStyles2;
              const cal3 = (this._data_reportsSLA[i]['nearDue'] * 95) / +maxvalue;
              const myStyles3 = {
                'width': cal3 + '%'
              };
              this._controlSLA_report3[i] = myStyles3;
            }
            if (this._data_reportsSLA[2] && this._data_reportsSLA[2] !== null) {
              if (this._data_reportsSLA[2]['risk'] !== null &&
                this._data_reportsSLA[2]['compliance'] !== null &&
                this._data_reportsSLA[2]['operation'] !== null &&
                this._data_reportsSLA[2]['finance'] !== null &&
                this._data_reportsSLA[2]['retail'] !== null) {
                const x = [];
                x[0] = this._data_reportsSLA[2]['risk'];
                x[1] = this._data_reportsSLA[2]['compliance'];
                x[2] = this._data_reportsSLA[2]['operation'];
                x[3] = this._data_reportsSLA[2]['finance'];
                x[4] = this._data_reportsSLA[2]['retail'];
                for (let j = 0; j < x.length; j++) {
                  const cal = (x[j]['allRequest'] * 85) / +maxvalue;
                  const myStyles = {
                    'width': cal + '%'
                  };
                  this._controlSLA_report_sub[j] = myStyles;
                  const cal2 = (x[j]['overDue'] * 85) / +maxvalue;
                  const myStyles2 = {
                    'width': cal2 + '%'
                  };
                  this._controlSLA_report2_sub[j] = myStyles2;
                  const cal3 = (x[j]['nearDue'] * 85) / +maxvalue;
                  const myStyles3 = {
                    'width': cal3 + '%'
                  };
                  this._controlSLA_report3_sub[j] = myStyles3;
                }
              }
            }
          }
        } else {
          return Swal.fire({
            title: 'error',
            text: data['message'],
            icon: 'error',
          });
        }

      });
    }
  }

  abc(boolean) {
    if (boolean === true) {
      this.tabs1 = boolean;
      this.department = '';
      this.workgroup = '';
      this.division = '';
      this.objectiveSelected = null;
      this.productgroupSelected = null;
      this.dateRange[0].selectedDate = null;
      this.start_date = '';
      this.end_date = '';
      // console.log(this.tabs1);
    }
    if (boolean === false) {
      this.tabs1 = boolean;
      this.department = '';
      this.workgroup = '';
      this.division = '';
      this.objectiveSelected = null;
      this.productgroupSelected = null;
      this.dateRange[0].selectedDate = null;
      this.start_date = '';
      this.end_date = '';
      // console.log(this.tabs1);
    }
  }

  ReportDialog(name, title, process, notype, overDue) {
    const dialogRef = this.dialog.open(ControlReportDialogComponent, {});
    // console.log('process>>', process);
    // console.log('notype>>', notype);
    // console.log(this.start_date);
    if (overDue === undefined) {
      overDue = 'no';
    }
    sessionStorage.setItem('name', JSON.stringify(name));
    sessionStorage.setItem('title', JSON.stringify(title));
    sessionStorage.setItem('process', JSON.stringify(process));
    sessionStorage.setItem('notype', JSON.stringify(notype));
    sessionStorage.setItem('dept', JSON.stringify(this.department));
    sessionStorage.setItem('sctr', JSON.stringify(this.workgroup));
    sessionStorage.setItem('grp', JSON.stringify(this.division));
    sessionStorage.setItem('objective', JSON.stringify(this.objectiveSelected));
    sessionStorage.setItem('productGroup', JSON.stringify(this.productgroupSelected));
    sessionStorage.setItem('startDate', this.start_date);
    sessionStorage.setItem('endDate', this.end_date);
    sessionStorage.setItem('overDue', JSON.stringify(overDue));
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  filterSelection(type, value) {
    // type คือ ฝาย(departmentList) กลุ่ม(workgroupList) สาย(divisionList);
    // value คาที่ต้องนำมาหา
    // ฝ่าย:dept - กลุ่ม:sctr - สาย:grp
    console.log('====== filterSelection =====');
    console.log('filterSelection type', type);
    console.log('filterSelection value', value);
    if (type === 'department' && value !== 'resetFilter') {
      // ================================= department ==================================
      this.fillterStatus = true;
      this.department_ouCode = value.ouCode;
      console.log('001 this.workgroup:', this.workgroup);
      console.log('001 this.division:', this.division);
      if ((this.workgroup === null || this.workgroup === undefined || this.workgroup === '')
        && (this.division === null || this.division === undefined || this.division === '')) {
        // ( department : select / workgroup : null / division : null )
        this.set_list_workgroup('ouCode', value.sctrOuCode);
        this.set_list_division('ouCode', value.groupOuCode);
      } else if ((this.workgroup !== null && this.workgroup !== undefined && this.workgroup !== '')
        && (this.division === null || this.division === undefined || this.division === '')) {
        // ( department : select / workgroup : not null / division : null )
        if (this.workgroup['ouCode'] !== value.sctrOuCode) {
          if (this.changResetStatus === true) {
            this.workgroup = '';
          } else {
            this.workgroup = undefined;
          }
          this.set_list_workgroup('ouCode', value.sctrOuCode);
          this.set_list_division('ouCode', value.groupOuCode);
        } else {
          this.set_list_division('ouCode_ouCode', this.workgroup['groupOuCode'], value.groupOuCode);
        }
      } else if ((this.workgroup === null || this.workgroup === undefined || this.workgroup === '')
        && (this.division !== null && this.division !== undefined && this.division !== '')) {
        // ( department : select / workgroup : null / division : not null )
        if (this.division['ouCode'] !== value.groupOuCode) {
          if (this.changResetStatus === true) {
            this.division = '';
          } else {
            this.division = undefined;
          }
          this.set_list_workgroup('ouCode', value.sctrOuCode);
          this.set_list_division('ouCode', value.groupOuCode);
        } else {
          this.set_list_workgroup('ouCode_groupOuCode', value.sctrOuCode, this.division['ouCode']);
        }
      } else if ((this.workgroup !== null && this.workgroup !== undefined && this.workgroup !== '')
        && (this.division !== null && this.division !== undefined && this.division !== '')) {
        // ( department : select / workgroup : not null / division : not null )
      }
      // this.sendSearchAdvanced();
    } else if (type === 'workgroup' && value !== 'resetFilter') {
      // ================================= workgroup ==================================
      this.fillterStatus = true;
      this.workgroup_ouCode = value.ouCode;
      if ((this.department === null || this.department === undefined || this.department === '')
        && (this.division === null || this.division === undefined || this.division === '')) {
        // ( department : null  / workgroup : select / division : null )
        this.set_list_department('sctrOuCode', value.ouCode);
        this.set_list_division('ouCode', value.groupOuCode);
      } else if ((this.department !== null && this.department !== undefined && this.department !== '')
        && (this.division === null || this.division === undefined || this.division === '')) {
        // ( department : not null  / workgroup : select / division : null )
        if (this.department['sctrOuCode'] !== value.ouCode) {
          if (this.changResetStatus === true) {
            this.department = '';
          } else {
            this.department = undefined;
          }
          this.set_list_department('sctrOuCode', value.ouCode);
          this.set_list_division('ouCode', value.groupOuCode);
        } else {
          this.set_list_division('ouCode_ouCode', this.department['groupOuCode'], value.groupOuCode);
        }
      } else if ((this.department === null || this.department === undefined || this.department === '')
        && (this.division !== null && this.division !== undefined && this.division !== '')) {
        // ( department : null / workgroup : select / division : not null )
        if (this.division['ouCode'] !== value.groupOuCode) {
          if (this.changResetStatus === true) {
            this.division = '';
          } else {
            this.division = undefined;
          }
          this.set_list_department('sctrOuCode', value.ouCode);
          this.set_list_division('ouCode', value.groupOuCode);
        } else {
          this.set_list_department('sctrOuCode_groupOuCode', value.ouCode, this.division['ouCode']);
        }
      } else if ((this.department !== null && this.department !== undefined && this.department !== '')
        && (this.division !== null && this.division !== undefined && this.division !== '')) {
        // ( department : not null / workgroup : select / division : not null )
      }
      // this.sendSearchAdvanced();
    } else if (type === 'division' && value !== 'resetFilter') {
      // ================================= division ==================================
      this.fillterStatus = true;
      this.department_ouCode = value.ouCode;
      if ((this.department === null || this.department === undefined || this.department === '')
        && (this.workgroup === null || this.workgroup === undefined || this.workgroup === '')) {
        // ( department : null / workgroup : null / division : select)
        this.set_list_department('groupOuCode', value.ouCode);
        this.set_list_workgroup('groupOuCode', value.ouCode);
      } else if ((this.department !== null && this.department !== undefined && this.department !== '')
        && (this.workgroup === null || this.workgroup === undefined || this.workgroup === '')) {
        // ( department : not null / workgroup : null / division : select)
        if (this.department['groupOuCode'] !== value.ouCode) {
          if (this.changResetStatus === true) {
            this.department = '';
          } else {
            this.department = undefined;
          }
          this.set_list_department('groupOuCode', value.ouCode);
          this.set_list_workgroup('groupOuCode', value.ouCode);
        } else {
          this.set_list_workgroup('groupOuCode', value.ouCode);
        }
      } else if ((this.department === null || this.department === undefined || this.department === '')
        && (this.workgroup !== null && this.workgroup !== undefined && this.workgroup !== '')) {
        // ( department : null / workgroup : not null / division : select)
        if (this.workgroup['groupOuCode'] !== value.ouCode) {
          if (this.changResetStatus === true) {
            this.workgroup = '';
          } else {
            this.workgroup = undefined;
          }
          this.set_list_workgroup('groupOuCode', value.ouCode);
          this.set_list_department('groupOuCode', value.ouCode);
        } else {
          this.set_list_department('sctrOuCode_groupOuCode', this.workgroup['ouCode'], value.ouCode);
        }
      } else if ((this.department !== null && this.department !== undefined && this.department !== '')
        && (this.workgroup !== null && this.workgroup !== undefined && this.workgroup !== '')) {
        // ( department : not null / workgroup : not null / division : select)
        if (this.department['groupOuCode'] !== value.ouCode) {
          if (this.changResetStatus === true) {
            this.department = '';
          } else {
            this.department = undefined;
          }
          this.set_list_department('groupOuCode', value.ouCode);
        }
        if (this.workgroup['groupOuCode'] !== value.ouCode) {
          if (this.changResetStatus === true) {
            this.workgroup = '';
          } else {
            this.workgroup = undefined;
          }
          this.set_list_workgroup('groupOuCode', value.ouCode);
        }
      }
      // this.sendSearchAdvanced();
    } else if ((type === 'department' || type === 'workgroup' || type === 'division') && value === 'resetFilter') {

      if (this.changResetStatus === true) {
        console.log('00000-1 this.changResetStatus', this.changResetStatus);
        this.department = undefined;
        this.workgroup = undefined;
        this.division = undefined;
        this.fillterStatus = false;
        this.changResetStatus = !this.changResetStatus;
      } else {
        console.log('00000-2 this.changResetStatus', this.changResetStatus);
        this.department = '';
        this.workgroup = '';
        this.division = '';
        this.fillterStatus = false;
        this.changResetStatus = !this.changResetStatus;
      }
      // this.sendSearchAdvanced();
      this.departmentList_fill = JSON.parse(JSON.stringify(this.departmentList));
      this.workgroupList_fill = JSON.parse(JSON.stringify(this.workgroupList));
      this.divisionList_fill = JSON.parse(JSON.stringify(this.divisionList));
    }
    // this.departmentList_fill = [];
    // this.workgroupList_fill = [];
    // this.divisionList_fill = [];
    console.log('====== filterSelection end =====');
  }

  set_list_workgroup(search_at, search_ouCode1, search_ouCode2?) {
    if (search_at === 'ouCode') {
      this.workgroupList_fill = this.workgroupList.filter((ele) => {
        return ele.ouCode === search_ouCode1;
      });
      if (this.workgroupList_fill.length === 0) {
        this.workgroupList_fill = 'nondata';
        // this.workgroup = null;
      }
    } else if (search_at === 'groupOuCode') {
      this.workgroupList_fill = this.workgroupList.filter((ele) => {
        return ele.groupOuCode === search_ouCode1;
      });
      if (this.workgroupList_fill.length === 0) {
        this.workgroupList_fill = 'nondata';
        // this.workgroup = null;
      }
    } else if (search_at === 'ouCode_groupOuCode') {
      this.workgroupList_fill = this.workgroupList.filter((ele) => {
        return ele.ouCode === search_ouCode1 && ele.groupOuCode === search_ouCode2;
      });
      if (this.workgroupList_fill.length === 0) {
        this.workgroupList_fill = 'nondata';
        // this.workgroup = null;
      }
    }
  }

  set_list_division(search_at, search_ouCode1, search_ouCode2?) {
    if (search_at === 'ouCode') {
      this.divisionList_fill = this.divisionList.filter((ele) => {
        return ele.ouCode === search_ouCode1;
      });
      if (this.divisionList_fill.length === 0) {
        this.divisionList_fill = 'nondata';
        // this.division = null;
      }
    } else if (search_at === 'ouCode_ouCode') {
      this.divisionList_fill = this.divisionList.filter((ele) => {
        return ele.ouCode === search_ouCode1 && ele.ouCode === search_ouCode2;
      });
      if (this.divisionList_fill.length === 0) {
        this.divisionList_fill = 'nondata';
        // this.division = null;
      }
    }
  }

  set_list_department(search_at, search_ouCode1, search_ouCode2?) {
    if (search_at === 'sctrOuCode') {
      this.departmentList_fill = this.departmentList.filter((ele) => {
        return ele.sctrOuCode === search_ouCode1;
      });
      if (this.departmentList_fill.length === 0) {
        this.departmentList_fill = 'nondata';
        // this.department = null;
      }
    } else if (search_at === 'groupOuCode') {
      this.departmentList_fill = this.departmentList.filter((ele) => {
        return ele.groupOuCode === search_ouCode1;
      });
      if (this.departmentList_fill.length === 0) {
        this.departmentList_fill = 'nondata';
        // this.department = null;
      }
    } else if (search_at === 'sctrOuCode_groupOuCode') {
      this.departmentList_fill = this.departmentList.filter((ele) => {
        return ele.sctrOuCode === search_ouCode1 && ele.groupOuCode === search_ouCode2;
      });
      if (this.departmentList_fill.length === 0) {
        this.departmentList_fill = 'nondata';
        // this.department = null;
      }
    }
  }

}
