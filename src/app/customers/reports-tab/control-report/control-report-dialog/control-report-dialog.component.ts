import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { MatDialog } from '@angular/material';
import { ReportsTabService } from '../../reports-tab.service';

@Component({
  selector: 'app-control-report-dialog',
  templateUrl: './control-report-dialog.component.html',
  styleUrls: ['./control-report-dialog.component.scss'],
})
export class ControlReportDialogComponent implements OnInit {

  constructor(private dialog: MatDialog, private dialogRef: MatDialogRef<ControlReportDialogComponent>,
    private _service: ReportsTabService) {
    this.dialogRef.disableClose = true;
  }

  name: any;
  title: any;
  data: any;
  datatest: any;
  test: any;
  datestart: any;
  dateend: any;
  overDue: any;
  datagrp: any;
  datasctr: any;
  datadept: any;
  dataObjective: any;
  dataproductGroup: any;
  dataprocess: any;
  datanoType: any;
  ngOnInit() {
    this.name = JSON.parse(sessionStorage.getItem('name'));
    this.title = JSON.parse(sessionStorage.getItem('title'));
    // this.overDue = JSON.parse(sessionStorage.getItem('overDue'));
    // if (sessionStorage.getItem('startDate') === "undefined") {
    //   this.datestart = null;
    // } else {
    //   this.datestart = sessionStorage.getItem('startDate')
    // }

    // if (sessionStorage.getItem('endDate') === "undefined") {
    //   this.dateend = null;
    // } else {
    //   this.dateend = sessionStorage.getItem('endDate')
    // }
    // const datasend = {
    //   process: JSON.parse(sessionStorage.getItem('process')),
    //   noType: JSON.parse(sessionStorage.getItem('notype')),
    //   search: {
    //     grp: JSON.parse(sessionStorage.getItem('grp')) ? JSON.parse(sessionStorage.getItem('grp')) : null,
    //     sctr: JSON.parse(sessionStorage.getItem('sctr')) ? JSON.parse(sessionStorage.getItem('sctr')) : null,
    //     dept: JSON.parse(sessionStorage.getItem('dept')) ? JSON.parse(sessionStorage.getItem('dept')) : null,
    //     objective: JSON.parse(sessionStorage.getItem('objective')),
    //     productGroup: JSON.parse(sessionStorage.getItem('productGroup')),
    //     startDate: this.datestart,
    //     endDate: this.dateend,
    //   },
    // };
console.log(sessionStorage.getItem('overDue'));
    if (sessionStorage.getItem('overDue') === "undefined") {
      this.overDue = null;
    } else {
      this.overDue = JSON.parse(sessionStorage.getItem('overDue'))
    }

    if (sessionStorage.getItem('process') === "undefined") {
      this.dataprocess = null;
    } else {
      this.dataprocess = JSON.parse(sessionStorage.getItem('process'))
    }

    if (sessionStorage.getItem('notype') === "undefined") {
      this.datanoType = null;
    } else {
      this.datanoType = JSON.parse(sessionStorage.getItem('notype'))
    }

    if (sessionStorage.getItem('grp') === "undefined") {
      this.datagrp = null;
    } else {
      const grp = JSON.parse(sessionStorage.getItem('grp'))
      this.datagrp = grp['ouName']
    }

    if (sessionStorage.getItem('sctr') === "undefined") {
      this.datasctr = null;
    } else {
      const sctr  = JSON.parse(sessionStorage.getItem('sctr'))
      this.datasctr = sctr['ouName']
    }

    if (sessionStorage.getItem('dept') === "undefined") {
      this.datadept = null;
    } else {
      const dept  = JSON.parse(sessionStorage.getItem('dept'))
      this.datadept = dept['ouName']
    }

    if (sessionStorage.getItem('objective') === "undefined") {
      this.dataObjective = null;
    } else {
      this.dataObjective = JSON.parse(sessionStorage.getItem('objective'))
    }

    if (sessionStorage.getItem('productGroup') === "undefined") {
      this.dataproductGroup = null;
    } else {
      this.dataproductGroup = JSON.parse(sessionStorage.getItem('productGroup'))
    }

    if (sessionStorage.getItem('startDate') === "undefined" || sessionStorage.getItem('startDate') === null ) {
      this.datestart = null;
    } else {
      this.datestart = sessionStorage.getItem('startDate');
    }

    if (sessionStorage.getItem('endDate') === "undefined" || sessionStorage.getItem('endDate') === null) {
      this.dateend = null;
    } else {
      this.dateend = sessionStorage.getItem('endDate');
    }

    const datasend = {
      process: this.dataprocess,
      noType: this.datanoType,
      search: {
        grp: this.datagrp ? this.datagrp : null,
        sctr: this.datasctr ? this.datasctr : null,
        dept: this.datadept ? this.datadept : null,
        objective: this.dataObjective ? this.dataObjective : null,
        productGroup: this.dataproductGroup ? this.dataproductGroup  : null,
        startDate: this.datestart ,
        endDate: this.dateend,
      },
    };
    console.log('send>>>', datasend);
    this._service.GetControlReportDetail(datasend).subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        this.data = res['data'];
        for (let index = 0; index < this.data.length; index++) {
          const element = this.data[index];
          this.datatest = element.responsibleMan;
          if (this.datatest.length <= 1) {
            this.test = this.datatest;
          } else if (this.datatest.length > 1) {
            this.test = this.datatest[1] + ' อื่น ๆ';
          }
          // console.log(this.test);
        }
      }
    });
  }
  close() {
    this.dialogRef.close();
  }
}
