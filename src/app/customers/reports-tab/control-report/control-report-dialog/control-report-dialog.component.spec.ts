import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlReportDialogComponent } from './control-report-dialog.component';

describe('ControlReportDialogComponent', () => {
  let component: ControlReportDialogComponent;
  let fixture: ComponentFixture<ControlReportDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlReportDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlReportDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
