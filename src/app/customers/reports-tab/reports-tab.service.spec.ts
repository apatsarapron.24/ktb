import { TestBed } from '@angular/core/testing';

import { ReportsTabService } from './reports-tab.service';

describe('ReportsTabService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReportsTabService = TestBed.get(ReportsTabService);
    expect(service).toBeTruthy();
  });
});
