import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import {
  IAngularMyDpOptions
} from 'angular-mydatepicker';
import * as Highcharts from 'highcharts';
import { ProductTurnaroundService } from '../../../services/product-turnaround/product-turnaround.service';
import { DateAdapter } from '@angular/material';
import * as moment from 'moment';

declare var $: any;
@Component({
  selector: 'app-product-turnaround',
  templateUrl: './product-turnaround.component.html',
  styleUrls: ['./product-turnaround.component.scss']
})
export class ProductTurnaroundComponent implements OnInit {
  @ViewChild('charts') public chartEl: ElementRef;
  myDpOptions: IAngularMyDpOptions = {
    dateRange: false,
    dateFormat: 'dd/mm/yyyy',
    // other options are here...
    stylesData: {
      selector: 'dp',
      styles: `
           .dp .top:auto
           .dp .myDpIconLeftArrow,
           .dp .myDpIconRightArrow,
           .dp .myDpHeaderBtn {
               color: #3855c1;
            }
           .dp .myDpHeaderBtn:focus,
           .dp .myDpMonthLabel:focus,
           .dp .myDpYearLabel:focus {
               color: #3855c1;
            }
           .dp .myDpDaycell:focus,
           .dp .myDpMonthcell:focus,
           .dp .myDpYearcell:focus {
               box-shadow: inset 0 0 0 1px #66afe9;
            }
           .dp .myDpSelector:focus {
               border: 1px solid #ADD8E6;
            }
           .dp .myDpSelectorArrow:focus:before {
               border-bottom-color: #ADD8E6;
            }
           .dp .myDpCurrMonth,
           .dp .myDpMonthcell,
           .dp .myDpYearcell {
               color: #00a6e6;
            }
           .dp .myDpDaycellWeekNbr {
               color: #3855c1;
            }
           .dp .myDpPrevMonth,
           .dp .myDpNextMonth {
               color: #0098D2;
            }
           .dp .myDpWeekDayTitle {
               background-color: #0098D2;
               color: #ffffff;
            }
           .dp .myDpHeaderBtnEnabled:hover,
           .dp .myDpMonthLabel:hover,
           .dp .myDpYearLabel:hover {
               color:#0098D2;
            }
           .dp .myDpMarkCurrDay,
           .dp .myDpMarkCurrMonth,
           .dp .myDpMarkCurrYear {
               border-bottom: 2px solid #3855c1;
            }
           .dp .myDpDisabled {
               color: #999;
            }
           .dp .myDpHighlight {
               color: #74CBEC;
            }
           .dp .myDpTableSingleDay:hover,
           .dp .myDpTableSingleMonth:hover,
           .dp .myDpTableSingleYear:hover {
               background-color: #add8e6;
               color: #0098D2;
            }
           .dp .myDpRangeColor {
               background-color: #dbeaff;
            }
           .dp .myDpSelectedDay,
           .dp .myDpSelectedMonth,
           .dp .myDpSelectedYear {
               background-color: #0098D2;
               color: #ffffff;
            }
            `
    }
  };


  // model graft
  maximum: any;
  minnimum: any;
  average: any;
  graphList: any = [];
  nameProduct: any = [];
  draft: any = [];
  verify: any = [];
  buVerify: any = [];
  buVerifySumation: any = [];
  poCommentOther: any = [];
  verify2: any = [];
  pcAgenda: any = [];
  pcResults: any = [];
  poConclusion: any = [];
  pcConclusion: any = [];
  poIssue: any = [];
  waitingAnnoucement: any = [];

  fillterStatus = false;
  department_ouCode = '';
  workgroup_ouCode = '';
  changResetStatus = true;
  divisionList_fill: any;
  workgroupList_fill: any;
  departmentList_fill: any;

  // dropdown modal
  divisionList: any;
  workgroupList: any;
  departmentList: any;
  divisionList_data: any;
  workgroupList_data: any;
  departmentList_data: any;
  objective = [
    {
      title: 'ทั้งหมด',
      value: 'ทั้งหมด'
    },
    {
      title: 'ผลิตภัณฑ์ใหม่',
      value: 'ผลิตภัณฑ์ใหม่'
    },
    {
      title: 'ทบทวนผลิตภัณฑ์ระหว่างปี',
      value: 'ทบทวนผลิตภัณฑ์ระหว่างปี'
    },
    {
      title: 'ทบทวนผลิตภัณฑ์ประจำปี',
      value: 'ทบทวนผลิตภัณฑ์ประจำปี'
    },
  ];

  objectiveSelected = null;

  productgroup = [
    {
      title: 'Credit Product',
      value: 'Credit Product'
    },
    {
      title: 'Non-Credit Product',
      value: 'Non-Credit Product'
    },
    {
      title: 'รวมหลายกลุ่มผลิตภัณฑ์',
      value: 'รวมหลายกลุ่มผลิตภัณฑ์'
    },
  ];

  productnumSelected = null;
  department : any;
  workgroup : any;
  division : any;
  departmentname = '';
  workgroupname = '';
  divisionname = '';

  productgroupSelected = null;
  checksearch = 'false';
  showsearch = true;
  dateRange = [];
  start_date: any;
  end_date: any;
  myOptions: any;
  constructor(private productTurnaroundService: ProductTurnaroundService, private adapter: DateAdapter<any>) {
    this.adapter.setLocale('th-TH');
  }

  ngOnInit() {
    // this.createChart(this.chartEl.nativeElement, this.myOptions);
    this.setupCalendar(0);
    this.productTurnaroundService.divisionList().subscribe(res => {
      this.divisionList = res['data'];
      this.divisionList_data = res['data'];
      this.divisionList_fill = JSON.parse(JSON.stringify(res['data']));
    });
    this.productTurnaroundService.workgroupList().subscribe(res => {
      this.workgroupList = res['data'];
      this.workgroupList_data = res['data'];
      this.workgroupList_fill = JSON.parse(JSON.stringify(res['data']));
    });
    this.productTurnaroundService.departmentList().subscribe(res => {
      this.departmentList = res['data'];
      this.departmentList_data = res['data'];
      this.departmentList_fill = JSON.parse(JSON.stringify(res['data']));
    });
  }

  ShowSearch() {
    if (this.showsearch === true) {
      this.showsearch = false;
    } else {
      this.showsearch = true;
    }
  }

  SearchChat(searchvalue) {
    this.checksearch = 'true';
    console.log('this.division', this.division);
    // this.divisionname = this.division.ouName;
    const searchData = {
      grp: this.division ? this.division['ouName'] : '',
      sctr: this.workgroup ? this.workgroup['ouName'] : '',
      dept: this.department ? this.department['ouName'] : '',
      objective: this.objectiveSelected ? this.objectiveSelected : '',
      productGroup: this.productgroupSelected ? this.productgroupSelected : '',
      startDate: this.start_date ? this.start_date : '',
      endDate: this.end_date ? this.end_date : '',
      limitData: searchvalue ? searchvalue : '5',
    };
    console.log('senddata >>', searchData);
    this.productTurnaroundService.getProductTurnaround(searchData).subscribe(res => {
      // console.log('res', res);
      const getData = res['data'];
      console.log('res >>>>>', getData);
      this.maximum = getData['maximum'];
      this.minnimum = getData['minnimum'];
      this.average = getData['average'];
      this.graphList = getData['graphList'];
      for (let index = 0; index < this.graphList.length; index++) {
        const element = this.graphList[index];
        this.nameProduct.push(element.nameProduct);
        this.draft.push(element.draft);
        this.verify.push(element.verify);
        this.buVerify.push(element.buVerify);
        this.buVerifySumation.push(element.buVerify.buVerifySumation);
        this.poCommentOther.push(element.poCommentOther);
        this.verify2.push(element.verify2);
        this.pcAgenda.push(element.pcAgenda);
        this.pcResults.push(element.pcResults);
        this.poConclusion.push(element.poConclusion);
        this.pcConclusion.push(element.pcConclusion);
        this.poIssue.push(element.poIssue);
        this.waitingAnnoucement.push(element.waitingAnnoucement);
      }
      console.log('res>>>>', this.buVerify);
      // for (let index = 0; index < this.buVerify.length; index++) {
      //   const elementbu = this.buVerify[index];
      //   this.buVerifySumation.push(elementbu.buVerifySumation);
      // }
      this.myOptions = {
        chart: {
          type: 'bar'
        },
        title: {
          text: ''
        },
        xAxis: {
          categories: this.nameProduct
        },

        yAxis: {
          // min: 0,
          title: {
            text: ''
          },
          stackLabels: {
            enabled: true,
            // style: {
            //   fontWeight: 'bold',
            //   color: ( // theme
            //     Highcharts.defaultOptions.title.style &&
            //     Highcharts.defaultOptions.title.style.color
            //   ) || '#4B4B4B'
            // }
          }
        },
        legend: {
          reversed: true,
          // layout: 'vertical',
          // align: 'right',
          // verticalAlign: 'bottom',
          x: -40,
          y: 80,
          // floating: true,
          // borderWidth: 1,
        },
        plotOptions: {
          series: {
            stacking: 'normal'
          },
          bar: {
            dataLabels: {
              // enabled: true
            },
            showInLegend: false
          }
        },
        tooltip: {
          formatter: function () {

            if (this.series.name === 'หน่วยงานที่เกี่ยวข้องให้ความเห็น') {
              console.log(this)
              console.log("INDEX :", this.point.index);
              console.log("NAME :", (this.series.name));
              console.log("BU", this.series.userOptions.buVerify[this.point.index]);

              return 'หน่วยงานที่เกี่ยวข้องให้ความเห็น : ' + this.y + ' , ' +
                'Risk : ' + this.series.userOptions.buVerify[this.point.index].risk + ' , ' +
                ' Compliance : ' + this.series.userOptions.buVerify[this.point.index].compliance + ' , ' +
                ' Finance : ' + this.series.userOptions.buVerify[this.point.index].finance + ' , ' +
                ' Operation : ' + this.series.userOptions.buVerify[this.point.index].operation + ' , ' +
                ' Retail Shared Services : ' + this.series.userOptions.buVerify[this.point.index].retailSharedServices
            } else {
              return this.series.name + " : " + this.y
            }
          }
        },
        // legend: {
        // layout: 'vertical',
        // align: 'right',
        // verticalAlign: 'bottom',
        //   // x: -40,
        //   y: 80,
        //   floating: true,
        //   borderWidth: 1,
        // },
        colors: [
          '#F08800',
          '#F59B01',
          '#FAB905',
          '#FFCA05',
          '#868686',
          '#BBBBBB',
          '#124F68',
          '#2E7A99',
          '#408DAD',
          '#67B3D2',
          '#8DD2EE'
        ],
        series: [{
          name: 'รอการประกาศใช้ผลิตภัณฑ์',
          data: this.waitingAnnoucement,
        },
        {
          name: 'PO สรุปประเด็นคงค้าง',
          data: this.poIssue
        }, {
          name: 'PC พิจารณาการสรุปผลการอนุมัติ',
          data: this.pcConclusion
        }, {
          name: 'PO สรุปผลการอนุมัติ',
          data: this.poConclusion
        }, {
          name: 'PC บันทึกผลการอนุมัติ',
          data: this.pcResults
        }, {
          name: 'PC พิจารณาบรรจุวาระ',
          data: this.pcAgenda
        }, {
          name: 'ผู้บริหารของ PO พิจารณาใบคำขอ',
          data: this.verify2
        }, {
          name: 'PO ให้ความเห็นเพิ่มเติม',
          data: this.poCommentOther
        }, {
          name: 'หน่วยงานที่เกี่ยวข้องให้ความเห็น',
          data: this.buVerifySumation,
          buVerify: this.buVerify
        }, {
          name: 'ผู้บริหารของ PO พิจารณาใบคำขอ',
          data: this.verify
        }, {
          name: 'PO จัดทำใบคำขอ',
          data: this.draft
        },]
      };

      this.productTurnaroundService.createChart(this.chartEl.nativeElement, this.myOptions);
      this.nameProduct = [];
      this.draft = [];
      this.verify = [];
      this.buVerify = [];
      this.poCommentOther = [];
      this.verify2 = [];
      this.pcAgenda = [];
      this.pcResults = [];
      this.poConclusion = [];
      this.pcConclusion = [];
      this.poIssue = [];
      this.waitingAnnoucement = [];
      this.buVerifySumation = [];
    });
    // this.myOptions = {
    //   chart: {
    //     type: 'bar'
    //   },
    //   title: {
    //     text: ''
    //   },
    //   xAxis: {
    //     categories: this.nameProduct
    //   },

    //   yAxis: {
    //     // min: 0,
    //     title: {
    //       text: ''
    //     },
    //     stackLabels: {
    //       enabled: true,
    //       // style: {
    //       //   fontWeight: 'bold',
    //       //   color: ( // theme
    //       //     Highcharts.defaultOptions.title.style &&
    //       //     Highcharts.defaultOptions.title.style.color
    //       //   ) || '#4B4B4B'
    //       // }
    //     }
    //   },
    //   legend: {
    //     reversed: true,
    //     // layout: 'vertical',
    //     // align: 'right',
    //     // verticalAlign: 'bottom',
    //     x: -40,
    //     y: 80,
    //     // floating: true,
    //     // borderWidth: 1,
    //   },
    //   plotOptions: {
    //     series: {
    //       stacking: 'normal'
    //     },
    //     bar: {
    //       dataLabels: {
    //         // enabled: true
    //       },
    //       showInLegend: false
    //     }
    //   },
    //   // legend: {
    //   // layout: 'vertical',
    //   // align: 'right',
    //   // verticalAlign: 'bottom',
    //   //   // x: -40,
    //   //   y: 80,
    //   //   floating: true,
    //   //   borderWidth: 1,
    //   // },
    //   colors: [
    //     '#F08800',
    //     '#F59B01',
    //     '#FAB905',
    //     '#FFCA05',
    //     '#868686',
    //     '#BBBBBB',
    //     '#124F68',
    //     '#2E7A99',
    //     '#408DAD',
    //     '#67B3D2',
    //     '#8DD2EE'
    //   ],
    //   series: [{
    //     name: 'รอการประกาศใช้ผลิตภัณฑ์',
    //     data: this.waitingAnnoucement,
    //   },
    //   {
    //     name: 'PO สรุปประเด็นคงค้าง',
    //     data: this.poIssue
    //   }, {
    //     name: 'PC พิจารณาการสรุปผลการอนุมัติ',
    //     data: this.pcConclusion
    //   }, {
    //     name: 'PO สรุปผลการอนุมัติ',
    //     data: this.poConclusion
    //   }, {
    //     name: 'PC บันทึกผลการอนุมัติ',
    //     data: this.pcResults
    //   }, {
    //     name: 'PC พิจารณาบรรจุวาระ',
    //     data: this.pcAgenda
    //   }, {
    //     name: 'ผู้บริหารของ PO พิจารณาใบคำขอ',
    //     data: this.verify2
    //   }, {
    //     name: 'PO ให้ความเห็นเพิ่มเติม',
    //     data: this.poCommentOther
    //   }, {
    //     name: ['หน่วยงานที่เกี่ยวข้องให้ความเห็น'],
    //     data: this.buVerifySumation,
    //   }, {
    //     name: 'ผู้บริหารของ PO พิจารณาใบคำขอ',
    //     data: this.verify
    //   }, {
    //     name: 'PO จัดทำใบคำขอ',
    //     data: this.draft
    //   },]
    // };
    // this.productTurnaroundService.createChart(this.chartEl.nativeElement, this.myOptions);
    // this.nameProduct = [];
    // this.draft = [];
    // this.verify = [];
    // this.buVerify = [];
    // this.poCommentOther = [];
    // this.verify2 = [];
    // this.pcAgenda = [];
    // this.pcResults = [];
    // this.poConclusion = [];
    // this.pcConclusion = [];
    // this.poIssue = [];
    // this.waitingAnnoucement = [];
    // this.buVerifySumation = [];
    // this.myOptions = [];
  }

  // filter
  filterSaechWorkgroup(data) {
    const arr = this.divisionList_data.filter(ele => {
      return ele.ouName === data;
    });
    const arr_workgroupList = this.workgroupList_data.filter(ele => {
      return ele.groupOuCode === arr[0].ouCode;
    });
    this.workgroupList = arr_workgroupList;
    this.workgroup = '';
    this.division = '';
    this.departmentList = [];
  }
  filterSaechDepartment(data) {
    const arr = this.workgroupList_data.filter(ele => {
      return ele.ouName === data;
    });
    const arr_Department = this.departmentList_data.filter(ele => {
      return ele.sctrOuCode === arr[0].ouCode;
    });
    this.departmentList = arr_Department;
    this.division = '';
  }

  setupCalendar(calendarIndex, option?) {
    // set min and max date : 'minStartDate' , 'maxStartDate' , 'minEndDate' , 'maxEndDate'
    // set defalut ngModule date : 'selectedDate' , 'selectedStartDate' , 'selectedEndDate'
    // buddha year : 'modifiedStartDate' 'modifiedEndDate'
    // set state calendar show or not : 'calendarState'
    // 'DKKs' Author
    // *** example option ***
    const templateCalendar = {
      minStartDate: new Date(1900, 0, 1),
      maxStartDate: undefined,
      minEndDate: new Date(1900, 0, 1),
      maxEndDate: undefined,
      selectedDate: '',
      selectedStartDate: new Date(),
      selectedEndDate: new Date(),
      modifiedStartDate: this.dateToBuddha(new Date()),
      modifiedEndDate: this.dateToBuddha(new Date()),
      calendarState: false
    };

    this.dateRange[calendarIndex] = option ? option : templateCalendar;

  }

  onSelectStart(event, calendarIndex) {
    // console.log(event);
    this.dateRange[calendarIndex].selectedStartDate = event;
    this.dateRange[calendarIndex].modifiedStartDate = this.dateToBuddha(event);
    console.log('start', this.dateRange[calendarIndex].modifiedStartDate);

    this.dateRange[calendarIndex].minEndDate = this.dateRange[calendarIndex].selectedStartDate;
  }

  onSelectEnd(event, calendarIndex) {
    // console.log(event);
    this.dateRange[calendarIndex].selectedEndDate = event;
    this.dateRange[calendarIndex].modifiedEndDate = this.dateToBuddha(event);

    this.dateRange[calendarIndex].maxStartDate = this.dateRange[calendarIndex].selectedEndDate;
    console.log('end', this.dateRange[calendarIndex].modifiedEndDate);
  }

  openCalendar(calendarIndex) {
    this.dateRange[calendarIndex].calendarState = true;

    // check if already have value selected.Need to transform to normal format
    if (this.dateRange[calendarIndex].selectedDate) {
      // dd/mm/yyyy -> yyyy-mm-dd
      const splitData = this.dateRange[calendarIndex].selectedDate.replace(/\s/g, '').split('-');
      const convertYear = splitData.map(d => {
        return `${(+d.split('/')[2] - 543)}-${d.split('/')[1]}-${d.split('/')[0]}`;
      });

      // set current date when open calendar
      this.dateRange[calendarIndex].selectedStartDate = new Date(convertYear[0]);
      this.dateRange[calendarIndex].selectedEndDate = new Date(convertYear[1]);
      this.dateRange[calendarIndex].modifiedStartDate = splitData[0];
      this.dateRange[calendarIndex].modifiedEndDate = splitData[1];
    }
  }

  getLogUserSearchData() {
    if (this.dateRange[0].selectedDate !== '') {
      this.start_date = moment(this.dateRange[0].selectedStartDate).format('YYYY-MM-DD');
      this.end_date = moment(this.dateRange[0].selectedEndDate).format('YYYY-MM-DD');
    } else {
      this.start_date = '';
      this.end_date = '';
      console.log('null');
    }
  }

  closeCalendar(calendarIndex) {
    this.dateRange[calendarIndex].calendarState = false;
  }

  clearCalendar(calendarIndex) {
    this.setupCalendar(calendarIndex);
  }

  applyCalendar(calendarIndex) {
    this.dateRange[calendarIndex].selectedDate
      = `${this.dateRange[calendarIndex].modifiedStartDate} - ${this.dateRange[calendarIndex].modifiedEndDate}`;
    this.dateRange[calendarIndex].calendarState = false;
    this.getLogUserSearchData();
  }

  dateToBuddha(dateNormal) {
    // console.log('dateNormal>>>', dateNormal);
    const now = moment(dateNormal);
    now.locale('th');
    const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();
    return now.format('DD/MM') + '/' + buddhishYear;
  }

  filterSelection(type, value) {
    // type คือ ฝาย(departmentList) กลุ่ม(workgroupList) สาย(divisionList);
    // value คาที่ต้องนำมาหา
    // ฝ่าย:dept - กลุ่ม:sctr - สาย:grp
    console.log('====== filterSelection =====');
    console.log('filterSelection type', type);
    console.log('filterSelection value', value);
    if (type === 'department' && value !== 'resetFilter') {
      // ================================= department ==================================
      this.fillterStatus = true;
      this.department_ouCode = value.ouCode;
      console.log('001 this.workgroup:', this.workgroup );
      console.log('001 this.division:', this.division );
      if ((this.workgroup === null || this.workgroup === undefined || this.workgroup === '')
      && (this.division === null || this.division === undefined || this.division === '') ) {
         // ( department : select / workgroup : null / division : null )
         this.set_list_workgroup('ouCode', value.sctrOuCode);
         this.set_list_division('ouCode', value.groupOuCode);
      } else if ((this.workgroup !== null && this.workgroup !== undefined && this.workgroup !== '')
      && (this.division === null || this.division === undefined || this.division === '')) {
        // ( department : select / workgroup : not null / division : null )
        if (this.workgroup['ouCode'] !== value.sctrOuCode) {
          if (this.changResetStatus === true) {
            this.workgroup = '';
          } else {
            this.workgroup = undefined;
          }
          this.set_list_workgroup('ouCode', value.sctrOuCode);
          this.set_list_division('ouCode', value.groupOuCode);
        } else {
          this.set_list_division('ouCode_ouCode', this.workgroup['groupOuCode'], value.groupOuCode);
        }
      } else if ((this.workgroup === null || this.workgroup === undefined || this.workgroup === '')
      && (this.division !== null &&  this.division !== undefined &&  this.division !== '')) {
        // ( department : select / workgroup : null / division : not null )
        if (this.division['ouCode'] !== value.groupOuCode) {
          if (this.changResetStatus === true) {
            this.division = '';
          } else {
            this.division = undefined;
          }
          this.set_list_workgroup('ouCode', value.sctrOuCode);
          this.set_list_division('ouCode', value.groupOuCode);
        } else {
          this.set_list_workgroup('ouCode_groupOuCode', value.sctrOuCode, this.division['ouCode']);
        }
      } else if ((this.workgroup !== null && this.workgroup !== undefined && this.workgroup !== '')
      && (this.division !== null &&  this.division !== undefined &&  this.division !== '')) {
        // ( department : select / workgroup : not null / division : not null )
      }
      // this.sendSearchAdvanced();
    } else if (type === 'workgroup' && value !== 'resetFilter') {
      // ================================= workgroup ==================================
      this.fillterStatus = true;
      this.workgroup_ouCode = value.ouCode;
      if ((this.department === null || this.department === undefined || this.department === '')
      && (this.division === null || this.division === undefined || this.division === '')) {
        // ( department : null  / workgroup : select / division : null )
        this.set_list_department('sctrOuCode', value.ouCode);
        this.set_list_division('ouCode', value.groupOuCode);
      } else if ((this.department !== null && this.department !== undefined && this.department !== '')
      && (this.division === null || this.division === undefined || this.division === '') ) {
        // ( department : not null  / workgroup : select / division : null )
        if (this.department['sctrOuCode'] !== value.ouCode) {
          if (this.changResetStatus === true) {
            this.department = '';
          } else {
            this.department = undefined;
          }
          this.set_list_department('sctrOuCode', value.ouCode);
          this.set_list_division('ouCode', value.groupOuCode);
        } else {
          this.set_list_division('ouCode_ouCode', this.department['groupOuCode'], value.groupOuCode);
        }
      } else if ((this.department === null || this.department === undefined || this.department === '')
      && (this.division !== null && this.division !== undefined && this.division !== '')) {
        // ( department : null / workgroup : select / division : not null )
        if (this.division['ouCode'] !== value.groupOuCode) {
          if (this.changResetStatus === true) {
            this.division = '';
          } else {
            this.division = undefined;
          }
          this.set_list_department('sctrOuCode', value.ouCode);
          this.set_list_division('ouCode', value.groupOuCode);
        } else {
          this.set_list_department('sctrOuCode_groupOuCode', value.ouCode, this.division['ouCode']);
        }
      } else if ((this.department !== null && this.department !== undefined && this.department !== '')
      && (this.division !== null && this.division !== undefined && this.division !== '')) {
        // ( department : not null / workgroup : select / division : not null )
      }
      // this.sendSearchAdvanced();
    }  else if (type === 'division' && value !== 'resetFilter') {
      // ================================= division ==================================
      this.fillterStatus = true;
      this.department_ouCode = value.ouCode;
      if ((this.department === null || this.department === undefined || this.department === '')
      && (this.workgroup === null || this.workgroup === undefined || this.workgroup === '')) {
        // ( department : null / workgroup : null / division : select)
        this.set_list_department('groupOuCode', value.ouCode);
        this.set_list_workgroup('groupOuCode', value.ouCode);
      } else if ((this.department !== null && this.department !== undefined && this.department !== '')
      && (this.workgroup === null || this.workgroup === undefined || this.workgroup === '')) {
        // ( department : not null / workgroup : null / division : select)
        if (this.department['groupOuCode'] !== value.ouCode) {
          if (this.changResetStatus === true) {
            this.department = '';
          } else {
            this.department = undefined;
          }
          this.set_list_department('groupOuCode', value.ouCode);
          this.set_list_workgroup('groupOuCode', value.ouCode);
        } else {
          this.set_list_workgroup('groupOuCode', value.ouCode);
        }
      } else if ((this.department === null || this.department === undefined || this.department === '')
      && (this.workgroup !== null && this.workgroup !== undefined && this.workgroup !== '')) {
         // ( department : null / workgroup : not null / division : select)
         if (this.workgroup['groupOuCode'] !== value.ouCode) {
          if (this.changResetStatus === true) {
            this.workgroup = '';
          } else {
            this.workgroup = undefined;
          }
          this.set_list_workgroup('groupOuCode', value.ouCode);
          this.set_list_department('groupOuCode', value.ouCode);
        } else {
          this.set_list_department('sctrOuCode_groupOuCode', this.workgroup['ouCode'], value.ouCode );
        }
      } else if ((this.department !== null && this.department !== undefined && this.department !== '')
      && (this.workgroup !== null && this.workgroup !== undefined && this.workgroup !== '')) {
        // ( department : not null / workgroup : not null / division : select)
        if (this.department['groupOuCode'] !== value.ouCode) {
          if (this.changResetStatus === true) {
            this.department = '';
          } else {
            this.department = undefined;
          }
          this.set_list_department('groupOuCode', value.ouCode);
        }
        if (this.workgroup['groupOuCode'] !== value.ouCode) {
          if (this.changResetStatus === true) {
            this.workgroup = '';
          } else {
            this.workgroup = undefined;
          }
          this.set_list_workgroup('groupOuCode', value.ouCode);
        }
      }
      // this.sendSearchAdvanced();
    } else if ( (type === 'department' || type === 'workgroup' || type === 'division') && value === 'resetFilter') {

      if (this.changResetStatus === true) {
        console.log('00000-1 this.changResetStatus', this.changResetStatus);
        this.department = undefined;
        this.workgroup = undefined;
        this.division = undefined;
        this.fillterStatus = false;
        this.changResetStatus = !this.changResetStatus;
      } else {
        console.log('00000-2 this.changResetStatus', this.changResetStatus);
        this.department = '';
        this.workgroup = '';
        this.division = '';
        this.fillterStatus = false;
        this.changResetStatus = !this.changResetStatus;
      }
      // this.sendSearchAdvanced();
      this.departmentList_fill = JSON.parse(JSON.stringify(this.departmentList));
      this.workgroupList_fill = JSON.parse(JSON.stringify(this.workgroupList));
      this.divisionList_fill = JSON.parse(JSON.stringify(this.divisionList));
    }
    // this.departmentList_fill = [];
    // this.workgroupList_fill = [];
    // this.divisionList_fill = [];
    console.log('====== filterSelection end =====');
  }

  set_list_workgroup(search_at, search_ouCode1, search_ouCode2?) {
    if (search_at === 'ouCode') {
      this.workgroupList_fill = this.workgroupList.filter( (ele) => {
        return ele.ouCode === search_ouCode1;
      });
      if (this.workgroupList_fill.length === 0) {
        this.workgroupList_fill = 'nondata';
        // this.workgroup = null;
      }
    } else if (search_at === 'groupOuCode') {
      this.workgroupList_fill = this.workgroupList.filter( (ele) => {
        return ele.groupOuCode === search_ouCode1 ;
      });
      if (this.workgroupList_fill.length === 0) {
        this.workgroupList_fill = 'nondata';
        // this.workgroup = null;
      }
    } else if (search_at === 'ouCode_groupOuCode') {
      this.workgroupList_fill = this.workgroupList.filter( (ele) => {
        return ele.ouCode === search_ouCode1 && ele.groupOuCode === search_ouCode2;
      });
      if (this.workgroupList_fill.length === 0) {
        this.workgroupList_fill = 'nondata';
        // this.workgroup = null;
      }
    }
  }

  set_list_division(search_at, search_ouCode1, search_ouCode2?) {
    if (search_at === 'ouCode') {
      this.divisionList_fill = this.divisionList.filter( (ele) => {
        return ele.ouCode === search_ouCode1;
      });
      if (this.divisionList_fill.length === 0) {
        this.divisionList_fill = 'nondata';
        // this.division = null;
      }
    } else if (search_at === 'ouCode_ouCode') {
      this.divisionList_fill = this.divisionList.filter( (ele) => {
        return ele.ouCode === search_ouCode1 && ele.ouCode === search_ouCode2;
      });
      if (this.divisionList_fill.length === 0) {
        this.divisionList_fill = 'nondata';
        // this.division = null;
      }
    }
  }

  set_list_department(search_at, search_ouCode1, search_ouCode2?) {
    if (search_at === 'sctrOuCode') {
      this.departmentList_fill = this.departmentList.filter( (ele) => {
        return ele.sctrOuCode === search_ouCode1;
      });
      if (this.departmentList_fill.length === 0) {
        this.departmentList_fill = 'nondata';
        // this.department = null;
      }
    } else if (search_at === 'groupOuCode') {
      this.departmentList_fill = this.departmentList.filter( (ele) => {
        return ele.groupOuCode === search_ouCode1 ;
      });
      if (this.departmentList_fill.length === 0) {
        this.departmentList_fill = 'nondata';
        // this.department = null;
      }
    } else if (search_at === 'sctrOuCode_groupOuCode') {
      this.departmentList_fill = this.departmentList.filter( (ele) => {
        return ele.sctrOuCode === search_ouCode1 && ele.groupOuCode === search_ouCode2;
      });
      if (this.departmentList_fill.length === 0) {
        this.departmentList_fill = 'nondata';
        // this.department = null;
      }
    }
  }
}
