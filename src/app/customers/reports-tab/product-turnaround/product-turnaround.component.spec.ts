import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductTurnaroundComponent } from './product-turnaround.component';

describe('ProductTurnaroundComponent', () => {
  let component: ProductTurnaroundComponent;
  let fixture: ComponentFixture<ProductTurnaroundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductTurnaroundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductTurnaroundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
