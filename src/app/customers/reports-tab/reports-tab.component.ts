import { Component, OnInit } from '@angular/core';
import { RouterLink, Router, NavigationEnd } from '@angular/router';

@Component({
    selector: 'app-reports-tab',
    templateUrl: './reports-tab.component.html',
    styleUrls: ['./reports-tab.component.scss']
})
export class ReportsTabComponent implements OnInit {
    tab_number = 1;

    constructor(private router: Router) {
    }

    ngOnInit() {
    }

    click_tab(num: number) {
        if (num) {
            this.tab_number = num;
        }
    }

    backToDashboardPage() {
        localStorage.setItem('requestId', '');
        this.router.navigate(['dashboard1']);
    }
}
