import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskControlReportComponent } from './risk-control-report.component';

describe('RiskControlReportComponent', () => {
  let component: RiskControlReportComponent;
  let fixture: ComponentFixture<RiskControlReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiskControlReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskControlReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
