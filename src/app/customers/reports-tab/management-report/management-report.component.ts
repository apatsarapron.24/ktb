import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import * as Highcharts from 'highcharts';
import { Datagrahp, Datamocup, Labelgrahp, Labelgrahp2, ManagementReports, ManagementReports2 } from './management-report.model';
import { ProductTurnaroundService } from '../../../services/product-turnaround/product-turnaround.service';
import { Person, ReportsTabService } from '../reports-tab.service';
import { map } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-management-report',
    templateUrl: './management-report.component.html',
    styleUrls: ['./management-report.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ManagementReportComponent implements OnInit {
    people: Person[] = [];
    selectedPeople = [];
    dropdownSettings: IDropdownSettings;
    dropdownList2;
    selectedItems2;
    dropdownSettings2: IDropdownSettings;
    categorytype: string;
    hide = true;
    department = '';
    divisionList_data: any;
    workgroupList_data: any;
    workgroupList: any;
    departmentList: any;
    departmentList_data: any;
    workgroup = '';
    division = '';
    divisionList: any;
    aspect = 'มุมมองรายปี';
    managegraph = {
        indicators: {
            transaction: [],
            compensation: [],
        },
        product: [],
        aspect: null
    };
    transaction = [];
    compensation = [];
    checknamegraph =
        { name: ['วงเงินโครงการ', 'Limit', 'Outstanding', 'A/R Turnover', 'Advance Payment', 'Utilization/Turnover', 'จำนวนลูกค้า'] };
    checknamegraph2 = { name: ['NII', 'Non-NII', 'RAROC', 'ROI', 'EP'] };
    checkgraph = [];
    checkgraph2 = [];
    valueserie = [];
    valueserie2 = [];
    valueport = [];
    valueport2 = [];
    showsearch = true;

    fillterStatus = false;
    department_ouCode = '';
    workgroup_ouCode = '';
    changResetStatus = true;
    divisionList_fill: any;
    workgroupList_fill: any;
    departmentList_fill: any;
    constructor(private productTurnaroundService: ProductTurnaroundService, private _service: ReportsTabService) {
    }

    ngOnInit() {
        this._service.getPeople()
            .pipe(map(x => x.filter(y => !y.disabled)))
            .subscribe((res) => {
                this.people = res;
                console.log('this.people',this.people);
            });
        this.productTurnaroundService.divisionList().subscribe(res => {
            this.divisionList = res['data'];
            this.divisionList_data = res['data'];
            this.divisionList_fill = JSON.parse(JSON.stringify(res['data']));
        });
        this.productTurnaroundService.workgroupList().subscribe(res => {
            this.workgroupList = res['data'];
            this.workgroupList_data = res['data'];
            this.workgroupList_fill = JSON.parse(JSON.stringify(res['data']));
        });
        this.productTurnaroundService.departmentList().subscribe(res => {
            this.departmentList = res['data'];
            this.departmentList_data = res['data'];
            this.departmentList_fill = JSON.parse(JSON.stringify(res['data']));
        });
        this.categorytype = 'มุมมองรายไตรมาส';
        // console.log(this._series);
        this.dropdownSettings = {
            singleSelection: false,
            idField: 'idindex',
            textField: 'labelName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 4,
            allowSearchFilter: false
        };
        this.selectedItems2 = [];
        this.dropdownSettings2 = {
            singleSelection: false,
            idField: 'requestId',
            textField: 'productName',
            selectAllText: 'ทั้งหมด',
            unSelectAllText: 'ยกเลิกทั้งหมด',
            itemsShowLimit: 3,
            allowSearchFilter: false
        };
        const par = {
            'grp': this.department ? this.department : null,
            'sctr': this.workgroup ? this.workgroup : null,
            'dept': this.division ? this.division : null,
            'announced': true
        };
        this._service.Getsearchriskcontrolreport(par).subscribe(result => {
            if (result['status'] === 'success') {
                for (let i = 0; i < result['data']['product'].length; i++) {
                    if (result['data']['product'][i]['productName'] === null) {
                        result['data']['product'].splice(i, 1);
                    }
                }
                this.dropdownList2 = result['data']['product'];
            }
        });
    }

    ShowSearch() {
        if (this.showsearch === true) {
            this.showsearch = false;
        } else {
            this.showsearch = true;
        }
    }

    onSearch() {
        this.checkgraph = [
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined
        ];
        this.checkgraph2 = [
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined
        ];
        this.transaction = [];
        this.compensation = [];
        console.log('this.selectedPeople',this.selectedPeople);
        for (let i = 0; i < this.selectedPeople.length; i++) {
            const test = this.checknamegraph.name.filter(e => {
                return e === this.selectedPeople[i];
            });
            if (test && test.length) {
                this.transaction.push(test[0]);
            }
            const test2 = this.checknamegraph2.name.filter(e => {
                return e === this.selectedPeople[i];
            });
            if (test2 && test2.length) {
                this.compensation.push(test2[0]);
            }
        }
        if (this.transaction.length === this.checknamegraph.name.length) {
            this.transaction = [];
            this.transaction[0] = 'ทั้งหมด';
        }
        // console.log(this.transaction);
        // console.log(this.compensation);
        if (this.people.length === this.selectedPeople.length) {
            this.managegraph = {
                indicators: {
                    transaction: this.transaction ? this.transaction : [],
                    compensation: this.compensation ? this.compensation : [],
                },
                product: this.selectedItems2 ? this.selectedItems2 : [],
                aspect: this.aspect ? this.aspect : null
            };
        } else {
            this.managegraph = {
                indicators: {
                    transaction: this.transaction ? this.transaction : [],
                    compensation: this.compensation ? this.compensation : [],
                },
                product: this.selectedItems2 ? this.selectedItems2 : [],
                aspect: this.aspect ? this.aspect : null
            };
        }
        console.log('this.managegraph',this.managegraph);
        this._service.Getmanagecontrolreport(this.managegraph).subscribe(data => {
            console.log(data);
            if (data['status'] === 'success') {
                const x = data['data'];
                const y = x['transaction'];
                const z = x['compensation'];
                const namegraph = [];
                let yeargraph = [];
                const yeargraphs = [];
                const serie_name2 = [];
                this.valueserie = [];
                this.valueport = [];
                this.onSearch2(z);
                for (let i = 0; i < y.length; i++) {
                    const c = [];
                    const a = [];
                    const f = [];
                    if (y[i]['categoryname'] && y[i]['categoryname'].length) {
                        this.hide = false;
                        // serie_name[i] = (y[i]['categoryname'][0]['seriesname'][0]['name']);
                        // serie_name3[i] = (y[i]['categoryname'][0]['seriesname'][0]['name']);
                        // x.length = จำนวนของกราฟ
                        namegraph[i] = y[i]['name'];
                        if (y[i]['categoryname'] && y[i]['categoryname'].length) {
                            for (let w = 0; w < y[i]['categoryname'].length; w++) {
                                const b = [];
                                const n = [];
                                const result = y[i]['categoryname'][w]['name'];
                                yeargraph.push(result);
                                for (let e = 0; e < y[i]['categoryname'][w]['seriesname'].length; e++) {
                                    if (y[i]['categoryname'][w]['seriesname'][0]['requestId'] === y[i]['categoryname'][w]['seriesname'][e]['requestId']) {
                                        const row2 = y[i]['categoryname'][w]['seriesname'][e]['seriesvalues'];
                                        const row = y[i]['categoryname'][w]['seriesname'][e]['name'];
                                        b.push(row2);
                                        n.push(row);
                                    } else {
                                        y[i]['categoryname'][w]['seriesname'][0]['requestId'] = y[i]['categoryname'][w]['seriesname'][e]['requestId'];
                                        const row2 = y[i]['categoryname'][w]['seriesname'][e]['seriesvalues'];
                                        const row = y[i]['categoryname'][w]['seriesname'][e]['name'];
                                        b.push(row2);
                                        n.push(row);
                                    }
                                }
                                // console.log(y[i]['categoryname'][w]['seriesname'].length);
                                a.push(b);
                                f.push(n);
                                // }
                            }
                        }
                        // if (y[i]['categoryname'] && y[i]['categoryname'].length) {
                        c.push(a);
                        serie_name2.push(f);
                        // c.push(b);
                        // console.log(c);
                        this.valueserie.push(c);
                        for (let v = 0; v < this.valueserie[i][0][0].length; v++) {
                            let row = [];
                            const row2 = [];
                            for (let b = 0; b < this.valueserie[i][0].length; b++) {
                                row = this.valueserie[i][0][b][v];
                                row2.push(row);
                            }
                            this.valueport.push(row2);
                        }
                        console.log(this.valueport);

                        // }
                        if (y[i]['categoryname'] && y[i]['categoryname'].length) {
                            yeargraphs.push(yeargraph);
                            yeargraph = [];
                        }
                        if (y[i]['categoryname'] && y[i]['categoryname'].length) {
                            const chart = new Highcharts.Chart({
                                chart: {
                                    renderTo: '0' + i.toString(),
                                    type: 'column'
                                },
                                colors: ['#04A3E3', '#4EBCE9', '#6AC7ED', '#8DD2EE', '#BED9B0', '#F6D37B', '#ECA2A2', '#A2AFEC'],
                                title: {
                                    text: '',
                                    style: {
                                        Height: '30px',
                                        Width: '131px',
                                        color: '#333333',
                                        fontamily: 'Prompt',
                                        fontsize: '20px',
                                        fontweight: '500',
                                        letterspacing: '0',
                                        lineheight: '30px',
                                    },
                                    align: 'left'
                                },
                                xAxis: {
                                    categories: yeargraphs[i],
                                },
                                legend: {
                                    itemStyle: {
                                        Height: '24px',
                                        Width: '83px',
                                        color: '#363636',
                                        fontfamily: 'Kanit',
                                        fontsize: '16px',
                                        letterspacing: '0',
                                        lineheight: '24px',
                                    },
                                    align: 'left'
                                },
                                yAxis: [{
                                    title: {
                                        text: ''
                                    },
                                    opposite: false
                                }],
                                credits: {
                                    enabled: false
                                },
                            });
                            for (let q = 0; q < serie_name2[i][0].length; q++) {
                                chart.addSeries({
                                    name: 'เกิดจริง - ' + serie_name2[i][0][q],
                                    data: this.valueport[q],
                                    type: 'column'
                                });
                                chart.addSeries({
                                    name: 'เป้าหมาย - ' + serie_name2[i][0][q + 1],
                                    data: this.valueport[(++q)],
                                    type: 'spline'
                                });
                            }
                        }
                    } else {
                        serie_name2[i] = null;
                        this.valueport[i] = null;
                        namegraph.splice(i, 1);
                        yeargraphs[i] = null;
                        this.valueserie[i] = null;
                    }
                    this.checkgraph[i] = namegraph[i];
                    this.valueport = [];
                    console.log(serie_name2);
                }
            } else {
                return Swal.fire({
                    title: 'error',
                    text: data['message'],
                    icon: 'error',
                });
            }
        });
    }

    onSearch2(data) {
        const y = data;
        const namegraph = [];
        let yeargraph = [];
        const yeargraphs = [];
        const serie_name2 = [];
        this.valueserie2 = [];
        this.valueport2 = [];
        for (let i = 0; i < y.length; i++) {
            const c = [];
            const a = [];
            const f = [];
            if (y[i]['categoryname'] && y[i]['categoryname'].length) {
                this.hide = false;
                // serie_name[i] = (y[i]['categoryname'][0]['seriesname'][0]['name']);
                // serie_name3[i] = (y[i]['categoryname'][0]['seriesname'][0]['name']);
                // x.length = จำนวนของกราฟ
                namegraph[i] = y[i]['name'];
                if (y[i]['categoryname'] && y[i]['categoryname'].length) {
                    for (let w = 0; w < y[i]['categoryname'].length; w++) {
                        const b = [];
                        const n = [];
                        const result = y[i]['categoryname'][w]['name'];
                        yeargraph.push(result);
                        for (let e = 0; e < y[i]['categoryname'][w]['seriesname'].length; e++) {
                            if (y[i]['categoryname'][w]['seriesname'][0]['requestId'] === y[i]['categoryname'][w]['seriesname'][e]['requestId']) {
                                const row2 = y[i]['categoryname'][w]['seriesname'][e]['seriesvalues'];
                                const row = y[i]['categoryname'][w]['seriesname'][e]['name'];
                                b.push(row2);
                                n.push(row);
                            } else {
                                y[i]['categoryname'][w]['seriesname'][0]['requestId'] = y[i]['categoryname'][w]['seriesname'][e]['requestId'];
                                const row2 = y[i]['categoryname'][w]['seriesname'][e]['seriesvalues'];
                                const row = y[i]['categoryname'][w]['seriesname'][e]['name'];
                                b.push(row2);
                                n.push(row);
                            }
                        }
                        // console.log(y[i]['categoryname'][w]['seriesname'].length);
                        a.push(b);
                        f.push(n);
                        // }
                    }
                }
                // if (y[i]['categoryname'] && y[i]['categoryname'].length) {
                c.push(a);
                serie_name2.push(f);
                // c.push(b);
                // console.log(c);
                this.valueserie2.push(c);
                for (let v = 0; v < this.valueserie2[i][0][0].length; v++) {
                    let row = [];
                    const row2 = [];
                    for (let b = 0; b < this.valueserie2[i][0].length; b++) {
                        row = this.valueserie2[i][0][b][v];
                        row2.push(row);
                    }
                    this.valueport2.push(row2);
                }
                console.log(this.valueport2);

                // }
                if (y[i]['categoryname'] && y[i]['categoryname'].length) {
                    yeargraphs.push(yeargraph);
                    yeargraph = [];
                }
                if (y[i]['categoryname'] && y[i]['categoryname'].length) {
                    const chart = new Highcharts.Chart({
                        chart: {
                            renderTo: '00' + i.toString(),
                            type: 'column'
                        },
                        colors: ['#04A3E3', '#4EBCE9', '#6AC7ED', '#8DD2EE', '#BED9B0', '#F6D37B', '#ECA2A2', '#A2AFEC'],
                        title: {
                            text: '',
                            style: {
                                Height: '30px',
                                Width: '131px',
                                color: '#333333',
                                fontamily: 'Prompt',
                                fontsize: '20px',
                                fontweight: '500',
                                letterspacing: '0',
                                lineheight: '30px',
                            },
                            align: 'left'
                        },
                        xAxis: {
                            categories: yeargraphs[i],
                        },
                        legend: {
                            itemStyle: {
                                Height: '24px',
                                Width: '83px',
                                color: '#363636',
                                fontfamily: 'Kanit',
                                fontsize: '16px',
                                letterspacing: '0',
                                lineheight: '24px',
                            },
                            align: 'left'
                        },
                        yAxis: [{
                            title: {
                                text: ''
                            },
                            opposite: false
                        }],
                        credits: {
                            enabled: false
                        },
                    });
                    for (let q = 0; q < serie_name2[i][0].length; q++) {
                        chart.addSeries({
                            name: 'เกิดจริง - ' + serie_name2[i][0][q],
                            data: this.valueport2[q],
                            type: 'column'
                        });
                        chart.addSeries({
                            name: 'เป้าหมาย - ' + serie_name2[i][0][q + 1],
                            data: this.valueport2[(++q)],
                            type: 'spline'
                        });
                    }
                }
            } else {
                serie_name2[i] = null;
                this.valueport2[i] = null;
                namegraph.splice(i, 1);
                yeargraphs[i] = null;
                this.valueserie2[i] = null;
            }
            this.checkgraph2[i] = namegraph[i];
            console.log(this.checkgraph2);
            this.valueport2 = [];
        }
    }

    // filterSaechWorkgroup(data) {
    //     const arr = this.divisionList_data.filter(ele => {
    //         return ele.ouName === data;
    //     });
    //     const arr_workgroupList = this.workgroupList_data.filter(ele => {
    //         return ele.groupOuCode === arr[0].ouCode;
    //     });
    //     this.workgroupList = arr_workgroupList;
    //     this.workgroup = '';
    //     this.division = '';
    //     this.departmentList = [];
    //     this.dropdownList2 = [];
    //     const par = {
    //         'grp': this.department ? this.department['ouName'] : '',
    //         'sctr': this.workgroup ? this.workgroup['ouName'] : '',
    //         'dept': this.division ? this.division['ouName'] : '',
    //         'announced': true
    //     };
    //     this._service.Getsearchriskcontrolreport(par).subscribe(result => {
    //         if (result['status'] === 'success') {
    //             for (let i = 0; i < result['data']['product'].length; i++) {
    //                 if (result['data']['product'][i]['productName'] === null) {
    //                     result['data']['product'].splice(i, 1);
    //                 }
    //             }
    //             this.selectedItems2 = [];
    //             this.dropdownList2 = result['data']['product'];
    //         }
    //     });
    // }

    // filterSaechDepartment(data) {
    //     const arr = this.workgroupList_data.filter(ele => {
    //         return ele.ouName === data;
    //     });
    //     const arr_Department = this.departmentList_data.filter(ele => {
    //         return ele.sctrOuCode === arr[0].ouCode;
    //     });
    //     this.departmentList = arr_Department;
    //     this.division = '';
    //     this.dropdownList2 = [];
    //     const par = {
    //         'grp': this.department ? this.department['ouName'] : '',
    //         'sctr': this.workgroup ? this.workgroup['ouName'] : '',
    //         'dept': this.division ? this.division['ouName'] : '',
    //         'announced': true
    //     };
    //     this._service.Getsearchriskcontrolreport(par).subscribe(result => {
    //         if (result['status'] === 'success') {
    //             for (let i = 0; i < result['data']['product'].length; i++) {
    //                 if (result['data']['product'][i]['productName'] === null) {
    //                     result['data']['product'].splice(i, 1);
    //                 }
    //             }
    //             this.selectedItems2 = [];
    //             this.dropdownList2 = result['data']['product'];
    //         }
    //     });
    // }

    filterforproductname() {
        // this.division = data;
        this.dropdownList2 = [];
        const par = {
            'grp': this.division ? this.division['ouName'] : '',
            'sctr': this.workgroup ? this.workgroup['ouName'] : '',
            'dept': this.department ? this.department['ouName'] : '',
            'announced': true
        };
        console.log('par',par);
        this._service.Getsearchriskcontrolreport(par).subscribe(result => {
            console.log('result',result)
            if (result['status'] === 'success') {
                for (let i = 0; i < result['data']['product'].length; i++) {
                    if (result['data']['product'][i]['productName'] === null) {
                        result['data']['product'].splice(i, 1);
                    }
                }
                this.selectedItems2 = [];
                this.dropdownList2 = result['data']['product'];
                console.log('this.dropdownList2',this.dropdownList2)
            }
        });
    }

    filterSelection(type, value) {
        // type คือ ฝาย(departmentList) กลุ่ม(workgroupList) สาย(divisionList);
        // value คาที่ต้องนำมาหา
        // ฝ่าย:dept - กลุ่ม:sctr - สาย:grp
        console.log('====== filterSelection =====');
        console.log('filterSelection type', type);
        console.log('filterSelection value', value);
        if (type === 'department' && value !== 'resetFilter') {
          // ================================= department ==================================
          this.fillterStatus = true;
          this.department_ouCode = value.ouCode;
          console.log('001 this.workgroup:', this.workgroup );
          console.log('001 this.division:', this.division );
          this.filterforproductname();
          if ((this.workgroup === null || this.workgroup === undefined || this.workgroup === '')
          && (this.division === null || this.division === undefined || this.division === '') ) {
             // ( department : select / workgroup : null / division : null )
             this.set_list_workgroup('ouCode', value.sctrOuCode);
             this.set_list_division('ouCode', value.groupOuCode);
             this.filterforproductname();
          } else if ((this.workgroup !== null && this.workgroup !== undefined && this.workgroup !== '')
          && (this.division === null || this.division === undefined || this.division === '')) {
            // ( department : select / workgroup : not null / division : null )
            if (this.workgroup['ouCode'] !== value.sctrOuCode) {
              if (this.changResetStatus === true) {
                this.workgroup = '';
              } else {
                this.workgroup = undefined;
              }
              this.set_list_workgroup('ouCode', value.sctrOuCode);
              this.set_list_division('ouCode', value.groupOuCode);
              this.filterforproductname();
            } else {
              this.set_list_division('ouCode_ouCode', this.workgroup['groupOuCode'], value.groupOuCode);
              this.filterforproductname();
            }
            
          } else if ((this.workgroup === null || this.workgroup === undefined || this.workgroup === '')
          && (this.division !== null &&  this.division !== undefined &&  this.division !== '')) {
            // ( department : select / workgroup : null / division : not null )
            if (this.division['ouCode'] !== value.groupOuCode) {
              if (this.changResetStatus === true) {
                this.division = '';
              } else {
                this.division = undefined;
              }
              this.set_list_workgroup('ouCode', value.sctrOuCode);
              this.set_list_division('ouCode', value.groupOuCode);
              this.filterforproductname();
            } else {
              this.set_list_workgroup('ouCode_groupOuCode', value.sctrOuCode, this.division['ouCode']);
              this.filterforproductname();
            }
          } else if ((this.workgroup !== null && this.workgroup !== undefined && this.workgroup !== '')
          && (this.division !== null &&  this.division !== undefined &&  this.division !== '')) {
            this.filterforproductname();
            // ( department : select / workgroup : not null / division : not null )
          }
          // this.sendSearchAdvanced();
        } else if (type === 'workgroup' && value !== 'resetFilter') {
          // ================================= workgroup ==================================
          this.fillterStatus = true;
          this.workgroup_ouCode = value.ouCode;
          if ((this.department === null || this.department === undefined || this.department === '')
          && (this.division === null || this.division === undefined || this.division === '')) {
            // ( department : null  / workgroup : select / division : null )
            this.set_list_department('sctrOuCode', value.ouCode);
            this.set_list_division('ouCode', value.groupOuCode);
            this.filterforproductname();
          } else if ((this.department !== null && this.department !== undefined && this.department !== '')
          && (this.division === null || this.division === undefined || this.division === '') ) {
            // ( department : not null  / workgroup : select / division : null )
            if (this.department['sctrOuCode'] !== value.ouCode) {
              if (this.changResetStatus === true) {
                this.department = '';
              } else {
                this.department = undefined;
              }
              this.set_list_department('sctrOuCode', value.ouCode);
              this.set_list_division('ouCode', value.groupOuCode);
              this.filterforproductname();
            } else {
              this.set_list_division('ouCode_ouCode', this.department['groupOuCode'], value.groupOuCode);
              this.filterforproductname();
            }
          } else if ((this.department === null || this.department === undefined || this.department === '')
          && (this.division !== null && this.division !== undefined && this.division !== '')) {
            // ( department : null / workgroup : select / division : not null )
            if (this.division['ouCode'] !== value.groupOuCode) {
              if (this.changResetStatus === true) {
                this.division = '';
              } else {
                this.division = undefined;
              }
              this.set_list_department('sctrOuCode', value.ouCode);
              this.set_list_division('ouCode', value.groupOuCode);
              this.filterforproductname();
            } else {
              this.set_list_department('sctrOuCode_groupOuCode', value.ouCode, this.division['ouCode']);
              this.filterforproductname();
            }
          } else if ((this.department !== null && this.department !== undefined && this.department !== '')
          && (this.division !== null && this.division !== undefined && this.division !== '')) {
            this.filterforproductname();
            // ( department : not null / workgroup : select / division : not null )
          }
          // this.sendSearchAdvanced();
        }  else if (type === 'division' && value !== 'resetFilter') {
          // ================================= division ==================================
          this.fillterStatus = true;
          this.department_ouCode = value.ouCode;
          if ((this.department === null || this.department === undefined || this.department === '')
          && (this.workgroup === null || this.workgroup === undefined || this.workgroup === '')) {
            // ( department : null / workgroup : null / division : select)
            this.set_list_department('groupOuCode', value.ouCode);
            this.set_list_workgroup('groupOuCode', value.ouCode);
            this.filterforproductname();
          } else if ((this.department !== null && this.department !== undefined && this.department !== '')
          && (this.workgroup === null || this.workgroup === undefined || this.workgroup === '')) {
            // ( department : not null / workgroup : null / division : select)
            if (this.department['groupOuCode'] !== value.ouCode) {
              if (this.changResetStatus === true) {
                this.department = '';
              } else {
                this.department = undefined;
              }
              this.set_list_department('groupOuCode', value.ouCode);
              this.set_list_workgroup('groupOuCode', value.ouCode);
              this.filterforproductname();
            } else {
              this.set_list_workgroup('groupOuCode', value.ouCode);
              this.filterforproductname();
            }
          } else if ((this.department === null || this.department === undefined || this.department === '')
          && (this.workgroup !== null && this.workgroup !== undefined && this.workgroup !== '')) {
             // ( department : null / workgroup : not null / division : select)
             if (this.workgroup['groupOuCode'] !== value.ouCode) {
              if (this.changResetStatus === true) {
                this.workgroup = '';
              } else {
                this.workgroup = undefined;
              }
              this.set_list_workgroup('groupOuCode', value.ouCode);
              this.set_list_department('groupOuCode', value.ouCode);
              this.filterforproductname();
            } else {
              this.set_list_department('sctrOuCode_groupOuCode', this.workgroup['ouCode'], value.ouCode );
              this.filterforproductname();
            }
          } else if ((this.department !== null && this.department !== undefined && this.department !== '')
          && (this.workgroup !== null && this.workgroup !== undefined && this.workgroup !== '')) {
            // ( department : not null / workgroup : not null / division : select)
            if (this.department['groupOuCode'] !== value.ouCode) {
              if (this.changResetStatus === true) {
                this.department = '';
              } else {
                this.department = undefined;
              }
              this.set_list_department('groupOuCode', value.ouCode);
              this.filterforproductname();
            }
            if (this.workgroup['groupOuCode'] !== value.ouCode) {
              if (this.changResetStatus === true) {
                this.workgroup = '';
              } else {
                this.workgroup = undefined;
              }
              this.set_list_workgroup('groupOuCode', value.ouCode);
              this.filterforproductname();
            }
          }
          // this.sendSearchAdvanced();
        } else if ( (type === 'department' || type === 'workgroup' || type === 'division') && value === 'resetFilter') {
    
          if (this.changResetStatus === true) {
            console.log('00000-1 this.changResetStatus', this.changResetStatus);
            this.department = undefined;
            this.workgroup = undefined;
            this.division = undefined;
            this.fillterStatus = false;
            this.changResetStatus = !this.changResetStatus;
            this.filterforproductname();
          } else {
            console.log('00000-2 this.changResetStatus', this.changResetStatus);
            this.department = '';
            this.workgroup = '';
            this.division = '';
            this.fillterStatus = false;
            this.changResetStatus = !this.changResetStatus;
            this.filterforproductname();
          }
          // this.sendSearchAdvanced();
          this.departmentList_fill = JSON.parse(JSON.stringify(this.departmentList));
          this.workgroupList_fill = JSON.parse(JSON.stringify(this.workgroupList));
          this.divisionList_fill = JSON.parse(JSON.stringify(this.divisionList));
        }
        // this.departmentList_fill = [];
        // this.workgroupList_fill = [];
        // this.divisionList_fill = [];
        console.log('====== filterSelection end =====');
      }
    
      set_list_workgroup(search_at, search_ouCode1, search_ouCode2?) {
        if (search_at === 'ouCode') {
          this.workgroupList_fill = this.workgroupList.filter( (ele) => {
            return ele.ouCode === search_ouCode1;
          });
          if (this.workgroupList_fill.length === 0) {
            this.workgroupList_fill = 'nondata';
            // this.workgroup = null;
          }
        } else if (search_at === 'groupOuCode') {
          this.workgroupList_fill = this.workgroupList.filter( (ele) => {
            return ele.groupOuCode === search_ouCode1 ;
          });
          if (this.workgroupList_fill.length === 0) {
            this.workgroupList_fill = 'nondata';
            // this.workgroup = null;
          }
        } else if (search_at === 'ouCode_groupOuCode') {
          this.workgroupList_fill = this.workgroupList.filter( (ele) => {
            return ele.ouCode === search_ouCode1 && ele.groupOuCode === search_ouCode2;
          });
          if (this.workgroupList_fill.length === 0) {
            this.workgroupList_fill = 'nondata';
            // this.workgroup = null;
          }
        }
      }
    
      set_list_division(search_at, search_ouCode1, search_ouCode2?) {
        if (search_at === 'ouCode') {
          this.divisionList_fill = this.divisionList.filter( (ele) => {
            return ele.ouCode === search_ouCode1;
          });
          if (this.divisionList_fill.length === 0) {
            this.divisionList_fill = 'nondata';
            // this.division = null;
          }
        } else if (search_at === 'ouCode_ouCode') {
          this.divisionList_fill = this.divisionList.filter( (ele) => {
            return ele.ouCode === search_ouCode1 && ele.ouCode === search_ouCode2;
          });
          if (this.divisionList_fill.length === 0) {
            this.divisionList_fill = 'nondata';
            // this.division = null;
          }
        }
      }
    
      set_list_department(search_at, search_ouCode1, search_ouCode2?) {
        if (search_at === 'sctrOuCode') {
          this.departmentList_fill = this.departmentList.filter( (ele) => {
            return ele.sctrOuCode === search_ouCode1;
          });
          if (this.departmentList_fill.length === 0) {
            this.departmentList_fill = 'nondata';
            // this.department = null;
          }
        } else if (search_at === 'groupOuCode') {
          this.departmentList_fill = this.departmentList.filter( (ele) => {
            return ele.groupOuCode === search_ouCode1 ;
          });
          if (this.departmentList_fill.length === 0) {
            this.departmentList_fill = 'nondata';
            // this.department = null;
          }
        } else if (search_at === 'sctrOuCode_groupOuCode') {
          this.departmentList_fill = this.departmentList.filter( (ele) => {
            return ele.sctrOuCode === search_ouCode1 && ele.groupOuCode === search_ouCode2;
          });
          if (this.departmentList_fill.length === 0) {
            this.departmentList_fill = 'nondata';
            // this.department = null;
          }
        }
      }
}
