export class Labelgrahp {
    idindex: number;
    labelName: string;
}

export class Labelgrahp2 {
    idindex: number;
    graphName: string;
}

export class Datagrahp {
    values: number[];
}

export class Datamocup {
    labelName: string;
    graphName: string[];
    values: Datagrahp[];
    types: string[];
}

export class ManagementReports {
    id: number;
    name: string;
    category: string;
    categoryname: Categorys[];
}

export class Categorys {
    id: number;
    name: string;
    seriesname: Seriesname[];
}

export class ManagementReports2 {
    id: number;
    name: string;
    seriesname: Seriesname[];
    category: string;
}

export class Seriesname {
    id: number;
    name: string;
    graphid: number;
    seriesvalues: Seriesvalues[];
}

export class Seriesname2 {
    id: number;
    name: string;
    graphid: number;
    seriesvalues: Seriesvalues2[];
}

export class Seriesvalues {
    id: number;
    values: number;
    nameid: number;
    graphid: number;
    typegraph: string;
}

export class Seriesvalues2 {
    id: number;
    valures: number;
    nameid: number;
    graphid: number;
    typegraph: string;
}
