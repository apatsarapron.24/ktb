import {Injectable} from '@angular/core';
import {HttpHeaders, HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable, of} from 'rxjs';
import {delay, map} from 'rxjs/operators';

export interface Person {
    name: string;
    category: string;
    disabled: boolean;
}

@Injectable({
    providedIn: 'root'
})


export class ReportsTabService {

    api_Url = environment.apiUrl;
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
        })
    };

    constructor(private http: HttpClient) {
    }

    Getcontrolreport(data: any) {
        return this.http.post(this.api_Url + '/api/pps/product/report/controlReport',
            data,
            this.httpOptions
        );
    }

    GetcontrolreportSLA(data: any) {
        return this.http.post(this.api_Url + '/api/pps/product/report/controlReportSLA',
            data,
            this.httpOptions
        );
    }

    GetControlReportDetail(data: any) {
        return this.http.post(this.api_Url + '/api/pps/product/report/controlReportDetail',
            data,
            this.httpOptions
        );
    }

    Getriskcontrolreport(data: any) {
        return this.http.post(this.api_Url + '/api/pps/product/report/riskControlReport',
            data,
            this.httpOptions
        );
    }
    Getmanagecontrolreport(data: any) {
        return this.http.post(this.api_Url + '/api/pps/product/report/managementReport',
            data,
            this.httpOptions
        );
    }
    Getsearchriskcontrolreport(data: any) {
        return this.http.post(this.api_Url + '/api/pps/product/report/searchRiskControlReport',
            data,
            this.httpOptions
        );
    }
    getPeople(term: string = null): Observable<Person[]> {
        let items = getMockPeople();
        if (term) {
            items = items.filter(x => x.category.toLocaleLowerCase().indexOf(term.toLocaleLowerCase()) > -1);
        }
        return of(items).pipe(delay(500));
    }
}

function getMockPeople() {
    return [
        {
            'index': 1,
            'category': 'วงเงินโครงการ',
            'name': 'ปริมาณธุรกรรม',
            'disabled': false
        },
        {
            'index': 2,
            'category': 'Limit',
            'name': 'ปริมาณธุรกรรม',
            'disabled': false
        },
        {
            'index': 3,
            'category': 'Outstanding',
            'name': 'ปริมาณธุรกรรม',
            'disabled': false
        },
        {
            'index': 4,
            'category': 'A/R Turnover',
            'name': 'ปริมาณธุรกรรม',
            'disabled': false
        },
        {
            'index': 5,
            'category': 'Advance Payment',
            'name': 'ปริมาณธุรกรรม',
            'disabled': false
        },
        {
            'index': 6,
            'category': 'Utilization/Turnover',
            'name': 'ปริมาณธุรกรรม',
            'disabled': false
        },
        {
            'index': 7,
            'category': 'จำนวนลูกค้า',
            'name': 'ปริมาณธุรกรรม',
            'disabled': false
        },
        {
            'index': 8,
            'category': 'NII',
            'name': 'ผลการตอบแทน',
            'disabled': false
        },
        {
            'index': 9,
            'category': 'Non-NII',
            'name': 'ผลการตอบแทน',
            'disabled': false
        },
        {
            'index': 10,
            'category': 'RAROC',
            'name': 'ผลการตอบแทน',
            'disabled': false
        },
        {
            'index': 11,
            'category': 'ROI',
            'name': 'ผลการตอบแทน',
            'disabled': false
        },
        {
            'index': 12,
            'category': 'EP',
            'name': 'ผลการตอบแทน',
            'disabled': false
        }
    ];
}

