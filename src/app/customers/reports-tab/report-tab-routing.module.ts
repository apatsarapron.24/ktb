import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ReportsTabComponent} from './reports-tab.component';

const routes: Routes = [
    {
        path: '',
        component: ReportsTabComponent,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ReportTabRoutingModule {
}
