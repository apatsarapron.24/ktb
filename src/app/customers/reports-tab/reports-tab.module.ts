import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {ControlReportComponent} from './control-report/control-report.component';
import {ReportTabRoutingModule} from './report-tab-routing.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatSelectModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ReportsTabComponent} from './reports-tab.component';
import {AngularMyDatePickerModule} from 'angular-mydatepicker';
import {ManagementReportComponent} from './management-report/management-report.component';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';
import {ProductTurnaroundComponent} from './product-turnaround/product-turnaround.component';
import {RiskControlReportComponent} from './risk-control-report/risk-control-report.component';
import {MasterFileReportComponent} from './master-file-report/master-file-report.component';
import {DateRangePickerModule} from '@syncfusion/ej2-angular-calendars';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';
import {ControlReportDialogComponent} from './control-report/control-report-dialog/control-report-dialog.component';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import {RequestModule} from '../request/request.module';
import {NgSelectModule} from '@ng-select/ng-select';
import {NgOptionHighlightModule} from '@ng-select/ng-option-highlight';

@NgModule({
    declarations: [ControlReportComponent, ReportsTabComponent, ManagementReportComponent, ProductTurnaroundComponent, RiskControlReportComponent, MasterFileReportComponent, ControlReportDialogComponent],
    imports: [
        CommonModule,
        ReportTabRoutingModule,
        FlexLayoutModule,
        MatSelectModule,
        ReactiveFormsModule,
        AngularMyDatePickerModule,
        NgMultiSelectDropDownModule.forRoot(),
        FormsModule,
        DateRangePickerModule,
        MatNativeDateModule,
        MatDatepickerModule,
        TooltipModule.forRoot(),
        NgSelectModule,
        NgOptionHighlightModule,
        RequestModule
    ],
    entryComponents: [ControlReportDialogComponent],
    providers: [
        DatePipe,
        MatDatepickerModule,
        MatNativeDateModule,
    ]
})
export class ReportsTabModule {
}

