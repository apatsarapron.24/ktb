import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterFileReportComponent } from './master-file-report.component';

describe('MasterFileReportComponent', () => {
  let component: MasterFileReportComponent;
  let fixture: ComponentFixture<MasterFileReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterFileReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterFileReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
