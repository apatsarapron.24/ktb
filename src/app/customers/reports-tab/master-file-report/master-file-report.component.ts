import { Component, OnInit } from '@angular/core';
import { MasterFileReportService } from '../../../services/master-file-report/master-file-report.service';

@Component({
  selector: 'app-master-file-report',
  templateUrl: './master-file-report.component.html',
  styleUrls: ['./master-file-report.component.scss']
})
export class MasterFileReportComponent implements OnInit {

  requestid: any;

  // แสดงผลน้องลง
  showsearch = true;
  showdata = false;
  showproduct = false;
  showoutstanding = false;
  showproductperformance = false;
  showbusinessloan = false;
  showpersonalloan = false;
  showhousingloan = false;
  showothercredit = false;
  shownoncredit = false;
  showothernoncredit = false;
  showInformation = false;
  showImprove = false;
  showIssue = false;
  showPerformance = false;
  // dropdown
  product = '';
  department: any;
  workgroup: any;
  division: any;
  productList: any;
  departmentList: any;
  workgroupList: any;
  divisionList: any;
  divisionList_data: any;
  workgroupList_data: any;
  departmentList_data: any;

  // ข้อมูลเบื้องต้น
  defaultdata: any;
  productName = '';
  productCode = '';
  productGroup = '';
  productType = '';
  lastReviewDate = '';
  nextReviewDate = '';
  manager = '';
  managerTel = '';
  promulgateDate = '';
  circularNo = '';
  circularDate = '';

  // ปรับปรุงผลิตภัณฑ์
  datahistoryproduct: any = [];

  // issue
  dataissue: any = [];

  // product performance
  dataproductperformance: any = [];
  years: any = [];
  headers = ['ที่เกิดขึ้นจริง ปี '];
  subheader = ['Q1', '%', 'Q2', '%', 'Q3', '%', 'Q4', '%'];
  transaction: any = [];
  compensation: any = [];
  control: any = [];

  // product performnce triger
  datatriger: any = [];

  // product get group
  businessStatus = false;
  personalStatus = false;
  housingStatus = false;
  otherStatus = false;
  nonCreditStatus = false;
  otherNonCreditStatus = false;

  // เอกสารแนบ
  showfile: any;

  // new filter
  divisionList_fill: any;
  workgroupList_fill: any;
  departmentList_fill: any;
  fillterStatus = false;
  changResetStatus = true;
  fromSend_department = '';
  fromSend_workgroup = '';
  fromSend_division = '';
  department_ouCode = '';
  workgroup_ouCode = '';
  division_ouCode = '';

  constructor(
    private _Service: MasterFileReportService,
  ) { }

  ngOnInit() {
    this.getSearchRiskControlReport();
    // this._Service.divisionList().subscribe(res => {
    //   this.divisionList = res['data'];
    //   this.divisionList_data = res['data'];
    // });
    // this._Service.workgroupList().subscribe(res => {
    //   this.workgroupList = res['data'];
    //   this.workgroupList_data = res['data'];
    // });
    // this._Service.departmentList().subscribe(res => {
    //   this.departmentList = res['data'];
    //   this.departmentList_data = res['data'];
    // });

    this._Service.divisionList().subscribe((res) => {
      this.divisionList = res['data'];
      this.divisionList_fill = JSON.parse(JSON.stringify(res['data']));
    });
    this._Service.workgroupList().subscribe((res) => {
      this.workgroupList = res['data'];
      this.workgroupList_fill = JSON.parse(JSON.stringify(res['data']));
    });
    this._Service.departmentList().subscribe((res) => {
      this.departmentList = res['data'];
      this.departmentList_fill = JSON.parse(JSON.stringify(res['data']));
    });
  }

  getSearchRiskControlReport() {
    let _data: any;
    const data = {
      'grp': null,
      'sctr': null,
      'dept': null
    };

    this._Service.getSearchRiskControlReport(data).subscribe(res => {
      if (res['status'] === 'success') {
        _data = res['data'];
        this.productList = _data['product'];
      }
    });
  }

  getSearchProductReport() {
    let _data: any;
    const data = {
      grp: this.division ? this.division['ouName'] : '',
      sctr: this.workgroup ? this.workgroup['ouName'] : '',
      dept: this.department ? this.department['ouName'] : '',
    };

    this._Service.getSearchRiskControlReport(data).subscribe(res => {
      if (res['status'] === 'success') {
        _data = res['data'];
        this.productList = _data['product'];
      } else {
        this.productList = null;
      }
    });
  }

  selectProduct() {
    console.log('product', this.product);
    this.requestid = this.product;
    localStorage.setItem('requestId', this.requestid);
  }

  showSearch() {
    if (this.showsearch === true) {
      this.showsearch = false;
    } else {
      this.showsearch = true;
    }
  }
  showData() {
    if (this.showdata === true) {
      this.showdata = false;
    } else {
      this.showdata = true;
    }
  }
  showProduct() {
    if (this.showproduct === true) {
      this.showproduct = false;
    } else {
      this.showproduct = true;
    }
  }
  showOutstanding() {
    if (this.showoutstanding === true) {
      this.showoutstanding = false;
    } else {
      this.showoutstanding = true;
    }
  }
  showProductPerformance() {
    if (this.showproductperformance === true) {
      this.showproductperformance = false;
    } else {
      this.showproductperformance = true;
    }
  }
  showBusinessLoan() {
    if (this.showbusinessloan === true) {
      this.showbusinessloan = false;
    } else {
      this.showbusinessloan = true;
    }
  }
  showPersonalLoan() {
    if (this.showpersonalloan === true) {
      this.showpersonalloan = false;
    } else {
      this.showpersonalloan = true;
    }
  }
  showHousingLoan() {
    if (this.showhousingloan === true) {
      this.showhousingloan = false;
    } else {
      this.showhousingloan = true;
    }
  }
  showOtherCredit() {
    if (this.showothercredit === true) {
      this.showothercredit = false;
    } else {
      this.showothercredit = true;
    }
  }
  showNonCredit() {
    if (this.shownoncredit === true) {
      this.shownoncredit = false;
    } else {
      this.shownoncredit = true;
    }
  }
  showOtherNonCredit() {
    if (this.showothernoncredit === true) {
      this.showothernoncredit = false;
    } else {
      this.showothernoncredit = true;
    }
  }

  // filter
  // filterSaechWorkgroup(data) {
  //   console.log('department', data);
  //   const arr = this.divisionList_data.filter(ele => {
  //     return ele.ouName === data;
  //   });
  //   const arr_workgroupList = this.workgroupList_data.filter(ele => {
  //     return ele.groupOuCode === arr[0].ouCode;
  //   });
  //   this.workgroupList = arr_workgroupList;
  //   this.workgroup = '';
  //   this.division = '';
  //   this.departmentList = [];
  //   this.getSearchProductReport();
  // }

  // filterSaechDepartment(data) {
  //   console.log('workgroup', data);
  //   const arr = this.workgroupList_data.filter(ele => {
  //     return ele.ouName === data;
  //   });
  //   const arr_Department = this.departmentList_data.filter(ele => {
  //     return ele.sctrOuCode === arr[0].ouCode;
  //   });
  //   this.departmentList = arr_Department;
  //   this.division = '';
  //   this.getSearchProductReport();
  // }

  search() {
    this.getDefaultData();
    this.getHistoryProduct();
    this.getIssueMasterFileReport();
    this.getProductPerformance();
    this.getPerfomanceTriger();
    this.productGetGroup();
  }

  // ----------------------------- ข้อมูลเบื้องต้น-----------------------------
  getDefaultData() {
    this._Service.getDefaultData(this.requestid).subscribe(res => {
      if (res['data']) {
        this.defaultdata = res['data'];
        this.productName = this.defaultdata.productName;
        this.productCode = this.defaultdata.productCode;
        this.productGroup = this.defaultdata.productGroup;
        this.productType = this.defaultdata.productType;
        this.lastReviewDate = this.defaultdata.lastReviewDate;
        this.nextReviewDate = this.defaultdata.nextReviewDate;
        this.manager = this.defaultdata.manager;
        this.managerTel = this.defaultdata.managerTel;
        this.promulgateDate = this.defaultdata.promulgateDate;
        this.circularNo = this.defaultdata.circularNo;
        this.circularDate = this.defaultdata.circularDate;
        this.showInformation = true;
        this.showImprove = true;
        this.showIssue = true;
        this.showPerformance = true;
        this.showdata = true;
      }
    });
  }
  // ----------------------------- End ข้อมูลเบื้องต้น-----------------------------

  // ----------------------------- ประวัติการปรับปรุงผลิตภัณฑ์ ---------------
  getHistoryProduct() {
    this._Service.getHistoryProduct(this.requestid).subscribe(res => {
      if (res['data']) {
        this.datahistoryproduct = res['data'];
        this.showproduct = true;
      }
    });
  }
  // ----------------------------- End ประวัติการปรับปรุงผลิตภัณฑ์ ---------------


  // ----------------------------- ประเด็นคงค้าง / สิ่งที่ต้องดำเนินการ ---------------
  getIssueMasterFileReport() {
    this._Service.getIssueMasterFileReport(this.requestid).subscribe(res => {
      if (res['data']) {
        this.dataissue = res['data'];
        this.showoutstanding = true;
      }
    });
  }
  // ----------------------------- End ประเด็นคงค้าง / สิ่งที่ต้องดำเนินการ ---------------


  // ------------------------------- product performance -------------------------------------
  getProductPerformance() {
    this._Service.getProductPerformance(this.requestid).subscribe(res => {
      if (res['data']) {
        this.dataproductperformance = res['data'];
        this.years = this.dataproductperformance.performance.year;
        this.transaction = this.dataproductperformance.performance.transaction;
        this.compensation = this.dataproductperformance.performance.compensation;
        this.control = this.dataproductperformance.performance.control;
        this.showproductperformance = true;
      }
    });
  }

  getPerfomanceTriger() {
    this._Service.getPerfomanceTriger(this.requestid).subscribe(res => {
      if (res['data']) {
        // console.log('res', res);
        this.datatriger = res['data'];
      }
    });
  }
  // ------------------------------- End product performance -------------------------------------

  productGetGroup() {
    if (localStorage) {
      this._Service.productGetGroup(localStorage.getItem('requestId')).subscribe(res => {
        console.log('get group: ', res);
        this.businessStatus = false;
        this.personalStatus = false;
        this.housingStatus = false;
        this.otherStatus = false;
        this.nonCreditStatus = false;
        this.otherNonCreditStatus = false;
        const productGet = res['data'];
        console.log('productGet =>', productGet);
        productGet.product.forEach(ele => {
          if (ele === 'business_loan') {
            this.businessStatus = true;
            this.showbusinessloan = true;
          }
          if (ele === 'persenal_loan') {
            this.personalStatus = true;
            this.showpersonalloan = true;
          }
          if (ele === 'housing_loan') {
            this.housingStatus = true;
            this.showhousingloan = true;
          }
          if (ele === 'other') {
            this.otherStatus = true;
            this.showothercredit = true;
          }
          if (ele === 'non_credit_product') {
            this.nonCreditStatus = true;
            this.shownoncredit = true;
          }
          if (ele === 'other_non_creadit') {
            this.otherNonCreditStatus = true;
            this.showothernoncredit = true;
          }

        });
      });
    }
  }

  filterSelection(type, value) {
    // type คือ ฝาย(departmentList) กลุ่ม(workgroupList) สาย(divisionList);
    // value คาที่ต้องนำมาหา
    // ฝ่าย:dept - กลุ่ม:sctr - สาย:grp
    // console.log('====== filterSelection =====');
    console.log('filterSelection type', type);
    console.log('filterSelection value', value);
    if (type === 'department' && value !== 'resetFilter') {

      // ================================= department ==================================
      this.fillterStatus = true;
      this.department_ouCode = value.ouCode;
      console.log('001 this.workgroup:', this.workgroup);
      console.log('001 this.division:', this.division);
      if ((this.workgroup === null || this.workgroup === undefined || this.workgroup === '')
        && (this.division === null || this.division === undefined || this.division === '')) {
        // ( department : select / workgroup : null / division : null )
        this.set_list_workgroup('ouCode', value.sctrOuCode);
        this.set_list_division('ouCode', value.groupOuCode);
        this.getSearchProductReport();
      } else if ((this.workgroup !== null && this.workgroup !== undefined && this.workgroup !== '')
        && (this.division === null || this.division === undefined || this.division === '')) {
        // ( department : select / workgroup : not null / division : null )
        if (this.workgroup['ouCode'] !== value.sctrOuCode) {
          if (this.changResetStatus === true) {
            this.workgroup = '';
          } else {
            this.workgroup = undefined;
          }
          this.set_list_workgroup('ouCode', value.sctrOuCode);
          this.set_list_division('ouCode', value.groupOuCode);
          this.getSearchProductReport();
        } else {
          this.set_list_division('ouCode_ouCode', this.workgroup['groupOuCode'], value.groupOuCode);
          this.getSearchProductReport();
        }
      } else if ((this.workgroup === null || this.workgroup === undefined || this.workgroup === '')
        && (this.division !== null && this.division !== undefined && this.division !== '')) {
        // ( department : select / workgroup : null / division : not null )
        if (this.division['ouCode'] !== value.groupOuCode) {
          if (this.changResetStatus === true) {
            this.division = '';
          } else {
            this.division = undefined;
          }
          this.set_list_workgroup('ouCode', value.sctrOuCode);
          this.set_list_division('ouCode', value.groupOuCode);
          this.getSearchProductReport();
        } else {
          this.set_list_workgroup('ouCode_groupOuCode', value.sctrOuCode, this.division['ouCode']);
          this.getSearchProductReport();
        }
      } else if ((this.workgroup !== null && this.workgroup !== undefined && this.workgroup !== '')
        && (this.division !== null && this.division !== undefined && this.division !== '')) {
        // ( department : select / workgroup : not null / division : not null )
        this.getSearchProductReport();
      }
    } else if (type === 'workgroup' && value !== 'resetFilter') {

      // ================================= workgroup ==================================
      this.fillterStatus = true;
      this.workgroup_ouCode = value.ouCode;
      if ((this.department === null || this.department === undefined || this.department === '')
        && (this.division === null || this.division === undefined || this.division === '')) {
        // ( department : null  / workgroup : select / division : null )
        this.set_list_department('sctrOuCode', value.ouCode);
        this.set_list_division('ouCode', value.groupOuCode);
        this.getSearchProductReport();
      } else if ((this.department !== null && this.department !== undefined && this.department !== '')
        && (this.division === null || this.division === undefined || this.division === '')) {
        // ( department : not null  / workgroup : select / division : null )
        if (this.department['sctrOuCode'] !== value.ouCode) {
          if (this.changResetStatus === true) {
            this.department = '';
          } else {
            this.department = undefined;
          }
          this.set_list_department('sctrOuCode', value.ouCode);
          this.set_list_division('ouCode', value.groupOuCode);
          this.getSearchProductReport();
        } else {
          this.set_list_division('ouCode_ouCode', this.department['groupOuCode'], value.groupOuCode);
          this.getSearchProductReport();
        }
      } else if ((this.department === null || this.department === undefined || this.department === '')
        && (this.division !== null && this.division !== undefined && this.division !== '')) {
        // ( department : null / workgroup : select / division : not null )
        if (this.division['ouCode'] !== value.groupOuCode) {
          if (this.changResetStatus === true) {
            this.division = '';
          } else {
            this.division = undefined;
          }
          this.set_list_department('sctrOuCode', value.ouCode);
          this.set_list_division('ouCode', value.groupOuCode);
          this.getSearchProductReport();
        } else {
          this.set_list_department('sctrOuCode_groupOuCode', value.ouCode, this.division['ouCode']);
          this.getSearchProductReport();
        }
      } else if ((this.department !== null && this.department !== undefined && this.department !== '')
        && (this.division !== null && this.division !== undefined && this.division !== '')) {
        // ( department : not null / workgroup : select / division : not null )
        this.getSearchProductReport();
      }
    } else if (type === 'division' && value !== 'resetFilter') {

      // ================================= division ==================================
      this.fillterStatus = true;
      this.department_ouCode = value.ouCode;
      if ((this.department === null || this.department === undefined || this.department === '')
        && (this.workgroup === null || this.workgroup === undefined || this.workgroup === '')) {
        // ( department : null / workgroup : null / division : select)
        this.set_list_department('groupOuCode', value.ouCode);
        this.set_list_workgroup('groupOuCode', value.ouCode);
        this.getSearchProductReport();
      } else if ((this.department !== null && this.department !== undefined && this.department !== '')
        && (this.workgroup === null || this.workgroup === undefined || this.workgroup === '')) {
        // ( department : not null / workgroup : null / division : select)
        if (this.department['groupOuCode'] !== value.ouCode) {
          if (this.changResetStatus === true) {
            this.department = '';
          } else {
            this.department = undefined;
          }
          this.set_list_department('groupOuCode', value.ouCode);
          this.set_list_workgroup('groupOuCode', value.ouCode);
          this.getSearchProductReport();
        } else {
          this.set_list_workgroup('groupOuCode', value.ouCode);
          this.getSearchProductReport();
        }
      } else if ((this.department === null || this.department === undefined || this.department === '')
        && (this.workgroup !== null && this.workgroup !== undefined && this.workgroup !== '')) {
        // ( department : null / workgroup : not null / division : select)
        if (this.workgroup['groupOuCode'] !== value.ouCode) {
          if (this.changResetStatus === true) {
            this.workgroup = '';
          } else {
            this.workgroup = undefined;
          }
          this.set_list_workgroup('groupOuCode', value.ouCode);
          this.set_list_department('groupOuCode', value.ouCode);
          this.getSearchProductReport();
        } else {
          this.set_list_department('sctrOuCode_groupOuCode', this.workgroup['ouCode'], value.ouCode);
          this.getSearchProductReport();
        }
      } else if ((this.department !== null && this.department !== undefined && this.department !== '')
        && (this.workgroup !== null && this.workgroup !== undefined && this.workgroup !== '')) {
        // ( department : not null / workgroup : not null / division : select)
        if (this.department['groupOuCode'] !== value.ouCode) {
          if (this.changResetStatus === true) {
            this.department = '';
          } else {
            this.department = undefined;
          }
          this.set_list_department('groupOuCode', value.ouCode);
          this.getSearchProductReport();
        }
        if (this.workgroup['groupOuCode'] !== value.ouCode) {
          if (this.changResetStatus === true) {
            this.workgroup = '';
          } else {
            this.workgroup = undefined;
          }
          this.set_list_workgroup('groupOuCode', value.ouCode);
          this.getSearchProductReport();
        }
      }
    } else if ((type === 'department' || type === 'workgroup' || type === 'division') && value === 'resetFilter') {

      if (this.changResetStatus === true) {
        console.log('00000-1 this.changResetStatus', this.changResetStatus);
        this.department = undefined;
        this.workgroup = undefined;
        this.division = undefined;
        this.fillterStatus = false;
        this.changResetStatus = !this.changResetStatus;
        this.getSearchProductReport();
      } else {
        console.log('00000-2 this.changResetStatus', this.changResetStatus);
        this.department = '';
        this.workgroup = '';
        this.division = '';
        this.fillterStatus = false;
        this.changResetStatus = !this.changResetStatus;
        this.getSearchProductReport();
      }

      this.departmentList_fill = JSON.parse(JSON.stringify(this.departmentList));
      this.workgroupList_fill = JSON.parse(JSON.stringify(this.workgroupList));
      this.divisionList_fill = JSON.parse(JSON.stringify(this.divisionList));
    }
    // this.departmentList_fill = [];
    // this.workgroupList_fill = [];
    // this.divisionList_fill = [];
    // console.log('====== filterSelection end =====');
  }

  set_list_department(search_at, search_ouCode1, search_ouCode2?) {
    if (search_at === 'sctrOuCode') {
      this.departmentList_fill = this.departmentList.filter((ele) => {
        return ele.sctrOuCode === search_ouCode1;
      });
      if (this.departmentList_fill.length === 0) {
        this.departmentList_fill = 'nondata';
        // this.department = null;
      }
    } else if (search_at === 'groupOuCode') {
      this.departmentList_fill = this.departmentList.filter((ele) => {
        return ele.groupOuCode === search_ouCode1;
      });
      if (this.departmentList_fill.length === 0) {
        this.departmentList_fill = 'nondata';
        // this.department = null;
      }
    } else if (search_at === 'sctrOuCode_groupOuCode') {
      this.departmentList_fill = this.departmentList.filter((ele) => {
        return ele.sctrOuCode === search_ouCode1 && ele.groupOuCode === search_ouCode2;
      });
      if (this.departmentList_fill.length === 0) {
        this.departmentList_fill = 'nondata';
        // this.department = null;
      }
    }
  }

  set_list_workgroup(search_at, search_ouCode1, search_ouCode2?) {
    if (search_at === 'ouCode') {
      this.workgroupList_fill = this.workgroupList.filter((ele) => {
        return ele.ouCode === search_ouCode1;
      });
      if (this.workgroupList_fill.length === 0) {
        this.workgroupList_fill = 'nondata';
        // this.workgroup = null;
      }
    } else if (search_at === 'groupOuCode') {
      this.workgroupList_fill = this.workgroupList.filter((ele) => {
        return ele.groupOuCode === search_ouCode1;
      });
      if (this.workgroupList_fill.length === 0) {
        this.workgroupList_fill = 'nondata';
        // this.workgroup = null;
      }
    } else if (search_at === 'ouCode_groupOuCode') {
      this.workgroupList_fill = this.workgroupList.filter((ele) => {
        return ele.ouCode === search_ouCode1 && ele.groupOuCode === search_ouCode2;
      });
      if (this.workgroupList_fill.length === 0) {
        this.workgroupList_fill = 'nondata';
        // this.workgroup = null;
      }
    }
  }

  set_list_division(search_at, search_ouCode1, search_ouCode2?) {
    if (search_at === 'ouCode') {
      this.divisionList_fill = this.divisionList.filter((ele) => {
        return ele.ouCode === search_ouCode1;
      });
      if (this.divisionList_fill.length === 0) {
        this.divisionList_fill = 'nondata';
        // this.division = null;
      }
    } else if (search_at === 'ouCode_ouCode') {
      this.divisionList_fill = this.divisionList.filter((ele) => {
        return ele.ouCode === search_ouCode1 && ele.ouCode === search_ouCode2;
      });
      if (this.divisionList_fill.length === 0) {
        this.divisionList_fill = 'nondata';
        // this.division = null;
      }
    }
  }
}
