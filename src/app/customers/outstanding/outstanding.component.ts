import { element } from 'protractor';
import { Component, OnInit, ViewChild } from '@angular/core';
import { OutstandingService } from '../../services/outstanding/outstanding.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { DateComponent } from '../datepicker/date/date.component';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { promise } from 'protractor';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogSaveStatusComponent } from '../outstanding/dialog-save-status/dialog-save-status.component';

@Component({
  selector: 'app-outstanding',
  templateUrl: './outstanding.component.html',
  styleUrls: ['./outstanding.component.scss'],
})
export class OutstandingComponent implements OnInit {
  constructor(
    private outstandingService: OutstandingService,
    private router: Router,
    private dialog: MatDialog
  ) { }

  @ViewChild(DateComponent) Date: DateComponent;

  headers = ['ติดตามความคืบหน้า เดือนที่ '];
  subheaders = ['รายละเอียด', '% รวมที่ดำเนินการเสร็จ'];
  announced: any;
  saveStatus = '';

  selectradio: any;
  slrirk = true;
  sllaw = true;
  sloperating = true;
  slfinance = true;
  slcredit = true;
  slissue = true;
  slcheckissue = true;

  dataoutstanding: any;
  datarisk: any = {
    extraComment: [],
    extraCommentProgressDelete: [],
    extraCommentProgressId: [],
    processConsiders: [],
    processConsidersProgressDelete: [],
    processConsidersProgressId: [],
    readiness: [],
    readinessProgressDelete: [],
    readinessProgressId: []
  };
  process_riskconsiders = 0;
  countprocess_riskconsiders = [];
  countprocess_readiness = [];
  countprocess_extracomment = [];
  process_readiness = 0;
  process_extracomment = 0;

  datacompliance: any = [];
  process_account = 0;
  countprocess_account = [];
  dataoperation: any = [];
  process_operationconsiders = 0;
  countprocess_operationconsiders = [];
  process_operation1 = 0;
  countprocess_operation1 = [];
  process_processoperation = 0;
  countprocess_processoperation = [];
  process_operation2 = 0;
  countprocess_operation2 = [];

  datafinance: any = [];
  process_budget = 0;
  countprocess_budget = [];
  process_performance = 0;
  countprocess_performance = [];
  process_governance = 0;
  countprocess_governance = [];

  dataretail: any = [];
  process_retailconsiders = 0;
  countprocess_retailconsiders = [];
  process_retail1 = 0;
  countprocess_retail1 = [];
  process_processretail = 0;
  countprocess_processretail = [];
  process_retail2 = 0;
  countprocess_retail2 = [];

  dataResult: any = {
    result: [],
  };
  countprocess_result = [];
  process_result = 0;

  process_department1_progress = 0;
  count_department1_progress = [];
  process_department2_progress = 0;
  count_department2_progress = [];
  process_rule = 0;
  count_rule = [];
  process_law = 0;
  count_law = [];
  get_role: any;
  get_level: any;

  processConsiders = [];
  popUpShowRow = null;
  popUpShowCol = [{ indextable: null, indexCol: null }];
  attachOutsideOnClick_1 = false;
  enabled = false;
  enabledCount = false;
  clickedOutsideCount: number;
  clickedOutsideDetalCount: number;
  disabledRowAll = false;
  disabledRow = true;
  disabledCol = [];
  colZise = 3;
  editText = '';

  // risk
  popUpShowRowRiskconsiders = null;
  popUpShowRowReadiness = null;
  popUpShowRowExtracomment = null;

  // compliance
  popUpShowRowAccount = null;
  popUpShowRowDepartment1 = null;
  popUpShowRowDepartment2 = null;
  popUpShowRowRule = null;
  popUpShowRowLaw = null;

  // finance
  popUpShowRowbudget = null;
  popUpShowRowperformance = null;
  popUpShowRowgovernance = null;

  // operation
  operationConsidersPopUpShowRow = null;
  operation1PopUpShowRow = null;
  processOperationPopUpShowRow = null;
  operation2PopUpShowRow = null;

  // retail
  popUpShowRowRetail = null;
  popUpShowRowRetail_retail1 = null;
  popUpShowRowRetail_processRetail = null;
  popUpShowRowRetail_retail2 = null;

  // result
  popUpShowRowResult = null;

  status_loading = true;
  message_success = '';
  saveDraftstatus_State = false;

  // check data before Sendwork
  ListAletEmpty = {
    status: false,
    risk: {
      status: true,
      table: []
    },
    compliance: {
      status: true,
      table: []
    },
    operation: {
      status: true,
      table: []
    },
    finance: {
      status: true,
      table: []
    },
    retail: {
      status: true,
      table: []
    },
    result: {
      status: true,
      table: []
    },
  }
  ngOnInit() {
    this.get_role = localStorage.getItem('role');
    this.get_level = localStorage.getItem('level');

    if (this.get_role === 'PO' && this.get_level !== 'พนักงาน') {
      this.get_role = null;
    }
    console.log('Role:', this.get_role);
    console.log('Level:', this.get_level);
    this.getOutstanding();
    this.getCollectIssue();
  }
  backpage() {
    // localStorage.setItem('requestId', '');
    this.router.navigate(['/dashboard1']);
  }
  changeSaveDraft() {
    this.saveDraftstatus_State = true;
    this.outstandingService.status_state = true;
    console.log('saveDraftstatus:', this.saveDraftstatus_State)

  }
  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate');
    // return confirm();
    const subject = new Subject<boolean>();
    // const status = this.sidebarService.outPageStatus();
    console.log('000 sidebarService status:', status);
    if (this.saveDraftstatus_State === true) {
      // console.log('000 sidebarService status:', status);
      const dialogRef = this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        console.log('data::', data);
        if (data.returnStatus === true) {
          subject.next(true);
          this.saveData();
          this.saveDraftstatus_State = false;
        } else if (data.returnStatus === false) {
          console.log('000000 data::', data.returnStatus);
          // this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
        }
      });
      console.log('8889999', subject);
      return subject;
    } else {
      return true;
    }
  }
  saveData() {
    // this.dataoutstanding.requestId = localStorage.getItem('requestId');
    // console.log('this.dataoutstanding', this.dataoutstanding.risk);
    const dataConsider = [];
    const dataReadiness = [];
    const dataExtra = [];
    const dataAccount = [];
    const dataDepartment1 = [];
    const dataDepartment2 = [];
    const dataRule = [];
    const dataLaw = [];

    if (this.get_role === 'PO' && this.get_level === 'พนักงาน') {
      this.status_loading = true;
      this.dataoutstanding.risk.processConsiders.forEach((elements) => {
        elements.subProcess.forEach((sudElements) => {
          sudElements.subProcessRisk.forEach((sudProcess) => {
            dataConsider.push(sudProcess);
          });
        });
      });
      if (this.dataoutstanding.risk.readiness != null) {
        this.dataoutstanding.risk.readiness.forEach((elements) => {
          elements.detail.forEach((sudElements) => {
            dataReadiness.push(sudElements);
          });
        });
      }
      this.dataoutstanding.risk.extraComment.forEach((elements) => {
        dataExtra.push(elements);
      });

      this.dataoutstanding.compliance.account.forEach((elements) => {
        elements.topic.forEach((sudElements) => {
          sudElements.detail.forEach((sudProcess) => {
            dataAccount.push(sudProcess);
          });
        });
      });
      this.dataoutstanding.compliance.department1.forEach((elements) => {
        elements.topic.forEach((sudElements) => {
          sudElements.detail.forEach((sudProcess) => {
            dataDepartment1.push(sudProcess);
          });
        });
      });

      this.dataoutstanding.compliance.department2.forEach((elements) => {
        elements.topic.forEach((sudElements) => {
          sudElements.detail.forEach((sudProcess) => {
            dataDepartment2.push(sudProcess);
          });
        });
      });
      this.dataoutstanding.compliance.rule.forEach((elements) => {
        elements.topic.forEach((sudElements) => {
          sudElements.detail.forEach((sudProcess) => {
            dataRule.push(sudProcess);
          });
        });
      });
      this.dataoutstanding.compliance.law.forEach((elements) => {
        elements.topic.forEach((sudElements) => {
          sudElements.detail.forEach((sudProcess) => {
            dataLaw.push(sudProcess);
            // console.log('addLaw', sudProcess.process);
          });
        });
      });
      // console.log(dataConsider);

      const datasend = {
        requestId: Number(localStorage.getItem('requestId')),
        risk: {
          processConsidersProgressDelete: this.dataoutstanding.risk[
            'processConsidersProgressDelete'
          ],
          processConsidersProgressId: this.dataoutstanding.risk
            .processConsidersProgressId,
          processConsiders: dataConsider,
          readinessProgressDelete: this.dataoutstanding.risk[
            'readinessProgressDelete'
          ],
          readinessProgressId: this.dataoutstanding.risk.readinessProgressId,
          readiness: dataReadiness,
          extraCommentProgressDelete: this.dataoutstanding.risk[
            'extraCommentProgressDelete'
          ],
          extraCommentProgressId: this.dataoutstanding.risk
            .extraCommentProgressId,
          extraComment: dataExtra,
        },
        compliance: {
          accountProgressId: this.dataoutstanding.compliance.accountProgressId,
          accountProgressDelete: this.dataoutstanding.compliance[
            'accountProgressDelete'
          ],
          account: dataAccount,

          department1ProgressId: this.dataoutstanding.compliance
            .department1ProgressId,
          department1ProgressDelete: this.dataoutstanding.compliance[
            'department1ProgressDelete'
          ],
          department1: dataDepartment1,

          department2ProgressId: this.dataoutstanding.compliance
            .department2ProgressId,
          department2ProgressDelete: this.dataoutstanding.compliance[
            'department2ProgressDelete'
          ],
          department2: dataDepartment2,

          ruleProgressId: this.dataoutstanding.compliance.ruleProgressId,
          ruleProgressDelete: this.dataoutstanding.compliance[
            'ruleProgressDelete'
          ],
          rule: dataRule,

          lawProgressId: this.dataoutstanding.compliance.lawProgressId,
          lawProgressDelete: this.dataoutstanding.compliance[
            'lawProgressDelete'
          ],
          law: dataLaw,
        },
        finance: {
          budgetProgressId: this.dataoutstanding.finance.budgetProgressId,
          budgetProgressDelete: this.dataoutstanding.finance[
            'budgetProgressDelete'
          ],
          budget: this.saveFinance_Result().budget,
          performanceProgressId: this.dataoutstanding.finance
            .performanceProgressId,
          performanceProgressDelete: this.dataoutstanding.finance[
            'performanceProgressDelete'
          ],
          performance: this.saveFinance_Result().performance,
          governanceProgressId: this.dataoutstanding.finance
            .governanceProgressId,
          governanceProgressDelete: this.dataoutstanding.finance[
            'governanceProgressDelete'
          ],
          governance: this.saveFinance_Result().governance,
        },
        operation: {
          operationProgressId: this.dataoutstanding.operation
            .processOperationId,
          operationProgressDelete: this.dataoutstanding.operation[
            'operationProgressDelete'
          ],
          operation: this.saveOperation_Result().processOperation,
          operation1ProgressId: this.dataoutstanding.operation
            .processOperation1Id,
          operation1ProgressDelete: this.dataoutstanding.operation[
            'operation1ProgressDelete'
          ],
          operation1: this.saveOperation_Result().operation1,
          operation2ProgressId: this.dataoutstanding.operation
            .processOperation2Id,
          operation2ProgressDelete: this.dataoutstanding.operation[
            'operation2ProgressDelete'
          ],
          operation2: this.saveOperation_Result().operation2,
          considersProgressId: this.dataoutstanding.operation
            .processConsidersId,
          considersProgressDelete: this.dataoutstanding.operation[
            'considersProgressDelete'
          ],
          considers: this.saveOperation_Result().processConsiders,
        },
        retail: {
          processConsidersProgressId: this.dataoutstanding.retail
            .processConsidersProgressId,
          processConsidersProgressDelete: this.dataoutstanding.retail[
            'processConsidersProgressDelete'
          ],
          processConsiders: this.SaveRetail_Result().processConsiders,
          retail1ProgressId: this.dataoutstanding.retail.retail1ProgressId,
          retail1ProgressDelete: this.dataoutstanding.retail[
            'retail1ProgressDelete'
          ],
          retail1: this.SaveRetail_Result().retail1,
          processOperationsProgressId: this.dataoutstanding.retail
            .processOperationsProgressId,
          processOperationsProgressDelete: this.dataoutstanding.retail[
            'processOperationsProgressDelete'
          ],
          processOperations: this.SaveRetail_Result().processOperations,
          retail2ProgressId: this.dataoutstanding.retail.retail2ProgressId,
          retail2ProgressDelete: this.dataoutstanding.retail[
            'retail2ProgressDelete'
          ],
          retail2: this.SaveRetail_Result().retail2,
        },
        result: {
          resultProgressId: this.dataoutstanding.result.resultProgressId,
          result: this.SaveRetail_Result().result,
          resultProgressDelete: this.dataoutstanding.result[
            'resultProgressDelete'
          ],
        },
      };

      console.log('save ', datasend);

      this.outstandingService.updateOutstanding(datasend).subscribe((res) => {
        console.log('res save', res);
        if (res['status'] === 'success') {
          // this.status_loading = false;
          this.getOutstanding();
          this.saveDraftstatus_State = false;
          setTimeout(() => (this.saveStatus = ''), 5000);
          // setTimeout(() => (this.status_loading = true ), 500);
          // setTimeout(() => (this.status_loading = false ), 500);
          //  this.dataoutstanding any;
          this.datarisk = {
            extraComment: [],
            extraCommentProgressDelete: [],
            extraCommentProgressId: [],
            processConsiders: [],
            processConsidersProgressDelete: [],
            processConsidersProgressId: [],
            readiness: [],
            readinessProgressDelete: [],
            readinessProgressId: []
          };
          this.process_riskconsiders = 0;
          this.countprocess_riskconsiders = [];
          this.countprocess_readiness = [];
          this.countprocess_extracomment = [];
          this.process_readiness = 0;
          this.process_extracomment = 0;
          this.process_account = 0;
          this.countprocess_account = [];

          // ===================================
          this.process_department1_progress = 0;
          this.count_department1_progress = [];
          this.process_department2_progress = 0;
          this.count_department2_progress = [];
          this.process_rule = 0;
          this.count_rule = [];
          this.process_law = 0;
          this.count_law = [];
          //  ==================================
          this.process_operationconsiders = 0;
          this.countprocess_operationconsiders = [];
          this.process_operation1 = 0;
          this.countprocess_operation1 = [];
          this.process_processoperation = 0;
          this.countprocess_processoperation = [];
          this.process_operation2 = 0;
          this.countprocess_operation2 = [];

          //  ==================================
          this.process_budget = 0;
          this.countprocess_budget = [];
          this.process_performance = 0;
          this.countprocess_performance = [];
          this.process_governance = 0;
          this.countprocess_governance = [];
          // ==================================================//

          this.process_retailconsiders = 0;
          this.countprocess_retailconsiders = [];
          this.process_retail1 = 0;
          this.countprocess_retail1 = [];
          this.process_processretail = 0;
          this.countprocess_processretail = [];
          this.process_retail2 = 0;
          this.countprocess_retail2 = [];

          this.dataResult.result = [];
          this.countprocess_result = [];
          this.process_result = 0;
          // console.log('res save', res);

          setTimeout(() => {
            this.message_success = 'บันทึกประเด็นคงค้างและสิ่งที่ต้องดำเนินการเรียบร้อย';
            this.saveStatus = 'success';
          }, 1000);
        } else {
          Swal.fire({
            title: 'error',
            text: res['message'],
            icon: 'error',
            showConfirmButton: false,
            timer: 3000,
          });
          this.status_loading = false;
        }
      });
    } else if (this.get_level !== 'พนักงาน') {
      this.status_loading = true;
      let _boardApprove: boolean;
      let _edit: boolean;
      let _editText = '';
      if (this.selectradio) {
        if (this.selectradio === 'เป็นไปตามมติคณะกรรมการผลิตภัณฑ์') {
          _boardApprove = true;
          _edit = false;
          _editText = '';
        } else if (this.selectradio === 'ส่งกลับแก้ไข') {
          _boardApprove = false;
          _edit = true;
          _editText = this.editText;
        }

        const data = {
          requestId: Number(localStorage.getItem('requestId')),
          send: false,
          boardApprove: _boardApprove,
          edit: _edit,
          editText: _editText,
        };
        console.log('datasend savedata::', data);
        if (data['boardApprove'] === false && data['editText'] === '') {
          alert('กรุณาระบุรายละเอียดเพิ่มเติม');
        } else {
          this.outstandingService.updateCollectIssue(data).subscribe((res) => {
            console.log('res', res);
            if (res['status'] === 'success') {
              // this.getOutstanding();
              this.message_success = 'บันทึกสอบทานประเด็นคงค้างและสิ่งที่ต้องดำเนินการเรียบร้อย';
              this.saveStatus = 'success';
              this.status_loading = false;
              this.saveDraftstatus_State = false;
              setTimeout(() => (this.saveStatus = ''), 3000);
            } else {
              Swal.fire({
                title: 'error',
                text: res['message'],
                icon: 'error',
                showConfirmButton: false,
                timer: 3000,
              });
              this.status_loading = false;
            }
          });
        }

      } else {
        this.status_loading = false;
      }
    }
  }

  SendWork() {
    console.log(this.get_role);
    // console.log('data:',this.dataoutstanding)
    const dataConsider = [];
    const dataReadiness = [];
    const dataExtra = [];
    const dataAccount = [];
    const dataDepartment1 = [];
    const dataDepartment2 = [];
    const dataRule = [];
    const dataLaw = [];
    if (this.get_role === 'PO' && this.get_level === 'พนักงาน') {
      this.status_loading = true;
      if (this.checkDateAndResponsible() == false) {
        console.log(this.ListAletEmpty)
        this.status_loading = false;
        document.getElementById('opendModalSendwork').click();
      } else {
        // this.status_loading = true;
        this.dataoutstanding.risk.processConsiders.forEach((elements) => {
          elements.subProcess.forEach((sudElements) => {
            sudElements.subProcessRisk.forEach((sudProcess) => {
              dataConsider.push(sudProcess);
            });
          });
        });
        if (this.dataoutstanding.risk.readiness !== null) {
          this.dataoutstanding.risk.readiness.forEach((elements) => {
            elements.detail.forEach((sudElements) => {
              dataReadiness.push(sudElements);
            });
          });
          this.dataoutstanding.risk.extraComment.forEach((elements) => {
            dataExtra.push(elements);
          });
        }



        this.dataoutstanding.compliance.account.forEach((elements) => {
          elements.topic.forEach((sudElements) => {
            sudElements.detail.forEach((sudProcess) => {
              dataAccount.push(sudProcess);
            });
          });
        });
        this.dataoutstanding.compliance.department1.forEach((elements) => {
          elements.topic.forEach((sudElements) => {
            sudElements.detail.forEach((sudProcess) => {
              dataDepartment1.push(sudProcess);
            });
          });
        });

        this.dataoutstanding.compliance.department2.forEach((elements) => {
          elements.topic.forEach((sudElements) => {
            sudElements.detail.forEach((sudProcess) => {
              dataDepartment2.push(sudProcess);
            });
          });
        });
        this.dataoutstanding.compliance.rule.forEach((elements) => {
          elements.topic.forEach((sudElements) => {
            sudElements.detail.forEach((sudProcess) => {
              dataRule.push(sudProcess);
            });
          });
        });
        this.dataoutstanding.compliance.law.forEach((elements) => {
          elements.topic.forEach((sudElements) => {
            sudElements.detail.forEach((sudProcess) => {
              dataLaw.push(sudProcess);
              // console.log('addLaw', sudProcess.process);
            });
          });
        });
        // console.log(dataConsider);

        const datasend = {
          requestId: Number(localStorage.getItem('requestId')),
          risk: {
            processConsidersProgressDelete: this.dataoutstanding.risk[
              'processConsidersProgressDelete'
            ],
            processConsidersProgressId: this.dataoutstanding.risk
              .processConsidersProgressId,
            processConsiders: dataConsider,
            readinessProgressDelete: this.dataoutstanding.risk[
              'readinessProgressDelete'
            ],
            readinessProgressId: this.dataoutstanding.risk.readinessProgressId,
            readiness: dataReadiness,
            extraCommentProgressDelete: this.dataoutstanding.risk[
              'extraCommentProgressDelete'
            ],
            extraCommentProgressId: this.dataoutstanding.risk
              .extraCommentProgressId,
            extraComment: dataExtra,
          },
          compliance: {
            accountProgressId: this.dataoutstanding.compliance.accountProgressId,
            accountProgressDelete: this.dataoutstanding.compliance[
              'accountProgressDelete'
            ],
            account: dataAccount,

            department1ProgressId: this.dataoutstanding.compliance
              .department1ProgressId,
            department1ProgressDelete: this.dataoutstanding.compliance[
              'department1ProgressDelete'
            ],
            department1: dataDepartment1,

            department2ProgressId: this.dataoutstanding.compliance
              .department2ProgressId,
            department2ProgressDelete: this.dataoutstanding.compliance[
              'department2ProgressDelete'
            ],
            department2: dataDepartment2,

            ruleProgressId: this.dataoutstanding.compliance.ruleProgressId,
            ruleProgressDelete: this.dataoutstanding.compliance[
              'ruleProgressDelete'
            ],
            rule: dataRule,

            lawProgressId: this.dataoutstanding.compliance.lawProgressId,
            lawProgressDelete: this.dataoutstanding.compliance[
              'lawProgressDelete'
            ],
            law: dataLaw,
          },
          finance: {
            budgetProgressId: this.dataoutstanding.finance.budgetProgressId,
            budgetProgressDelete: this.dataoutstanding.finance[
              'budgetProgressDelete'
            ],
            budget: this.saveFinance_Result().budget,
            performanceProgressId: this.dataoutstanding.finance
              .performanceProgressId,
            performanceProgressDelete: this.dataoutstanding.finance[
              'performanceProgressDelete'
            ],
            performance: this.saveFinance_Result().performance,
            governanceProgressId: this.dataoutstanding.finance
              .governanceProgressId,
            governanceProgressDelete: this.dataoutstanding.finance[
              'governanceProgressDelete'
            ],
            governance: this.saveFinance_Result().governance,
          },
          operation: {
            operationProgressId: this.dataoutstanding.operation
              .processOperationId,
            operationProgressDelete: this.dataoutstanding.operation[
              'operationProgressDelete'
            ],
            operation: this.saveOperation_Result().processOperation,
            operation1ProgressId: this.dataoutstanding.operation
              .processOperation1Id,
            operation1ProgressDelete: this.dataoutstanding.operation[
              'operation1ProgressDelete'
            ],
            operation1: this.saveOperation_Result().operation1,
            operation2ProgressId: this.dataoutstanding.operation
              .processOperation2Id,
            operation2ProgressDelete: this.dataoutstanding.operation[
              'operation2ProgressDelete'
            ],
            operation2: this.saveOperation_Result().operation2,
            considersProgressId: this.dataoutstanding.operation
              .processConsidersId,
            considersProgressDelete: this.dataoutstanding.operation[
              'considersProgressDelete'
            ],
            considers: this.saveOperation_Result().processConsiders,
          },
          retail: {
            processConsidersProgressId: this.dataoutstanding.retail
              .processConsidersProgressId,
            processConsidersProgressDelete: this.dataoutstanding.retail[
              'processConsidersProgressDelete'
            ],
            processConsiders: this.SaveRetail_Result().processConsiders,
            retail1ProgressId: this.dataoutstanding.retail.retail1ProgressId,
            retail1ProgressDelete: this.dataoutstanding.retail[
              'retail1ProgressDelete'
            ],
            retail1: this.SaveRetail_Result().retail1,
            processOperationsProgressId: this.dataoutstanding.retail
              .processOperationsProgressId,
            processOperationsProgressDelete: this.dataoutstanding.retail[
              'processOperationsProgressDelete'
            ],
            processOperations: this.SaveRetail_Result().processOperations,
            retail2ProgressId: this.dataoutstanding.retail.retail2ProgressId,
            retail2ProgressDelete: this.dataoutstanding.retail[
              'retail2ProgressDelete'
            ],
            retail2: this.SaveRetail_Result().retail2,
          },
          result: {
            resultProgressId: this.dataoutstanding.result.resultProgressId,
            result: this.SaveRetail_Result().result,
            resultProgressDelete: this.dataoutstanding.result[
              'resultProgressDelete'
            ],
          },
        };

        console.log('save ', datasend);

        this.outstandingService.updateOutstanding(datasend).subscribe((resSaved) => {
          console.log('res save before SendWork:', resSaved);
          if (resSaved['status'] === 'success') {
            this.status_loading = false;
            this.outstandingService.SendDataOutstanding(Number(localStorage.getItem('requestId')))
              .subscribe((res) => {
                console.log(res);
                this.saveDraftstatus_State = false;
                if (res['status'] === 'success') {
                  this.status_loading = false;
                  this.saveDraftstatus_State = false;
                  localStorage.setItem('requestId', '');
                  Swal.fire({
                    title: 'success',
                    text: 'ส่งงานเรียบร้อย',
                    icon: 'success',
                    showConfirmButton: false,
                    timer: 3000,
                  }).then(() => {
                    this.router.navigate(['/dashboard1']);
                  });

                } else {
                  this.status_loading = false;
                  this.saveDraftstatus_State = false;
                  Swal.fire({
                    title: 'error',
                    text: res['message'],
                    icon: 'error',
                    showConfirmButton: false,
                    timer: 3000,
                  });

                }
              });
          } else {
            this.status_loading = false;
            Swal.fire({
              title: 'error',
              text: resSaved['message'],
              icon: 'error',
              showConfirmButton: false,
              timer: 3000,
            });

          }
        });
      }
    } else if (this.get_level !== 'พนักงาน') {
      let _boardApprove = false;
      let _edit = false;
      let _editText = '';
      if (this.selectradio) {
        if (this.selectradio === 'เป็นไปตามมติคณะกรรมการผลิตภัณฑ์') {
          _boardApprove = true;
          _edit = false;
          _editText = '';
        } else if (this.selectradio === 'ส่งกลับแก้ไข') {
          _boardApprove = false;
          _edit = true;
          _editText = this.editText;
        }

        const data = {
          requestId: Number(localStorage.getItem('requestId')),
          send: true,
          boardApprove: _boardApprove,
          edit: _edit,
          editText: _editText,
        };
        console.log('datasend sendwork::', data);
        if (data['boardApprove'] === false && data['editText'] === '') {
          alert('กรุณาระบุรายละเอียดเพิ่มเติม');
        } else {
          this.outstandingService.updateCollectIssue(data).subscribe((res) => {
            console.log('res', res);
            if (res['status'] === 'success') {
              this.saveDraftstatus_State = false;
              this.status_loading = false;
              Swal.fire({
                title: 'success',
                text: 'ส่งงานเรียบร้อย',
                icon: 'success',
                showConfirmButton: false,
                timer: 3000,
              }).then(() => {
                localStorage.setItem('requestId', '');
                this.router.navigate(['/dashboard1']);
              });

            } else {
              this.status_loading = false;
              this.saveDraftstatus_State = false;
              Swal.fire({
                title: 'error',
                text: res['message'],
                icon: 'error',
                showConfirmButton: false,
                timer: 3000,
              });
            }
          });
        }
      }
    }
  }

  checkDateAndResponsible() {
    let riskStatus = {
      status: true,
      table: []
    }
    let complianceStatus = {
      status: true,
      table: []
    }
    let operationStatus = {
      status: true,
      table: []
    }
    let financeStatus = {
      status: true,
      table: []
    }
    let retailStatus = {
      status: true,
      table: []
    }
    let resultStatus = {
      status: true,
      table: []
    }

    console.log('data::', this.dataoutstanding)
    // ----------------------------------------Risk------------------------------------------------------//
    const RiskData = this.dataoutstanding.risk
    if (RiskData.processConsiders !== null) {
      if (RiskData.processConsiders.length !== 0) {
        RiskData.processConsiders.forEach((process, index) => {
          process.subProcess.forEach((element) => {
            element.subProcessRisk.forEach((risk) => {
              if ((risk.responsible == null || risk.responsible == '')
                || (risk.finishDate == null || risk.finishDate == '')) {
                const found = riskStatus.table.find(processConsiders => processConsiders == 'รายละเอียดกระบวนการทำงาน');
                if (found == undefined) {
                  riskStatus.table.push('รายละเอียดกระบวนการทำงาน')
                }
              }
            });
          });
        })
      }
    }
    if (RiskData.readiness !== null) {
      if (RiskData.readiness.length !== 0) {
        RiskData.readiness.forEach((process, index) => {
          process.detail.forEach((detail) => {
            // console.log('test:',detail)
            if ((detail.responsible == null || detail.responsible == '')
              || (detail.finishDate == null || detail.finishDate == '')) {
              const found = riskStatus.table.find(readiness => readiness == 'ความพร้อมด้านเทคโนโลยีสารสนเทศ');
              if (found == undefined) {
                riskStatus.table.push('ความพร้อมด้านเทคโนโลยีสารสนเทศ')
              }
            }
          });
        })
      }
    }
    if (RiskData.extraComment !== null) {
      if (RiskData.extraComment.length !== 0) {
        RiskData.extraComment.forEach((process, index) => {
          if ((process.responsible == null || process.responsible == '')
            || (process.finishDate == null || process.finishDate == '')) {
            const found = riskStatus.table.find(extraComment => extraComment == 'ความเห็น');
            if (found == undefined) {
              riskStatus.table.push('ความเห็น')
            }
          }
        })
      }
    }
    // ---------------------------------------------End Risk-----------------------------------------------//
    // -----------------------------------------------Compliance-----------------------------------------------//
    const ComplianceData = this.dataoutstanding.compliance
    if (ComplianceData.account !== null) {
      if (ComplianceData.account.length !== 0) {
        ComplianceData.account.forEach((process, index) => {
          process.topic.forEach(topic => {
            topic.detail.forEach(ele => {
              if ((ele.responsible == null || ele.responsible == '')
                || (ele.finishDate == null || ele.finishDate == '')) {
                const found = complianceStatus.table.find(element => element == 'ฝ่ายกำกับระเบียบและการปฏิบัติงาน');
                if (found == undefined) {
                  complianceStatus.table.push('ฝ่ายกำกับระเบียบและการปฏิบัติงาน')
                }
              }
            })
          })

        })
      }
    }
    if (ComplianceData.department1 !== null) {
      if (ComplianceData.department1.length !== 0) {
        ComplianceData.department1.forEach((process, index) => {
          process.topic.forEach(topic => {
            topic.detail.forEach(ele => {
              if ((ele.responsible == null || ele.responsible == '')
                || (ele.finishDate == null || ele.finishDate == '')) {
                const found = complianceStatus.table.find(element => element == 'ฝ่ายกำกับการปฎิบัติตามกฎเกณฑ์ 1');
                if (found == undefined) {
                  complianceStatus.table.push('ฝ่ายกำกับการปฎิบัติตามกฎเกณฑ์ 1')
                }
              }
            })
          })
        })
      }
    }
    if (ComplianceData.department2 !== null) {
      if (ComplianceData.department2.length !== 0) {
        ComplianceData.department2.forEach((process, index) => {
          process.topic.forEach(topic => {
            topic.detail.forEach(ele => {
              if ((ele.responsible == null || ele.responsible == '')
                || (ele.finishDate == null || ele.finishDate == '')) {
                const found = complianceStatus.table.find(element => element == 'ฝ่ายกำกับการปฎิบัติตามกฎเกณฑ์ 2');
                if (found == undefined) {
                  complianceStatus.table.push('ฝ่ายกำกับการปฎิบัติตามกฎเกณฑ์ 2')
                }
              }
            })
          })
        })
      }
    }
    if (ComplianceData.rule !== null) {
      if (ComplianceData.rule.length !== 0) {
        ComplianceData.rule.forEach((process, index) => {
          process.topic.forEach(topic => {
            topic.detail.forEach(ele => {
              if ((ele.responsible == null || ele.responsible == '')
                || (ele.finishDate == null || ele.finishDate == '')) {
                const found = complianceStatus.table.find(element => element == 'ฝ่ายกำกับการปฎิบัติตามกฎเกณฑ์ทางการ');
                if (found == undefined) {
                  complianceStatus.table.push('ฝ่ายกำกับการปฎิบัติตามกฎเกณฑ์ทางการ')
                }
              }
            })
          })
        })
      }
    }
    if (ComplianceData.law !== null) {
      if (ComplianceData.law.length !== 0) {
        ComplianceData.law.forEach((process, index) => {
          process.topic.forEach(topic => {
            topic.detail.forEach(ele => {
              if ((ele.responsible == null || ele.responsible == '')
                || (ele.finishDate == null || ele.finishDate == '')) {
                const found = complianceStatus.table.find(element => element == 'ฝ่ายนิติการ');
                if (found == undefined) {
                  complianceStatus.table.push('ฝ่ายนิติการ')
                }
              }
            })
          })
        })
      }
    }
    // ------------------------------------------End Compliance-----------------------------------------------//

    // ------------------------------------------Operation-----------------------------------------------//
    const OperationData = this.dataoutstanding.operation
    if (OperationData.processConsiders !== null) {
      if (OperationData.processConsiders.length !== 0) {
        OperationData.processConsiders.forEach((process, index) => {
          process.subProcess.forEach((element) => {
            element.subProcessRisk.forEach((risk) => {
              if ((risk.responsible == null || risk.responsible == '')
                || (risk.finishDate == null || risk.finishDate == '')) {
                const found = operationStatus.table.find(processConsiders => processConsiders == 'รายละเอียดกระบวนการทำงาน');
                if (found == undefined) {
                  operationStatus.table.push('รายละเอียดกระบวนการทำงาน')
                }
              }
            });
          });
        })
      }
    }
    if (OperationData.operation1 !== null) {
      if (OperationData.operation1.length !== 0) {
        OperationData.operation1.forEach((process, index) => {
          if ((process.responsible == null || process.responsible == '')
            || (process.finishDate == null || process.finishDate == '')) {
            const found = operationStatus.table.find(element => element == 'ความเห็นเพิ่มเติม(1)');
            if (found == undefined) {
              operationStatus.table.push('ความเห็นเพิ่มเติม(1)')
            }
          }
        })
      }
    }
    if (OperationData.processOperation !== null) {
      if (OperationData.processOperation.length !== 0) {
        OperationData.processOperation.forEach((process, index) => {
          process.subProcess.forEach((detail) => {
            if ((detail.responsible == null || detail.responsible == '')
              || (detail.finishDate == null || detail.finishDate == '')) {
              const found = operationStatus.table.find(readiness => readiness == 'ความพร้อมด้านการดำเนินการ');
              if (found == undefined) {
                operationStatus.table.push('ความพร้อมด้านการดำเนินการ')
              }
            }
          });
        })
      }
    }
    if (OperationData.operation2 !== null) {
      if (OperationData.operation2.length !== 0) {
        OperationData.operation2.forEach((process, index) => {
          if ((process.responsible == null || process.responsible == '')
            || (process.finishDate == null || process.finishDate == '')) {
            const found = operationStatus.table.find(element => element == 'ความเห็นเพิ่มเติม(2)');
            if (found == undefined) {
              operationStatus.table.push('ความเห็นเพิ่มเติม(2)')
            }
          }
        })
      }
    }
    // ---------------------------------------End Operation-----------------------------------------------//
    // --------------------------------------------Finance-----------------------------------------------//
    const FinanceData = this.dataoutstanding.finance
    if (FinanceData.budget !== null) {
      if (FinanceData.budget.length !== 0) {
        FinanceData.budget.forEach((process, index) => {
          if ((process.responsible == null || process.responsible == '')
            || (process.finishDate == null || process.finishDate == '')) {
            const found = financeStatus.table.find(element => element == 'การบัญชี, งบประมาณและเบิกจ่าย');
            if (found == undefined) {
              financeStatus.table.push('การบัญชี, งบประมาณและเบิกจ่าย')
            }
          }
        })
      }
    }
    if (FinanceData.performance !== null) {
      if (FinanceData.performance.length !== 0) {
        FinanceData.performance.forEach((process, index) => {
          if ((process.responsible == null || process.responsible == '')
            || (process.finishDate == null || process.finishDate == '')) {
            const found = financeStatus.table.find(element => element == 'การวัดผลงานภายหลังออกผลิตภัณฑ์');
            if (found == undefined) {
              financeStatus.table.push('การวัดผลงานภายหลังออกผลิตภัณฑ์')
            }
          }
        })
      }
    }
    if (FinanceData.governance !== null) {
      if (FinanceData.governance.length !== 0) {
        FinanceData.governance.forEach((process, index) => {
          if ((process.responsible == null || process.responsible == '')
            || (process.finishDate == null || process.finishDate == '')) {
            const found = financeStatus.table.find(element => element == 'Data Governance (DG)');
            if (found == undefined) {
              financeStatus.table.push('Data Governance (DG)')
            }
          }
        })
      }
    }
    // ---------------------------------------End Finance-----------------------------------------------//

    // ------------------------------------------Retail-----------------------------------------------//
    const RetailData = this.dataoutstanding.retail
    if (RetailData.processConsiders !== null) {
      if (RetailData.processConsiders.length !== 0) {
        RetailData.processConsiders.forEach((process, index) => {
          process.subProcess.forEach((element) => {
            element.subProcessRisk.forEach((risk) => {
              if ((risk.responsible == null || risk.responsible == '')
                || (risk.finishDate == null || risk.finishDate == '')) {
                const found = retailStatus.table.find(processConsiders => processConsiders == 'รายละเอียดกระบวนการทำงาน');
                if (found == undefined) {
                  retailStatus.table.push('รายละเอียดกระบวนการทำงาน')
                }
              }
            });
          });
        })
      }
    }
    if (RetailData.retail1 !== null) {
      if (RetailData.retail1.length !== 0) {
        RetailData.retail1.forEach((process, index) => {
          if ((process.responsible == null || process.responsible == '')
            || (process.finishDate == null || process.finishDate == '')) {
            const found = retailStatus.table.find(element => element == 'ความเห็นเพิ่มเติม(1)');
            if (found == undefined) {
              retailStatus.table.push('ความเห็นเพิ่มเติม(1)')
            }
          }
        })
      }
    }
    if (RetailData.processOperations !== null) {
      if (RetailData.processOperations.length !== 0) {
        RetailData.processOperations.forEach((process, index) => {
          process.subProcess.forEach((detail) => {
            if ((detail.responsible == null || detail.responsible == '')
              || (detail.finishDate == null || detail.finishDate == '')) {
              const found = retailStatus.table.find(readiness => readiness == 'ความพร้อมด้านการดำเนินการ');
              if (found == undefined) {
                retailStatus.table.push('ความพร้อมด้านการดำเนินการ')
              }
            }
          });
        })
      }
    }
    if (RetailData.retail2 !== null) {
      if (RetailData.retail2.length !== 0) {
        RetailData.retail2.forEach((process, index) => {
          if ((process.responsible == null || process.responsible == '')
            || (process.finishDate == null || process.finishDate == '')) {
            const found = retailStatus.table.find(element => element == 'ความเห็นเพิ่มเติม(2)');
            if (found == undefined) {
              retailStatus.table.push('ความเห็นเพิ่มเติม(2)')
            }
          }
        })
      }
    }
    // ---------------------------------------End Retail-----------------------------------------------//

    // ------------------------------------------Result-----------------------------------------------//
    const ResultData = this.dataoutstanding.result
    if (ResultData.resul !== null) {
      if (ResultData.result.length !== 0) {
        ResultData.result.forEach((process, index) => {
          if ((process.responsible == null || process.responsible == '')
            || (process.finishDate == null || process.finishDate == '')) {
            const found = resultStatus.table.find(element => element == 'ระบุข้อมูลเพิ่มเติม');
            if (found == undefined) {
              resultStatus.table.push('ระบุข้อมูลเพิ่มเติม')
            }
          }
        })
      }
    }
    // ---------------------------------------End Result-----------------------------------------------//
    // console.log('risk::', riskStatus)
    // console.log('compliance:', complianceStatus)
    // console.log('operation:', operationStatus)
    // console.log('finance:', financeStatus)
    // console.log('retail', retailStatus)
    // console.log('result', resultStatus)
    if (riskStatus.table.length !== 0) {
      riskStatus.status = false
    }
    if (complianceStatus.table.length !== 0) {
      complianceStatus.status = false
    }
    if (operationStatus.table.length !== 0) {
      operationStatus.status = false
    }
    if (financeStatus.table.length !== 0) {
      financeStatus.status = false
    }
    if (retailStatus.table.length !== 0) {
      retailStatus.status = false
    }
    if (resultStatus.table.length !== 0) {
      resultStatus.status = false
    }
    this.ListAletEmpty.risk = riskStatus
    this.ListAletEmpty.compliance = complianceStatus
    this.ListAletEmpty.operation = operationStatus
    this.ListAletEmpty.finance = financeStatus
    this.ListAletEmpty.retail = retailStatus
    this.ListAletEmpty.result = resultStatus
    if (this.ListAletEmpty.risk.status &&
      this.ListAletEmpty.compliance.status &&
      this.ListAletEmpty.operation.status &&
      this.ListAletEmpty.finance.status &&
      this.ListAletEmpty.retail.status && this.ListAletEmpty.result.status) {
      this.ListAletEmpty.status = true
    }
    return this.ListAletEmpty.status
  }
  changeDate(date: any) {
    const SendI = this.Date.changeDate(date);
    this.changeSaveDraft();
    return SendI;
  }

  covertDate(datePC) {
    if (datePC != null) {
      const year = Number(datePC[0] + datePC[1] + datePC[2] + datePC[3]) + 543;
      const Date =
        datePC[8] + datePC[9] + '/' + datePC[5] + datePC[6] + '/' + year;
      return Date;
    } else {
      return null;
    }
  }
  // =============================================================Risk=======================================================
  addProcess() {
    this.changeSaveDraft();
    this.popUpShowRowRiskconsiders = null;
    const progress = {
      issu: null,
      progress: null,
      detail: null,
      percent: null,
    };

    this.countprocess_riskconsiders.push(progress);
    this.dataoutstanding.risk.processConsidersProgressId.push(null);
    this.dataoutstanding.risk.processConsiders.forEach((elements) => {
      elements.subProcess.forEach((sudElements) => {
        sudElements.subProcessRisk.forEach((sudProcess) => {
          sudProcess.process.push({
            issu: null,
            progress: null,
            detail: null,
            percent: null,
          });
        });
      });
    });

    // console.log('risk', this.dataoutstanding.risk.processConsidersProgressId);
  }

  addProcessReadiness() {
    this.changeSaveDraft();
    this.popUpShowRowReadiness = null;
    const progress = {
      issu: null,
      progress: null,
      detail: null,
      percent: null,
    };
    this.countprocess_readiness.push(progress);
    this.dataoutstanding.risk.readinessProgressId.push(null);
    this.dataoutstanding.risk.readiness.forEach((elements) => {
      elements.detail.forEach((sudElements) => {
        sudElements.process.push({
          issu: null,
          progress: null,
          detail: null,
          percent: null,
        });
      });
    });
  }

  addProcessExtracomment() {
    this.changeSaveDraft();
    this.popUpShowRowExtracomment = null;
    const progress = {
      issu: null,
      progress: null,
      detail: null,
      percent: null,
    };
    this.countprocess_extracomment.push(progress);
    this.dataoutstanding.risk.extraCommentProgressId.push(null);
    this.dataoutstanding.risk.extraComment.forEach((elements) => {
      elements.process.push({
        issu: null,
        progress: null,
        detail: null,
        percent: null,
      });
      // console.log('addProcessExtracomment', elements.process);
    });
  }
  // ==============================================================End Risk=================================================

  // =======================================================Compliance=======================================================
  addProcessAccount() {
    this.changeSaveDraft();
    const progress = {
      issu: null,
      progress: null,
      detail: null,
      percent: null,
    };
    this.countprocess_account.push(progress);
    this.dataoutstanding.compliance.accountProgressId.push(null);
    this.dataoutstanding.compliance.account.forEach((elements) => {
      elements.topic.forEach((sudElements) => {
        sudElements.detail.forEach((sudProcess) => {
          sudProcess.process.push({
            issu: null,
            progress: null,
            detail: null,
            percent: null,
          });
          // console.log('addProcessAccount', sudProcess.process);
        });
      });
      // });
    });
  }

  addDepartment1() {
    this.changeSaveDraft();
    this.popUpShowRowDepartment1 = null;
    const progress = {
      issu: null,
      progress: null,
      detail: null,
      percent: null,
    };
    this.count_department1_progress.push(progress);
    this.dataoutstanding.compliance.department1ProgressId.push(null);
    this.dataoutstanding.compliance.department1.forEach((elements) => {
      elements.topic.forEach((sudElements) => {
        sudElements.detail.forEach((sudProcess) => {
          sudProcess.process.push({
            issu: null,
            progress: null,
            detail: null,
            percent: null,
          });
          // console.log('addDepartment1', sudProcess.process);
        });
      });
      // });
    });
  }

  addDepartment2() {
    this.changeSaveDraft();
    this.popUpShowRowDepartment2 = null;
    const progress = {
      issu: null,
      progress: null,
      detail: null,
      percent: null,
    };
    this.count_department2_progress.push(progress);
    this.dataoutstanding.compliance.department2ProgressId.push(null);
    this.dataoutstanding.compliance.department2.forEach((elements) => {
      elements.topic.forEach((sudElements) => {
        sudElements.detail.forEach((sudProcess) => {
          sudProcess.process.push({
            issu: null,
            progress: null,
            detail: null,
            percent: null,
          });
          // console.log('addDepartment2', sudProcess.process);
        });
      });
      // });
    });
  }

  addRule() {
    this.changeSaveDraft();
    this.popUpShowRowRule = null;
    const progress = {
      issu: null,
      progress: null,
      detail: null,
      percent: null,
    };
    this.count_rule.push(progress);
    this.dataoutstanding.compliance.ruleProgressId.push(null);
    this.dataoutstanding.compliance.rule.forEach((elements) => {
      elements.topic.forEach((sudElements) => {
        sudElements.detail.forEach((sudProcess) => {
          sudProcess.process.push({
            issu: null,
            progress: null,
            detail: null,
            percent: null,
          });
          // console.log('addRule', sudProcess.process);
        });
      });
      // });
    });
  }

  addLaw() {
    this.changeSaveDraft();
    this.popUpShowRowLaw = null;
    const progress = {
      issu: null,
      progress: null,
      detail: null,
      percent: null,
    };
    this.count_law.push(progress);
    this.dataoutstanding.compliance.lawProgressId.push(null);
    this.dataoutstanding.compliance.law.forEach((elements) => {
      elements.topic.forEach((sudElements) => {
        sudElements.detail.forEach((sudProcess) => {
          sudProcess.process.push({
            issu: null,
            progress: null,
            detail: null,
            percent: null,
          });
          // console.log('addLaw', sudProcess.process);
        });
      });
    });
  }
  // =========================================================Compliance===================================================

  // ===================================================Retial =========================================================//
  addProcessRetail() {
    this.changeSaveDraft();
    this.popUpShowRowRetail = null;
    const progress = {
      issu: null,
      progress: null,
      detail: null,
      percent: null,
    };
    this.countprocess_retailconsiders.push(progress);
    this.dataoutstanding.retail.processConsidersProgressId.push(null);
    this.dataoutstanding.retail.processConsiders.forEach((elements) => {
      elements.subProcess.forEach((sudElements) => {
        if (sudElements.subProcessRisk.length === 0) {
          sudElements.subProcessRisk.push({
            pcCommentId: null,
            pcComment: null,
            pcExtraComment: null,
            pcIssue: null,
            responsible: null,
            finishDate: null,
            process: [
              {
                issu: null,
                progress: null,
                detail: null,
                percent: null,
              },
            ],
          });
        } else {
          sudElements.subProcessRisk.forEach((sudProcess) => {
            sudProcess.process.push({
              issu: null,
              progress: null,
              detail: null,
              percent: null,
            });
          });
        }
      });
    });
  }

  addRetail1() {
    this.changeSaveDraft();
    this.popUpShowRowRetail_retail1 = null;
    const progress = {
      issu: null,
      progress: null,
      detail: null,
      percent: null,
    };
    this.countprocess_retail1.push(progress);
    this.dataoutstanding.retail.retail1ProgressId.push(null);
    this.dataoutstanding.retail.retail1.forEach((elements) => {
      // elements.subProcess.forEach((sudElements) => {
      //   sudElements.process.forEach((sudProcess) => {
      elements.process.push({
        issu: null,
        progress: null,
        detail: null,
        percent: null,
      });

      //   });
      // });
    });
    // console.log('addRetail1', this.dataoutstanding.retail.retail1);
  }

  addProcessOperations() {
    this.changeSaveDraft();
    this.popUpShowRowRetail_processRetail = null;
    const progress = {
      issu: null,
      progress: null,
      detail: null,
      percent: null,
    };
    this.countprocess_processretail.push(progress);
    this.dataoutstanding.retail.processOperationsProgressId.push(null);
    this.dataoutstanding.retail.processOperations.forEach((elements) => {
      elements.subProcess.forEach((sudElements) => {
        //   sudElements.process.forEach((sudProcess) => {
        sudElements.process.push({
          issu: null,
          progress: null,
          detail: null,
          percent: null,
        });
      });
      // });
    });
    // console.log(
    //   'retail processOperations',
    //   this.dataoutstanding.retail.processOperations
    // );
  }

  add_retail2() {
    this.changeSaveDraft();
    this.popUpShowRowRetail_retail2 = null;
    const progress = {
      issu: null,
      progress: null,
      detail: null,
      percent: null,
    };
    this.countprocess_retail2.push(progress);
    this.dataoutstanding.retail.retail2ProgressId.push(null);
    this.dataoutstanding.retail.retail2.forEach((elements) => {
      // elements.subProcess.forEach((sudElements) => {
      //   sudElements.process.forEach((sudProcess) => {
      elements.process.push({
        issu: null,
        progress: null,
        detail: null,
        percent: null,
      });

      //   });
      // });
    });
    // console.log('addRetail 2', this.dataoutstanding.retail.retail2);
  }

  SaveRetail_Result() {
    // console.log('retail:', this.dataoutstanding.retail);
    const send = {
      processConsiders: [],
      retail1: [],
      retail2: [],
      processOperations: [],
      result: [],
    };

    this.dataoutstanding.retail.processConsiders.forEach((elements) => {
      elements.subProcess.forEach((subElements) => {
        subElements.subProcessRisk.forEach((subprocessrisk) => {
          send.processConsiders.push(subprocessrisk);
        });
      });
    });

    for (
      let index = 0;
      index < this.dataoutstanding.retail.retail1.length;
      index++
    ) {
      const retail1 = this.dataoutstanding.retail.retail1[index];
      send.retail1.push(retail1);
    }

    for (
      let index = 0;
      index < this.dataoutstanding.retail.processOperations.length;
      index++
    ) {
      const processOperations = this.dataoutstanding.retail.processOperations[
        index
      ].subProcess;
      processOperations.forEach((subProcess) => {
        send.processOperations.push(subProcess);
      });
    }

    for (
      let index = 0;
      index < this.dataoutstanding.retail.retail2.length;
      index++
    ) {
      const retail2 = this.dataoutstanding.retail.retail2[index];
      send.retail2.push(retail2);
    }

    // ========= Result=====================
    for (
      let index = 0;
      index < this.dataoutstanding.result.result.length;
      index++
    ) {
      const result = this.dataoutstanding.result.result[index];
      send.result.push(result);
    }
    // console.log('Save retail/Result:', send);
    return send;
  }

  // =========================================END Retail==============================================================//

  // ============================================================= Operation ==============================================
  addProcessOperation() {
    this.changeSaveDraft();
    this.operationConsidersPopUpShowRow = null;
    const progress = {
      issu: null,
      progress: null,
      detail: null,
      percent: null,
    };

    this.countprocess_operationconsiders.push(progress);
    this.dataoutstanding.operation.processConsidersId.push(null);
    this.dataoutstanding.operation.processConsiders.forEach((elements) => {
      elements.subProcess.forEach((sudElements) => {
        sudElements.subProcessRisk.forEach((sudProcess) => {
          sudProcess.process.push({
            issu: null,
            progress: null,
            detail: null,
            percent: null,
          });
        });
      });
    });
  }

  addOperation1() {
    this.changeSaveDraft();
    this.operation1PopUpShowRow = null;
    const progress = {
      issu: null,
      progress: null,
      detail: null,
      percent: null,
    };

    this.countprocess_operation1.push(progress);
    this.dataoutstanding.operation.processOperation1Id.push(null);
    this.dataoutstanding.operation.operation1.forEach((elements) => {
      elements.process.push({
        issu: null,
        progress: null,
        detail: null,
        percent: null,
      });
    });
  }

  addOperation() {
    this.changeSaveDraft();
    this.processOperationPopUpShowRow = null;

    const progress = {
      issu: null,
      progress: null,
      detail: null,
      percent: null,
    };

    this.countprocess_processoperation.push(progress);
    this.dataoutstanding.operation.processOperationId.push(null);
    this.dataoutstanding.operation.processOperation.forEach((elements) => {
      elements.subProcess.forEach((subelement) => {
        subelement.process.push({
          issu: null,
          progress: null,
          detail: null,
          percent: null,
        });
      });
    });
  }

  addOperation2() {
    this.changeSaveDraft();
    this.operation2PopUpShowRow = null;
    const progress = {
      issu: null,
      progress: null,
      detail: null,
      percent: null,
    };

    this.countprocess_operation2.push(progress);
    this.dataoutstanding.operation.processOperation2Id.push(null);
    this.dataoutstanding.operation.operation2.forEach((elements) => {
      elements.process.push({
        issu: null,
        progress: null,
        detail: null,
        percent: null,
      });
    });
  }

  saveOperation_Result() {
    const send = {
      processConsiders: [],
      operation1: [],
      processOperation: [],
      operation2: [],
    };

    this.dataoutstanding.operation.processConsiders.forEach((elements) => {
      elements.subProcess.forEach((subprocessrisk, i) => {
        subprocessrisk.subProcessRisk.forEach((process) => {
          send.processConsiders.push(process);
        });
      });
    });
    // console.log(send.processConsiders);

    for (
      let index = 0;
      index < this.dataoutstanding.operation.operation1.length;
      index++
    ) {
      const operation1 = this.dataoutstanding.operation.operation1[index];

      send.operation1.push(operation1);
    }

    this.dataoutstanding.operation.processOperation.forEach((elements) => {
      elements.subProcess.forEach((subprocess, i) => {
        send.processOperation.push(subprocess);
      });
    });

    for (
      let index = 0;
      index < this.dataoutstanding.operation.operation2.length;
      index++
    ) {
      const operation2 = this.dataoutstanding.operation.operation2[index];

      send.operation2.push(operation2);
    }
    // console.log('save operation:', send);
    return send;
  }
  // ======================================================  End Operation ===============================================

  // ==========================================================  Finance =================================================
  addBudget() {
    this.changeSaveDraft();
    // tslint:disable-next-line: no-shadowed-variable
    this.popUpShowRowbudget = null;
    const progress = {
      issu: null,
      progress: null,
      detail: null,
      percent: null,
    };

    this.countprocess_budget.push(progress);
    this.dataoutstanding.finance.budgetProgressId.push(null);
    this.dataoutstanding.finance.budget.forEach((elements) => {
      elements.process.push({
        issu: null,
        progress: null,
        detail: null,
        percent: null,
      });
    });
  }

  addPerformance() {
    this.changeSaveDraft();
    this.popUpShowRowperformance = null;
    const progress = {
      issu: null,
      progress: null,
      detail: null,
      percent: null,
    };

    this.countprocess_performance.push(progress);
    this.dataoutstanding.finance.performanceProgressId.push(null);
    this.dataoutstanding.finance.performance.forEach((elements) => {
      elements.process.push({
        issu: null,
        progress: null,
        detail: null,
        percent: null,
      });
    });
  }

  addGovernance() {
    this.changeSaveDraft();
    this.popUpShowRowgovernance = null;
    const progress = {
      issu: null,
      progress: null,
      detail: null,
      percent: null,
    };

    this.countprocess_governance.push(progress);
    this.dataoutstanding.finance.governanceProgressId.push(null);
    this.dataoutstanding.finance.governance.forEach((elements) => {
      elements.process.push({
        issu: null,
        progress: null,
        detail: null,
        percent: null,
      });
    });
  }

  saveFinance_Result() {
    const send = {
      budget: [],
      performance: [],
      governance: [],
    };

    for (
      let index = 0;
      index < this.dataoutstanding.finance.budget.length;
      index++
    ) {
      const budget = this.dataoutstanding.finance.budget[index];

      send.budget.push(budget);
    }

    for (
      let index = 0;
      index < this.dataoutstanding.finance.performance.length;
      index++
    ) {
      const performance = this.dataoutstanding.finance.performance[index];

      send.performance.push(performance);
    }

    for (
      let index = 0;
      index < this.dataoutstanding.finance.governance.length;
      index++
    ) {
      const governance = this.dataoutstanding.finance.governance[index];

      send.governance.push(governance);
    }
    // console.log('save finance:', send);
    return send;
  }
  // =========================================================End  Finance =================================================

  // ==============================================Result============================================================//
  Result() {
    this.changeSaveDraft();
    this.popUpShowRowResult = null;
    const progress = {
      issu: null,
      progress: null,
      detail: null,
      percent: null,
    };
    this.countprocess_result.push(progress);
    this.dataoutstanding.result.resultProgressId.push(null);
    this.dataoutstanding.result.result.forEach((elements) => {
      // elements.subProcess.forEach((sudElements) => {
      //   sudElements.process.forEach((sudProcess) => {
      elements.process.push({
        issu: null,
        progress: null,
        detail: null,
        percent: null,
      });

      //   });
      // });
    });
    // console.log('add result', this.dataoutstanding.result.result);
  }
  convertDate(date: any) {
    if (date !== null) {
      const now = moment(date);
      now.locale('th');
      const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();
      const convetRiskconsider =
        moment(date).format('DD/MM') + '/' + buddhishYear;
      date = convetRiskconsider;
    }
    return date;
  }
  // =========================================END Result============================================================//
  getOutstanding() {
    const ID = localStorage.getItem('requestId');
    if (ID === null || ID === undefined || ID === '') {
      this.status_loading = false;
    }
    this.outstandingService
      .getOutstanding(Number(localStorage.getItem('requestId')))
      .subscribe((res) => {
        if (res['status'] === 'success' && res['data']) {
          console.log(res);
          this.announced = res['data'].announced;
          this.dataoutstanding = res['data'];
          this.status_loading = false;
          // --------------- Risk ---------------
          console.log('this.dataoutstanding.risk', this.dataoutstanding.risk);
          this.datarisk = this.dataoutstanding.risk;
          this.dataoutstanding.risk['processConsidersProgressDelete'] = [];
          this.dataoutstanding.risk['readinessProgressDelete'] = [];
          this.dataoutstanding.risk['extraCommentProgressDelete'] = [];
          // processConsiders
          let _subprocess_considers: any;
          let _processrisk_considers: any;
          for (
            let index = 0;
            index < this.datarisk.processConsiders.length;
            index++
          ) {
            // tslint:disable-next-line: no-shadowed-variable
            const element = this.datarisk.processConsiders[index];
            _subprocess_considers = element.subProcess;

            for (let j = 0; j < _subprocess_considers.length; j++) {
              const processrisk = _subprocess_considers[j];
              _processrisk_considers = processrisk;

              for (
                let k = 0;
                k < _processrisk_considers.subProcessRisk.length;
                k++
              ) {
                const process = _processrisk_considers.subProcessRisk[k];
                // if (process.finishDate !== null) {
                //   const now = moment(process.finishDate);
                //   now.locale('th');
                //   const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();
                //   const convetRiskconsider = moment(process.finishDate).format('DD/MM') + '/' + buddhishYear;
                //   process.finishDate = convetRiskconsider;
                // }

                this.process_riskconsiders = process.process.length;
              }
            }
          }
          for (let m = 0; m < this.process_riskconsiders; m++) {
            this.countprocess_riskconsiders.push(m);
          }

          // readiness
          let _detail_readiness: any;
          let _process_readiness: any;
          if (this.datarisk.readiness !== null) {
            for (let index = 0; index < this.datarisk.readiness.length; index++) {
              // tslint:disable-next-line: no-shadowed-variable
              const element = this.datarisk.readiness[index];
              _detail_readiness = element.detail;

              for (let j = 0; j < _detail_readiness.length; j++) {
                const processreadiness = _detail_readiness[j];
                _process_readiness = processreadiness.process;

                // if (processreadiness.finishDate !== null) {
                //   const now = moment(processreadiness.finishDate);
                //   now.locale('th');
                //   const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();
                //   const convetRiskReadiness = moment(processreadiness.finishDate).format('DD/MM') + '/' + buddhishYear;
                //   processreadiness.finishDate = convetRiskReadiness;
                // }

                this.process_readiness = _process_readiness.length;
              }
            }
          }

          for (let k = 0; k < this.process_readiness; k++) {
            this.countprocess_readiness.push(k);
          }

          // extraComment
          let _process_extracomment: any;
          for (
            let index = 0;
            index < this.datarisk.extraComment.length;
            index++
          ) {
            // tslint:disable-next-line: no-shadowed-variable
            const element = this.datarisk.extraComment[index];
            // if (element.finishDate !== null) {
            //   const now = moment(element.finishDate);
            //   now.locale('th');
            //   const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();
            //   const convetRiskExtraComment = moment(element.finishDate).format('DD/MM') + '/' + buddhishYear;
            //   element.finishDate = convetRiskExtraComment;
            // }
            _process_extracomment = element.process;
            this.process_extracomment = _process_extracomment.length;
          }
          for (let j = 0; j < this.process_extracomment; j++) {
            this.countprocess_extracomment.push(j);
          }
          // --------------- End Risk ---------------

          // --------------- Compliance ---------------
          this.datacompliance = this.dataoutstanding.compliance;
          this.dataoutstanding.compliance['accountProgressDelete'] = [];
          this.dataoutstanding.compliance['department1ProgressDelete'] = [];
          this.dataoutstanding.compliance['department2ProgressDelete'] = [];
          this.dataoutstanding.compliance['ruleProgressDelete'] = [];
          this.dataoutstanding.compliance['lawProgressDelete'] = [];

          // account
          for (
            let index = 0;
            index < this.datacompliance.account.length;
            index++
          ) {
            const elementAccount = this.datacompliance.account[index];
            for (let i = 0; i < elementAccount.topic.length; i++) {
              const topic = elementAccount.topic[i].detail;
              for (let k = 0; k < topic.length; k++) {
                const process = topic[k];
                this.process_account = process.process.length;
                // if (process.finishDate !== null) {
                //   const now = moment(process.finishDate);
                //   now.locale('th');
                //   const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();
                //   const convetSAccount = moment(process.finishDate).format('DD/MM') + '/' + buddhishYear;
                //   process.finishDate = convetSAccount;
                // }
              }
            }
          }
          for (let m = 0; m < this.process_account; m++) {
            this.countprocess_account.push(m);
          }

          // department1
          for (
            let index = 0;
            index < this.datacompliance.department1.length;
            index++
          ) {
            const elementdepartment1 = this.datacompliance.department1[index];
            for (let i = 0; i < elementdepartment1.topic.length; i++) {
              const topic = elementdepartment1.topic[i].detail;
              for (let k = 0; k < topic.length; k++) {
                const process = topic[k];
                this.process_department1_progress = process.process.length;
                // if (process.finishDate !== null) {
                //   const now = moment(process.finishDate);
                //   now.locale('th');
                //   const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();
                //   const convetSAccount = moment(process.finishDate).format('DD/MM') + '/' + buddhishYear;
                //   process.finishDate = convetSAccount;
                // }
              }
            }
          }
          for (let m = 0; m < this.process_department1_progress; m++) {
            this.count_department1_progress.push(m);
          }

          // department2
          for (
            let index = 0;
            index < this.datacompliance.department2.length;
            index++
          ) {
            const elementdepartment2 = this.datacompliance.department2[index];
            for (let i = 0; i < elementdepartment2.topic.length; i++) {
              const topic = elementdepartment2.topic[i].detail;
              for (let k = 0; k < topic.length; k++) {
                const process = topic[k];
                this.process_department2_progress = process.process.length;
                // if (process.finishDate !== null) {
                //   const now = moment(process.finishDate);
                //   now.locale('th');
                //   const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();
                //   const convetSAccount = moment(process.finishDate).format('DD/MM') + '/' + buddhishYear;
                //   process.finishDate = convetSAccount;
                // }
              }
            }
          }
          for (let m = 0; m < this.process_department2_progress; m++) {
            this.count_department2_progress.push(m);
          }

          // rule
          for (
            let index = 0;
            index < this.datacompliance.rule.length;
            index++
          ) {
            const elementrule = this.datacompliance.rule[index];
            for (let i = 0; i < elementrule.topic.length; i++) {
              const topic = elementrule.topic[i].detail;
              for (let k = 0; k < topic.length; k++) {
                const process = topic[k];
                this.process_rule = process.process.length;
                // if (process.finishDate !== null) {
                //   const now = moment(process.finishDate);
                //   now.locale('th');
                //   const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();
                //   const convetSAccount = moment(process.finishDate).format('DD/MM') + '/' + buddhishYear;
                //   process.finishDate = convetSAccount;
                // }
              }
            }
          }
          for (let m = 0; m < this.process_rule; m++) {
            this.count_rule.push(m);
          }

          // law
          for (let index = 0; index < this.datacompliance.law.length; index++) {
            const elementlaw = this.datacompliance.law[index];
            for (let i = 0; i < elementlaw.topic.length; i++) {
              const topic = elementlaw.topic[i].detail;
              for (let k = 0; k < topic.length; k++) {
                const process = topic[k];
                this.process_law = process.process.length;
                // if (process.finishDate !== null) {
                //   const now = moment(process.finishDate);
                //   now.locale('th');
                //   const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();
                //   const convetSAccount = moment(process.finishDate).format('DD/MM') + '/' + buddhishYear;
                //   process.finishDate = convetSAccount;
                // }
              }
            }
          }
          for (let m = 0; m < this.process_law; m++) {
            this.count_law.push(m);
          }

          // --------------- End Compliance ---------------

          // --------------- Operation ---------------
          this.dataoperation = this.dataoutstanding.operation;
          this.dataoutstanding.operation['considersProgressDelete'] = [];
          this.dataoutstanding.operation['operation1ProgressDelete'] = [];
          this.dataoutstanding.operation['operationProgressDelete'] = [];
          this.dataoutstanding.operation['operation2ProgressDelete'] = [];

          // processConsiders
          let _subprocess_operationconsiders: any;
          let _processrisk_operationconsiders: any;
          for (
            let index = 0;
            index < this.dataoperation.processConsiders.length;
            index++
          ) {
            // tslint:disable-next-line: no-shadowed-variable
            const element = this.dataoperation.processConsiders[index];
            _subprocess_operationconsiders = element.subProcess;

            for (let j = 0; j < _subprocess_operationconsiders.length; j++) {
              const processoperation = _subprocess_operationconsiders[j];
              _processrisk_operationconsiders = processoperation;

              // if (processoperation.finishDate !== null) {
              //   const now = moment(processoperation.finishDate);
              //   now.locale('th');
              //   const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();
              //   const convertOperation = moment(processoperation.finishDate).format('DD/MM') + '/' + buddhishYear;
              //   processoperation.finishDate = convertOperation;
              // }
              for (
                let k = 0;
                k < _processrisk_operationconsiders.subProcessRisk.length;
                k++
              ) {
                const process =
                  _processrisk_operationconsiders.subProcessRisk[k];
                this.process_operationconsiders = process.process.length;
              }
            }
          }
          for (let m = 0; m < this.process_operationconsiders; m++) {
            this.countprocess_operationconsiders.push(m);
          }

          // operation1
          for (
            let index = 0;
            index < this.dataoperation.operation1.length;
            index++
          ) {
            // tslint:disable-next-line: no-shadowed-variable
            const element = this.dataoperation.operation1[index];
            this.process_operation1 = element.process.length;
            // if (element.finishDate !== null) {
            //   const now = moment(element.finishDate);
            //   now.locale('th');
            //   const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();
            //   const convertOperation = moment(element.finishDate).format('DD/MM') + '/' + buddhishYear;
            //   element.finishDate = convertOperation;
            // }
          }
          for (let j = 0; j < this.process_operation1; j++) {
            this.countprocess_operation1.push(j);
            //  console.log(this.countprocess_operation1);
          }

          // processOperation
          let _subprocess_processoperation: any;
          for (
            let index = 0;
            index < this.dataoperation.processOperation.length;
            index++
          ) {
            // tslint:disable-next-line: no-shadowed-variable
            const element = this.dataoperation.processOperation[index];
            _subprocess_processoperation = element.subProcess;

            for (let j = 0; j < _subprocess_processoperation.length; j++) {
              const subprocess = _subprocess_processoperation[j];
              this.process_processoperation = subprocess.process.length;
              // if (subprocess.finishDate !== null) {
              //   const now = moment(subprocess.finishDate);
              //   now.locale('th');
              //   const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();
              //   const convertOperation = moment(subprocess.finishDate).format('DD/MM') + '/' + buddhishYear;
              //   subprocess.finishDate = convertOperation;
              // }
            }
          }
          for (let k = 0; k < this.process_processoperation; k++) {
            this.countprocess_processoperation.push(k);
          }

          // operation2
          for (
            let index = 0;
            index < this.dataoperation.operation2.length;
            index++
          ) {
            // tslint:disable-next-line: no-shadowed-variable
            const element = this.dataoperation.operation2[index];
            this.process_operation2 = element.process.length;
            // if (element.finishDate !== null) {
            //   const now = moment(element.finishDate);
            //   now.locale('th');
            //   const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();
            //   const convertOperation = moment(element.finishDate).format('DD/MM') + '/' + buddhishYear;
            //   element.finishDate = convertOperation;
            // }
          }
          for (let j = 0; j < this.process_operation2; j++) {
            this.countprocess_operation2.push(j);
            // console.log(this.countprocess_operation2);
          }

          // --------------- End Operation ---------------

          // --------------- Finance ---------------
          this.datafinance = this.dataoutstanding.finance;
          this.dataoutstanding.finance['budgetProgressDelete'] = [];
          this.dataoutstanding.finance['performanceProgressDelete'] = [];
          this.dataoutstanding.finance['governanceProgressDelete'] = [];
          // budget
          for (let index = 0; index < this.datafinance.budget.length; index++) {
            // tslint:disable-next-line: no-shadowed-variable
            const element = this.datafinance.budget[index];
            this.process_budget = element.process.length;
            // if (element.finishDate !== null) {
            //   const now = moment(element.finishDate);
            //   now.locale('th');
            //   const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();
            //   const convetSAccount = moment(element.finishDate).format('DD/MM') + '/' + buddhishYear;
            //   element.finishDate = convetSAccount;
            // }
          }
          for (let j = 0; j < this.process_budget; j++) {
            this.countprocess_budget.push(j);
          }

          // performance
          for (
            let index = 0;
            index < this.datafinance.performance.length;
            index++
          ) {
            // tslint:disable-next-line: no-shadowed-variable
            const element = this.datafinance.performance[index];
            this.process_performance = element.process.length;
            // if (element.finishDate !== null) {
            //   const now = moment(element.finishDate);
            //   now.locale('th');
            //   const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();
            //   const convetSAccount = moment(element.finishDate).format('DD/MM') + '/' + buddhishYear;
            //   element.finishDate = convetSAccount;
            // }
          }
          for (let j = 0; j < this.process_performance; j++) {
            this.countprocess_performance.push(j);
          }

          // governance
          for (
            let index = 0;
            index < this.datafinance.governance.length;
            index++
          ) {
            // tslint:disable-next-line: no-shadowed-variable
            const element = this.datafinance.governance[index];
            this.process_governance = element.process.length;
            // if (element.finishDate !== null) {
            //   const now = moment(element.finishDate);
            //   now.locale('th');
            //   const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();
            //   const convetSAccount = moment(element.finishDate).format('DD/MM') + '/' + buddhishYear;
            //   element.finishDate = convetSAccount;
            // }
          }
          for (let j = 0; j < this.process_governance; j++) {
            this.countprocess_governance.push(j);
          }
          // --------------- End Finance ---------------

          // --------------- Retail ---------------

          this.dataretail = this.dataoutstanding.retail;
          this.dataoutstanding.retail['processConsidersProgressDelete'] = [];
          this.dataoutstanding.retail['retail1ProgressDelete'] = [];
          this.dataoutstanding.retail['processOperationsProgressDelete'] = [];
          this.dataoutstanding.retail['retail2ProgressDelete'] = [];
          // processConsiders
          for (
            let index = 0;
            index < this.dataretail.processConsiders.length;
            index++
          ) {
            // tslint:disable-next-line: no-shadowed-variable
            const element = this.dataretail.processConsiders[index];

            for (let j = 0; j < element.subProcess.length; j++) {
              const processretail = element.subProcess[j].subProcessRisk;
              // if (processretail.finishDate !== null) {
              //   const now = moment(processretail.finishDate);
              //   now.locale('th');
              //   const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();
              //   const convertRetail = moment(processretail.finishDate).format('DD/MM') + '/' + buddhishYear;
              //   processretail.finishDate = convertRetail;
              // }
              // console.log('processretail', processretail);

              for (let i = 0; i < processretail.length; i++) {
                const count_processretail = processretail[i].process;
                // console.log('count_processretail', count_processretail);
                this.process_retailconsiders = count_processretail.length;

                // console.log('count_processretail', count_processretail);
              }
            }
          }
          //  console.log('processretail', this.process_retailconsiders);
          for (let m = 0; m < this.process_retailconsiders; m++) {
            this.countprocess_retailconsiders.push(m);
          }
          //  console.log( this.countprocess_retailconsiders);

          // retail1

          for (let index = 0; index < this.dataretail.retail1.length; index++) {
            // tslint:disable-next-line: no-shadowed-variable
            const element = this.dataretail.retail1[index];
            this.process_retail1 = element.process.length;
            // if (element.finishDate !== null) {
            //   const now = moment(element.finishDate);
            //   now.locale('th');
            //   const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();
            //   const convertRetail = moment(element.finishDate).format('DD/MM') + '/' + buddhishYear;
            //   element.finishDate = convertRetail;
            // }
          }
          for (let j = 0; j < this.process_retail1; j++) {
            this.countprocess_retail1.push(j);
          }

          // processOperations
          let _subprocess_processretail: any;
          for (
            let index = 0;
            index < this.dataretail.processOperations.length;
            index++
          ) {
            // tslint:disable-next-line: no-shadowed-variable
            const element = this.dataretail.processOperations[index];
            _subprocess_processretail = element.subProcess;

            for (let j = 0; j < _subprocess_processretail.length; j++) {
              const subprocess = _subprocess_processretail[j];
              this.process_processretail = subprocess.process.length;
              // if (subprocess.finishDate !== null) {
              //   const now = moment(subprocess.finishDate);
              //   now.locale('th');
              //   const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();
              //   const convertRetail = moment(subprocess.finishDate).format('DD/MM') + '/' + buddhishYear;
              //   subprocess.finishDate = convertRetail;
              // }
            }
          }
          for (let k = 0; k < this.process_processretail; k++) {
            this.countprocess_processretail.push(k);
          }
          // console.log('countprocess_processretail:', this.countprocess_processretail);
          // retail2
          for (let index = 0; index < this.dataretail.retail2.length; index++) {
            // tslint:disable-next-line: no-shadowed-variable
            const element = this.dataretail.retail2[index];
            this.process_retail2 = element.process.length;
            // if (element.finishDate !== null) {
            //   const now = moment(element.finishDate);
            //   now.locale('th');
            //   const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();
            //   const convertRetail = moment(element.finishDate).format('DD/MM') + '/' + buddhishYear;
            //   element.finishDate = convertRetail;
            // }
          }
          for (let j = 0; j < this.process_retail2; j++) {
            this.countprocess_retail2.push(j);
          }

          // --------------- Retail ---------------

          // -------------Resiult------------------
          this.dataResult = this.dataoutstanding.result;
          this.dataoutstanding.result['resultProgressDelete'] = [];
          for (let index = 0; index < this.dataResult.result.length; index++) {
            // tslint:disable-next-line: no-shadowed-variable
            const element = this.dataResult.result[index];
            this.process_result = element.process.length;
            // if (element.finishDate !== null) {
            //   const now = moment(element.finishDate);
            //   now.locale('th');
            //   const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();
            //   const convertRetail = moment(element.finishDate).format('DD/MM') + '/' + buddhishYear;
            //   element.finishDate = convertRetail;
            // }
          }
          // tslint:disable-next-line: no-shadowed-variable
          for (let j = 0; j < this.process_result; j++) {
            this.countprocess_result.push(j);
          }

          // -------------Resiult------------------
        }
        this.status_loading = false;
      });
  }

  showlessRisk() {
    if (this.slrirk === true) {
      this.slrirk = false;
    } else {
      this.slrirk = true;
    }
  }
  showlessLaw() {
    if (this.sllaw === true) {
      this.sllaw = false;
    } else {
      this.sllaw = true;
    }
  }
  showlessOperating() {
    if (this.sloperating === true) {
      this.sloperating = false;
    } else {
      this.sloperating = true;
    }
  }
  showlessFinance() {
    if (this.slfinance === true) {
      this.slfinance = false;
    } else {
      this.slfinance = true;
    }
  }
  showlessCredit() {
    if (this.slcredit === true) {
      this.slcredit = false;
    } else {
      this.slcredit = true;
    }
  }
  showlessIssue() {
    if (this.slissue === true) {
      this.slissue = false;
    } else {
      this.slissue = true;
    }
  }
  showlessCheckIssue() {
    if (this.slcheckissue === true) {
      this.slcheckissue = false;
    } else {
      this.slcheckissue = true;
    }
  }

  // =====================================================Risk Delete ===================================================

  // Riskconsiders
  popUpShowRowRisk(index) {
    if (this.popUpShowRowRiskconsiders === index) {
      this.popUpShowRowRiskconsiders = null;
    } else {
      this.popUpShowRowRiskconsiders = index;
    }
  }
  RiskConsidersDeleteData(index) {

    console.log(index);
    this.dataoutstanding.risk.processConsiders.forEach((processConsiders) => {
      processConsiders.subProcess.forEach((subProcess) => {
        subProcess.subProcessRisk.forEach((subProcessRisk) => {
          // console.log(subProcessRisk);
          subProcessRisk.process[index].detail = null;
          subProcessRisk.process[index].percent = null;
        });
      });
    });
    this.popUpShowRowRiskconsiders = null;
    this.changeSaveDraft();
  }
  RiskConsidersDeleteColumn(index) {
    // console.log(index);
    this.countprocess_riskconsiders.splice(index, 1);
    // console.log(
    //   this.dataoutstanding.risk.processConsiders[0].subProcess[0].subProcessRisk[0].process[index].progress
    // );
    this.dataoutstanding.risk['processConsidersProgressDelete'].push(
      this.dataoutstanding.risk.processConsiders[0].subProcess[0]
        .subProcessRisk[0].process[index].progress
    );
    this.dataoutstanding.risk.processConsidersProgressId.splice(index, 1);
    this.dataoutstanding.risk.processConsiders.forEach((processConsiders) => {
      processConsiders.subProcess.forEach((subProcess) => {
        subProcess.subProcessRisk.forEach((subProcessRisk) => {
          console.log(subProcessRisk.process);
          subProcessRisk.process.splice(index, 1);
        });
      });
    });
    this.popUpShowRowRiskconsiders = null;
    this.changeSaveDraft();
  }

  RiskConsidersDeleteRow(index, processIndex, subProcessRiskIndex) {
    this.dataoutstanding.risk.processConsiders[index].subProcess[
      processIndex
    ].subProcessRisk[subProcessRiskIndex].process.forEach((process) => {
      process.detail = null;
      process.percent = null;
    });
    this.changeSaveDraft();
  }

  // Readiness
  popUpShowRow_Readiness(index) {
    if (this.popUpShowRowReadiness === index) {
      this.popUpShowRowReadiness = null;
    } else {
      this.popUpShowRowReadiness = index;
    }
  }
  ReadinessDeleteData(index) {
    console.log(index);
    this.dataoutstanding.risk.readiness.forEach((elements) => {
      elements.detail.forEach((sudElements) => {
        // console.log(sudElements.process[index]);
        sudElements.process[index].detail = null;
        sudElements.process[index].percent = null;
      });
    });
    this.popUpShowRowReadiness = null;
    this.changeSaveDraft();
  }
  ReadinessDeleteColumn(index) {
    // console.log(index);
    this.countprocess_readiness.splice(index, 1);
    this.dataoutstanding.risk['readinessProgressDelete'].push(
      this.dataoutstanding.risk.readiness[0].detail[0].process[index].progress
    );
    this.dataoutstanding.risk.readinessProgressId.splice(index, 1);
    this.dataoutstanding.risk.readiness.forEach((elements) => {
      elements.detail.forEach((sudElements) => {
        //  console.log(sudElements.process[index]);
        sudElements.process.splice(index, 1);
      });
    });
    this.popUpShowRowReadiness = null;
    this.changeSaveDraft();
  }
  ReadinessDeleteRow(index, detailIndex) {
    this.dataoutstanding.risk.readiness[index].detail[
      detailIndex
    ].process.forEach((process) => {
      process.detail = null;
      process.percent = null;
    });
    this.changeSaveDraft();
  }

  // extracomment
  popUpShowRow_Extracomment(index) {
    if (this.popUpShowRowExtracomment === index) {
      this.popUpShowRowExtracomment = null;
    } else {
      this.popUpShowRowExtracomment = index;
    }
  }
  ExtracommentDeleteData(index) {
    console.log(index);
    this.dataoutstanding.risk.extraComment.forEach((extraComment) => {
      // console.log(extraComment.process[index]);
      extraComment.process[index].detail = null;
      extraComment.process[index].percent = null;
    });
    this.popUpShowRowExtracomment = null;
    this.changeSaveDraft();
  }
  ExtracommentDeleteColumn(index) {
    // console.log(index);
    this.countprocess_extracomment.splice(index, 1);
    this.dataoutstanding.risk.extraCommentProgressId.splice(index, 1);
    this.dataoutstanding.risk['extraCommentProgressDelete'].push(
      this.dataoutstanding.risk.extraComment[0].process[index].progress
    );
    this.dataoutstanding.risk.extraComment.forEach((extraComment) => {
      // console.log(extraComment.process[index]);
      extraComment.process.splice(index, 1);
    });
    this.popUpShowRowExtracomment = null;
    this.changeSaveDraft();
  }
  ExtracommentDeleteRow(index) {
    this.dataoutstanding.risk.extraComment[index].process.forEach((process) => {
      process.detail = null;
      process.percent = null;
    });
    this.changeSaveDraft();
  }
  // ===================================================End Risk Delete =================================================

  // ===========================================Compliance Delete ======================================================
  // Account
  popUpShowRow_Account(index) {
    if (this.popUpShowRowAccount === index) {
      this.popUpShowRowAccount = null;
    } else {
      this.popUpShowRowAccount = index;
    }
  }
  AccountDeleteData(index) {
    // console.log(index);
    this.dataoutstanding.compliance.account.forEach((topic) => {
      topic.topic.forEach((detail) => {
        // console.log(detail.detail);
        // tslint:disable-next-line: no-shadowed-variable
        detail.detail.forEach((Sub_detail) => {
          // console.log(Sub_detail.process[index]);
          Sub_detail.process[index].detail = null;
          Sub_detail.process[index].percent = null;
        });
      });
    });
    this.popUpShowRowAccount = null;
    this.changeSaveDraft();
  }

  AccountDeleteColumn(index) {
    console.log(index);
    this.countprocess_account.splice(index, 1);
    this.dataoutstanding.compliance['accountProgressDelete'].push(
      this.dataoutstanding.compliance.account[0].topic[0].detail[0].process[
        index
      ].progress
    );
    this.dataoutstanding.compliance.accountProgressId.splice(index, 1);
    this.dataoutstanding.compliance.account.forEach((topic) => {
      topic.topic.forEach((detail) => {
        // console.log(detail.detail);
        detail.detail.forEach((item) => {
          console.log(item.process);
          item.process.splice(index, 1);
        });
      });
    });
    this.popUpShowRowAccount = null;
    this.changeSaveDraft();
  }
  AccountDeleteRow(index, topicIndex, detailIndex) {
    // console.log(
    //   this.dataoutstanding.compliance.account[index].topic[topicIndex].detail[detailIndex].process
    // );
    this.dataoutstanding.compliance.account[index].topic[topicIndex].detail[
      detailIndex
    ].process.forEach((item) => {
      item.detail = null;
      item.percent = null;
    });
    this.changeSaveDraft();
  }

  // department1
  popUpShowRow_Department1(index) {
    if (this.popUpShowRowDepartment1 === index) {
      this.popUpShowRowDepartment1 = null;
    } else {
      this.popUpShowRowDepartment1 = index;
    }
  }
  Department1DeleteData(index) {
    this.dataoutstanding.compliance.department1.forEach((topic) => {
      topic.topic.forEach((detail) => {
        detail.detail.forEach((proces) => {
          // console.log(proces);
          proces.process[index].detail = null;
          proces.process[index].percent = null;
        });
      });
    });
    this.popUpShowRowDepartment1 = null;
    this.changeSaveDraft();
  }
  Department1DeleteColumn(index) {
    this.count_department1_progress.splice(index, 1);
    this.dataoutstanding.compliance['department1ProgressDelete'].push(
      this.dataoutstanding.compliance.department1[0].topic[0].detail[0].process[
        index
      ].progress
    );
    this.dataoutstanding.compliance.department1ProgressId.splice(index, 1);
    this.dataoutstanding.compliance.department1.forEach((topic) => {
      topic.topic.forEach((detail) => {
        // console.log(detail.detail)
        detail.detail.forEach((process) => {
          process.process.splice(index, 1);
        });
      });
    });
    this.popUpShowRowDepartment1 = null;
    this.changeSaveDraft();
  }
  Department1DeleteRow(index, topicIndex, detailIndex) {
    this.dataoutstanding.compliance.department1[index].topic[topicIndex].detail[
      detailIndex
    ].process.forEach((item) => {
      item.detail = null;
      item.percent = null;
    });
    this.changeSaveDraft();
  }

  // department2
  popUpShowRow_Department2(index) {
    if (this.popUpShowRowDepartment2 === index) {
      this.popUpShowRowDepartment2 = null;
    } else {
      this.popUpShowRowDepartment2 = index;
    }

  }
  Department2DeleteData(index) {
    this.dataoutstanding.compliance.department2.forEach((topic) => {
      topic.topic.forEach((detail) => {
        detail.detail.forEach((proces) => {
          // console.log(proces);
          proces.process[index].detail = null;
          proces.process[index].percent = null;
        });
      });
    });
    this.popUpShowRowDepartment2 = null;
    this.changeSaveDraft();
  }
  Department2DeleteColumn(index) {
    this.count_department2_progress.splice(index, 1);
    this.dataoutstanding.compliance['department2ProgressDelete'].push(
      this.dataoutstanding.compliance.department2[0].topic[0].detail[0].process[
        index
      ].progress
    );
    this.dataoutstanding.compliance.department2ProgressId.splice(index, 1);
    this.dataoutstanding.compliance.department2.forEach((topic) => {
      topic.topic.forEach((detail) => {
        // console.log(detail.detail)
        detail.detail.forEach((process) => {
          process.process.splice(index, 1);
        });
      });
    });
    this.popUpShowRowDepartment2 = null;
    this.changeSaveDraft();
  }
  Department2DeleteRow(index, topicIndex, detailIndex) {
    this.dataoutstanding.compliance.department2[index].topic[topicIndex].detail[
      detailIndex
    ].process.forEach((item) => {
      item.detail = null;
      item.percent = null;
    });
    this.changeSaveDraft();
  }

  // Rule
  popUpShowRow_Rule(index) {
    if (this.popUpShowRowRule === index) {
      this.popUpShowRowRule = null;
    } else {
      this.popUpShowRowRule = index;
    }
  }
  RuleDeleteData(index) {
    this.dataoutstanding.compliance.rule.forEach((elements) => {
      elements.topic.forEach((sudElements) => {
        sudElements.detail.forEach((sudProcess) => {
          sudProcess.process[index].detail = null;
          sudProcess.process[index].percent = null;
        });
      });
    });
    this.popUpShowRowRule = null;
    this.changeSaveDraft();
  }
  RuleDeleteColumn(index) {
    this.count_rule.splice(index, 1);
    this.dataoutstanding.compliance['ruleProgressDelete'].push(
      this.dataoutstanding.compliance.rule[0].topic[0].detail[0].process[index]
        .progress
    );
    this.dataoutstanding.compliance.ruleProgressId.splice(index, 1);
    this.dataoutstanding.compliance.rule.forEach((topic) => {
      topic.topic.forEach((detail) => {
        // console.log(detail.detail)
        detail.detail.forEach((process) => {
          process.process.splice(index, 1);
        });
      });
    });
    this.popUpShowRowRule = null;
    this.changeSaveDraft();
  }
  RuleDeleteRow(index, topicIndex, detailIndex) {
    this.dataoutstanding.compliance.rule[index].topic[topicIndex].detail[
      detailIndex
    ].process.forEach((item) => {
      item.detail = null;
      item.percent = null;
    });
    this.changeSaveDraft();
  }

  // Law
  popUpShowRow_Law(index) {
    if (this.popUpShowRowLaw === index) {
      this.popUpShowRowLaw = null;
    } else {
      this.popUpShowRowLaw = index;
    }
  }
  LawDeleteData(index) {
    this.dataoutstanding.compliance.law.forEach((elements) => {
      elements.topic.forEach((sudElements) => {
        sudElements.detail.forEach((sudProcess) => {
          sudProcess.process[index].detail = null;
          sudProcess.process[index].percent = null;
        });
      });
    });
    this.popUpShowRowLaw = null;
    this.changeSaveDraft();
  }
  LawDeleteColumn(index) {
    this.count_law.splice(index, 1);
    this.dataoutstanding.compliance['lawProgressDelete'].push(
      this.dataoutstanding.compliance.law[0].topic[0].detail[0].process[index]
        .progress
    );
    this.dataoutstanding.compliance.lawProgressId.splice(index, 1);
    this.dataoutstanding.compliance.law.forEach((topic) => {
      topic.topic.forEach((detail) => {
        // console.log(detail.detail)
        detail.detail.forEach((process) => {
          process.process.splice(index, 1);
        });
      });
    });
    this.popUpShowRowLaw = null;
    this.changeSaveDraft();
  }
  LawDeleteRow(index, topicIndex, detailIndex) {
    this.dataoutstanding.compliance.law[index].topic[topicIndex].detail[
      detailIndex
    ].process.forEach((item) => {
      item.detail = null;
      item.percent = null;
    });
    this.changeSaveDraft();
  }
  // =======================================End Compliance Delete =====================================================

  // =================================================== Operation Delete ================================================
  // operation.processConsiders
  operationConsidersPopUpRow(index) {
    if (this.operationConsidersPopUpShowRow === index) {
      this.operationConsidersPopUpShowRow = null;
    } else {
      this.operationConsidersPopUpShowRow = index;
    }
  }

  operationConsidersDeleteData(j: any) {
    // tslint:disable-next-line: no-shadowed-variable
    this.dataoutstanding.operation.processConsiders.forEach((element) => {
      element.subProcess.forEach((sub) => {
        sub.subProcessRisk.forEach((process) => {
          process.process[j].detail = null;
          process.process[j].percent = null;
        });
      });
    });
    this.operationConsidersPopUpShowRow = null;
    this.changeSaveDraft();
  }

  operationConsidersDeleteColumn(j: any) {
    // console.log(this.dataoutstanding.operation.processConsiders[0].subProcess[0].subProcessRisk[0].process[j]);
    this.dataoutstanding.operation['considersProgressDelete'].push(
      this.dataoutstanding.operation.processConsiders[0].subProcess[0]
        .subProcessRisk[0].process[j].progress
    );
    // tslint:disable-next-line: no-shadowed-variable
    this.dataoutstanding.operation.processConsiders.forEach((element) => {
      element.subProcess.forEach((sub) => {
        sub.subProcessRisk.forEach((process) => {
          process.process.splice(j, 1);
        });
      });
    });
    this.dataoutstanding.operation.processConsidersId.splice(j, 1);
    this.countprocess_operationconsiders.splice(j, 1);
    this.operationConsidersPopUpShowRow = null;
    this.changeSaveDraft();
    // console.log(this.dataoutstanding.operation['considersProgressDelete']);
  }

  operationConsidersDeleteRow(index, processIndex, subProcessRiskIndex) {
    this.dataoutstanding.operation.processConsiders[index].subProcess[
      processIndex
    ].subProcessRisk[subProcessRiskIndex].process.forEach((process) => {
      process.detail = null;
      process.percent = null;
    });
    this.changeSaveDraft();
  }

  // operation1
  operation1PopUpRow(index) {
    if (this.operation1PopUpShowRow === index) {
      this.operation1PopUpShowRow = null;
    } else {
      this.operation1PopUpShowRow = index;
    }
  }
  operation1DeleteData(j: any) {
    // tslint:disable-next-line: no-shadowed-variable
    this.dataoutstanding.operation.operation1.forEach((element) => {
      element.process[j].detail = null;
      element.process[j].percent = null;
    });
    this.operation1PopUpShowRow = null;
    this.changeSaveDraft();
  }

  operation1DeleteColumn(j: any) {
    // console.log(this.dataoutstanding.operation.operation1[0].process[j]);
    this.dataoutstanding.operation['operation1ProgressDelete'].push(
      this.dataoutstanding.operation.operation1[0].process[j].progress
    );
    // tslint:disable-next-line: no-shadowed-variable
    this.dataoutstanding.operation.operation1.forEach((element) => {
      element.process.splice(j, 1);
    });
    this.dataoutstanding.operation.processOperation1Id.splice(j, 1);
    this.countprocess_operation1.splice(j, 1);
    this.operation1PopUpShowRow = null;
    this.changeSaveDraft();
    // console.log(this.dataoutstanding.operation['operation1ProgressDelete']);
  }

  operation1DeleteRow(index) {
    this.dataoutstanding.operation.operation1[index].process.forEach(
      (process) => {
        process.detail = null;
        process.percent = null;
      }
    );
    this.changeSaveDraft();
  }

  // processOperation
  processOperationPopUpRow(index) {
    if (this.processOperationPopUpShowRow === index) {
      this.processOperationPopUpShowRow = null;
    } else {
      this.processOperationPopUpShowRow = index;
    }
  }

  processOperationDeleteData(j: any) {
    // tslint:disable-next-line: no-shadowed-variable
    this.dataoutstanding.operation.processOperation.forEach((element) => {
      element.subProcess.forEach((sub) => {
        sub.process[j].detail = null;
        sub.process[j].percent = null;
      });
    });
    this.processOperationPopUpShowRow = null;
    this.changeSaveDraft();
  }

  processOperationDeleteColumn(j: any) {
    // console.log(this.dataoutstanding.operation.processOperation[0].subProcess[0].process[j]);
    this.dataoutstanding.operation['operationProgressDelete'].push(
      this.dataoutstanding.operation.processOperation[0].subProcess[0].process[
        j
      ].progress
    );
    // tslint:disable-next-line: no-shadowed-variable
    this.dataoutstanding.operation.processOperation.forEach((element) => {
      element.subProcess.forEach((sub) => {
        sub.process.splice(j, 1);
      });
    });
    this.dataoutstanding.operation.processOperationId.splice(j, 1);
    this.countprocess_processoperation.splice(j, 1);
    this.processOperationPopUpShowRow = null;
    this.changeSaveDraft();
    // console.log(this.dataoutstanding.operation['operationProgressDelete']);
  }
  processOperationDeleteRow(index, processIndex) {
    this.dataoutstanding.operation.processOperation[index].subProcess[
      processIndex
    ].process.forEach((process) => {
      process.detail = null;
      process.percent = null;
    });
    this.changeSaveDraft();
  }

  // operation2
  operation2PopUpRow(index) {
    if (this.operation2PopUpShowRow === index) {
      this.operation2PopUpShowRow = null;
    } else {
      this.operation2PopUpShowRow = index;
    }
  }
  operation2DeleteData(j: any) {
    // tslint:disable-next-line: no-shadowed-variable
    this.dataoutstanding.operation.operation2.forEach((element) => {
      element.process[j].detail = null;
      element.process[j].percent = null;
    });
    this.operation2PopUpShowRow = null;
    this.changeSaveDraft();
  }

  operation2DeleteColumn(j: any) {
    // console.log(this.dataoutstanding.operation.operation2[0].process[j]);
    this.dataoutstanding.operation['operation2ProgressDelete'].push(
      this.dataoutstanding.operation.operation2[0].process[j].progress
    );
    // tslint:disable-next-line: no-shadowed-variable
    this.dataoutstanding.operation.operation2.forEach((element) => {
      element.process.splice(j, 1);
    });
    this.dataoutstanding.operation.processOperation2Id.splice(j, 1);
    this.countprocess_operation2.splice(j, 1);
    this.operation2PopUpShowRow = null;
    this.changeSaveDraft();
    // console.log(this.dataoutstanding.operation['operation2ProgressDelete']);
  }

  operation2DeleteRow(index) {
    this.dataoutstanding.operation.operation2[index].process.forEach(
      (process) => {
        process.detail = null;
        process.percent = null;
      }
    );
    this.changeSaveDraft();
  }

  // =================================================== End Operation Delete============================================

  // =====================================================retail Delete===============================================//
  popUpRow(index) {
    //   console.log('popUpRow', index);
    if (this.popUpShowRowRetail === index) {
      this.popUpShowRowRetail = null;
    } else {
      this.popUpShowRowRetail = index;
    }
  }
  // ====retail.processConsiders ===
  RetaildeleteData(j: any) {
    console.log('index:', j);
    // tslint:disable-next-line: no-shadowed-variable
    this.dataoutstanding.retail.processConsiders.forEach((element) => {
      element.subProcess.forEach((sub) => {
        sub.subProcessRisk.forEach((process) => {
          // console.log(process.process);
          process.process[j].detail = null;
          process.process[j].percent = null;
        });
      });
    });
    this.popUpShowRowRetail = null;
    this.changeSaveDraft();
  }

  RetaildeleteColumn(j: any) {
    console.log('index:', j);
    console.log(
      this.dataoutstanding.retail.processConsiders[0].subProcess[0]
        .subProcessRisk[0].process[j]
    );
    this.dataoutstanding.retail['processConsidersProgressDelete'].push(
      this.dataoutstanding.retail.processConsiders[0].subProcess[0]
        .subProcessRisk[0].process[j].progress
    );

    for (
      let i = 0;
      i < this.dataoutstanding.retail.processConsidersProgressId.length;
      i++
    ) {
      const Id = this.dataoutstanding.retail.processConsidersProgressId[i];
      if (
        this.dataoutstanding.retail.processConsiders[0].subProcess[0]
          .subProcessRisk[0].process[j].progress === Id
      ) {
        this.dataoutstanding.retail.processConsidersProgressId.splice(i, 1);
      }
    }
    // tslint:disable-next-line: no-shadowed-variable
    this.dataoutstanding.retail.processConsiders.forEach((element) => {
      element.subProcess.forEach((sub) => {
        sub.subProcessRisk.forEach((process) => {
          // this.dataoutstanding.retail.processConsidersProgressDelete.push(process.process[j].progress);
          process.process.splice(j, 1);
        });
      });
    });
    this.countprocess_retailconsiders.splice(j, 1);
    this.popUpShowRowRetail = null;
    this.changeSaveDraft();
    // console.log(this.dataoutstanding.retail['processConsidersProgressDelete']);
  }
  RetailDeleteRow(index, processIndex, subProcessRiskIndex) {
    // console.log('index', index);
    // console.log('processIndex', processIndex);
    // console.log('subProcessRiskIndex', subProcessRiskIndex);
    this.dataoutstanding.retail.processConsiders[index].subProcess[
      processIndex
    ].subProcessRisk[subProcessRiskIndex].process.forEach((process) => {
      //  console.log(process);
      process.detail = null;
      process.percent = null;
    });
    this.changeSaveDraft();
  }
  // ==== End retail.processConsiders ===

  // ======retail.retail1 ==========
  popUpRowRetail1(index: any) {
    if (this.popUpShowRowRetail_retail1 === index) {
      this.popUpShowRowRetail_retail1 = null;
    } else {
      this.popUpShowRowRetail_retail1 = index;
    }
  }
  RetaildeleteDataRetail1(index: any) {
    // console.log(index);
    this.dataoutstanding.retail.retail1.forEach((elements) => {
      elements.process[index].detail = null;
      elements.process[index].percent = null;
    });
    this.popUpShowRowRetail_retail1 = null;
    this.changeSaveDraft();
  }
  RetaildeleteColumnRetail1(column: any) {
    this.dataoutstanding.retail['retail1ProgressDelete'].push(
      this.dataoutstanding.retail.retail1[0].process[column].progress
    );
    //  console.log(this.dataoutstanding.retail['retail1ProgressDelete']);

    for (
      let i = 0;
      i < this.dataoutstanding.retail.retail1ProgressId.length;
      i++
    ) {
      const Id = this.dataoutstanding.retail.retail1ProgressId[i];
      if (
        this.dataoutstanding.retail.retail1[0].process[column].progress === Id
      ) {
        this.dataoutstanding.retail.retail1ProgressId.splice(i, 1);
      }
    }

    this.dataoutstanding.retail.retail1.forEach((elements) => {
      elements.process.splice(column, 1);
    });
    this.countprocess_retail1.splice(column, 1);
    this.popUpShowRowRetail_retail1 = null;
    this.changeSaveDraft();
    // console.log(this.dataoutstanding.retail.retail1);
  }

  RetailDeleteRowRetail1(index: any) {
    // console.log(index);

    this.dataoutstanding.retail.retail1[index].process.forEach((elements) => {
      // console.log(elements);
      elements.detail = null;
      elements.percent = null;
    });
    this.changeSaveDraft();
  }
  // ======End retail.retail1 ==========

  // ===========retail.processOperations===============

  popUpShowRowRetailProcessRetail(index: any) {
    if (this.popUpShowRowRetail_processRetail === index) {
      this.popUpShowRowRetail_processRetail = null;
    } else {
      this.popUpShowRowRetail_processRetail = index;
    }
  }
  RetaildeleteDataProcessRetail(index: any) {
    // console.log(index);
    // tslint:disable-next-line: no-shadowed-variable
    this.dataoutstanding.retail.processOperations.forEach((element) => {
      element.subProcess.forEach((sub) => {
        // console.log(sub.process[index]);
        sub.process[index].detail = null;
        sub.process[index].percent = null;
      });
    });
    this.popUpShowRowRetail_processRetail = null;
    this.changeSaveDraft();
  }
  RetaildeleteColumnProcessRetail(index: any) {
    console.log(index);

    this.dataoutstanding.retail['processOperationsProgressDelete'].push(
      this.dataoutstanding.retail.processOperations[0].subProcess[0].process[
        index
      ].progress
    );
    console.log(this.dataoutstanding.retail['processOperationsProgressDelete']);

    for (
      let i = 0;
      i < this.dataoutstanding.retail.processOperationsProgressId.length;
      i++
    ) {
      const Id = this.dataoutstanding.retail.processOperationsProgressId[i];
      if (
        this.dataoutstanding.retail.processOperations[0].subProcess[0].process[
          index
        ].progress === Id
      ) {
        this.dataoutstanding.retail.processOperationsProgressId.splice(i, 1);
      }
    }

    this.countprocess_processretail.splice(index, 1);
    // tslint:disable-next-line: no-shadowed-variable
    this.dataoutstanding.retail.processOperations.forEach((element) => {
      element.subProcess.forEach((sub) => {
        // console.log(sub.process[index]);

        sub.process.splice(index, 1);
      });
    });
    this.popUpShowRowRetail_processRetail = null;
    this.changeSaveDraft();
  }

  RetailDeleteRowRetailProcess(index, processIndex) {
    // console.log(index);
    // console.log(processIndex);
    this.dataoutstanding.retail.processOperations[index].subProcess.forEach(
      // tslint:disable-next-line: no-shadowed-variable
      (element) => {
        element.process.forEach((process) => {
          // console.log(process);
          process.detail = null;
          process.percent = null;
        });
      }
    );
    this.changeSaveDraft();
  }
  // ============End retail.processOperations ===============

  // ==============retail reatail2 ========================
  popUpRowRetail2(index: any) {
    if (this.popUpShowRowRetail_retail2 === index) {
      this.popUpShowRowRetail_retail2 = null;
    } else {
      this.popUpShowRowRetail_retail2 = index;
    }
  }
  RetaildeleteDataRetail2(index: any) {
    // console.log(index);
    this.dataoutstanding.retail.retail2.forEach((elements) => {
      elements.process[index].detail = null;
      elements.process[index].percent = null;
    });
    this.popUpShowRowRetail_retail2 = null;
    this.changeSaveDraft();
  }
  RetaildeleteColumnRetail2(column: any) {
    // console.log(column);
    this.dataoutstanding.retail['retail2ProgressDelete'].push(
      this.dataoutstanding.retail.retail2[0].process[column].progress
    );
    console.log(this.dataoutstanding.retail['retail2ProgressDelete']);

    for (
      let i = 0;
      i < this.dataoutstanding.retail.retail2ProgressId.length;
      i++
    ) {
      const Id = this.dataoutstanding.retail.retail2ProgressId[i];
      if (
        this.dataoutstanding.retail.retail2[0].process[column].progress === Id
      ) {
        this.dataoutstanding.retail.retail2ProgressId.splice(i, 1);
      }
    }

    this.countprocess_retail2.splice(column, 1);
    this.dataoutstanding.retail.retail2.forEach((elements) => {
      elements.process.splice(column, 1);
    });
    this.popUpShowRowRetail_retail2 = null;
    this.changeSaveDraft();
    // console.log(this.dataoutstanding.retail.retail1);
  }

  RetailDeleteRowRetail2(index: any) {
    // console.log(index);

    this.dataoutstanding.retail.retail2[index].process.forEach((elements) => {
      // console.log(elements);
      elements.detail = null;
      elements.percent = null;
    });
    this.changeSaveDraft();
  }
  // =============End retail.retail2 ======================

  // =============================================End retail Delete=================================================//

  // ==================================================Result Delete===============================================//
  popUpRowResult(index: any) {
    if (this.popUpShowRowResult === index) {
      this.popUpShowRowResult = null;
    } else {
      this.popUpShowRowResult = index;
    }
  }
  RetaildeleteDataResult(index: any) {
    //  console.log(index);
    this.dataoutstanding.result.result.forEach((elements) => {
      // console.log(  elements.process[index]);
      elements.process[index].detail = null;
      elements.process[index].percent = null;
    });
    this.popUpShowRowResult = null;
    this.changeSaveDraft();
  }
  RetaildeleteColumnResult(index: any) {
    console.log(index);
    this.dataoutstanding.result['resultProgressDelete'].push(
      this.dataoutstanding.result.result[0].process[index].progress
    );

    for (
      let i = 0;
      i < this.dataoutstanding.result.resultProgressId.length;
      i++
    ) {
      const Id = this.dataoutstanding.result.resultProgressId[i];
      if (
        this.dataoutstanding.result.result[0].process[index].progress === Id
      ) {
        this.dataoutstanding.result.resultProgressId.splice(i, 1);
      }
    }
    // console.log(this.dataoutstanding.retail['resultProgressDelete']);
    this.countprocess_result.splice(index, 1);
    this.dataoutstanding.result.result.forEach((elements) => {
      // console.log(sub.process[index]);
      elements.process.splice(index, 1);
    });
    this.popUpShowRowResult = null;
    this.changeSaveDraft();
  }

  RetailDeleteRowResult(index: any) {
    // console.log(index);
    this.dataoutstanding.result.result[index].process.forEach((elements) => {
      // console.log(elements);
      elements.detail = null;
      elements.percent = null;
    });
    this.changeSaveDraft();
  }
  // ================================================End Result Delete==============================================//

  // ==========================================finance Delete====================================================
  // ====finance.budget ===
  popupRowbudget(index) {
    if (this.popUpShowRowbudget === index) {
      this.popUpShowRowbudget = null;
    } else {
      this.popUpShowRowbudget = index;
    }
  }
  BudgetdeleteData(j: any) {
    console.log('index', j);
    // tslint:disable-next-line: no-shadowed-variable
    this.dataoutstanding.finance.budget.forEach((element) => {
      console.log(element.process[j]);
      element.process[j].detail = null;
      element.process[j].percent = null;
    });
    this.popUpShowRowbudget = null;
    this.changeSaveDraft();
  }
  BudgetdeleteColumn(j: any) {
    // console.log('index:', j);
    // console.log(this.dataoutstanding.finance.budget[0].process[j]);
    this.dataoutstanding.finance['budgetProgressDelete'].push(
      this.dataoutstanding.finance.budget[0].process[j].progress
    );
    // tslint:disable-next-line: no-shadowed-variable
    this.dataoutstanding.finance.budget.forEach((element) => {
      element.process.splice(j, 1);
    });
    this.dataoutstanding.finance.budgetProgressId.splice(j, 1);
    this.countprocess_budget.splice(j, 1);
    this.popUpShowRowbudget = null;
    this.changeSaveDraft();
    // console.log(this.dataoutstanding.finance['budgetProgressDelete']);
  }
  BudgetDeleteRow(index) {
    // console.log(processIndex);
    console.log('index', index);
    this.dataoutstanding.finance.budget[index].process.forEach((process) => {
      //  console.log(process);
      process.detail = null;
      process.percent = null;
    });
    this.changeSaveDraft();
  }
  // ==== End finance.budget ===

  // ==== finance.performance ===
  popupRowperformance(index) {
    if (this.popUpShowRowperformance === index) {
      this.popUpShowRowperformance = null;
    } else {
      this.popUpShowRowperformance = index;
    }
  }
  PerformancedeleteData(j: any) {
    console.log('index', j);
    // tslint:disable-next-line: no-shadowed-variable
    this.dataoutstanding.finance.performance.forEach((element) => {
      console.log(element.process[j]);
      element.process[j].detail = null;
      element.process[j].percent = null;
    });
    this.popUpShowRowperformance = null;
    this.changeSaveDraft();
  }
  PerformancedeleteColumn(j: any) {
    console.log('index:', j);
    console.log(this.dataoutstanding.finance.performance[0].process[j]);
    this.dataoutstanding.finance['performanceProgressDelete'].push(
      this.dataoutstanding.finance.performance[0].process[j].progress
    );
    // tslint:disable-next-line: no-shadowed-variable
    this.dataoutstanding.finance.performance.forEach((element) => {
      // element.process.forEach((process) => {
      element.process.splice(j, 1);
      // });
    });
    this.dataoutstanding.finance.performanceProgressId.splice(j, 1);
    this.countprocess_performance.splice(j, 1);
    this.popUpShowRowperformance = null;
    this.changeSaveDraft();
    // console.log(this.dataoutstanding.finance['performanceProgressDelete']);
  }
  PerformanceDeleteRow(index) {
    // console.log(processIndex);
    // console.log('index', index);
    this.dataoutstanding.finance.performance[index].process.forEach(
      (process) => {
        //  console.log(process);
        process.detail = null;
        process.percent = null;
      }
    );
    this.changeSaveDraft();
  }
  // ==== End finance.performance ===

  // ==== finance.governance ===
  popupRowgovernance(index) {
    if (this.popUpShowRowgovernance === index) {
      this.popUpShowRowgovernance = null;
    } else {
      this.popUpShowRowgovernance = index;
    }
  }
  GovernancedeleteData(j: any) {
    // console.log('index', j);
    // tslint:disable-next-line: no-shadowed-variable
    this.dataoutstanding.finance.governance.forEach((element) => {
      console.log(element.process[j]);
      element.process[j].detail = null;
      element.process[j].percent = null;
    });
    this.popUpShowRowgovernance = null;
    this.changeSaveDraft();
  }
  GovernancedeleteColumn(j: any) {
    console.log('index:', j);
    console.log(this.dataoutstanding.finance.governance[0].process[j]);
    this.dataoutstanding.finance['governanceProgressDelete'].push(
      this.dataoutstanding.finance.governance[0].process[j].progress
    );
    // tslint:disable-next-line: no-shadowed-variable
    this.dataoutstanding.finance.governance.forEach((element) => {
      // element.process.forEach((process) => {
      element.process.splice(j, 1);
      // });
    });
    this.dataoutstanding.finance.governanceProgressId.splice(j, 1);
    this.countprocess_governance.splice(j, 1);
    this.popUpShowRowgovernance = null;
    this.changeSaveDraft();
    // console.log(this.dataoutstanding.finance['governanceProgressDelete']);
  }
  GovernanceDeleteRow(index) {
    // console.log(processIndex);
    // console.log('index',index);
    this.dataoutstanding.finance.governance[index].process.forEach(
      (process) => {
        //  console.log(process);
        process.detail = null;
        process.percent = null;
      }
    );
    this.changeSaveDraft();
  }
  // ==== End finance.governance ===
  // ==========================================End finance Delete====================================================

  // ========================== สอบทานการปิดประเด็นคงค้าง/สิ่งที่ต้องดำเนินการ ===============================

  getCollectIssue() {
    let _data: any;

    this.outstandingService
      .getCollectIssue(Number(localStorage.getItem('requestId')))
      .subscribe((res) => {
        if (res['status'] === 'success' && res['data']) {
          _data = res['data'];
          console.log('_data', _data);
          this.editText = _data.editText;
          if (_data.boardApprove === true && _data.edit === false) {
            this.selectradio = 'เป็นไปตามมติคณะกรรมการผลิตภัณฑ์';
          } else if (_data.edit === true && _data.boardApprove === false) {
            this.selectradio = 'ส่งกลับแก้ไข';
          }
        }
      });
  }

  changeSelectRadio() {
    if (this.selectradio === 'เป็นไปตามมติคณะกรรมการผลิตภัณฑ์') {
      this.editText = '';
    }
  }

  // ========================== End สอบทานการปิดประเด็นคงค้าง/สิ่งที่ต้องดำเนินการ ===============================
  rowClick(Row: any, e: any, index: any) {
    // auto height risk.processConsider
    // console.log(e);
    // console.log(index);
    const id = document.getElementById(String(index) + 'div');
    id.style.height = (e.target.clientHeight * Row) + 'px';
  }

  autoGrowTextZone(e) {
    e.target.style.height = '0px';
    e.target.style.overflow = 'hidden';
    e.target.style.height = (e.target.scrollHeight + 0) + 'px';
  }
}
