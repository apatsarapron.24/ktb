import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OutstandingRoutingModule } from './outstanding-routing.module';
import { OutstandingComponent } from './outstanding.component';
import { MatRadioModule } from '@angular/material/radio';
import { FormsModule } from '@angular/forms';
import { DateModule } from '../datepicker/date/date.module';
import { MatDialogModule } from '@angular/material';
import { TextareaAutoresizeDirectiveModule } from '../../textarea-autoresize.directive/textarea-autoresize.directive.module';
import { CanDeactivateGuard } from '../../services/configGuard/config-guard.service';
import { DialogSaveStatusComponent } from '../outstanding/dialog-save-status/dialog-save-status.component';

@NgModule({
  declarations: [
    OutstandingComponent,
    DialogSaveStatusComponent
  ],
  imports: [
    MatDialogModule,
    CommonModule,
    OutstandingRoutingModule,
    MatRadioModule,
    FormsModule,
    DateModule,
    TextareaAutoresizeDirectiveModule
  ],
  providers: [CanDeactivateGuard],
  entryComponents: [DialogSaveStatusComponent]
})
export class OutstandingModule { }
