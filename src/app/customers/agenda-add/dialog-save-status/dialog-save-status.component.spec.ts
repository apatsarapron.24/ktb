import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogSaveStatusComponent } from './dialog-save-status.component';

describe('DialogSaveStatusComponent', () => {
  let component: DialogSaveStatusComponent;
  let fixture: ComponentFixture<DialogSaveStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogSaveStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogSaveStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
