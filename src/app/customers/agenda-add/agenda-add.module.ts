import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AgendaAddRoutingModule } from './agenda-add-routing.module';
import { AgendaAddComponent} from './agenda-add.component'
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule} from '@angular/material/radio';
import { MatSelectModule} from '@angular/material/select';
import { FormsModule } from '@angular/forms';
import { MatButtonModule, MatTableModule, MatDialogModule } from '@angular/material';
import { CanDeactivateGuard } from '../../services/configGuard/config-guard.service';
import { DialogSaveStatusComponent } from '../agenda-add/dialog-save-status/dialog-save-status.component';
@NgModule({
  declarations: [AgendaAddComponent, DialogSaveStatusComponent],
  imports: [
    CommonModule,
    AgendaAddRoutingModule,
    MatCheckboxModule,
    MatRadioModule,
    MatSelectModule,
    FormsModule,
    MatDialogModule
  ],
  providers: [CanDeactivateGuard],
  entryComponents: [DialogSaveStatusComponent]
})
export class AgendaAddModule { }
