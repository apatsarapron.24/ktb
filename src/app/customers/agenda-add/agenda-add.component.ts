import { Component, OnInit } from '@angular/core';
import { AgendaAddService } from '../../services/agenda/agenda-add/agenda-add.service';
import { DialogSaveStatusComponent } from '../agenda-add/dialog-save-status/dialog-save-status.component'
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { RequestService } from '../../services/request/request.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
declare var $: any;

@Component({
  selector: 'app-agenda-add',
  templateUrl: './agenda-add.component.html',
  styleUrls: ['./agenda-add.component.scss']
})
export class AgendaAddComponent implements OnInit {
  selectedData: string;
  processSelected = null;
  processtest: number;
  meeting: number = null;
  processdropdown = [];
  textdetail: string;
  confirmState = 'confirm';
  sendrewardData = false;
  agendaData: string;
  isreturn = null;
  canEdit = null;
  saveDraftstatus_State = false;
  backDashboard = null
  constructor(
    private router: Router,
    private Sidebar: RequestService,
    private agendaService: AgendaAddService,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.processdropdown = [];
    this.getAgenda();
    // this.getDropdownMeettingList();
  }


  getDropdownMeettingList(meeting) {
    this.agendaService.getDropdownMeettingList().subscribe(res => {
      // this.processSelected = res;
      // console.log(this.processSelected);
      if (res['status'] === 'success' && res['data']) {
        this.processSelected = res['data'];
        this.processSelected.dropdownMeettingList.forEach(value => {
          if (value.isDropdown === true || value.id === meeting) {
            this.processdropdown.push(value)
          }
        })
      }

      console.log(this.processdropdown);
    });
  }

  getAgenda() {
    this.Sidebar.sendEvent();
    const requestid = {
      'requestId': localStorage.getItem('requestId'),
    };
    this.agendaService.getAgenda(requestid).subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        const agendaData = res['data'];
        console.log(agendaData);
        this.meeting = res['data'].meetingId;
        this.isreturn = agendaData['isReturn'];
        this.canEdit = !res['canEdit'];
        if (this.isreturn === true) {
          this.selectedData = 'ส่งคืน';
          this.processtest = agendaData['meetingId'];
          this.textdetail = agendaData['detail'];
        } else if (this.isreturn === false) {
          this.selectedData = agendaData['agenda'];
          this.processtest = agendaData['meetingId'];
          this.textdetail = agendaData['detail'];
        }

      }

      if (this.meeting !== null) {
        this.getDropdownMeettingList(this.processtest);
      } else {
        this.getDropdownMeettingList(null)
      }
    });
  }



  SaveData() {

    if (this.selectedData === 'ส่งคืน') {
      if (this.textdetail === null || this.textdetail === '') {
        alert('กรุณากรอกรายละเอียดเพิ่มเติม');
      } else {
        this.sendrewardData = true;
        const data = {
          'meetingId': this.processtest,
          'requestId': localStorage.getItem('requestId'),
          'agenda': 'ส่งคืน',
          'detail': this.textdetail,
          'isReturn': this.sendrewardData,
        };
        console.log('Save: ', data);
        this.agendaService.productMeetingAgenda(data).subscribe(res => {
          console.log(res);
          if (res['status'] === 'success') {
            this.saveDraftstatus_State = false;
            Swal.fire({
              title: 'success',
              text: 'บันทึกเรียบร้อย',
              icon: 'success',
              showConfirmButton: false,
              timer: 2000
            }).then(() => {
              if (this.backDashboard !== null) {
                this.router.navigate(['/dashboard1']);
              }
              else {
                this.router.navigate([this.Sidebar.goToLink]);
              }
            })
            // this.getAgenda();
          } else {
            Swal.fire({
              title: 'error',
              text: res['message'],
              icon: 'error',
            });
          }
        });
      }
    } else {
      const data = {
        'meetingId': this.processtest,
        'requestId': localStorage.getItem('requestId'),
        'agenda': this.selectedData,
        'detail': this.textdetail,
        'isReturn': this.sendrewardData,
      };
      console.log('Save: ', data);
      this.agendaService.productMeetingAgenda(data).subscribe(res => {
        console.log(res);
        if (res['status'] === 'success') {
          this.saveDraftstatus_State = false;
          // this.getAgenda();
          Swal.fire({
            title: 'success',
            text: 'บันทึกเรียบร้อย',
            icon: 'success',
            showConfirmButton: false,
            timer: 2000,
          }).then(() => {
            if (this.backDashboard !== null) {
              this.router.navigate(['/dashboard1']);
            }
            else {
              this.router.navigate([this.Sidebar.goToLink]);
            }
          })
          this.saveDraftstatus_State = false;
          console.log('>>>>>>>>>>>>>>>>>')

          // this.getAgenda();
        } else {
          Swal.fire({
            title: 'error',
            text: res['message'],
            icon: 'error',
          });
        }
      });
    }
  }

  checkmodelForward() {
    if (this.selectedData === 'ส่งคืน') {
      if (this.textdetail === null || this.textdetail === '') {
        alert('กรุณากรอกรายละเอียดเพิ่มเติม');
        $('#confirmModal').modal('hide');
      } else {
        $('#confirmModal').modal('show');
      }
    } else {
      $('#confirmModal').modal('show');
    }
  }
  ForwardData() {
    if (this.selectedData === 'ส่งคืน') {
      // if (this.textdetail === null || this.textdetail === '') {
      //   alert('กรุณากรอกรายละเอียดเพิ่มเติม');
      //   $('#confirmModal').modal('hide');
      // } else {
      // $('#confirmModal').modal('show');
      const returnagenda = {
        'meetingId': this.processtest,
        'requestId': localStorage.getItem('requestId'),
        'agenda': 'ส่งคืน',
        'detail': this.textdetail,
        'isReturn': this.sendrewardData,
      };
      console.log('Reward: ', returnagenda);
      this.agendaService.returnAgenda(returnagenda).subscribe(res => {
        console.log(res);
        if (res['status'] === 'success') {
          this.saveDraftstatus_State = false;
          this.confirmState = 'success';
          if (this.confirmState = 'success') {
            $('#successModal').modal('show');
            $('#confirmModal').modal('hide');
          }
          this.getAgenda();
          setTimeout(() => {
            // $('#confirmModal').modal('hide');
            $('#successModal').modal('hide');
            this.confirmState = 'confirm';
          }, 3000);

        } else {
          Swal.fire({
            title: 'error',
            text: res['message'],
            icon: 'error',
          });
        }
      }, err => {
        console.log(err);
      });
      // }
    } else {
      const returnagenda = {
        'meetingId': this.processtest,
        'requestId': localStorage.getItem('requestId'),
        'agenda': this.selectedData,
        'detail': this.textdetail,
        'isReturn': this.sendrewardData,
      };
      console.log('Reward: ', returnagenda);
      this.agendaService.returnAgenda(returnagenda).subscribe(res => {
        console.log(res);
        if (res['status'] === 'success') {
          this.saveDraftstatus_State = false;
          this.confirmState = 'success';
          if (this.confirmState = 'success') {
            $('#successModal').modal('show');
            $('#confirmModal').modal('hide');
          }
          this.getAgenda();
          setTimeout(() => {
            // $('#confirmModal').modal('hide');
            $('#successModal').modal('hide');
            this.confirmState = 'confirm';
          }, 3000);

        } else {
          Swal.fire({
            title: 'error',
            text: res['message'],
            icon: 'error',
          });
        }
      }, err => {
        console.log(err);
      });
    }
  }

  backToDashboardPage() {
    this.backDashboard = 'dashboard'
    this.router.navigate(['/dashboard1']);
  }

  changeSaveDraft() {
    this.saveDraftstatus_State = true;
    this.agendaService.status_state = true;
    console.log(this.saveDraftstatus_State);
  }
  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate');
    // return confirm();
    const subject = new Subject<boolean>();
    // const status = this.sidebarService.outPageStatus();
    console.log('000 sidebarService status:', status);
    if (this.saveDraftstatus_State === true) {
      console.log(this.Sidebar.goToLink)
      // console.log('000 sidebarService status:', status);
      const dialogRef = this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        console.log('data::', data);
        if (data.returnStatus === true) {

          this.SaveData();

          this.saveDraftstatus_State = false;
          // console.log('outPageStatus', this.SaveData())
          subject.next(false);
        } else if (data.returnStatus === false) {
          console.log('000000 data::', data.returnStatus);
          // this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
        }
      });
      console.log('8889999', subject);
      return subject;
    } else {
      return true;
    }
  }
}
