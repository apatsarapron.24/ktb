import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgendaAddComponent} from './agenda-add.component';
import { CanDeactivateGuard } from '../../services/configGuard/config-guard.service'
const routes: Routes = [
  {
    path: '',
    component: AgendaAddComponent,
    canDeactivate: [CanDeactivateGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AgendaAddRoutingModule { }
