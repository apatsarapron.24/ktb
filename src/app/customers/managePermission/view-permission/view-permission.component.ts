import { Component, OnInit } from '@angular/core';
import { RouterLink, Router, ActivatedRoute } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { RoleDetailService } from '../../../services/managePermission/role-detail/role-detail.service'

export interface PeriodicElement {
  no: Number;
  permission: String;
  level: String;
}

let fecthData: PeriodicElement[];
// [
//   {
//     no: 1,
//     user_permission: 'Product Owner',
//     permission_level: 'พนักงาน, ฝ่าย, กลุ่ม, สาย',
//   },
//   {
//     no: 2,
//     user_permission: 'Risk',
//     permission_level: 'พนักงาน, ฝ่าย, กลุ่ม, สาย',
//   },
//   {
//     no: 3,
//     user_permission: 'Compliance',
//     permission_level: 'พนักงาน, ฝ่าย, กลุ่ม, สาย',
//   },
//   {
//     no: 4,
//     user_permission: 'Operation',
//     permission_level: 'พนักงาน, ฝ่าย, กลุ่ม, สาย',
//   },
//   {
//     no: 5,
//     user_permission: 'Finance',
//     permission_level: 'พนักงาน, ฝ่าย, กลุ่ม, สาย',
//   },
//   {
//     no: 6,
//     user_permission: 'Retail',
//     permission_level: 'พนักงาน, ฝ่าย, กลุ่ม, สาย',
//   },
//   {
//     no: 6,
//     user_permission: 'PC',
//     permission_level: 'พนักงาน, ฝ่าย, กลุ่ม, สาย',
//   },
// ];

const initData = [
];
@Component({
  selector: 'app-view-permission',
  templateUrl: './view-permission.component.html',
  styleUrls: ['./view-permission.component.scss']
})
export class ViewPermissionComponent implements OnInit {

  displayedColumns: string[] = ['no', 'permission', 'level', 'view_detail'];
  dataSourceSearch = new MatTableDataSource(initData);

  constructor(private router: Router,
    private getRole: RoleDetailService) { }

  ngOnInit() {

    this.getRole.getRole().subscribe(res => {

      this.dataSourceSearch = new MatTableDataSource(res['data']);
      console.log('this.getRole', res['data']);
    });



  }

  back() {
    this.router.navigate(['manage-permission']);
  }

  viewDetail(id) {
    sessionStorage.setItem('permission_id', id);
    this.router.navigate(['manage-permission/view-permission/detail']);
  }

}
