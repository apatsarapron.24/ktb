import { Component, OnInit } from '@angular/core';
import { RouterLink, Router, ActivatedRoute } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { RoleDetailService } from '../../../../services/managePermission/role-detail/role-detail.service';
import Swal from 'sweetalert2';
declare var $: any;

export interface PeriodicElement {
  title: String;
  data: {
    no: Number;
    display: String;
    employee: String;
    department: String;
    workgroup: String;
    division: String;
  }[];
  // data: Array<any>;
}

const fecthData: PeriodicElement[] = [
  {
    title: 'ส่วนที่ 1',
    data: [
      {
        no: 1,
        display: 'Product Owner',
        employee: 'View',
        department: 'Approve',
        workgroup: 'Edit',
        division: 'Approve',
      },
      {
        no: 2,
        display:
          'ความพร้อมด้านการให้บริการลูกค้าอย่างเป็นธรรม (Market Conduct)',
        employee: 'View',
        department: 'Approve',
        workgroup: 'Edit',
        division: 'Approve',
      },
    ],
  },
  {
    title: 'ส่วนที่ 2',
    data: [
      {
        no: 1,
        display: 'Product Owner',
        employee: 'View',
        department: 'Approve',
        workgroup: 'Edit',
        division: 'Approve',
      },
      {
        no: 2,
        display:
          'ความพร้อมด้านการให้บริการลูกค้าอย่างเป็นธรรม (Market Conduct)',
        employee: 'View',
        department: 'Approve',
        workgroup: 'Edit',
        division: 'Approve',
      },
    ],
  },
];

const initData = [];

@Component({
  selector: 'app-view-detail-permission',
  templateUrl: './view-detail-permission.component.html',
  styleUrls: ['./view-detail-permission.component.scss'],
})
export class ViewDetailPermissionComponent implements OnInit {
  // displayedColumns: string[] = ['no1', 'display1', 'employee', 'department', 'workgroup', 'division'];
  // dataSourceSearch = new MatTableDataSource(initData);
  editdetailmode = false;
  confirmState = 'confirm';
  dataDetail: any = [];
  permission: any = [];
  status_loading = false;
  roleName: any = '';
  rolePermissionId: any;
  permissionList: any = [
    {
      title: 'View',
      value: 'View',
    },
    {
      title: 'Edit',
      value: 'Edit',
    },
    {
      title: 'Approve',
      value: 'Approve',
    },
    {
      title: 'None',
      value: '-',
    },
  ];

  constructor(private router: Router, private getDetail: RoleDetailService) {}

  ngOnInit() {
    this.getDetail
      .getRoleDetail(sessionStorage.getItem('permission_id'))
      .subscribe((res) => {
        console.log(res);
        this.dataDetail = res['data'];
        this.permission = res['permission'];
        this.roleName = res['roleName'];
        this.rolePermissionId = res['rolePermissionId'];
      });
    console.log('sessionStorage', sessionStorage.getItem('permission_id'));
  }
  back() {
    this.router.navigate(['manage-permission/view-permission']);
  }
  selectChange(data, list, headIndex, subIndex, index) {
    console.log('per:', data);
    console.log('list:', list);
    console.log('index:', index);
    // list.permission[index] = data;
  }

  editDetail() {
    this.status_loading = true;
    if (this.editdetailmode === false) {
      this.editdetailmode = true;
      setTimeout(() => {
        this.status_loading = false;
      }, 3000);
    } else {
      this.editdetailmode = false;
      this.getDetail
        .getRoleDetail(sessionStorage.getItem('permission_id'))
        .subscribe((res) => {
          console.log(res);
          this.dataDetail = res['data'];
          setTimeout(() => {
            this.status_loading = false;
          }, 3000);
        });
    }
  }

  onSave() {
    // this.status_loading = true;
    const _data = {
      rolePermissionId: this.rolePermissionId,
      data: this.dataDetail,
    };
    console.log('data>>>>>', _data);
    this.status_loading = true;
    this.getDetail.postRolePermissionDetail(_data).subscribe(
      (res) => {
        console.log(res);

        if (res['status'] === 'success') {
          this.confirmState = 'success';
          this.editdetailmode = false;
          this.status_loading = false;
          // setTimeout(() => {
          //   $('#confirmModal').modal('hide');
          //   this.confirmState = 'confirm';
          // }, 3000);
          Swal.fire({
            title: 'success',
            text: 'คุณได้บันทึกข้อมูลเรียบร้อย',
            icon: 'success',
            showConfirmButton: false,
            timer: 3000
          });

        } else {
          this.status_loading = false;
          Swal.fire({
            title: 'error',
            text: res['message'],
            icon: 'error',
          });
        }
      },
      (err) => {
        console.log(err);
        this.status_loading = false;
        Swal.fire({
          title: 'error',
          text: err['message'],
          icon: 'error',
        });
      }
    );
  }
}
