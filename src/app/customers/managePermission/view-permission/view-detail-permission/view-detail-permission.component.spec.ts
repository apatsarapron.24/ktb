import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewDetailPermissionComponent } from './view-detail-permission.component';

describe('ViewDetailPermissionComponent', () => {
  let component: ViewDetailPermissionComponent;
  let fixture: ComponentFixture<ViewDetailPermissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewDetailPermissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewDetailPermissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
