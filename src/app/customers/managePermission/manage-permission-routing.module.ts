import { ManagePermissionComponent } from './manage-permission.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreatePermissionComponent } from './create-permission/create-permission.component';
import { ViewPermissionComponent } from './view-permission/view-permission.component';
import { ViewDetailPermissionComponent } from './view-permission/view-detail-permission/view-detail-permission.component';

const routes: Routes = [
{
  path: '',
  component: ManagePermissionComponent,
},
{
  path: 'create-permission',
  component: CreatePermissionComponent,
},
{
  path: 'view-permission',
  children: [
    {
      path: '',
      component: ViewPermissionComponent,
    },
    {
      path: 'detail',
      component: ViewDetailPermissionComponent,
    },
  ]
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagePermissionRoutingModule { }
