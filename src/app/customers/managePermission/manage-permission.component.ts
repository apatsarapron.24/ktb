import { element } from 'protractor';
import { Data } from './../dashboard-cus/dashboard-cus.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { RouterLink, Router, ActivatedRoute } from '@angular/router';
import { RoleDetailService } from '../../services/managePermission/role-detail/role-detail.service';
import { CreatePermissionService } from '../../services/managePermission/create-permission/create-permission.service'
const fecthData = [
  {
    id: 1,
    empoyeeid: '580200',
    name: 'นางสาว ข.',
    user_permission: 'Product Owner',
    permission_level: 'ระดับพนักงาน',
    position: 'เจ้าหน้าที่กำกับการปฏิบัติตามกฎเกณฑ์ทาง...',
    department: 'กำกับการปฏิบัติตามกฎเกณฑ์ทาง...',
    workgroup: 'กำกับงานกฎเกณฑ์',
    division: 'กำกับกฎเกณฑ์และกฎหมาย',
  },
  {
    id: 2,
    empoyeeid: '580201',
    name: 'นางสาว ข.',
    user_permission: 'Product Owner',
    permission_level: 'ระดับพนักงาน',
    position: 'เจ้าหน้าที่กำกับการปฏิบัติตามกฎเกณฑ์ทางหลวง...',
    department: 'กำกับการปฏิบัติตามกฎเกณฑ์ทาง...',
    workgroup: 'กำกับงานกฎเกณฑ์',
    division: 'กำกับกฎเกณฑ์และกฎหมาย',
  },
];

@Component({
  selector: 'app-manage-permission',
  templateUrl: './manage-permission.component.html',
  styleUrls: ['./manage-permission.component.scss'],
})
export class ManagePermissionComponent implements OnInit {
  saveDraftstatus: any = '';
  statusDelete: any = '';
  displayedColumns: string[] = [
    'empoyeeid',
    'name',
    'user_permission',
    'permission_level',
    'position',
    'department',
    'workgroup',
    'division',
    'permission_manage',
  ];
  dataSourceSearch = new MatTableDataSource();

  detailData = [];

  permissionSelected = [];
  permissionData = [
    { label: '1', value: '1' },
    { label: '2', value: '2' },
    { label: '3', value: '3' },
    { label: '4', value: '4' },
    { label: '5', value: '5' },
  ];
  permission_role: any = [
    { value: '1', name: 'Product Owner' },
    { value: '2', name: 'Owner' },
  ];

  permissionLavel_list: any = [
    { value: 'admin', name: 'ระดับผู้ดูแล' },
    { value: 'officer', name: 'ระดับพนักงาน' },
    { value: 'user', name: 'ระดับผู้ใช้งาน' },
  ];

  edit_user: any = {
    id: '',
    employeeId: '',
    name: ''
  };
  permission: any;
  permission_level: any;
  edit: any;

  // MatPaginator Inputs
  length = 100;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];

  // MatPaginator Output
  pageEvent: PageEvent;

  permissionList = [];
  level = [];
  // addPermissionData = {};
  confirmData: any;

  @ViewChild('paginator') paginator: MatPaginator;

  constructor(private router: Router,
    private getUser: RoleDetailService,
    private CreatePermissionService: CreatePermissionService) { }

  ngOnInit() {
    this.getUser.getUser().subscribe(res => {
      this.dataSourceSearch = new MatTableDataSource(res['data']);
      console.log('this.getuser', this.dataSourceSearch);
      this.dataSourceSearch.paginator = this.paginator;
    });
    this.getUser.getRole().subscribe(res => {
      this.permissionList = res['data'];
      console.log('permissionList', this.permissionList);
    });
  }

  filLevel(permission) {
    console.log('permission', permission);
    this.permission = permission;
    const fildata = this.permissionList.filter(ele => {
      return ele.permission === permission;
    });
    this.level = fildata[0].level;
    this.permission_level = '';
  }

  editPermission(data: any) {
    console.log('edit', data);
    this.edit = data;
    this.edit_user.id = data.uid;
    this.edit_user.employeeId = data.uid;
    this.edit_user.name = data.name;
    this.permission = data['role'];
    console.log('permissionList', this.permissionList);
    this.filLevel(this.permission);
    this.permission_level = data['level'];
  }

  save() {
    const sendData = [
      {
        userId: this.edit['uid'],
        role: this.permission,
        level: this.permission + '-' + this.permission_level
      }
    ];
    console.log(sendData);
    this.CreatePermissionService.saveUser(sendData).subscribe(res => {
      console.log(res);
      if (res['status'] === 'success') {
        this.ngOnInit();
        this.saveDraftstatus = res['status'];
        setTimeout(() => {
          this.saveDraftstatus = 'fail';
        }, 3000);
        setTimeout(() => {
          this.saveDraftstatus = '';
        }, 3000);

      } else {
        alert(res['message']);
      }
    });
  }



  selectedData(e) {
    console.log(e);
  }

  checkDuplicate(input, source) {
    let check = true;
    console.log(source);
    if (source) {
      source.forEach((ele) => {
        if (ele.empoyeeid === input) {
          check = false;
        }
      });
    }
    return check;
  }

  viewPermission() {
    this.router.navigate(['manage-permission/view-permission']);
  }

  addPermission() {
    this.router.navigate(['manage-permission/create-permission']);
  }

  delete() {
    // use ID
    const data = {
      'uid': this.edit.uid,
      'roleLevel': this.permission + '-' + this.permission_level
    };
    console.log('delete:', data);
    this.getUser.deleteUser(data).subscribe(res => {
      console.log('res delete', res);
      if (res['status'] === 'success') {
        this.ngOnInit();
        this.statusDelete = 'success';
        setTimeout(() => {
          this.statusDelete = '';
        }, 3000);
      } else {
        alert(res['message']);
      }
    });
  }
}
// department: "กำกับการปฏิบัติตามกฎเกณฑ์ทาง..."
// division: "กำกับกฎเกณฑ์และกฎหมาย"
// empoyeeid: "580200"
// name: "นางสาว ข."
// permission_level: "ระดับพนักงาน"
// position: "เจ้าหน้าที่กำกับการปฏิบัติตามกฎเกณฑ์ทาง..."
// user_permission: "Product Owner"
// workgroup: "กำกับงานกฎเกณฑ์"
