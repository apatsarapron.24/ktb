import { ManagePermissionComponent } from './manage-permission.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCheckboxModule } from '@angular/material/checkbox';
import {MatSelectModule} from '@angular/material/select';

import { ManagePermissionRoutingModule } from './manage-permission-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { FormsModule } from '@angular/forms';
import { CreatePermissionComponent } from './create-permission/create-permission.component';
import { ViewPermissionComponent } from './view-permission/view-permission.component';
import { ViewDetailPermissionComponent } from './view-permission/view-detail-permission/view-detail-permission.component';
import { ConfirmPermissionComponent } from './create-permission/confirm-permission/confirm-permission.component';
import { MatDialogModule } from '@angular/material';
import { SavePermissionComponent } from './create-permission/save-permission/save-permission.component';
import {MatPaginatorModule} from '@angular/material/paginator';

@NgModule({
  declarations: [ManagePermissionComponent,
    CreatePermissionComponent,
    ViewPermissionComponent,
    ViewDetailPermissionComponent,
    ConfirmPermissionComponent,
    SavePermissionComponent],
  imports: [
    CommonModule,
    ManagePermissionRoutingModule,
    MatButtonModule,
    MatTableModule,
    FormsModule,
    MatCheckboxModule,
    MatDialogModule,
    MatPaginatorModule,
    MatSelectModule
   ],
   entryComponents: [ConfirmPermissionComponent, SavePermissionComponent]
})
export class ManagePermissionModule { }
