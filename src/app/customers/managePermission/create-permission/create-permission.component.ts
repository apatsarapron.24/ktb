import { element } from 'protractor';
import { Data } from '../../dashboard-cus/dashboard-cus.component';
import { Component, OnInit, Inject,  ViewChild, ElementRef} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { RouterLink, Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import Swal from 'sweetalert2';
import { ConfirmPermissionComponent } from './confirm-permission/confirm-permission.component';
import { CreatePermissionService } from '../../../services/managePermission/create-permission/create-permission.service';

export interface PeriodicElement {
  no: Number;
  empoyeeid: String;
  name: String;
  position: String;
  department: String;
  workgroup: String;
  division: String;
  permission: String;
  managePermission: String;
}

const fecthData: PeriodicElement[]  = [
  {
    no: 1,
    empoyeeid: '580200',
    name: 'นางสาว ข.',
    position: 'เจ้าหน้าที่กำกับการปฏิบัติตามกฎเกณฑ์ทาง...',
    department: 'กำกับการปฏิบัติตามกฎเกณฑ์ทาง...',
    workgroup: 'กำกับงานกฎเกณฑ์',
    division: 'กำกับกฎเกณฑ์และกฎหมาย',
    permission: '',
    managePermission: '',
  },
  {
    no: 2,
    empoyeeid: '580201',
    name: 'นางสาว ข.',
    position: 'เจ้าหน้าที่กำกับการปฏิบัติตามกฎเกณฑ์ทางหลวง...',
    department: 'กำกับการปฏิบัติตามกฎเกณฑ์ทาง...',
    workgroup: 'กำกับงานกฎเกณฑ์',
    division: 'กำกับกฎเกณฑ์และกฎหมาย',
    permission: '',
    managePermission: '',
  },
];

const initData = [
];

@Component({
  selector: 'app-create-permission',
  templateUrl: './create-permission.component.html',
  styleUrls: ['./create-permission.component.scss']
})


export class CreatePermissionComponent implements OnInit {
  displayedColumns: string[] = ['select', 'empoyeeid', 'name', 'position', 'department', 'workgroup', 'division'];
  dataSourceSearch = new MatTableDataSource(initData);
  selection = new SelectionModel<PeriodicElement>(true, []);


  view_info_data: any = '';
  modal_status: any;

  empoyeeid = '';
  name = '';
  department = '';
  workgroup = '';
  division = '';

  search_data = false;

  animal: string;
  names: string;

  // model
  loopUserCount = 0;
  modelEmpoyeeID: any;
  modelempoyeeName: any;

  user_permission = '' ;
  permission_level = '' ;

  divisionList: any;
  workgroupList: any;
  departmentList: any;
  divisionList_data: any;
  workgroupList_data: any;
  departmentList_data: any;

  constructor(private router: Router, private dialog: MatDialog, private createPermissionService: CreatePermissionService) { }

  ngOnInit() {
    this.createPermissionService.divisionList().subscribe( res => {
      this.divisionList = res['data'];
      this.divisionList_data = res['data'];
    });
    this.createPermissionService.workgroupList().subscribe( res => {
      this.workgroupList = res['data'];
      this.workgroupList_data = res['data'];
    });
    this.createPermissionService.departmentList().subscribe( res => {
      this.departmentList = res['data'];
      this.departmentList_data = res['data'];
    });
  }

  searchResult() {
    console.log('this.workgroup', this.workgroup);
    const searchSend = {
      empSctr: this.department ? this.department : '',
      empGrp: this.workgroup ? this.workgroup : '',
      empDept: this.division ? this.division : '',
      empNumber: this.empoyeeid ? this.empoyeeid : '',
      empThainame: this.name ? this.name : '',
    };

    console.log('searchSend', searchSend);
    this.createPermissionService.userSearch(searchSend).subscribe( res => {
      console.log('res', res);
      const getData = res['data'];
      this.dataSourceSearch = new MatTableDataSource(getData);
      this.search_data = true;
      this.selection = new SelectionModel<PeriodicElement>(true, []);
    });
    // this.dataSourceSearch = new MatTableDataSource(fecthData);
    // this.search_data = true;
    // this.selection = new SelectionModel<PeriodicElement>(true, []);
  }

  back() {
    this.router.navigate(['manage-permission']);
  }

  cancel() {
    this.search_data = false;
  }

  checkDuplicate(input, source) {
    let check = true;
    console.log(source);
    if (source) {
      source.forEach(ele => {
        if (ele.empoyeeid === input) { check = false; }
      });
    }
    return check;
   }


   /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSourceSearch.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSourceSearch.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: PeriodicElement): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    // return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.select + 1}`;
  }

  submitAddPermission() {
    const dataSend = {
      userSelect: this.selection.selected ? this.selection.selected : '',
      userPermission: this.user_permission ? this.user_permission : '',
      permissionLevel: this.permission_level ? this.permission_level : '',
    };
    console.log('dataSend', dataSend);
  }
  // filter
  filterSaechWorkgroup(data) {
      const arr =  this.divisionList_data.filter( ele => {
        return ele.ouName === data;
      });
      const arr_workgroupList = this.workgroupList_data.filter( ele => {
        return ele.groupOuCode === arr[0].ouCode;
      });
      this.workgroupList = arr_workgroupList;
      this.workgroup = '';
      this.division = '';
      this.departmentList = [];
  }
  filterSaechDepartment(data) {
    const arr =  this.workgroupList_data.filter( ele => {
      return ele.ouName === data;
    });
    const arr_Department = this.departmentList_data.filter( ele => {
      return ele.sctrOuCode === arr[0].ouCode;
    });
    this.departmentList = arr_Department;
    this.division = '';
}


  // model =====

  openConfirmDialog() {
    this.dialog.open(ConfirmPermissionComponent, { disableClose: true , data: {dialogData: this.selection}});
    // localStorage.removeItem('confirmSavePermissionData');
    // localStorage.setItem('confirmPermissionData', JSON.stringify(this.selection));
  }

}


