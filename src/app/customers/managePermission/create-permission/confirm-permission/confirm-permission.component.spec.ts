import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmPermissionComponent } from './confirm-permission.component';

describe('ConfirmPermissionComponent', () => {
  let component: ConfirmPermissionComponent;
  let fixture: ComponentFixture<ConfirmPermissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmPermissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmPermissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
