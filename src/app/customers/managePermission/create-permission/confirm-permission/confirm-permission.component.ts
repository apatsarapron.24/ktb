import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { SavePermissionComponent } from '../save-permission/save-permission.component';
import { CreatePermissionService } from '../../../../services/managePermission/create-permission/create-permission.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

export interface DialogData {
  dialogData: any;
}

@Component({
  selector: 'app-confirm-permission',
  templateUrl: './confirm-permission.component.html',
  styleUrls: ['./confirm-permission.component.scss']
})
export class ConfirmPermissionComponent implements OnInit {
  confirmData: any;

  // model
  loopUserCount = 0;
  addPermissionData = [];
  permissionList = [];
  level = [];

  statusSavePage = false;

  constructor(
    private router: Router,
    private createPermissionService: CreatePermissionService,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<ConfirmPermissionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {
    this.dialogRef.disableClose = true;
  }

  ngOnInit() {
    console.log('data000', this.data.dialogData.selected);
    // const getDataList = this.data.dialogData.selected;

    this.confirmData = this.data.dialogData.selected;
    if (this.confirmData) {
      this.managePermission();
      this.createPermissionService.getRole().subscribe( res => {
      this.permissionList = res['data'];
      console.log('permissionList', this.permissionList);
      });
    }
  }
  close() {
    this.dialogRef.close();
  }
  managePermission() {
    this.loopUserCount = 0;
    this.confirmData.forEach( element  => {
      this.addPermissionData.push({
        modelEmpoyeeID: element.empeId,
        modelempoyeeName: element.titleNameTh + ' ' + element.empeNameTh + '  ' + element.empeLastNameTh,
        user_permission: null,
        permission_level: null,
      });
    });
  }
  managePermissionNext() {
    console.log('loopUserCount', this.loopUserCount);
    if (this.loopUserCount < this.confirmData.length - 1) {
      this.loopUserCount = this.loopUserCount + 1;
      if (this.addPermissionData[this.loopUserCount].user_permission) {
        const permissionName = this.addPermissionData[this.loopUserCount].user_permission;
        this.filLevel(permissionName);
      }
    } else {
      console.log('aaaaaaaasssasasas');
      this.statusSavePage = !(this.statusSavePage);
      // this.dialogRef.close();
      // this.openSaveDialog();
    }
  }
  managePermissionBack() {
    if (this.loopUserCount > 0) {
      this.loopUserCount = this.loopUserCount - 1;
      if (this.addPermissionData[this.loopUserCount].user_permission) {
        const permissionName = this.addPermissionData[this.loopUserCount].user_permission;
        this.filLevel(permissionName);
      }
    }
  }

  filLevel(permission) {
    console.log('permission', permission);
    const fildata = this.permissionList.filter(ele => {
      return ele.permission === permission;
    });
    this.level = fildata[0].level;
  }

  openSaveDialog() {
    this.dialog.open(SavePermissionComponent, {
      disableClose: true,
      data: { dialogSaveData: this.addPermissionData}
    });  // ติด SavePermissionComponent
  }

  // ------------------------ save ---------------------------

  save() {
    const sendData = [];
    this.addPermissionData.forEach(element => {
      sendData.push({
        userId: element.modelEmpoyeeID,
        role: element.user_permission,
        level: element.user_permission + '-' + element.permission_level
      });
    });
    this.createPermissionService.saveUser(sendData).subscribe( res => {
      if (res['status'] === 'success') {
        this.dialogRef.close();
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'คุณได้เพิ่มสิทธิ์การใช้งานเรียบร้อยแล้ว',
          showConfirmButton: false,
        }).then(() => {
          this.router.navigate(['/manage-permission']);
        });
      } else {
        Swal.fire({
          position: 'center',
          icon: 'error',
          title: 'เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้',
          showConfirmButton: false,
        });
      }
    }, err => {
      console.log(err);
      alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
    });
  }
  back() {
    this.loopUserCount = 0;
    this.statusSavePage = !(this.statusSavePage);
  }

}
