import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ConfirmPermissionComponent } from '../confirm-permission/confirm-permission.component';
import Swal from 'sweetalert2';
import { CreatePermissionService } from '../../../../services/managePermission/create-permission/create-permission.service';
import { Router } from '@angular/router';

export interface DialogData {
  dialogSaveData: any;
}

@Component({
  selector: 'app-save-permission',
  templateUrl: './save-permission.component.html',
  styleUrls: ['./save-permission.component.scss']
})
export class SavePermissionComponent implements OnInit {

  addPermissionData: any;
  permissionData: any;

  constructor(
    private router: Router,
    private createPermissionService: CreatePermissionService,
    private dialogRef: MatDialogRef<SavePermissionComponent>,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {
    this.dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.addPermissionData = this.data.dialogSaveData;
  }

  close() {
    this.dialogRef.close();
  }
  managePermissionNext() {
    const sendData = [];
    this.addPermissionData.forEach(element => {
      sendData.push({
        userId: element.modelEmpoyeeID,
        role: element.user_permission,
        level: element.user_permission + '-' + element.permission_level
      });
    });
    this.createPermissionService.saveUser(sendData).subscribe( res => {
      if (res['status'] === 'success') {
        this.dialogRef.close();
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'คุณได้เพิ่มสิทธิ์การใช้งานเรียบร้อยแล้ว',
          showConfirmButton: false,
        }).then(() => {
          this.router.navigate(['/manage-permission']);
        });
      } else {
        Swal.fire({
          position: 'center',
          icon: 'error',
          title: 'เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้',
          showConfirmButton: false,
        });
      }
    }, err => {
      console.log(err);
      alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
    });
  }
  managePermissionBack() {
    this.dialogRef.close();
    // this.dialog.open(ConfirmPermissionComponent, { disableClose: true }); // ติดยังไม่สามารถกลับ dialog ก่อนหน้าได้
  }
}
