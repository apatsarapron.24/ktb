import { CommentRoutingModule } from './comment-routing.module';
import { CommentComponent } from './comment.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// ======= pagination =========
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { DialogSaveStatusCommentComponent } from './dialog-save-status-comment/dialog-save-status-comment.component';
import { CanDeactivateGuard } from './../../services/configGuard/config-guard.service';
import { MatDialogModule } from '@angular/material';

@NgModule({
  declarations: [
    CommentComponent,
    DialogSaveStatusCommentComponent,
  ],
  imports: [
    CommonModule,
    CommentRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatDialogModule
  ],
  providers: [CanDeactivateGuard],
  entryComponents: [DialogSaveStatusCommentComponent]
})
export class CommentModule { }

