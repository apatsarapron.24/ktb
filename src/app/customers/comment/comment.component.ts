import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CommentService } from '../../services/comment/comment.service';
import * as moment from 'moment';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { DialogSaveStatusCommentComponent } from './dialog-save-status-comment/dialog-save-status-comment.component';
import { CanDeactivateGuard } from './../../services/configGuard/config-guard.service';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

export interface UserData {
  id: string;
  name: string;
  empePosition: string;
  dept: string;
  stcr: string;
  grp: string;
  remark: string;
  detail: string;
  createdAt: string;
}


// const datatable: UserData[] = [
//   {
//     id: '1',
//     name: 'นางสาวใจดี ทดลองทำ',
//     position: 'ผู้ช่วยกรรมการผู้จัดการใหญ่ ผู้บริหารฝ่าย',
//     department: 'พัฒนาผลิตภัณฑ์สินเชื่อธุรกิจ',
//     groupwrk: 'กลยุทธ์วิสาหกิจขนาดกลาง',
//     division: 'กำกับกฎเกณฑ์และกฎหมาย',
//     comments: 'เห็นชอบ',
//     details: 'เห็นชอบเห็นชอบเห็นชอบเห็นชอบเห็นชอบ',
//     datetime: '10/7/2562  11:02:00',
//   },
//   {
//     id: '2',
//     name: 'นางสาวใจดี ทดลองทำ',
//     position: 'ผู้ช่วยกรรมการผู้จัดการใหญ่ ผู้บริหารฝ่าย',
//     department: 'พัฒนาผลิตภัณฑ์สินเชื่อธุรกิจ',
//     groupwrk: 'กลยุทธ์วิสาหกิจขนาดกลาง',
//     division: 'กำกับกฎเกณฑ์และกฎหมาย',
//     comments: 'เห็นชอบ',
//     details: 'เห็นชอบเห็นชอบเห็นชอบเห็นชอบเห็นชอบ',
//     datetime: '10/7/2562  11:02:00',
//   },
//   {
//     id: '3',
//     name: 'นางสาวใจดี ทดลองทำ',
//     position: 'ผู้ช่วยกรรมการผู้จัดการใหญ่ ผู้บริหารฝ่าย',
//     department: 'พัฒนาผลิตภัณฑ์สินเชื่อธุรกิจ',
//     groupwrk: 'กลยุทธ์วิสาหกิจขนาดกลาง',
//     division: 'กำกับกฎเกณฑ์และกฎหมาย',
//     comments: 'เห็นชอบ',
//     details: 'เห็นชอบเห็นชอบเห็นชอบเห็นชอบเห็นชอบ',
//     datetime: '10/7/2562  11:02:00',
//   },
//   {
//     id: '4',
//     name: 'นางสาวใจดี ทดลองทำ',
//     position: 'ผู้ช่วยกรรมการผู้จัดการใหญ่ ผู้บริหารฝ่าย',
//     department: 'พัฒนาผลิตภัณฑ์สินเชื่อธุรกิจ',
//     groupwrk: 'กลยุทธ์วิสาหกิจขนาดกลาง',
//     division: 'กำกับกฎเกณฑ์และกฎหมาย',
//     comments: 'เห็นชอบ',
//     details: 'เห็นชอบเห็นชอบเห็นชอบเห็นชอบเห็นชอบ',
//     datetime: '10/7/2562  11:02:00',
//   },
//   {
//     id: '5',
//     name: 'นางสาวใจดี ทดลองทำ',
//     position: 'ผู้ช่วยกรรมการผู้จัดการใหญ่ ผู้บริหารฝ่าย',
//     department: 'พัฒนาผลิตภัณฑ์สินเชื่อธุรกิจ',
//     groupwrk: 'กลยุทธ์วิสาหกิจขนาดกลาง',
//     division: 'กำกับกฎเกณฑ์และกฎหมาย',
//     comments: 'เห็นชอบ',
//     details: 'เห็นชอบเห็นชอบเห็นชอบเห็นชอบเห็นชอบ',
//     datetime: '10/7/2562  11:02:00',
//   },
//   {
//     id: '6',
//     name: 'นางสาวใจดี ทดลองทำ',
//     position: 'ผู้ช่วยกรรมการผู้จัดการใหญ่ ผู้บริหารฝ่าย',
//     department: 'พัฒนาผลิตภัณฑ์สินเชื่อธุรกิจ',
//     groupwrk: 'กลยุทธ์วิสาหกิจขนาดกลาง',
//     division: 'กำกับกฎเกณฑ์และกฎหมาย',
//     comments: 'เห็นชอบ',
//     details: 'เห็นชอบเห็นชอบเห็นชอบเห็นชอบเห็นชอบ',
//     datetime: '10/7/2562  11:02:00',
//   }
// ];

export interface Food {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})

export class CommentComponent implements OnInit {
  foods: Food[] = [
    { value: 'steak-0', viewValue: 'Steak' },
    { value: 'pizza-1', viewValue: 'Pizza' },
    { value: 'tacos-2', viewValue: 'Tacos' }
  ];
  // MatPaginator Inputs
  length = 100;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  pageEvent: any;

  displayedColumns: string[] = ['id', 'name', 'position', 'department', 'groupwrk', 'division', 'comments', 'details', 'datetime'];
  dataSource: MatTableDataSource<UserData>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  commentForm: FormGroup;

  disabled = true;
  radioSelect: string;
  opinion = null;
  dataCommentlist: any = [];
  comment: string;
  checkAllow: string;
  saveDraftstatus = null;
  getRole = '';
  message_error: any;
  list_error: any = [];
  saveDraftstatus_State = false;

  constructor(
    private getComment: CommentService,
    private router: Router,
    public dialog: MatDialog,
    private guardService: CanDeactivateGuard
  ) { }

  ngOnInit() {
    this.getRole = localStorage.getItem('role');
    this.getComment.checkComment(localStorage.getItem('requestId')).subscribe(res => {
      this.checkAllow = res['message'];
      console.log('checkComment', res['message']);
      if (res['message'] === 'ALLOW PERMISSION') {
        this.getComment.commentDraft(localStorage.getItem('requestId')).subscribe(res => {
          console.log('commentDraft', res);
          if (res['data'].id !== null) {
            this.opinion = res['data'].remark;
            this.comment = res['data'].detail;
            if (this.opinion !== null) {
              this.disabled = false;
            }
            console.log('commentDraft', res['data'].remark);
          }
        });
      }
    });

    this.dataSource = new MatTableDataSource(this.dataCommentlist);
    this.getCommentList();
  }

  getCommentList() {
    this.getComment.commentList(localStorage.getItem('requestId')).subscribe(res => {
      console.log('this.getComment.commentList', res['data'].listPerson);
      this.dataCommentlist = res['data']['listPerson'];
      this.dataCommentlist.forEach((data, index) => {
        data.name = data['titleNameTh'] + data['empeNameTh'] + data['empeLastNameTh'];
        data.createdAt = moment(data.createdAt).format('DD/MM/YYYY');
      });
      this.dataSource = new MatTableDataSource(this.dataCommentlist);
      console.log(this.dataSource);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  selectRadio(select) {
    this.saveDraftstatus = null;
    if (select) {
      this.opinion = select;
      // this.radioSelect = select;
      this.disabled = false;
      console.log('this.radioSelect: ', this.radioSelect);
      console.log('ess: ', select);
    }
    this.changeSaveDraft();
  }
  detailComment() {
    this.saveDraftstatus = null;
    console.log('comment:', this.comment);
  }
  addComment(event) {
    const dataTosave = {
      requestId: +(localStorage.getItem('requestId')),
      remark: this.opinion,
      detail: this.comment,
      status: event
    };
    console.log(dataTosave);

    if (dataTosave.remark === 'เห็นชอบ โดยมีความเห็นเพิ่มเติม' &&
      (dataTosave.detail === '' || dataTosave.detail === null || dataTosave.detail === undefined)) {
      Swal.fire({
        title: 'error',
        text: 'กรุณาระบุรายละเอียดความเห็นเกี่ยวกับใบคำขอ',
        icon: 'error',
      });
    } else {
      this.getComment.commentSend(dataTosave).subscribe(res => {
        console.log('commentSend>>>>', res);
        if (event === 'save') {
          if (res['status'] === 'fail') {
            Swal.fire({
              title: 'error',
              text: res['message'],
              icon: 'error',
            });
          } else if (res['status'] === 'success') {
            this.saveDraftstatus = 'success';
            this.saveDraftstatus_State = false;
            setTimeout(() => {
              this.saveDraftstatus = 'fail';
            }, 3000);
          } else {
            Swal.fire({
              title: 'error',
              text: res['message'],
              icon: 'error',
              showConfirmButton: false,
              timer: 3000,
            });
          }
        }



        if (event === 'send') {
          if (res['status'] === 'fail') {
            this.list_error = [];
            if (res['message'] === 'ยังบันทึกผู้ดำเนินการไม่ครบ') {
              this.message_error = res['message'];
              this.list_error = res['data'];
              document.getElementById('opendModalSendworkCompliance').click();
            } else {
              Swal.fire({
                title: 'error',
                text: res['message'],
                icon: 'error',
              });
            }
          } else if (res['status'] === 'success') {
            Swal.fire({
              title: 'success',
              text: 'ส่งงานเรียบร้อย',
              icon: 'success',
              showConfirmButton: false,
              timer: 3000,
            });
            this.saveDraftstatus_State = false;
            localStorage.setItem('requestId', '');
            this.router.navigate(['/dashboard1']);
          } else {
            Swal.fire({
              title: 'error',
              text: res['message'],
              icon: 'error',
              showConfirmButton: false,
              timer: 3000,
            });
          }
        }


      });

    }

  }

  covertDate(datePC) {
    if (datePC != null) {
      const year = Number(datePC[6] + datePC[7] + datePC[8] + datePC[9]) + 543;
      const Date =
        datePC[0] + datePC[1] + '/' + datePC[3] + datePC[4] + '/' + year;
      return Date;
    } else {
      return null;
    }
  }

  changeSaveDraft() {
    this.saveDraftstatus_State = true;
    this.getComment.status_state = true;
    console.log('saveDraftstatus:', this.saveDraftstatus_State);
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate');
    // return confirm();
    const subject = new Subject<boolean>();
    // const status = this.sidebarService.outPageStatus();
    // console.log('000 sidebarService status:', status);
    if (this.saveDraftstatus_State === true) {
      // console.log('000 sidebarService status:', status);
      const dialogRef = this.dialog.open(DialogSaveStatusCommentComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        console.log('data::', data);
        if (data.returnStatus === true) {
          subject.next(true);
          this.addComment('save');
          this.saveDraftstatus_State = false;
        } else if (data.returnStatus === false) {
          console.log('000000 data::', data.returnStatus);
          // this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
        }
      });
      console.log('=====', subject);
      return subject;
    } else {
      return true;
    }
  }
}


