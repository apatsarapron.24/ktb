import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogSaveStatusCommentComponent } from './dialog-save-status-comment.component';

describe('DialogSaveStatusCommentComponent', () => {
  let component: DialogSaveStatusCommentComponent;
  let fixture: ComponentFixture<DialogSaveStatusCommentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogSaveStatusCommentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogSaveStatusCommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
