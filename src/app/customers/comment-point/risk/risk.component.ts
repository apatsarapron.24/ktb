import { element } from 'protractor';
import { Data } from '../../dashboard-cus/dashboard-cus.component';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import {
  AngularMyDatePickerDirective,
  IAngularMyDpOptions,
  IMyDateModel,
} from 'angular-mydatepicker';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
  CdkDragExit,
  CdkDragEnter,
  CdkDragStart,
  CdkDragEnd,
} from '@angular/cdk/drag-drop';
// import { Observable } from 'rxjs/Observable';
// import { Observable } from 'rxjs/Observable';
import { RiskService } from '../../../services/comment-point/risk/risk.service';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import * as moment from 'moment';
import { RequestService } from '../../../services/request/request.service';
import { SubmitService } from '../../../services/comment-point/submit/submit.service';
import { numberFormat } from 'highcharts';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { from } from 'rxjs';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { promise } from 'protractor';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
declare var $: any;

import { DialogStatusService } from '../../../services/comment-point/dialog-status/dialog-status.service';
import { DialogSaveStatusComponent } from '../../comment-point/dialog-save-status/dialog-save-status.component';


@Component({
  selector: 'app-risk',
  templateUrl: './risk.component.html',
  styleUrls: ['./risk.component.scss'],
})
export class RiskComponent implements OnInit {

  constructor(
    private router: Router,
    private sidebarService: RequestService,
    private Submit: SubmitService,
    private riskService: RiskService,
    public dialog: MatDialog,
  ) { }


  header_table = ['กระบวนการย่อย', 'In/Out', 'IT Related'];
  processConsiders: any[] = [
    {
      id: 166,
      mainProcess: 'ขั้นตอน A',
      validate: true,
      subProcess: [
        {
          id: 170,
          subTitle: 'rrr',
          inOut: 'No',
          itRelate: 'No',
          validate: true,
          subProcessRisk: [
            {
              id: 171,
              risk: 'rr',
              riskControl: 'rr',
              riskControlOther: 'rr',
              validate: true,
              buCommentId: 171,
              remark: 'ยอมรับได้',
              subject: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
              management: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
              poCommentRemark: null,
              poComment: null,
              pcCommentId: null,
              pcComment: null,
              pcExtraComment: null,
              pcIssue: null,
            },
          ],
        },
      ],
    },
    {
      id: 167,
      mainProcess: 'ขั้นตอน B',
      validate: true,
      subProcess: [
        {
          id: 171,
          subTitle: 'rrr',
          inOut: 'No',
          itRelate: 'Yes',
          validate: true,
          subProcessRisk: [
            {
              id: 172,
              risk: 'ee',
              riskControl: 'rr',
              riskControlOther: 'rr',
              validate: true,
              buCommentId: 172,
              remark: 'ยอมรับได้',
              subject: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
              management: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
              poCommentRemark: null,
              poComment: null,
              pcCommentId: null,
              pcComment: null,
              pcExtraComment: null,
              pcIssue: null,
            },
          ],
        },
      ],
    },
  ];

  readiness: any[] = [
    {
      'id': 45,
      'process': 'ความพร้อมของอุปกรณ์/กระบวนการ',
      'detail': [
        {
          'id': 89,
          'subProcess': 'ความมั่นคงปลอดภัยด้านไซเบอร์ (Security & Cyber Risk)',
          'management': 'ww',
          'responsible': 'ww',
          'dueDate': '2020-09-17T17:00:00.000+0000',
          'buCommentId': 105,
          'remark': 'ยอมรับได้',
          'commentSubject': 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
          'commentManagement': 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
          'poCommentRemark': null,
          'poComment': null,
          'pcCommentId': null,
          'pcComment': null,
          'pcExtraComment': null,
          'pcIssue': null
        },
        {
          'id': 90,
          'subProcess': 'ความถูกต้องเชื่อถือได้ (Integrity)',
          'management': 'ww',
          'responsible': 'ww',
          'dueDate': '2020-09-17T17:00:00.000+0000',
          'buCommentId': 106,
          'remark': 'ยอมรับได้',
          'commentSubject': 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
          'commentManagement': 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
          'poCommentRemark': null,
          'poComment': null,
          'pcCommentId': null,
          'pcComment': null,
          'pcExtraComment': null,
          'pcIssue': null
        },
        {
          'id': 91,
          'subProcess': 'ความพร้อมใช้ของบริการ (Availability)',
          'management': 'ww',
          'responsible': 'ww',
          'dueDate': '2020-09-17T17:00:00.000+0000',
          'buCommentId': 107,
          'remark': 'ยอมรับได้',
          'commentSubject': 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
          'commentManagement': 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
          'poCommentRemark': null,
          'poComment': null,
          'pcCommentId': null,
          'pcComment': null,
          'pcExtraComment': null,
          'pcIssue': null
        }
      ]
    },
    {
      'id': 46,
      'process': 'การคุ้มครองข้อมูลส่วนบุคคล (Data Privacy)',
      'detail': [
        {
          'id': 92,
          'subProcess': 'ee',
          'management': 'ww',
          'responsible': 'ww',
          'dueDate': '2020-09-17T17:00:00.000+0000',
          'buCommentId': 108,
          'remark': 'ยอมรับได้',
          'commentSubject': 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
          'commentManagement': 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
          'poCommentRemark': null,
          'poComment': null,
          'pcCommentId': null,
          'pcComment': null,
          'pcExtraComment': null,
          'pcIssue': null
        }
      ]
    }
  ];
  displayedColumns: string[] = [
    'mainProcess',
    'subProcess',
    'risk',
    'riskControl',
    'riskControlOther',
    'remark',
    'subject',
    'management',
  ];

  displayedColumnsReadiness: string[] = [
    'process',
    'subProcess',
    'management2',
    'responsible',
    'dueDate',
    'remark2',
    'commentSubject',
    'commentManagement',
  ];

  dataSource = this.processConsiders;
  dataSource_readness = this.readiness;
  FormData = [];
  message_alert_save: any = '';
  updateRisk = {
    requestId: null,
    status: false,
    processConsiders: [],
    processConsidersDelete: [],
    processConsidersSubDelete: [],
    processConsidersSubRiskDelete: [],
    readiness: [],
    readinessDelete: [],
    readinessDetailDelete: [],
    extraComment: [],
    extraCommentDelete: [],
  };
  listRisk = [
    'ผู้บริหารฝ่ายบริหารความเสี่ยงด้านเทคโนโลยีสารสนเทศ',
    'ผู้บริหารฝ่ายบริหารความเสี่ยงด้านปฏิบัติการ',
    'ผู้บริหารฝ่ายจัดการอาชญากรรมทางการเงินและการทุจริต',
    'ผู้บริหารฝ่ายบริหารความเสี่ยงสินเชื่อรายย่อย',
    'ผู้บริหารฝ่ายวิเคราะห์แบบจำลองและบริหารพอร์ตสินเชื่อ',
    'ผู้บริหารฝ่ายนโยบายสินเชื่อ',
    'ผู้บริหารกลุ่มกลั่นกรองสินเชื่อ'
  ];
  GetRoleCEO: any;
  // covert date not important...
  convertDate: any = [];
  convertDatePO: any = [];
  convertDatePC: any = [];
  // dropIndex: number;
  saveDraftstatus = null;

  // new table
  dropIndex: number;
  add_mainProcess: null;
  staticTitleIndex = []; // !important ex [0,1,2] mean set fix row index 0 1 2

  // Risk sub_table
  Risk_dropIndex = 0;
  staticTitleIndexRisk = [];

  // status page
  FormDataStatus = true;
  IN_OUT: any = ['No', 'Yes', ''];
  requestId: any;
  ResData: any;
  arryPC: any = [];
  GetRole: any;
  Getlevel: any;
  Getname: any;

  validate = null;
  Res_PO: any;
  GetData_PO: any = {
    readiness: [],
    processConsiders: [],
    extraComment: [],
    commentOperation2: [],
    question: [],
  };
  CommentPO: any = {
    status: false,
    requestId: null,
    processConsiders: [],
    readiness: [],
    extraComment: [],
  };

  Comment_PC: any = {
    status: false,
    requestId: null,
    processConsiders: [],
    readiness: [],
    extraComment: [],
  };
  BU_statusUpdate = false;
  validateBu = 0;
  lengthReadiness: any;
  lengthReadinessPC: any;
  lengthReadinessRisk: any;
  messageData: any;
  statusprocessConsiders: boolean;
  statusReadiness: boolean;
  statusextraComment: boolean;
  arrayStatus: any = [];
  status_loading = false;
  alertMessage: any = '';
  saveDraftstatus_State = false;

  note = [
    'No คือ ไม่ใช่ประเด็นคงค้าง',
    'Pending 1 คือ ต้องแก้ไขก่อนประกาศใช้ผลิตภัณฑ์',
    'Pending 2 คือ สามารถใช้ผลิตภัณฑ์ได้ก่อน แล้วค่อยแก้ไขตามหลังได้',
    'Pending 3 คือ ไม่เกี่ยวข้องกับการประกาศใช้ผลิตภัณฑ์',
    'Pending 4 คือ ต้องแก้ไขให้แล้วเสร็จก่อนนำเสนอเรื่องต่อบอร์ด',
  ];

  ngOnInit() {
    this.GetRest();


    console.log(this.lengthReadiness);

  }

  GetRest() {
    this.sidebarService.sendEvent();
    if (localStorage) {
      this.requestId = localStorage.getItem('requestId');
      this.GetRole = localStorage.getItem('role');
      this.Getlevel = localStorage.getItem('level');
      this.Getname = localStorage.getItem('ouName');
      for (let i = 0; i < this.listRisk.length; i++) {
        if (this.Getlevel === this.listRisk[i]) {
          this.GetRoleCEO = this.listRisk[i];
          break;
        } else {
          this.GetRoleCEO = null;
        }
      }

      if (this.GetRole === 'Risk') {
        this.status_loading = true;
        this.riskService.getRisk(this.requestId).subscribe((res) => {
          this.ResData = res;
          console.log(res);
          this.FormData = this.ResData.data;
          console.log('this.FormData 555', this.FormData);
          for (let index = 0; index < this.FormData.length; index++) {
            if (this.FormData[index].validate === true) {
              this.validate = this.FormData[index].validate;
            } else {
              if (this.validate !== true) {
                this.validate = false;
              }
            }

            // console.log(this.FormData);
            for (let i = 0; i < this.FormData.length; i++) {
              // console.log('gg:', this.FormData[i]);
              // console.log('gg:', this.FormData[i]);
              if (this.FormData[i].role === 'พนักงาน') {
                // window.addEventListener('mouseover', function (e) {
                //   const eles = document.getElementById('rowtest' + String(i)).offsetHeight;
                //   console.log(eles);

                // });
              }

            }
            // tslint:disable-next-line: no-shadowed-variable
            const Data = this.FormData[index].readiness;

            // console.log('date:', this.FormData[index].readiness);
            if (Data !== null) {
              for (let i = 0; i < Data.length; i++) {
                const Readeiness = Data[i].detail;
                Readeiness.forEach((item, _i) => {
                  // console.log('dueDate:', item.dueDate);
                  if (item.dueDate != null) {
                    const type = String(item.dueDate);
                    const year = Number(type.substr(0, 4)) + 543;
                    const month = type.substr(5, 2);
                    const day = type.substr(8, 2);
                    const date = day + '/' + month + '/' + year;
                    this.convertDate.push({ Convert: date });
                  } else {
                    this.convertDate.push({ Convert: '' });
                  }

                });
              }
            }
          }
          this.status_loading = false;
          console.log('res Risk:', this.FormData);
        }, err => {
          this.status_loading = false;
          console.log(err);
        });
      } else if (this.GetRole === 'PO') {
        this.status_loading = true;
        this.riskService.getRisk_PO(this.requestId).subscribe((res) => {
          this.ResData = res;
          this.FormData = null;
          this.FormData = this.ResData.data;
          for (let index = 0; index < this.FormData.length; index++) {
            if (this.FormData[index].validate === true) {
              this.validate = this.FormData[index].validate;
            } else { this.validate = false; }

            // tslint:disable-next-line: no-shadowed-variable
            const Data = this.FormData[index].readiness;
            console.log('date:', Data);
            if (Data !== null) {
              for (let i = 0; i < Data.length; i++) {
                const Readeiness = Data[i].detail;
                Readeiness.forEach((item, _i) => {
                  // console.log('dueDate:',   item.dueDate.substr(0, 10));
                  if (item.dueDate !== null) {
                    const type = String(item.dueDate);
                    const year = Number(type.substr(0, 4)) + 543;
                    const month = type.substr(5, 2);
                    const day = type.substr(8, 2);
                    const date = day + '/' + month + '/' + year;
                    this.convertDatePO.push({ Convert: date });
                  } else {

                    this.convertDatePO.push({ Convert: '' });
                  }

                });
              }
            }
          }
          console.log('res PO:', this.FormData);
          this.status_loading = false;
        }, err => {
          this.status_loading = false;
          console.log(err);
        });
      } else if (this.GetRole === 'PC') {
        this.status_loading = true;
        this.riskService.getRisk_PC(this.requestId).subscribe((res) => {
          this.ResData = res;

          this.FormData = this.ResData.data;
          for (let index = 0; index < this.FormData.length; index++) {
            // if (this.FormData[index].validate === true) {
            this.validate = this.FormData[index].validate;
            // }
            // tslint:disable-next-line: no-shadowed-variable
            const Data = this.FormData[index].readiness;
            // console.log('date:', Data);
            if (Data !== null) {
              for (let i = 0; i < Data.length; i++) {
                const Readeiness = Data[i].detail;
                Readeiness.forEach((item, _i) => {
                  // console.log('dueDate:',   item.dueDate);
                  if (item.dueDate !== null) {
                    const type = String(item.dueDate);
                    const year = Number(type.substr(0, 4)) + 543;
                    const month = type.substr(5, 2);
                    const day = type.substr(8, 2);
                    const date = day + '/' + month + '/' + year;
                    this.convertDatePC.push({ Convert: date });
                  } else {

                    this.convertDatePC.push({ Convert: '' });
                  }
                });
              }
            }
          }
          this.status_loading = false;
          console.log('res PC:', this.FormData);
        }, err => {
          this.status_loading = false;
          console.log(err);
        });
      } else {
        this.status_loading = true;
        this.riskService.getRisk_PO(this.requestId).subscribe((res) => {
          this.ResData = res;

          this.FormData = this.ResData.data;
          for (let index = 0; index < this.FormData.length; index++) {
            if (this.FormData[index].validate === true) {
              this.validate = this.FormData[index].validate;
            }

            // tslint:disable-next-line: no-shadowed-variable
            const Data = this.FormData[index].readiness;
            console.log('date:', Data);
            if (Data !== null) {
              for (let i = 0; i < Data.length; i++) {
                const Readeiness = Data[i].detail;
                Readeiness.forEach((item, _i) => {
                  // console.log('dueDate:',   item.dueDate.substr(0, 10));
                  if (item.dueDate !== null) {
                    const type = String(item.dueDate);
                    const year = Number(type.substr(0, 4)) + 543;
                    const month = type.substr(5, 2);
                    const day = type.substr(8, 2);
                    const date = day + '/' + month + '/' + year;
                    this.convertDatePO.push({ Convert: date });
                  } else {
                    this.convertDatePO.push({ Convert: '' });
                  }
                });
              }
            }
          }
          this.status_loading = false;
          console.log('res PO:', this.FormData);
        }
        , err => {
          this.status_loading = false;
          console.log(err);
        });
      }
    }
  }
  makeid(length) {
    let result = '';
    const characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
  addRow() {
    this.changeSaveDraft();
    const tempateAddRow = {
      validate: true,
      id: '',
      mainProcess: '',
      subProcess: [
        {
          validate: true,
          id: this.makeid(7),
          subTitle: '',
          inOut: null,
          itRelate: null,
          subProcessRisk: [
            {
              validate: true,
              id: '',
              risk: '',
              riskControl: '',
              riskControlOther: '',
              remark: null,
              subject: '',
              management: '',
            },
          ],
        },
      ],
    };

    for (let index = 0; index < this.FormData.length; index++) {
      if (this.FormData[index].validate === true) {
        this.FormData[index].processConsiders.push(tempateAddRow);
      }
    }
  }
  changeSaveDraft() {
    this.saveDraftstatus_State = true;
    this.riskService.status_state = true;
    console.log('saveDraftstatus:', this.saveDraftstatus_State)

  }
  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate');
    // return confirm();
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    console.log('000 sidebarService status:', status);
    if (this.saveDraftstatus_State === true) {
      // console.log('000 sidebarService status:', status);
      const dialogRef = this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        console.log('data::', data);
        if (data.returnStatus === true) {
          subject.next(true);
          this.savedData();
          this.saveDraftstatus_State = false;
        } else if (data.returnStatus === false) {
          console.log('000000 data::', data.returnStatus);
          this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
        }
      });
      console.log('8889999', subject);
      return subject;
    } else {
      return true;
    }
  }

  // new table ================================================================================
  checkStaticTitle(index) {
    return this.staticTitleIndex.includes(index);
  }

  drop(event: CdkDragDrop<string[]>) {
    for (let index = 0; index < this.FormData.length; index++) {
      if (this.FormData[index].validate === true) {
        console.log('drage:', this.FormData[index]);
      }
    }
    const fixedBody = this.staticTitleIndex;
    console.log('event Group:', event);
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (event.previousContainer.data.length > 1) {
      // check if only one left child
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (fixedBody.includes(this.dropIndex)) {
      // check only 1 child left and static type move only not delete
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      // const templatesubTitleRisk = {
      //   id: this.makeid(5),
      //   subTitle: '',
      //   inOut: '',
      //   itRelate: '',
      //   subProcessRisk: [
      //     {
      //       id: '',
      //       risk: '',
      //       riskControl: '',
      //       riskControlOther: '',
      //       remark: '',
      //       subject: '',
      //       management: '',
      //     },
      //   ],
      // };

      // DeleteRow[this.dropIndex].subProcess.push(
      //   templatesubTitleRisk
      // );
    } else {
      // check only 1 child left and not a static type move and delete
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      // remove current row
      this.removeAddItem(this.dropIndex);
      this.dropIndex = null;
    }
    this.changeSaveDraft();
  }

  dragExited(event: CdkDragExit<string[]>) {
    console.log('Exited', event);
  }

  entered(event: CdkDragEnter<string[]>) {
    console.log('Entered', event);
  }

  startDrag(_event: CdkDragStart<string[]>, istep) {
    // console.log('startDrag', event);
    console.log('startDrag index', istep);
    this.dropIndex = istep;
  }

  removeAddItem(index) {
    this.changeSaveDraft();
    if (index != null) {
      for (let i = 0; i < this.FormData.length; i++) {
        if (this.FormData[i].validate === true) {
          if (this.FormData[i].processConsiders[index].id !== '') {
            this.updateRisk.processConsidersDelete.push(
              this.FormData[i].processConsiders[index].id
            );
          }
          this.FormData[i].processConsiders.splice(index, 1);
        }
      }
    }
  }

  // ==================================================================
  dropItem(event: CdkDragDrop<string[]>) {
    this.changeSaveDraft();
    console.log('event item :', event);
    console.log('previousContainer :', event.container.data.length);
    console.log('index group', this.dropIndex);
    console.log('index item:', this.Risk_dropIndex);
    const fixedBody = this.staticTitleIndexRisk;

    // if (
    //   event.previousContainer.data.length === 1 &&
    //   event.container.data.length !== 1
    // ) {
    //   console.log('index:', this.dropIndex);
    //   this.removeAddItem(this.dropIndex);
    //   this.dropIndex = null;
    // }
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (event.previousContainer.data.length > 1) {
      // check if only one left child
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      // check only 1 child left and not a static type move and delete
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      // remove current row
      this.removeAddItemRisk(this.dropIndex, this.Risk_dropIndex);
      this.dropIndex = null;
      this.Risk_dropIndex = null;
    }
    this.changeSaveDraft();
  }
  connectItem(): any[] {
    const mapping = [];
    for (let index = 0; index < this.FormData.length; index++) {
      const Group = this.FormData[index];

      if (Group.validate === true) {
        for (let p = 0; p < Group.processConsiders.length; p++) {
          Group.processConsiders[p].subProcess.forEach((_item, i) =>
            mapping.push(`${Group.processConsiders[p].subProcess[i].id}`)
          );
        }
      }
    }

    // console.log('map', mapping);
    return mapping;
  }

  removeAddItemRisk(subIndex, Riskkindex) {
    this.changeSaveDraft();
    for (let index = 0; index < this.FormData.length; index++) {
      if (this.FormData[index].validate === true) {
        const DeleteGroup = this.FormData[index].processConsiders;

        if (DeleteGroup[subIndex].subProcess.length === 1) {
          this.updateRisk.processConsidersSubDelete.push(
            DeleteGroup[subIndex].id
          );
          DeleteGroup.splice(subIndex, 1);
        } else if (
          DeleteGroup[subIndex].subProcess.length > 1 &&
          Riskkindex != null
        ) {
          this.updateRisk.processConsidersSubDelete.push(
            DeleteGroup[subIndex].subProcess[Riskkindex].id
          );
          DeleteGroup[subIndex].subProcess.splice(Riskkindex, 1);
        }
      }
    }
  }

  startDrag_Risk(event: CdkDragStart<string[]>, isup, istep) {
    console.log('Risks tartDrag', event);
    console.log('Risk startDrag index', isup);
    this.Risk_dropIndex = isup;
    this.dropIndex = istep;
  }
  enteredRisk(event: CdkDragEnter<string[]>) {
    console.log('Entered Risk', event);
  }
  dragExitedRisk(event: CdkDragExit<string[]>) {
    console.log('Exited Risk', event);
  }

  dragEnded(event: CdkDragEnd) {
    console.log('dragEnded Event > item', event.source.data);
  }

  checkStaticTitle_Risk(index) {
    return this.staticTitleIndexRisk.includes(index);
  }

  removeDataItem(stepindex, subindex, riskIndex) {
    this.changeSaveDraft();
    console.log('stepindex', stepindex);
    console.log('subindex', subindex);
    console.log('riskIndex', riskIndex);
    const fixedBody = [];
    // tslint:disable-next-line: max-line-length
    for (let num = 0; num < this.FormData.length; num++) {
      if (this.FormData[num].validate === true) {
        const DeleteRow = this.FormData[num].processConsiders;

        if (
          fixedBody.includes(stepindex) &&
          DeleteRow[stepindex].subProcess[subindex].subProcessRisk.length > 1 &&
          this.Risk_dropIndex !== 0
        ) {
          console.log('delete fixed row only have many risk[]');
          // this.FormData.operations[stepindex].detailDelete.push(this.FormData.operations[stepindex].subProcess[subindex]['id']);
          // console.log(
          //   DeleteRow[stepindex].subProcess[subindex]
          //     .subProcessRisk[riskIndex].id
          // );
          if (
            DeleteRow[stepindex].subProcess[subindex].subProcessRisk[riskIndex]
              .id !== ''
          ) {
            this.updateRisk.processConsidersSubRiskDelete.push(
              DeleteRow[stepindex].subProcess[subindex].subProcessRisk[
                riskIndex
              ].id
            );
          }

          DeleteRow[stepindex].subProcess[subindex].subProcessRisk.splice(
            riskIndex,
            1
          );
        } else if (
          fixedBody.includes(stepindex) &&
          DeleteRow[stepindex].subProcess[subindex].subProcessRisk.length === 1
        ) {
          console.log('3 clear fixed row only');
          if (subindex !== 0) {
            if (DeleteRow[stepindex].subProcess[subindex].id !== '') {
              this.updateRisk.processConsidersSubDelete.push(
                DeleteRow[stepindex].subProcess[subindex].id
              );
            }
            DeleteRow[stepindex].subProcess.splice(subindex, 1);
          } else {
            this.updateRisk.processConsidersSubDelete.push(
              DeleteRow[stepindex].subProcess[subindex].id
            );

            Object.keys(DeleteRow[stepindex].subProcess[subindex]).forEach(
              (key, _index) => {
                if (key === 'subProcessRisk') {
                  // tslint:disable-next-line: no-shadowed-variable
                  for (
                    // tslint:disable-next-line: no-shadowed-variable
                    let index = 0;
                    index <
                    DeleteRow[stepindex].subProcess[subindex][key].length;
                    index++
                  ) {
                    Object.keys(
                      DeleteRow[stepindex].subProcess[subindex][key][riskIndex]
                    ).forEach((sub, _i) => {
                      DeleteRow[stepindex].subProcess[subindex][key][index][
                        sub
                      ] = '';
                    });
                  }
                } else {
                  DeleteRow[stepindex].subProcess[subindex][key] = '';
                }
              }
            );
          }

          // console.log(
          //   'tt',
          //   this.FormData.operations[stepindex].subProcess[subindex]
          // );

          // this.FormData.operations[stepindex].detailDelete.push(
          //   this.FormData.operations[stepindex].subProcess[subindex]['id']
          // );
        } else if (
          DeleteRow[stepindex].subProcess.length === 1 &&
          DeleteRow[stepindex].subProcess[subindex].subProcessRisk.length > 1
        ) {
          console.log(' delete risk row only have 1 row');
          if (
            DeleteRow[stepindex].subProcess[subindex].subProcessRisk[riskIndex]
              .id !== ''
          ) {
            this.updateRisk.processConsidersSubRiskDelete.push(
              DeleteRow[stepindex].subProcess[subindex].subProcessRisk[
                riskIndex
              ].id
            );
          }
          DeleteRow[stepindex].subProcess[subindex].subProcessRisk.splice(
            riskIndex,
            1
          );
        } else if (
          DeleteRow[stepindex].subProcess.length > 1 &&
          DeleteRow[stepindex].subProcess[subindex].subProcessRisk.length > 1
        ) {
          console.log(' delete risk row only have many row');
          // this.FormData.operations[stepindex].detailDelete.push(
          //   this.FormData.operations[stepindex].subProcess[subindex]['id']
          // );
          if (
            DeleteRow[stepindex].subProcess[subindex].subProcessRisk[riskIndex]
              .id !== ''
          ) {
            this.updateRisk.processConsidersSubRiskDelete.push(
              DeleteRow[stepindex].subProcess[subindex].subProcessRisk[
                riskIndex
              ].id
            );
          }
          DeleteRow[stepindex].subProcess[subindex].subProcessRisk.splice(
            riskIndex,
            1
          );
        } else if (
          DeleteRow[stepindex].subProcess[subindex].subProcessRisk.length === 0
        ) {
          console.log('delete row permanent', DeleteRow[stepindex].id);
          // this.FormData.operationsDelete.push(
          //   this.FormData.operations[stepindex].id
          // );
          if (DeleteRow[stepindex].id !== '') {
            this.updateRisk.processConsidersDelete.push(
              DeleteRow[stepindex].id
            );
            DeleteRow.splice(stepindex, 1);
          }
        } else if (
          DeleteRow[stepindex].subProcess[subindex].subProcessRisk.length ===
          1 &&
          subindex !== 0 &&
          DeleteRow[stepindex].subProcess.length > 0
        ) {
          console.log('delete row have 1 sub , 1 risk');
          if (DeleteRow[stepindex].subProcess[subindex].id !== '') {
            this.updateRisk.processConsidersSubDelete.push(
              DeleteRow[stepindex].subProcess[subindex].id
            );
            DeleteRow[stepindex].subProcess.splice(subindex, 1);
          }
        } else if (
          DeleteRow[stepindex].subProcess[subindex].subProcessRisk.length ===
          1 &&
          subindex === 0 &&
          DeleteRow[stepindex].subProcess.length === 1
        ) {
          console.log('delete row have 1 sub , 1 risk and delete main row');
          if (DeleteRow[stepindex].id !== '') {
            this.updateRisk.processConsidersDelete.push(
              DeleteRow[stepindex].id
            );
          }
          DeleteRow.splice(stepindex, 1);
        } else if (
          DeleteRow[stepindex].subProcess[subindex].subProcessRisk.length ===
          1 &&
          subindex === 0 &&
          DeleteRow[stepindex].subProcess.length > 1
        ) {
          console.log('delete row have many sub , 1 risk and delete 1 sub ');
          if (DeleteRow[stepindex].subProcess[subindex].id !== '') {
            this.updateRisk.processConsidersSubDelete.push(
              DeleteRow[stepindex].subProcess[subindex].id
            );
          }
          DeleteRow[stepindex].subProcess.splice(subindex, 1);
        } else {
          console.log('not in case');
        }
      }
    }
    this.changeSaveDraft();
  }

  Add_commentOperation_1() {
    this.changeSaveDraft();
    const extraComment = {
      id: '',
      subject: '',
      management: '',
      poComment: null,
    };
    for (let index = 0; index < this.FormData.length; index++) {
      if (this.FormData[index].validate === true) {
        this.FormData[index].extraComment.push(extraComment);
      }
    }
  }

  savedData() {
    console.log('saveDraftstatus:', this.saveDraftstatus);
    console.log('formData:', this.FormData);
    this.saveDraftstatus_State = false;
    // this.riskService.status_state = false;
    this.updateRisk.requestId = Number(this.requestId);
    if (this.GetRole !== 'PO' && this.GetRole !== 'PC') {
      for (let index = 0; index < this.FormData.length; index++) {
        // if (this.FormData[index].validate === true) {
        const UpdateData = this.FormData[index];
        if (UpdateData.role === String(localStorage.getItem('level'))) {
          for (let t = 0; t < UpdateData.processConsiders.length; t++) {
            this.updateRisk.processConsiders.push(
              UpdateData.processConsiders[t]
            );
          }

          if (UpdateData.readiness !== null) {
            for (let t = 0; t < UpdateData.readiness.length; t++) {
              this.updateRisk.readiness.push(UpdateData.readiness[t]);
            }
          }

          for (let t = 0; t < UpdateData.extraComment.length; t++) {
            this.updateRisk.extraComment.push(UpdateData.extraComment[t]);
          }
        }

        // console.log( this.updateRisk);
        // console.log(UpdateData);
      }

      for (
        let index = 0;
        index < this.updateRisk.processConsiders.length;
        index++
      ) {
        const Group = this.updateRisk.processConsiders[index].subProcess;

        for (let n = 0; n < Group.length; n++) {
          const subProcess = Group[n].id;
          if (typeof subProcess === 'string') {
            // console.log('subProcess', subProcess);
            this.updateRisk.processConsiders[index].subProcess[n].id = '';
          }
        }

        if (Group.length === 0) {
          this.updateRisk.processConsidersDelete.push(
            this.updateRisk.processConsiders[index].id
          );
          this.updateRisk.processConsiders.splice(index, 1);
        }
      }
      for (
        let index = 0;
        index < this.updateRisk.processConsiders.length;
        index++
      ) {
        if (
          this.updateRisk.processConsiders[index].id === '' &&
          this.updateRisk.processConsiders[index].mainProcess === ''
        ) {
          this.updateRisk.processConsiders.splice(index, 1);
        }
      }
      for (
        let index = 0;
        index < this.updateRisk.processConsiders.length;
        index++
      ) {
        if (
          this.updateRisk.processConsiders[index].id === '' &&
          this.updateRisk.processConsiders[index].mainProcess === ''
        ) {
          this.updateRisk.processConsiders.splice(index, 1);
        }
      }

      for (
        let i = 0;
        i < this.updateRisk.processConsidersSubDelete.length;
        i++
      ) {
        if (typeof this.updateRisk.processConsidersSubDelete[i] === 'string') {
          this.updateRisk.processConsidersSubDelete.splice(i, 1);
        }
      }

      for (let w = 0; w < this.updateRisk.extraComment.length; w++) {
        if (
          this.updateRisk.extraComment[w].subject === '' &&
          this.updateRisk.extraComment[w].management === ''
        ) {
          console.log('id:', this.updateRisk.extraComment[w].id);
          this.updateRisk.extraCommentDelete.push(
            this.updateRisk.extraComment[w].id
          );
          this.updateRisk.extraComment.splice(w, 1);
        }
      }
      for (let w = 0; w < this.updateRisk.extraComment.length; w++) {
        if (
          this.updateRisk.extraComment[w].subject === '' &&
          this.updateRisk.extraComment[w].management === ''
        ) {
          console.log('id:', this.updateRisk.extraComment[w].id);
          this.updateRisk.extraCommentDelete.push(
            this.updateRisk.extraComment[w].id
          );
          this.updateRisk.extraComment.splice(w, 1);
        }
      }
      this.BU_statusUpdate = true;
      for (
        let index = 0;
        index < this.updateRisk.processConsiders.length;
        index++
      ) {
        if (
          this.updateRisk.processConsiders[index].id === null &&
          this.updateRisk.processConsiders[index].mainProcess === ''
        ) {
          this.updateRisk.processConsiders.splice(index, 1);
          this.BU_statusUpdate = true;
        } else if (
          this.updateRisk.processConsiders[index].id === null &&
          this.updateRisk.processConsiders[index].mainProcess !== ''
        ) {
          const subProcess = this.updateRisk.processConsiders[index].subProcess;
          for (let r = 0; r < subProcess.length; r++) {
            if (
              subProcess[r].inOut === null ||
              subProcess[r].itRelate === null
            ) {
              alert('กรุณาเลือกข้อมูล IN/OUT หรือ IT Related ให้ถูกต้อง');
              this.BU_statusUpdate = false;
              break;
            }
          }
          break;
        }
        else {
          this.BU_statusUpdate = true;
        }
        // console.log('FormData:', this.FormData);
      }

      console.log('update:', this.updateRisk);
      console.log('processConsiders:', this.updateRisk.processConsiders);
      console.log('BU_statusUpdate:', this.BU_statusUpdate);

      this.update();

    } else if (this.GetRole === 'PO') {
      // console.log('role', this.GetRole);
      // console.log('dataPo', this.FormData);
      for (let ii = 0; ii < this.FormData.length; ii++) {
        const PO_risk = this.FormData[ii];

        for (let index = 0; index < PO_risk.processConsiders.length; index++) {
          const Main = PO_risk.processConsiders[index].subProcess;
          // tslint:disable-next-line: no-shadowed-variable
          for (let i = 0; i < Main.length; i++) {
            const SUB = Main[i].subProcessRisk;
            if (SUB.length !== 0) {
              for (let k = 0; k < SUB.length; k++) {
                if (SUB[k].buCommentId !== null) {
                  this.CommentPO.processConsiders.push({
                    buCommentId: SUB[k].buCommentId,
                    poCommentRemark: SUB[k].poCommentRemark,
                    poComment: SUB[k].poComment,
                  });
                }
              }
            }
          }
        }

        if (PO_risk.readiness !== null) {
          for (let index = 0; index < PO_risk.readiness.length; index++) {
            const readiness = PO_risk.readiness[index].detail;
            // console.log('Main:', commentOperation);
            // tslint:disable-next-line: no-shadowed-variable
            for (let i = 0; i < readiness.length; i++) {
              if (readiness[i].buCommentId !== null) {
                this.CommentPO.readiness.push({
                  buCommentId: readiness[i].buCommentId,
                  poCommentRemark: readiness[i].poCommentRemark,
                  poComment: readiness[i].poComment,
                });
              } else {
                this.CommentPO.readiness.push({
                  buCommentId: readiness[i].id,
                  poCommentRemark: readiness[i].poCommentRemark,
                  poComment: readiness[i].poComment,
                });
              }
            }
          }
        }

        if (PO_risk.extraComment.length !== 0) {
          for (let index = 0; index < PO_risk.extraComment.length; index++) {
            const extraComment = PO_risk.extraComment;
            // console.log('Main:', commentOperation1);
            // tslint:disable-next-line: no-shadowed-variable

            this.CommentPO.extraComment.push({
              buCommentId: extraComment[index].id,
              poCommentRemark: extraComment[index].poCommentRemark,
              poComment: extraComment[index].poComment,
            });
          }
        }
      }

      // const check_processConsiders = Object.values(
      //   this.CommentPO.processConsiders
      // ).every(
      //   (value) => value['poComment'] !== null && value['poComment'] !== ''
      // );
      // const check_poCommentRemark = Object.values(
      //   this.CommentPO.processConsiders
      // ).every(
      //   (value) =>
      //     value['poCommentRemark'] !== null && value['poCommentRemark'] !== ''
      // );

      // const check_readiness_poComment = Object.values(
      //   this.CommentPO.readiness
      // ).every(
      //   (value) => value['poComment'] !== null && value['poComment'] !== ''
      // );
      // const check_readiness_poCommentRemark = Object.values(
      //   this.CommentPO.readiness
      // ).every(
      //   (value) =>
      //     value['poCommentRemark'] !== null && value['poCommentRemark'] !== ''
      // );

      // const check_extra_poComment = Object.values(
      //   this.CommentPO.extraComment
      // ).every(
      //   (value) => value['poComment'] !== null && value['poComment'] !== ''
      // );
      // const check_extra_poCommentRemark = Object.values(
      //   this.CommentPO.extraComment
      // ).every(
      //   (value) =>
      //     value['poCommentRemark'] !== null && value['poCommentRemark'] !== ''
      // );
      // console.log('check_readiness_poComment:>>>>>', check_readiness_poComment, check_readiness_poCommentRemark);
      // console.log('processConsiders:>>>>>', check_poCommentRemark, check_processConsiders);
      // console.log('extraComment:>>>>>', this.CommentPO.extraComment, check_extra_poComment, check_extra_poCommentRemark);

      // if (
      //   check_processConsiders === true &&
      //   check_poCommentRemark === true &&
      //   check_readiness_poComment === true &&
      //   check_readiness_poCommentRemark === true &&
      //   check_extra_poComment === true &&
      //   check_extra_poCommentRemark === true
      // ) {
      //   this.CommentPO.status = true;
      // }

      // console.log('this.CommentPO', this.CommentPO);

      this.CommentPO.requestId = Number(this.requestId);
      const arr = this.CheckValueSandWorkPO(this.CommentPO);
      if (arr.length == 0) {
        this.CommentPO.statue = true
      }
      this.status_loading = true;
      this.riskService.UpdataCommentRisk_PO(this.CommentPO).subscribe(
        (res) => {
          // console.log('res PO:', res);
          if (res['status'] === 'success') {
            this.CommentPO = {
              status: false,
              requestId: null,
              processConsiders: [],
              readiness: [],
              extraComment: [],
            }
            this.alertMessage = 'บันทึกความเห็นของ' + this.Getname + 'เรียบร้อย';
            this.saveDraftstatus = 'success';
            setTimeout(() => {
              this.saveDraftstatus = 'fail';
            }, 3000);
            // this.CommentPO.status = false;
            this.GetRest();
            this.status_loading = false;

          } else {
            Swal.fire({
              title: 'error',
              text: res['message'],
              icon: 'error',
            });
            this.status_loading = false;
          }
        },
        (err) => {
          this.status_loading = false;
          console.log(err);
          alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
        }
      );
    } else if (this.GetRole === 'PC') {
      // console.log('role', this.GetRole);
      // console.log('dataPC', this.FormData);

      for (let ii = 0; ii < this.FormData.length; ii++) {
        const PO_risk = this.FormData[ii];

        for (let index = 0; index < PO_risk.processConsiders.length; index++) {
          const Main = PO_risk.processConsiders[index].subProcess;
          // tslint:disable-next-line: no-shadowed-variable
          for (let i = 0; i < Main.length; i++) {
            const SUB = Main[i].subProcessRisk;
            if (SUB.length !== 0) {
              for (let k = 0; k < SUB.length; k++) {
                if (SUB[k].buCommentId !== null) {
                  this.Comment_PC.processConsiders.push({
                    buCommentId: SUB[k].buCommentId,
                    pcComment: SUB[k].pcComment,
                    pcExtraComment: SUB[k].pcExtraComment,
                    pcIssue: SUB[k].pcIssue,
                  });
                }
              }
            }
          }
        }

        if (PO_risk.readiness !== null) {
          for (let index = 0; index < PO_risk.readiness.length; index++) {
            const readiness = PO_risk.readiness[index].detail;
            // console.log('Main:', commentOperation);
            // tslint:disable-next-line: no-shadowed-variable
            for (let i = 0; i < readiness.length; i++) {
              if (readiness[i].buCommentId !== null) {
                this.Comment_PC.readiness.push({
                  buCommentId: readiness[i].buCommentId,
                  pcComment: readiness[i].pcComment,
                  pcExtraComment: readiness[i].pcExtraComment,
                  pcIssue: readiness[i].pcIssue,
                });
              } else {
                this.Comment_PC.readiness.push({
                  buCommentId: readiness[i].id,
                  pcComment: readiness[i].pcComment,
                  pcExtraComment: readiness[i].pcExtraComment,
                  pcIssue: readiness[i].pcIssue,
                });
              }
            }
          }
        }

        if (PO_risk.extraComment.length !== 0) {
          for (let index = 0; index < PO_risk.extraComment.length; index++) {
            const extraComment = PO_risk.extraComment;
            // console.log('Main:', commentOperation1);
            // tslint:disable-next-line: no-shadowed-variable

            this.Comment_PC.extraComment.push({
              buCommentId: extraComment[index].id,
              pcComment: extraComment[index].pcComment,
              pcExtraComment: extraComment[index].pcExtraComment,
              pcIssue: extraComment[index].pcIssue,
            });
          }
        }
      }
      console.log('Comment PC:', this.Comment_PC);

      const check_extra_pcIssue = Object.values(
        this.Comment_PC.extraComment
      ).every((value) => value['pcIssue'] !== null && value['pcIssue'] !== '');
      const check_extra_pcExtraComment = Object.values(
        this.Comment_PC.extraComment
      ).every(
        (value) =>
          value['pcExtraComment'] !== null && value['pcExtraComment'] !== ''
      );
      const check_extra_pcComment = Object.values(
        this.Comment_PC.extraComment
      ).every(
        (value) => value['pcComment'] !== null && value['pcComment'] !== ''
      );

      const check_process_pcIssue = Object.values(
        this.Comment_PC.processConsiders
      ).every((value) => value['pcIssue'] !== null && value['pcIssue'] !== '');
      const check_process_pcExtraComment = Object.values(
        this.Comment_PC.processConsiders
      ).every(
        (value) =>
          value['pcExtraComment'] !== null && value['pcExtraComment'] !== ''
      );
      const check_process_pcComment = Object.values(
        this.Comment_PC.processConsiders
      ).every(
        (value) => value['pcComment'] !== null && value['pcComment'] !== ''
      );

      const check_readiness_pcIssue = Object.values(
        this.Comment_PC.readiness
      ).every((value) => value['pcIssue'] !== null && value['pcIssue'] !== '');
      const check_readiness_pcExtraComment = Object.values(
        this.Comment_PC.readiness
      ).every(
        (value) =>
          value['pcExtraComment'] !== null && value['pcExtraComment'] !== ''
      );
      const check_readiness_pcComment = Object.values(
        this.Comment_PC.readiness
      ).every(
        (value) => value['pcComment'] !== null && value['pcComment'] !== ''
      );

      console.log('Comment PC:', this.Comment_PC.extraComment, check_extra_pcIssue, check_extra_pcExtraComment, check_extra_pcComment);
      console.log('Comment PC:', this.Comment_PC.processConsiders, check_process_pcIssue, check_process_pcExtraComment, check_process_pcComment);
      console.log('Comment PC:', this.Comment_PC.readiness, check_readiness_pcIssue, check_readiness_pcExtraComment, check_readiness_pcComment);

      if (
        check_extra_pcIssue === true &&
        check_extra_pcExtraComment === true &&
        check_extra_pcComment === true &&
        check_process_pcIssue === true &&
        check_process_pcExtraComment === true &&
        check_process_pcComment === true &&
        check_readiness_pcIssue === true &&
        check_readiness_pcExtraComment === true &&
        check_readiness_pcComment === true
      ) {
        this.Comment_PC.status = true;
      }

      console.log('this.Comment_PC.status', this.Comment_PC.status);
      // const check_extra_pcExtraComment = Object.values(this.CommentPO.extraComment).every(value => value['poCommentRemark'] !== null && value['poCommentRemark'] !== '');
      this.Comment_PC.requestId = Number(this.requestId);
      this.status_loading = true;
      this.riskService.UpdataCommentRisk_PC(this.Comment_PC).subscribe(
        (res) => {
          console.log('res PO:', res);
          if (res['status'] === 'success') {
            this.alertMessage = 'บันทึกความเห็นของคณะกรรมการผลิตภัณฑ์เรียบร้อย';
            this.saveDraftstatus = 'success';
            // this.Comment_PC.status = false;
            this.Comment_PC = {
              status: false,
              requestId: null,
              processConsiders: [],
              readiness: [],
              extraComment: [],
            };
            this.GetRest();
            this.status_loading = false;
          } else {
            Swal.fire({
              title: 'error',
              text: res['message'],
              icon: 'error',
            });
            this.status_loading = false;
          }
        },
        (err) => {
          this.status_loading = false;
          console.log(err);
          alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
        }
      );

    } else {
      Swal.fire({
        title: 'error',
        text: 'ไม่สามรถบันทึกข้มูลได้',
        icon: 'error',
      });
    }
  }

  validateCommentBu() {
    // console.log('this.validateBu', this.updateRisk);
    // processConsiders>>subProcess>>subProcessRisk.remark/management/subject
    this.updateRisk.processConsiders.forEach((process, i) => {
      process.subProcess.forEach((sub) => {
        sub.subProcessRisk.forEach((subRisk) => {
          if (subRisk.management === '' || subRisk.management === null) {
            this.validateBu = this.validateBu + 1;
          }
          if (subRisk.remark === '' || subRisk.remark === null) {
            this.validateBu = this.validateBu + 1;
          }
          if (subRisk.subject === '' || subRisk.subject === null) {
            this.validateBu = this.validateBu + 1;
          }
        });
      });
    });
    this.updateRisk.processConsiders.forEach((process, i) => {
      process.subProcess.forEach((sub) => {
        sub.subProcessRisk.forEach((subRisk) => {
          if (subRisk.management === '' || subRisk.management === null) {
            this.validateBu = this.validateBu + 1;
          }
          if (subRisk.remark === '' || subRisk.remark === null) {
            this.validateBu = this.validateBu + 1;
          }
          if (subRisk.subject === '' || subRisk.subject === null) {
            this.validateBu = this.validateBu + 1;
          }
        });
      });
    });

    // readiness>>detail.remark/commentSubject/commentManagement
    if (
      this.updateRisk.readiness.length !== 0 &&
      this.updateRisk.readiness !== null
    ) {
      this.updateRisk.readiness.forEach((process, i) => {
        process.detail.forEach((subRisk) => {
          if (subRisk.remark === '' || subRisk.remark === null) {
            this.validateBu = this.validateBu + 1;
          }
          if (
            subRisk.commentSubject === '' ||
            subRisk.commentSubject === null
          ) {
            this.validateBu = this.validateBu + 1;
          }
          if (
            subRisk.commentManagement === '' ||
            subRisk.commentManagement === null
          ) {
            this.validateBu = this.validateBu + 1;
          }
        });
      });
    }

    // console.log(this.validateBu);
    // console.log(this.GetRest);

    return this.validateBu;
  }

  update() {
    this.validateCommentBu();
    // if (this.validateBu > 0) {
    // alert('กรุณากรอกข้อมูลให้ครบถ้วน');
    //   this.updateRisk = {
    //     requestId: Number(localStorage.getItem('requestId')),
    //     processConsiders: [],
    //     processConsidersDelete: [],
    //     processConsidersSubDelete: [],
    //     processConsidersSubRiskDelete: [],
    //     readiness: [],
    //     readinessDelete: [],
    //     readinessDetailDelete: [],
    //     extraComment: [],
    //     extraCommentDelete: [],
    //   };
    //   this.CommentPO.requestId = Number(localStorage.getItem('requestId'));
    //   this.CommentPO.processConsiders = [];
    //   this.CommentPO.readiness = [];
    //   this.CommentPO.extraComment = [];

    //   this.Comment_PC.requestId = Number(localStorage.getItem('requestId'));
    //   this.Comment_PC.processConsiders = [];
    //   this.Comment_PC.readiness = [];
    //   this.Comment_PC.extraComment = [];
    //   this.validateBu = 0;
    // } else {
    console.log(this.updateRisk);
    this.status_loading = true;
    this.riskService.UpdateRisk(this.updateRisk).subscribe(
      (res) => {
        console.log('res BU:', res);
        if (res['status'] === 'success') {
          if (this.Getlevel !== 'พนักงาน') {
            this.alertMessage = 'บันทึกความเห็นของ' + this.Getlevel + 'เรียบร้อย';
          } else {
            this.alertMessage = 'บันทึกความเห็นของสายงานบริหารความเสี่ยงเรียบร้อย';
          }

          this.saveDraftstatus = 'success';
          setTimeout(() => {
            this.saveDraftstatus = 'fail';
          }, 3000);
          this.CommentPO.requestId = null;
          this.CommentPO.processConsiders = [];
          this.CommentPO.readiness = [];
          this.CommentPO.extraComment = [];

          this.Comment_PC.requestId = null;
          this.Comment_PC.processConsiders = [];
          this.Comment_PC.readiness = [];
          this.Comment_PC.extraComment = [];

          (this.updateRisk.requestId = null),
            (this.updateRisk.processConsiders = []);
          this.updateRisk.processConsidersDelete = [];
          this.updateRisk.processConsidersSubDelete = [];
          this.updateRisk.processConsidersSubRiskDelete = [];
          this.updateRisk.readiness = [];
          this.updateRisk.readinessDelete = [];
          this.updateRisk.readinessDetailDelete = [];
          this.updateRisk.extraComment = [];
          this.updateRisk.extraCommentDelete = [];
          this.FormData = [];
          this.GetRest();
          this.status_loading = false;
        } else {
          Swal.fire({
            title: 'error',
            text: res['message'],
            icon: 'error',
          });
          this.status_loading = false;
        }
      },
      (err) => {
        console.log(err);
        alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
      }
    );
    // }
  }
  sendWork() {
    this.saveDraftstatus_State = false;
    this.riskService.status_state = false;
    if (
      this.GetRole !== 'PO' &&
      this.GetRole !== 'PC' &&
      this.GetRole === 'Risk'
    ) {

      for (let index = 0; index < this.FormData.length; index++) {
        // if (this.FormData[index].validate === true) {
        const UpdateData = this.FormData[index];
        if (UpdateData.role === String(localStorage.getItem('level'))) {
          for (let t = 0; t < UpdateData.processConsiders.length; t++) {
            this.updateRisk.processConsiders.push(
              UpdateData.processConsiders[t]
            );
          }

          if (UpdateData.readiness !== null) {
            for (let t = 0; t < UpdateData.readiness.length; t++) {
              this.updateRisk.readiness.push(UpdateData.readiness[t]);
            }
          }

          for (let t = 0; t < UpdateData.extraComment.length; t++) {
            this.updateRisk.extraComment.push(UpdateData.extraComment[t]);
          }
        }

        // console.log( this.updateRisk);
        // console.log(UpdateData);
      }

      for (
        let index = 0;
        index < this.updateRisk.processConsiders.length;
        index++
      ) {
        const Group = this.updateRisk.processConsiders[index].subProcess;

        for (let n = 0; n < Group.length; n++) {
          const subProcess = Group[n].id;
          if (typeof subProcess === 'string') {
            // console.log('subProcess', subProcess);
            this.updateRisk.processConsiders[index].subProcess[n].id = '';
          }
        }

        if (Group.length === 0) {
          this.updateRisk.processConsidersDelete.push(
            this.updateRisk.processConsiders[index].id
          );
          this.updateRisk.processConsiders.splice(index, 1);
        }
      }
      for (
        let index = 0;
        index < this.updateRisk.processConsiders.length;
        index++
      ) {
        if (
          this.updateRisk.processConsiders[index].id === '' &&
          this.updateRisk.processConsiders[index].mainProcess === ''
        ) {
          this.updateRisk.processConsiders.splice(index, 1);
        }
      }
      for (
        let index = 0;
        index < this.updateRisk.processConsiders.length;
        index++
      ) {
        if (
          this.updateRisk.processConsiders[index].id === '' &&
          this.updateRisk.processConsiders[index].mainProcess === ''
        ) {
          this.updateRisk.processConsiders.splice(index, 1);
        }
      }

      for (
        let i = 0;
        i < this.updateRisk.processConsidersSubDelete.length;
        i++
      ) {
        if (typeof this.updateRisk.processConsidersSubDelete[i] === 'string') {
          this.updateRisk.processConsidersSubDelete.splice(i, 1);
        }
      }

      for (let w = 0; w < this.updateRisk.extraComment.length; w++) {
        if (
          this.updateRisk.extraComment[w].subject === '' &&
          this.updateRisk.extraComment[w].management === ''
        ) {
          console.log('id:', this.updateRisk.extraComment[w].id);
          this.updateRisk.extraCommentDelete.push(
            this.updateRisk.extraComment[w].id
          );
          this.updateRisk.extraComment.splice(w, 1);
        }
      }
      for (let w = 0; w < this.updateRisk.extraComment.length; w++) {
        if (
          this.updateRisk.extraComment[w].subject === '' &&
          this.updateRisk.extraComment[w].management === ''
        ) {
          console.log('id:', this.updateRisk.extraComment[w].id);
          this.updateRisk.extraCommentDelete.push(
            this.updateRisk.extraComment[w].id
          );
          this.updateRisk.extraComment.splice(w, 1);
        }
      }

      for (
        let index = 0;
        index < this.updateRisk.processConsiders.length;
        index++
      ) {
        if (
          this.updateRisk.processConsiders[index].id === null &&
          this.updateRisk.processConsiders[index].mainProcess === ''
        ) {
          this.updateRisk.processConsiders.splice(index, 1);
          this.BU_statusUpdate = true;
        } else if (
          this.updateRisk.processConsiders[index].id === null &&
          this.updateRisk.processConsiders[index].mainProcess !== ''
        ) {
          const subProcess = this.updateRisk.processConsiders[index].subProcess;
          for (let r = 0; r < subProcess.length; r++) {
            if (
              subProcess[r].inOut === null ||
              subProcess[r].itRelate === null
            ) {
              alert('กรุณาเลือกข้อมูล IN/OUT หรือ IT Related ให้ถูกต้อง');
              this.BU_statusUpdate = false;
              break;
            }
          }

          break;
        } else {
          this.BU_statusUpdate = true;
        }
      }
      this.BU_statusUpdate = true;
      for (let k = 0; k < this.updateRisk.processConsiders.length; k++) {
        // console.log('risk:', this.updateRisk.processConsiders[k]);
        if (this.updateRisk.processConsiders[k].mainProcess == '' ||
          this.updateRisk.processConsiders[k].mainProcess == null) {
          this.BU_statusUpdate = false;
          console.log('mainProcess:', this.updateRisk.processConsiders[k].mainProcess);
          break;
        }
        else {
          this.updateRisk.processConsiders[k].subProcess.forEach(subProcess => {
            // for(let j = 0; j < subProcess.length; j++) {
            if (
              (subProcess.subTitle === '' || subProcess.subTitle === null) ||
              (subProcess.inOut === '' || subProcess.inOut === null) ||
              (subProcess.itRelate === '' || subProcess.itRelate === null)
            ) {
              // console.log('subTitle:', subProcess.subTitle);
              this.BU_statusUpdate = false;
            }
            else {
              subProcess.subProcessRisk.forEach(risk => {
                if ((risk.risk === '' || risk.risk === null) ||
                  (risk.riskControl === '' || risk.riskControl === null) ||
                  (risk.riskControlOther === '' || risk.riskControlOther === null)) {
                  console.log('false');
                  this.BU_statusUpdate = false;
                }
              })
            }
          })
        }
      }

      // check varriable if empty and null
      this.statusprocessConsiders = true;
      this.statusReadiness = true;
      this.statusextraComment = true;
      console.log("processConsiders:", this.updateRisk.processConsiders);

      for (let i = 0; i < this.updateRisk.processConsiders.length; i++) {
        if (this.updateRisk.processConsiders[i].mainProcess === '' ||
          this.updateRisk.processConsiders[i].mainProcess === null) {
          this.statusprocessConsiders = false;
        } else {
          for (let k = 0; k < this.updateRisk.processConsiders[i].subProcess.length; k++) {
            const subProcess = this.updateRisk.processConsiders[i].subProcess[k]
            if ((subProcess.subTitle === '' || subProcess.subTitle === null) ||
              (subProcess.inOut === '' || subProcess.inOut === null) ||
              (subProcess.itRelate === '' || subProcess.itRelate === null)) {
              this.statusprocessConsiders = false;
            }
            else {
              subProcess.subProcessRisk.forEach(risk => {
                if ((risk.risk === '' || risk.risk === null) ||
                  (risk.riskControl === '' || risk.riskControl === null) ||
                  (risk.riskControlOther === '' || risk.riskControlOther === null) ||
                  (risk.remark === '' || risk.remark === null) ||
                  (risk.subject === '' || risk.subject === null) ||
                  (risk.management === '' || risk.management === null)) {
                  console.log('false');
                  this.statusprocessConsiders = false;
                }
              })
            }
          }
        }
      }

      for (let i = 0; i < this.updateRisk.readiness.length; i++) {
        for (let g = 0; g < this.updateRisk.readiness[i].detail.length; g++) {
          if ((this.updateRisk.readiness[i].detail[g].remark == '' ||
            this.updateRisk.readiness[i].detail[g].remark == null) ||
            (this.updateRisk.readiness[i].detail[g].commentManagement == '' ||
              this.updateRisk.readiness[i].detail[g].commentManagement == null) ||
            (this.updateRisk.readiness[i].detail[g].commentSubject == '' ||
              this.updateRisk.readiness[i].detail[g].commentSubject == null)) {
            this.statusReadiness = false
          }
        }
      }

      for (let i = 0; i < this.updateRisk.extraComment.length; i++) {
        if ((this.updateRisk.extraComment[i].subject == '' ||
          this.updateRisk.extraComment[i].subject == null) ||
          (this.updateRisk.extraComment[i].management == '' ||
            this.updateRisk.extraComment[i].management == null)) {
          this.statusextraComment = false
        }
      }
      this.validateCommentBu();
      this.arrayStatus = [];
      // console.log("readiness:", this.updateRisk.readiness);
      console.log("statusprocessConsiders:", this.statusprocessConsiders);
      console.log("statusReadiness:", this.statusReadiness);
      console.log("statusextraComment:", this.statusextraComment);
      console.log("validate:", this.validateBu);
      console.log("BU_statusUpdate::", this.BU_statusUpdate);
      if ((this.validateBu > 0 || this.BU_statusUpdate === false) && this.statusprocessConsiders == false) {
        // alert('กรุณากรอกข้อมูลตารางรายละเอียดการทำงาน ให้ครบถ้วน');
        this.arrayStatus.push('ตารางรายละเอียดการทำงาน')
        this.updateRisk = {
          status: false,
          requestId: Number(localStorage.getItem('requestId')),
          processConsiders: [],
          processConsidersDelete: [],
          processConsidersSubDelete: [],
          processConsidersSubRiskDelete: [],
          readiness: [],
          readinessDelete: [],
          readinessDetailDelete: [],
          extraComment: [],
          extraCommentDelete: [],
        };
        this.CommentPO.requestId = Number(localStorage.getItem('requestId'));
        this.CommentPO.processConsiders = [];
        this.CommentPO.readiness = [];
        this.CommentPO.extraComment = [];
        this.Comment_PC.requestId = Number(localStorage.getItem('requestId'));
        this.Comment_PC.processConsiders = [];
        this.Comment_PC.readiness = [];
        this.Comment_PC.extraComment = [];
      }
      if (this.statusReadiness == false) {
        // alert('กรุณากรอกข้อมูลตารางความพร้อมด้านเทคโนโลยีสารสนเทศ ให้ครบถ้วน');
        console.log('ตารางความพร้อมด้านเทคโนโลยีสารสนเทศ');
        this.arrayStatus.push('ตารางความพร้อมด้านเทคโนโลยีสารสนเทศ')
      }
      if (this.statusextraComment == false) {
        // alert('กรุณากรอกข้อมูลตารางความเห็นเพิ่มเติม ให้ครบถ้วน');
        console.log('ตารางความเห็นเพิ่มเติม');
        this.arrayStatus.push('ตารางความเห็นเพิ่มเติม')
      }
      else if (this.validateBu >= 0 && this.BU_statusUpdate === true && this.statusprocessConsiders == true
        && this.statusReadiness == true
        && this.statusextraComment == true) {
        this.updateRisk.requestId = Number(this.requestId);
        this.status_loading = true;

        this.riskService.UpdateRisk(this.updateRisk).subscribe((resSave) => {

          console.log('saved');
          this.status_loading = false;
          this.updateRisk.processConsiders = [];
          this.updateRisk.processConsidersDelete = [];
          this.updateRisk.processConsidersSubDelete = [];
          this.updateRisk.processConsidersSubRiskDelete = [];
          this.updateRisk.readiness = [];
          this.updateRisk.readinessDelete = [];
          this.updateRisk.readinessDetailDelete = [];
          this.updateRisk.extraComment = [];
          this.updateRisk.extraCommentDelete = [];
          // this.FormData = [];
          // this.GetRest();
          // if(resSave['status'] === 'success') {
          this.Submit.SubmitWork(this.requestId).subscribe(
            (res) => {
              console.log(res);
              if (res['status'] === 'success') {
                // alert('ส่งงานเรียบร้อย');
                Swal.fire({
                  title: 'success',
                  text: 'ส่งงานเรียบร้อย',
                  icon: 'success',
                  showConfirmButton: false,
                  timer: 3000,
                }).then(() => {
                  localStorage.setItem('requestId', '');
                  this.router.navigate(['/dashboard1']);
                })
                // this.router.navigate(['/dashboard1']);
              } else {
                // alert(res['message']);
                if (res['message'] === 'other risk not approve') {
                  document.getElementById('opendModalSendwork').click();
                  this.messageData = res['data'];
                } else {
                  if (res['message'] === 'กรุณาไปให้ความเห็นหน้า ความเห็น / ส่งงาน') {
                    this.router.navigate(['/comment']);
                  } else {
                    Swal.fire({
                      title: 'error',
                      text: res['message'],
                      icon: 'error',
                      // showConfirmButton: false,
                      // timer: 1500,
                    }).then(() => {
                      this.POcommentConsider(); // this should execute now
                    });
                  }
                }
              }
            },
            (err) => {
              console.log(err);
              Swal.fire({
                title: 'error',
                text: err,
                icon: 'error',
                showConfirmButton: false,
                timer: 3000,
              });
              this.status_loading = false;
            }
          );

          // }else if(resSave['message'] === 'งานนี้ถูกส่งไปแล้ว') {
          //   Swal.fire({
          //     title: 'error',
          //     text: resSave['message'],
          //     icon: 'error',
          //     showConfirmButton: false,
          //     timer: 3000,
          //   });
          // }


        }, err => {
          this.status_loading = false;
          console.log(err);
        })




      }

      if (this.statusextraComment == false || this.statusReadiness == false ||
        this.statusprocessConsiders == false
      ) {
        console.log('arr:', this.arrayStatus);
        this.validateBu = 0;
        alert('กรุณากรอกข้อมูล ' + this.arrayStatus + ' ให้ครบถ้วน');

      }
      // this.validateBu = 0;
    }
    else if (this.GetRole === 'PO') {

      for (let ii = 0; ii < this.FormData.length; ii++) {
        const PO_risk = this.FormData[ii];

        for (let index = 0; index < PO_risk.processConsiders.length; index++) {
          const Main = PO_risk.processConsiders[index].subProcess;
          // tslint:disable-next-line: no-shadowed-variable
          for (let i = 0; i < Main.length; i++) {
            const SUB = Main[i].subProcessRisk;
            if (SUB.length !== 0) {
              for (let k = 0; k < SUB.length; k++) {
                if (SUB[k].buCommentId !== null) {
                  this.CommentPO.processConsiders.push({
                    buCommentId: SUB[k].buCommentId,
                    poCommentRemark: SUB[k].poCommentRemark,
                    poComment: SUB[k].poComment,
                  });
                }
              }
            }
          }
        }

        if (PO_risk.readiness !== null) {
          for (let index = 0; index < PO_risk.readiness.length; index++) {
            const readiness = PO_risk.readiness[index].detail;
            // console.log('Main:', commentOperation);
            // tslint:disable-next-line: no-shadowed-variable
            for (let i = 0; i < readiness.length; i++) {
              if (readiness[i].buCommentId !== null) {
                this.CommentPO.readiness.push({
                  buCommentId: readiness[i].buCommentId,
                  poCommentRemark: readiness[i].poCommentRemark,
                  poComment: readiness[i].poComment,
                });
              } else {
                this.CommentPO.readiness.push({
                  buCommentId: readiness[i].id,
                  poCommentRemark: readiness[i].poCommentRemark,
                  poComment: readiness[i].poComment,
                });
              }
            }
          }
        }

        if (PO_risk.extraComment.length !== 0) {
          for (let index = 0; index < PO_risk.extraComment.length; index++) {
            const extraComment = PO_risk.extraComment;
            // console.log('Main:', commentOperation1);
            // tslint:disable-next-line: no-shadowed-variable

            this.CommentPO.extraComment.push({
              buCommentId: extraComment[index].id,
              poCommentRemark: extraComment[index].poCommentRemark,
              poComment: extraComment[index].poComment,
            });
          }
        }
      }
      this.CommentPO.requestId = localStorage.getItem('requestId')
      const arr = this.CheckValueSandWorkPO(this.CommentPO);

      if (arr.length > 0) {
        alert('กรุณาให้ความเห็นตาราง ' + arr + ' ให้ครบ')
        this.CommentPO = {
          status: false,
          requestId: localStorage.getItem('requestId'),
          processConsiders: [],
          readiness: [],
          extraComment: [],
        }
      }
      else {
        console.log(this.CommentPO)
        this.riskService.UpdataCommentRisk_PO(this.CommentPO).subscribe(
          (res) => {
            // if (res['status'] === 'success') {
            this.Submit.sendDocument_PO(this.requestId, 'Risk').subscribe(
              (res) => {
                console.log(res);
                if (res['status'] === 'success') {
                  // alert('ส่งงานเรียบร้อย');
                  Swal.fire({
                    title: 'success',
                    text: 'ส่งงานเรียบร้อย',
                    icon: 'success',
                    showConfirmButton: false,
                    timer: 3000,
                  }).then(() => {
                    localStorage.setItem('requestId', '');
                    this.router.navigate(['/dashboard1']);
                  })
                } else {
                  // alert(res['message']);
                  if (res['message'] === 'กรุณาไปให้ความเห็นหน้า ความเห็น / ส่งงาน') {
                    this.router.navigate(['/comment']);
                  } else {
                    Swal.fire({
                      title: 'error',
                      text: res['message'],
                      icon: 'error',
                      showConfirmButton: false,
                      timer: 3000,
                    });
                  }
                }
              },
              (err) => {
                console.log(err);
                alert('เกิดข้อผิดพลาดไม่สามารถส่งงานได้');
              }
            );
            // }
          })
      }

    } else if (this.GetRole === 'PC') {
      this.CheckPC()
      if (this.arryPC.length > 0) {
        this.saveDraftstatus = null;
        alert('กรุณาให้ความเห็นตาราง ' + this.arryPC + ' ให้ครบ')
        this.Comment_PC = {
          status: false,
          requestId: this.requestId,
          processConsiders: [],
          readiness: [],
          extraComment: [],
        };
      }
      else {
        this.riskService.UpdataCommentRisk_PC(this.Comment_PC).subscribe(
          (res) => {
            console.log('res PO:', res);
            if (res['status'] === 'success') {
              this.Submit.sendComment_PC(this.requestId).subscribe(
                (res) => {
                  console.log(res);
                  if (res['status'] === 'success') {
                    // alert('ส่งงานเรียบร้อย');
                    Swal.fire({
                      title: 'success',
                      text: 'ส่งงานเรียบร้อย',
                      icon: 'success',
                      showConfirmButton: false,
                      timer: 3000,
                    }).then(() => {
                      localStorage.setItem('requestId', '');
                      this.router.navigate(['/dashboard1']);
                    })
                  } else {
                    // alert(res['message']);
                    Swal.fire({
                      title: 'error',
                      text: res['message'],
                      icon: 'error',
                      showConfirmButton: false,
                      timer: 3000,
                    });
                  }
                },
                (err) => {
                  console.log(err);
                  alert('เกิดข้อผิดพลาดไม่สามารถส่งงานได้');
                }
              );
            } else {
              // alert(res['message']);
              Swal.fire({
                title: 'error',
                text: res['message'],
                icon: 'error',
                showConfirmButton: false,
                timer: 3000,
              });
            }
          }
        );

      }

    } else {
      Swal.fire({
        title: 'error',
        text: 'ไม่สารมรถส่งงานด้',
        icon: 'error',
        showConfirmButton: false,
        timer: 3000,
      });
    }
  }
  CheckPC() {

    for (let ii = 0; ii < this.FormData.length; ii++) {
      const PO_risk = this.FormData[ii];

      for (let index = 0; index < PO_risk.processConsiders.length; index++) {
        const Main = PO_risk.processConsiders[index].subProcess;
        // tslint:disable-next-line: no-shadowed-variable
        for (let i = 0; i < Main.length; i++) {
          const SUB = Main[i].subProcessRisk;
          if (SUB.length !== 0) {
            for (let k = 0; k < SUB.length; k++) {
              if (SUB[k].buCommentId !== null) {
                this.Comment_PC.processConsiders.push({
                  buCommentId: SUB[k].buCommentId,
                  pcComment: SUB[k].pcComment,
                  pcExtraComment: SUB[k].pcExtraComment,
                  pcIssue: SUB[k].pcIssue,
                });
              }
            }
          }
        }
      }

      if (PO_risk.readiness !== null) {
        for (let index = 0; index < PO_risk.readiness.length; index++) {
          const readiness = PO_risk.readiness[index].detail;
          // console.log('Main:', commentOperation);
          // tslint:disable-next-line: no-shadowed-variable
          for (let i = 0; i < readiness.length; i++) {
            if (readiness[i].buCommentId !== null) {
              this.Comment_PC.readiness.push({
                buCommentId: readiness[i].buCommentId,
                pcComment: readiness[i].pcComment,
                pcExtraComment: readiness[i].pcExtraComment,
                pcIssue: readiness[i].pcIssue,
              });
            } else {
              this.Comment_PC.readiness.push({
                buCommentId: readiness[i].id,
                pcComment: readiness[i].pcComment,
                pcExtraComment: readiness[i].pcExtraComment,
                pcIssue: readiness[i].pcIssue,
              });
            }
          }
        }
      }

      if (PO_risk.extraComment.length !== 0) {
        for (let index = 0; index < PO_risk.extraComment.length; index++) {
          const extraComment = PO_risk.extraComment;
          // console.log('Main:', commentOperation1);
          // tslint:disable-next-line: no-shadowed-variable

          this.Comment_PC.extraComment.push({
            buCommentId: extraComment[index].id,
            pcComment: extraComment[index].pcComment,
            pcExtraComment: extraComment[index].pcExtraComment,
            pcIssue: extraComment[index].pcIssue,
          });
        }
      }
    }
    console.log('Comment PC:', this.Comment_PC);

    const check_extra_pcIssue = Object.values(
      this.Comment_PC.extraComment
    ).every((value) => value['pcIssue'] !== null && value['pcIssue'] !== '');
    const check_extra_pcExtraComment = Object.values(
      this.Comment_PC.extraComment
    ).every(
      (value) =>
        value['pcExtraComment'] !== null && value['pcExtraComment'] !== ''
    );
    const check_extra_pcComment = Object.values(
      this.Comment_PC.extraComment
    ).every(
      (value) => value['pcComment'] !== null && value['pcComment'] !== ''
    );

    const check_process_pcIssue = Object.values(
      this.Comment_PC.processConsiders
    ).every((value) => value['pcIssue'] !== null && value['pcIssue'] !== '');
    const check_process_pcExtraComment = Object.values(
      this.Comment_PC.processConsiders
    ).every(
      (value) =>
        value['pcExtraComment'] !== null && value['pcExtraComment'] !== ''
    );
    const check_process_pcComment = Object.values(
      this.Comment_PC.processConsiders
    ).every(
      (value) => value['pcComment'] !== null && value['pcComment'] !== ''
    );

    const check_readiness_pcIssue = Object.values(
      this.Comment_PC.readiness
    ).every((value) => value['pcIssue'] !== null && value['pcIssue'] !== '');
    const check_readiness_pcExtraComment = Object.values(
      this.Comment_PC.readiness
    ).every(
      (value) =>
        value['pcExtraComment'] !== null && value['pcExtraComment'] !== ''
    );
    const check_readiness_pcComment = Object.values(
      this.Comment_PC.readiness
    ).every(
      (value) => value['pcComment'] !== null && value['pcComment'] !== ''
    );

    console.log('Comment PC:', this.Comment_PC.extraComment, check_extra_pcIssue, check_extra_pcExtraComment, check_extra_pcComment);
    console.log('Comment PC:', this.Comment_PC.processConsiders, check_process_pcIssue, check_process_pcExtraComment, check_process_pcComment);
    console.log('Comment PC:', this.Comment_PC.readiness, check_readiness_pcIssue, check_readiness_pcExtraComment, check_readiness_pcComment);

    this.arryPC = [];
    if (
      check_extra_pcIssue === true &&
      check_extra_pcExtraComment === true &&
      check_extra_pcComment === true &&
      check_process_pcIssue === true &&
      check_process_pcExtraComment === true &&
      check_process_pcComment === true &&
      check_readiness_pcIssue === true &&
      check_readiness_pcExtraComment === true &&
      check_readiness_pcComment === true
    ) {
      this.Comment_PC.status = true;
    }

    if (
      check_process_pcIssue === false ||
      check_process_pcExtraComment === false ||
      check_process_pcComment === false
    ) {
      this.arryPC.push('รายละเอียดกระบวนการทำงาน')
    }

    if (
      check_readiness_pcIssue === false ||
      check_readiness_pcExtraComment === false ||
      check_readiness_pcComment === false
    ) {
      this.arryPC.push('ความพร้อมด้านเทคโนโลยีสารสนเทศ')
    }

    if (
      check_extra_pcIssue === false ||
      check_extra_pcExtraComment === false ||
      check_extra_pcComment === false
    ) {
      this.arryPC.push('ความเห็นเพิ่มเติม')
    }
    console.log('this.Comment_PC.status', this.Comment_PC.status);
    // const check_extra_pcExtraComment = Object.values(this.CommentPO.extraComment).every(value => value['poCommentRemark'] !== null && value['poCommentRemark'] !== '');
    this.Comment_PC.requestId = Number(this.requestId);

  }
  CheckValueSandWorkPO(comment) {
    console.log(comment)
    let processConsiders = true;
    let readiness = true;
    let extraComment = true;
    const arry = []
    for (let i = 0; i < comment.processConsiders.length; i++) {
      if (comment.processConsiders[i].poComment == '' ||
        comment.processConsiders[i].poComment == null ||
        comment.processConsiders[i].poCommentRemark == '' ||
        comment.processConsiders[i].poCommentRemark == null) {
        processConsiders = false
        arry.push('รายละเอียดกระบวนการทำงาน')
        break;
      }
    }
    for (let i = 0; i < comment.readiness.length; i++) {
      if (comment.readiness[i].poComment == '' ||
        comment.readiness[i].poComment == null ||
        comment.readiness[i].poCommentRemark == '' ||
        comment.readiness[i].poCommentRemark == null) {
        readiness = false
        arry.push('ความพร้อมด้านเทคโนโลยีสารสนเทศ')
        break;
      }
    }

    for (let i = 0; i < comment.extraComment.length; i++) {
      if (comment.extraComment[i].poComment == '' ||
        comment.extraComment[i].poComment == null ||
        comment.extraComment[i].poCommentRemark == '' ||
        comment.extraComment[i].poCommentRemark == null) {
        extraComment = false
        arry.push('ความเห็นเพิ่มเติม')
        break;
      }
    }

    if (processConsiders == true && readiness == true && extraComment == true) {
      comment.status = true;
    }
    return arry;
  }
  sendBackWork() {
    this.Submit.SendBackWork(this.requestId).subscribe(res => {
      console.log(res);
      if (res['status'] === 'success') {
        Swal.fire({
          title: 'success',
          text: 'ส่งงานกลับไปแก้ไขเรียบร้อย',
          icon: 'success',
          showConfirmButton: false,
          timer: 3000
        });
        this.router.navigate(['/dashboard1']);
      } else {
        Swal.fire({
          title: 'error',
          text: res['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 3000
        });
      }
    });
  }
  POcommentConsider() {
    this.Submit.linkToAny();
  }
  Checklevel() {
    var status = false;
    const RoleArry = [
      'ฝ่าย',
      'กลุ่ม',
      'สาย']
    for (let i = 0; i < RoleArry.length; i++) {
      if (this.Getlevel == RoleArry[i]) {
        status = true
      }
    }
    return status
  }
  backToDashboardPage() {
    // localStorage.setItem('requestId', '');
    this.router.navigate(['dashboard1']);
  }

  autoGrowTextZone(e) {
    e.target.style.height = '0px';
    e.target.style.overflow = 'hidden';
    e.target.style.height = (e.target.scrollHeight + 0) + 'px';
  }
  covertDateToCorrect(dueDate: any) {
    if (dueDate !== null) {
      const type = String(dueDate);
      const year = Number(type.substr(0, 4)) + 543;
      const month = type.substr(5, 2);
      const day = type.substr(8, 2);
      const date = day + '/' + month + '/' + year;
      return date;
    } else {
      return '';
    }
  }
}
