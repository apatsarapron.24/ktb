import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommentPointRoutingModule } from './comment-point-routing.module';
import { CommentPointComponent } from './comment-point.component';
import { FinanceComponent } from './finance/finance.component';
import { MatButtonModule, MatTableModule, MatDialogModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogFinanceComponent } from './finance/dialog-finance/dialog-finance.component';
import { OperationComponent } from './operation/operation.component';

import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { CommentManagementComponent } from './comment-management/comment-management.component';
import { DialogCommentsComponent } from './comment-management/dialog-comments/dialog-comments.component';
import { ComplianceComponent } from './compliance/compliance.component';

import { AngularMyDatePickerModule } from 'angular-mydatepicker';
import { BrowserModule } from '@angular/platform-browser';
import { MatInputModule } from '@angular/material/input';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { RetailComponent } from './retail/retail.component';
import { RiskComponent } from './risk/risk.component';
import { TextareaAutoresizeDirectiveModule} from '../../textarea-autoresize.directive/textarea-autoresize.directive.module'
import { DialogSaveStatusComponent } from './dialog-save-status/dialog-save-status.component';

import { DialogStatusService } from '../../services/comment-point/dialog-status/dialog-status.service';

@NgModule({
  declarations: [
    OperationComponent,
    CommentPointComponent,
    FinanceComponent,
    DialogFinanceComponent,
    RetailComponent,
    CommentManagementComponent,
    DialogCommentsComponent,
    ComplianceComponent,
    RiskComponent,
    DialogSaveStatusComponent,
  ],
  imports: [
    // BrowserModule,
    // AppModule,
    CommonModule,
    CommentPointRoutingModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatDialogModule,
    MatAutocompleteModule,
    AngularMyDatePickerModule,
    MatInputModule,
    DragDropModule,
    TextareaAutoresizeDirectiveModule
  ],
  exports:[
    OperationComponent,
    FinanceComponent,
    RetailComponent,
    ComplianceComponent,
    RiskComponent,
  ],
  providers: [DialogStatusService],
  entryComponents: [DialogFinanceComponent, DialogCommentsComponent,
    DialogSaveStatusComponent]
})
export class CommentPointModule { }
