import { element } from 'protractor';
import { Data } from '../../dashboard-cus/dashboard-cus.component';
import { Component, OnInit } from '@angular/core';
import {
  AngularMyDatePickerDirective,
  IAngularMyDpOptions,
  IMyDateModel,
} from 'angular-mydatepicker';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
  CdkDragExit,
  CdkDragEnter,
  CdkDragStart,
  CdkDragEnd,
} from '@angular/cdk/drag-drop';
import { StepFiveService } from '../../../services/request/product-detail/step-five/step-five.service';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { RequestService } from '../../../services/request/request.service';
import { OperationService } from '../../../services/comment-point/operation/operation.service';
import { SubmitService } from '../../../services/comment-point/submit/submit.service';
import { numberFormat } from 'highcharts';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import Swal from 'sweetalert2';
import { DialogStatusService } from '../../../services/comment-point/dialog-status/dialog-status.service';
import { DialogSaveStatusComponent } from '../../comment-point/dialog-save-status/dialog-save-status.component';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { promise } from 'protractor';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


@Component({
  selector: 'app-operation',
  templateUrl: './operation.component.html',
  styleUrls: ['./operation.component.scss'],
})
export class OperationComponent implements OnInit {

  constructor(
    private router: Router,
    private stepFiveService: StepFiveService,
    private sidebarService: RequestService,
    private Operation_Service: OperationService,
    private Submit: SubmitService,
    public dialog: MatDialog,
  ) { }

  header_table = ['กระบวนการย่อย', 'In/Out', 'IT Related'];
  FormData = {
    requestId: null,
    processConsiders: [],
    deleteCommentOperation1: [],
    deleteCommentOperation2: [],
    commentOperationProcess: [],
    commentOperation1: [],
    commentOperation2: [],
    requestProcessConsidersDelete: [],
    requestProcessConsidersSubDelete: [],
    requestProcessConsidersSubRiskDelete: [],
  };
  arryPC: any = [];
  // covert date not important...
  convertDate: any = [];
  convertDatePO: any = [];
  // dropIndex: number;
  saveDraftstatus = null;

  // new table
  dropIndex: number;
  add_mainProcess: null;
  staticTitleIndex = []; // !important ex [0,1,2] mean set fix row index 0 1 2

  // Risk sub_table
  Risk_dropIndex = 0;
  staticTitleIndexRisk = [];

  // status page
  FormDataStatus = true;
  IN_OUT: any = ['No', 'Yes', ''];
  requestId: any;
  ResData: any;

  GetRole: any;
  GetLevel: any;
  validate = null;
  Res_PO: any;
  GetData_PO: any = {
    commentOperationProcess: [],
    processConsiders: [],
    commentOperation1: [],
    commentOperation2: [],
    question: [],
  };
  CommentPO: any = {
    requestId: null,
    status: false,
    commentOperationProcess: [],
    processConsiders: [],
    commentOperation1: [],
    commentOperation2: [],
  };
  Comment_PC: any = {
    requestId: null,
    status: false,
    commentOperationProcess: [],
    processConsiders: [],
    commentOperation1: [],
    commentOperation2: [],
  };
  BU_statusUpdate = true;

  lengthOperationPC: any;
  lengthOperation: any;
  validateBu = 0;

  status_loading = false;
  Getname: any;
  alertMessage: any = '';

  statusprocessConsiders: boolean;
  statuscommentOperationProcess: boolean;
  statuscommentOperation1: boolean;
  statuscommentOperation2: boolean;
  arrayStatus: any = [];
  saveDraftstatus_State = false;

  note = [
    'No คือ ไม่ใช่ประเด็นคงค้าง',
    'Pending 1 คือ ต้องแก้ไขก่อนประกาศใช้ผลิตภัณฑ์',
    'Pending 2 คือ สามารถใช้ผลิตภัณฑ์ได้ก่อน แล้วค่อยแก้ไขตามหลังได้',
    'Pending 3 คือ ไม่เกี่ยวข้องกับการประกาศใช้ผลิตภัณฑ์',
    'Pending 4 คือ ต้องแก้ไขให้แล้วเสร็จก่อนนำเสนอเรื่องต่อบอร์ด',
  ];

  ngOnInit() {
    this.GetRest();

    // this.savedData();
    // this.saveDraftstatus = null;
  }

  GetRest() {
    this.sidebarService.sendEvent();
    if (localStorage) {
      this.requestId = localStorage.getItem('requestId');
      this.GetRole = localStorage.getItem('role');
      this.GetLevel = localStorage.getItem('level');
      this.Getname = localStorage.getItem('ouName');
      if (this.GetRole === 'Operation') {
        this.status_loading = true;
        this.Operation_Service.getOperation(this.requestId).subscribe((res) => {
          this.status_loading = false;
          this.ResData = res;
          console.log('res:', res);
          this.validate = this.ResData.data.validate;
          this.FormData.processConsiders = this.ResData.data.processConsiders;
          this.FormData.commentOperationProcess = this.ResData.data.commentOperationProcess;
          this.FormData.commentOperation1 = this.ResData.data.commentOperation1;
          this.FormData.commentOperation2 = this.ResData.data.commentOperation2;

          for (
            let index = 0;
            index < this.FormData.commentOperationProcess.length;
            index++
          ) {
            // console.log('dueDate:',   this.FormData.commentOperationProcess[index].commentOperation);
            this.FormData.commentOperationProcess[
              index
            ].commentOperation.forEach((item, i) => {
              // console.log('dueDate:',   item.dueDate.substr(0, 10));
              if (item.dueDate !== null) {
                const type = String(item.dueDate);
                const year = Number(type.substr(0, 4)) + 543;
                const month = type.substr(5, 2);
                const day = type.substr(8, 2);
                const date = day + '/' + month + '/' + year;
                this.convertDate.push({ Convert: date });
              } else {
                this.convertDate.push({ Convert: '' });
              }

            });
          }

          for (
            let index = 0;
            index < this.FormData.processConsiders.length;
            index++
          ) {
            for (
              let p = 0;
              p < this.FormData.processConsiders[index].subProcess.length;
              p++
            ) {
              if (
                this.FormData.processConsiders[index].subProcess[p]
                  .subProcessRisk.length === 0
              ) {
                this.FormData.processConsiders[index].subProcess[
                  p
                ].subProcessRisk.push({
                  id: '',
                  risk: '',
                  riskControl: '',
                  riskControlOther: '',
                  remark: '',
                  subject: '',
                  management: '',
                });
              }

              if (
                this.FormData.processConsiders[index].subProcess[p].subTitle ===
                null
              ) {
                this.FormData.processConsiders[index].subProcess[p].subTitle =
                  '';
              }
            }
          }

          // this.FormData.processConsiders.push({
          //   id: '',
          //   mainProcess: '',
          //   subProcess: [
          //     {
          //       id: this.makeid(5),
          //       subTitle: '',
          //       inOut: '',
          //       itRelate: '',
          //       subProcessRisk: [
          //         {
          //           id: '',
          //           risk: '',
          //           riskControl: '',
          //           riskControlOther: '',
          //           remark: '',
          //           subject: '',
          //           management: '',
          //         },
          //       ],
          //     },
          //   ],
          // });
          
        }, err => {
          this.status_loading = false;
          console.log(err);
        })
      } else if (this.GetRole === 'PO') {
        console.log(this.requestId);
        this.status_loading = true;
        this.Operation_Service.getOperation_PO(this.requestId).subscribe(
          (res) => {
            this.Res_PO = res;
            // this.GetData_PO = this.Res_PO.data;
            console.log('res', res);
            this.validate = this.Res_PO.data.validate;
            this.GetData_PO.processConsiders = this.Res_PO.data.processConsiders;
            this.GetData_PO.commentOperationProcess = this.Res_PO.data.commentOperationProcess;
            this.GetData_PO.commentOperation1 = this.Res_PO.data.commentOperation1;
            this.GetData_PO.commentOperation2 = this.Res_PO.data.commentOperation2;

            for (
              let index = 0;
              index < this.GetData_PO.commentOperationProcess.length;
              index++
            ) {
              this.GetData_PO.commentOperationProcess[
                index
              ].commentOperation.forEach((item, i) => {
                // console.log('dueDate:',   item.dueDate.substr(0, 10));
                if (item.dueDate !== null) {
                  const typePO = String(item.dueDate);
                  const yearPO = Number(typePO.substr(0, 4)) + 543;
                  const monthPO = typePO.substr(5, 2);
                  const dayPO = typePO.substr(8, 2);

                  const datePO = dayPO + '/' + monthPO + '/' + yearPO;
                  // console.log('dueDate:',  date);
                  this.convertDatePO.push({ Convert: datePO });
                } else {
                  this.convertDatePO.push({ Convert: '' });
                }


              });
            }
            console.log('PO validate::', this.validate);
            console.log('res PO::', this.GetData_PO);
            this.status_loading = false;
          }
          , err => {
            this.status_loading = false;
            console.log(err);
          })
      } else if (this.GetRole === 'PC') {
        console.log(this.requestId);
        this.status_loading = true;
        this.Operation_Service.Get_PC_Operation(
          Number(this.requestId)
        ).subscribe((res) => {
          this.Res_PO = res;
          // this.GetData_PO = this.Res_PO.data;
          console.log('res PC', res);
          this.validate = this.Res_PO.data.validate;
          this.GetData_PO.processConsiders = this.Res_PO.data.processConsiders;
          this.GetData_PO.commentOperationProcess = this.Res_PO.data.commentOperationProcess;
          this.GetData_PO.commentOperation1 = this.Res_PO.data.commentOperation1;
          this.GetData_PO.commentOperation2 = this.Res_PO.data.commentOperation2;

          for (
            let index = 0;
            index < this.GetData_PO.commentOperationProcess.length;
            index++
          ) {
            this.GetData_PO.commentOperationProcess[
              index
            ].commentOperation.forEach((item, i) => {
              // console.log('dueDate:',   item.dueDate.substr(0, 10));
              if (item.dueDate !== null) {
                const typePO = String(item.dueDate);
                const yearPO = Number(typePO.substr(0, 4)) + 543;
                const monthPO = typePO.substr(5, 2);
                const dayPO = typePO.substr(8, 2);

                const datePO = dayPO + '/' + monthPO + '/' + yearPO;
                // console.log('dueDate:',  date);
                this.convertDatePO.push({ Convert: datePO });
              } else {
                this.convertDatePO.push({ Convert: '' });
              }
            });
          }
          this.status_loading = false;
          // console.log('PO validate::', this.validate);
          // console.log('res PO::', this.GetData_PO);
        }, err => {
          this.status_loading = false;
          console.log(err);
        })
      } else {
        this.status_loading = true;
        this.Operation_Service.getOperation_PO(this.requestId).subscribe(
          (res) => {
            this.Res_PO = res;
            // this.GetData_PO = this.Res_PO.data;
            this.validate = this.Res_PO.data.validate;
            this.GetData_PO.processConsiders = this.Res_PO.data.processConsiders;
            this.GetData_PO.commentOperationProcess = this.Res_PO.data.commentOperationProcess;
            this.GetData_PO.commentOperation1 = this.Res_PO.data.commentOperation1;
            this.GetData_PO.commentOperation2 = this.Res_PO.data.commentOperation2;

            for (
              let index = 0;
              index < this.GetData_PO.commentOperationProcess.length;
              index++
            ) {
              this.GetData_PO.commentOperationProcess[
                index
              ].commentOperation.forEach((item, i) => {
                // console.log('dueDate:',   item.dueDate.substr(0, 10));
                if (item.dueDate !== null) {
                  const typePO = String(item.dueDate);
                  const yearPO = Number(typePO.substr(0, 4)) + 543;
                  const monthPO = typePO.substr(5, 2);
                  const dayPO = typePO.substr(8, 2);

                  const datePO = dayPO + '/' + monthPO + '/' + yearPO;
                  // console.log('dueDate:',  date);
                  this.convertDatePO.push({ Convert: datePO });
                } else {
                  this.convertDatePO.push({ Convert: '' });
                }

              });
            }
            console.log('PO validate::', this.validate);
            console.log('res PO::', this.GetData_PO);
            this.status_loading = false;
          }
          , err => {
            this.status_loading = false;
            console.log(err);
          })
      }
    }
  }
  changeSaveDraft() {
    this.saveDraftstatus_State = true;
    this.Operation_Service.status_state = true;
    console.log('saveDraftstatus:', this.saveDraftstatus_State)

  }
  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate');
    // return confirm();
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    console.log('000 sidebarService status:', status);
    if (this.saveDraftstatus_State === true) {
      // console.log('000 sidebarService status:', status);
      const dialogRef = this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        console.log('data::', data);
        if (data.returnStatus === true) {
          subject.next(true);
          this.savedData();
          this.saveDraftstatus_State = false;
        } else if (data.returnStatus === false) {
          console.log('000000 data::', data.returnStatus);
          this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
        }
      });
      console.log('8889999', subject);
      return subject;
    } else {
      return true;
    }
  }
  savedData() {
    this.saveDraftstatus_State = false;
    this.Operation_Service.status_state = false;
    if (this.GetRole === 'PO') {
      this.CommentPO.requestId = Number(this.requestId);
      console.log(this.GetData_PO);
      for (
        let index = 0;
        index < this.GetData_PO.processConsiders.length;
        index++
      ) {
        const Main = this.GetData_PO.processConsiders[index].subProcess;

        // tslint:disable-next-line: no-shadowed-variable
        for (let i = 0; i < Main.length; i++) {
          const SUB = Main[i].subProcessRisk;
          // console.log(SUB)
          if (SUB.length !== 0) {
            for (let k = 0; k < SUB.length; k++) {
              if (SUB[k].buCommentId !== null) {
                this.CommentPO.processConsiders.push({
                  buCommentId: SUB[k].buCommentId,
                  poComment: SUB[k].poComment,
                });
              }
            }
          }
        }
      }

      for (
        let index = 0;
        index < this.GetData_PO.commentOperationProcess.length;
        index++
      ) {
        const commentOperation = this.GetData_PO.commentOperationProcess[index]
          .commentOperation;
        // console.log('Main:', commentOperation);
        // tslint:disable-next-line: no-shadowed-variable
        for (let i = 0; i < commentOperation.length; i++) {
          if (commentOperation[i].buCommentId !== null) {
            this.CommentPO.commentOperationProcess.push({
              buCommentId: commentOperation[i].buCommentId,
              poComment: commentOperation[i].poComment,
            });
          } else {
            this.CommentPO.commentOperationProcess.push({
              buCommentId: commentOperation[i].id,
              poComment: commentOperation[i].poComment,
            });
          }
        }
      }

      if (this.GetData_PO.commentOperation1.length !== 0) {
        for (
          let index = 0;
          index < this.GetData_PO.commentOperation1.length;
          index++
        ) {
          const commentOperation1 = this.GetData_PO.commentOperation1;
          // console.log('Main:', commentOperation1);
          // tslint:disable-next-line: no-shadowed-variable
          if (commentOperation1[index].id !== null) {
            this.CommentPO.commentOperation1.push({
              buCommentId: commentOperation1[index].id,
              poComment: commentOperation1[index].poComment,
            });
          }
        }
      }

      if (this.GetData_PO.commentOperation2.length !== 0) {
        for (
          let index = 0;
          index < this.GetData_PO.commentOperation2.length;
          index++
        ) {
          const commentOperation2 = this.GetData_PO.commentOperation2;
          // console.log('Main:', commentOperation1);
          // tslint:disable-next-line: no-shadowed-variable
          if (commentOperation2[index].id !== null) {
            this.CommentPO.commentOperation2.push({
              buCommentId: commentOperation2[index].id,
              poComment: commentOperation2[index].poComment,
            });
          }
        }
      }


      const check_commentOperation1 = Object.values(this.CommentPO.commentOperation1).every(value => value['poComment'] !== '' && value['poComment'] !== null);
      const check_commentOperation2 = Object.values(this.CommentPO.commentOperation2).every(value => value['poComment'] !== '' && value['poComment'] !== null);
      const check_commentOperationProcess = Object.values(this.CommentPO.commentOperationProcess).every(value => value['poComment'] !== '' && value['poComment'] !== null);
      const check_processConsiders = Object.values(this.CommentPO.processConsiders).every(value => value['poComment'] !== '' && value['poComment'] !== null);


      if (check_commentOperation1 === true &&
        check_commentOperation2 === true &&
        check_commentOperationProcess === true &&
        check_processConsiders === true
      ) {
        this.CommentPO.status = true;
      }

      console.log(check_commentOperation1, check_commentOperation2, check_commentOperationProcess, check_processConsiders);
      console.log('CommentPO:', this.CommentPO.status);
      this.status_loading = true;
      this.Operation_Service.UpdataCommentOperation_PO(
        this.CommentPO
      ).subscribe((res) => {
        console.log('update PO:', res);
        console.log('CommentPO:', this.CommentPO);


        if (res['status'] === 'success') {
          this.alertMessage = 'บันทึกความเห็นของ' + this.Getname + 'เรียบร้อย';
          this.CommentPO.commentOperationProcess = [];
          this.CommentPO.processConsiders = [];
          this.CommentPO.commentOperation1 = [];
          this.CommentPO.commentOperation2 = [];
          this.CommentPO.status = false;

          this.saveDraftstatus = 'success';
          setTimeout(() => {
            this.saveDraftstatus = 'fail';
          }, 3000);
          this.GetRest();
          this.status_loading = false;
        } else {
          Swal.fire({
            title: 'error',
            text: res['message'],
            icon: 'error',
          });
          this.status_loading = false;
        }
      }, err => {
        this.status_loading = false;
        console.log(err);
      })
    } else if (this.GetRole !== 'PO' && this.GetRole !== 'PC') {
      this.FormData.requestId = Number(this.requestId);
      for (
        let index = 0;
        index < this.FormData.processConsiders.length;
        index++
      ) {
        const Group = this.FormData.processConsiders[index].subProcess;

        for (let n = 0; n < Group.length; n++) {
          const subProcess = Group[n].id;
          if (typeof subProcess === 'string') {
            console.log('subProcess', subProcess);
            this.FormData.processConsiders[index].subProcess[n].id = '';
          }
        }
      }

      for (
        let i = 0;
        i < this.FormData.requestProcessConsidersSubDelete.length;
        i++
      ) {
        if (
          typeof this.FormData.requestProcessConsidersSubDelete[i] === 'string'
        ) {
          this.FormData.requestProcessConsidersSubDelete.splice(i, 1);
        }
      }
      for (
        let i = 0;
        i < this.FormData.requestProcessConsidersSubDelete.length;
        i++
      ) {
        if (
          typeof this.FormData.requestProcessConsidersSubDelete[i] === 'string'
        ) {
          this.FormData.requestProcessConsidersSubDelete.splice(i, 1);
        }
      }
      // console.log('processConsiders', this.FormData.processConsiders);
      // console.log('Delete', this.FormData.requestProcessConsidersDelete);
      // console.log('Delete Sub', this.FormData.requestProcessConsidersSubDelete);
      // console.log(
      //   'Delete SubRisk',
      //   this.FormData.requestProcessConsidersSubRiskDelete
      // );
      for (let w = 0; w < this.FormData.commentOperation1.length; w++) {
        if (
          this.FormData.commentOperation1[w].id === '' &&
          this.FormData.commentOperation1[w].subject === '' &&
          this.FormData.commentOperation1[w].manage === ''
        ) {
          this.FormData.commentOperation1.splice(w, 1);
        }
      }
      for (let w = 0; w < this.FormData.commentOperation1.length; w++) {
        if (
          this.FormData.commentOperation1[w].id === '' &&
          this.FormData.commentOperation1[w].subject === '' &&
          this.FormData.commentOperation1[w].manage === ''
        ) {
          // console.log('data', w, this.FormData.commentOperation1[w]);
          this.FormData.commentOperation1.splice(w, 1);
        }
      }

      for (let w = 0; w < this.FormData.commentOperation2.length; w++) {
        if (
          this.FormData.commentOperation2[w].id === '' &&
          this.FormData.commentOperation2[w].subject === '' &&
          this.FormData.commentOperation2[w].manage === ''
        ) {
          this.FormData.commentOperation2.splice(w, 1);
        }
      }
      for (let w = 0; w < this.FormData.commentOperation2.length; w++) {
        if (
          this.FormData.commentOperation2[w].id === '' &&
          this.FormData.commentOperation2[w].subject === '' &&
          this.FormData.commentOperation2[w].manage === ''
        ) {
          this.FormData.commentOperation2.splice(w, 1);
        }
      }

      for (
        let index = 0;
        index < this.FormData.processConsiders.length;
        index++
      ) {
        if (
          this.FormData.processConsiders[index].id === '' &&
          this.FormData.processConsiders[index].mainProcess === ''
        ) {
          this.FormData.processConsiders.splice(index, 1);
        }
      }
      for (
        let index = 0;
        index < this.FormData.processConsiders.length;
        index++
      ) {
        if (
          this.FormData.processConsiders[index].id === '' &&
          this.FormData.processConsiders[index].mainProcess === ''
        ) {
          this.FormData.processConsiders.splice(index, 1);
          this.BU_statusUpdate = true;
        } else if (
          this.FormData.processConsiders[index].id === '' &&
          this.FormData.processConsiders[index].mainProcess !== ''
        ) {
          const subProcess = this.FormData.processConsiders[index].subProcess;
          for (let r = 0; r < subProcess.length; r++) {
            if (subProcess[r].inOut === null || subProcess[r].itRelate === null) {
              alert('กรุณาเลือกข้อมูล IN/OUT หรือ IT Related ให้ถูกต้อง');
              this.BU_statusUpdate = false;
              break;
            }
          }

          break;
        } else {
          this.BU_statusUpdate = true;
        }
        // console.log('FormData:', this.FormData);
      }
      console.log('BU_statusUpdate:', this.BU_statusUpdate);
      // if (this.BU_statusUpdate === true) {
      this.update();
      // }
      // this.update();
    } else if (this.GetRole === 'PC') {
      this.Comment_PC.requestId = Number(this.requestId);
      for (
        let index = 0;
        index < this.GetData_PO.processConsiders.length;
        index++
      ) {
        const Main = this.GetData_PO.processConsiders[index].subProcess;

        // tslint:disable-next-line: no-shadowed-variable
        for (let i = 0; i < Main.length; i++) {
          const SUB = Main[i].subProcessRisk;
          if (SUB.length !== 0) {
            for (let k = 0; k < SUB.length; k++) {
              if (SUB[k].buCommentId !== null) {
                this.Comment_PC.processConsiders.push({
                  buCommentId: SUB[k].buCommentId,
                  pcComment: SUB[k].pcComment,
                  pcExtraComment: SUB[k].pcExtraComment,
                  pcIssue: SUB[k].pcIssue,
                });
              }
            }
          }
        }
      }

      for (
        let index = 0;
        index < this.GetData_PO.commentOperationProcess.length;
        index++
      ) {
        const commentOperation = this.GetData_PO.commentOperationProcess[index]
          .commentOperation;
        // console.log('Main:', commentOperation);
        // tslint:disable-next-line: no-shadowed-variable
        for (let i = 0; i < commentOperation.length; i++) {
          if (commentOperation[i].buCommentId !== null) {
            this.Comment_PC.commentOperationProcess.push({
              buCommentId: commentOperation[i].buCommentId,
              pcComment: commentOperation[i].pcComment,
              pcExtraComment: commentOperation[i].pcExtraComment,
              pcIssue: commentOperation[i].pcIssue,
            });
          } else {
            this.Comment_PC.commentOperationProcess.push({
              buCommentId: commentOperation[i].id,
              pcComment: commentOperation[i].pcComment,
              pcExtraComment: commentOperation[i].pcExtraComment,
              pcIssue: commentOperation[i].pcIssue,
            });
          }
        }
      }

      if (this.GetData_PO.commentOperation1.length !== 0) {
        for (
          let index = 0;
          index < this.GetData_PO.commentOperation1.length;
          index++
        ) {
          const commentOperation1 = this.GetData_PO.commentOperation1;
          // console.log('Main:', commentOperation1);
          // tslint:disable-next-line: no-shadowed-variable
          if (commentOperation1.id !== null) {
            this.Comment_PC.commentOperation1.push({
              buCommentId: commentOperation1[index].id,
              pcComment: commentOperation1[index].pcComment,
              pcExtraComment: commentOperation1[index].pcExtraComment,
              pcIssue: commentOperation1[index].pcIssue,
            });
          }
        }
      }

      if (this.GetData_PO.commentOperation2.length !== 0) {
        for (
          let index = 0;
          index < this.GetData_PO.commentOperation2.length;
          index++
        ) {
          const commentOperation2 = this.GetData_PO.commentOperation2;
          // console.log('Main:', commentOperation1);
          // tslint:disable-next-line: no-shadowed-variable
          if (commentOperation2.id !== null) {
            this.Comment_PC.commentOperation2.push({
              buCommentId: commentOperation2[index].id,
              pcComment: commentOperation2[index].pcComment,
              pcExtraComment: commentOperation2[index].pcExtraComment,
              pcIssue: commentOperation2[index].pcIssue,
            });
          }
        }
      }


      // console.log('processConsiders:', this.Comment_PC.processConsiders);
      const check_process_pcIssue = Object.values(this.Comment_PC.processConsiders).every(value => value['pcIssue'] !== null && value['pcIssue'] !== '');
      const check_process_pcExtraComment = Object.values(this.Comment_PC.processConsiders).every(value => value['pcExtraComment'] !== null && value['pcExtraComment'] !== '');
      const check_process_pcComment = Object.values(this.Comment_PC.processConsiders).every(value => value['pcComment'] !== null && value['pcComment'] !== '');
      console.log('processConsiders:', this.Comment_PC.processConsiders, check_process_pcIssue, check_process_pcExtraComment, check_process_pcComment);

      const check_commentOperation1_pcIssue = Object.values(this.Comment_PC.commentOperation1).every(value => value['pcIssue'] !== null && value['pcIssue'] !== '');
      const check_commentOperation1_pcExtraComment = Object.values(this.Comment_PC.commentOperation1).every(value => value['pcExtraComment'] !== null && value['pcExtraComment'] !== '');
      const check_commentOperation1_pcComment = Object.values(this.Comment_PC.commentOperation1).every(value => value['pcComment'] !== null && value['pcComment'] !== '');
      console.log('commentOperation1:', this.Comment_PC.commentOperation1, check_commentOperation1_pcIssue, check_commentOperation1_pcExtraComment, check_commentOperation1_pcComment);

      const check_commentOperation2_pcIssue = Object.values(this.Comment_PC.commentOperation2).every(value => value['pcIssue'] !== null && value['pcIssue'] !== '');
      const check_commentOperation2_pcExtraComment = Object.values(this.Comment_PC.commentOperation2).every(value => value['pcExtraComment'] !== null && value['pcExtraComment'] !== '');
      const check_commentOperation2_pcComment = Object.values(this.Comment_PC.commentOperation2).every(value => value['pcComment'] !== null && value['pcComment'] !== '');
      console.log('commentOperation2:', this.Comment_PC.commentOperation2, check_commentOperation2_pcIssue, check_commentOperation2_pcExtraComment, check_commentOperation2_pcComment);

      const check_OperationProcess_pcIssue = Object.values(this.Comment_PC.commentOperationProcess).every(value => value['pcIssue'] !== null && value['pcIssue'] !== '');
      const check_OperationProcess_pcExtraComment = Object.values(this.Comment_PC.commentOperationProcess).every(value => value['pcExtraComment'] !== null && value['pcExtraComment'] !== '');
      const check_OperationProcess_pcComment = Object.values(this.Comment_PC.commentOperationProcess).every(value => value['pcComment'] !== null && value['pcComment'] !== '');
      console.log('commentOperationProcess:', this.Comment_PC.commentOperationProcess, check_OperationProcess_pcIssue, check_OperationProcess_pcExtraComment, check_OperationProcess_pcComment);


      if (check_process_pcIssue === true && check_process_pcExtraComment === true && check_process_pcComment === true &&
        check_commentOperation1_pcIssue === true && check_commentOperation1_pcExtraComment === true && check_commentOperation1_pcComment === true &&
        check_commentOperation2_pcIssue === true && check_commentOperation2_pcExtraComment === true && check_commentOperation1_pcComment === true &&
        check_OperationProcess_pcIssue === true && check_OperationProcess_pcExtraComment === true && check_OperationProcess_pcComment === true) {
        this.Comment_PC.status = true;
      }
      console.log('Comment_PC:', this.Comment_PC);
      this.status_loading = true;
      this.Operation_Service.UpdataCommentOperation_PC(
        this.Comment_PC
      ).subscribe((res) => {
        console.log('update PC:', res);
        // console.log('CommentPO:', this.CommentPO);

        if (res['status'] === 'success') {
          this.Comment_PC.commentOperationProcess = [];
          this.Comment_PC.processConsiders = [];
          this.Comment_PC.commentOperation1 = [];
          this.Comment_PC.commentOperation2 = [];
          this.Comment_PC.status = false;
          this.alertMessage = 'บันทึกความเห็นของคณะกรรมการผลิตภัณฑ์เรียบร้อย';
          this.saveDraftstatus = 'success';
          setTimeout(() => {
            this.saveDraftstatus = 'fail';
          }, 3000);
          this.GetRest();
          this.status_loading = false;
        } else {
          Swal.fire({
            title: 'error',
            text: res['message'],
            icon: 'error',
          });
          this.status_loading = false;
        }
      }, err => {
        this.status_loading = false;
        console.log(err);
      })
    }
  }
  convertTime(item) {
    const typePO = String(item);
    const yearPO = Number(typePO.substr(0, 4)) + 543;
    const monthPO = typePO.substr(5, 2);
    const dayPO = typePO.substr(8, 2);

    const datePO = dayPO + '/' + monthPO + '/' + yearPO;
    return datePO;
  }
  update() {
    this.status_loading = true;
    this.Operation_Service.UpdateOperation(this.FormData).subscribe(
      (res) => {
        console.log('res BU:', res);
        if (res['status'] === 'success') {
          this.alertMessage = 'บันทึกความเห็นของสายงานปฎิบัติการเรียบร้อย';
          this.saveDraftstatus = 'success';
          setTimeout(() => {
            this.saveDraftstatus = 'fail';
          }, 3000);
          this.GetRest();
          this.status_loading = false;
        } else {
          Swal.fire({
            title: 'error',
            text: res['message'],
            icon: 'error',
          });
          this.status_loading = false;
        }
      },
      (err) => {
        this.status_loading = false;
        console.log(err);
        alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
      }
    );
  }

  validateCommentBu() {
    // console.log('this.validateBu', this.FormData);
    // processConsiders>>subProcess>>subProcessRisk.remark/subject/management
    this.FormData.processConsiders.forEach((process, i) => {
      process.subProcess.forEach((sub) => {
        sub.subProcessRisk.forEach((subRisk) => {
          if (subRisk.management === '' || subRisk.management === null) {
            this.validateBu = this.validateBu + 1;
          }
          if (subRisk.remark === '' || subRisk.remark === null) {
            this.validateBu = this.validateBu + 1;
          }
          if (subRisk.subject === '' || subRisk.subject === null) {
            this.validateBu = this.validateBu + 1;
          }
        });
      });
    });
    // commentOperationProcess>>commentOperation.reviews/subject/manage
    this.FormData.commentOperationProcess.forEach((process, i) => {
      process.commentOperation.forEach((sub) => {
        if (sub.subject === '' || sub.subject === null) {
          this.validateBu = this.validateBu + 1;
        }
        if (sub.manage === '' || sub.manage === null) {
          this.validateBu = this.validateBu + 1;
        }
        if (sub.reviews === '' || sub.reviews === null) {
          this.validateBu = this.validateBu + 1;
        }
      });
    });
    // console.log(this.validateBu);

    return this.validateBu;
  }
  checkPC() {
    this.Comment_PC.requestId = Number(this.requestId);
    for (
      let index = 0;
      index < this.GetData_PO.processConsiders.length;
      index++
    ) {
      const Main = this.GetData_PO.processConsiders[index].subProcess;

      // tslint:disable-next-line: no-shadowed-variable
      for (let i = 0; i < Main.length; i++) {
        const SUB = Main[i].subProcessRisk;
        if (SUB.length !== 0) {
          for (let k = 0; k < SUB.length; k++) {
            if (SUB[k].buCommentId !== null) {
              this.Comment_PC.processConsiders.push({
                buCommentId: SUB[k].buCommentId,
                pcComment: SUB[k].pcComment,
                pcExtraComment: SUB[k].pcExtraComment,
                pcIssue: SUB[k].pcIssue,
              });
            }
          }
        }
      }
    }

    for (
      let index = 0;
      index < this.GetData_PO.commentOperationProcess.length;
      index++
    ) {
      const commentOperation = this.GetData_PO.commentOperationProcess[index]
        .commentOperation;
      // console.log('Main:', commentOperation);
      // tslint:disable-next-line: no-shadowed-variable
      for (let i = 0; i < commentOperation.length; i++) {
        if (commentOperation[i].buCommentId !== null) {
          this.Comment_PC.commentOperationProcess.push({
            buCommentId: commentOperation[i].buCommentId,
            pcComment: commentOperation[i].pcComment,
            pcExtraComment: commentOperation[i].pcExtraComment,
            pcIssue: commentOperation[i].pcIssue,
          });
        } else {
          this.Comment_PC.commentOperationProcess.push({
            buCommentId: commentOperation[i].id,
            pcComment: commentOperation[i].pcComment,
            pcExtraComment: commentOperation[i].pcExtraComment,
            pcIssue: commentOperation[i].pcIssue,
          });
        }
      }
    }

    if (this.GetData_PO.commentOperation1.length !== 0) {
      for (
        let index = 0;
        index < this.GetData_PO.commentOperation1.length;
        index++
      ) {
        const commentOperation1 = this.GetData_PO.commentOperation1;
        // console.log('Main:', commentOperation1);
        // tslint:disable-next-line: no-shadowed-variable
        if (commentOperation1.id !== null) {
          this.Comment_PC.commentOperation1.push({
            buCommentId: commentOperation1[index].id,
            pcComment: commentOperation1[index].pcComment,
            pcExtraComment: commentOperation1[index].pcExtraComment,
            pcIssue: commentOperation1[index].pcIssue,
          });
        }
      }
    }

    if (this.GetData_PO.commentOperation2.length !== 0) {
      for (
        let index = 0;
        index < this.GetData_PO.commentOperation2.length;
        index++
      ) {
        const commentOperation2 = this.GetData_PO.commentOperation2;
        // console.log('Main:', commentOperation1);
        // tslint:disable-next-line: no-shadowed-variable
        if (commentOperation2.id !== null) {
          this.Comment_PC.commentOperation2.push({
            buCommentId: commentOperation2[index].id,
            pcComment: commentOperation2[index].pcComment,
            pcExtraComment: commentOperation2[index].pcExtraComment,
            pcIssue: commentOperation2[index].pcIssue,
          });
        }
      }
    }


    // console.log('processConsiders:', this.Comment_PC.processConsiders);
    const check_process_pcIssue = Object.values(this.Comment_PC.processConsiders).every(value => value['pcIssue'] !== null && value['pcIssue'] !== '');
    const check_process_pcExtraComment = Object.values(this.Comment_PC.processConsiders).every(value => value['pcExtraComment'] !== null && value['pcExtraComment'] !== '');
    const check_process_pcComment = Object.values(this.Comment_PC.processConsiders).every(value => value['pcComment'] !== null && value['pcComment'] !== '');
    console.log('processConsiders:', this.Comment_PC.processConsiders, check_process_pcIssue, check_process_pcExtraComment, check_process_pcComment);

    const check_commentOperation1_pcIssue = Object.values(this.Comment_PC.commentOperation1).every(value => value['pcIssue'] !== null && value['pcIssue'] !== '');
    const check_commentOperation1_pcExtraComment = Object.values(this.Comment_PC.commentOperation1).every(value => value['pcExtraComment'] !== null && value['pcExtraComment'] !== '');
    const check_commentOperation1_pcComment = Object.values(this.Comment_PC.commentOperation1).every(value => value['pcComment'] !== null && value['pcComment'] !== '');
    console.log('commentOperation1:', this.Comment_PC.commentOperation1, check_commentOperation1_pcIssue, check_commentOperation1_pcExtraComment, check_commentOperation1_pcComment);

    const check_commentOperation2_pcIssue = Object.values(this.Comment_PC.commentOperation2).every(value => value['pcIssue'] !== null && value['pcIssue'] !== '');
    const check_commentOperation2_pcExtraComment = Object.values(this.Comment_PC.commentOperation2).every(value => value['pcExtraComment'] !== null && value['pcExtraComment'] !== '');
    const check_commentOperation2_pcComment = Object.values(this.Comment_PC.commentOperation2).every(value => value['pcComment'] !== null && value['pcComment'] !== '');
    console.log('commentOperation2:', this.Comment_PC.commentOperation2, check_commentOperation2_pcIssue, check_commentOperation2_pcExtraComment, check_commentOperation2_pcComment);

    const check_OperationProcess_pcIssue = Object.values(this.Comment_PC.commentOperationProcess).every(value => value['pcIssue'] !== null && value['pcIssue'] !== '');
    const check_OperationProcess_pcExtraComment = Object.values(this.Comment_PC.commentOperationProcess).every(value => value['pcExtraComment'] !== null && value['pcExtraComment'] !== '');
    const check_OperationProcess_pcComment = Object.values(this.Comment_PC.commentOperationProcess).every(value => value['pcComment'] !== null && value['pcComment'] !== '');
    console.log('commentOperationProcess:', this.Comment_PC.commentOperationProcess, check_OperationProcess_pcIssue, check_OperationProcess_pcExtraComment, check_OperationProcess_pcComment);


    if (check_process_pcIssue === true && check_process_pcExtraComment === true && check_process_pcComment === true &&
      check_commentOperation1_pcIssue === true && check_commentOperation1_pcExtraComment === true && check_commentOperation1_pcComment === true &&
      check_commentOperation2_pcIssue === true && check_commentOperation2_pcExtraComment === true && check_commentOperation1_pcComment === true &&
      check_OperationProcess_pcIssue === true && check_OperationProcess_pcExtraComment === true && check_OperationProcess_pcComment === true) {
      this.Comment_PC.status = true;
    }
    this.arryPC = []
    if (check_process_pcIssue === false ||
      check_process_pcExtraComment === false ||
      check_process_pcComment === false) {
      this.arryPC.push('รายละเอียดกระบวนการทำงาน')
    }

    if (check_commentOperation1_pcIssue === false ||
      check_commentOperation1_pcExtraComment === false ||
      check_commentOperation1_pcComment === false) {
      this.arryPC.push('ความเห็นเพิ่มเติม(1)')
    }

    if (check_OperationProcess_pcIssue === false ||
      check_OperationProcess_pcExtraComment === false ||
      check_OperationProcess_pcComment === false) {
      this.arryPC.push('ความพร้อมด้านการดำเนินการ')
    }

    if (check_commentOperation2_pcIssue === false ||
      check_commentOperation2_pcExtraComment === false ||
      check_commentOperation2_pcComment === false) {
      this.arryPC.push('ความเห็นเพิ่มเติม(2)')
    }

    return this.arryPC;
  }
  sendWork() {
    this.saveDraftstatus_State = false;
    // this.Operation_Service.status_state = false;
    if (this.GetRole !== 'PO' && this.GetRole !== 'PC') {

      // if (this.GetLevel == 'ฝ่าย' || this.GetLevel == 'กลุ่ม' || this.GetLevel == 'สาย'
      // ) {
      //   this.router.navigate(['/comment']);
      // }

      this.FormData.requestId = Number(this.requestId);
      for (
        let index = 0;
        index < this.FormData.processConsiders.length;
        index++
      ) {
        const Group = this.FormData.processConsiders[index].subProcess;

        for (let n = 0; n < Group.length; n++) {
          const subProcess = Group[n].id;
          if (typeof subProcess === 'string') {
            console.log('subProcess', subProcess);
            this.FormData.processConsiders[index].subProcess[n].id = '';
          }
        }
      }

      for (
        let i = 0;
        i < this.FormData.requestProcessConsidersSubDelete.length;
        i++
      ) {
        if (
          typeof this.FormData.requestProcessConsidersSubDelete[i] === 'string'
        ) {
          this.FormData.requestProcessConsidersSubDelete.splice(i, 1);
        }
      }
      for (
        let i = 0;
        i < this.FormData.requestProcessConsidersSubDelete.length;
        i++
      ) {
        if (
          typeof this.FormData.requestProcessConsidersSubDelete[i] === 'string'
        ) {
          this.FormData.requestProcessConsidersSubDelete.splice(i, 1);
        }
      }
      // console.log('processConsiders', this.FormData.processConsiders);
      // console.log('Delete', this.FormData.requestProcessConsidersDelete);
      // console.log('Delete Sub', this.FormData.requestProcessConsidersSubDelete);
      // console.log(
      //   'Delete SubRisk',
      //   this.FormData.requestProcessConsidersSubRiskDelete
      // );
      for (let w = 0; w < this.FormData.commentOperation1.length; w++) {
        if (
          this.FormData.commentOperation1[w].id === '' &&
          this.FormData.commentOperation1[w].subject === '' &&
          this.FormData.commentOperation1[w].manage === ''
        ) {
          this.FormData.commentOperation1.splice(w, 1);
        }
      }
      for (let w = 0; w < this.FormData.commentOperation1.length; w++) {
        if (
          this.FormData.commentOperation1[w].id === '' &&
          this.FormData.commentOperation1[w].subject === '' &&
          this.FormData.commentOperation1[w].manage === ''
        ) {
          // console.log('data', w, this.FormData.commentOperation1[w]);
          this.FormData.commentOperation1.splice(w, 1);
        }
      }

      for (let w = 0; w < this.FormData.commentOperation2.length; w++) {
        if (
          this.FormData.commentOperation2[w].id === '' &&
          this.FormData.commentOperation2[w].subject === '' &&
          this.FormData.commentOperation2[w].manage === ''
        ) {
          this.FormData.commentOperation2.splice(w, 1);
        }
      }
      for (let w = 0; w < this.FormData.commentOperation2.length; w++) {
        if (
          this.FormData.commentOperation2[w].id === '' &&
          this.FormData.commentOperation2[w].subject === '' &&
          this.FormData.commentOperation2[w].manage === ''
        ) {
          this.FormData.commentOperation2.splice(w, 1);
        }
      }

      for (
        let index = 0;
        index < this.FormData.processConsiders.length;
        index++
      ) {
        if (
          this.FormData.processConsiders[index].id === '' &&
          this.FormData.processConsiders[index].mainProcess === ''
        ) {
          this.FormData.processConsiders.splice(index, 1);
        }
      }
      for (
        let index = 0;
        index < this.FormData.processConsiders.length;
        index++
      ) {
        if (
          this.FormData.processConsiders[index].id === '' &&
          this.FormData.processConsiders[index].mainProcess === ''
        ) {
          this.FormData.processConsiders.splice(index, 1);
          this.BU_statusUpdate = true;
        } else if (
          this.FormData.processConsiders[index].id === '' &&
          this.FormData.processConsiders[index].mainProcess !== ''
        ) {
          const subProcess = this.FormData.processConsiders[index].subProcess;
          for (let r = 0; r < subProcess.length; r++) {
            if (subProcess[r].inOut === null || subProcess[r].itRelate === null) {
              alert('กรุณาเลือกข้อมูล IN/OUT หรือ IT Related ให้ถูกต้อง');
              this.BU_statusUpdate = false;
              break;
            }
          }

          break;
        } else {
          this.BU_statusUpdate = true;
        }
        // console.log('FormData:', this.FormData);
      }

      console.log('Check::', this.FormData);
      this.statusprocessConsiders = true;
      this.statuscommentOperationProcess = true;
      this.statuscommentOperation1 = true;
      this.statuscommentOperation2 = true;
      this.arrayStatus = [];
      for (let r = 0; r < this.FormData.processConsiders.length; r++) {
        if (this.FormData.processConsiders[r].mainProcess == '' || this.FormData.processConsiders[r].mainProcess == null) {
          this.statusprocessConsiders = false
        } else {
          for (let k = 0; k < this.FormData.processConsiders[r].subProcess.length; k++) {
            const subProcess = this.FormData.processConsiders[r].subProcess[k]
            if ((subProcess.subTitle === '' || subProcess.subTitle === null) ||
              (subProcess.inOut === '' || subProcess.inOut === null) ||
              (subProcess.itRelate === '' || subProcess.itRelate === null)) {
              this.statusprocessConsiders = false;
            }
            else {
              subProcess.subProcessRisk.forEach(risk => {
                if ((risk.risk === '' || risk.risk === null) ||
                  (risk.riskControl === '' || risk.riskControl === null) ||
                  (risk.riskControlOther === '' || risk.riskControlOther === null) ||
                  (risk.remark === '' || risk.remark === null) ||
                  (risk.subject === '' || risk.subject === null) ||
                  (risk.management === '' || risk.management === null)) {
                  console.log('false');
                  this.statusprocessConsiders = false;
                }
              })
            }
          }
        }
      }

      for (let i = 0; i < this.FormData.commentOperation1.length; i++) {
        if ((this.FormData.commentOperation1[i].subject == '' ||
          this.FormData.commentOperation1[i].subject == null) ||
          (this.FormData.commentOperation1[i].manage == '' ||
            this.FormData.commentOperation1[i].manage == null)) {
          this.statuscommentOperation1 = false
        }
      }

      for (let i = 0; i < this.FormData.commentOperationProcess.length; i++) {
        for (let g = 0; g < this.FormData.commentOperationProcess[i].commentOperation.length; g++) {
          if ((this.FormData.commentOperationProcess[i].commentOperation[g].reviews == '' ||
            this.FormData.commentOperationProcess[i].commentOperation[g].reviews == null) ||
            (this.FormData.commentOperationProcess[i].commentOperation[g].manage == '' ||
              this.FormData.commentOperationProcess[i].commentOperation[g].manage == null) ||
            (this.FormData.commentOperationProcess[i].commentOperation[g].subject == '' ||
              this.FormData.commentOperationProcess[i].commentOperation[g].subject == null)) {
            this.statuscommentOperationProcess = false
          }
        }
      }


      for (let i = 0; i < this.FormData.commentOperation2.length; i++) {
        if ((this.FormData.commentOperation2[i].subject == '' ||
          this.FormData.commentOperation2[i].subject == null) ||
          (this.FormData.commentOperation2[i].manage == '' ||
            this.FormData.commentOperation2[i].manage == null)) {
          this.statuscommentOperation2 = false
        }
      }

      this.validateCommentBu();
      console.log('statusprocessConsiders', this.statusprocessConsiders);
      console.log('statuscommentOperationProcess', this.statuscommentOperationProcess);
      console.log('statuscommentOperation1', this.statuscommentOperation1);
      console.log('statuscommentOperation2', this.statuscommentOperation2);
      // console.log('RequestId', this.requestId);
      if (this.statusprocessConsiders == false) {
        this.arrayStatus.push('รายละเอียดกระบวนการทำงาน')
      }
      if (this.statuscommentOperationProcess == false) {
        this.arrayStatus.push('ความพร้อมด้านการดำเนินการ')
      }
      if (this.statuscommentOperation1 == false) {
        this.arrayStatus.push('ความเห็นเพิ่มเติม(1)')
      }
      if (this.statuscommentOperation2 == false) {
        this.arrayStatus.push('ความเห็นเพิ่มเติม(2)')
      }
      if (this.GetRole !== 'PO' && this.GetRole !== 'PC' && this.GetRole === 'Operation') {

        if ((this.validateBu > 0 && this.GetLevel !== 'แอดมิน') || this.arrayStatus.length > 0) {
          alert('กรุณากรอกข้อมูลตาราง ' + this.arrayStatus + ' ให้ครบถ้วน');
          this.validateBu = 0;
        } else {
          this.Operation_Service.UpdateOperation(this.FormData).subscribe((ress) => {
            console.log(ress)
            if (ress['status'] === 'success' || (this.GetLevel == 'ฝ่าย' || this.GetLevel == 'กลุ่ม' || this.GetLevel == 'สาย')) {
              this.Submit.SubmitWork(this.requestId).subscribe(
                (res) => {
                  console.log(res);
                  if (res['status'] === 'success') {
                    // alert('ส่งงานเรียบร้อย');
                    this.status_loading = false
                    Swal.fire({
                      title: 'success',
                      text: 'ส่งงานเรียบร้อย',
                      icon: 'success',
                      showConfirmButton: false,
                      timer: 3000,
                    }).then(() => {
                      this.router.navigate(['/dashboard1']);
                    })

                    this.FormData = {
                      requestId: null,
                      processConsiders: [],
                      deleteCommentOperation1: [],
                      deleteCommentOperation2: [],
                      commentOperationProcess: [],
                      commentOperation1: [],
                      commentOperation2: [],
                      requestProcessConsidersDelete: [],
                      requestProcessConsidersSubDelete: [],
                      requestProcessConsidersSubRiskDelete: [],
                    };

                    // this.router.navigate(['/dashboard1']);
                  } else {
                    if (res['message'] === 'กรุณาไปให้ความเห็นหน้า ความเห็น / ส่งงาน') {
                      this.router.navigate(['/comment']);
                    } else {
                      Swal.fire({
                        title: 'error',
                        text: res['message'],
                        icon: 'error',
                        // showConfirmButton: false,
                        // timer: 1500,
                      }).then(() => {
                        this.POcommentConsider(); // this should execute now
                      });
                    }
                    // alert(res['message']);

                  }
                },
                (err) => {
                  console.log(err);
                  Swal.fire({
                    title: 'error',
                    text: err,
                    icon: 'error',
                    showConfirmButton: false,
                    timer: 3000,
                  });
                }
              );
            } else {
              Swal.fire({
                title: 'error',
                text: ress['message'],
                icon: 'error',
                showConfirmButton: false,
                timer: 3000,
              });
            }

          }, err => {
            this.status_loading = false;
            console.log(err);
          })


        }


      }
    } else if (this.GetRole === 'PO') {
      const arry = this.CheckValePO();
      this.CommentPO.requestId = Number(this.requestId);
      if (arry.length > 0) {
        alert('กรุณาให้ความเห็นตาราง ' + arry + ' ให้ครบ')
        this.CommentPO = {
          requestId: localStorage.getItem('requestId'),
          status: false,
          commentOperationProcess: [],
          processConsiders: [],
          commentOperation1: [],
          commentOperation2: [],
        };
      }
      else if (arry.length == 0) {
        this.Operation_Service.UpdataCommentOperation_PO(
          this.CommentPO
        ).subscribe((resSaved) => {
          if (resSaved['status'] == 'success' || (this.GetLevel == 'ฝ่าย' || this.GetLevel == 'กลุ่ม' || this.GetLevel == 'สาย')) {
            this.Submit.sendDocument_PO(this.requestId, 'Operation').subscribe(
              (res) => {
                console.log(res);
                if (res['status'] === 'success') {
                  // alert('ส่งงานเรียบร้อย');
                  Swal.fire({
                    title: 'success',
                    text: 'ส่งงานเรียบร้อย',
                    icon: 'success',
                    showConfirmButton: false,
                    timer: 3000,
                  }).then(() => {
                    localStorage.setItem('requestId', '');
                    this.router.navigate(['/dashboard1']);
                  })
                } else {
                  // alert(res['message']);
                  if (res['message'] === 'กรุณาไปให้ความเห็นหน้า ความเห็น / ส่งงาน') {
                    this.router.navigate(['/comment']);
                  } else {
                    Swal.fire({
                      title: 'error',
                      text: res['message'],
                      icon: 'error',
                      showConfirmButton: false,
                      timer: 3000,
                    });
                  }
                }
              },
              (err) => {
                console.log(err);
                alert('เกิดข้อผิดพลาดไม่สามารถส่งงานได้');
              }
            );
          } else {
            Swal.fire({
              title: 'error',
              text: resSaved['message'],
              icon: 'error',
              showConfirmButton: false,
              timer: 3000,
            });
          }
        })

      }

    } else if (this.GetRole === 'PC') {
      const arr = this.checkPC()
      if (arr.length > 0) {
        alert('กรุณาให้ความเห็นตาราง ' + arr + ' ให้ครบ')
        this.Comment_PC = {
          requestId: localStorage.getItem('requestId'),
          status: false,
          commentOperationProcess: [],
          processConsiders: [],
          commentOperation1: [],
          commentOperation2: [],
        };
      }
      else {
        this.Operation_Service.UpdataCommentOperation_PC(
          this.Comment_PC
        ).subscribe((res) => {
          if (res['status'] === 'success') {
            this.Submit.sendComment_PC(this.requestId).subscribe(
              (res) => {
                console.log(res);
                if (res['status'] === 'success') {
                  // alert('ส่งงานเรียบร้อย');
                  Swal.fire({
                    title: 'success',
                    text: 'ส่งงานเรียบร้อย',
                    icon: 'success',
                    showConfirmButton: false,
                    timer: 3000,
                  }).then(() => {
                    localStorage.setItem('requestId', '');
                    this.router.navigate(['/dashboard1']);
                  })
                } else {
                  // alert(res['message']);
                  Swal.fire({
                    title: 'error',
                    text: res['message'],
                    icon: 'error',
                    showConfirmButton: false,
                    timer: 3000,
                  });
                }
              },
              (err) => {
                console.log(err);
                alert('เกิดข้อผิดพลาดไม่สามารถส่งงานได้');
              }
            );
          } else {
            // alert(res['message']);
            Swal.fire({
              title: 'error',
              text: res['message'],
              icon: 'error',
              showConfirmButton: false,
              timer: 3000,
            });
          }
        })
      }


    } else {
      Swal.fire({
        title: 'error',
        text: 'ไม่สามารถส่งงานได้',
        icon: 'error',
        showConfirmButton: false,
        timer: 3000,
      });
    }
  }
  CheckValePO() {
    // console.log(this.CommentPO)
    // this.CommentPO.requestId = Number(this.requestId);
    // console.log(this.GetData_PO);
    const arr = [];
    for (
      let index = 0;
      index < this.GetData_PO.processConsiders.length;
      index++
    ) {
      const Main = this.GetData_PO.processConsiders[index].subProcess;

      // tslint:disable-next-line: no-shadowed-variable
      for (let i = 0; i < Main.length; i++) {
        const SUB = Main[i].subProcessRisk;
        // console.log(SUB)
        if (SUB.length !== 0) {
          for (let k = 0; k < SUB.length; k++) {
            if (SUB[k].buCommentId !== null) {
              this.CommentPO.processConsiders.push({
                buCommentId: SUB[k].buCommentId,
                poComment: SUB[k].poComment,
              });
            }
          }
        }
      }
    }

    for (
      let index = 0;
      index < this.GetData_PO.commentOperationProcess.length;
      index++
    ) {
      const commentOperation = this.GetData_PO.commentOperationProcess[index]
        .commentOperation;
      // console.log('Main:', commentOperation);
      // tslint:disable-next-line: no-shadowed-variable
      for (let i = 0; i < commentOperation.length; i++) {
        if (commentOperation[i].buCommentId !== null) {
          this.CommentPO.commentOperationProcess.push({
            buCommentId: commentOperation[i].buCommentId,
            poComment: commentOperation[i].poComment,
          });
        } else {
          this.CommentPO.commentOperationProcess.push({
            buCommentId: commentOperation[i].id,
            poComment: commentOperation[i].poComment,
          });
        }
      }
    }

    if (this.GetData_PO.commentOperation1.length !== 0) {
      for (
        let index = 0;
        index < this.GetData_PO.commentOperation1.length;
        index++
      ) {
        const commentOperation1 = this.GetData_PO.commentOperation1;
        // console.log('Main:', commentOperation1);
        // tslint:disable-next-line: no-shadowed-variable
        if (commentOperation1[index].id !== null) {
          this.CommentPO.commentOperation1.push({
            buCommentId: commentOperation1[index].id,
            poComment: commentOperation1[index].poComment,
          });
        }
      }
    }

    if (this.GetData_PO.commentOperation2.length !== 0) {
      for (
        let index = 0;
        index < this.GetData_PO.commentOperation2.length;
        index++
      ) {
        const commentOperation2 = this.GetData_PO.commentOperation2;
        // console.log('Main:', commentOperation1);
        // tslint:disable-next-line: no-shadowed-variable
        if (commentOperation2[index].id !== null) {
          this.CommentPO.commentOperation2.push({
            buCommentId: commentOperation2[index].id,
            poComment: commentOperation2[index].poComment,
          });
        }
      }
    }


    const check_commentOperation1 = Object.values(this.CommentPO.commentOperation1).every(value => value['poComment'] !== '' && value['poComment'] !== null);
    const check_commentOperation2 = Object.values(this.CommentPO.commentOperation2).every(value => value['poComment'] !== '' && value['poComment'] !== null);
    const check_commentOperationProcess = Object.values(this.CommentPO.commentOperationProcess).every(value => value['poComment'] !== '' && value['poComment'] !== null);
    const check_processConsiders = Object.values(this.CommentPO.processConsiders).every(value => value['poComment'] !== '' && value['poComment'] !== null);


    if (check_commentOperation1 === true &&
      check_commentOperation2 === true &&
      check_commentOperationProcess === true &&
      check_processConsiders === true
    ) {
      this.CommentPO.status = true;
    }

    if (check_processConsiders == false) {
      arr.push('รายละเอียดกระบวนการทำงาน')
    }
    if (check_commentOperationProcess == false) {
      arr.push('ความพร้อมด้านการดำเนินการ')
    }
    if (check_commentOperation1 == false) {
      arr.push('ความเห็นเพิ่มเติม(1)')
    }
    if (check_commentOperation2 == false) {
      arr.push('ความเห็นเพิ่มเติม(2)')
    }

    return arr;
  }

  POcommentConsider() {
    this.Submit.linkToAny();
  }
  // new table ================================================================================
  checkStaticTitle(index) {
    return this.staticTitleIndex.includes(index);
  }

  drop(event: CdkDragDrop<string[]>) {
    this.changeSaveDraft();
    const fixedBody = this.staticTitleIndex;
    console.log('event Group:', event);
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (event.previousContainer.data.length > 1) {
      // check if only one left child
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (fixedBody.includes(this.dropIndex)) {
      // check only 1 child left and static type move only not delete
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      const templatesubTitleRisk = {
        id: this.makeid(5),
        subTitle: '',
        inOut: '',
        itRelate: '',
        subProcessRisk: [
          {
            id: '',
            risk: '',
            riskControl: '',
            riskControlOther: '',
            remark: '',
            subject: '',
            management: '',
          },
        ],
      };

      this.FormData.processConsiders[this.dropIndex].subProcess.push(
        templatesubTitleRisk
      );
    } else {
      // check only 1 child left and not a static type move and delete
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      // remove current row
      this.removeAddItem(this.dropIndex);
      this.dropIndex = null;
    }
    this.changeSaveDraft();
  }

  dragExited(event: CdkDragExit<string[]>) {
    console.log('Exited', event);
  }

  entered(event: CdkDragEnter<string[]>) {
    console.log('Entered', event);
  }

  startDrag(event: CdkDragStart<string[]>, istep) {
    // console.log('startDrag', event);
    // console.log('startDrag index', istep);
    this.dropIndex = istep;
  }

  removeAddItem(index) {
    this.changeSaveDraft();
    if (index != null) {
      this.FormData.requestProcessConsidersDelete.push(
        this.FormData.processConsiders[index].id
      );
      this.FormData.processConsiders.splice(index, 1);
    }
  }

  // ==================================================================
  dropItem(event: CdkDragDrop<string[]>) {
    this.changeSaveDraft();
    console.log('event item :', event);
    console.log('previousContainer :', event.container.data.length);
    console.log('index group', this.dropIndex);
    console.log('index item:', this.Risk_dropIndex);
    const fixedBody = this.staticTitleIndexRisk;

    // if (
    //   event.previousContainer.data.length === 1 &&
    //   event.container.data.length !== 1
    // ) {
    //   console.log('index:', this.dropIndex);
    //   this.removeAddItem(this.dropIndex);
    //   this.dropIndex = null;
    // }
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (event.previousContainer.data.length > 1) {
      // check if only one left child
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      // check only 1 child left and not a static type move and delete
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      // remove current row
      this.removeAddItemRisk(this.dropIndex, this.Risk_dropIndex);
      this.dropIndex = null;
      this.Risk_dropIndex = null;
    }
    this.changeSaveDraft();
  }

  connectItem(): any[] {
    const mapping = [];
    for (
      let index = 0;
      index < this.FormData.processConsiders.length;
      index++
    ) {
      const Group = this.FormData.processConsiders[index].subProcess;

      // tslint:disable-next-line: no-shadowed-variable
      // this.FormData.operations[index].subProcess.forEach(
      //   ( item, i) =>
      //    mapping.push(`${this.FormData.operations[index].subProcess[i].id}`)
      // );

      for (let j = 0; j < Group.length; j++) {
        mapping.push(String(Group[j].id));
      }
    }

    // console.log('map', mapping);
    return mapping;
  }

  removeAddItemRisk(subIndex, Riskkindex) {
    this.changeSaveDraft();
    if (this.FormData.processConsiders[subIndex].subProcess.length === 1) {
      this.FormData.requestProcessConsidersSubDelete.push(
        this.FormData.processConsiders[subIndex].id
      );
      this.FormData.processConsiders.splice(subIndex, 1);
    } else if (
      this.FormData.processConsiders[subIndex].subProcess.length > 1 &&
      Riskkindex != null
    ) {
      this.FormData.requestProcessConsidersSubDelete.push(
        this.FormData.processConsiders[subIndex].subProcess[Riskkindex].id
      );
      this.FormData.processConsiders[subIndex].subProcess.splice(Riskkindex, 1);
    }
  }

  startDrag_Risk(event: CdkDragStart<string[]>, isup, istep) {
    this.changeSaveDraft();
    console.log('Risks tartDrag', event);
    console.log('Risk startDrag index', isup);
    this.Risk_dropIndex = isup;
    this.dropIndex = istep;
  }
  enteredRisk(event: CdkDragEnter<string[]>) {
    console.log('Entered Risk', event);
  }
  dragExitedRisk(event: CdkDragExit<string[]>) {
    console.log('Exited Risk', event);
  }

  dragEnded(event: CdkDragEnd) {
    console.log('dragEnded Event > item', event.source.data);
  }

  checkStaticTitle_Risk(index) {
    return this.staticTitleIndexRisk.includes(index);
  }
  // ===============================================================================================

  addRow() {
    this.changeSaveDraft();
    const tempateAddRow = {
      id: '',
      mainProcess: '',
      validate: true,
      subProcess: [
        {
          validate: true,
          id: this.makeid(5),
          subTitle: '',
          inOut: '',
          itRelate: '',
          subProcessRisk: [
            {
              validate: true,
              id: '',
              risk: '',
              riskControl: '',
              riskControlOther: '',
              remark: '',
              subject: '',
              management: '',
            },
          ],
        },
      ],
    };
    this.FormData.processConsiders.push(tempateAddRow);
  }

  removeDataItem(stepindex, subindex, riskIndex) {
    this.changeSaveDraft();
    console.log('stepindex', stepindex);
    console.log('subindex', subindex);
    console.log('riskIndex', riskIndex);
    const fixedBody = [];
    // tslint:disable-next-line: max-line-length
    if (
      fixedBody.includes(stepindex) &&
      this.FormData.processConsiders[stepindex].subProcess[subindex]
        .subProcessRisk.length > 1 &&
      this.Risk_dropIndex !== 0
    ) {
      console.log('delete fixed row only have many risk[]');
      // this.FormData.operations[stepindex].detailDelete.push(this.FormData.operations[stepindex].subProcess[subindex]['id']);
      // console.log(
      //   this.FormData.processConsiders[stepindex].subProcess[subindex]
      //     .subProcessRisk[riskIndex].id
      // );
      if (
        this.FormData.processConsiders[stepindex].subProcess[subindex]
          .subProcessRisk[riskIndex].id !== ''
      ) {
        this.FormData.requestProcessConsidersSubRiskDelete.push(
          this.FormData.processConsiders[stepindex].subProcess[subindex]
            .subProcessRisk[riskIndex].id
        );
      }

      this.FormData.processConsiders[stepindex].subProcess[
        subindex
      ].subProcessRisk.splice(riskIndex, 1);
    } else if (
      fixedBody.includes(stepindex) &&
      this.FormData.processConsiders[stepindex].subProcess[subindex]
        .subProcessRisk.length === 1
    ) {
      console.log('3 clear fixed row only');
      if (subindex !== 0) {
        if (
          this.FormData.processConsiders[stepindex].subProcess[subindex].id !==
          ''
        ) {
          this.FormData.requestProcessConsidersSubDelete.push(
            this.FormData.processConsiders[stepindex].subProcess[subindex].id
          );
        }
        this.FormData.processConsiders[stepindex].subProcess.splice(
          subindex,
          1
        );
      } else {
        this.FormData.requestProcessConsidersSubDelete.push(
          this.FormData.processConsiders[stepindex].subProcess[subindex].id
        );

        Object.keys(
          this.FormData.processConsiders[stepindex].subProcess[subindex]
        ).forEach((key, index) => {
          if (key === 'subProcessRisk') {
            // tslint:disable-next-line: no-shadowed-variable
            for (
              // tslint:disable-next-line: no-shadowed-variable
              let index = 0;
              index <
              this.FormData.processConsiders[stepindex].subProcess[subindex][
                key
              ].length;
              index++
            ) {
              Object.keys(
                this.FormData.processConsiders[stepindex].subProcess[subindex][
                key
                ][riskIndex]
              ).forEach((sub, i) => {
                this.FormData.processConsiders[stepindex].subProcess[subindex][
                  key
                ][index][sub] = '';
              });
            }
          } else {
            this.FormData.processConsiders[stepindex].subProcess[subindex][
              key
            ] = '';
          }
        });
      }

      // console.log(
      //   'tt',
      //   this.FormData.operations[stepindex].subProcess[subindex]
      // );

      // this.FormData.operations[stepindex].detailDelete.push(
      //   this.FormData.operations[stepindex].subProcess[subindex]['id']
      // );
    } else if (
      this.FormData.processConsiders[stepindex].subProcess.length === 1 &&
      this.FormData.processConsiders[stepindex].subProcess[subindex]
        .subProcessRisk.length > 1
    ) {
      console.log(' delete risk row only have 1 row');
      if (
        this.FormData.processConsiders[stepindex].subProcess[subindex]
          .subProcessRisk[riskIndex].id !== ''
      ) {
        this.FormData.requestProcessConsidersSubRiskDelete.push(
          this.FormData.processConsiders[stepindex].subProcess[subindex]
            .subProcessRisk[riskIndex].id
        );
      }
      this.FormData.processConsiders[stepindex].subProcess[
        subindex
      ].subProcessRisk.splice(riskIndex, 1);
    } else if (
      this.FormData.processConsiders[stepindex].subProcess.length > 1 &&
      this.FormData.processConsiders[stepindex].subProcess[subindex]
        .subProcessRisk.length > 1
    ) {
      console.log(' delete risk row only have many row');
      // this.FormData.operations[stepindex].detailDelete.push(
      //   this.FormData.operations[stepindex].subProcess[subindex]['id']
      // );
      if (
        this.FormData.processConsiders[stepindex].subProcess[subindex]
          .subProcessRisk[riskIndex].id !== ''
      ) {
        this.FormData.requestProcessConsidersSubRiskDelete.push(
          this.FormData.processConsiders[stepindex].subProcess[subindex]
            .subProcessRisk[riskIndex].id
        );
      }
      this.FormData.processConsiders[stepindex].subProcess[
        subindex
      ].subProcessRisk.splice(riskIndex, 1);
    } else if (
      this.FormData.processConsiders[stepindex].subProcess[subindex]
        .subProcessRisk.length === 0
    ) {
      console.log(
        'delete row permanent',
        this.FormData.processConsiders[stepindex].id
      );
      // this.FormData.operationsDelete.push(
      //   this.FormData.operations[stepindex].id
      // );
      if (this.FormData.processConsiders[stepindex].id !== '') {
        this.FormData.requestProcessConsidersDelete.push(
          this.FormData.processConsiders[stepindex].id
        );
        this.FormData.processConsiders.splice(stepindex, 1);
      }
    } else if (
      this.FormData.processConsiders[stepindex].subProcess[subindex]
        .subProcessRisk.length === 1 &&
      subindex !== 0 &&
      this.FormData.processConsiders[stepindex].subProcess.length > 0
    ) {
      console.log('delete row have 1 sub , 1 risk');
      if (
        this.FormData.processConsiders[stepindex].subProcess[subindex].id !== ''
      ) {
        this.FormData.requestProcessConsidersSubDelete.push(
          this.FormData.processConsiders[stepindex].subProcess[subindex].id
        );
        this.FormData.processConsiders[stepindex].subProcess.splice(
          subindex,
          1
        );
      }
    } else if (
      this.FormData.processConsiders[stepindex].subProcess[subindex]
        .subProcessRisk.length === 1 &&
      subindex === 0 &&
      this.FormData.processConsiders[stepindex].subProcess.length === 1
    ) {
      console.log('delete row have 1 sub , 1 risk and delete main row');
      if (this.FormData.processConsiders[stepindex].id !== '') {
        this.FormData.requestProcessConsidersDelete.push(
          this.FormData.processConsiders[stepindex].id
        );
      }
      this.FormData.processConsiders.splice(stepindex, 1);
    } else if (
      this.FormData.processConsiders[stepindex].subProcess[subindex]
        .subProcessRisk.length === 1 &&
      subindex === 0 &&
      this.FormData.processConsiders[stepindex].subProcess.length > 1
    ) {
      console.log('delete row have many sub , 1 risk and delete 1 sub ');
      if (
        this.FormData.processConsiders[stepindex].subProcess[subindex].id !== ''
      ) {
        this.FormData.requestProcessConsidersSubDelete.push(
          this.FormData.processConsiders[stepindex].subProcess[subindex].id
        );
      }
      this.FormData.processConsiders[stepindex].subProcess.splice(subindex, 1);
    } else {
      console.log('not in case');
    }
    this.changeSaveDraft();
  }

  Add_commentOperation_1() {
    this.changeSaveDraft();
    const commentOperation1 = {
      id: '',
      subject: '',
      manage: '',
      poComment: null,
    };

    this.FormData.commentOperation1.push(commentOperation1);
  }

  Add_commentOperation_2() {
    this.changeSaveDraft();
    const commentOperation2 = {
      id: '',
      subject: '',
      manage: '',
      poComment: null,
    };

    this.FormData.commentOperation2.push(commentOperation2);
  }

  // ============================================PO Role========================================================//

  // function gen KEY เพื่อเชื่อม group dragable table
  makeid(length) {
    let result = '';
    const characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  backToDashboardPage() {
    // localStorage.setItem('requestId', '');
    this.router.navigate(['dashboard1']);
  }

  autoGrowTextZone(e) {
    e.target.style.height = '0px';
    e.target.style.overflow = 'hidden';
    e.target.style.height = (e.target.scrollHeight + 0) + 'px';
  }
  covertDateToCorrect(dueDate: any) {
    if (dueDate !== null) {
      const type = String(dueDate);
      const year = Number(type.substr(0, 4)) + 543;
      const month = type.substr(5, 2);
      const day = type.substr(8, 2);
      const date = day + '/' + month + '/' + year;
      return date;
    } else {
      return '';
    }
  }
}
