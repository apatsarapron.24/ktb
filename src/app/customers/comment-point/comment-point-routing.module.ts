import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RiskComponent } from './risk/risk.component';
import { CommentPointComponent } from './comment-point.component';
import { FinanceComponent } from './finance/finance.component';
import { OperationComponent } from './operation/operation.component';
import { RetailComponent } from './retail/retail.component';
import { ComplianceComponent } from './compliance/compliance.component';
import { DialogStatusService } from '../../services/comment-point/dialog-status/dialog-status.service';

const routes: Routes = [
  {
    path: '',
    component: CommentPointComponent,
  },
  {
    path: 'risk',
    component: RiskComponent,
    canDeactivate: [DialogStatusService]
  },
  {
    path: 'finance',
    component: FinanceComponent,
    canDeactivate: [DialogStatusService]
  },
  {
    path: 'operation',
    component: OperationComponent,
    canDeactivate: [DialogStatusService]

  }, {
    path: 'retail',
    component: RetailComponent,
    canDeactivate: [DialogStatusService]
  },
  {
    path: 'compliance',
    component: ComplianceComponent,
    canDeactivate: [DialogStatusService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CommentPointRoutingModule { }
