import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogFinanceComponent } from './dialog-finance.component';

describe('DialogFinanceComponent', () => {
  let component: DialogFinanceComponent;
  let fixture: ComponentFixture<DialogFinanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogFinanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogFinanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
