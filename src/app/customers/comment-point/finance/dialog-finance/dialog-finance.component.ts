import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import Swal from 'sweetalert2';

export interface DialogData {
  headerDetail: any;
  bodyDetail: any;
  deleteStatus: any;
}

@Component({
  selector: 'app-dialog-finance',
  templateUrl: './dialog-finance.component.html',
  styleUrls: ['./dialog-finance.component.scss']
})
export class DialogFinanceComponent implements OnInit {
  close() {
    this.data.deleteStatus = false;
    this.dialogRef.close(this.data);
  }

  back() {
    this.data.deleteStatus = false;
    this.dialogRef.close(this.data);
  }

  save() {
    this.data.deleteStatus = true;
    this.dialogRef.close(this.data);
  }


  constructor(
    private dialogRef: MatDialogRef<DialogFinanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.dialogRef.disableClose = true;
  }

  ngOnInit() {
  }

}
