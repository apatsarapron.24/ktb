import { from } from 'rxjs';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { saveAs } from 'file-saver';
import { DialogFinanceComponent } from './dialog-finance/dialog-finance.component';
import { CommentManagementComponent } from '../comment-management/comment-management.component';
import { FinanceService } from '../../../services/comment-point/finance/finance.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
// import { CommentManagementComponent } from '../comment-management/comment-management.component';
import { DialogStatusService } from '../../../services/comment-point/dialog-status/dialog-status.service';
import { DialogSaveStatusComponent } from '../../comment-point/dialog-save-status/dialog-save-status.component';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { promise } from 'protractor';
import { RequestService } from '../../../services/request/request.service';

@Component({
  selector: 'app-finance',
  templateUrl: './finance.component.html',
  styleUrls: ['./finance.component.scss'],
})
export class FinanceComponent implements OnInit {
  note = [
    'No	คือ	ไม่ใช่ประเด็นคงค้าง',
    'Pending 1	คือ	ต้องแก้ไขก่อนประกาศใช้ผลิตภัณฑ์',
    'Pending 2 คือ	สามารถใช้ผลิตภัณฑ์ได้ก่อน แล้วค่อยแก้ไขตามหลังได้ ',
    'Pending 3	คือ	ไม่เกี่ยวข้องกับการประกาศใช้ผลิตภัณฑ์',
    'Pending 4	คือ	ต้องแก้ไขให้แล้วเสร็จก่อนนำเสนอเรื่องต่อบอร์ด',
  ];

  data = {
    deleteFinanceBudget: [],
    deleteFinancePerformance: [],
    deleteFinanceGovernance: [],
    template: [
      {
        tableHead: 'การบัญชี, งบประมาณและเบิกจ่าย',
        tableBody: [
          {
            id: null,
            topic:
              '1. มีการประมาณการต้นทุน และค่าใช้จ่ายในการดำเนินงานของทั้งโครงการ จากการออกผลิตภัณฑ์/บริการ',
            requestId: null,
            inspection: null,
            comment: null,
            reviews: null,
            poComment: null,
            pcComment: null,
            pcExtraComment: null,
            pcIssue: null,
          },
          {
            topic:
              '2. ผลิตภัณฑ์ที่ออกหรือปรับปรุงใหม่มีการสร้างรหัสบัญชีใหม่หรือไม่',
            requestId: null,
            inspection: null,
            comment: null,
            reviews: null,
            poComment: null,
            pcComment: null,
            pcExtraComment: null,
            pcIssue: null,
          },
          {
            topic: '3. มีการกำหนดวิธีการบันทึกบัญชีใหม่หรือไม่',
            requestId: null,
            inspection: null,
            comment: null,
            reviews: null,
            poComment: null,
            pcComment: null,
            pcExtraComment: null,
            pcIssue: null,
          },
          {
            topic: '4. ผลิตภัณฑ์มีรายได้ที่เกี่ยวข้องกับภาษีหรือไม่',
            requestId: null,
            inspection: null,
            comment: null,
            reviews: null,
            poComment: null,
            pcComment: null,
            pcExtraComment: null,
            pcIssue: null,
          },
          {
            topic:
              '5. มีการจัดทำมาตรฐานการรายงานทางการเงิน ฉบับที่ 9 (IFRS 9) หรือไม่',
            requestId: null,
            inspection: null,
            comment: null,
            reviews: null,
            poComment: null,
            pcComment: null,
            pcExtraComment: null,
            pcIssue: null,
          },
        ],
      },
      {
        tableHead: 'การวัดผลงานภายหลังออกผลิตภัณฑ์',
        tableBody: [
          {
            id: null,
            topic:
              '1. มีการกำหนดแนวทาง/ตัวชี้วัดความสามารถในการทำกำไรของทั้งโครงการ จากออกผลิตภัณฑ์/บริการ',
            requestId: null,
            inspection: null,
            comment: null,
            reviews: null,
            poComment: null,
            pcComment: null,
            pcExtraComment: null,
            pcIssue: null,
          },
          {
            id: null,
            topic:
              '2. มีการกำหนด Field Requirement สำหรับการออกผลิตภัณฑ์หรือการปรับปรุงผลิตภัณฑ์ ครบถ้วน (ตามเอกสารแนบ Field Requirment)',
            requestId: null,
            inspection: null,
            comment: null,
            reviews: null,
            poComment: null,
            pcComment: null,
            pcExtraComment: null,
            pcIssue: null,
          },
          {
            id: null,
            // tslint:disable-next-line:max-line-length
            topic:
              // tslint:disable-next-line: max-line-length
              '3. ผลิตภัณฑ์ที่ออกหรือปรับปรุงใหม่มีความพร้อมในการออกรายงาน เช่น ชื่อรายงาน, รูปแบบและเงื่อนไข, ความถี่ในการออกรายงาน,สิทธิ์ของผู้ใช้ข้อมูลหรือรายงาน และมีช่องทางในการเผยแพร่ครบถ้วนหรือไม่',
            requestId: null,
            inspection: null,
            comment: null,
            reviews: null,
            poComment: null,
            pcComment: null,
            pcExtraComment: null,
            pcIssue: null,
          },
          {
            id: null,
            // tslint:disable-next-line:max-line-length
            topic:
              '4. มีการหารือและกำหนดแนวทางในการจัดสรรรายได้และค่าใช้จ่าย (Revenue & Cost Allocation) พร้อมทั้งระบุ product owner ให้ชัดเจน',
            requestId: null,
            inspection: null,
            comment: null,
            reviews: null,
            poComment: null,
            pcComment: null,
            pcExtraComment: null,
            pcIssue: null,
          },
        ],
      },
      {
        tableHead: 'Data Governance (DG)',
        tableBody: [
          {
            id: null,
            // tslint:disable-next-line:max-line-length
            topic:
              // tslint:disable-next-line: max-line-length
              '1. ผลิตภัณฑ์ที่ออกหรือปรับปรุงใหม่มีผังเส้นทางเดินของข้อมูล (Data Flow) และผังการทำงานของระบบงาน (Application Flow) และมีกำหนดแนวทางการจัดเก็บข้อมูลสำรอง (Back up Process)',
            requestId: null,
            inspection: null,
            comment: null,
            reviews: null,
            poComment: null,
            pcComment: null,
            pcExtraComment: null,
            pcIssue: null,
          },
          {
            id: null,
            // tslint:disable-next-line:max-line-length
            topic:
              // tslint:disable-next-line: max-line-length
              '2. ผลิตภัณฑ์ที่ออกหรือปรับปรุงใหม่มีแนวทางการรักษาความปลอดภัยของระบบ (security) และแนวทางรายงานเหตุการณ์ผิดปกติของระบบ (Incident Response)',
            requestId: null,
            inspection: null,
            comment: null,
            reviews: null,
            poComment: null,
            pcComment: null,
            pcExtraComment: null,
            pcIssue: null,
          },
          {
            id: null,
            // tslint:disable-next-line:max-line-length
            topic:
              // tslint:disable-next-line: max-line-length
              '3. ผลิตภัณฑ์ที่ออกหรือปรับปรุงใหม่มีคู่มือปฏิบัติงานของระบบงาน รวมทั้งมีรายชื่อ Fields ในระบบพร้อมคำอธิบายที่เกี่ยวข้อง  (Data Dictionary)',
            requestId: null,
            inspection: null,
            comment: null,
            reviews: null,
            poComment: null,
            pcComment: null,
            pcExtraComment: null,
            pcIssue: null,
          },
          {
            id: null,
            // tslint:disable-next-line:max-line-length
            topic:
              '4. ผลิตภัณฑ์ที่ออกหรือปรับปรุงใหม่มีความพร้อมในการส่งข้อมูลเข้า BDW (Process Flow)',
            requestId: null,
            inspection: null,
            comment: null,
            reviews: null,
            poComment: null,
            pcComment: null,
            pcExtraComment: null,
            pcIssue: null,
          },
          {
            id: null,
            // tslint:disable-next-line:max-line-length
            topic:
              '5. ผลิตภัณฑ์ที่ออกหรือปรับปรุงใหม่ต้องมีการรายงานต่อ ธปท. หรือไม่ (หากมี มีขั้นตอนอะไรบ้าง)',
            requestId: null,
            inspection: null,
            comment: null,
            reviews: null,
            poComment: null,
            pcComment: null,
            pcExtraComment: null,
            pcIssue: null,
          },
        ],
      },
    ],
    comments: [
      {
        id: 1,
        question: {
          id: 1,
          date: ' 02/06/2563  เวลา 13:00:54 น.',
          by: 'พนักงานดูแลหน่วยงานบริหารความเสี่ยง',
          detail: 'detail text',
          attachments: {
            file: [],
            fileDelete: [],
          },
          edit_status: true, // เช็คว่าเเก้ไขได้หรอไม่
          edit_comment_status: false, // เช็คสถานะการเเก้ไขความคิดเห็น
          status_submit: false, // เช็คว่าเคยส่งงานหรือยัง
        },
        answers: {
          id: 1,
          date: ' 02/06/2563  เวลา 13:00:54 น.',
          by: 'พนักงานดูแลหน่วยงานบริหารความเสี่ยง',
          detail: 'detail text',
          attachments: {
            file: [],
            fileDelete: [],
          },
          edit_status: true,
          edit_comment_status: false,
          status_submit: false,
        },
      },
      {
        id: 2,
        question: {
          id: 2,
          date: ' 02/06/2563  เวลา 13:00:54 น.',
          by: 'พนักงานดูแลหน่วยงานบริหารความเสี่ยง',
          detail: 'detail text',
          attachments: {
            file: [],
            fileDelete: [],
          },
          edit_status: true,
          edit_comment_status: false,
          status_submit: false,
        },
        answers: {
          id: 2,
          date: ' 02/06/2563  เวลา 13:00:54 น.',
          by: 'พนักงานดูแลหน่วยงานบริหารความเสี่ยง',
          detail: 'detail text',
          attachments: {
            file: [],
            fileDelete: [],
          },
          edit_status: true,
          edit_comment_status: false,
          status_submit: false,
        },
      },
    ],
  };

  dataCommentNew = {
    id: null,
    question: {
      id: null,
      date: ' 02/06/2563  เวลา 13:00:54 น.',
      by: 'พนักงานดูแลหน่วยงานบริหารความเสี่ยง',
      detail: 'detail text',
      attachments: {
        file: [],
        fileDelete: [],
      },
      edit_status: true,
      edit_comment_status: false,
      status_submit: false,
    },
    answers: {
      id: null,
      date: ' 02/06/2563  เวลา 13:00:54 น.',
      by: 'พนักงานดูแลหน่วยงานบริหารความเสี่ยง',
      detail: 'detail text',
      attachments: {
        file: [],
        fileDelete: [],
      },
      edit_status: true,
      edit_comment_status: false,
      status_submit: false,
    },
  };

  // role
  //  - ผู้ดูแลสายงาน https://projects.invisionapp.com/d/main/#/console/19297795/411777491/preview
  //  - พนักงานหน่วยงานเจ้าของผลิตภัณฑ์ https://projects.invisionapp.com/d/main/#/console/19297795/411774129/preview

  userRole = 'พนักงานหน่วยงานเจ้าของผลิตภัณฑ์';
  rowBy: string;
  validate = null; // Status การเเก้ไข
  roleService: any; // get ข้อมูลจาก role api
  rolename: string;
  Getname: any;
  // question
  // commentQuestion = {
  //   roleService: null,
  //   validate: false,
  //   question: []
  // };
  commentQuestion: any;
  question: any;
  alertMessage: any = '';
  saveDraftstatus = null;
  sendWorkStatus = false;

  staticTitleIndex_accounting = []; // !important ex [0,1,2] mean set fix row index 0 1 2
  staticTitleIndex_performance = []; // !important ex [0,1,2] mean set fix row index 0 1 2
  staticTitleIndex_dataGovernance = []; // !important ex [0,1,2] mean set fix row index 0 1 2

  // download
  displayedColumns_download: string[] = ['name', 'size', 'download'];
  dataCommentQ_download: MatTableDataSource<FileList>;
  dataCommentA_download: MatTableDataSource<FileList>;
  dataCommentNew_downloa: MatTableDataSource<FileList>;
  table_download = [];

  validateBu = 0;

  // New comment
  fileNew = null;
  templateCommentsNew = {
    date: '',
    by: '',
    detail: '',
    attachments: {
      file: [],
      fileDelete: [],
    },
  };
  btn_sand = true;
  // dialog
  deleteQuestionStatus = false;
  deleteAnswersStatus = false;

  status_loading = false;

  @ViewChild(CommentManagementComponent)
  commentManagement: CommentManagementComponent;

  // testfile = '
  testfile = {};

  getlevel: any;
  DataSendFinance: any;

  statusFinance1: boolean;
  statusFinance2: boolean;
  statusFinance3: boolean;
  arryFinance: any = [];
  saveDraftstatus_State = false;

  checkStaticTitle(itable, index) {
    if (itable === 0) {
      return this.staticTitleIndex_accounting.includes(index);
    } else if (itable === 1) {
      return this.staticTitleIndex_performance.includes(index);
    } else if (itable === 2) {
      return this.staticTitleIndex_dataGovernance.includes(index);
    }
  }

  addRow_accounting(itable) {
    this.data.template[itable].tableBody.push({
      id: null,
      topic: null,
      requestId: null,
      inspection: null,
      comment: null,
      reviews: null,
      poComment: null,
      pcComment: null,
      pcExtraComment: null,
      pcIssue: null,
    });
    this.changeSaveDraft();
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate');
    // return confirm();
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    console.log('000 sidebarService status:', status);
    if (this.saveDraftstatus_State === true) {
      // console.log('000 sidebarService status:', status);
      const dialogRef = this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        console.log('data::', data);
        if (data.returnStatus === true) {
          subject.next(true);
          this.save();
          this.saveDraftstatus_State = false;
        } else if (data.returnStatus === false) {
          console.log('000000 data::', data.returnStatus);
          this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
        }
      });
      console.log('8889999', subject);
      return subject;
    } else {
      return true;
    }
  }
  removeDataItem(itable, index) {
    // console.log('removeDataItem itable', itable);
    // console.log('removeDataItem index', index);
    if (itable === 0) {
      if (this.data.template[itable].tableBody[index].id !== null) {
        this.data.deleteFinanceBudget.push(
          this.data.template[itable].tableBody[index].id
        );
      }
    } else if (itable === 1) {
      if (this.data.template[itable].tableBody[index].id !== null) {
        this.data.deleteFinancePerformance.push(
          this.data.template[itable].tableBody[index].id
        );
      }
    } else if (itable === 2) {
      if (this.data.template[itable].tableBody[index].id !== null) {
        this.data.deleteFinanceGovernance.push(
          this.data.template[itable].tableBody[index].id
        );
      }
    }
    this.data.template[itable].tableBody.splice(index, 1);
    this.changeSaveDraft();
  }

  deleteQuestion(index, id) {
    this.deleteQuestionStatus = false;
    const dialogRef = this.dialog.open(DialogFinanceComponent, {
      disableClose: true,
      data: {
        headerDetail: 'ยืนยันการลบคำถาม',
        bodyDetail: 'คุณแน่ใจหรือไม่? ว่าต้องการลบคำถาม',
        deleteQuestionStatus: false,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.deleteQuestionStatus = result.deleteStatus;
      if (this.deleteQuestionStatus) {
        this.data.comments.splice(index, 1);
      }
    });
  }

  deleteAnswers(index, id) {
    this.deleteAnswersStatus = false;
    const dialogRef = this.dialog.open(DialogFinanceComponent, {
      disableClose: true,
      data: {
        headerDetail: 'ยืนยันการลบตอบ',
        bodyDetail: 'คุณแน่ใจหรือไม่? ว่าต้องการลบคำตอบ',
        deleteAnswersStatus: false,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.deleteAnswersStatus = result.deleteStatus;
      if (this.deleteAnswersStatus) {
        this.data.comments[index].answers = {
          id: null,
          date: '',
          by: '',
          detail: '',
          attachments: {
            file: [],
            fileDelete: [],
          },
          edit_status: true,
          edit_comment_status: false,
          status_submit: false,
        };
      }
    });
  }

  backToDashboardPage() {
    // localStorage.setItem('requestId', '');
    this.router.navigate(['/dashboard1']);
  }

  save() {
    this.saveDraftstatus_State = false;
    this.financeService.status_state = false;
    // console.log('data', this.data);
    this.DataSendFinance = {
      requestId: localStorage.getItem('requestId'),
      status: this.sendWorkStatus,
      deleteFinanceBudget: this.data.deleteFinanceBudget,
      deleteFinancePerformance: this.data.deleteFinancePerformance,
      deleteFinanceGovernance: this.data.deleteFinanceGovernance,
      financeBudget: this.data.template[0].tableBody,
      financePerformance: this.data.template[1].tableBody,
      financeGovernance: this.data.template[2].tableBody,
    };
    this.commentManagement.saveComment();
    // console.log('send_data', send_data);
    // console.log(this.sendWorkStatus);
    this.status_loading = true;
    this.financeService.updateFinance(this.DataSendFinance, this.roleService).subscribe((res) => {
      console.log('res', res);
      if (res['status'] === 'success') {
        this.DataSendFinance = null;
        this.data.template[0].tableBody = null;
        this.data.template[1].tableBody = null;
        this.data.template[2].tableBody = null;
        this.getStart(localStorage.getItem('requestId'));
        this.status_loading = false;
        this.saveDraftstatus = 'success';
        if (this.roleService === 'BU') {
          this.alertMessage = 'บันทึกความเห็นของสายงานบริหารการเงินเรียบร้อย';
        } else if (this.roleService === 'PO') {
          this.alertMessage = 'บันทึกความเห็นของ' + this.Getname + 'เรียบร้อย';
        }
        setTimeout(() => {
          this.saveDraftstatus = 'fail';
        }, 3000);
      } else {

        Swal.fire({
          title: 'error',
          text: res['message'],
          icon: 'error',
          // showConfirmButton: false,
          // timer: 3000,
        });
        this.status_loading = false;
      }
    }, err => {
      this.status_loading = false;
      console.log(err);
    });

  }

  validateCommentBu() {
    // console.log('this.validateBu', this.data);
    this.data.template.forEach((process, i) => {
      process.tableBody.forEach((subRisk) => {
        if (subRisk.comment === '' || subRisk.comment === null) {
          this.validateBu = this.validateBu + 1;
        }
        if (subRisk.inspection === '' || subRisk.inspection === null) {
          this.validateBu = this.validateBu + 1;
        }
        if (subRisk.reviews === '' || subRisk.reviews === null) {
          this.validateBu = this.validateBu + 1;
        }
      });
    });

    console.log(this.validateBu);

    return this.validateBu;
  }



  send() {
    this.saveDraftstatus_State = false;
    // this.financeService.status_state = false;
    this.validateCommentBu();
    this.statusFinance1 = true;
    this.statusFinance2 = true;
    this.statusFinance3 = true;
    this.arryFinance = []
    this.DataSendFinance = {
      requestId: localStorage.getItem('requestId'),
      status: this.sendWorkStatus,
      deleteFinanceBudget: this.data.deleteFinanceBudget,
      deleteFinancePerformance: this.data.deleteFinancePerformance,
      deleteFinanceGovernance: this.data.deleteFinanceGovernance,
      financeBudget: this.data.template[0].tableBody,
      financePerformance: this.data.template[1].tableBody,
      financeGovernance: this.data.template[2].tableBody,
    };
    console.log('sand', this.DataSendFinance);
    for (let i = 0; i < this.data.template[0].tableBody.length; i++) {
      if (this.data.template[0].tableBody[i].topic == '' ||
        this.data.template[0].tableBody[i].topic == null ||
        this.data.template[0].tableBody[i].comment == '' ||
        this.data.template[0].tableBody[i].comment == null ||
        this.data.template[0].tableBody[i].inspection == '' ||
        this.data.template[0].tableBody[i].inspection == null ||
        this.data.template[0].tableBody[i].reviews == '' ||
        this.data.template[0].tableBody[i].reviews == null) {
        this.statusFinance1 = false;

      }
    }

    for (let i = 0; i < this.data.template[1].tableBody.length; i++) {
      if (this.data.template[1].tableBody[i].topic == '' ||
        this.data.template[1].tableBody[i].topic == null ||
        this.data.template[1].tableBody[i].comment == '' ||
        this.data.template[1].tableBody[i].comment == null ||
        this.data.template[1].tableBody[i].inspection == '' ||
        this.data.template[1].tableBody[i].inspection == null ||
        this.data.template[1].tableBody[i].reviews == '' ||
        this.data.template[1].tableBody[i].reviews == null) {
        this.statusFinance2 = false;

      }
    }

    for (let i = 0; i < this.data.template[2].tableBody.length; i++) {
      if (this.data.template[2].tableBody[i].topic == '' ||
        this.data.template[2].tableBody[i].topic == null ||
        this.data.template[2].tableBody[i].comment == '' ||
        this.data.template[2].tableBody[i].comment == null ||
        this.data.template[2].tableBody[i].inspection == '' ||
        this.data.template[2].tableBody[i].inspection == null ||
        this.data.template[2].tableBody[i].reviews == '' ||
        this.data.template[2].tableBody[i].reviews == null) {
        this.statusFinance3 = false;

      }
    }

    if (this.statusFinance1 == false) {
      this.arryFinance.push('การบัญชี/งบประมาณและเบิกจ่าย')
    }
    if (this.statusFinance2 == false) {
      this.arryFinance.push('การวัดผลงานภายหลังออกผลิตภัณฑ์')
    }
    if (this.statusFinance3 == false) {
      this.arryFinance.push('Data Governance(DG)')
    }

    console.log(this.statusFinance1)
    console.log(this.statusFinance2)
    console.log(this.statusFinance3)
    console.log(this.arryFinance)
    if (this.rolename !== 'PO' && this.rolename !== 'PC' && this.rolename === 'Finance') {

      // if(this.getlevel =='ฝ่าย' ||this.getlevel =='กลุ่ม' ||this.getlevel =='สาย'
      // ) {
      //   this.router.navigate(['/comment']);
      // }

      if (this.validateBu > 0 || this.arryFinance.length !== 0) {
        alert('กรุณากรอกข้อมูลตาราง ' + this.arryFinance + ' ให้ครบถ้วน');
        this.validateBu = 0;
      } else {
        console.log('saved')
        if (this.statusFinance1 == true && this.statusFinance2 == true && this.statusFinance3 == true) {
          this.financeService.updateFinance(this.DataSendFinance, this.roleService).subscribe((res) => {
            console.log('res', res);
            if (res['status'] == 'success' || (this.getlevel == 'ฝ่าย' || this.getlevel == 'กลุ่ม' || this.getlevel == 'สาย')) {
              this.financeService
                .sendDocument_BU(localStorage.getItem('requestId'))
                .subscribe((ress) => {
                  if (ress['status'] === 'success') {
                    Swal.fire({
                      title: 'success',
                      text: 'ส่งงานเรียบร้อย',
                      icon: 'success',
                      showConfirmButton: false,
                      timer: 3000,
                    }).then(() => {
                      localStorage.setItem('requestId', '');
                      this.router.navigate(['/dashboard1']);
                    })
                  } else if (ress['status'] === 'fail' &&
                    ress['message'] === 'กรุณาไปให้ความเห็นหน้า ความเห็น / ส่งงาน') {
                    this.router.navigate(['/comment']);
                  } else {
                    Swal.fire({
                      title: 'error',
                      text: ress['message'],
                      icon: 'error',
                      // showConfirmButton: false,
                      // timer: 1500,
                    }).then(() => {
                      this.POcommentConsiderAlert(); // this should execute now
                    });
                  }
                }, err => {
                  this.status_loading = false;
                  console.log(err);
                });
            } else {
              Swal.fire({
                title: 'error',
                text: res['message'],
                icon: 'error',
                showConfirmButton: false,
                timer: 3000,
              });
            }
          }, err => {
            this.status_loading = false;
            console.log(err);
          });
        }
      }
    } else if (this.rolename === 'PO') {
      // console.log(this.checkDetail_PO())
      const arry = this.checkDetail_PO()

      if (arry.length > 0) {
        alert('กรุณาให้ความเห็นตาราง ' + arry + ' ให้ครบ')
      }
      else if (arry.length == 0) {
        this.DataSendFinance = {
          requestId: localStorage.getItem('requestId'),
          status: this.sendWorkStatus,
          deleteFinanceBudget: this.data.deleteFinanceBudget,
          deleteFinancePerformance: this.data.deleteFinancePerformance,
          deleteFinanceGovernance: this.data.deleteFinanceGovernance,
          financeBudget: this.data.template[0].tableBody,
          financePerformance: this.data.template[1].tableBody,
          financeGovernance: this.data.template[2].tableBody,
        };
        this.financeService.updateFinance(this.DataSendFinance, this.roleService).subscribe((res) => {
          console.log('res', res);
          if (res['status'] === 'success' || (this.getlevel == 'ฝ่าย' || this.getlevel == 'กลุ่ม' || this.getlevel == 'สาย')) {
            this.DataSendFinance = null;
            this.data.template[0].tableBody = null;
            this.data.template[1].tableBody = null;
            this.data.template[2].tableBody = null;
            this.getStart(localStorage.getItem('requestId'));
            this.financeService.sendDocument_PO(localStorage.getItem('requestId'))
              .subscribe((ress) => {
                if (ress['status'] === 'success') {
                  Swal.fire({
                    title: 'success',
                    text: 'ส่งงานเรียบร้อย',
                    icon: 'success',
                    showConfirmButton: false,
                    timer: 3000,
                  }).then(() => {
                    localStorage.setItem('requestId', '');
                    this.router.navigate(['/dashboard1']);
                  })

                } else {
                  if (ress['message'] === 'กรุณาไปให้ความเห็นหน้า ความเห็น / ส่งงาน') {
                    this.router.navigate(['/comment']);
                  } else {
                    Swal.fire({
                      title: 'error',
                      text: ress['message'],
                      icon: 'error',
                      showConfirmButton: false,
                      timer: 3000,
                    });
                  }
                }
              }, err => {
                this.status_loading = false;
                console.log(err);
              });
          } else {
            Swal.fire({
              title: 'error',
              text: res['message'],
              icon: 'error',
              showConfirmButton: false,
              timer: 3000,
            });
          }
        }, err => {
          this.status_loading = false;
          console.log(err);
        });
      }

    } else {
      Swal.fire({
        title: 'error',
        text: 'ไม่สารมรถส่งงานด้',
        icon: 'error',
        showConfirmButton: false,
        timer: 3000,
      });
    }
    // } else {
    //   alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
    // }
    // });
  }
  POcommentConsiderAlert() {
    this.financeService.linkToAny();
  }
  checkDetail_BU() {
    let checkDtail = true;
    this.data.template.forEach((ele) => {
      ele.tableBody.forEach((eles) => {
        if (eles.topic === null || eles.topic === '') {
          checkDtail = false;
        } else if (eles.inspection === null || eles.inspection === '') {
          checkDtail = false;
        } else if (eles.comment === null || eles.comment === '') {
          checkDtail = false;
        } else if (eles.reviews === null || eles.reviews === '') {
          checkDtail = false;
        }
      });
    });
    this.sendWorkStatus = checkDtail;
    // console.log('this.sendWorkStatus 777', this.sendWorkStatus);
  }

  checkDetail_PO() {
    let checkDtail = true;
    const arr = []
    this.data.template.forEach((ele, index) => {
      ele.tableBody.forEach((eles) => {
        if (eles.poComment === null || eles.poComment === '') {
          checkDtail = false;
          if (index == 0) {
            arr.push('การบัญชี/งบประมาณและเบิกจ่าย')
          }
          if (index == 1) {
            arr.push('การวัดผลงานภายหลังออกผลิตภัณฑ์')
          }
          if (index == 2) {
            arr.push('Data Governance (DG)')
          }

        }
      });

    });
    this.sendWorkStatus = checkDtail;
    console.log('this.sendWorkStatus 777', this.sendWorkStatus);
    return arr;
  }

  changeSaveDraft() {
    this.saveDraftstatus_State = true;
    this.financeService.status_state = true;
    console.log("saveDraftstatus_State:", this.saveDraftstatus_State)
    if (this.rolename === 'Finance') {
      this.checkDetail_BU();
    } else if (this.rolename === 'PO') {
      this.checkDetail_PO();
    }
  }

  getStart(requestId: any) {
    console.log('2222', this.data);
    console.log('getStart', requestId);
    // set table
    this.financeService
      .getFinance(requestId, this.roleService)
      .subscribe((res) => {
        console.log('res', res);
        const dataRes = res['data'];
        this.data.template[0].tableBody = dataRes.financeBudget;
        this.data.template[1].tableBody = dataRes.financePerformance;
        this.data.template[2].tableBody = dataRes.financeGovernance;
        this.validate = dataRes.validate;
        if (this.rolename === 'Finance') {
          this.checkDetail_BU();
        } else if (this.rolename === 'PO') {
          this.checkDetail_PO();
        }
      }, err => {
        this.status_loading = false;
        console.log(err);
      });
  }

  setfile() {
    this.table_download = [];
    this.data.comments.forEach((ele, index) => {
      // console.log('getStart', index);
      // console.log('getStart002', ele.question.attachments.file);

      if (ele.question.attachments.file) {
        this.dataCommentQ_download = new MatTableDataSource(
          ele.question.attachments.file
        );
      } else {
        this.dataCommentQ_download = new MatTableDataSource();
      }

      if (ele.answers.attachments.file) {
        this.dataCommentA_download = new MatTableDataSource(
          ele.answers.attachments.file
        );
      } else {
        this.dataCommentA_download = new MatTableDataSource();
      }

      this.table_download.push({
        dataCommentQ: this.dataCommentQ_download,
        dataCommentA: this.dataCommentA_download,
      });
    });
  }

  sendCommentNew() {
    console.log('sendCommentNew');
    const templateComments = {
      id: null,
      question: {
        id: null,
        date: '',
        by: this.rowBy,
        detail: this.templateCommentsNew.detail,
        attachments: {
          file: this.templateCommentsNew.attachments.file,
          fileDelete: [],
        },
        edit_status: true,
        edit_comment_status: false,
        status_submit: false,
      },
      answers: {
        id: null,
        date: '',
        by: '',
        detail: '',
        attachments: {
          file: [],
          fileDelete: [],
        },
        edit_status: true,
        edit_comment_status: false,
        status_submit: false,
      },
    };
    this.data.comments.push(templateComments);
    this.setfile();
    console.log('sendCommentNew', this.data.comments);
    this.templateCommentsNew = {
      date: '',
      by: '',
      detail: '',
      attachments: {
        file: [],
        fileDelete: [],
      },
    };
    this.dataCommentNew_downloa = new MatTableDataSource();
    this.changeSaveDraft();
  }

  // File
  onSelectFile(event, tableName, index?) {
    console.log('event', event);
    console.log('tableName', tableName);
    console.log('index', index);
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      const file = event.target.files;
      for (let i = 0; i < filesAmount; i++) {
        console.log('type:', file[i]);
        if (
          file[i].type === 'application/doc' ||
          file[i].type === 'application/pdf' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
          file[i].type === 'application/pptx' ||
          file[i].type === 'application/docx' ||
          (file[i].type === 'application/ppt' && file[i].size)
        ) {
          if (file[i].size <= 5000000) {
            this.multiple_file(file[i], tableName, index);
          }
        }
      }
    }
  }

  multiple_file(file, tableName, index?) {
    console.log('download', file);
    const reader = new FileReader();
    if (file) {
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');

        const templateFile = {
          fileName: file.name,
          fileSize: file.size,
          base64File: splitFile[1],
        };
        console.log('aaaa', templateFile);
        this.testfile = templateFile;
        if (tableName === 'new') {
          this.templateCommentsNew.attachments.file.push(templateFile);
          this.dataCommentNew_downloa = new MatTableDataSource(
            this.templateCommentsNew.attachments.file
          );
          this.fileNew = null;
        } else if (tableName === 'question') {
          this.data.comments[index].question.attachments.file.push(
            templateFile
          );
          this.table_download[index].dataCommentQ = new MatTableDataSource(
            this.data.comments[index].question.attachments.file
          );
          this.fileNew = null;
        } else if (tableName === 'answers') {
          this.data.comments[index].answers.attachments.file.push(templateFile);
          this.table_download[index].dataCommentA = new MatTableDataSource(
            this.data.comments[index].answers.attachments.file
          );
          this.fileNew = null;
        }
      };
    }
    this.changeSaveDraft();
  }

  downloadFile(base64FileData: any, fileNameData: any) {
    console.log('testfile', this.testfile);
    const contentType = '';
    const b64Data = base64FileData;
    const filename = fileNameData;
    console.log('b64Data', b64Data);
    console.log('filename', filename);
    const config = this.b64toBlob(b64Data, contentType);
    console.log('config', config);
    saveAs(config, filename);
  }

  b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  check_edit_comment_status() {
    let checkComment = true;
    this.data.comments.forEach((ele) => {
      if (ele.question.edit_comment_status === true) {
        checkComment = false;
      }
      if (ele.answers.edit_comment_status === true) {
        checkComment = false;
      }
    });
    this.btn_sand = checkComment;
    this.changeSaveDraft();
    console.log('checkComment', checkComment);
  }

  EvaluationDrop(event: CdkDragDrop<string[]>, index) {
    moveItemInArray(
      this.data.template[index].tableBody,
      event.previousIndex,
      event.currentIndex
    );
    this.changeSaveDraft();
  }

  constructor(
    public dialog: MatDialog,
    private financeService: FinanceService,
    private router: Router,
    private sidebarService: RequestService
  ) { }

  ngOnInit() {
    if (localStorage) {
      if (localStorage.getItem('requestId')) {
        this.rowBy = localStorage.getItem('role');
        this.rolename = localStorage.getItem('role');
        this.getlevel = localStorage.getItem('level');
        this.Getname = localStorage.getItem('ouName');
        console.log('rolename 1111', this.rolename);
        if (this.rolename === 'Finance' && this.getlevel === 'พนักงาน') {
          this.roleService = 'BU';
          this.userRole = 'ผู้ดูแลสายงาน';
        } else if (this.rolename === 'PO') {
          this.roleService = 'PO';
          this.userRole = 'พนักงานหน่วยงานเจ้าของผลิตภัณฑ์';
        } else {
          this.roleService = 'PO';
          this.userRole = 'พนักงานหน่วยงานเจ้าของผลิตภัณฑ์';
        }
        this.commentQuestion = {
          requestId: localStorage.getItem('requestId'),
          rolename: this.rolename,
          roleService: this.roleService,
          userRole: this.userRole,
        };
        this.getStart(localStorage.getItem('requestId'));
      }
    } else {
      console.log('Brownser not support');
    }
    console.log('this.roleService', this.roleService);
  }
  autoGrowTextZone(e) {
    e.target.style.height = '0px';
    e.target.style.overflow = 'hidden';
    e.target.style.height = (e.target.scrollHeight + 0) + 'px';
  }
}
