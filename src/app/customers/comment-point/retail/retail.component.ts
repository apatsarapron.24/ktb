import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { RetailService } from '../../../services/comment-point/retail/retail.service';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
  CdkDragExit,
  CdkDragEnter,
  CdkDragStart,
  CdkDragEnd,
} from '@angular/cdk/drag-drop';
import { SubmitService } from '../../../services/comment-point/submit/submit.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { DialogStatusService } from '../../../services/comment-point/dialog-status/dialog-status.service';
import { DialogSaveStatusComponent } from '../../comment-point/dialog-save-status/dialog-save-status.component';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { promise } from 'protractor';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RequestService } from '../../../services/request/request.service';
@Component({
  selector: 'app-retail',
  templateUrl: './retail.component.html',
  styleUrls: ['./retail.component.scss'],
})
export class RetailComponent implements OnInit {

  constructor(
    private retailService: RetailService,
    private Submit: SubmitService,
    private router: Router,
    public dialog: MatDialog,
    private sidebarService: RequestService
  ) { }
  retailData: any = {
    requestId: null,
    validate: false,
    listRetailProcessConsiders: [],
    listRetailProcessConsidersExtra: [],
    listRetailOperationDetail: [],
    listRetailOperationDetailExtra: [],
    requestProcessConsidersDelete: [],
    requestProcessConsidersSubDelete: [],
    requestProcessConsidersSubRiskDelete: [],
  };

  // covert date not important...
  convertDate: any = [];
  convertDatePO: any = [];
  // dropIndex: number;
  saveDraftstatus = null;

  // new table
  dropIndex: number;
  add_mainProcess: null;
  staticTitleIndex = []; // !important ex [0,1,2] mean set fix row index 0 1 2

  // Risk sub_table
  Risk_dropIndex = 0;
  staticTitleIndexRisk = [];
  header_table = ['กระบวนการย่อย', 'In/Out', 'IT Related'];
  // status page
  retailDataStatus = true;
  IN_OUT: any = ['No', 'Yes', ''];
  requestId: any;
  ResData: any;

  POData: any = {
    validate: false,
    listRetailProcessConsiders: [],
    listRetailProcessConsidersExtra: [],
    listRetailOperationDetail: [],
    listRetailOperationDetailExtra: [],
  };
  // row user
  // - ผู้ดูแลสายงาน https://projects.invisionapp.com/d/#/console/19297795/411661572/inspect
  // - พนักงานหน่วยงานเจ้าของผลิตภัณฑ์  https://projects.invisionapp.com/d/#/console/19297795/411661572/preview
  // - เลขานุการบอร์ด https://projects.invisionapp.com/d/#/console/19297795/411661571/preview
  userRow = 'เลขานุการบอร์ด';
  rowBy: string;
  validate = null; // Status การเเก้ไข

  CommentPO: any = {
    requestId: null,
    status: false,
    poCommentConsider: [],
    poCommentOperation: [],
    poCommentRetail1: [],
    poCommentRetail2: [],
  };

  Comment_PC: any = {
    requestId: null,
    status: false,
    listRetailProcessConsiders: [],
    listRetailProcessConsidersExtra: [],
    listRetailOperationDetail: [],
    listRetailOperationDetailExtra: [],
  };
  arryPC: any = [];
  BU_statusUpdate = true;

  validateBu: any = false;
  validateProcessConsiders = 0;

  lengthOperationPC: any;
  lengthOperation: any;

  level: any;
  Getname: any;
  alertMessage: any = '';
  status_loading = false;

  statuslistRetailProcessConsiders: boolean;
  statuslistRetailOperationDetail: boolean;
  statuslistRetailProcessConsidersExtra: boolean;
  statuslistRetailOperationDetailExtra: boolean;
  arrayStatus: any = [];

  saveDraftstatus_State = false;

  timeformat(time) {
    const date = moment(time).format('l');
    return date;
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate');
    // return confirm();
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    console.log('000 sidebarService status:', status);
    if (this.saveDraftstatus_State === true) {
      // console.log('000 sidebarService status:', status);
      const dialogRef = this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        console.log('data::', data);
        if (data.returnStatus === true) {
          subject.next(true);
          this.save();
          this.saveDraftstatus_State = false;
        } else if (data.returnStatus === false) {
          console.log('000000 data::', data.returnStatus);
          this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
        }
      });
      console.log('8889999', subject);
      return subject;
    } else {
      return true;
    }
  }
  save() {
    // this.saveDraftstatus = 'success';
    console.log('save');
    this.saveDraftstatus_State = false;
    this.retailService.status_state = false;
    if (this.rowBy === 'PO') {
      this.CommentPO.requestId = Number(this.requestId);
      for (
        let index = 0;
        index < this.POData.listRetailProcessConsiders.length;
        index++
      ) {
        const Main = this.POData.listRetailProcessConsiders[index].subProcess;

        // tslint:disable-next-line: no-shadowed-variable
        for (let i = 0; i < Main.length; i++) {
          const SUB = Main[i].subProcessRisk;
          if (SUB.length !== 0) {
            for (let k = 0; k < SUB.length; k++) {
              if (SUB[k].buCommentId !== null) {
                this.CommentPO.poCommentConsider.push({
                  buCommentId: SUB[k].buCommentId,
                  poComment: SUB[k].poComment,
                });
              }
            }
          }
        }
      }

      for (
        let index = 0;
        index < this.POData.listRetailOperationDetail.length;
        index++
      ) {
        const commentOperation = this.POData.listRetailOperationDetail[index]
          .commentOperation;
        // console.log('Main:', commentOperation);
        // tslint:disable-next-line: no-shadowed-variable
        for (let i = 0; i < commentOperation.length; i++) {
          if (commentOperation[i].buCommentId !== null) {
            this.CommentPO.poCommentOperation.push({
              buCommentId: commentOperation[i].buCommentId,
              poComment: commentOperation[i].poComment,
            });
          }
        }
      }
      // console.log('listRetailProcessConsidersExtra', this.POData.listRetailProcessConsidersExtra);
      if (this.POData.listRetailProcessConsidersExtra.length !== 0) {
        for (
          let index = 0;
          index < this.POData.listRetailProcessConsidersExtra.length;
          index++
        ) {
          const commentOperation1 = this.POData.listRetailProcessConsidersExtra[
            index
          ];
          // console.log('Main:', commentOperation1);
          // tslint:disable-next-line: no-shadowed-variable
          if (commentOperation1.id !== null) {
            this.CommentPO.poCommentRetail1.push({
              buCommentId: commentOperation1.id,
              poComment: commentOperation1.poComment,
            });
          }
        }
      }

      if (this.POData.listRetailOperationDetailExtra.length !== 0) {
        for (
          let index = 0;
          index < this.POData.listRetailOperationDetailExtra.length;
          index++
        ) {
          const commentOperation2 = this.POData.listRetailOperationDetailExtra[
            index
          ];
          // console.log('Main:', commentOperation1);
          // tslint:disable-next-line: no-shadowed-variable
          if (commentOperation2.id !== null) {
            this.CommentPO.poCommentRetail2.push({
              buCommentId: commentOperation2.id,
              poComment: commentOperation2.poComment,
            });
          }
        }
      }

      // tslint:disable-next-line: max-line-length
      const check_poCommentConsider = Object.values(this.CommentPO.poCommentConsider).every(value => value['poComment'] !== '' && value['poComment'] !== null);
      // tslint:disable-next-line: max-line-length
      const check_poCommentOperation = Object.values(this.CommentPO.poCommentOperation).every(value => value['poComment'] !== '' && value['poComment'] !== null);
      // tslint:disable-next-line: max-line-length
      const check_poCommentRetail1 = Object.values(this.CommentPO.poCommentRetail1).every(value => value['poComment'] !== '' && value['poComment'] !== null);
      // tslint:disable-next-line: max-line-length
      const check_poCommentRetail2 = Object.values(this.CommentPO.poCommentRetail2).every(value => value['poComment'] !== '' && value['poComment'] !== null);
      console.log('CommentPO to save:>>', check_poCommentConsider);
      console.log('CommentPO to save:>>', check_poCommentOperation);
      console.log('CommentPO to save:>>', check_poCommentRetail1);
      console.log('CommentPO to save:>>', check_poCommentRetail2);
      if (check_poCommentConsider === true &&
        check_poCommentOperation === true &&
        check_poCommentRetail1 === true &&
        check_poCommentRetail2 === true
      ) {
        this.CommentPO.status = true;
      }
      // const check_account = Object.values(this.CommentPO.poCommentConsider).every(value => value !==  && value.poComment !== null);
      this.status_loading = true;
      this.retailService.UpdateRetail_PO(this.CommentPO).subscribe((res) => {
        console.log('CommentPO to save:>>', this.CommentPO);
        console.log('res PO:', res);
        console.log('CommentPO:', this.CommentPO.poCommentOperation);

        if (res['status'] === 'success') {
          this.alertMessage = 'บันทึกความเห็นของ' + this.Getname + 'เรียบร้อย';
          this.saveDraftstatus = 'success';
          setTimeout(() => {
            this.saveDraftstatus = 'fail';
          }, 3000);
          this.getStart(this.requestId);
          this.status_loading = false;
          this.CommentPO.status = false;
          this.CommentPO.poCommentConsider = [];
          this.CommentPO.poCommentOperation = [];
          this.CommentPO.poCommentRetail1 = [];
          this.CommentPO.poCommentRetail2 = [];

        } else {
          Swal.fire({
            title: 'error',
            text: res['message'],
            icon: 'error',
          });
          this.status_loading = false;
        }
      }, err => {
        this.status_loading = false;
        console.log(err);
      });
    } else if (this.rowBy === 'PC') {
      this.Comment_PC.requestId = Number(this.requestId);
      for (
        let index = 0;
        index < this.POData.listRetailProcessConsiders.length;
        index++
      ) {
        const Main = this.POData.listRetailProcessConsiders[index].subProcess;

        // tslint:disable-next-line: no-shadowed-variable
        for (let i = 0; i < Main.length; i++) {
          const SUB = Main[i].subProcessRisk;
          if (SUB.length !== 0) {
            for (let k = 0; k < SUB.length; k++) {
              if (SUB[k].buCommentId !== null) {
                this.Comment_PC.listRetailProcessConsiders.push({
                  buCommentId: SUB[k].buCommentId,
                  pcComment: SUB[k].pcComment,
                  pcExtraComment: SUB[k].pcExtraComment,
                  pcIssue: SUB[k].pcIssue,
                });
              }
            }
          }
        }
      }

      for (
        let index = 0;
        index < this.POData.listRetailOperationDetail.length;
        index++
      ) {
        const commentOperation = this.POData.listRetailOperationDetail[index]
          .commentOperation;
        // console.log('Main:', commentOperation);
        // tslint:disable-next-line: no-shadowed-variable
        for (let i = 0; i < commentOperation.length; i++) {
          if (commentOperation[i].buCommentId !== null) {
            this.Comment_PC.listRetailOperationDetail.push({
              buCommentId: commentOperation[i].buCommentId,
              pcComment: commentOperation[i].pcComment,
              pcExtraComment: commentOperation[i].pcExtraComment,
              pcIssue: commentOperation[i].pcIssue,
            });
          }
        }
      }
      // console.log('listRetailProcessConsidersExtra', this.POData.listRetailProcessConsidersExtra);
      if (this.POData.listRetailProcessConsidersExtra.length !== 0) {
        for (
          let index = 0;
          index < this.POData.listRetailProcessConsidersExtra.length;
          index++
        ) {
          const commentOperation1 = this.POData.listRetailProcessConsidersExtra[
            index
          ];
          // console.log('Main:', commentOperation1);
          // tslint:disable-next-line: no-shadowed-variable

          this.Comment_PC.listRetailProcessConsidersExtra.push({
            buCommentId: commentOperation1.id,
            pcComment: commentOperation1.pcComment,
            pcExtraComment: commentOperation1.pcExtraComment,
            pcIssue: commentOperation1.pcIssue,
          });

        }
      }
      // console.log('Main:', this.POData.listRetailOperationDetailExtra);
      if (this.POData.listRetailOperationDetailExtra.length !== 0) {
        for (
          let index = 0;
          index < this.POData.listRetailOperationDetailExtra.length;
          index++
        ) {
          const commentOperation2 = this.POData.listRetailOperationDetailExtra[
            index
          ];

          // tslint:disable-next-line: no-shadowed-variable

          this.Comment_PC.listRetailOperationDetailExtra.push({
            buCommentId: commentOperation2.id,
            pcComment: commentOperation2.pcComment,
            pcExtraComment: commentOperation2.pcExtraComment,
            pcIssue: commentOperation2.pcIssue,
          });

        }
      }
      const check_process_pcIssue = Object.values(this.Comment_PC.listRetailProcessConsiders).every(value => value['pcIssue'] !== null && value['pcIssue'] !== '');
      const check_process_pcExtraComment = Object.values(this.Comment_PC.listRetailProcessConsiders).every(value => value['pcExtraComment'] !== null && value['pcExtraComment'] !== '');
      const check_process_pcComment = Object.values(this.Comment_PC.listRetailProcessConsiders).every(value => value['pcComment'] !== null && value['pcComment'] !== '');

      const check_processEx_pcIssue = Object.values(this.Comment_PC.listRetailProcessConsidersExtra).every(value => value['pcIssue'] !== null && value['pcIssue'] !== '');
      const check_processEx_pcExtraComment = Object.values(this.Comment_PC.listRetailProcessConsidersExtra).every(value => value['pcExtraComment'] !== null && value['pcExtraComment'] !== '');
      const check_processEx_pcComment = Object.values(this.Comment_PC.listRetailProcessConsidersExtra).every(value => value['pcComment'] !== null && value['pcComment'] !== '');

      const check_detail_pcIssue = Object.values(this.Comment_PC.listRetailOperationDetail).every(value => value['pcIssue'] !== null && value['pcIssue'] !== '');
      const check_detail_pcExtraComment = Object.values(this.Comment_PC.listRetailOperationDetail).every(value => value['pcExtraComment'] !== null && value['pcExtraComment'] !== '');
      const check_detail_pcComment = Object.values(this.Comment_PC.listRetailOperationDetail).every(value => value['pcComment'] !== null && value['pcComment'] !== '');

      const check_detailEx_pcIssue = Object.values(this.Comment_PC.listRetailOperationDetailExtra).every(value => value['pcIssue'] !== null && value['pcIssue'] !== '');
      const check_detailEx_pcExtraComment = Object.values(this.Comment_PC.listRetailOperationDetailExtra).every(value => value['pcExtraComment'] !== null && value['pcExtraComment'] !== '');
      const check_detailEx_pcComment = Object.values(this.Comment_PC.listRetailOperationDetailExtra).every(value => value['pcComment'] !== null && value['pcComment'] !== '');

      console.log('Comment_PC:', this.Comment_PC);
      console.log('listRetailProcessConsiders:', this.Comment_PC.listRetailProcessConsiders,
        check_process_pcIssue, check_process_pcExtraComment, check_process_pcComment);
      console.log('listRetailProcessConsidersEx:', this.Comment_PC.listRetailProcessConsidersExtra,
        check_processEx_pcIssue, check_processEx_pcExtraComment, check_processEx_pcComment);
      console.log('istRetailOperationDetail:', this.Comment_PC.listRetailOperationDetail,
        check_detail_pcIssue, check_detail_pcExtraComment, check_detail_pcComment);
      console.log('listRetailOperationDetailExtra:', this.Comment_PC.listRetailOperationDetailExtra,
        check_detailEx_pcIssue, check_detailEx_pcExtraComment, check_detailEx_pcComment);
      if (check_process_pcIssue === true && check_process_pcExtraComment === true && check_process_pcComment === true &&
        check_processEx_pcIssue === true && check_processEx_pcExtraComment === true && check_processEx_pcComment === true &&
        check_detail_pcIssue === true && check_detail_pcExtraComment === true && check_detail_pcComment === true &&
        check_detailEx_pcIssue === true && check_detailEx_pcExtraComment === true && check_detailEx_pcComment == true) {
        this.Comment_PC.status = true;
      }

      console.log('Comment_PC:', this.Comment_PC);
      this.status_loading = true;
      this.retailService.UpdateRetail_PC(this.Comment_PC).subscribe((res) => {
        console.log('res PC:', res);
        // console.log('CommentPO:', this.CommentPO);

        if (res['status'] === 'success') {
          this.alertMessage = 'บันทึกความเห็นของคณะกรรมการผลิตภัณฑ์เรียบร้อย';
          this.saveDraftstatus = 'success';
          setTimeout(() => {
            this.saveDraftstatus = 'fail';
          }, 3000);
          this.Comment_PC.status = false;

          this.Comment_PC.listRetailProcessConsiders = [];
          this.Comment_PC.listRetailProcessConsidersExtra = [];
          this.Comment_PC.listRetailOperationDetail = [];
          this.Comment_PC.listRetailOperationDetailExtra = [];

          this.getStart(this.requestId);
          this.status_loading = false;
        } else {
          Swal.fire({
            title: 'error',
            text: res['message'],
            icon: 'error',
          });
          this.status_loading = false;
        }
      }, err => {
        this.status_loading = false;
        console.log(err);
      });
    } else if (this.rowBy !== 'PO' && this.rowBy !== 'PC') {

      this.retailData.requestId = Number(this.requestId);
      for (
        let index = 0;
        index < this.retailData.listRetailProcessConsiders.length;
        index++
      ) {
        const Group = this.retailData.listRetailProcessConsiders[index]
          .subProcess;

        for (let n = 0; n < Group.length; n++) {
          const subProcess = Group[n].id;
          if (typeof subProcess === 'string') {
            console.log('subProcess', subProcess);
            this.retailData.listRetailProcessConsiders[index].subProcess[n].id =
              '';
          }
        }
      }

      // console.log('processConsiders', this.retailData.processConsiders);
      // console.log('Delete', this.retailData.requestProcessConsidersDelete);
      // console.log('Delete Sub', this.retailData.requestProcessConsidersSubDelete);
      // console.log(
      //   'Delete SubRisk',
      //   this.retailData.requestrequestProcessConsidersSubRiskDelete
      // );

      // console.log('DeleteConsider', this.retailData.detailDeleteConsider);
      // ===============================================================================================//
      for (
        let w = 0;
        w < this.retailData.listRetailProcessConsidersExtra.length;
        w++
      ) {
        if (
          this.retailData.listRetailProcessConsidersExtra[w].id === '' &&
          this.retailData.listRetailProcessConsidersExtra[w].subject === '' &&
          this.retailData.listRetailProcessConsidersExtra[w].management === ''
        ) {
          this.retailData.listRetailProcessConsidersExtra.splice(w, 1);
        }
      }
      for (
        let w = 0;
        w < this.retailData.listRetailProcessConsidersExtra.length;
        w++
      ) {
        if (
          this.retailData.listRetailProcessConsidersExtra[w].id === '' &&
          this.retailData.listRetailProcessConsidersExtra[w].subject === '' &&
          this.retailData.listRetailProcessConsidersExtra[w].management === ''
        ) {
          // console.log('data', w, this.retailData.commentOperation1[w]);
          this.retailData.listRetailProcessConsidersExtra.splice(w, 1);
        }
      }

      // ====== //

      for (
        let w = 0;
        w < this.retailData.listRetailOperationDetailExtra.length;
        w++
      ) {
        if (
          this.retailData.listRetailOperationDetailExtra[w].id === '' &&
          this.retailData.listRetailOperationDetailExtra[w].subject === '' &&
          this.retailData.listRetailOperationDetailExtra[w].management === ''
        ) {
          this.retailData.listRetailOperationDetailExtra.splice(w, 1);
        }
      }
      for (
        let w = 0;
        w < this.retailData.listRetailOperationDetailExtra.length;
        w++
      ) {
        if (
          this.retailData.listRetailOperationDetailExtra[w].id === '' &&
          this.retailData.listRetailOperationDetailExtra[w].subject === '' &&
          this.retailData.listRetailOperationDetailExtra[w].management === ''
        ) {
          // console.log('data', w, this.retailData.commentOperation1[w]);
          this.retailData.listRetailOperationDetailExtra.splice(w, 1);
        }
      }
      // ===============================================================================================//
      for (
        let index = 0;
        index < this.retailData.listRetailProcessConsiders.length;
        index++
      ) {
        if (
          this.retailData.listRetailProcessConsiders[index].id === '' &&
          this.retailData.listRetailProcessConsiders[index].mainProcess === ''
        ) {
          this.retailData.listRetailProcessConsiders.splice(index, 1);
        }
      }
      // ===============================================================================================//
      for (
        let index = 0;
        index < this.retailData.listRetailProcessConsiders.length;
        index++
      ) {
        if (
          this.retailData.listRetailProcessConsiders[index].id === '' &&
          this.retailData.listRetailProcessConsiders[index].mainProcess === ''
        ) {
          this.retailData.listRetailProcessConsiders.splice(index, 1);
          this.BU_statusUpdate = true;
        } else if (
          this.retailData.listRetailProcessConsiders[index].id === '' &&
          this.retailData.listRetailProcessConsiders[index].mainProcess !== ''
        ) {
          const subProcess = this.retailData.listRetailProcessConsiders[index]
            .subProcess;
          for (let r = 0; r < subProcess.length; r++) {
            if (subProcess[r].inOut === '' || subProcess[r].itRelate === '') {
              alert('กรุณาเลือกข้อมูล IN/OUT หรือ IT Related ให้ถูกต้อง');
              this.BU_statusUpdate = false;
              break;
            }
          }

          break;
        } else {
          this.BU_statusUpdate = true;
        }
        // console.log('retailData:', this.retailData);
      }



      // if (this.BU_statusUpdate === true) {
      // this.update();
      delete this.retailData['validate'];
      // console.log('retailData:', this.retailData);
      this.update();
      // }
    }
  }
  update() {
    this.status_loading = true;
    this.retailService.updateRetail(this.retailData).subscribe((res) => {
      console.log('res BU:', res);
      if (res['status'] === 'success') {
        this.alertMessage = 'บันทึกความเห็นของสายงานบริหารคุณภาพสินเชื่อฯเรียบร้อย';
        this.saveDraftstatus = 'success';
        setTimeout(() => {
          this.saveDraftstatus = 'fail';
        }, 3000);
        this.getStart(this.requestId);
        this.status_loading = false;
      } else {
        Swal.fire({
          title: 'error',
          text: res['message'],
          icon: 'error',
        });
        this.status_loading = false;
      }
    }, err => {
      this.status_loading = false;
      console.log(err);
    });
  }
  changeSaveDraft() {
    this.saveDraftstatus_State = true;
    this.retailService.status_state = true;
    console.log('saveDraftstatus:', this.saveDraftstatus_State)

  }
  getStart(requestId) {

    if (this.rowBy === 'Retail') {
      this.retailService.getRetail(requestId).subscribe((res) => {
        console.log('res Retail', res);
        // this.retailData = res['data'];
        this.retailData.listRetailProcessConsiders =
          res['data'].listRetailProcessConsiders;
        this.retailData.listRetailProcessConsidersExtra =
          res['data'].listRetailProcessConsidersExtra;
        this.retailData.listRetailOperationDetail =
          res['data'].listRetailOperationDetail;
        this.retailData.listRetailOperationDetailExtra =
          res['data'].listRetailOperationDetailExtra;
        this.validate = res['data'].validate;

        for (
          let index = 0;
          index < this.retailData.listRetailOperationDetail.length;
          index++
        ) {
          // console.log('dueDate:',   this.retailData.commentOperationProcess[index].commentOperation);
          this.retailData.listRetailOperationDetail[
            index
          ].commentOperation.forEach((item, i) => {
            // console.log('dueDate:',   item.dueDate.substr(0, 10));
            if (item.dueDate !== null) {
              const type = String(item.dueDate);
              const year = Number(type.substr(0, 4)) + 543;
              const month = type.substr(5, 2);
              const day = type.substr(8, 2);
              const date = day + '/' + month + '/' + year;
              this.convertDate.push({ Convert: date });
            } else {
              this.convertDate.push({ Convert: '' });
            }
          });
        }
      }, err => {
        this.status_loading = false;
        console.log(err);
      });
    } else if (this.rowBy === 'PO') {
      this.retailService.getRetail_PO(requestId).subscribe((res) => {
        console.log('res PO', res);
        this.POData = res['data'];
        this.validate = this.POData.validate;

        for (
          let index = 0;
          index < this.POData.listRetailOperationDetail.length;
          index++
        ) {
          // console.log('dueDate:',   this.retailData.commentOperationProcess[index].commentOperation);
          this.POData.listRetailOperationDetail[index].commentOperation.forEach(
            (item, i) => {
              if (item.dueDate !== null) {
                // console.log('dueDate:',   item.dueDate.substr(0, 10));
                const type = String(item.dueDate);
                const year = Number(type.substr(0, 4)) + 543;
                const month = type.substr(5, 2);
                const day = type.substr(8, 2);
                const date = day + '/' + month + '/' + year;
                this.convertDatePO.push({ Convert: date });
              } else {
                this.convertDatePO.push({ Convert: '' });
              }

            }
          );
        }
      }, err => {
        this.status_loading = false;
        console.log(err);
      });
    } else if (this.rowBy === 'PC') {
      this.retailService.getRetail_PC(requestId).subscribe((res) => {
        console.log('res PC', res);
        this.POData = res['data'];
        this.validate = this.POData.validate;

        for (
          let index = 0;
          index < this.POData.listRetailOperationDetail.length;
          index++
        ) {
          // console.log('dueDate:',   this.retailData.commentOperationProcess[index].commentOperation);
          this.POData.listRetailOperationDetail[index].commentOperation.forEach(
            (item, i) => {
              if (item.dueDate !== null) {
                // console.log('dueDate:',   item.dueDate.substr(0, 10));
                const type = String(item.dueDate);
                const year = Number(type.substr(0, 4)) + 543;
                const month = type.substr(5, 2);
                const day = type.substr(8, 2);
                const date = day + '/' + month + '/' + year;
                this.convertDatePO.push({ Convert: date });
              } else {
                this.convertDatePO.push({ Convert: '' });
              }
            }
          );
        }
      }, err => {
        this.status_loading = false;
        console.log(err);
      });
    } else {
      this.retailService.getRetail_PO(requestId).subscribe((res) => {
        console.log('res PO', res);
        this.POData = res['data'];
        this.validate = this.POData.validate;

        for (
          let index = 0;
          index < this.POData.listRetailOperationDetail.length;
          index++
        ) {
          // console.log('dueDate:',   this.retailData.commentOperationProcess[index].commentOperation);
          this.POData.listRetailOperationDetail[index].commentOperation.forEach(
            (item, i) => {
              // console.log('dueDate:',   item.dueDate.substr(0, 10));
              if (item.dueDate !== null) {
                const type = String(item.dueDate);
                const year = Number(type.substr(0, 4)) + 543;
                const month = type.substr(5, 2);
                const day = type.substr(8, 2);
                const date = day + '/' + month + '/' + year;
                this.convertDatePO.push({ Convert: date });
              } else {
                this.convertDatePO.push({ Convert: '' });
              }
            }
          );
        }
      }, err => {
        this.status_loading = false;
        console.log(err);
      });
    }
  }

  note = [
    'No คือ ไม่ใช่ประเด็นคงค้าง',
    'Pending 1 คือ ต้องแก้ไขก่อนประกาศใช้ผลิตภัณฑ์',
    'Pending 2 คือ สามารถใช้ผลิตภัณฑ์ได้ก่อน แล้วค่อยแก้ไขตามหลังได้',
    'Pending 3 คือ ไม่เกี่ยวข้องกับการประกาศใช้ผลิตภัณฑ์',
    'Pending 4 คือ ต้องแก้ไขให้แล้วเสร็จก่อนนำเสนอเรื่องต่อบอร์ด',
  ];

  ngOnInit() {

    if (localStorage) {
      if (localStorage.getItem('requestId')) {
        this.level = localStorage.getItem('level');
        this.rowBy = localStorage.getItem('role');
        this.requestId = localStorage.getItem('requestId');
        this.Getname = localStorage.getItem('ouName');
        this.getStart(localStorage.getItem('requestId'));

        // this.save();
        // this.saveDraftstatus = null;

      }
    } else {
      console.log('Brownser not support');
    }
  }
  // new table ================================================================================
  checkStaticTitle(index) {
    return this.staticTitleIndex.includes(index);
  }

  drop(event: CdkDragDrop<string[]>) {
    const fixedBody = this.staticTitleIndex;
    console.log('event Group:', event);
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (event.previousContainer.data.length > 1) {
      // check if only one left child
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (fixedBody.includes(this.dropIndex)) {
      // check only 1 child left and static type move only not delete
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      const templatesubTitleRisk = {
        id: this.makeid(5),
        subTitle: '',
        inOut: '',
        itRelate: '',
        subProcessRisk: [
          {
            id: '',
            risk: '',
            riskControl: '',
            riskControlOther: '',
            remark: '',
            subject: '',
            management: '',
          },
        ],
      };

      this.retailData.listRetailProcessConsiders[
        this.dropIndex
      ].subProcess.push(templatesubTitleRisk);
    } else {
      // check only 1 child left and not a static type move and delete
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      // remove current row
      this.removeAddItem(this.dropIndex);
      this.dropIndex = null;
    }
    this.changeSaveDraft();
  }

  dragExited(event: CdkDragExit<string[]>) {
    console.log('Exited', event);
  }

  entered(event: CdkDragEnter<string[]>) {
    console.log('Entered', event);
  }

  startDrag(event: CdkDragStart<string[]>, istep) {
    // console.log('startDrag', event);
    // console.log('startDrag index', istep);
    this.dropIndex = istep;
  }

  removeAddItem(index) {
    this.changeSaveDraft();
    if (index != null) {
      this.retailData.requestProcessConsidersDelete.push(
        this.retailData.listRetailProcessConsiders[index].id
      );
      this.retailData.listRetailProcessConsiders.splice(index, 1);
    }
  }

  // ==================================================================
  dropItem(event: CdkDragDrop<string[]>) {
    this.changeSaveDraft();
    console.log('event item :', event);
    console.log('previousContainer :', event.container.data.length);
    console.log('index group', this.dropIndex);
    console.log('index item:', this.Risk_dropIndex);
    const fixedBody = this.staticTitleIndexRisk;

    // if (
    //   event.previousContainer.data.length === 1 &&
    //   event.container.data.length !== 1
    // ) {
    //   console.log('index:', this.dropIndex);
    //   this.removeAddItem(this.dropIndex);
    //   this.dropIndex = null;
    // }
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (event.previousContainer.data.length > 1) {
      // check if only one left child
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      // check only 1 child left and not a static type move and delete
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      // remove current row
      this.removeAddItemRisk(this.dropIndex, this.Risk_dropIndex);
      this.dropIndex = null;
      this.Risk_dropIndex = null;
    }
    this.changeSaveDraft();
  }

  connectItem(): any[] {
    const mapping = [];
    for (
      let index = 0;
      index < this.retailData.listRetailProcessConsiders.length;
      index++
    ) {
      const Group = this.retailData.listRetailProcessConsiders[index]
        .subProcess;

      // tslint:disable-next-line: no-shadowed-variable
      // this.retailData.operations[index].subProcess.forEach(
      //   ( item, i) =>
      //    mapping.push(`${this.retailData.operations[index].subProcess[i].id}`)
      // );

      for (let j = 0; j < Group.length; j++) {
        mapping.push(String(Group[j].id));
      }
    }

    // console.log('map', mapping);
    return mapping;
  }

  removeAddItemRisk(subIndex, Riskkindex) {
    this.changeSaveDraft();
    if (
      this.retailData.listRetailProcessConsiders[subIndex].subProcess.length ===
      1
    ) {
      this.retailData.requestProcessConsidersDelete.push(
        this.retailData.listRetailProcessConsiders[subIndex].id
      );

      this.retailData.listRetailProcessConsiders.splice(subIndex, 1);
    } else if (
      this.retailData.listRetailProcessConsiders[subIndex].subProcess.length >
      1 &&
      Riskkindex != null
    ) {
      this.retailData.requestProcessConsidersSubRiskDelete.push(
        this.retailData.listRetailProcessConsiders[subIndex].subProcess[
          Riskkindex
        ].id
      );
      this.retailData.listRetailProcessConsiders[subIndex].subProcess.splice(
        Riskkindex,
        1
      );
    }
  }

  startDrag_Risk(event: CdkDragStart<string[]>, isup, istep) {
    this.changeSaveDraft();
    console.log('Risks tartDrag', event);
    console.log('Risk startDrag index', isup);
    this.Risk_dropIndex = isup;
    this.dropIndex = istep;
  }
  enteredRisk(event: CdkDragEnter<string[]>) {
    console.log('Entered Risk', event);
  }
  dragExitedRisk(event: CdkDragExit<string[]>) {
    console.log('Exited Risk', event);
  }

  dragEnded(event: CdkDragEnd) {
    console.log('dragEnded Event > item', event.source.data);
  }

  checkStaticTitle_Risk(index) {
    return this.staticTitleIndexRisk.includes(index);
  }

  removeDataItem(stepindex, subindex, riskIndex) {
    this.changeSaveDraft();
    console.log('stepindex', stepindex);
    console.log('subindex', subindex);
    console.log('riskIndex', riskIndex);
    const fixedBody = [];
    // tslint:disable-next-line: max-line-length
    if (
      fixedBody.includes(stepindex) &&
      this.retailData.listRetailProcessConsiders[stepindex].subProcess[subindex]
        .subProcessRisk.length > 1 &&
      this.Risk_dropIndex !== 0
    ) {
      console.log('delete fixed row only have many risk[]');
      // this.retailData.operations[stepindex].detailDelete.push(this.retailData.operations[stepindex].subProcess[subindex]['id']);
      // console.log(
      //   this.retailData.processConsiders[stepindex].subProcess[subindex]
      //     .subProcessRisk[riskIndex].id
      // );
      if (
        this.retailData.listRetailProcessConsiders[stepindex].subProcess[
          subindex
        ].subProcessRisk[riskIndex].id !== ''
      ) {
        this.retailData.requestProcessConsidersSubRiskDelete.push(
          this.retailData.listRetailProcessConsiders[stepindex].subProcess[
            subindex
          ].subProcessRisk[riskIndex].id
        );
      }

      this.retailData.listRetailProcessConsiders[stepindex].subProcess[
        subindex
      ].subProcessRisk.splice(riskIndex, 1);
    } else if (
      fixedBody.includes(stepindex) &&
      this.retailData.listRetailProcessConsiders[stepindex].subProcess[subindex]
        .subProcessRisk.length === 1
    ) {
      console.log('3 clear fixed row only');
      if (subindex !== 0) {
        if (
          this.retailData.listRetailProcessConsiders[stepindex].subProcess[
            subindex
          ].id !== ''
        ) {
          this.retailData.requestProcessConsidersSubDelete.push(
            this.retailData.processConsiders[stepindex].subProcess[subindex].id
          );
        }
        this.retailData.processConsiders[stepindex].subProcess.splice(
          subindex,
          1
        );
      } else {
        this.retailData.requestProcessConsidersSubDelete.push(
          this.retailData.listRetailProcessConsiders[stepindex].subProcess[
            subindex
          ].id
        );

        Object.keys(
          this.retailData.listRetailProcessConsiders[stepindex].subProcess[
          subindex
          ]
        ).forEach((key, index) => {
          if (key === 'subProcessRisk') {
            // tslint:disable-next-line: no-shadowed-variable
            for (
              // tslint:disable-next-line: no-shadowed-variable
              let index = 0;
              index <
              this.retailData.listRetailProcessConsiders[stepindex].subProcess[
                subindex
              ][key].length;
              index++
            ) {
              Object.keys(
                this.retailData.listRetailProcessConsiders[stepindex]
                  .subProcess[subindex][key][riskIndex]
              ).forEach((sub, i) => {
                this.retailData.listRetailProcessConsiders[
                  stepindex
                ].subProcess[subindex][key][index][sub] = '';
              });
            }
          } else {
            this.retailData.listRetailProcessConsiders[stepindex].subProcess[
              subindex
            ][key] = '';
          }
        });
      }

      // console.log(
      //   'tt',
      //   this.retailData.operations[stepindex].subProcess[subindex]
      // );

      // this.retailData.operations[stepindex].detailDelete.push(
      //   this.retailData.operations[stepindex].subProcess[subindex]['id']
      // );
    } else if (
      this.retailData.listRetailProcessConsiders[stepindex].subProcess
        .length === 1 &&
      this.retailData.listRetailProcessConsiders[stepindex].subProcess[subindex]
        .subProcessRisk.length > 1
    ) {
      console.log(' delete risk row only have 1 row');
      if (
        this.retailData.listRetailProcessConsiders[stepindex].subProcess[
          subindex
        ].subProcessRisk[riskIndex].id !== ''
      ) {
        this.retailData.requestProcessConsidersSubRiskDelete.push(
          this.retailData.listRetailProcessConsiders[stepindex].subProcess[
            subindex
          ].subProcessRisk[riskIndex].id
        );
      }
      this.retailData.listRetailProcessConsiders[stepindex].subProcess[
        subindex
      ].subProcessRisk.splice(riskIndex, 1);
    } else if (
      this.retailData.listRetailProcessConsiders[stepindex].subProcess.length >
      1 &&
      this.retailData.listRetailProcessConsiders[stepindex].subProcess[subindex]
        .subProcessRisk.length > 1
    ) {
      console.log(' delete risk row only have many row');
      // this.retailData.operations[stepindex].detailDelete.push(
      //   this.retailData.operations[stepindex].subProcess[subindex]['id']
      // );
      if (
        this.retailData.listRetailProcessConsiders[stepindex].subProcess[
          subindex
        ].subProcessRisk[riskIndex].id !== ''
      ) {
        this.retailData.requestProcessConsidersSubRiskDelete.push(
          this.retailData.listRetailProcessConsiders[stepindex].subProcess[
            subindex
          ].subProcessRisk[riskIndex].id
        );
      }
      this.retailData.listRetailProcessConsiders[stepindex].subProcess[
        subindex
      ].subProcessRisk.splice(riskIndex, 1);
    } else if (
      this.retailData.listRetailProcessConsiders[stepindex].subProcess[subindex]
        .subProcessRisk.length === 0
    ) {
      console.log(
        'delete row permanent',
        this.retailData.listRetailProcessConsiders[stepindex].id
      );
      // this.retailData.operationsDelete.push(
      //   this.retailData.operations[stepindex].id
      // );
      if (this.retailData.listRetailProcessConsiders[stepindex].id !== '') {
        this.retailData.requestProcessConsidersDelete.push(
          this.retailData.listRetailProcessConsiders[stepindex].id
        );
        this.retailData.listRetailProcessConsiders.splice(stepindex, 1);
      }
    } else if (
      this.retailData.listRetailProcessConsiders[stepindex].subProcess[subindex]
        .subProcessRisk.length === 1 &&
      subindex !== 0 &&
      this.retailData.listRetailProcessConsiders[stepindex].subProcess.length >
      0
    ) {
      console.log('delete row have 1 sub , 1 risk');
      if (
        this.retailData.listRetailProcessConsiders[stepindex].subProcess[
          subindex
        ].id !== ''
      ) {
        this.retailData.requestProcessConsidersSubDelete.push(
          this.retailData.listRetailProcessConsiders[stepindex].subProcess[
            subindex
          ].id
        );
        this.retailData.listRetailProcessConsiders[stepindex].subProcess.splice(
          subindex,
          1
        );
      }
    } else if (
      this.retailData.listRetailProcessConsiders[stepindex].subProcess[subindex]
        .subProcessRisk.length === 1 &&
      subindex === 0
    ) {
      console.log('delete row have 1 sub , 1 risk and delete main row');
      if (this.retailData.listRetailProcessConsiders[stepindex].id !== '') {
        this.retailData.requestProcessConsidersDelete.push(
          this.retailData.listRetailProcessConsiders[stepindex].id
        );
      }
      this.retailData.listRetailProcessConsiders.splice(stepindex, 1);
    } else {
      console.log('not in case');
    }
    this.changeSaveDraft();
  }
  Add_commentOperation_1() {
    this.changeSaveDraft();
    const listRetailProcessConsidersExtra = {
      id: '',
      subject: '',
      management: '',
      poComment: null,
    };

    this.retailData.listRetailProcessConsidersExtra.push(
      listRetailProcessConsidersExtra
    );
  }

  Add_commentOperation_2() {
    this.changeSaveDraft();
    const commentOperation2 = {
      id: '',
      subject: '',
      management: '',
      poComment: null,
    };

    this.retailData.listRetailOperationDetailExtra.push(commentOperation2);
  }
  addRow() {
    this.changeSaveDraft();
    const tempateAddRow = {
      validate: true,
      id: '',
      mainProcess: '',
      subProcess: [
        {
          validate: true,
          id: this.makeid(5),
          subTitle: '',
          inOut: '',
          itRelate: '',
          subProcessRisk: [
            {
              validate: true,
              id: '',
              risk: '',
              riskControl: '',
              riskControlOther: '',
              remark: '',
              subject: '',
              management: '',
            },
          ],
        },
      ],
    };
    this.retailData.listRetailProcessConsiders.push(tempateAddRow);
  }

  makeid(length) {
    let result = '';
    const characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  validateCommentBu() {
    this.validateBu = true;

    console.log('this.validateBu', this.retailData);
    this.retailData.listRetailProcessConsiders.forEach((process, i) => {
      process.subProcess.forEach((sub) => {
        sub.subProcessRisk.forEach((subRisk) => {
          // console.log('this.validateBu', subRisk);
          if (subRisk.management === '' || subRisk.management === null) {
            this.validateProcessConsiders = this.validateProcessConsiders + 1;
          }
          if (subRisk.remark === '' || subRisk.remark === null) {
            this.validateProcessConsiders = this.validateProcessConsiders + 1;
          }
          if (subRisk.subject === '' || subRisk.subject === null) {
            this.validateProcessConsiders = this.validateProcessConsiders + 1;
          }
        });
      });
    });
    this.retailData.listRetailOperationDetail.forEach((process, i) => {
      process.commentOperation.forEach((subRisk) => {
        // console.log('this.validateBu', subRisk);
        if (subRisk.subject === '' || subRisk.subject === null) {
          this.validateProcessConsiders = this.validateProcessConsiders + 1;
        }
        if (subRisk.manage === '' || subRisk.manage === null) {
          this.validateProcessConsiders = this.validateProcessConsiders + 1;
        }
        if (subRisk.reviews === '' || subRisk.reviews === null) {
          this.validateProcessConsiders = this.validateProcessConsiders + 1;
        }
      });
    });
    // console.log(this.validateProcessConsiders);


    return this.validateProcessConsiders;
  }



  send() {
    this.saveDraftstatus_State = false;
    // this.retailService.status_state = false;
    if (this.rowBy !== 'PO' && this.rowBy !== 'PC' && this.rowBy === 'Retail') {


      // if (this.level == 'ฝ่าย' || this.level == 'กลุ่ม' || this.level == 'สาย'
      // ) {
      //   this.router.navigate(['/comment']);
      // }

      this.retailData.requestId = localStorage.getItem('requestId');
      for (
        let index = 0;
        index < this.retailData.listRetailProcessConsiders.length;
        index++
      ) {
        const Group = this.retailData.listRetailProcessConsiders[index]
          .subProcess;

        for (let n = 0; n < Group.length; n++) {
          const subProcess = Group[n].id;
          if (typeof subProcess === 'string') {
            // console.log('subProcess', subProcess);
            this.retailData.listRetailProcessConsiders[index].subProcess[n].id =
              '';
          }
        }
      } for (
        let w = 0;
        w < this.retailData.listRetailProcessConsidersExtra.length;
        w++
      ) {
        if (
          this.retailData.listRetailProcessConsidersExtra[w].id === '' &&
          this.retailData.listRetailProcessConsidersExtra[w].subject === '' &&
          this.retailData.listRetailProcessConsidersExtra[w].management === ''
        ) {
          this.retailData.listRetailProcessConsidersExtra.splice(w, 1);
        }
      }
      for (
        let w = 0;
        w < this.retailData.listRetailProcessConsidersExtra.length;
        w++
      ) {
        if (
          this.retailData.listRetailProcessConsidersExtra[w].id === '' &&
          this.retailData.listRetailProcessConsidersExtra[w].subject === '' &&
          this.retailData.listRetailProcessConsidersExtra[w].management === ''
        ) {
          // console.log('data', w, this.retailData.commentOperation1[w]);
          this.retailData.listRetailProcessConsidersExtra.splice(w, 1);
        }
      }

      // ====== //

      for (
        let w = 0;
        w < this.retailData.listRetailOperationDetailExtra.length;
        w++
      ) {
        if (
          this.retailData.listRetailOperationDetailExtra[w].id === '' &&
          this.retailData.listRetailOperationDetailExtra[w].subject === '' &&
          this.retailData.listRetailOperationDetailExtra[w].management === ''
        ) {
          this.retailData.listRetailOperationDetailExtra.splice(w, 1);
        }
      }
      for (
        let w = 0;
        w < this.retailData.listRetailOperationDetailExtra.length;
        w++
      ) {
        if (
          this.retailData.listRetailOperationDetailExtra[w].id === '' &&
          this.retailData.listRetailOperationDetailExtra[w].subject === '' &&
          this.retailData.listRetailOperationDetailExtra[w].management === ''
        ) {
          // console.log('data', w, this.retailData.commentOperation1[w]);
          this.retailData.listRetailOperationDetailExtra.splice(w, 1);
        }
      }
      // ===============================================================================================//
      for (
        let index = 0;
        index < this.retailData.listRetailProcessConsiders.length;
        index++
      ) {
        if (
          this.retailData.listRetailProcessConsiders[index].id === '' &&
          this.retailData.listRetailProcessConsiders[index].mainProcess === ''
        ) {
          this.retailData.listRetailProcessConsiders.splice(index, 1);
        }
      }
      // ===============================================================================================//
      for (
        let index = 0;
        index < this.retailData.listRetailProcessConsiders.length;
        index++
      ) {
        if (
          this.retailData.listRetailProcessConsiders[index].id === '' &&
          this.retailData.listRetailProcessConsiders[index].mainProcess === ''
        ) {
          this.retailData.listRetailProcessConsiders.splice(index, 1);
          this.BU_statusUpdate = true;
        } else if (
          this.retailData.listRetailProcessConsiders[index].id === '' &&
          this.retailData.listRetailProcessConsiders[index].mainProcess !== ''
        ) {
          const subProcess = this.retailData.listRetailProcessConsiders[index]
            .subProcess;
          for (let r = 0; r < subProcess.length; r++) {
            if (subProcess[r].inOut === '' || subProcess[r].itRelate === '') {
              alert('กรุณาเลือกข้อมูล IN/OUT หรือ IT Related ให้ถูกต้อง');
              this.BU_statusUpdate = false;
              break;
            }
          }

          break;
        } else {
          this.BU_statusUpdate = true;
        }
        // console.log('retailData:', this.retailData);
      }

      this.statuslistRetailProcessConsiders = true;
      this.statuslistRetailOperationDetail = true;
      this.statuslistRetailProcessConsidersExtra = true;
      this.statuslistRetailOperationDetailExtra = true;
      this.arrayStatus = [];

      for (let r = 0; r < this.retailData.listRetailProcessConsiders.length; r++) {
        if (this.retailData.listRetailProcessConsiders[r].mainProcess == '' || this.retailData.listRetailProcessConsiders[r].mainProcess == null) {
          this.statuslistRetailProcessConsiders = false
        } else {
          for (let k = 0; k < this.retailData.listRetailProcessConsiders[r].subProcess.length; k++) {
            const subProcess = this.retailData.listRetailProcessConsiders[r].subProcess[k]
            if ((subProcess.subTitle === '' || subProcess.subTitle === null) ||
              (subProcess.inOut === '' || subProcess.inOut === null) ||
              (subProcess.itRelate === '' || subProcess.itRelate === null)) {
              this.statuslistRetailProcessConsiders = false;
            }
            else {
              subProcess.subProcessRisk.forEach(risk => {
                if ((risk.risk === '' || risk.risk === null) ||
                  (risk.riskControl === '' || risk.riskControl === null) ||
                  (risk.riskControlOther === '' || risk.riskControlOther === null) ||
                  (risk.remark === '' || risk.remark === null) ||
                  (risk.subject === '' || risk.subject === null) ||
                  (risk.management === '' || risk.management === null)) {
                  console.log('false');
                  this.statuslistRetailProcessConsiders = false;
                }
              })
            }
          }
        }
      }

      for (let i = 0; i < this.retailData.listRetailProcessConsidersExtra.length; i++) {
        if ((this.retailData.listRetailProcessConsidersExtra[i].subject == '' ||
          this.retailData.listRetailProcessConsidersExtra[i].subject == null) ||
          (this.retailData.listRetailProcessConsidersExtra[i].management == '' ||
            this.retailData.listRetailProcessConsidersExtra[i].management == null)) {
          this.statuslistRetailProcessConsidersExtra = false
        }
      }

      for (let i = 0; i < this.retailData.listRetailOperationDetail.length; i++) {
        for (let g = 0; g < this.retailData.listRetailOperationDetail[i].commentOperation.length; g++) {
          if ((this.retailData.listRetailOperationDetail[i].commentOperation[g].reviews == '' ||
            this.retailData.listRetailOperationDetail[i].commentOperation[g].reviews == null) ||
            (this.retailData.listRetailOperationDetail[i].commentOperation[g].manage == '' ||
              this.retailData.listRetailOperationDetail[i].commentOperation[g].manage == null) ||
            (this.retailData.listRetailOperationDetail[i].commentOperation[g].subject == '' ||
              this.retailData.listRetailOperationDetail[i].commentOperation[g].subject == null)) {
            this.statuslistRetailOperationDetail = false
          }
        }
      }


      for (let i = 0; i < this.retailData.listRetailOperationDetailExtra.length; i++) {
        if ((this.retailData.listRetailOperationDetailExtra[i].subject == '' ||
          this.retailData.listRetailOperationDetailExtra[i].subject == null) ||
          (this.retailData.listRetailOperationDetailExtra[i].management == '' ||
            this.retailData.listRetailOperationDetailExtra[i].management == null)) {
          this.statuslistRetailOperationDetailExtra = false
        }
      }



      this.validateCommentBu();
      console.log('statuslistRetailProcessConsiders', this.statuslistRetailProcessConsiders);
      console.log('statuslistRetailOperationDetail', this.statuslistRetailOperationDetail);
      console.log('statuscommentOperation1', this.statuslistRetailProcessConsidersExtra);
      console.log('statuscommentOperation2', this.statuslistRetailOperationDetailExtra);
      // console.log('RequestId', this.requestId);
      if (this.statuslistRetailProcessConsiders == false) {
        this.arrayStatus.push('รายละเอียดกระบวนการทำงาน')
      }
      if (this.statuslistRetailOperationDetail == false) {
        this.arrayStatus.push('ความพร้อมด้านการดำเนินการ')
      }
      if (this.statuslistRetailProcessConsidersExtra == false) {
        this.arrayStatus.push('ความเห็นเพิ่มเติม(1)')
      }
      if (this.statuslistRetailOperationDetailExtra == false) {
        this.arrayStatus.push('ความเห็นเพิ่มเติม(2)')
      }
      console.log('arrayStatus:', this.arrayStatus);
      if ((this.validateProcessConsiders > 0 && this.level !== 'แอดมิน' && !(this.level == 'ฝ่าย' || this.level == 'กลุ่ม' || this.level == 'สาย')) || this.arrayStatus.length > 0) {
        alert('กรุณากรอกข้อมูลตาราง ' + this.arrayStatus + ' ให้ครบถ้วน');
        this.validateProcessConsiders = 0;
      } else {
        this.retailService.updateRetail(this.retailData).subscribe((ress) => {
          // console.log('res:', res)
          if (ress['status'] === 'success' || (this.level == 'ฝ่าย' || this.level == 'กลุ่ม' || this.level == 'สาย')) {
            this.Submit.SubmitWork(this.requestId).subscribe(
              (res) => {
                console.log(res);
                if (res['status'] === 'success') {
                  // alert('ส่งงานเรียบร้อย');
                  Swal.fire({
                    title: 'success',
                    text: 'ส่งงานเรียบร้อย',
                    icon: 'success',
                    showConfirmButton: false,
                    timer: 3000,
                  }).then(() => {
                    localStorage.setItem('requestId', '');
                    this.router.navigate(['/dashboard1']);
                  })
                  // this.router.navigate(['/dashboard1']);
                  // this.router.navigate(['/dashboard1']);
                } else {
                  if (res['message'] === 'กรุณาไปให้ความเห็นหน้า ความเห็น / ส่งงาน') {
                    this.router.navigate(['/comment']);
                  } else {
                    Swal.fire({
                      title: 'error',
                      text: res['message'],
                      icon: 'error',
                      // showConfirmButton: false,
                      // timer: 1500,
                    }).then(() => {
                      this.POcommentConsiderAlert(); // this should execute now
                    });
                  }
                }
              },
              (err) => {
                console.log(err);
                Swal.fire({
                  title: 'error',
                  text: err,
                  icon: 'error',
                  showConfirmButton: false,
                  timer: 3000,
                });
              });

          } else if (ress['message'] === 'งานนี้ถูกส่งไปแล้ว') {
            Swal.fire({
              title: 'error',
              text: ress['message'],
              icon: 'error',
              showConfirmButton: false,
              timer: 3000,
            });
          }

        })
      }

      // console.log('data to save bu', this.retailData)

    } else if (this.rowBy === 'PO') {
      const arry = this.CheckValePO();

      if (arry.length > 0) {
        alert('กรุณาให้ความเห็นตาราง ' + arry + ' ให้ครบ')
        this.CommentPO = {
          requestId: localStorage.getItem('requestId'),
          status: false,
          poCommentConsider: [],
          poCommentOperation: [],
          poCommentRetail1: [],
          poCommentRetail2: [],
        };
      }
      else if (arry.length == 0) {
        this.retailService.UpdateRetail_PO(this.CommentPO).subscribe((resSave) => {
          if (resSave['status'] === 'success' || (this.level == 'ฝ่าย' || this.level == 'กลุ่ม' || this.level == 'สาย')) {
            this.Submit.sendDocument_PO(this.requestId, 'Retail').subscribe(
              (res) => {
                console.log(res);
                if (res['status'] === 'success') {
                  // alert('ส่งงานเรียบร้อย');
                  Swal.fire({
                    title: 'success',
                    text: 'ส่งงานเรียบร้อย',
                    icon: 'success',
                    showConfirmButton: false,
                    timer: 3000,
                  }).then(() => {
                    localStorage.setItem('requestId', '');
                    this.router.navigate(['/dashboard1']);
                  })
                } else {
                  // alert(res['message']);
                  if (res['message'] === 'กรุณาไปให้ความเห็นหน้า ความเห็น / ส่งงาน') {
                    this.router.navigate(['/comment']);
                  } else {
                    Swal.fire({
                      title: 'error',
                      text: res['message'],
                      icon: 'error',
                      showConfirmButton: false,
                      timer: 3000,
                    });
                  }
                }
              },
              (err) => {
                console.log(err);
                alert('เกิดข้อผิดพลาดไม่สามารถส่งงานได้');
              }
            );
          } else {
            Swal.fire({
              title: 'error',
              text: resSave['message'],
              icon: 'error',
              showConfirmButton: false,
              timer: 3000,
            });
          }
        }, err => {
          this.status_loading = false;
          console.log(err);
        });
      }

    } else if (this.rowBy === 'PC') {
      const arr = this.checkPC()
      if (arr.length > 0) {
        alert('กรุณาให้ความเห็นตาราง ' + arr + ' ให้ครบ')
        this.Comment_PC = {
          requestId: localStorage.getItem('requestId'),
          status: false,
          listRetailProcessConsiders: [],
          listRetailProcessConsidersExtra: [],
          listRetailOperationDetail: [],
          listRetailOperationDetailExtra: [],
        };
      }
      else {
        this.retailService.UpdateRetail_PC(this.Comment_PC).subscribe((res) => {
          console.log('res PC:', res);
          // console.log('CommentPO:', this.CommentPO);
          if (res['status'] === 'success') {
            this.Submit.sendComment_PC(this.requestId).subscribe(
              (res) => {
                console.log(res);
                if (res['status'] === 'success') {
                  // alert('ส่งงานเรียบร้อย');
                  Swal.fire({
                    title: 'success',
                    text: 'ส่งงานเรียบร้อย',
                    icon: 'success',
                    showConfirmButton: false,
                    timer: 3000,
                  }).then(() => {
                    localStorage.setItem('requestId', '');
                    this.router.navigate(['/dashboard1']);
                  })
                } else {
                  // alert(res['message']);
                  Swal.fire({
                    title: 'error',
                    text: res['message'],
                    icon: 'error',
                    showConfirmButton: false,
                    timer: 3000,
                  });
                }
              },
              (err) => {
                console.log(err);
                alert('เกิดข้อผิดพลาดไม่สามารถส่งงานได้');
              }
            );
          } else {
            // alert(res['message']);
            Swal.fire({
              title: 'error',
              text: res['message'],
              icon: 'error',
              showConfirmButton: false,
              timer: 3000,
            });
          }
        }, err => {
          this.status_loading = false;
          console.log(err);
        });
      }

    } else {
      Swal.fire({
        title: 'error',
        text: 'ไม่สารมรถส่งงานด้',
        icon: 'error',
        showConfirmButton: false,
        timer: 3000,
      });
    }
  }
  checkPC() {
    this.Comment_PC.requestId = Number(this.requestId);
    for (
      let index = 0;
      index < this.POData.listRetailProcessConsiders.length;
      index++
    ) {
      const Main = this.POData.listRetailProcessConsiders[index].subProcess;

      // tslint:disable-next-line: no-shadowed-variable
      for (let i = 0; i < Main.length; i++) {
        const SUB = Main[i].subProcessRisk;
        if (SUB.length !== 0) {
          for (let k = 0; k < SUB.length; k++) {
            if (SUB[k].buCommentId !== null) {
              this.Comment_PC.listRetailProcessConsiders.push({
                buCommentId: SUB[k].buCommentId,
                pcComment: SUB[k].pcComment,
                pcExtraComment: SUB[k].pcExtraComment,
                pcIssue: SUB[k].pcIssue,
              });
            }
          }
        }
      }
    }

    for (
      let index = 0;
      index < this.POData.listRetailOperationDetail.length;
      index++
    ) {
      const commentOperation = this.POData.listRetailOperationDetail[index]
        .commentOperation;
      // console.log('Main:', commentOperation);
      // tslint:disable-next-line: no-shadowed-variable
      for (let i = 0; i < commentOperation.length; i++) {
        if (commentOperation[i].buCommentId !== null) {
          this.Comment_PC.listRetailOperationDetail.push({
            buCommentId: commentOperation[i].buCommentId,
            pcComment: commentOperation[i].pcComment,
            pcExtraComment: commentOperation[i].pcExtraComment,
            pcIssue: commentOperation[i].pcIssue,
          });
        }
      }
    }
    // console.log('listRetailProcessConsidersExtra', this.POData.listRetailProcessConsidersExtra);
    if (this.POData.listRetailProcessConsidersExtra.length !== 0) {
      for (
        let index = 0;
        index < this.POData.listRetailProcessConsidersExtra.length;
        index++
      ) {
        const commentOperation1 = this.POData.listRetailProcessConsidersExtra[
          index
        ];
        // console.log('Main:', commentOperation1);
        // tslint:disable-next-line: no-shadowed-variable

        this.Comment_PC.listRetailProcessConsidersExtra.push({
          buCommentId: commentOperation1.id,
          pcComment: commentOperation1.pcComment,
          pcExtraComment: commentOperation1.pcExtraComment,
          pcIssue: commentOperation1.pcIssue,
        });

      }
    }
    // console.log('Main:', this.POData.listRetailOperationDetailExtra);
    if (this.POData.listRetailOperationDetailExtra.length !== 0) {
      for (
        let index = 0;
        index < this.POData.listRetailOperationDetailExtra.length;
        index++
      ) {
        const commentOperation2 = this.POData.listRetailOperationDetailExtra[
          index
        ];

        // tslint:disable-next-line: no-shadowed-variable

        this.Comment_PC.listRetailOperationDetailExtra.push({
          buCommentId: commentOperation2.id,
          pcComment: commentOperation2.pcComment,
          pcExtraComment: commentOperation2.pcExtraComment,
          pcIssue: commentOperation2.pcIssue,
        });

      }
    }
    const check_process_pcIssue = Object.values(this.Comment_PC.listRetailProcessConsiders).every(value => value['pcIssue'] !== null && value['pcIssue'] !== '');
    const check_process_pcExtraComment = Object.values(this.Comment_PC.listRetailProcessConsiders).every(value => value['pcExtraComment'] !== null && value['pcExtraComment'] !== '');
    const check_process_pcComment = Object.values(this.Comment_PC.listRetailProcessConsiders).every(value => value['pcComment'] !== null && value['pcComment'] !== '');

    const check_processEx_pcIssue = Object.values(this.Comment_PC.listRetailProcessConsidersExtra).every(value => value['pcIssue'] !== null && value['pcIssue'] !== '');
    const check_processEx_pcExtraComment = Object.values(this.Comment_PC.listRetailProcessConsidersExtra).every(value => value['pcExtraComment'] !== null && value['pcExtraComment'] !== '');
    const check_processEx_pcComment = Object.values(this.Comment_PC.listRetailProcessConsidersExtra).every(value => value['pcComment'] !== null && value['pcComment'] !== '');

    const check_detail_pcIssue = Object.values(this.Comment_PC.listRetailOperationDetail).every(value => value['pcIssue'] !== null && value['pcIssue'] !== '');
    const check_detail_pcExtraComment = Object.values(this.Comment_PC.listRetailOperationDetail).every(value => value['pcExtraComment'] !== null && value['pcExtraComment'] !== '');
    const check_detail_pcComment = Object.values(this.Comment_PC.listRetailOperationDetail).every(value => value['pcComment'] !== null && value['pcComment'] !== '');

    const check_detailEx_pcIssue = Object.values(this.Comment_PC.listRetailOperationDetailExtra).every(value => value['pcIssue'] !== null && value['pcIssue'] !== '');
    const check_detailEx_pcExtraComment = Object.values(this.Comment_PC.listRetailOperationDetailExtra).every(value => value['pcExtraComment'] !== null && value['pcExtraComment'] !== '');
    const check_detailEx_pcComment = Object.values(this.Comment_PC.listRetailOperationDetailExtra).every(value => value['pcComment'] !== null && value['pcComment'] !== '');

    console.log('Comment_PC:', this.Comment_PC);
    console.log('listRetailProcessConsiders:', this.Comment_PC.listRetailProcessConsiders,
      check_process_pcIssue, check_process_pcExtraComment, check_process_pcComment);
    console.log('listRetailProcessConsidersEx:', this.Comment_PC.listRetailProcessConsidersExtra,
      check_processEx_pcIssue, check_processEx_pcExtraComment, check_processEx_pcComment);
    console.log('istRetailOperationDetail:', this.Comment_PC.listRetailOperationDetail,
      check_detail_pcIssue, check_detail_pcExtraComment, check_detail_pcComment);
    console.log('listRetailOperationDetailExtra:', this.Comment_PC.listRetailOperationDetailExtra,
      check_detailEx_pcIssue, check_detailEx_pcExtraComment, check_detailEx_pcComment);
    if (check_process_pcIssue === true && check_process_pcExtraComment === true && check_process_pcComment === true &&
      check_processEx_pcIssue === true && check_processEx_pcExtraComment === true && check_processEx_pcComment === true &&
      check_detail_pcIssue === true && check_detail_pcExtraComment === true && check_detail_pcComment === true &&
      check_detailEx_pcIssue === true && check_detailEx_pcExtraComment === true && check_detailEx_pcComment == true) {
      this.Comment_PC.status = true;
    }
    this.arryPC = []

    this.arryPC = []
    if (check_process_pcIssue === false ||
      check_process_pcExtraComment === false ||
      check_process_pcComment === false) {
      this.arryPC.push('รายละเอียดกระบวนการทำงาน')
    }

    if (check_processEx_pcIssue === false ||
      check_processEx_pcExtraComment === false ||
      check_processEx_pcComment === false) {
      this.arryPC.push('ความเห็นเพิ่มเติม(1)')
    }

    if (check_detail_pcIssue === false ||
      check_detail_pcExtraComment === false ||
      check_detail_pcComment === false) {
      this.arryPC.push('ความพร้อมด้านการดำเนินการ')
    }

    if (check_detailEx_pcIssue === false ||
      check_detailEx_pcExtraComment === false ||
      check_detailEx_pcComment === false) {
      this.arryPC.push('ความเห็นเพิ่มเติม(2)')
    }
    return this.arryPC;
  }
  CheckValePO() {
    this.CommentPO.requestId = Number(this.requestId);
    for (
      let index = 0;
      index < this.POData.listRetailProcessConsiders.length;
      index++
    ) {
      const Main = this.POData.listRetailProcessConsiders[index].subProcess;

      // tslint:disable-next-line: no-shadowed-variable
      for (let i = 0; i < Main.length; i++) {
        const SUB = Main[i].subProcessRisk;
        if (SUB.length !== 0) {
          for (let k = 0; k < SUB.length; k++) {
            if (SUB[k].buCommentId !== null) {
              this.CommentPO.poCommentConsider.push({
                buCommentId: SUB[k].buCommentId,
                poComment: SUB[k].poComment,
              });
            }
          }
        }
      }
    }

    for (
      let index = 0;
      index < this.POData.listRetailOperationDetail.length;
      index++
    ) {
      const commentOperation = this.POData.listRetailOperationDetail[index]
        .commentOperation;
      // console.log('Main:', commentOperation);
      // tslint:disable-next-line: no-shadowed-variable
      for (let i = 0; i < commentOperation.length; i++) {
        if (commentOperation[i].buCommentId !== null) {
          this.CommentPO.poCommentOperation.push({
            buCommentId: commentOperation[i].buCommentId,
            poComment: commentOperation[i].poComment,
          });
        }
      }
    }
    // console.log('listRetailProcessConsidersExtra', this.POData.listRetailProcessConsidersExtra);
    if (this.POData.listRetailProcessConsidersExtra.length !== 0) {
      for (
        let index = 0;
        index < this.POData.listRetailProcessConsidersExtra.length;
        index++
      ) {
        const commentOperation1 = this.POData.listRetailProcessConsidersExtra[
          index
        ];
        // console.log('Main:', commentOperation1);
        // tslint:disable-next-line: no-shadowed-variable
        if (commentOperation1.id !== null) {
          this.CommentPO.poCommentRetail1.push({
            buCommentId: commentOperation1.id,
            poComment: commentOperation1.poComment,
          });
        }
      }
    }

    if (this.POData.listRetailOperationDetailExtra.length !== 0) {
      for (
        let index = 0;
        index < this.POData.listRetailOperationDetailExtra.length;
        index++
      ) {
        const commentOperation2 = this.POData.listRetailOperationDetailExtra[
          index
        ];
        // console.log('Main:', commentOperation1);
        // tslint:disable-next-line: no-shadowed-variable
        if (commentOperation2.id !== null) {
          this.CommentPO.poCommentRetail2.push({
            buCommentId: commentOperation2.id,
            poComment: commentOperation2.poComment,
          });
        }
      }
    }
    const arr = [];
    // tslint:disable-next-line: max-line-length
    const check_poCommentConsider = Object.values(this.CommentPO.poCommentConsider).every(value => value['poComment'] !== '' && value['poComment'] !== null);
    // tslint:disable-next-line: max-line-length
    const check_poCommentOperation = Object.values(this.CommentPO.poCommentOperation).every(value => value['poComment'] !== '' && value['poComment'] !== null);
    // tslint:disable-next-line: max-line-length
    const check_poCommentRetail1 = Object.values(this.CommentPO.poCommentRetail1).every(value => value['poComment'] !== '' && value['poComment'] !== null);
    // tslint:disable-next-line: max-line-length
    const check_poCommentRetail2 = Object.values(this.CommentPO.poCommentRetail2).every(value => value['poComment'] !== '' && value['poComment'] !== null);
    console.log('CommentPO to save:>>', check_poCommentConsider);
    console.log('CommentPO to save:>>', check_poCommentOperation);
    console.log('CommentPO to save:>>', check_poCommentRetail1);
    console.log('CommentPO to save:>>', check_poCommentRetail2);
    if (check_poCommentConsider === true &&
      check_poCommentOperation === true &&
      check_poCommentRetail1 === true &&
      check_poCommentRetail2 === true
    ) {
      this.CommentPO.status = true;
    }

    if (check_poCommentConsider == false) {
      arr.push('รายละเอียดกระบวนการทำงาน')
    }
    if (check_poCommentOperation == false) {
      arr.push('ความพร้อมด้านการดำเนินการ')
    }
    if (check_poCommentRetail1 == false) {
      arr.push('ความเห็นเพิ่มเติม(1)')
    }
    if (check_poCommentRetail2 == false) {
      arr.push('ความเห็นเพิ่มเติม(2)')
    }

    return arr;
  }

  POcommentConsiderAlert() {
    this.Submit.linkToAny();
  }

  backToDashboardPage() {
    // localStorage.setItem('requestId', '');
    this.router.navigate(['dashboard1']);
  }

  autoGrowTextZone(e) {
    e.target.style.height = '0px';
    e.target.style.overflow = 'hidden';
    e.target.style.height = (e.target.scrollHeight + 0) + 'px';
  }
  covertDateToCorrect(dueDate: any) {
    if (dueDate !== null) {
      const type = String(dueDate);
      const year = Number(type.substr(0, 4)) + 543;
      const month = type.substr(5, 2);
      const day = type.substr(8, 2);
      const date = day + '/' + month + '/' + year;
      return date;
    } else {
      return '';
    }
  }
}
