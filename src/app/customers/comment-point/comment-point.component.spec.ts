import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentPointComponent } from './comment-point.component';

describe('CommentPointComponent', () => {
  let component: CommentPointComponent;
  let fixture: ComponentFixture<CommentPointComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentPointComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentPointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
