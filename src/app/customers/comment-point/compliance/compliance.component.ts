import { Result } from './../../dashboard-cus/dashboard-cus.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
  CdkDragExit,
  CdkDragEnter,
  CdkDragStart,
} from '@angular/cdk/drag-drop';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ComplianceService } from './../../../services/comment-point/compliance/compliance.service';
import { SubmitService } from './../../../services/comment-point/submit/submit.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';

import { map, startWith } from 'rxjs/operators';
import { DialogStatusService } from '../../../services/comment-point/dialog-status/dialog-status.service';
import { DialogSaveStatusComponent } from '../../comment-point/dialog-save-status/dialog-save-status.component';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { promise } from 'protractor';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RequestService } from '../../../services/request/request.service';

export interface FileList {
  fileName: string;
  fileSize: string;
  base64File: string;
}

@Component({
  selector: 'app-compliance',
  templateUrl: './compliance.component.html',
  styleUrls: ['./compliance.component.scss'],
})
export class ComplianceComponent implements OnInit {
  validateBu = 0;
  data = {
    requestId: null,
    validate: false,
    complianceItems: [
      {
        id: null,
        complianceId: null,
        unit: '',
        bookNumber: '',
        topic: '',
        allow: '',
        other: '',
        createdAt: '',
        updatedAt: '',
        deletedAt: '',
      },
    ],
    account: [
      {
        id: null,
        regulation: '',
        masterId: null,
        topic: [
          {
            id: null,
            masterId: null,
            topic: '',
            detail: [
              {
                id: null,
                masterId: null,
                detail: '',
                comply: null,
                review: null,
                buCommentId: null,
                poComment: '',
                pcComment: '',
                pcExtraComment: '',
                pcIssue: '',
              },
            ],
          },
        ],
      },
    ],
    department1: [
      {
        id: null,
        masterId: null,
        department: '',
        bookNumber: '',
        topic: [
          {
            id: null,
            masterId: null,
            topic: '',
            detail: [
              {
                id: null,
                masterId: null,
                detail: '',
                comply: null,
                review: null,
                buCommentId: null,
                poComment: '',
                pcComment: '',
                pcExtraComment: '',
                pcIssue: '',
              },
            ],
          },
        ],
      },
    ],
    department2: [
      {
        id: null,
        masterId: null,
        department: '',
        bookNumber: '',
        topic: [
          {
            id: null,
            masterId: null,
            topic: '',
            detail: [
              {
                id: null,
                masterId: null,
                detail: '',
                comply: null,
                review: null,
                buCommentId: null,
                poComment: '',
                pcComment: '',
                pcExtraComment: '',
                pcIssue: '',
              },
            ],
          },
        ],
      },
    ],
    rule: [
      {
        id: null,
        masterId: null,
        department: '',
        bookNumber: '',
        topic: [
          {
            id: null,
            masterId: null,
            topic: '',
            detail: [
              {
                id: null,
                masterId: null,
                detail: '',
                comply: null,
                review: null,
                buCommentId: null,
                poComment: '',
                pcComment: '',
                pcExtraComment: '',
                pcIssue: '',
              },
            ],
          },
        ],
      },
    ],
    law: [
      {
        id: null,
        masterId: null,
        department: '',
        bookNumber: '',
        topic: [
          {
            id: null,
            masterId: null,
            topic: '',
            detail: [
              {
                id: null,
                masterId: null,
                detail: '',
                comply: null,
                review: null,
                buCommentId: null,
                poComment: '',
                pcComment: '',
                pcExtraComment: '',
                pcIssue: '',
              },
            ],
          },
        ],
      },
    ],
  };

  data_delete = {
    accountDelete: [],
    accountTopicDelete: [],
    accountDetailDelete: [],
    department1Delete: [],
    department1TopicDelete: [],
    department1DetailDelete: [],
    department2Delete: [],
    department2TopicDelete: [],
    department2DetailDelete: [],
    ruleDelete: [],
    ruleTopicDelete: [],
    ruleDetailDelete: [],
    lawDelete: [],
    lawTopicDelete: [],
    lawDetailDelete: [],
  };

  role: any;
  status_loading = false;
  status_loading2 = false;
  saveDraftstatus = null;
  saveDraftstatus_State = false;
  validate = null;
  // drag and drop
  dropIndex: number;
  issue_dropIndex: number;
  staticTitleIndexRisk = [];

  staticTitleIndex = []; // !important ex [0,1,2] mean set fix row index 0 1 2

  displayedColumns_file: string[] = ['name', 'size', 'delete'];
  dataSource_file: MatTableDataSource<FileList>;
  attachments: any = {
    file: [],
    fileDelete: [],
  };

  results: any;
  api_response: any;
  getlevel: any;

  list: any = {
    account: [],
    department1: [],
    department2: [],
    rule: [],
    law: [],
  };
  accountList: any = [];
  topicList: any = [];

  department1List: any = [];
  topicDepartment1List: any = [];

  department2List: any = [];
  topicDepartment2List: any = [];

  ruleList: any = [];
  topicRuleList: any = [];

  lawList: any = [];
  topicLawList: any = [];
  rolename: any;

  message_error: any;
  list_error: any = [];
  Getname: any;
  alertMessage: any = '';
  arryPC: any = [];
  StatusCompliance: any = [];
  constructor(
    private router: Router,
    public complianceService: ComplianceService,
    public Submit: SubmitService,
    public dialog: MatDialog,
    private sidebarService: RequestService
  ) { }

  note = [
    'No คือ ไม่ใช่ประเด็นคงค้าง',
    'Pending 1 คือ ต้องแก้ไขก่อนประกาศใช้ผลิตภัณฑ์',
    'Pending 2 คือ สามารถใช้ผลิตภัณฑ์ได้ก่อน แล้วค่อยแก้ไขตามหลังได้',
    'Pending 3 คือ ไม่เกี่ยวข้องกับการประกาศใช้ผลิตภัณฑ์',
    'Pending 4 คือ ต้องแก้ไขให้แล้วเสร็จก่อนนำเสนอเรื่องต่อบอร์ด',
  ];

  ngOnInit() {
    this.role = localStorage.getItem('role');
    this.rolename = localStorage.getItem('role');
    this.getlevel = localStorage.getItem('level');
    this.Getname = localStorage.getItem('ouName');
    if (
      (this.role === 'ฝ่ายกำกับระเบียบและการปฏิบัติงาน' ||
        this.role === 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ธุรกิจ 1' ||
        this.role === 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ธุรกิจ 2' ||
        this.role === 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ทางการ' ||
        this.role === 'ฝ่ายนิติการ') &&
      this.getlevel === 'พนักงาน'
    ) {
      this.role = 'ComplianceValidateTrue';
    } else if (
      (this.role === 'ฝ่ายกำกับระเบียบและการปฏิบัติงาน' ||
        this.role === 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ธุรกิจ 1' ||
        this.role === 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ธุรกิจ 2' ||
        this.role === 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ทางการ' ||
        this.role === 'ฝ่ายนิติการ') &&
      this.getlevel !== 'พนักงาน'
    ) {
      this.role = 'Compliance';
    }

    this.dataSource_file = new MatTableDataSource(this.attachments.file);
    this.GetDropDown();


    // console.log('data.validate', this.data.validate);
  }

  getDetail(id) {
    this.status_loading = true;
    if (this.role === 'ComplianceValidateTrue') {
      this.accountList = [];
      this.topicList = [];
      this.complianceService.getCompliance(id).subscribe((res) => {
        this.results = res;
        this.validate = this.results.data.validate;
        console.log('validate', this.validate);
        console.log('getDetail', this.results);
        this.data = this.results.data;

        // ------------------- account -------------------------------
        this.data.account.forEach((account, ii) => {
          if (account.topic.length === 0) {
            this.data_delete.accountDelete.push(account.id);
            this.data.account.splice(ii, 1);
          } else {
            account.topic.forEach((topic, index) => {
              if (topic.detail.length === 0) {
                console.log('number::', topic);
                this.data_delete.accountTopicDelete.push(topic.id);
                account.topic.splice(index, 1);
              }
            });
          }
        });
        for (let i = 0; i < this.data.account.length; i++) {
          if (this.data.account[i].topic.length === 0) {
            if (this.data.account[i].id !== null) {
              this.data_delete.accountDelete.push(this.data.account[i].id);
            }
            this.data.account.splice(i, 1);
          }
        }
        // ---------------------------account---------------------------------
        this.data.account.forEach((item, index) => {
          this.accountList.push([]);
          this.topicList.push([]);
          item.topic.forEach((topic) => {
            this.ChangeGroupDropdown('account', item.masterId, index, topic);
          });
        });

        // ---------------------------department1---------------------------------
        this.data.department1.forEach((item, index) => {
          this.department1List.push([]);
          this.topicDepartment1List.push([]);
          item.topic.forEach((topic) => {
            this.DepartmentHeader(
              'department1',
              item.masterId,
              index,
              topic,
              item
            );
          });
        });

        // ---------------------------department2---------------------------------
        this.data.department2.forEach((item, index) => {
          this.department2List.push([]);
          this.topicDepartment2List.push([]);
          item.topic.forEach((topic) => {
            this.DepartmentHeader2(
              'department2',
              item.masterId,
              index,
              topic,
              item
            );
          });
        });

        // ---------------------------rule---------------------------------
        this.data.rule.forEach((item, index) => {
          this.ruleList.push([]);
          this.topicRuleList.push([]);
          item.topic.forEach((topic) => {
            this.RuleHeader('rule', item.masterId, index, topic, item);
          });
        });

        // ---------------------------law---------------------------------
        this.data.law.forEach((item, index) => {
          this.lawList.push([]);
          this.topicLawList.push([]);
          item.topic.forEach((topic) => {
            this.LawHeader('law', item.masterId, index, topic, item);
          });
        });

        console.log(this.accountList);
        this.status_loading = false;

      }, err => {
        this.status_loading = false;
        console.log(err);
      });
    } else if (this.role === 'PC') {
      this.complianceService.getPCCompliance(id).subscribe((res) => {
        this.results = res;
        console.log('getDetail PC', this.results);
        this.data = this.results.data;
        this.validate = this.results.data.validate;

        this.status_loading = false;
      }, err => {
        this.status_loading = false;
        console.log(err);
      });
    } else {
      this.complianceService.getPOCompliance(id).subscribe((res) => {
        this.results = res;
        console.log('getDetail', this.results);
        this.data = this.results.data;
        this.validate = this.results.data.validate;

        this.status_loading = false;
      }, err => {
        this.status_loading = false;
        console.log(err);
      });
    }

    console.log(this.validate);
  }

  changeSaveDraft() {
    this.saveDraftstatus_State = true;
    this.complianceService.status_state = true;
    console.log(this.saveDraftstatus_State)
  }
  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate');
    // return confirm();
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    console.log('000 sidebarService status:', status);
    if (this.saveDraftstatus_State === true) {
      // console.log('000 sidebarService status:', status);
      const dialogRef = this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        console.log('data::', data);
        if (data.returnStatus === true) {
          subject.next(true);
          this.savedData();
          this.saveDraftstatus_State = false;
        } else if (data.returnStatus === false) {
          console.log('000000 data::', data.returnStatus);
          this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
        }
      });
      console.log('8889999', subject);
      return subject;
    } else {
      return true;
    }
  }
  checkStaticTitle(index) {
    console.log(this.staticTitleIndex);
    return this.staticTitleIndex.includes(index);
  }

  // drag and drop =====================================================================>
  drop(event: CdkDragDrop<string[]>, table_name) {
    const fixedBody = this.staticTitleIndex;
    if (event.previousContainer === event.container) {
      console.log('1');
      // moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (event.previousContainer.data.length > 1) {
      console.log(event.previousContainer);
      console.log('2');

      // check if only one left child
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      console.log('4');
      // check only 1 child left and not a static type move and delete
      console.log(
        'check only 1 child left and not a static type move and delete'
      );
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      // remove current row
      this.removeAddItem(this.dropIndex, table_name);
      this.dropIndex = null;
    }
    this.changeSaveDraft();
  }

  dropItem(event: CdkDragDrop<string[]>, table_name) {
    console.log('event item :', event);
    console.log('previousContainer :', event.container.data.length);
    console.log('index group', this.dropIndex);
    console.log('index item:', this.issue_dropIndex);
    const fixedBody = this.staticTitleIndexRisk;

    // if (
    //   event.previousContainer.data.length === 1 &&
    //   event.container.data.length !== 1
    // ) {
    //   console.log('index:', this.dropIndex);
    //   this.removeAddItem(this.dropIndex);
    //   this.dropIndex = null;
    // }
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (event.previousContainer.data.length > 1) {
      // check if only one left child
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      // check only 1 child left and not a static type move and delete
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      // remove current row
      this.removeAddItemRisk(this.dropIndex, this.issue_dropIndex, table_name);
      this.dropIndex = null;
      this.issue_dropIndex = null;
    }
    this.changeSaveDraft();
  }

  dragExited(event: CdkDragExit<string[]>) {
    console.log('Exited', event);
  }

  entered(event: CdkDragEnter<string[]>) {
    console.log('Entered', event.container.data);
    console.log(this.list.account);
  }
  startDrag(event: CdkDragStart<string[]>, istep) {
    console.log('startDrag index', istep);
    this.dropIndex = istep;
  }

  startDrag1(event: CdkDragStart<string[]>, istep, head, topic) {

    for (let i = 0; i < this.list.account.length; i++) {


      for (let k = 0; k < this.list.account[i].topic.length; k++) {
        if (event.source.data['topic'] === this.list.account[i].topic[k].topic) {
          console.log("MASTER : ", event.source.data['masterId']);
          // event.source.data['masterId'] = null;
          if (event.source.data['masterId'] !== null) {
            event.source.data['masterId'] = this.list.account[i].topic[k].id;
          } else {
            event.source.data['masterId'] = null;
          }

        } else {
          event.source.data['masterId'] = null;
        }
      }

    }
    // event.source.data['masterId'] = null;
    // event.source.data['topic'] = null;
    console.log('startDrag index', istep);
    this.dropIndex = istep;
  }

  startDrag_issue(event: CdkDragStart<string[]>, isup, istep) {
    console.log('Risks tartDrag', event);
    console.log('Risk startDrag index', isup);
    this.issue_dropIndex = isup;
    this.dropIndex = istep;
  }

  connectItem(): any[] {
    const mapping = [];
    for (let index = 0; index < this.data.account.length; index++) {
      const Group1 = this.data.account[index].topic;
      for (let j = 0; j < Group1.length; j++) {
        mapping.push('account' + String(Group1[j].id));
      }
    }
    for (let index = 0; index < this.data.department1.length; index++) {
      const Group1 = this.data.department1[index].topic;
      for (let j = 0; j < Group1.length; j++) {
        mapping.push('1dept' + String(Group1[j].id));
      }
    }
    for (let index = 0; index < this.data.department2.length; index++) {
      const Group1 = this.data.department2[index].topic;
      for (let j = 0; j < Group1.length; j++) {
        mapping.push('2dept' + String(Group1[j].id));
      }
    }
    for (let index = 0; index < this.data.rule.length; index++) {
      const Group1 = this.data.rule[index].topic;
      for (let j = 0; j < Group1.length; j++) {
        mapping.push('rule' + String(Group1[j].id));
      }
    }
    for (let index = 0; index < this.data.law.length; index++) {
      const Group1 = this.data.law[index].topic;
      for (let j = 0; j < Group1.length; j++) {
        mapping.push('law' + String(Group1[j].id));
      }
    }
    // console.log('map', mapping);
    return mapping;
  }

  removeAddItem(index, table_name) {
    console.log('index remove', index);
    if (index != null) {
      if (table_name === 'account') {
        if (typeof this.data.account[index].id === 'number') {
          this.data_delete.accountDelete.push(this.data.account[index].id);
        }
        this.data.account.splice(index, 1);
      }
      if (table_name === 'department1') {
        if (typeof this.data.department1[index].id === 'number') {
          this.data_delete.department1Delete.push(
            this.data.department1[index].id
          );
        }
        this.data.department1.splice(index, 1);
      }
      if (table_name === 'department2') {
        if (typeof this.data.department2[index].id === 'number') {
          this.data_delete.department2Delete.push(
            this.data.department2[index].id
          );
        }
        this.data.department2.splice(index, 1);
      }
      if (table_name === 'rule') {
        if (typeof this.data.rule[index].id === 'number') {
          this.data_delete.ruleDelete.push(this.data.rule[index].id);
        }
        this.data.rule.splice(index, 1);
      }
      if (table_name === 'law') {
        console.log(this.data.law[index]);
        if (typeof this.data.law[index].id === 'number') {
          this.data_delete.lawDelete.push(this.data.law[index].id);
        }
        this.data.law.splice(index, 1);
      }
    }
  }

  removeAddItemRisk(subIndex, Riskkindex, table_name) {
    console.log('in delete department');
    if (table_name === 'account') {
      if (this.data.account[subIndex].topic.length === 1) {
        this.data.account.splice(subIndex, 1);
      } else if (
        this.data.account[subIndex].topic.length > 1 &&
        Riskkindex != null
      ) {
        this.data.account[subIndex].topic.splice(Riskkindex, 1);
      }
    }
    if (table_name === 'department1') {
      console.log('in delete department');
      if (this.data.department1[subIndex].topic.length === 1) {
        this.data.department1.splice(subIndex, 1);
      } else if (
        this.data.department1[subIndex].topic.length > 1 &&
        Riskkindex != null
      ) {
        this.data.department1[subIndex].topic.splice(Riskkindex, 1);
      }
    }
    if (table_name === 'department2') {
      if (this.data.department2[subIndex].topic.length === 1) {
        this.data.department2.splice(subIndex, 1);
      } else if (
        this.data.department2[subIndex].topic.length > 1 &&
        Riskkindex != null
      ) {
        this.data.department2[subIndex].topic.splice(Riskkindex, 1);
      }
    }
    if (table_name === 'rule') {
      if (this.data.rule[subIndex].topic.length === 1) {
        this.data.rule.splice(subIndex, 1);
      } else if (
        this.data.rule[subIndex].topic.length > 1 &&
        Riskkindex != null
      ) {
        this.data.rule[subIndex].topic.splice(Riskkindex, 1);
      }
    }
    if (table_name === 'law') {
      if (this.data.law[subIndex].topic.length === 1) {
        this.data.law.splice(subIndex, 1);
      } else if (
        this.data.law[subIndex].topic.length > 1 &&
        Riskkindex != null
      ) {
        this.data.law[subIndex].topic.splice(Riskkindex, 1);
      }
    }
  }

  addRow(type_table) {
    if (type_table === 'account') {
      const tempateAddRow = {
        id: null,
        regulation: '',
        masterId: null,
        topic: [
          {
            id: this.makeid(5),
            masterId: null,
            topic: '',
            detail: [
              {
                id: null,
                masterId: null,
                detail: '',
                comply: null,
                review: null,
                buCommentId: null,
                poComment: '',
                pcComment: '',
                pcExtraComment: '',
                pcIssue: '',
              },
            ],
          },
        ],
      };
      this.data.account.push(tempateAddRow);
      console.log('this.data.account', this.data.account);
      console.log('list:', this.list.account);
    }
    if (type_table === 'department1') {
      const tempateAddRow = {
        id: null,
        masterId: null,
        department: '',
        bookNumber: '',
        topic: [
          {
            id: this.makeid(5),
            topic: '',
            masterId: null,
            detail: [
              {
                id: null,
                masterId: null,
                detail: '',
                comply: null,
                review: null,
                buCommentId: null,
                poComment: '',
                pcComment: '',
                pcExtraComment: '',
                pcIssue: '',
              },
            ],
          },
        ],
      };
      this.data.department1.push(tempateAddRow);
      console.log('this.data.department1', this.data.department1);
    }
    if (type_table === 'department2') {
      const tempateAddRow = {
        id: null,
        masterId: null,
        department: '',
        bookNumber: '',
        topic: [
          {
            id: this.makeid(5),
            masterId: null,
            topic: '',
            detail: [
              {
                id: null,
                masterId: null,
                detail: '',
                comply: null,
                review: null,
                buCommentId: null,
                poComment: '',
                pcComment: '',
                pcExtraComment: '',
                pcIssue: '',
              },
            ],
          },
        ],
      };
      this.data.department2.push(tempateAddRow);
      console.log('this.data.department2', this.data.department2);
    }
    if (type_table === 'rule') {
      const tempateAddRow = {
        id: null,
        masterId: null,
        department: '',
        bookNumber: '',
        topic: [
          {
            id: this.makeid(5),
            masterId: null,
            topic: '',
            detail: [
              {
                id: null,
                masterId: null,
                detail: '',
                comply: null,
                review: null,
                buCommentId: null,
                poComment: '',
                pcComment: '',
                pcExtraComment: '',
                pcIssue: '',
              },
            ],
          },
        ],
      };
      this.data.rule.push(tempateAddRow);
      console.log('this.data.rule', this.data.rule);
    }
    if (type_table === 'law') {
      const tempateAddRow = {
        id: null,
        masterId: null,
        department: '',
        bookNumber: '',
        topic: [
          {
            id: this.makeid(5),
            masterId: null,
            topic: '',
            detail: [
              {
                id: null,
                masterId: null,
                detail: '',
                comply: null,
                review: null,
                buCommentId: null,
                poComment: '',
                pcComment: '',
                pcExtraComment: '',
                pcIssue: '',
              },
            ],
          },
        ],
      };
      this.data.law.push(tempateAddRow);
      console.log('this.data.law', this.data.law);
    }
  }

  removeDataItem(table_name, istep, itopic, isub) {
    console.log(istep, itopic, isub, this.issue_dropIndex);
    // console.log('id sub', this.data.law[istep].topic[itopic].detail[isub].id);
    const fixedBody = [];
    if (table_name === 'account') {
      console.log('in account');
      if (
        typeof this.data.account[istep].topic[itopic].detail[isub].id !==
        'string'
      ) {
        this.data_delete.accountDetailDelete.push(
          this.data.account[istep].topic[itopic].detail[isub].id
        );
      }
      if (this.data.account[istep].topic.length > 1) {
        console.log('in topic length > 1');
        if (this.data.account[istep].topic[itopic].detail.length > 1) {
          console.log('in topic detail length > 1');
          this.data.account[istep].topic[itopic].detail.splice(isub, 1);
        } else if (this.data.account[istep].topic[itopic].detail.length === 1) {
          if (typeof this.data.account[istep].topic[itopic].id !== 'string') {
            this.data_delete.accountTopicDelete.push(
              this.data.account[istep].topic[itopic].id
            );
          }
          console.log('in topic detail length = 1');
          this.data.account[istep].topic.splice(itopic, 1);
        }
      } else if (this.data.account[istep].topic.length === 1) {
        console.log('in topic length = 1');
        if (this.data.account[istep].topic[itopic].detail.length > 1) {
          console.log('in topic detail length > 1');
          this.data.account[istep].topic[itopic].detail.splice(isub, 1);
        } else if (
          fixedBody.includes(istep) &&
          this.data.account[istep].topic[itopic].detail.length === 1 &&
          this.data.account.length === 1
        ) {
          console.log(
            'in topic detail length = 1 & first',
            this.data.account.length
          );
          if (typeof this.data.account[istep].topic[itopic].id !== 'string') {
            this.data_delete.accountTopicDelete.push(
              this.data.account[istep].topic[itopic].id
            );
          }
          if (typeof this.data.account[istep].id !== 'string') {
            this.data_delete.accountDelete.push(this.data.account[istep].id);
          }
          this.data.account.splice(istep, 1);
          this.addRow('account');
        } else if (this.data.account[istep].topic[itopic].detail.length === 1) {
          console.log('in topic detail length = 1');
          if (typeof this.data.account[istep].topic[itopic].id !== 'string') {
            this.data_delete.accountTopicDelete.push(
              this.data.account[istep].topic[itopic].id
            );
          }
          if (typeof this.data.account[istep].id !== 'string') {
            this.data_delete.accountDelete.push(this.data.account[istep].id);
          }
          this.data.account.splice(istep, 1);
        }
      }
    }
    if (table_name === 'department1') {
      console.log('in department1');
      if (
        typeof this.data.department1[istep].topic[itopic].detail[isub].id !==
        'string'
      ) {
        this.data_delete.department1DetailDelete.push(
          this.data.department1[istep].topic[itopic].detail[isub].id
        );
      }
      if (this.data.department1[istep].topic.length > 1) {
        console.log('in topic length > 1');
        if (this.data.department1[istep].topic[itopic].detail.length > 1) {
          console.log('in topic detail length > 1');
          this.data.department1[istep].topic[itopic].detail.splice(isub, 1);
        } else if (
          this.data.department1[istep].topic[itopic].detail.length === 1
        ) {
          if (
            typeof this.data.department1[istep].topic[itopic].id !== 'string'
          ) {
            this.data_delete.department1TopicDelete.push(
              this.data.department1[istep].topic[itopic].id
            );
          }
          console.log('in topic detail length = 1');
          this.data.department1[istep].topic.splice(itopic, 1);
        }
      } else if (this.data.department1[istep].topic.length === 1) {
        console.log('in topic length = 1');
        if (this.data.department1[istep].topic[itopic].detail.length > 1) {
          console.log('in topic detail length > 1');
          this.data.department1[istep].topic[itopic].detail.splice(isub, 1);
        } else if (
          fixedBody.includes(istep) &&
          this.data.department1[istep].topic[itopic].detail.length === 1 &&
          this.data.department1.length === 1
        ) {
          console.log(
            'in topic detail length = 1 & first',
            this.data.department1.length
          );
          if (
            typeof this.data.department1[istep].topic[itopic].id !== 'string'
          ) {
            this.data_delete.department1TopicDelete.push(
              this.data.department1[istep].topic[itopic].id
            );
          }
          if (typeof this.data.department1[istep].id !== 'string') {
            this.data_delete.department1Delete.push(
              this.data.department1[istep].id
            );
          }
          this.data.department1.splice(istep, 1);
          this.addRow('department1');
        } else if (
          this.data.department1[istep].topic[itopic].detail.length === 1
        ) {
          console.log('in topic detail length = 1');
          if (
            typeof this.data.department1[istep].topic[itopic].id !== 'string'
          ) {
            this.data_delete.department1TopicDelete.push(
              this.data.department1[istep].topic[itopic].id
            );
          }
          if (typeof this.data.department1[istep].id !== 'string') {
            this.data_delete.department1Delete.push(
              this.data.department1[istep].id
            );
          }
          this.data.department1.splice(istep, 1);
        }
      }
    }
    if (table_name === 'department2') {
      console.log('in department2');
      if (
        typeof this.data.department2[istep].topic[itopic].detail[isub].id !==
        'string'
      ) {
        this.data_delete.department2DetailDelete.push(
          this.data.department2[istep].topic[itopic].detail[isub].id
        );
      }
      if (this.data.department2[istep].topic.length > 1) {
        console.log('in topic length > 1');
        if (this.data.department2[istep].topic[itopic].detail.length > 1) {
          console.log('in topic detail length > 1');
          this.data.department2[istep].topic[itopic].detail.splice(isub, 1);
        } else if (
          this.data.department2[istep].topic[itopic].detail.length === 1
        ) {
          if (
            typeof this.data.department2[istep].topic[itopic].id !== 'string'
          ) {
            this.data_delete.department2TopicDelete.push(
              this.data.department2[istep].topic[itopic].id
            );
          }
          console.log('in topic detail length = 1');
          this.data.department2[istep].topic.splice(itopic, 1);
        }
      } else if (this.data.department2[istep].topic.length === 1) {
        console.log('in topic length = 1');
        if (this.data.department2[istep].topic[itopic].detail.length > 1) {
          console.log('in topic detail length > 1');
          this.data.department2[istep].topic[itopic].detail.splice(isub, 1);
        } else if (
          fixedBody.includes(istep) &&
          this.data.department2[istep].topic[itopic].detail.length === 1 &&
          this.data.department2.length === 1
        ) {
          console.log(
            'in topic detail length = 1 & first',
            this.data.department2.length
          );
          if (
            typeof this.data.department2[istep].topic[itopic].id !== 'string'
          ) {
            this.data_delete.department2TopicDelete.push(
              this.data.department2[istep].topic[itopic].id
            );
          }
          if (typeof this.data.department2[istep].id !== 'string') {
            this.data_delete.department2Delete.push(
              this.data.department2[istep].id
            );
          }
          this.data.department2.splice(istep, 1);
          this.addRow('department2');
        } else if (
          this.data.department2[istep].topic[itopic].detail.length === 1
        ) {
          console.log('in topic detail length = 1');
          if (
            typeof this.data.department2[istep].topic[itopic].id !== 'string'
          ) {
            this.data_delete.department2TopicDelete.push(
              this.data.department2[istep].topic[itopic].id
            );
          }
          if (typeof this.data.department2[istep].id !== 'string') {
            this.data_delete.department2Delete.push(
              this.data.department2[istep].id
            );
          }
          this.data.department2.splice(istep, 1);
        }
      }
    }
    if (table_name === 'rule') {
      console.log('in rule');
      if (
        typeof this.data.rule[istep].topic[itopic].detail[isub].id !== 'string'
      ) {
        this.data_delete.ruleDetailDelete.push(
          this.data.rule[istep].topic[itopic].detail[isub].id
        );
      }
      if (this.data.rule[istep].topic.length > 1) {
        console.log('in topic length > 1');
        if (this.data.rule[istep].topic[itopic].detail.length > 1) {
          console.log('in topic detail length > 1');
          this.data.rule[istep].topic[itopic].detail.splice(isub, 1);
        } else if (this.data.rule[istep].topic[itopic].detail.length === 1) {
          if (typeof this.data.rule[istep].topic[itopic].id !== 'string') {
            this.data_delete.ruleTopicDelete.push(
              this.data.rule[istep].topic[itopic].id
            );
          }
          console.log('in topic detail length = 1');
          this.data.rule[istep].topic.splice(itopic, 1);
        }
      } else if (this.data.rule[istep].topic.length === 1) {
        console.log('in topic length = 1');
        if (this.data.rule[istep].topic[itopic].detail.length > 1) {
          console.log('in topic detail length > 1');
          this.data.rule[istep].topic[itopic].detail.splice(isub, 1);
        } else if (
          fixedBody.includes(istep) &&
          this.data.rule[istep].topic[itopic].detail.length === 1 &&
          this.data.rule.length === 1
        ) {
          console.log(
            'in topic detail length = 1 & first',
            this.data.rule.length
          );
          if (typeof this.data.rule[istep].topic[itopic].id !== 'string') {
            this.data_delete.ruleTopicDelete.push(
              this.data.rule[istep].topic[itopic].id
            );
          }
          if (typeof this.data.rule[istep].id !== 'string') {
            this.data_delete.ruleDelete.push(this.data.rule[istep].id);
          }
          this.data.rule.splice(istep, 1);
          this.addRow('rule');
        } else if (this.data.rule[istep].topic[itopic].detail.length === 1) {
          console.log('in topic detail length = 1');
          if (typeof this.data.rule[istep].topic[itopic].id !== 'string') {
            this.data_delete.ruleTopicDelete.push(
              this.data.rule[istep].topic[itopic].id
            );
          }
          if (typeof this.data.rule[istep].id !== 'string') {
            this.data_delete.ruleDelete.push(this.data.rule[istep].id);
          }
          this.data.rule.splice(istep, 1);
        }
      }
    }

    if (table_name === 'law') {
      console.log('in law');
      if (
        typeof this.data.law[istep].topic[itopic].detail[isub].id !== 'string'
      ) {
        this.data_delete.lawDetailDelete.push(
          this.data.law[istep].topic[itopic].detail[isub].id
        );
      }
      if (this.data.law[istep].topic.length > 1) {
        console.log('in topic length > 1');
        if (this.data.law[istep].topic[itopic].detail.length > 1) {
          console.log('in topic detail length > 1');
          this.data.law[istep].topic[itopic].detail.splice(isub, 1);
        } else if (this.data.law[istep].topic[itopic].detail.length === 1) {
          if (typeof this.data.law[istep].topic[itopic].id !== 'string') {
            this.data_delete.lawTopicDelete.push(
              this.data.law[istep].topic[itopic].id
            );
          }
          console.log('in topic detail length = 1');
          this.data.law[istep].topic.splice(itopic, 1);
        }
      } else if (this.data.law[istep].topic.length === 1) {
        console.log('in topic length = 1');
        if (this.data.law[istep].topic[itopic].detail.length > 1) {
          console.log('in topic detail length > 1');
          this.data.law[istep].topic[itopic].detail.splice(isub, 1);
        } else if (
          fixedBody.includes(istep) &&
          this.data.law[istep].topic[itopic].detail.length === 1 &&
          this.data.law.length === 1
        ) {
          console.log(
            'in topic detail length = 1 & first',
            this.data.law.length
          );
          if (typeof this.data.law[istep].topic[itopic].id !== 'string') {
            this.data_delete.lawTopicDelete.push(
              this.data.law[istep].topic[itopic].id
            );
          }
          if (typeof this.data.law[istep].id !== 'string') {
            this.data_delete.lawDelete.push(this.data.law[istep].id);
          }
          this.data.law.splice(istep, 1);
          this.addRow('law');
        } else if (this.data.law[istep].topic[itopic].detail.length === 1) {
          console.log('in topic detail length = 1');
          if (typeof this.data.law[istep].topic[itopic].id !== 'string') {
            this.data_delete.lawTopicDelete.push(
              this.data.law[istep].topic[itopic].id
            );
          }
          if (typeof this.data.law[istep].id !== 'string') {
            this.data_delete.lawDelete.push(this.data.law[istep].id);
          }
          this.data.law.splice(istep, 1);
        }
      }
    }
    console.log(this.data_delete);
  }

  typeOf(value) {
    return typeof value;
  }

  check_id() {
    for (let index = 0; index < this.data.account.length; index++) {
      const Group1 = this.data.account[index].topic;
      for (let j = 0; j < Group1.length; j++) {
        const check_id = Group1[j].id;
        console.log('id', typeof check_id);
        if (typeof check_id === 'string') {
          console.log('id is String', check_id);
          console.log('index account', index, j);
          this.data.account[index].topic[j].id = null;
        }
      }
    }
    for (let index = 0; index < this.data.department1.length; index++) {
      const Group1 = this.data.department1[index].topic;
      for (let j = 0; j < Group1.length; j++) {
        const check_id = Group1[j].id;
        console.log('id', typeof check_id);
        if (typeof check_id === 'string') {
          console.log('id is String', check_id);
          console.log('index department1', index, j);
          this.data.department1[index].topic[j].id = null;
        }
      }
    }
    for (let index = 0; index < this.data.department2.length; index++) {
      const Group1 = this.data.department2[index].topic;
      for (let j = 0; j < Group1.length; j++) {
        const check_id = Group1[j].id;
        console.log('id', typeof check_id);
        if (typeof check_id === 'string') {
          console.log('id is String', check_id);
          console.log('index department2', index, j);
          this.data.department2[index].topic[j].id = null;
        }
      }
    }
    for (let index = 0; index < this.data.rule.length; index++) {
      const Group1 = this.data.rule[index].topic;
      for (let j = 0; j < Group1.length; j++) {
        const check_id = Group1[j].id;
        console.log('id', typeof check_id);
        if (typeof check_id === 'string') {
          console.log('id is String', check_id);
          console.log('index rule', index, j);
          this.data.rule[index].topic[j].id = null;
        }
      }
    }
    for (let index = 0; index < this.data.law.length; index++) {
      const Group1 = this.data.law[index].topic;
      for (let j = 0; j < Group1.length; j++) {
        const check_id = Group1[j].id;
        console.log('id', typeof check_id);
        if (typeof check_id === 'string') {
          console.log('id is String', check_id);
          console.log('index law', index, j);
          this.data.law[index].topic[j].id = null;
        }
      }
    }
  }

  po_datasend() {
    const dataSend = {
      requestId: localStorage.getItem('requestId'),
      status: false,
      account: [],
      department1: [],
      department2: [],
      rule: [],
      law: [],
    };
    for (let index = 0; index < this.data.account.length; index++) {
      const Group1 = this.data.account[index].topic;
      for (let j = 0; j < Group1.length; j++) {
        const second = this.data.account[index].topic[j].detail;
        for (let idetail = 0; idetail < second.length; idetail++) {
          if (
            this.data.account[index].topic[j].detail[idetail].buCommentId !==
            null
          ) {
            const addPoComment = {
              buCommentId: this.data.account[index].topic[j].detail[idetail]
                .buCommentId,
              poComment: this.data.account[index].topic[j].detail[idetail]
                .poComment,
            };
            dataSend.account.push(addPoComment);
          }
        }
      }
    }
    for (let index = 0; index < this.data.department1.length; index++) {
      const Group1 = this.data.department1[index].topic;
      for (let j = 0; j < Group1.length; j++) {
        const second = this.data.department1[index].topic[j].detail;
        for (let idetail = 0; idetail < second.length; idetail++) {
          const addPoComment = {
            buCommentId: this.data.department1[index].topic[j].detail[idetail]
              .buCommentId,
            poComment: this.data.department1[index].topic[j].detail[idetail]
              .poComment,
          };
          dataSend.department1.push(addPoComment);
        }
      }
    }
    for (let index = 0; index < this.data.department2.length; index++) {
      const Group1 = this.data.department2[index].topic;
      for (let j = 0; j < Group1.length; j++) {
        const second = this.data.department2[index].topic[j].detail;
        for (let idetail = 0; idetail < second.length; idetail++) {
          const addPoComment = {
            buCommentId: this.data.department2[index].topic[j].detail[idetail]
              .buCommentId,
            poComment: this.data.department2[index].topic[j].detail[idetail]
              .poComment,
          };
          dataSend.department2.push(addPoComment);
        }
      }
    }
    for (let index = 0; index < this.data.rule.length; index++) {
      const Group1 = this.data.rule[index].topic;
      for (let j = 0; j < Group1.length; j++) {
        const second = this.data.rule[index].topic[j].detail;
        for (let idetail = 0; idetail < second.length; idetail++) {
          const addPoComment = {
            buCommentId: this.data.rule[index].topic[j].detail[idetail]
              .buCommentId,
            poComment: this.data.rule[index].topic[j].detail[idetail].poComment,
          };
          dataSend.rule.push(addPoComment);
        }
      }
    }
    for (let index = 0; index < this.data.law.length; index++) {
      const Group1 = this.data.law[index].topic;
      for (let j = 0; j < Group1.length; j++) {
        const second = this.data.law[index].topic[j].detail;
        for (let idetail = 0; idetail < second.length; idetail++) {
          const addPoComment = {
            buCommentId: this.data.law[index].topic[j].detail[idetail]
              .buCommentId,
            poComment: this.data.law[index].topic[j].detail[idetail].poComment,
          };
          dataSend.law.push(addPoComment);
        }
      }
    }

    const check_account = Object.values(dataSend.account).every(
      (value) => value.poComment !== '' && value.poComment !== null
    );
    const check_department1 = Object.values(dataSend.department1).every(
      (value) => value.poComment !== '' && value.poComment !== null
    );
    const check_department2 = Object.values(dataSend.department2).every(
      (value) => value.poComment !== '' && value.poComment !== null
    );
    const check_law = Object.values(dataSend.law).every(
      (value) => value.poComment !== '' && value.poComment !== null
    );
    const check_rule = Object.values(dataSend.rule).every(
      (value) => value.poComment !== '' && value.poComment !== null
    );
    // some(value => value.poComment === '');
    console.log('check_policy', check_account);
    console.log('check_department1', check_department1);
    console.log('check_department2', check_department2);
    console.log('check_law', check_law);
    console.log('check_rule', check_rule);
    if (
      check_account === true &&
      check_department1 === true &&
      check_department2 === true &&
      check_law === true &&
      check_rule === true
    ) {
      dataSend.status = true;
    }
    console.log('dataSend po>>', dataSend);
    return dataSend;
  }

  pc_datasend() {
    const dataSend = {
      status: false,
      requestId: localStorage.getItem('requestId'),
      account: [],
      department1: [],
      department2: [],
      rule: [],
      law: [],
    };
    for (let index = 0; index < this.data.account.length; index++) {
      const Group1 = this.data.account[index].topic;
      for (let j = 0; j < Group1.length; j++) {
        const second = this.data.account[index].topic[j].detail;
        for (let idetail = 0; idetail < second.length; idetail++) {
          if (
            this.data.account[index].topic[j].detail[idetail].buCommentId !==
            null
          ) {
            const addPoComment = {
              buCommentId: this.data.account[index].topic[j].detail[idetail]
                .buCommentId,
              pcComment: this.data.account[index].topic[j].detail[idetail]
                .pcComment,
              pcExtraComment: this.data.account[index].topic[j].detail[idetail]
                .pcExtraComment,
              pcIssue: this.data.account[index].topic[j].detail[idetail]
                .pcIssue,
            };
            dataSend.account.push(addPoComment);
          }
        }
      }
    }
    for (let index = 0; index < this.data.department1.length; index++) {
      const Group1 = this.data.department1[index].topic;
      for (let j = 0; j < Group1.length; j++) {
        const second = this.data.department1[index].topic[j].detail;
        for (let idetail = 0; idetail < second.length; idetail++) {
          // 'pcComment': this.data.account[index].topic[j].detail[idetail].pcComment,
          // console.log(this.data.account[index].topic[j].detail);

          const addPoComment = {
            buCommentId: this.data.department1[index].topic[j].detail[idetail]
              .buCommentId,
            pcComment: this.data.department1[index].topic[j].detail[idetail]
              .pcComment,
            pcExtraComment: this.data.department1[index].topic[j].detail[
              idetail
            ].pcExtraComment,
            pcIssue: this.data.department1[index].topic[j].detail[idetail]
              .pcIssue,
          };
          dataSend.department1.push(addPoComment);
        }
      }
    }
    for (let index = 0; index < this.data.department2.length; index++) {
      const Group1 = this.data.department2[index].topic;
      for (let j = 0; j < Group1.length; j++) {
        const second = this.data.department2[index].topic[j].detail;
        for (let idetail = 0; idetail < second.length; idetail++) {
          const addPoComment = {
            buCommentId: this.data.department2[index].topic[j].detail[idetail]
              .buCommentId,
            pcComment: this.data.department2[index].topic[j].detail[idetail]
              .pcComment,
            pcExtraComment: this.data.department2[index].topic[j].detail[
              idetail
            ].pcExtraComment,
            pcIssue: this.data.department2[index].topic[j].detail[idetail]
              .pcIssue,
          };
          dataSend.department2.push(addPoComment);
        }
      }
    }
    for (let index = 0; index < this.data.rule.length; index++) {
      const Group1 = this.data.rule[index].topic;
      for (let j = 0; j < Group1.length; j++) {
        const second = this.data.rule[index].topic[j].detail;
        for (let idetail = 0; idetail < second.length; idetail++) {
          const addPoComment = {
            buCommentId: this.data.rule[index].topic[j].detail[idetail]
              .buCommentId,
            pcComment: this.data.rule[index].topic[j].detail[idetail].pcComment,
            pcExtraComment: this.data.rule[index].topic[j].detail[idetail]
              .pcExtraComment,
            pcIssue: this.data.rule[index].topic[j].detail[idetail].pcIssue,
          };
          dataSend.rule.push(addPoComment);
        }
      }
    }
    for (let index = 0; index < this.data.law.length; index++) {
      const Group1 = this.data.law[index].topic;
      for (let j = 0; j < Group1.length; j++) {
        const second = this.data.law[index].topic[j].detail;
        for (let idetail = 0; idetail < second.length; idetail++) {
          const addPoComment = {
            buCommentId: this.data.law[index].topic[j].detail[idetail]
              .buCommentId,
            pcComment: this.data.law[index].topic[j].detail[idetail].pcComment,
            pcExtraComment: this.data.law[index].topic[j].detail[idetail]
              .pcExtraComment,
            pcIssue: this.data.law[index].topic[j].detail[idetail].pcIssue,
          };
          dataSend.law.push(addPoComment);
        }
      }
    }

    // console.log('department1', dataSend.department1);
    // console.log('department2', dataSend.department2);
    // console.log('law', dataSend.law);
    // console.log('rule', dataSend.rule);

    const check_account_pcIssue = Object.values(dataSend.account).every(
      (value) => value['pcIssue'] !== null && value['pcIssue'] !== ''
    );
    // tslint:disable-next-line: max-line-length
    const check_account_pcExtraComment = Object.values(dataSend.account).every(
      (value) =>
        value['pcExtraComment'] !== null && value['pcExtraComment'] !== ''
    );
    // tslint:disable-next-line: max-line-length
    const check_account_pcComment = Object.values(dataSend.account).every(
      (value) => value['pcComment'] !== null && value['pcComment'] !== ''
    );
    console.log(
      'dataSend PC',
      dataSend.account,
      check_account_pcIssue,
      check_account_pcExtraComment,
      check_account_pcComment
    );
    // tslint:disable-next-line: max-line-length
    const check_department1_pcIssue = Object.values(dataSend.department1).every(
      (value) => value['pcIssue'] !== null && value['pcIssue'] !== ''
    );
    // tslint:disable-next-line: max-line-length
    const check_department1_pcExtraComment = Object.values(
      dataSend.department1
    ).every(
      (value) =>
        value['pcExtraComment'] !== null && value['pcExtraComment'] !== ''
    );
    // tslint:disable-next-line: max-line-length
    const check_department1_pcComment = Object.values(
      dataSend.department1
    ).every(
      (value) => value['pcComment'] !== null && value['pcComment'] !== ''
    );
    // tslint:disable-next-line: max-line-length
    console.log(
      'department1',
      dataSend.department1,
      check_department1_pcIssue,
      check_department1_pcExtraComment,
      check_department1_pcComment
    );
    // tslint:disable-next-line: max-line-length
    const check_department2_pcIssue = Object.values(dataSend.department2).every(
      (value) => value['pcIssue'] !== null && value['pcIssue'] !== ''
    );
    // tslint:disable-next-line: max-line-length
    const check_department2_pcExtraComment = Object.values(
      dataSend.department2
    ).every(
      (value) =>
        value['pcExtraComment'] !== null && value['pcExtraComment'] !== ''
    );
    // tslint:disable-next-line: max-line-length
    const check_department2_pcComment = Object.values(
      dataSend.department2
    ).every(
      (value) => value['pcComment'] !== null && value['pcComment'] !== ''
    );
    // tslint:disable-next-line: max-line-length
    console.log(
      'department2',
      dataSend.department2,
      check_department2_pcIssue,
      check_department2_pcExtraComment,
      check_department2_pcComment
    );
    // tslint:disable-next-line: max-line-length
    const check_law_pcIssue = Object.values(dataSend.law).every(
      (value) => value['pcIssue'] !== null && value['pcIssue'] !== ''
    );
    // tslint:disable-next-line: max-line-length
    const check_law_pcExtraComment = Object.values(dataSend.law).every(
      (value) =>
        value['pcExtraComment'] !== null && value['pcExtraComment'] !== ''
    );
    const check_law_pcComment = Object.values(dataSend.law).every(
      (value) => value['pcComment'] !== null && value['pcComment'] !== ''
    );
    console.log(
      'law',
      dataSend.law,
      check_law_pcIssue,
      check_law_pcExtraComment,
      check_law_pcComment
    );

    const check_rule_pcIssue = Object.values(dataSend.rule).every(
      (value) => value['pcIssue'] !== null && value['pcIssue'] !== ''
    );
    // tslint:disable-next-line: max-line-length
    const check_rule_pcExtraComment = Object.values(dataSend.rule).every(
      (value) =>
        value['pcExtraComment'] !== null && value['pcExtraComment'] !== ''
    );
    const check_rule_pcComment = Object.values(dataSend.rule).every(
      (value) => value['pcComment'] !== null && value['pcComment'] !== ''
    );
    console.log(
      'rule',
      dataSend.rule,
      check_rule_pcIssue,
      check_rule_pcExtraComment,
      check_rule_pcComment
    );

    if (
      check_account_pcIssue === true &&
      check_account_pcExtraComment === true &&
      check_account_pcComment === true &&
      check_department1_pcIssue === true &&
      check_department1_pcExtraComment === true &&
      check_department1_pcComment === true &&
      check_department2_pcIssue === true &&
      check_department2_pcExtraComment === true &&
      check_department2_pcComment === true &&
      check_law_pcIssue === true &&
      check_law_pcExtraComment === true &&
      check_law_pcComment === true &&
      check_rule_pcIssue === true &&
      check_rule_pcExtraComment === true &&
      check_rule_pcComment === true
    ) {
      dataSend.status = true;
    }
    return dataSend;
  }

  savedData() {
    // console.log(this.checkValue(this.rolename, this.data));

    // this.complianceService.status_state = false;
    if (this.role !== 'PO' && this.role !== 'PC') {
      // if (this.checkValue(this.rolename, this.data) === true) {
      //   console.log('value:::', this.checkValue(this.rolename, this.data));
      // this.checkMasterId();
      this.check_id();
      console.log('before', this.data);
      const dataSend = Object.assign(this.data, this.data_delete);
      dataSend['requestId'] = localStorage.getItem('requestId');
      console.log('after', this.data);

      console.log('merge', dataSend);

      this.status_loading = true;
      this.complianceService.updateCompliance(dataSend).subscribe((res) => {
        console.log(res);
        this.api_response = res;
        if (this.api_response.status === 'success') {
          if (this.getlevel === 'พนักงาน') {
            this.alertMessage = 'บันทึกความเห็นของสายงานกำกับกฎเกณฑ์และกฎหมายเรียบร้อย';
          } else {
            this.alertMessage = 'บันทึกความเห็นของ' + this.getlevel + 'เรียบร้อย';
          }

          this.saveDraftstatus = 'success';
          setTimeout(() => {
            this.saveDraftstatus = 'fail';
          }, 3000);
          // this.GetDropDown();
          // this.getDetail(localStorage.getItem('requestId'));
          console.log('update success');
          this.ngOnInit();
          // this.status_loading = false;
        } else {
          // alert(this.api_response.message);
          Swal.fire({
            title: 'error',
            text: res['message'],
            icon: 'error',
          });
          console.log(this.api_response);
          this.status_loading = false;
        }
      }, err => {
        this.status_loading = false;
        console.log(err);
      });
      // } else {
      //   alert('กรุณากรอกข้อมูลให้ครบถ้วน');
      // }
    }
    if (this.role === 'PO') {
      const dataSend = this.po_datasend();
      console.log('in savedData', dataSend);
      this.status_loading = true;
      this.complianceService.updatePOCompliance(dataSend).subscribe((res) => {
        this.api_response = res;
        if (this.api_response.status === 'success') {
          this.alertMessage = 'บันทึกความเห็นของ' + this.Getname + 'เรียบร้อย';
          this.saveDraftstatus = 'success';
          // this.status_loading = false;
          this.GetDropDown();
          this.getDetail(localStorage.getItem('requestId'));
          console.log('update success');
        } else {
          // alert(this.api_response.message);
          Swal.fire({
            title: 'error',
            text: res['message'],
            icon: 'error',
          });
          console.log(this.api_response);
          this.status_loading = false;
        }
      }, err => {
        this.status_loading = false;
        console.log(err);
      });
    }

    if (this.role === 'PC') {
      const dataSend = this.pc_datasend();
      console.log('in savedData', dataSend);
      this.status_loading = true;
      this.complianceService.updatePCCompliance(dataSend).subscribe((res) => {
        this.api_response = res;
        if (this.api_response.status === 'success') {
          this.alertMessage = 'บันทึกความเห็นของคณะกรรมการผลิตภัณฑ์เรียบร้อย';
          this.saveDraftstatus = 'success';
          this.getDetail(localStorage.getItem('requestId'));
          this.status_loading = false;
          console.log('update success');
        } else {
          // alert(this.api_response.message);
          Swal.fire({
            title: 'error',
            text: res['message'],
            icon: 'error',
          });
          console.log(this.api_response);
          this.status_loading = false;
        }
      }, err => {
        this.status_loading = false;
        console.log(err);
      });
    }
    this.saveDraftstatus_State = false;
  }

  validateCommentBu() {
    console.log('this.validateBu', this.data);
    // account>>topic>>detail.comply/review
    if (localStorage.getItem('role') === 'ฝ่ายกำกับระเบียบและการปฏิบัติงาน') {
      this.data.account.forEach((process, i) => {
        process.topic.forEach((sub) => {
          sub.detail.forEach((subRisk) => {
            if (subRisk.comply === '' || subRisk.comply === null) {
              this.validateBu = this.validateBu + 1;
            }
            if (subRisk.review === '' || subRisk.review === null) {
              this.validateBu = this.validateBu + 1;
            }
          });
        });
      });
    }

    // department1>>topic>>detail.review/comply
    else if (localStorage.getItem('role') === 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ธุรกิจ 1') {
      this.data.department1.forEach((process, i) => {
        process.topic.forEach((sub) => {
          sub.detail.forEach((subTopic) => {
            if (subTopic.comply === '' || subTopic.comply === null) {
              this.validateBu = this.validateBu + 1;
            }
            if (subTopic.review === '' || subTopic.review === null) {
              this.validateBu = this.validateBu + 1;
            }
          });
        });
      });
    }

    // department1>>topic>>detail.review/comply
    else if (localStorage.getItem('role') === 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ธุรกิจ 2') {
      this.data.department2.forEach((process, i) => {
        process.topic.forEach((sub) => {
          sub.detail.forEach((subTopic) => {
            if (subTopic.comply === '' || subTopic.comply === null) {
              this.validateBu = this.validateBu + 1;
            }
            if (subTopic.review === '' || subTopic.review === null) {
              this.validateBu = this.validateBu + 1;
            }
          });
        });
      });
    }
    // rule>>topic>>detail.review/comply
    else if (localStorage.getItem('role') === 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ทางการ') {
      this.data.rule.forEach((process, i) => {
        process.topic.forEach((sub) => {
          sub.detail.forEach((subTopic) => {
            if (subTopic.comply === '' || subTopic.comply === null) {
              this.validateBu = this.validateBu + 1;
            }
            if (subTopic.review === '' || subTopic.review === null) {
              this.validateBu = this.validateBu + 1;
            }
          });
        });
      });
    }
    // law>>topic>>detail.review/comply
    else if (localStorage.getItem('role') === 'ฝ่ายนิติการ') {
      this.data.law.forEach((process, i) => {
        process.topic.forEach((sub) => {
          sub.detail.forEach((subTopic) => {
            if (subTopic.comply === '' || subTopic.comply === null) {
              this.validateBu = this.validateBu + 1;
            }
            if (subTopic.review === '' || subTopic.review === null) {
              this.validateBu = this.validateBu + 1;
            }
          });
        });
      });
    }
    console.log("BU::", this.validateBu)
    return this.validateBu;
  }

  sendWork() {
    this.saveDraftstatus_State = false;
    // this.complianceService.status_state = false;
    this.validateCommentBu();
    if (
      this.role !== 'PC' &&
      this.role !== 'PO' &&
      (this.role === 'ComplianceValidateTrue' || this.role === 'Compliance')
    ) {

      // if (this.checkValue(this.rolename, this.data) === true) {
      console.log('value:::', this.checkValue(this.rolename, this.data));
      // this.checkMasterId();
      this.check_id();
      console.log('before', this.data);
      const dataSend = Object.assign(this.data, this.data_delete);
      var EmptyRow = true;
      dataSend['requestId'] = localStorage.getItem('requestId');
      console.log('after', this.data);
      console.log('merge', dataSend);
      if (this.rolename == 'ฝ่ายกำกับระเบียบและการปฏิบัติงาน' && dataSend.account.length == 0) {
        EmptyRow = false
      }
      else if (this.rolename == 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ธุรกิจ 1' && dataSend.department1.length == 0) {
        EmptyRow = false
      }
      else if (this.rolename == 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ธุรกิจ 2' && dataSend.department2.length == 0) {
        EmptyRow = false
      }
      else if (this.rolename == 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ทางการ' && dataSend.rule.length == 0) {
        EmptyRow = false
      }
      else if (this.rolename == 'ฝ่ายนิติการ' && dataSend.law.length == 0) {
        EmptyRow = false
      }
      // this.status_loading = true;
      if (this.validateBu > 0 || this.checkValue(this.rolename, this.data) == false) {
        alert('กรุณากรอกข้อมูลตาราง ' + localStorage.getItem('role') + ' ให้ครบถ้วน');
        this.validateBu = 0;
      } else if (EmptyRow == false && (this.getlevel !== 'ฝ่าย' || this.getlevel !== 'กลุ่ม' || this.getlevel !== 'สาย')) {
        alert('กรุณาเพิ่มข้อมูลตาราง ' + localStorage.getItem('role') + ' อย่างน้อย 1 ข้อมูล');
      } else {
        this.complianceService.updateCompliance(dataSend).subscribe((res) => {
          console.log(res);
          this.api_response = res;
          if ((this.api_response['status'] == 'success' && EmptyRow) || (this.getlevel == 'ฝ่าย' || this.getlevel == 'กลุ่ม' || this.getlevel == 'สาย')) {
            this.complianceService
              .submit(localStorage.getItem('requestId'))
              .subscribe(
                (res) => {
                  console.log(res);
                  if (res['status'] === 'success') {
                    Swal.fire({
                      title: 'success',
                      text: 'ส่งงานเรียบร้อย',
                      icon: 'success',
                      showConfirmButton: false,
                      timer: 3000,
                    }).then(() => {
                      // localStorage.setItem('requestId', '');
                      this.router.navigate(['/dashboard1']);
                    })


                    // this.router.navigate(['/dashboard1']);
                    // this.router.navigate(['/dashboard1']);
                  } else {
                    if (
                      res['message'] === 'กรุณาไปให้ความเห็นหน้า ความเห็น / ส่งงาน'
                    ) {
                      this.router.navigate(['/comment']);
                    } else if (res['message'] === 'ยังบันทึกผู้ดำเนินการไม่ครบ') {
                      this.message_error = res['message'];
                      this.list_error = res['data'];
                      document
                        .getElementById('opendModalSendworkCompliance')
                        .click();
                    } else {
                      Swal.fire({
                        title: 'error',
                        text: res['message'],
                        icon: 'error',
                        // showConfirmButton: false,
                        // timer: 1500,
                      }).then(() => {
                        this.POcommentConsider(); // this should execute now
                      });
                    }
                  }
                },
                (err) => {
                  console.log('>>>>>', err);
                  Swal.fire({
                    title: 'error',
                    text: err,
                    icon: 'error',
                    showConfirmButton: false,
                    timer: 3000,
                  });
                }
              );
          } else {
            Swal.fire({
              title: 'error',
              text: this.api_response['message'],
              icon: 'error',
              showConfirmButton: false,
              timer: 3000,
            });
          }
        }, err => {
          this.status_loading = false;
          console.log(err);
        })
      }
    } else if (this.role === 'PO') {

      const arr = this.po_datasend()
      const ss = this.CheckValueCompliance(arr);

      if (ss.length > 0) {
        alert('กรุณาให้ความเห็นตาราง ' + ss + ' ให้ครบ')
        arr.status = false
      }
      else {
        console.log('ss')
        this.complianceService.updatePOCompliance(arr).subscribe((res) => {
          this.api_response = res;
          console.log(res)
          if (this.api_response.status === 'success' || (this.getlevel == 'ฝ่าย' || this.getlevel == 'กลุ่ม' || this.getlevel == 'สาย')) {
            this.Submit.sendDocument_PO(
              localStorage.getItem('requestId'),
              'Compliance'
            ).subscribe(
              (res) => {
                console.log(res);
                if (res['status'] === 'success') {
                  // alert('ส่งงานเรียบร้อย');
                  Swal.fire({
                    title: 'success',
                    text: 'ส่งงานเรียบร้อย',
                    icon: 'success',
                    showConfirmButton: false,
                    timer: 3000,
                  }).then(() => {
                    // localStorage.setItem('requestId', '');
                    this.router.navigate(['/dashboard1']);
                  })
                } else {
                  // alert(res['message']);
                  if (res['message'] === 'กรุณาไปให้ความเห็นหน้า ความเห็น / ส่งงาน') {
                    this.router.navigate(['/comment']);
                  } else {
                    Swal.fire({
                      title: 'error',
                      text: res['message'],
                      icon: 'error',
                      showConfirmButton: false,
                      timer: 3000,
                    });
                  }
                }
              },
              (err) => {
                console.log(err);
                alert('เกิดข้อผิดพลาดไม่สามารถส่งงานได้');
              }
            );
          } else {
            Swal.fire({
              title: 'error',
              text: this.api_response['message'],
              icon: 'error',
              showConfirmButton: false,
              timer: 3000,
            });
          }
        }, err => {
          this.status_loading = false;
          console.log(err);
        })
      }
      console.log('arr::', arr);



    } else if (this.role === 'PC') {
      const arr = this.checkPC()
      if (arr.status.length > 0) {
        alert('กรุณาให้ความเห็นตาราง ' + arr.status + ' ให้ครบ')
        // arr.data.status = false
        arr.data = {
          status: false,
          requestId: localStorage.getItem('requestId'),
          account: [],
          department1: [],
          department2: [],
          rule: [],
          law: [],
        };
      }
      else {
        this.complianceService.updatePCCompliance(arr.data).subscribe((res) => {
          this.api_response = res;
          if (this.api_response.status === 'success') {
            this.Submit.sendComment_PC(localStorage.getItem('requestId')).subscribe(
              (res) => {
                console.log(res);
                if (res['status'] === 'success') {
                  // alert('ส่งงานเรียบร้อย');
                  Swal.fire({
                    title: 'success',
                    text: 'ส่งงานเรียบร้อย',
                    icon: 'success',
                    showConfirmButton: false,
                    timer: 3000,
                  }).then(() => {
                    // localStorage.setItem('requestId', '');
                    this.router.navigate(['/dashboard1']);
                  })
                } else {
                  // alert(res['message']);
                  Swal.fire({
                    title: 'error',
                    text: res['message'],
                    icon: 'error',
                    showConfirmButton: false,
                    timer: 3000,
                  });
                }
              },
              (err) => {
                console.log(err);
                alert('เกิดข้อผิดพลาดไม่สามารถส่งงานได้');
              }
            );
          } else {
            Swal.fire({
              title: 'error',
              text: res['message'],
              icon: 'error',
              showConfirmButton: false,
              timer: 3000,
            });
          }
        }, err => {
          this.status_loading = false;
          console.log(err);
        })
      }

    } else {
      Swal.fire({
        title: 'error',
        text: 'ไม่สามารถส่งงานได้',
        icon: 'error',
        showConfirmButton: false,
        timer: 3000,
      });
    }
  }

  CheckValueCompliance(arr) {
    const StatusArray = []
    for (let i = 0; i < arr.account.length; i++) {
      if (arr.account[i].poComment == '' || arr.account[i].poComment == null) {
        StatusArray.push('ฝ่ายกำกับระเบียบและการปฏิบัติงาน')
        break;
      }
    }

    for (let i = 0; i < arr.department1.length; i++) {
      if (arr.department1[i].poComment == '' || arr.department1[i].poComment == null) {
        StatusArray.push('ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ธุรกิจ 1')
        break;
      }
    }

    for (let i = 0; i < arr.department2.length; i++) {
      if (arr.department2[i].poComment == '' || arr.department2[i].poComment == null) {
        StatusArray.push('ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ธุรกิจ 2')
        break;
      }
    }

    for (let i = 0; i < arr.law.length; i++) {
      if (arr.law[i].poComment == '' || arr.law[i].poComment == null) {
        StatusArray.push('ฝ่ายนิติการ')
        break;
      }
    }

    for (let i = 0; i < arr.rule.length; i++) {
      if (arr.rule[i].poComment == '' || arr.rule[i].poComment == null) {
        StatusArray.push('ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ทางการ')
        break;
      }
    }

    if (StatusArray.length == 0) {
      arr.status = true;
    }
    return StatusArray;
  }
  checkPC() {
    const dataSend = {
      status: false,
      requestId: localStorage.getItem('requestId'),
      account: [],
      department1: [],
      department2: [],
      rule: [],
      law: [],
    };
    for (let index = 0; index < this.data.account.length; index++) {
      const Group1 = this.data.account[index].topic;
      for (let j = 0; j < Group1.length; j++) {
        const second = this.data.account[index].topic[j].detail;
        for (let idetail = 0; idetail < second.length; idetail++) {
          if (
            this.data.account[index].topic[j].detail[idetail].buCommentId !==
            null
          ) {
            const addPoComment = {
              buCommentId: this.data.account[index].topic[j].detail[idetail]
                .buCommentId,
              pcComment: this.data.account[index].topic[j].detail[idetail]
                .pcComment,
              pcExtraComment: this.data.account[index].topic[j].detail[idetail]
                .pcExtraComment,
              pcIssue: this.data.account[index].topic[j].detail[idetail]
                .pcIssue,
            };
            dataSend.account.push(addPoComment);
          }
        }
      }
    }
    for (let index = 0; index < this.data.department1.length; index++) {
      const Group1 = this.data.department1[index].topic;
      for (let j = 0; j < Group1.length; j++) {
        const second = this.data.department1[index].topic[j].detail;
        for (let idetail = 0; idetail < second.length; idetail++) {
          // 'pcComment': this.data.account[index].topic[j].detail[idetail].pcComment,
          // console.log(this.data.account[index].topic[j].detail);

          const addPoComment = {
            buCommentId: this.data.department1[index].topic[j].detail[idetail]
              .buCommentId,
            pcComment: this.data.department1[index].topic[j].detail[idetail]
              .pcComment,
            pcExtraComment: this.data.department1[index].topic[j].detail[
              idetail
            ].pcExtraComment,
            pcIssue: this.data.department1[index].topic[j].detail[idetail]
              .pcIssue,
          };
          dataSend.department1.push(addPoComment);
        }
      }
    }
    for (let index = 0; index < this.data.department2.length; index++) {
      const Group1 = this.data.department2[index].topic;
      for (let j = 0; j < Group1.length; j++) {
        const second = this.data.department2[index].topic[j].detail;
        for (let idetail = 0; idetail < second.length; idetail++) {
          const addPoComment = {
            buCommentId: this.data.department2[index].topic[j].detail[idetail]
              .buCommentId,
            pcComment: this.data.department2[index].topic[j].detail[idetail]
              .pcComment,
            pcExtraComment: this.data.department2[index].topic[j].detail[
              idetail
            ].pcExtraComment,
            pcIssue: this.data.department2[index].topic[j].detail[idetail]
              .pcIssue,
          };
          dataSend.department2.push(addPoComment);
        }
      }
    }
    for (let index = 0; index < this.data.rule.length; index++) {
      const Group1 = this.data.rule[index].topic;
      for (let j = 0; j < Group1.length; j++) {
        const second = this.data.rule[index].topic[j].detail;
        for (let idetail = 0; idetail < second.length; idetail++) {
          const addPoComment = {
            buCommentId: this.data.rule[index].topic[j].detail[idetail]
              .buCommentId,
            pcComment: this.data.rule[index].topic[j].detail[idetail].pcComment,
            pcExtraComment: this.data.rule[index].topic[j].detail[idetail]
              .pcExtraComment,
            pcIssue: this.data.rule[index].topic[j].detail[idetail].pcIssue,
          };
          dataSend.rule.push(addPoComment);
        }
      }
    }
    for (let index = 0; index < this.data.law.length; index++) {
      const Group1 = this.data.law[index].topic;
      for (let j = 0; j < Group1.length; j++) {
        const second = this.data.law[index].topic[j].detail;
        for (let idetail = 0; idetail < second.length; idetail++) {
          const addPoComment = {
            buCommentId: this.data.law[index].topic[j].detail[idetail]
              .buCommentId,
            pcComment: this.data.law[index].topic[j].detail[idetail].pcComment,
            pcExtraComment: this.data.law[index].topic[j].detail[idetail]
              .pcExtraComment,
            pcIssue: this.data.law[index].topic[j].detail[idetail].pcIssue,
          };
          dataSend.law.push(addPoComment);
        }
      }
    }

    // console.log('department1', dataSend.department1);
    // console.log('department2', dataSend.department2);
    // console.log('law', dataSend.law);
    // console.log('rule', dataSend.rule);

    const check_account_pcIssue = Object.values(dataSend.account).every(
      (value) => value['pcIssue'] !== null && value['pcIssue'] !== ''
    );
    // tslint:disable-next-line: max-line-length
    const check_account_pcExtraComment = Object.values(dataSend.account).every(
      (value) =>
        value['pcExtraComment'] !== null && value['pcExtraComment'] !== ''
    );
    // tslint:disable-next-line: max-line-length
    const check_account_pcComment = Object.values(dataSend.account).every(
      (value) => value['pcComment'] !== null && value['pcComment'] !== ''
    );
    console.log(
      'dataSend PC',
      dataSend.account,
      check_account_pcIssue,
      check_account_pcExtraComment,
      check_account_pcComment
    );
    // tslint:disable-next-line: max-line-length
    const check_department1_pcIssue = Object.values(dataSend.department1).every(
      (value) => value['pcIssue'] !== null && value['pcIssue'] !== ''
    );
    // tslint:disable-next-line: max-line-length
    const check_department1_pcExtraComment = Object.values(
      dataSend.department1
    ).every(
      (value) =>
        value['pcExtraComment'] !== null && value['pcExtraComment'] !== ''
    );
    // tslint:disable-next-line: max-line-length
    const check_department1_pcComment = Object.values(
      dataSend.department1
    ).every(
      (value) => value['pcComment'] !== null && value['pcComment'] !== ''
    );
    // tslint:disable-next-line: max-line-length
    console.log(
      'department1',
      dataSend.department1,
      check_department1_pcIssue,
      check_department1_pcExtraComment,
      check_department1_pcComment
    );
    // tslint:disable-next-line: max-line-length
    const check_department2_pcIssue = Object.values(dataSend.department2).every(
      (value) => value['pcIssue'] !== null && value['pcIssue'] !== ''
    );
    // tslint:disable-next-line: max-line-length
    const check_department2_pcExtraComment = Object.values(
      dataSend.department2
    ).every(
      (value) =>
        value['pcExtraComment'] !== null && value['pcExtraComment'] !== ''
    );
    // tslint:disable-next-line: max-line-length
    const check_department2_pcComment = Object.values(
      dataSend.department2
    ).every(
      (value) => value['pcComment'] !== null && value['pcComment'] !== ''
    );
    // tslint:disable-next-line: max-line-length
    console.log(
      'department2',
      dataSend.department2,
      check_department2_pcIssue,
      check_department2_pcExtraComment,
      check_department2_pcComment
    );
    // tslint:disable-next-line: max-line-length
    const check_law_pcIssue = Object.values(dataSend.law).every(
      (value) => value['pcIssue'] !== null && value['pcIssue'] !== ''
    );
    // tslint:disable-next-line: max-line-length
    const check_law_pcExtraComment = Object.values(dataSend.law).every(
      (value) =>
        value['pcExtraComment'] !== null && value['pcExtraComment'] !== ''
    );
    const check_law_pcComment = Object.values(dataSend.law).every(
      (value) => value['pcComment'] !== null && value['pcComment'] !== ''
    );
    console.log(
      'law',
      dataSend.law,
      check_law_pcIssue,
      check_law_pcExtraComment,
      check_law_pcComment
    );

    const check_rule_pcIssue = Object.values(dataSend.rule).every(
      (value) => value['pcIssue'] !== null && value['pcIssue'] !== ''
    );
    // tslint:disable-next-line: max-line-length
    const check_rule_pcExtraComment = Object.values(dataSend.rule).every(
      (value) =>
        value['pcExtraComment'] !== null && value['pcExtraComment'] !== ''
    );
    const check_rule_pcComment = Object.values(dataSend.rule).every(
      (value) => value['pcComment'] !== null && value['pcComment'] !== ''
    );
    console.log(
      'rule',
      dataSend.rule,
      check_rule_pcIssue,
      check_rule_pcExtraComment,
      check_rule_pcComment
    );

    if (
      check_account_pcIssue === true &&
      check_account_pcExtraComment === true &&
      check_account_pcComment === true &&
      check_department1_pcIssue === true &&
      check_department1_pcExtraComment === true &&
      check_department1_pcComment === true &&
      check_department2_pcIssue === true &&
      check_department2_pcExtraComment === true &&
      check_department2_pcComment === true &&
      check_law_pcIssue === true &&
      check_law_pcExtraComment === true &&
      check_law_pcComment === true &&
      check_rule_pcIssue === true &&
      check_rule_pcExtraComment === true &&
      check_rule_pcComment === true
    ) {
      dataSend.status = true;
    }
    this.arryPC = []
    if (check_account_pcIssue === false ||
      check_account_pcExtraComment === false ||
      check_account_pcComment === false) {
      this.arryPC.push('ฝ่ายกำกับระเบียบและการปฏิบัติงาน')
    }

    if (check_department1_pcIssue === false ||
      check_department1_pcExtraComment === false ||
      check_department1_pcComment === false) {
      this.arryPC.push('ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ธุรกิจ 1')
    }

    if (check_department2_pcIssue === false ||
      check_department2_pcExtraComment === false ||
      check_department2_pcComment === false) {
      this.arryPC.push('ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ธุรกิจ 2')
    }

    if (check_law_pcIssue === false ||
      check_law_pcExtraComment === false ||
      check_law_pcComment === false) {
      this.arryPC.push('ฝ่ายนิติการ')
    }

    if (check_rule_pcIssue === false ||
      check_rule_pcExtraComment === false ||
      check_rule_pcComment === false) {
      this.arryPC.push('ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ทางการ')
    }
    const dataPC = {
      data: dataSend,
      status: this.arryPC
    };
    return dataPC;
  }
  POcommentConsider() {
    this.Submit.linkToAny();
  }

  makeid(length) {
    let result = '';
    const characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  backToDashboardPage() {
    // localStorage.setItem('requestId', '');
    this.router.navigate(['dashboard1']);
  }

  autoGrowTextZone(e) {
    e.target.style.height = '0px';
    e.target.style.overflow = 'hidden';
    e.target.style.height = e.target.scrollHeight + 0 + 'px';
  }

  // -----------------------------------------------DropDownList------------------------------------------------------
  GetDropDown() {
    // this.status_loading = true;
    this.complianceService.getDropDrownlist().subscribe((list) => {
      console.log('list:', list);
      this.list.account = list['data']['account'];
      this.list.department1 = list['data']['department1'];
      this.list.department2 = list['data']['department2'];
      this.list.rule = list['data']['rule'];
      this.list.law = list['data']['law'];

      // this.status_loading = false;

      this.getDetail(localStorage.getItem('requestId'));
    }, err => {
      this.status_loading = false;
      console.log(err);
    });

  }

  getDataListId(name: any, j: any) {
    if (name === 'account') {
      return 'TopicList' + j;
    }

    if (name === 'department1') {
      return 'TopicDepartment1List' + j;
    }

    if (name === 'department2') {
      return 'TopicDepartment2List' + j;
    }

    if (name === 'rule') {
      return 'TopicRuleList' + j;
    }

    if (name === 'law') {
      return 'TopicLawList' + j;
    }
  }
  getDetailListId(topic: any, j: any, k: any) {
    if (topic === 'account') {
      return 'DetailList' + String(j) + String(k);
    }

    if (topic === 'department1') {
      return 'DetailDepartment1List' + String(j) + String(k);
    }

    if (topic === 'department2') {
      return 'DetailDepartment2List' + String(j) + String(k);
    }

    if (topic === 'rule') {
      return 'DetailRuleList' + String(j) + String(k);
    }

    if (topic === 'law') {
      return 'DetailLawList' + String(j) + String(k);
    }
  }
  // -------------------------------------DropDownList-Account----------------------------------------
  ChangeGroupDropdown(data: any, header: any, index: any, topic: any) {
    // console.log(topic);

    this.accountList[index] = [];
    this.topicList[index] = [];

    // console.log('data:', data);
    // console.log('head:', header);
    if (data === 'account') {

      this.list.account.forEach((item, i) => {

        if (item.id == header) {

          // header.masterId = item.id
          item.topic.forEach((itemTopic, umn) => {
            this.accountList[index].push(itemTopic);
            this.topicList[index].push(itemTopic);
            // tslint:disable-next-line: no-shadowed-variable]
            this.TopicSelect(item.regulation, index, topic);
            // tslint:disable-next-line: no-shadowed-variable
            for (let i = 0; i < topic.length; i++) {
              if (topic[i].topic === item.topic[umn].topic) {
                topic[i].topic = item.topic[umn].topic;
                topic[i].masterId = item.topic[umn].id;
              } else {
                topic[i].topic = item.topic[0].topic;
                topic[i].masterId = item.topic[0].id;
              }
            }
          });
        }
      });
    }
  }

  TopicSelect(header, index, topic) {
    // console.log(header);
    // console.log(index);
    // console.log(topic);
    // console.log(this.topicList[index]);
  }
  SelectDropDown(header: any, data: any) {
    console.log(data);
    for (let index = 0; index < this.list.account.length; index++) {
      if (header === this.list.account[index].regulation) {
        // console.log( this.list.account[index].topic);
        // tslint:disable-next-line: no-unused-expression
        data.masterId = this.list.account[index].id;
        data.topic.forEach((topic) => {
          topic.topic = '';
        });
        break;
      } else {
        // tslint:disable-next-line: no-unused-expression
        data.masterId = null;
        data.topic.forEach((topic) => {
          topic.topic = '';
        });
      }
    }

  }
  ChangeTopic(topic, item, header) {
    console.log('topic', topic);
    console.log('item', item);
    console.log('header', header);
    console.log('list', this.list.account);
    for (let index = 0; index < this.list.account.length; index++) {
      if (header === this.list.account[index].regulation) {
        for (let p = 0; p < this.list.account[index].topic.length; p++) {
          if (topic.topic === this.list.account[index].topic[p].topic) {
            console.log(this.list.account[index].topic[p].id);
            topic.masterId = this.list.account[index].topic[p].id;
            break;
          } else {
            topic.masterId = null;
            topic.detail.forEach((detail) => {
              detail.masterId = null;
              // detail.id = null;
              detail.detail = '';
            });
          }
        }
      }
    }
    console.log('topic', topic);
    console.log('item', item);
    console.log('header', header);
  }
  SelectDetail(topic: any, header: any) {
    console.log(topic);
    console.log(header);
    for (let index = 0; index < this.list.account.length; index++) {
      if (header === this.list.account[index].regulation) {
        for (let p = 0; p < this.list.account[index].topic.length; p++) {
          if (topic === this.list.account[index].topic[p].topic) {
            return this.list.account[index].topic[p].detail;
          }
        }
      }
    }
  }

  ChangeDetail(topic, item, header, detail) {
    console.log('list', this.list.account);
    for (let index = 0; index < this.list.account.length; index++) {
      if (header === this.list.account[index].regulation) {
        for (let p = 0; p < this.list.account[index].topic.length; p++) {
          if (topic.topic === this.list.account[index].topic[p].topic) {
            console.log(
              'topitopictopicc:',
              this.list.account[index].topic[p].detail
            );
            for (
              let k = 0;
              k < this.list.account[index].topic[p].detail.length;
              k++
            ) {
              if (
                detail.detail ===
                this.list.account[index].topic[p].detail[k].detail
              ) {
                console.log('kk:', this.list.account[index].topic[p].detail[k]);
                detail.masterId = this.list.account[index].topic[p].detail[
                  k
                ].id;
                break;
              } else {
                detail.masterId = null;
              }
            }
          }
        }
      }
    }
    console.log('topic', topic);
    console.log('item', item);
    console.log('header', header);
    console.log('detail', detail);

  }
  // -----------------------------------End DropDownList-Account----------------------------------------

  // -------------------------------------DropdownList-Department 1-----------------------------------
  DepartmentHeader(data: any, header: any, index: any, topic: any, head: any) {
    this.department1List[index] = [];
    this.topicDepartment1List[index] = [];
    if (data === 'department1') {
      this.list.department1.forEach((item, i) => {
        if (item.id == header) {
          head.bookNumber = item.bookNumber;
          head.masterId = item.id;
          console.log(head.bookNumber);
          item.topic.forEach((itemTopic, umn) => {
            this.department1List[index].push(itemTopic);
            this.topicDepartment1List[index].push(itemTopic);
            // tslint:disable-next-line: no-shadowed-variable]
            this.TopicSelectDepartment1(item.department, index, topic);
            // tslint:disable-next-line: no-shadowed-variable
            for (let i = 0; i < topic.length; i++) {
              if (topic[i].topic === item.topic[umn].topic) {
                topic[i].topic = item.topic[umn].topic;
                topic[i].masterId = item.topic[umn].id;
              } else {
                topic[i].topic = item.topic[0].topic;
                topic[i].masterId = item.topic[0].id;
              }
            }
          });
        }
      });
    }
  }
  TopicSelectDepartment1(header, index, topic) {
    console.log(header);
    console.log(index);
    console.log(topic);
    // console.log(this.topicList[index]);
  }
  test(e) {
    console.log('eee:', e);
  }
  SelectDropDownDepartment1(e, bookNumber: any) {
    let name = e.target.value;
    console.log(e);
    console.log(name);
    console.log(this.list.department1);
    // console.log(this.list.department1);
    for (let index = 0; index < this.list.department1.length; index++) {
      if (name == ("หน่วยงาน :" + this.list.department1[index].department + ", เลขที่หนังสือ :" + this.list.department1[index].bookNumber.replace(/\n/g, ''))) {

        bookNumber.department = this.list.department1[index].department
        bookNumber.bookNumber = this.list.department1[index].bookNumber;
        bookNumber.masterId = this.list.department1[index].id;
        bookNumber.topic.forEach((topic) => {
          topic.topic = '';
        });
        break;
      } else {
        bookNumber.bookNumber = '';
        bookNumber.masterId = null;
        bookNumber.topic.forEach((topic) => {
          topic.topic = '';
        });
      }
    }
  }

  ChangeTopicDepartment1(topic, header, event) {
    for (let index = 0; index < this.list.department1.length; index++) {
      if (header.masterId === this.list.department1[index].id) {
        for (let p = 0; p < this.list.department1[index].topic.length; p++) {
          if (event.replace(/\n/g, '') === this.list.department1[index].topic[p].topic.replace(/\n/g, '')) {
            console.log(this.list.department1[index].topic[p].id);
            topic.masterId = this.list.department1[index].topic[p].id;
            break;
          }
          else {
            topic.masterId = null;
            // topic.id = null;
            topic.detail.forEach((detail) => {
              detail.masterId = null;
              detail.detail = '';
              // detail.id = null;
            });
          }
        }
      }
    }
    console.log('topic', topic);
    console.log('header', header);
    console.log('event', event);
  }
  SelectDetailDepartment1(topic: any, header: any) {
    console.log(topic);
    console.log(header);
    for (let index = 0; index < this.list.department1.length; index++) {
      if (header === this.list.department1[index].department) {
        for (let p = 0; p < this.list.department1[index].topic.length; p++) {
          if (topic === this.list.department1[index].topic[p].topic) {
            return this.list.department1[index].topic[p].detail;
          }
        }
      }
    }
  }

  // head , item , processItem , $event
  ChangeDetailDepartment1(head, topic, detail, event) {
    console.log('list', this.list.department1);
    for (let index = 0; index < this.list.department1.length; index++) {
      if (head.masterId === this.list.department1[index].id) {
        for (let p = 0; p < this.list.department1[index].topic.length; p++) {
          if (topic.masterId === this.list.department1[index].topic[p].id) {
            console.log(
              'topitopictopicc:',
              this.list.department1[index].topic[p].detail
            );
            for (
              let k = 0;
              k < this.list.department1[index].topic[p].detail.length;
              k++
            ) {
              if (
                event ===
                this.list.department1[index].topic[p].detail[k].detail
              ) {
                console.log(
                  'kk:',
                  this.list.department1[index].topic[p].detail[k]
                );
                detail.masterId = this.list.department1[index].topic[p].detail[
                  k
                ].id;
                break;
              } else {
                detail.masterId = null;
              }
            }
          }
        }
      }
    }
    console.log('topic', topic);
    console.log('head', head);
    console.log('event', event);
    console.log('detail', detail);
  }
  // -----------------------------------End DropdownList-Department 1-----------------------------------

  // -------------------------------------DropdownList-Department 2-------------------------------------
  DepartmentHeader2(data: any, header: any, index: any, topic: any, head: any) {
    this.status_loading = true;
    this.department2List[index] = [];
    this.topicDepartment2List[index] = [];
    console.log("OOOO ", header);

    if (data === 'department2') {
      this.list.department2.forEach((item, i) => {
        console.log("No", item.id);
        if (item.id == header) {
          console.log("have", item.id);

          head.bookNumber = item.bookNumber;
          head.master = item.id;
          console.log(head.bookNumber);
          item.topic.forEach((itemTopic, umn) => {
            this.department2List[index].push(itemTopic);
            this.topicDepartment2List[index].push(itemTopic);
            // tslint:disable-next-line: no-shadowed-variable]
            this.TopicSelectDepartment2(item.department, index, topic);
            // tslint:disable-next-line: no-shadowed-variable
            for (let i = 0; i < topic.length; i++) {
              if (topic[i].topic === item.topic[umn].topic) {
                topic[i].topic = item.topic[umn].topic;
                topic[i].masterId = item.topic[umn].id;
              } else {
                topic[i].topic = item.topic[0].topic;
                topic[i].masterId = item.topic[0].id;
              }
            }
          });
        }
      });
    }
    this.status_loading = false;
  }
  TopicSelectDepartment2(header, index, topic) {
    console.log(header);
    console.log(index);
    console.log(topic);
    // console.log(this.topicList[index]);
  }
  SelectDropDownDepartment2(header: any, bookNumber: any) {
    // console.log(this.list.department2);
    let name = header.target.value;
    for (let index = 0; index < this.list.department2.length; index++) {
      if (name == ("หน่วยงาน :" + this.list.department2[index].department + ", เลขที่หนังสือ :" + this.list.department2[index].bookNumber.replace(/\n/g, ''))) {

        bookNumber.department = this.list.department2[index].department
        bookNumber.bookNumber = this.list.department2[index].bookNumber;
        bookNumber.masterId = this.list.department2[index].id;
        bookNumber.topic.forEach((topic) => {
          topic.topic = '';
        });
        break;
      } else {
        bookNumber.bookNumber = '';
        bookNumber.masterId = null;
        bookNumber.topic.forEach((topic) => {
          topic.topic = '';
        });
      }
    }
  }

  ChangeTopicDepartment2(topic, header, event) {
    for (let index = 0; index < this.list.department2.length; index++) {
      if (header.masterId === this.list.department2[index].id) {
        for (let p = 0; p < this.list.department2[index].topic.length; p++) {
          if (event.replace(/\n/g, '') === this.list.department2[index].topic[p].topic.replace(/\n/g, '')) {
            console.log(this.list.department2[index].topic[p].id);
            topic.masterId = this.list.department2[index].topic[p].id;
            break;
          } else {
            topic.masterId = null;
            topic.detail.forEach((detail) => {
              detail.masterId = null;
              detail.detail = '';
              // detail.id = null;
              // detail.detail = '';
            });
          }
        }
      }
    }
    console.log('topic', topic);
    console.log('event', event);
    console.log('header', header);
  }
  SelectDetailDepartment2(topic: any, header: any) {
    console.log(topic);
    console.log(header);
    for (let index = 0; index < this.list.department2.length; index++) {
      if (header === this.list.department2[index].department) {
        for (let p = 0; p < this.list.department2[index].topic.length; p++) {
          if (topic === this.list.department2[index].topic[p].topic) {
            return this.list.department2[index].topic[p].detail;
          }
        }
      }
    }
  }


  ChangeDetailDepartment2(head, topic, detail, event) {
    console.log('topic', topic);
    console.log('event', event);
    console.log('head', head);
    console.log('detail', detail);
    console.log('list', this.list.department2);
    for (let index = 0; index < this.list.department2.length; index++) {
      if (head.masterId === this.list.department2[index].id) {
        for (let p = 0; p < this.list.department2[index].topic.length; p++) {
          if (topic.masterId === this.list.department2[index].topic[p].id) {
            console.log(
              'topitopictopicc:',
              this.list.department2[index].topic[p].detail
            );
            for (
              let k = 0;
              k < this.list.department2[index].topic[p].detail.length;
              k++
            ) {
              if (
                event ===
                this.list.department2[index].topic[p].detail[k].detail
              ) {
                console.log(
                  'kk:',
                  this.list.department2[index].topic[p].detail[k]
                );
                detail.masterId = this.list.department2[index].topic[p].detail[
                  k
                ].id;
                break;
              } else {
                detail.masterId = null;
              }
            }
          }
        }
      }
    }
  }
  // -------------------------------------End DropdownList-Department 2----------------------------------

  // -------------------------------------DropdownList- Rule-------------------------------------
  RuleHeader(data: any, header: any, index: any, topic: any, head: any) {
    this.ruleList[index] = [];
    this.topicRuleList[index] = [];
    head.department = head.department
    console.log(topic);
    console.log(head);
    if (data === 'rule') {
      this.list.rule.forEach((item, i) => {
        if (item.id == header) {
          head.bookNumber = item.bookNumber;
          head.masterId = item.id;
          // console.log(head.bookNumber);
          // console.log(item.id);
          item.topic.forEach((itemTopic, umn) => {
            this.ruleList[index].push(itemTopic);
            this.topicRuleList[index].push(itemTopic);
            // tslint:disable-next-line: no-shadowed-variable]
            this.TopicSelectDepartment2(item.department, index, topic);
            // this.sameValue(this.topicRuleList[index] , item.id);
            // tslint:disable-next-line: no-shadowed-variable

            // tslint:disable-next-line: no-shadowed-variable
            for (let i = 0; i < topic.length; i++) {
              if (topic[i].topic === item.topic[umn].topic) {
                topic[i].topic = item.topic[umn].topic;
                topic[i].masterId = item.topic[umn].id;
              } else {
                topic[i].topic = item.topic[0].topic;
                topic[i].masterId = item.topic[0].id;
              }
            }
          });
        }
      });
    }
  }

  TopicSelectRule(header, index, topic) {
    console.log(header);
    console.log(index);
    console.log(topic);
    // console.log(this.topicList[index]);
  }
  SelectDropDownRule(header: any, bookNumber: any) {
    let name = header.target.value;

    console.log(this.list.rule)
    for (let index = 0; index < this.list.rule.length; index++) {
      if (name == ("หน่วยงาน :" + this.list.rule[index].department + ", เลขที่หนังสือ :" + this.list.rule[index].bookNumber.replace(/\n/g, ''))) {
        console.log(this.list.rule[index].bookNumber)
        bookNumber.department = this.list.rule[index].department
        bookNumber.bookNumber = this.list.rule[index].bookNumber;
        bookNumber.masterId = this.list.rule[index].id;
        bookNumber.topic.forEach((topic) => {
          topic.topic = '';
        });
        break;
      } else {
        bookNumber.bookNumber = '';
        bookNumber.masterId = null;
        bookNumber.topic.forEach((topic) => {
          topic.topic = '';
        });
      }
    }
  }
  ChangeTopicRule(topic, header, event) {
    console.log('topic', topic);
    console.log('header', header.masterId);
    console.log('event', event);

    for (let index = 0; index < this.list.rule.length; index++) {
      if (header.masterId === this.list.rule[index].id) {
        for (let p = 0; p < this.list.rule[index].topic.length; p++) {
          console.log('topic>>', topic);
          console.log('topic.topic ', topic.masterId, '===', this.list.rule[index].topic[p].id)
          if (event.replace(/\n/g, '') === this.list.rule[index].topic[p].topic.replace(/\n/g, '')) {
            console.log('id topic', this.list.rule[index].topic[p].id);
            topic.masterId = this.list.rule[index].topic[p].id;
            break;
          } else {
            console.log('else????')
            topic.masterId = null;
            topic.detail.forEach((detail) => {
              detail.masterId = null;
              detail.detail = '';
              // detail.id = null;
              // detail.detail = '';
            });
          }
        }
      }
    }
  }
  SelectDetailRule(topic: any, header: any) {
    console.log(topic);
    console.log(header);
    for (let index = 0; index < this.list.rule.length; index++) {
      if (header === this.list.rule[index].department) {
        for (let p = 0; p < this.list.rule[index].topic.length; p++) {
          if (topic === this.list.rule[index].topic[p].topic) {
            return this.list.rule[index].topic[p].detail;
          }
        }
      }
    }
  }
  // head , item , processItem , $event
  ChangeDetailRule(head, topic, detail, event) {
    console.log('topic', topic);
    console.log('head', head);
    console.log('event', event);
    console.log('detail', detail);
    console.log('list', this.list.rule);
    for (let index = 0; index < this.list.rule.length; index++) {
      if (head.masterId === this.list.rule[index].id) {
        for (let p = 0; p < this.list.rule[index].topic.length; p++) {
          if (topic.masterId === this.list.rule[index].topic[p].id) {
            console.log(
              'topitopictopicc:',
              this.list.rule[index].topic[p].detail
            );
            for (
              let k = 0;
              k < this.list.rule[index].topic[p].detail.length;
              k++
            ) {
              if (
                event ===
                this.list.rule[index].topic[p].detail[k].detail
              ) {

                console.log('kk:', this.list.rule[index].topic[p].detail[k].id);
                detail.masterId = this.list.rule[index].topic[p].detail[k].id;
                console.log('detail.masterId', detail.masterId)
                break;
              } else {
                console.log('else????')
                detail.masterId = null;
              }
            }
          }
        }
      }
    }
  }
  // -------------------------------------End DropdownList- Rule----------------------------------

  //  -------------------------------------DropdownList - Law--------------------------------------
  LawHeader(data: any, header: any, index: any, topic: any, head: any) {
    this.lawList[index] = [];
    this.topicLawList[index] = [];
    if (data === 'law') {
      this.list.law.forEach((item, i) => {
        if (item.id == header) {
          head.bookNumber = item.bookNumber;
          head.masterId = item.id;
          console.log(head.bookNumber);
          item.topic.forEach((itemTopic, umn) => {
            this.lawList[index].push(itemTopic);
            this.topicLawList[index].push(itemTopic);
            // tslint:disable-next-line: no-shadowed-variable]
            this.TopicSelectLaw(item.department, index, topic);
            // tslint:disable-next-line: no-shadowed-variable
            for (let i = 0; i < topic.length; i++) {
              if (topic[i].topic === item.topic[umn].topic) {
                topic[i].topic = item.topic[umn].topic;
                topic[i].masterId = item.topic[umn].id;
              } else {
                topic[i].topic = item.topic[0].topic;
                topic[i].masterId = item.topic[0].id;
              }
            }
          });
        }
      });
    }
  }
  TopicSelectLaw(header, index, topic) {
    console.log(header);
    console.log(index);
    console.log(topic);
    // console.log(this.topicList[index]);
  }
  SelectDropDownLaw(header: any, head: any) {
    let name = header.target.value;
    console.log();
    console.log(name);
    // console.log(this.list.law);
    for (let index = 0; index < this.list.law.length; index++) {
      if (name == ("หน่วยงาน :" + this.list.law[index].department + ", เลขที่หนังสือ :" + this.list.law[index].bookNumber.replace(/\n/g, ''))) {
        console.log(this.list.law[index]);
        head.department = this.list.law[index].department
        head.bookNumber = this.list.law[index].bookNumber;
        head.masterId = this.list.law[index].id;
        head.topic.forEach((topic) => {
          topic.topic = '';
        });
        break;
      } else {
        head.bookNumber = '';
        head.masterId = null;
        head.topic.forEach((topic) => {
          topic.topic = '';
        });
      }
    }
  }


  ChangeTopicLaw(topic, header, event) {
    for (let index = 0; index < this.list.law.length; index++) {
      if (header.masterId === this.list.law[index].id) {
        for (let p = 0; p < this.list.law[index].topic.length; p++) {
          if (event.replace(/\n/g, '') === this.list.law[index].topic[p].topic.replace(/\n/g, '')) {
            console.log(this.list.law[index].topic[p].id);
            topic.masterId = this.list.law[index].topic[p].id;
            break;
          } else {
            topic.masterId = null;
            topic.detail.forEach((detail) => {
              detail.masterId = null;
              detail.detail = '';
            });
          }
        }
      }
    }
    console.log('topic', topic);
    console.log('event', event);
    console.log('header', header);
  }
  SelectDetailLaw(topic: any, header: any) {
    console.log(topic);
    console.log(header);
    for (let index = 0; index < this.list.law.length; index++) {
      if (header === this.list.law[index].department) {
        for (let p = 0; p < this.list.law[index].topic.length; p++) {
          if (topic === this.list.law[index].topic[p].topic) {
            return this.list.law[index].topic[p].detail;
          }
        }
      }
    }
  }



  ChangeDetailLaw(head, topic, detail, event) {
    console.log('topic', topic);
    console.log('event', event);
    console.log('head', head);
    console.log('detail', detail);
    for (let index = 0; index < this.list.law.length; index++) {
      if (head.masterId === this.list.law[index].id) {
        for (let p = 0; p < this.list.law[index].topic.length; p++) {
          if (topic.masterId === this.list.law[index].topic[p].id) {
            console.log(
              'topitopictopicc:',
              this.list.law[index].topic[p].detail
            );
            for (
              let k = 0;
              k < this.list.law[index].topic[p].detail.length;
              k++
            ) {
              if (
                event === this.list.law[index].topic[p].detail[k].detail
              ) {
                console.log('kk:', this.list.law[index].topic[p].detail[k]);
                detail.masterId = this.list.law[index].topic[p].detail[k].id;
                break;
              } else {
                detail.masterId = null;
              }
            }
          }
        }
      }
    }
  }

  //  ---------------------------------End DropdownList - Law--------------------------------------
  checkValue(name: any, data: any) {
    this.StatusCompliance = [];
    let status = true;
    const listkey = [];
    let dataCheck = null;
    // ---------------------Account ------------------------------
    if (
      name === 'ฝ่ายกำกับระเบียบและการปฏิบัติงาน' &&
      data.account.length !== 0
    ) {
      dataCheck = data.account;
      console.log(dataCheck);

      const keys = Object.keys(dataCheck[0].topic[0].detail[0]);
      keys.forEach((key) => {
        if (
          key !== 'poComment' &&
          key !== 'buCommentId' &&
          key !== 'pcCommentId' &&
          key !== 'id' &&
          key !== 'masterId' &&
          key !== 'pcComment' &&
          key !== 'pcExtraComment' &&
          key !== 'pcIssue'
        ) {
          listkey.push(key);
        }
      });

      console.log(listkey);
      for (let i = 0; i < dataCheck.length; i++) {
        if (dataCheck[i].regulation === '' || dataCheck[i].regulation === null) {
          status = false;
          break;
        } else {
          for (let index = 0; index < dataCheck.length; index++) {
            if (
              dataCheck[index]['regulation'] === '' ||
              dataCheck[index]['regulation'] === null
            ) {
              status = false;
            } else {
              for (let k = 0; k < dataCheck[index].topic.length; k++) {
                if (
                  dataCheck[index].topic[k].topic === '' ||
                  dataCheck[index].topic[k].topic === null
                ) {
                  status = false;
                } else {
                  for (
                    let p = 0;
                    p < dataCheck[index].topic[k].detail.length;
                    p++
                  ) {
                    if (
                      dataCheck[index].topic[k].topic === '' ||
                      dataCheck[index].topic[k].topic === null
                    ) {
                      status = false;
                    } else {
                      for (
                        let l = 0;
                        l < dataCheck[index].topic[k].detail.length;
                        l++
                      ) {
                        for (let i = 0; i < listkey.length; i++) {
                          if (
                            dataCheck[index].topic[k].detail[l][listkey[i]] ===
                            '' ||
                            dataCheck[index].topic[k].detail[l][listkey[i]] ===
                            null ||
                            dataCheck[index].topic[k].detail[l][listkey[i]] ===
                            'null'
                          ) {
                            status = false;
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
        if (status == false) {
          this.StatusCompliance.push('ฝ่ายกำกับระเบียบและการปฏิบัติงาน')
        }
      }
      // ----list key Detail-----



    } else if (
      name === 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ธุรกิจ 1' &&
      data.department1.length !== 0
    ) {
      dataCheck = data.department1;
      console.log(dataCheck);
      // ----list key Detail-----
      const keys = Object.keys(dataCheck[0].topic[0].detail[0]);
      keys.forEach((key) => {
        if (
          key !== 'poComment' &&
          key !== 'buCommentId' &&
          key !== 'pcCommentId' &&
          key !== 'id' &&
          key !== 'masterId' &&
          key !== 'pcComment' &&
          key !== 'pcExtraComment' &&
          key !== 'pcIssue'
        ) {
          listkey.push(key);
        }
      });

      console.log(listkey);

      for (let i = 0; i < dataCheck.length; i++) {
        if (dataCheck[i].department === '' || dataCheck[i].department === null) {
          status = false;
          break;
        }
        else {
          for (let index = 0; index < dataCheck.length; index++) {
            if (
              dataCheck[index]['department'] === '' ||
              dataCheck[index]['department'] === null ||
              dataCheck[index]['bookNumber'] === ''
            ) {
              status = false;
            } else {
              for (let k = 0; k < dataCheck[index].topic.length; k++) {
                if (
                  dataCheck[index].topic[k].topic === '' ||
                  dataCheck[index].topic[k].topic === null
                ) {
                  status = false;
                } else {
                  for (
                    let p = 0;
                    p < dataCheck[index].topic[k].detail.length;
                    p++
                  ) {
                    if (
                      dataCheck[index].topic[k].topic === '' ||
                      dataCheck[index].topic[k].topic === null
                    ) {
                      status = false;
                    } else {
                      for (
                        let l = 0;
                        l < dataCheck[index].topic[k].detail.length;
                        l++
                      ) {
                        for (let i = 0; i < listkey.length; i++) {
                          if (
                            dataCheck[index].topic[k].detail[l][listkey[i]] ===
                            '' ||
                            dataCheck[index].topic[k].detail[l][listkey[i]] ===
                            null ||
                            dataCheck[index].topic[k].detail[l][listkey[i]] ===
                            'null'
                          ) {
                            status = false;
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }

    } else if (
      name === 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ธุรกิจ 2' &&
      data.department2.length !== 0
    ) {
      dataCheck = data.department2;
      console.log(dataCheck);
      // ----list key Detail-----
      const keys = Object.keys(dataCheck[0].topic[0].detail[0]);
      keys.forEach((key) => {
        if (
          key !== 'poComment' &&
          key !== 'buCommentId' &&
          key !== 'pcCommentId' &&
          key !== 'id' &&
          key !== 'masterId' &&
          key !== 'pcComment' &&
          key !== 'pcExtraComment' &&
          key !== 'pcIssue'
        ) {
          listkey.push(key);
        }
      });

      console.log(listkey);

      for (let i = 0; i < dataCheck.length; i++) {
        if (dataCheck[i].department === '' || dataCheck[i].department === null) {
          status = false;
          break;
        } else {
          for (let index = 0; index < dataCheck.length; index++) {
            if (
              dataCheck[index]['department'] === '' ||
              dataCheck[index]['department'] === null ||
              dataCheck[index]['bookNumber'] === ''
            ) {
              status = false;
            } else {
              for (let k = 0; k < dataCheck[index].topic.length; k++) {
                if (
                  dataCheck[index].topic[k].topic === '' ||
                  dataCheck[index].topic[k].topic === null
                ) {
                  status = false;
                } else {
                  for (
                    let p = 0;
                    p < dataCheck[index].topic[k].detail.length;
                    p++
                  ) {
                    if (
                      dataCheck[index].topic[k].topic === '' ||
                      dataCheck[index].topic[k].topic === null
                    ) {
                      status = false;
                    } else {
                      for (
                        let l = 0;
                        l < dataCheck[index].topic[k].detail.length;
                        l++
                      ) {
                        for (let i = 0; i < listkey.length; i++) {
                          if (
                            dataCheck[index].topic[k].detail[l][listkey[i]] ===
                            '' ||
                            dataCheck[index].topic[k].detail[l][listkey[i]] ===
                            null ||
                            dataCheck[index].topic[k].detail[l][listkey[i]] ===
                            'null'
                          ) {
                            status = false;
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }

    } else if (
      name === 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ทางการ' &&
      data.rule.length !== 0
    ) {
      dataCheck = data.rule;
      console.log(dataCheck);
      // ----list key Detail-----
      const keys = Object.keys(dataCheck[0].topic[0].detail[0]);
      keys.forEach((key) => {
        if (
          key !== 'poComment' &&
          key !== 'buCommentId' &&
          key !== 'pcCommentId' &&
          key !== 'id' &&
          key !== 'masterId' &&
          key !== 'pcComment' &&
          key !== 'pcExtraComment' &&
          key !== 'pcIssue'
        ) {
          listkey.push(key);
        }
      });

      console.log(listkey);

      for (let i = 0; i < dataCheck.length; i++) {
        if (dataCheck[i].department === '' || dataCheck[i].department === null) {
          status = false;
          break;
        } else {
          for (let index = 0; index < dataCheck.length; index++) {
            if (
              dataCheck[index]['department'] === '' ||
              dataCheck[index]['department'] === null ||
              dataCheck[index]['bookNumber'] === ''
            ) {
              status = false;
            } else {
              for (let k = 0; k < dataCheck[index].topic.length; k++) {
                if (
                  dataCheck[index].topic[k].topic === '' ||
                  dataCheck[index].topic[k].topic === null
                ) {
                  status = false;
                } else {
                  for (
                    let p = 0;
                    p < dataCheck[index].topic[k].detail.length;
                    p++
                  ) {
                    if (
                      dataCheck[index].topic[k].topic === '' ||
                      dataCheck[index].topic[k].topic === null
                    ) {
                      status = false;
                    } else {
                      for (
                        let l = 0;
                        l < dataCheck[index].topic[k].detail.length;
                        l++
                      ) {
                        for (let i = 0; i < listkey.length; i++) {
                          if (
                            dataCheck[index].topic[k].detail[l][listkey[i]] ===
                            '' ||
                            dataCheck[index].topic[k].detail[l][listkey[i]] ===
                            null ||
                            dataCheck[index].topic[k].detail[l][listkey[i]] ===
                            'null'
                          ) {
                            status = false;
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }

    } else if (name === 'ฝ่ายนิติการ' && data.law.length !== 0) {
      dataCheck = data.law;
      console.log(dataCheck);
      // ----list key Detail-----
      const keys = Object.keys(dataCheck[0].topic[0].detail[0]);
      keys.forEach((key) => {
        if (
          key !== 'poComment' &&
          key !== 'buCommentId' &&
          key !== 'pcCommentId' &&
          key !== 'id' &&
          key !== 'masterId' &&
          key !== 'pcComment' &&
          key !== 'pcExtraComment' &&
          key !== 'pcIssue'
        ) {
          listkey.push(key);
        }
      });

      console.log(listkey);
      for (let i = 0; i < dataCheck.length; i++) {
        if (dataCheck[i].department === '' || dataCheck[i].department === null) {
          status = false;
          break;
        } else {
          for (let index = 0; index < dataCheck.length; index++) {
            if (
              dataCheck[index]['department'] === '' ||
              dataCheck[index]['department'] === null ||
              dataCheck[index]['bookNumber'] === ''
            ) {
              status = false;
            } else {
              for (let k = 0; k < dataCheck[index].topic.length; k++) {
                if (
                  dataCheck[index].topic[k].topic === '' ||
                  dataCheck[index].topic[k].topic === null
                ) {
                  status = false;
                } else {
                  for (
                    let p = 0;
                    p < dataCheck[index].topic[k].detail.length;
                    p++
                  ) {
                    if (
                      dataCheck[index].topic[k].topic === '' ||
                      dataCheck[index].topic[k].topic === null
                    ) {
                      status = false;
                    } else {
                      for (
                        let l = 0;
                        l < dataCheck[index].topic[k].detail.length;
                        l++
                      ) {
                        for (let i = 0; i < listkey.length; i++) {
                          if (
                            dataCheck[index].topic[k].detail[l][listkey[i]] ===
                            '' ||
                            dataCheck[index].topic[k].detail[l][listkey[i]] ===
                            null ||
                            dataCheck[index].topic[k].detail[l][listkey[i]] ===
                            'null'
                          ) {
                            status = false;
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    } else {
      if (
        data.account.length === 0 ||
        data.department1.length === 0 ||
        data.department2.length === 0 ||
        data.rule.length === 0 ||
        data.law.length === 0
      ) {
        status = true;
        dataCheck = true;
      }
    }
    // ------------------End law-------------------------
    console.log('status:', status);
    console.log('dataCheck:', dataCheck);
    if (this.data.validate === false) {
      return (status = true);
    } else if (this.data.validate === true) {
      if (dataCheck && status) {
        return (status = true);
      } else {
        return (status = false);
      }
    }
  }

  checkMasterId() {
    console.log(this.data);
    if (this.rolename === 'ฝ่ายกำกับระเบียบและการปฏิบัติงาน') {
      for (let index = 0; index < this.data.account.length; index++) {
        if (this.data.account[index].masterId !== null) {
          for (let i = 0; i < this.data.account[index].topic.length; i++) {
            if (this.data.account[index].topic[i].masterId !== null) {
              for (
                let k = 0;
                k < this.data.account[index].topic[i].detail.length;
                k++
              ) {
                if (this.data.account[index].topic[i].detail[k].id === null) {
                  this.data.account[index].topic[i].detail[k].masterId = null;
                }
              }
            }
          }
        }
      }
    }
    // if (this.rolename === 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ธุรกิจ 1') {
    //   for (let index = 0; index < this.data.department1.length; index++) {
    //     if (this.data.department1[index].masterId !== null) {
    //       for (let i = 0; i < this.data.department1[index].topic.length; i++) {
    //         if (this.data.department1[index].topic[i].masterId !== null) {
    //           for (
    //             let k = 0;
    //             k < this.data.department1[index].topic[i].detail.length;
    //             k++
    //           ) {
    //             if (
    //               this.data.department1[index].topic[i].detail[k].id === null
    //             ) {
    //               this.data.department1[index].topic[i].detail[
    //                 k
    //               ].masterId = null;
    //             }
    //           }
    //         }
    //       }
    //     }
    //   }
    // }
    if (this.rolename === 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ธุรกิจ 2') {
      for (let index = 0; index < this.data.department2.length; index++) {
        if (this.data.department2[index].masterId !== null) {
          for (let i = 0; i < this.data.department2[index].topic.length; i++) {
            if (this.data.department2[index].topic[i].masterId !== null) {
              for (
                let k = 0;
                k < this.data.department2[index].topic[i].detail.length;
                k++
              ) {
                if (
                  this.data.department2[index].topic[i].detail[k].id === null
                ) {
                  this.data.department2[index].topic[i].detail[
                    k
                  ].masterId = null;
                }
              }
            }
          }
        }
      }
    }
    // if (this.rolename === 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ทางการ') {
    //   for (let index = 0; index < this.data.rule.length; index++) {
    //     if (this.data.rule[index].masterId !== null) {
    //       for (let i = 0; i < this.data.rule[index].topic.length; i++) {
    //         if (this.data.rule[index].topic[i].masterId !== null) {
    //           for (
    //             let k = 0;
    //             k < this.data.rule[index].topic[i].detail.length;
    //             k++
    //           ) {
    //             if (this.data.rule[index].topic[i].detail[k].id === null) {
    //               this.data.rule[index].topic[i].detail[k].masterId = null;
    //             }
    //           }
    //         }
    //       }
    //     }
    //   }
    // }
    if (this.rolename === 'ฝ่ายนิติการ') {
      for (let index = 0; index < this.data.law.length; index++) {
        if (this.data.law[index].masterId !== null) {
          for (let i = 0; i < this.data.law[index].topic.length; i++) {
            if (this.data.law[index].topic[i].masterId !== null) {
              for (
                let k = 0;
                k < this.data.law[index].topic[i].detail.length;
                k++
              ) {
                if (this.data.law[index].topic[i].detail[k].id === null) {
                  this.data.law[index].topic[i].detail[k].masterId = null;
                }
              }
            }
          }
        }
      }
    } else {
      console.log(this.rolename);
    }
  }

  containsObject(obj: any, list: any) {

    if (list != null) {
      var i;
      for (i = 0; i < list.length; i++) {
        if (list[i].id == obj) {
          return true;
        }
      }
    }

    return false;
  }

  accountContainsObject(header, topic, detail) {

    for (let i = 0; i < this.list.account.length; i++) {

      if (this.list.account[i].id == header) {
        for (let j = 0; j < this.list.account[i].topic.length; j++) {
          if (this.list.account[i].topic[j].id == topic) {
            return true;

          }
        }
      }
    }
    return false;
  }
}
