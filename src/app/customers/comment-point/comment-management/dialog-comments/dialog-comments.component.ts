import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import Swal from 'sweetalert2';

export interface DialogData {
  headerDetail: any;
  bodyDetail: any;
  deleteStatus: any;
}

@Component({
  selector: 'app-dialog-comments',
  templateUrl: './dialog-comments.component.html',
  styleUrls: ['./dialog-comments.component.scss']
})
export class DialogCommentsComponent implements OnInit {
  close() {
    this.data.deleteStatus = false;
    this.dialogRef.close(this.data);
  }

  back() {
    this.data.deleteStatus = false;
    this.dialogRef.close(this.data);
  }

  save() {
    this.data.deleteStatus = true;
    this.dialogRef.close(this.data);
  }

  constructor(
    private dialogRef: MatDialogRef<DialogCommentsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.dialogRef.disableClose = true;
  }

  ngOnInit() {
  }


}
