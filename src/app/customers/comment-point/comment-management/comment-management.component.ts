import { Component, OnInit, Input } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { saveAs } from 'file-saver';
import { DialogCommentsComponent } from './dialog-comments/dialog-comments.component';
import { CommentManagementService } from '../../../services/comment-point/comment-management/comment-management.service';
import { AnyAaaaRecord } from 'dns';
import Swal from 'sweetalert2';
import { RequestService } from '../../../services/request/request.service';

@Component({
  selector: 'app-comment-management',
  templateUrl: './comment-management.component.html',
  styleUrls: ['./comment-management.component.scss']
})
export class CommentManagementComponent implements OnInit {
  showDateStatus = false;
  showHead = false;
  data = {
    validate: null,
    requestId: null,
    result: [
      {
        question: {
          id: 17,
          date: '20/07/2020',
          requestId: 5,
          questionText: 'question ????',
          userId: '550731',
          attachments: {
            file: [],
            fileDelete: []
          },
          role: 'Risk-ฝ่ายบริหารความเสี่ยงสินเชื่อรายย่อย',
          edit_status: true,
          edit_comment_status: false,
          status_submit: false,
          dataCommentQ: null
        },
        answer: {
          id: 17,
          date: '20/07/2020',
          questionId: 5,
          answer: 'question ????',
          userId: '550731',
          attachments: {
            file: [],
            fileDelete: []
          },
          role: 'Risk-ฝ่ายบริหารความเสี่ยงสินเชื่อรายย่อย',
          edit_status: true,
          edit_comment_status: false,
          status_submit: false,
          dataCommentA: null
        }
      },
      {
        question: {
          id: 18,
          date: '20/07/2020',
          requestId: 5,
          questionText: 'question ???? 2222222',
          userId: '550731',
          attachments: {
            file: [],
            fileDelete: []
          },
          role: 'Risk-ฝ่ายบริหารความเสี่ยงสินเชื่อรายย่อย',
          edit_status: true,
          edit_comment_status: false,
          status_submit: false,
          dataCommentQ: null
        },
        answer: {
          id: 17,
          date: '20/07/2020',
          questionId: 5,
          answer: 'question ????',
          userId: '550731',
          attachments: {
            file: [],
            fileDelete: []
          },
          role: 'Risk-ฝ่ายบริหารความเสี่ยงสินเชื่อรายย่อย',
          edit_status: true,
          edit_comment_status: false,
          status_submit: false,
          dataCommentA: null
        }
      }
    ]
  };

  // download
  displayedColumns_download: string[] = ['name', 'size', 'download'];
  dataCommentQ_download: MatTableDataSource<FileList>;
  dataCommentA_download: MatTableDataSource<FileList>;
  dataCommentNew_download: MatTableDataSource<FileList>;
  table_download = [];

  // New comment
  fileNew = null;
  templateCommentsNew = {
    date: '',
    by: '',
    detail: '',
    attachments: {
      file: [],
      fileDelete: []
    },
  };
  btn_sand = true;
  // New answer
  newAnswer = [{
    newAnswerStatus: false,
    questionId: null,
    answer: null,
    attachments: {
      file: []
    },
    dataCommentA: null
  }];
  btn_sand_answer = false; // disabled ปุ่มส่งคำตอบ
  btn_newAnswerStatus = false; // แสดงปุ่มส่งคำตอบ

  // dialog
  deleteQuestionStatus = false;
  deleteAnswersStatus = false;

  // testfile = '
  testfile = {};
  status_loading = true;

  @Input() rolePageName = { role: null, validate: null };

  // role
  rolename: string;
  role_detail: string;
  userRole: string;
  newQuestionStatus = false; // สิทธ์ เพิ่มคำถาม
  newAnswerStatus = true; // สิทธ์ เพิ่มตอบคำถาม
  countAnswer = [];
  addStatus = true; // สิทธ์ เพิ่มคำถาม-คำตอบ
  validate = true; // สิทธ์ เเก้ไขคำถาม-คำตอบ ทังหมด

  OverSize: any;

  Compliance_1 = 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ธุรกิจ 1';
  Compliance_2 = 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ธุรกิจ 2';
  getHTML() {
    console.log('getHTML rolePageName=>', this.rolePageName);
  }
  // checkAnswer() {
  //   console.log('sendAnswerNew');
  //   this.commentManagementService.getQuestion_answer_PO(localStorage.getItem('requestId'), this.rolename).subscribe(res => {
  //     console.log('getQuestion_answer', res);
  //   });
  // }

  getQuestionPO() {
    console.log('getQuestionPO');
    console.log('this.newAnswer 1 ', this.newAnswer);
    console.log('aaaaa', this.rolePageName.role);
    this.commentManagementService.getQuestion_answer_PO(localStorage.getItem('requestId'), this.rolePageName.role).subscribe(res => {
      console.log('getQuestionPO', res['data']);
      this.data = res['data'];
      // set item
      this.newAnswer = [];
      this.table_download = [];
      this.data.result.forEach((ele, index) => {
        if (ele.question !== null) {
          if (ele.answer === null) {
            ele.question.edit_status = true;
          } else {
            ele.question.edit_status = false;
          }
          ele.question.edit_comment_status = false;
          ele.question.attachments.fileDelete = [];

          if (ele.question.attachments.file.length > 0) {
            ele.question.dataCommentQ = new MatTableDataSource(ele.question.attachments.file);
          } else {
            ele.question.dataCommentQ = new MatTableDataSource();
          }
        } else { this.dataCommentQ_download = new MatTableDataSource(); }

        if (ele.answer !== null) {
          ele.answer.edit_status = true;
          ele.answer.edit_comment_status = false;
          ele.answer.attachments.fileDelete = [];

          if (ele.answer.attachments.file.length > 0) {
            ele.answer.dataCommentA = new MatTableDataSource(ele.answer.attachments.file);
          } else {
            ele.answer.dataCommentA = new MatTableDataSource();
          }
          this.newAnswer.push({
            newAnswerStatus: false,
            questionId: ele.answer.questionId,
            answer: ele.answer.answer,
            attachments: {
              file: []
            },
            dataCommentA: null
          });
        } else {
          this.newAnswer.push({
            newAnswerStatus: true,
            questionId: ele.question.id,
            answer: null,
            attachments: {
              file: []
            },
            dataCommentA: null
          });
        }

      });
      this.dataCommentNew_download = new MatTableDataSource();
      console.log('this.newAnswer 2 555', this.newAnswer);
      this.checkNewAnswerStatus();
    });
    console.log('getQuestionPO005', this.data);
  }

  getQuestionBU() {
    console.log('getQuestionBU');
    this.commentManagementService.getQuestion_answer_BU(localStorage.getItem('requestId')).subscribe(res => {
      console.log('getQuestionBU', res['data']);
      this.data = res['data'];
      // set item
      this.table_download = [];
      this.data.result.forEach((ele, index) => {
        if (ele.question !== null) {
          if (ele.answer === null) {
            ele.question.edit_status = true;
          } else {
            ele.question.edit_status = false;
          }
          ele.question.edit_comment_status = false;
          ele.question.attachments.fileDelete = [];

          if (ele.question.attachments.file.length > 0) {
            ele.question.dataCommentQ = new MatTableDataSource(ele.question.attachments.file);
          } else {
            ele.question.dataCommentQ = new MatTableDataSource();
          }
        } else { this.dataCommentQ_download = new MatTableDataSource(); }

        if (ele.answer !== null) {
          ele.answer.edit_status = true;
          ele.answer.edit_comment_status = false;
          ele.answer.attachments.fileDelete = [];

          if (ele.answer.attachments.file.length > 0) {
            ele.answer.dataCommentA = new MatTableDataSource(ele.answer.attachments.file);
          } else {
            ele.answer.dataCommentA = new MatTableDataSource();
          }
        }

      });
      this.dataCommentNew_download = new MatTableDataSource();
      console.log('getQuestionBUPO005-2', this.data);
    });
  }

  checkTextNewAnswer() {
    let checkText = true;
    this.newAnswer.forEach(ele => {
      if (ele.answer === '' || ele.answer === null) {
        checkText = false;
      }
    });
    this.btn_sand_answer = checkText;
  }

  checkNewAnswerStatus() {
    let checkText = false;
    this.newAnswer.forEach(ele => {
      if (ele.newAnswerStatus) {
        checkText = true;
      }
    });
    if (this.rolename === 'PO') {
      this.btn_newAnswerStatus = checkText;
    } else {
      this.btn_newAnswerStatus = false;
    }
    console.log('this.newAnswerStatus', this.btn_newAnswerStatus);
  }


  sendQuestionNew() {
    console.log('sendQuestionNew');
    const questionNew = {
      requestId: localStorage.getItem('requestId'),
      question: this.templateCommentsNew.detail,
      attachments: this.templateCommentsNew.attachments
    };
    this.commentManagementService.sendQuestion(questionNew).subscribe(res => {
      console.log('sendQuestionNew');
      if (res['status'] === 'success') {
        console.log('sendQuestionNew');
        this.getQuestionBU();
      }
    });
    this.templateCommentsNew = {
      date: '',
      by: this.rolename,
      detail: '',
      attachments: {
        file: [],
        fileDelete: []
      },
    };
  }

  sendAnswerNew() {
    console.log('sendAnswerNew');
    this.newAnswer.forEach((ele, index) => {
      if (ele.newAnswerStatus) {
        const sendAnswer = {
          questionId: ele.questionId,
          answer: ele.answer,
          attachments: ele.attachments
        };
        console.log('sendAnswer ddd', sendAnswer);
        this.commentManagementService.sendAnswer(sendAnswer).subscribe(res => {
          if (res['status'] === 'success') {
            ele.newAnswerStatus = false;
          }
          if (this.newAnswer.length === (index + 1)) {
            console.log('===newAnswer===', this.newAnswer);
            this.getQuestionPO();
          }
        });
      } else {
        if (this.newAnswer.length === (index + 1)) {
          console.log('===newAnswer===', this.newAnswer);
          this.getQuestionPO();
        }
      }
    });

  }

  editQuestion(icomment) {
    const sendAnswer = {
      id: this.data.result[icomment].question.id,
      requestId: localStorage.getItem('requestId'),
      question: this.data.result[icomment].question.questionText,
      attachments: this.data.result[icomment].question.attachments
    };
    this.commentManagementService.editQuestion(sendAnswer).subscribe(res => {
      if (res['status'] === 'success') {
        this.getQuestionBU();
      }
    });
  }

  editAnswer(icomment) {
    const sendAnswer = {
      questionId: this.data.result[icomment].answer.questionId,
      answer: this.data.result[icomment].answer.answer,
      attachments: this.data.result[icomment].answer.attachments
    };
    this.commentManagementService.sendAnswer(sendAnswer).subscribe(res => {
      if (res['status'] === 'success') {
        this.getQuestionPO();
      }
    });
  }



  // File
  onSelectFile(event, tableName, index?) {
    this.OverSize = [];
    console.log('event', event);
    console.log('tableName', tableName);
    console.log('index', index);
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      const file = event.target.files;
      for (let i = 0; i < filesAmount; i++) {
        console.log('type:', file[i]);
        if (
          file[i].type === 'image/svg+xml' ||
          file[i].type === 'image/jpeg' ||
          // file[i].type === 'image/raw' ||
          // file[i].type === 'image/svg' ||
          // file[i].type === 'image/tif' ||
          // file[i].type === 'image/gif' ||
          file[i].type === 'image/jpg' ||
          file[i].type === 'image/png' ||
          file[i].type === 'application/doc' ||
          file[i].type === 'application/pdf' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
          file[i].type === 'application/pptx' ||
          file[i].type === 'application/docx' ||
          (file[i].type === 'application/ppt' && file[i].size)
        ) {
          if (file[i].size <= 5000000) {
            this.multiple_file(file[i], tableName, index);
          }
          else {
            this.OverSize.push(file[i].name);
          }
        }
        else {
          alert('File Not Support');
        }
      }
    }
    if (this.OverSize.length !== 0) {
      console.log('open modal');
      document.getElementById('testttt').click();
    }
  }

  multiple_file(file, tableName, index?) {
    console.log('download', file);
    const reader = new FileReader();
    if (file) {
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');

        const templateFile = {
          id: null,
          fileName: file.name,
          fileSize: file.size,
          base64File: splitFile[1],
        };
        console.log('aaaa', templateFile);
        this.testfile = templateFile;
        if (tableName === 'new') {
          this.templateCommentsNew.attachments.file.push(templateFile);
          this.dataCommentNew_download = new MatTableDataSource(this.templateCommentsNew.attachments.file);
          this.fileNew = null;
        } else if (tableName === 'newAnswer') {
          this.newAnswer[index].attachments.file.push(templateFile);
          this.newAnswer[index].dataCommentA = new MatTableDataSource(this.newAnswer[index].attachments.file);
          this.fileNew = null;
        } else if (tableName === 'question') {
          this.data.result[index].question.attachments.file.push(templateFile);
          this.data.result[index].question.dataCommentQ = new MatTableDataSource(this.data.result[index].question.attachments.file);
          // this.table_download[index].dataCommentQ = new MatTableDataSource(this.data.result[index].question.attachments.file);
          this.fileNew = null;
        } else if (tableName === 'answer') {
          this.data.result[index].answer.attachments.file.push(templateFile);
          this.data.result[index].answer.dataCommentA = new MatTableDataSource(this.data.result[index].answer.attachments.file);
          // this.table_download[index].dataCommentA = new MatTableDataSource(this.data.result[index].answer.attachments.file);
          this.fileNew = null;
        }
      };
    }
  }

  delete_mutifile(icomment, tableName, index) {
    // console.log('delete_mutifile icomment', icomment);
    // console.log('delete_mutifile tableName', tableName);
    // console.log('delete_mutifile index', index);
    if (tableName === 'question') {
      if (this.data.result[icomment].question.attachments.file[index].id !== null) {
        console.log('delete_question1');
        this.data.result[icomment].question.attachments.fileDelete.push(this.data.result[icomment].question.attachments.file[index].id);
      }
      this.data.result[icomment].question.attachments.file.splice(index, 1);
      this.data.result[icomment].question.dataCommentQ = new MatTableDataSource(this.data.result[icomment].question.attachments.file);
    } else if (tableName === 'answer') {
      if (this.data.result[icomment].answer.attachments.file[index].id !== null) {
        console.log('delete_answer 2');
        this.data.result[icomment].answer.attachments.fileDelete.push(this.data.result[icomment].answer.attachments.file[index].id);
      }
      this.data.result[icomment].answer.attachments.file.splice(index, 1);
      this.data.result[icomment].answer.dataCommentA = new MatTableDataSource(this.data.result[icomment].answer.attachments.file);

    } else if (tableName === 'new') {
      this.templateCommentsNew.attachments.file.splice(index, 1);
      this.dataCommentNew_download = new MatTableDataSource(this.templateCommentsNew.attachments.file);
    } else if (tableName === 'newAnswer') {
      this.newAnswer[icomment].attachments.file.splice(index, 1);
      this.newAnswer[icomment].dataCommentA = new MatTableDataSource(this.newAnswer[icomment].attachments.file);
    }
  }

  downloadFile(pathdata: any) {
    const contentType = '';
    const sendpath = pathdata;
    // console.log('path', sendpath);
    this.requestService.getfile(sendpath).subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        const datagetfile = res['data'];
        const b64Data = datagetfile['data'];
        const filename = datagetfile['fileName'];
        // console.log('base64', b64Data);
        const config = this.b64toBlob(b64Data, contentType);
        saveAs(config, filename);
      }

    });
  }

  b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const convertbyte = atob(b64Data);
    const byteCharacters = atob(convertbyte);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  downloadFile64(data: any) {
    this.status_loading = true;
    const contentType = '';
    this.status_loading = false;
    const b64Data = data.base64File;
    const filename = data.fileName;
    console.log('base64', b64Data);
    console.log('filename', filename);
    const config = this.base64(b64Data, contentType);
    saveAs(config, filename);
  }

  base64(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  check_edit_comment_status() {
    let checkComment = true;
    console.log('this.data.result 555', this.data.result);
    this.data.result.forEach(ele => {
      if (ele.question !== null && ele.question.edit_comment_status === true) {
        checkComment = false;
      }
      if (ele.answer !== null && ele.answer.edit_comment_status === true) {
        checkComment = false;
      }
    });
    this.btn_sand = checkComment;
    console.log('checkComment', checkComment);
  }

  deleteQuestion(index, id) {
    this.deleteQuestionStatus = false;
    const dialogRef = this.dialog.open(DialogCommentsComponent, {
      disableClose: true,
      data: { headerDetail: 'ยืนยันการลบคำถาม', bodyDetail: 'คุณแน่ใจหรือไม่? ว่าต้องการลบคำถาม', deleteQuestionStatus: false }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.deleteQuestionStatus = result.deleteStatus;
      if (this.deleteQuestionStatus) {
        this.commentManagementService.deleteQuestion(id).subscribe(res => {
          if (res['status'] === 'success') {
            this.getQuestionBU();
            Swal.fire({
              icon: 'success',
              title: 'คุณได้ลบคำถามแล้วเรียบร้อย',
              showConfirmButton: false,
            });
          }
        });
        this.data.result.splice(index, 1);
      }
    });
  }

  deleteAnswers(index, id) {
    this.deleteAnswersStatus = false;
    const dialogRef = this.dialog.open(DialogCommentsComponent, {
      disableClose: true,
      data: { headerDetail: 'ยืนยันการลบคำตอบ', bodyDetail: 'คุณแน่ใจหรือไม่? ว่าต้องการลบคำตอบ', deleteAnswersStatus: false }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.deleteAnswersStatus = result.deleteStatus;
      if (this.deleteAnswersStatus) {
        this.commentManagementService.deleteAnswer(id).subscribe(res => {
          if (res['status'] === 'success') {
            this.getQuestionPO();
            Swal.fire({
              icon: 'success',
              title: 'คุณได้ลบคำตอบแล้วเรียบร้อย',
              showConfirmButton: false,
            });
          }
        });
      }
    });
  }


  saveComment() {
    console.log('saveComment');
  }

  sendComment() {
    console.log('sendComment');
  }

  check_hr(index: any, head: string) {
    // console.log('check_hr index', index);
    // console.log('check_hr head', index);
    if (this.data.result.length > 0) {
      if (this.data.result.length === 1) {
        if (this.data.result[index].answer !== null) {
          if (head === 'question') {
            return true;
          } else if (head === 'answer') {
            return false;
          }
        } else { return false; }
      } else if (this.data.result.length === index + 1) {
        if (this.data.result[index].answer !== null) {
          if (head === 'question') {
            return true;
          } else if (head === 'answer') {
            return false;
          }
        } else { return false; }
      } else {
        if (this.data.result[index].answer !== null) {
          if (head === 'question') {
            return true;
          } else if (head === 'answer') {
            return true;
          }
        } else { return true; }
      }
    }
    return false;
  }






  constructor(
    public dialog: MatDialog,
    public commentManagementService: CommentManagementService,
    public requestService: RequestService,
  ) { }

  ngOnInit() {
    this.data.result = [];
    this.rolename = localStorage.getItem('role');
    this.role_detail = localStorage.getItem('ouName');
    console.log('role', localStorage.getItem('role'));
    this.userRole = localStorage.getItem('level');
    // กำหนดสิทธ์ New question
    if (this.rolename === 'Risk'
      || this.rolename === 'Operation'
      || this.rolename === 'Compliance'
      || this.rolename === 'ฝ่ายกำกับระเบียบและการปฏิบัติงาน'
      || this.rolename === 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ทางการ'
      || this.rolename === this.Compliance_1
      || this.rolename === this.Compliance_2
      || this.rolename === 'ฝ่ายนิติการ'
      || this.rolename === 'Finance'
      || this.rolename === 'Retail') {
      this.validate = this.rolePageName.validate;
      if (this.rolename === this.rolePageName.role) {
        this.newQuestionStatus = true;
        this.newAnswerStatus = false;
        this.showDateStatus = true;
        this.validate = this.rolePageName.validate;
        if (this.validate) {
          this.showHead = true;
        }
        this.getQuestionBU();
      } else if (this.rolename === 'Risk') {
        // console.log('eeeeeeee this.rolename', this.rolename);
        // console.log('eeeeeeee this.userRole', this.userRole);
        if (this.userRole === 'ฝ่าย'
          || this.userRole === 'กลุ่ม'
          || this.userRole === 'สาย'
          || this.userRole === 'แอดมิน'
        ) {
          this.rolePageName.role = 'Risk-พนักงาน';
          this.newQuestionStatus = true;
          this.newAnswerStatus = true;
          this.showDateStatus = true;
          this.validate = this.rolePageName.validate;
          if (this.validate) {
            this.showHead = true;
          } else { this.showHead = false; }
          this.getQuestionPO();
        } else if (this.rolename + '-' + this.userRole === this.rolePageName.role) {
          this.newQuestionStatus = true;
          this.newAnswerStatus = false;
          this.showDateStatus = true;
          this.validate = this.rolePageName.validate;
          if (this.validate) {
            this.showHead = true;
          }
          this.getQuestionBU();
        }
      } else if (this.rolename === 'ฝ่ายกำกับระเบียบและการปฏิบัติงาน'
        || this.rolename === 'ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ทางการ'
        || this.rolename === this.Compliance_1
        || this.rolename === this.Compliance_2
        || this.rolename === 'ฝ่ายนิติการ'
        || this.rolename === 'Compliance'
      ) {
        this.newQuestionStatus = true;
        this.newAnswerStatus = false;
        this.showDateStatus = true;
        this.validate = this.rolePageName.validate;
        if (this.validate) {
          this.showHead = true;
        }
        this.getQuestionBU();
      } else {
        this.newQuestionStatus = true;
        this.newAnswerStatus = true;
        this.showDateStatus = true;
        this.validate = this.rolePageName.validate;
        if (this.validate) {
          this.showHead = true;
        } else { this.showHead = false; }
        this.getQuestionPO();
      }
      this.getHTML();
    } else if (this.rolename === 'PO') {
      if (this.rolePageName.role === 'Risk-ฝ่าย'
        || this.rolePageName.role === 'Risk-กลุ่ม'
        || this.rolePageName.role === 'Risk-สาย'
        || this.rolePageName.role === 'Risk-แอดมิน'
      ) { this.rolePageName.role = 'Risk-พนักงาน'; }
      this.newQuestionStatus = false;
      this.newAnswerStatus = true;
      this.validate = this.rolePageName.validate;
      this.showDateStatus = true;
      this.showHead = false;
      this.getQuestionPO();
      this.getHTML();
    } else if (this.rolename === 'PC') {
      if (this.rolePageName.role === 'Risk-เลขานุการ'
        || this.rolePageName.role === 'Risk-คณะกรรมการ'
      ) { this.rolePageName.role = 'Risk-พนักงาน'; }
      this.showDateStatus = true;
      this.newQuestionStatus = false;
      this.newAnswerStatus = false;
      this.validate = this.rolePageName.validate;
      this.showHead = false;
      this.getQuestionPO();
    } else {
      console.log('0000000000');
      this.showDateStatus = true;
      this.newQuestionStatus = false;
      this.newAnswerStatus = false;
      this.validate = this.rolePageName.validate;
      this.showHead = false;
      this.getQuestionPO();
    }
  }
  autoGrowTextZone(e) {
    e.target.style.height = '0px';
    e.target.style.overflow = 'hidden';
    e.target.style.height = (e.target.scrollHeight + 0) + 'px';
  }

}
