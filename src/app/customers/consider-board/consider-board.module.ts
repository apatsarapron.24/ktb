import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConsiderBoardRoutingModule } from './consider-board-routing.module';
import { ConsiderBoardComponent } from './consider-board.component';

import { MatSelectModule} from '@angular/material/select';
import { FormsModule } from '@angular/forms';
import { MatTableModule, MatDialogModule} from '@angular/material';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule} from '@angular/material/radio';
import { AngularMyDatePickerModule } from 'angular-mydatepicker';
import { DateModule } from '../datepicker/date/date.module';
import {TextareaAutoresizeDirectiveModule} from '../../textarea-autoresize.directive/textarea-autoresize.directive.module';
import { CanDeactivateGuard } from '../../services/configGuard/config-guard.service';
import { DialogSaveStatusComponent} from '../consider-board/dialog-save-status/dialog-save-status.component'
@NgModule({
  declarations: [ConsiderBoardComponent, DialogSaveStatusComponent],
  imports: [
    CommonModule,
    ConsiderBoardRoutingModule,
    MatSelectModule,
    FormsModule,
    MatTableModule,
    MatCheckboxModule,
    MatRadioModule,
    AngularMyDatePickerModule,
    DateModule,
    TextareaAutoresizeDirectiveModule,
    MatDialogModule
  ],
  providers: [CanDeactivateGuard],
  entryComponents: [DialogSaveStatusComponent]
})
export class ConsiderBoardModule { }
