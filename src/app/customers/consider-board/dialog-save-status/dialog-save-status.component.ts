import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Router } from '@angular/router';

export interface DialogData {
  headerDetail: any;
  bodyDetail1: any;
  bodyDetail2: any;
  returnStatus: any;
  icon: any;
}

@Component({
  selector: 'app-dialog-save-status',
  templateUrl: './dialog-save-status.component.html',
  styleUrls: ['./dialog-save-status.component.scss']
})
export class DialogSaveStatusComponent implements OnInit {

  constructor(
    private router: Router,
    private dialogRef: MatDialogRef<DialogSaveStatusComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.dialogRef.disableClose = true;
  }

  ngOnInit() {
  }

  close() {
    this.data.returnStatus = null;
    this.dialogRef.close(this.data);
  }

  back() {
    this.data.returnStatus = false;
    this.dialogRef.close(this.data);
  }

  save() {
    this.data.returnStatus = true;
    this.dialogRef.close(this.data);
  }

}
