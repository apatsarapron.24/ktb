import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsiderBoardComponent } from './consider-board.component';
import { CanDeactivateGuard } from '../../services/configGuard/config-guard.service';
const routes: Routes = [
  {
    path: '',
    component: ConsiderBoardComponent,
    canDeactivate: [CanDeactivateGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsiderBoardRoutingModule { }
