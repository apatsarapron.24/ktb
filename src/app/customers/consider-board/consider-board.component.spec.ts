import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsiderBoardComponent } from './consider-board.component';

describe('ConsiderBoardComponent', () => {
  let component: ConsiderBoardComponent;
  let fixture: ComponentFixture<ConsiderBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsiderBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsiderBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
