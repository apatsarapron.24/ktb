import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import {
  AngularMyDatePickerDirective,
  IAngularMyDpOptions,
  IMyDateModel
} from 'angular-mydatepicker';
import { ConsiderBoardService } from '../../services/consider-board/consider-board.service';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { DateComponent } from '../datepicker/date/date.component';
import { DialogSaveStatusComponent } from '../consider-board/dialog-save-status/dialog-save-status.component';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
declare var $: any;
@Component({
  selector: 'app-consider-board',
  templateUrl: './consider-board.component.html',
  styleUrls: ['./consider-board.component.scss']
})
export class ConsiderBoardComponent implements OnInit {
  constructor(
    private router: Router,
    private considerBoardService: ConsiderBoardService,
    public dialog: MatDialog
  ) { }

  @ViewChild(DateComponent) Date: DateComponent;
  saveDraftstatus: any = '';
  // myDpOptions: IAngularMyDpOptions = {
  //   dateRange: false,
  //   dateFormat: 'dd/mm/yyyy',
  //   // other options are here...
  //   stylesData: {
  //     selector: 'dp',
  //     styles: `
  //          .dp .top:auto
  //          .dp .myDpIconLeftArrow,
  //          .dp .myDpIconRightArrow,
  //          .dp .myDpHeaderBtn {
  //              color: #3855c1;
  //           }
  //          .dp .myDpHeaderBtn:focus,
  //          .dp .myDpMonthLabel:focus,
  //          .dp .myDpYearLabel:focus {
  //              color: #3855c1;
  //           }
  //          .dp .myDpDaycell:focus,
  //          .dp .myDpMonthcell:focus,
  //          .dp .myDpYearcell:focus {
  //              box-shadow: inset 0 0 0 1px #66afe9;
  //           }
  //          .dp .myDpSelector:focus {
  //              border: 1px solid #ADD8E6;
  //           }
  //          .dp .myDpSelectorArrow:focus:before {
  //              border-bottom-color: #ADD8E6;
  //           }
  //          .dp .myDpCurrMonth,
  //          .dp .myDpMonthcell,
  //          .dp .myDpYearcell {
  //              color: #00a6e6;
  //           }
  //          .dp .myDpDaycellWeekNbr {
  //              color: #3855c1;
  //           }
  //          .dp .myDpPrevMonth,
  //          .dp .myDpNextMonth {
  //              color: #0098D2;
  //           }
  //          .dp .myDpWeekDayTitle {
  //              background-color: #0098D2;
  //              color: #ffffff;
  //           }
  //          .dp .myDpHeaderBtnEnabled:hover,
  //          .dp .myDpMonthLabel:hover,
  //          .dp .myDpYearLabel:hover {
  //              color:#0098D2;
  //           }
  //          .dp .myDpMarkCurrDay,
  //          .dp .myDpMarkCurrMonth,
  //          .dp .myDpMarkCurrYear {
  //              border-bottom: 2px solid #3855c1;
  //           }
  //          .dp .myDpDisabled {
  //              color: #999;
  //           }
  //          .dp .myDpHighlight {
  //              color: #74CBEC;
  //           }
  //          .dp .myDpTableSingleDay:hover,
  //          .dp .myDpTableSingleMonth:hover,
  //          .dp .myDpTableSingleYear:hover {
  //              background-color: #add8e6;
  //              color: #0098D2;
  //           }
  //          .dp .myDpRangeColor {
  //              background-color: #dbeaff;
  //           }
  //          .dp .myDpSelectedDay,
  //          .dp .myDpSelectedMonth,
  //          .dp .myDpSelectedYear {
  //              background-color: #0098D2;
  //              color: #ffffff;
  //           }
  //           `
  //   }
  // };
  model: any = null;
  saveDraftstatus_State = false;
  @ViewChild('dp') mydp: AngularMyDatePickerDirective;
  // dataconsider
  test: [{

  }]
  considerdata = {
    know: null,
    knowText: null,
    agree: null,
    agreeText: null,
    approve: null,
    approveText: null,
    operateOther: null,
    operateOtherText: null,
    presentation: null,
    presentationTime: null,
    presentationText: null,
    passDepartmentFirst: null,
    passDepartmentFirstText: null,
    BooleannotApprove: null,
    notApproveText: null,
    considerText: null,
    considerResultIssue: [
      {
        id: null,
        issueText: '',
        issuedata: [
          {
            title: 'Pending_1',
            value: 'Pending_1'
          },
          {
            title: 'Pending_2',
            value: 'Pending_2'
          },
          {
            title: 'Pending_3',
            value: 'Pending_3'
          },
          {
            title: 'Pending_4',
            value: 'Pending_4'
          },
          {
            title: 'No',
            value: 'No'
          }
        ],
      },
    ],
    deleteconsiderResultIssue: [],
  };

  processSelected = [];
  issuetexttest = [];
  idconsider = [];
  considerDate = '';
  addDataTest: any = {};
  listConsider: any = [];
  page = '';
  topic = '';
  detail = '';
  datacon: any;
  considerText = '';
  considerResultIssue: any;
  confirmState = 'confirm';
  // ลบข้อมูล
  dataid = [];
  datasenwork: any;
  status_loading = false;
  ngOnInit() {
    this.getConsiderResult();
  }
  // onDateChanged(event: IMyDateModel): void {
  //   // date selected
  // }
  changeDate(date: any) {
    const SendI = this.Date.changeDate(date);
    this.changeSaveDraft();
    return SendI;
  }
  // fontend <== backend
  getConsiderResult() {
    // this.status_loading = true;
    const requestid = {
      'requestId': localStorage.getItem('requestId'),
    };
    this.status_loading = true;
    this.considerBoardService.postconsiderResultAfterSend(requestid).subscribe(res => {
      // console.log('res:', res);
      if (res['status'] === 'success' && res['data']) {
        this.status_loading = false;
        // ส่งงาน
        this.page = 'true';
        const dataaftersend = res['data'];
        this.datacon = dataaftersend['considerResult'];
        this.considerText = dataaftersend['considerText'];
        this.considerResultIssue = dataaftersend['considerResultIssue'];
        console.log(dataaftersend);
        console.log(this.considerResultIssue);
      } else {
        // บันทึก
        this.page = 'false';
        this.status_loading = true;
        this.considerBoardService.postconsiderResult(requestid).subscribe(res => {
          console.log(res)
          if (res['status'] === 'success' && res['data']) {
            this.status_loading = false;
            const dataconsider = res['data'];
            // console.log(dataconsider);
            this.considerdata.know = dataconsider['know'];
            this.considerdata.knowText = dataconsider['knowText'];
            this.considerdata.agree = dataconsider['agree'];
            this.considerdata.agreeText = dataconsider['agreeText'];
            this.considerdata.approve = dataconsider['approve'];
            this.considerdata.approveText = dataconsider['approveText'];
            this.considerdata.operateOther = dataconsider['operateOther'];
            this.considerdata.operateOtherText = dataconsider['operateOtherText'];
            this.considerdata.presentation = dataconsider['presentation'];
            this.considerdata.presentationTime = dataconsider['presentationTime'];
            this.considerdata.presentationText = dataconsider['presentationText'];
            this.considerdata.passDepartmentFirst = dataconsider['passDepartmentFirst'];
            this.considerdata.passDepartmentFirstText = dataconsider['passDepartmentFirstText'];
            this.considerdata.BooleannotApprove = dataconsider['notApprove'];
            this.considerdata.notApproveText = dataconsider['notApproveText'];
            this.considerdata.considerText = dataconsider['considerText'];
            this.listConsider = dataconsider['considerResultIssue'];

            this.model = this.considerdata.presentationTime
            console.log('model', this.model);
            const value = moment(this.model).format('DD/MM/YYYY');
            console.log('value', value);
            this.Date.datepicker_input.dateData = this.model;
            this.Date.ngOnInit();


            // this.Date.endDate = value;
            this.considerdata.considerResultIssue = [];
            this.issuetexttest = [];
            for (let index = 0; index < this.listConsider.length; index++) {
              const element = this.listConsider[index];
              this.issuetexttest.push(element.issueText);
              this.considerdata.considerResultIssue.push({
                id: element.id,
                issueText: element.issueText,
                issuedata: [
                  {
                    title: 'Pending_1',
                    value: 'Pending_1'
                  },
                  {
                    title: 'Pending_2',
                    value: 'Pending_2'
                  },
                  {
                    title: 'Pending_3',
                    value: 'Pending_3'
                  },
                  {
                    title: 'Pending_4',
                    value: 'Pending_4'
                  },
                  {
                    title: 'No',
                    value: 'No'
                  }
                ]
              });
              //  this.processSelected = [element.issue];
            }
            console.log('123', this.considerdata.considerResultIssue);
            this.processSelected = [];
            // console.log(this.considerdata.considerResultIssue);
            for (let index = 0; index < this.listConsider.length; index++) {
              this.processSelected.push(this.listConsider[index].issue);
            }
            this.listConsider = [];
          } else {
            this.status_loading = false;
          }
        }, err => {
          this.status_loading = false;
          console.log(err);
        });
      }
    });
  }

  // fontend ==> backend updatesave send=false
  saveConsiderResult() {
    this.saveDraftstatus_State = false;
    if (this.model == null) {
      this.considerdata.presentationTime = null;
    } else {
      this.considerdata.presentationTime = this.model;
    }
    this.listConsider = [];
    for (let index = 0; index < this.considerdata.considerResultIssue.length; index++) {
      this.listConsider.push({
        id: null,
        issueText: '',
        issue: null
      });
      if (this.considerdata.considerResultIssue[index].id !== null) {
        this.listConsider[index].id = this.considerdata.considerResultIssue[index].id;
      }
      // console.log('datasend',this.issuetexttest);
      this.listConsider[index].issueText = this.issuetexttest[index];
    }

    // for (let index = 0; index < this.issuetexttest.length; index++) {
    //   // if (this.considerdata.considerResultIssue[index].issueText !== null) {
    //   console.log('issuetexttest',this.issuetexttest);
    //   this.listConsider[index].issueText = this.issuetexttest[index];
    //   // }
    // }

    for (let index = 0; index < this.processSelected.length; index++) {
      this.listConsider[index].issue = this.processSelected[index];
    }

    for (let index = 0; index < this.listConsider.length; index++) {
      if (this.listConsider[index].issue === null && this.listConsider[index].issueText === '' && this.listConsider[index].id === null || this.listConsider[index].issueText === undefined) {
        console.log('split', this.listConsider);
        this.listConsider.splice(index, 1);
      }
    }
    console.log('list save', this.listConsider);



    let _textboxknowText = false;
    let _textboxagreeText = false;
    let _textboxapproveText = false;
    let _textboxoperateOtherText = false;
    let _textboxpresentationText = false;
    let _textboxpassDepartmentFirstText = false;
    let _textboxnotApproveText = false;
    let _checktextbox = false;
    let _checktextboxknow = false;
    let _checktextboxagree = false;
    let _checktextboxapprove = false;
    let _checktextboxoperateOther = false;
    let _checktextboxpresentation = false;
    let _checktextboxpassDepartmentFirst = false;
    let _checktextboxBooleannotApprove = false;
    let _checkdatatosend = false;
    if (this.considerdata.know === true) {
      if (this.considerdata.knowText === '' || this.considerdata.knowText === null) {
        _textboxknowText = true;
      } else {
        _textboxknowText = false;
      }
    } else {
      _checktextboxknow = true;
      this.considerdata.knowText = null;
    }
    if (this.considerdata.agree === true) {
      if (this.considerdata.agreeText === '' || this.considerdata.agreeText === null) {
        _textboxagreeText = true;
      } else {
        _textboxagreeText = false;
      }
    } else {
      _checktextboxagree = true;
      this.considerdata.agreeText = null;
    }
    if (this.considerdata.approve === true) {
      if (this.considerdata.approveText === '' || this.considerdata.approveText === null) {
        _textboxapproveText = true;
      } else {
        _textboxapproveText = false;
      }
    } else {
      _checktextboxapprove = true;
      this.considerdata.approveText = null;
    }
    if (this.considerdata.operateOther === true) {
      if (this.considerdata.operateOtherText === '' || this.considerdata.operateOtherText === null) {
        _textboxoperateOtherText = true;
      } else {
        _textboxoperateOtherText = false;
      }
    } else {
      _checktextboxoperateOther = true;
      this.considerdata.operateOtherText = null;
    }
    if (this.considerdata.presentation === true) {
      if (this.considerdata.presentationText === '' || this.considerdata.presentationText === null) {
        _textboxpresentationText = true;
      } else {
        _textboxpresentationText = false;
      }
    } else {
      _checktextboxpresentation = true;
      this.considerdata.presentationText = null;
    }
    if (this.considerdata.passDepartmentFirst === true) {
      if (this.considerdata.passDepartmentFirstText === '' || this.considerdata.passDepartmentFirstText === null) {
        _textboxpassDepartmentFirstText = true;
      } else {
        _textboxpassDepartmentFirstText = false;
      }
    } else {
      _checktextboxpassDepartmentFirst = true;
      this.considerdata.passDepartmentFirstText = null;
    }
    if (this.considerdata.BooleannotApprove === true) {
      if (this.considerdata.notApproveText === '' || this.considerdata.notApproveText === null) {
        _textboxnotApproveText = true;
      } else {
        _textboxnotApproveText = false;
      }
    } else {
      _checktextboxBooleannotApprove = true;
      this.considerdata.notApproveText = null;
    }

    if (_textboxknowText === true || _textboxagreeText === true
      || _textboxapproveText === true || _textboxoperateOtherText === true
      || _textboxpresentationText === true || _textboxpassDepartmentFirstText === true
      || _textboxnotApproveText === true) {
      alert('กรุณาระบุรายละเอียดเพิ่มเติม');
      _checktextbox = true;
    }
    if ((this.considerdata.know === true || this.considerdata.agree === true
      || this.considerdata.approve === true || this.considerdata.operateOther === true
      || this.considerdata.presentation === true || this.considerdata.passDepartmentFirst === true
      || this.considerdata.BooleannotApprove === true) && _checktextbox === false) {

      const data = {
        'send': false,
        'requestId': localStorage.getItem('requestId'),
        'know': this.considerdata.know,
        'knowText': this.considerdata.knowText,
        'agree': this.considerdata.agree,
        'agreeText': this.considerdata.agreeText,
        'approve': this.considerdata.approve,
        'approveText': this.considerdata.approveText,
        'operateOther': this.considerdata.operateOther,
        'operateOtherText': this.considerdata.operateOtherText,
        'presentation': this.considerdata.presentation,
        'presentationTime': null,
        'presentationText': this.considerdata.presentationText,
        'passDepartmentFirst': this.considerdata.passDepartmentFirst,
        'passDepartmentFirstText': this.considerdata.passDepartmentFirstText,
        'notApprove': this.considerdata.BooleannotApprove,
        'notApproveText': this.considerdata.notApproveText,
        'considerText': this.considerdata.considerText,
        'considerResultIssue': this.listConsider,
        'deleteconsiderResultIssue': this.dataid,
      };

      if (this.considerdata.presentationTime) {
        data.presentationTime = moment(this.considerdata.presentationTime).format().substring(0, 10);
      } else { data.presentationTime = null; }
      console.log('data to send>>>', data);
      this.status_loading = true;
      this.considerBoardService.UpdateconsiderResult(data).subscribe(res => {
        console.log('sen seve', res);
        if (res['status'] === 'success') {
          this.status_loading = false;
          this.saveDraftstatus = res['status'];
          setTimeout(() => {
            this.saveDraftstatus = 'fail';
          }, 3000);
          this.listConsider = [];
          this.dataid = [];
          this.getConsiderResult();
          // this.issuetexttest = [];
        } else {
          Swal.fire({
            title: 'error',
            text: res['message'],
            icon: 'error',
            showConfirmButton: false,
            timer: 3000,
          });
          this.status_loading = false;
        }
      });
    }

    // check data checkbox = false all
    if (_checktextboxknow === true && _checktextboxagree === true && _checktextboxapprove === true
      && _checktextboxoperateOther === true && _checktextboxpresentation === true && _checktextboxpassDepartmentFirst === true && _checktextboxBooleannotApprove === true) {
      _checkdatatosend = true;
    }
    // save send = false if checkbox = false all
    if (_checkdatatosend === true) {
      const data = {
        'send': false,
        'requestId': localStorage.getItem('requestId'),
        'know': this.considerdata.know,
        'knowText': this.considerdata.knowText,
        'agree': this.considerdata.agree,
        'agreeText': this.considerdata.agreeText,
        'approve': this.considerdata.approve,
        'approveText': this.considerdata.approveText,
        'operateOther': this.considerdata.operateOther,
        'operateOtherText': this.considerdata.operateOtherText,
        'presentation': this.considerdata.presentation,
        'presentationTime': null,
        'presentationText': this.considerdata.presentationText,
        'passDepartmentFirst': this.considerdata.passDepartmentFirst,
        'passDepartmentFirstText': this.considerdata.passDepartmentFirstText,
        'notApprove': this.considerdata.BooleannotApprove,
        'notApproveText': this.considerdata.notApproveText,
        'considerText': this.considerdata.considerText,
        'considerResultIssue': this.listConsider,
        'deleteconsiderResultIssue': this.dataid,
      };

      if (this.considerdata.presentationTime) {
        data.presentationTime = moment(this.considerdata.presentationTime).format().substring(0, 10);
      } else { data.presentationTime = null; }
      console.log('data to send>>>', data);
      this.status_loading = true;
      this.considerBoardService.UpdateconsiderResult(data).subscribe(res => {
        console.log('sen seve', res);
        if (res['status'] === 'success') {
          this.status_loading = false;
          this.saveDraftstatus = res['status'];
          setTimeout(() => {
            this.saveDraftstatus = 'fail';
          }, 3000);
          this.listConsider = [];
          this.dataid = [];
          this.getConsiderResult();
          // this.issuetexttest = [];
        } else {
          Swal.fire({
            title: 'error',
            text: res['message'],
            icon: 'error',
            showConfirmButton: false,
            timer: 3000,
          });
          this.status_loading = false;
        }
      });
    }
  }

  checksendConsiderResult() {
    let _listissueText = false;
    let _listissue = false;
    this.saveDraftstatus_State = false;
    if (this.model == null) {
      this.considerdata.presentationTime = null;
    } else {
      this.considerdata.presentationTime = this.model;
    }
    this.listConsider = [];
    for (let index = 0; index < this.processSelected.length; index++) {
      this.listConsider.push({
        id: null,
        issueText: '',
        issue: this.processSelected[index]
      });
    }

    for (let index = 0; index < this.issuetexttest.length; index++) {
      if (this.issuetexttest.length !== 0 && this.processSelected[index] !== undefined) {
        this.listConsider[index].issueText = this.issuetexttest[index];
      }
      // console.log('list>>>>>>>', this.listConsider);
      // if (this.processSelected[index] === undefined || this.processSelected[index] === '') {
      //   _listissue = true;
      //   // break;
      //   console.log('_listissue ', _listissue);
      // } else {
      //   _listissue = false;
      //   console.log('_listissue ', _listissue);
      // }
    }

    for (let index = 0; index < this.considerdata.considerResultIssue.length; index++) {
      if (this.considerdata.considerResultIssue[index].id !== null) {
        this.listConsider[index].id = this.considerdata.considerResultIssue[index].id;
      }

      if (this.issuetexttest[index] === undefined || this.issuetexttest[index] === '') {
        _listissueText = true;
        console.log('_listissueText ', _listissueText);
        // break;
      } else {
        _listissueText = false;
        console.log('_listissueText ', _listissueText);
      }

      if (this.processSelected[index] === undefined || this.processSelected[index] === '') {
        _listissue = true;
        // break;
        console.log('_listissue ', _listissue);
      } else {
        _listissue = false;
        console.log('_listissue ', _listissue);
      }

      if (this.processSelected[index] === undefined && this.issuetexttest[index] === undefined) {
        _listissue = false;
        _listissueText = false;
      }

    }

    console.log('list', this.listConsider);
    let _textboxknowText = false;
    let _textboxagreeText = false;
    let _textboxapproveText = false;
    let _textboxoperateOtherText = false;
    let _textboxpresentationText = false;
    let _textboxpassDepartmentFirstText = false;
    let _textboxnotApproveText = false;
    let _checkflow = false;
    let _checklist = false;

    let _checktextboxknow = false;
    let _checktextboxagree = false;
    let _checktextboxapprove = false;
    let _checktextboxoperateOther = false;
    let _checktextboxpresentation = false;
    let _checktextboxpassDepartmentFirst = false;
    let _checktextboxBooleannotApprove = false;
    let _checkdatatosend = false;
    if (this.considerdata.know === true) {
      if (this.considerdata.knowText === '' || this.considerdata.knowText === null) {
        _textboxknowText = true;
      } else {
        _textboxknowText = false;
      }
    } else {
      _checktextboxknow = true;
      this.considerdata.knowText = null;
    }
    if (this.considerdata.agree === true) {
      if (this.considerdata.agreeText === '' || this.considerdata.agreeText === null) {
        _textboxagreeText = true;
      } else {
        _textboxagreeText = false;
      }
    } else {
      _checktextboxagree = true;
      this.considerdata.agreeText = null;
    }
    if (this.considerdata.approve === true) {
      if (this.considerdata.approveText === '' || this.considerdata.approveText === null) {
        _textboxapproveText = true;
      } else {
        _textboxapproveText = false;
      }
    } else {
      _checktextboxapprove = true;
      this.considerdata.approveText = null;
    }
    if (this.considerdata.operateOther === true) {
      if (this.considerdata.operateOtherText === '' || this.considerdata.operateOtherText === null) {
        _textboxoperateOtherText = true;
      } else {
        _textboxoperateOtherText = false;
      }
    } else {
      _checktextboxoperateOther = true;
      this.considerdata.operateOtherText = null;
    }
    if (this.considerdata.presentation === true) {
      if (this.considerdata.presentationText === '' || this.considerdata.presentationText === null) {
        _textboxpresentationText = true;
      } else {
        _textboxpresentationText = false;
      }
    } else {
      _checktextboxpresentation = true;
      this.considerdata.presentationText = null;
    }
    if (this.considerdata.passDepartmentFirst === true) {
      if (this.considerdata.passDepartmentFirstText === '' || this.considerdata.passDepartmentFirstText === null) {
        _textboxpassDepartmentFirstText = true;
      } else {
        _textboxpassDepartmentFirstText = false;
      }
    } else {
      _checktextboxpassDepartmentFirst = true;
      this.considerdata.passDepartmentFirstText = null;
    }
    if (this.considerdata.BooleannotApprove === true) {
      if (this.considerdata.notApproveText === '' || this.considerdata.notApproveText === null) {
        _textboxnotApproveText = true;
      } else {
        _textboxnotApproveText = false;
      }
    } else {
      _checktextboxBooleannotApprove = true;
      this.considerdata.notApproveText = null;
    }

    if (_checktextboxknow === true && _checktextboxagree === true && _checktextboxapprove === true
      && _checktextboxoperateOther === true && _checktextboxpresentation === true && _checktextboxpassDepartmentFirst === true && _checktextboxBooleannotApprove === true) {
      _checkdatatosend = true;
    }
    if (_listissue === true || _listissueText === true) {
      _checklist = false;
    } else {
      _checklist = true;
    }
    console.log('hi', _checklist)

    if (_textboxknowText === true || _textboxagreeText === true
      || _textboxapproveText === true || _textboxoperateOtherText === true
      || _textboxpresentationText === true || _textboxpassDepartmentFirstText === true
      || _textboxnotApproveText === true) {
      if ((this.considerdata.considerText === '' || this.considerdata.considerText === null) && _checklist === false) {
        alert('กรุณาระบุรายละเอียดเพิ่มเติม ประเด็นพิจารณา และระบุประเด็นคงค้าง / สิ่งที่ต้องดำเนินการตามความเห็นคณะกรรมการผลิตภัณฑ์');
      } else if (this.considerdata.considerText === '' || this.considerdata.considerText === null) {
        alert('กรุณาระบุรายละเอียดเพิ่มเติม และประเด็นพิจารณา');
      } else if (_checklist === false) {
        alert('กรุณาะบุรายละเอียดเพิ่มเติม และระบุประเด็นคงค้าง / สิ่งที่ต้องดำเนินการตามความเห็นคณะกรรมการผลิตภัณฑ์');
      } else {
        alert('กรุณาระบุรายละเอียดเพิ่มเติม');
      }
    } else if (this.considerdata.considerText === '' || this.considerdata.considerText === null) {
      if ((_checklist === false) && _checkdatatosend === true) {
        alert('กรุณาระบุรายละเอียดประเด็นพิจารณา ระบุผลการพิจารณา และระบุประเด็นคงค้าง / สิ่งที่ต้องดำเนินการตามความเห็นคณะกรรมการผลิตภัณฑ์');
      } else if (_checklist === false) {
        alert('กรุณาระบุรายละเอียดประเด็นพิจารณา และระบุประเด็นคงค้าง / สิ่งที่ต้องดำเนินการตามความเห็นคณะกรรมการผลิตภัณฑ์');
      } else if (_checkdatatosend === true) {
        alert('กรุณาระบุรายละเอียดประเด็นพิจารณา และระบุผลการพิจารณา');
      } else {
        alert('กรุณาระบุรายละเอียดประเด็นพิจารณา');
      }
      _checkflow = false;
    } else if (_checklist === false) {
      if (_checkdatatosend === true) {
        alert('กรุณาระบุประเด็นคงค้าง / สิ่งที่ต้องดำเนินการตามความเห็นคณะกรรมการผลิตภัณฑ์ และระบุผลการพิจารณา');
      } else {
        alert('กรุณาระบุประเด็นคงค้าง / สิ่งที่ต้องดำเนินการตามความเห็นคณะกรรมการผลิตภัณฑ์');
      }
      _checkflow = false;
    } else if (_checkdatatosend === true) {
      alert('กรุณาระบุผลการพิจารณา');
      _checkflow = false;
    }
    else {
      _checkflow = true;
    }


    if ((this.considerdata.know === true || this.considerdata.agree === true
      || this.considerdata.approve === true || this.considerdata.operateOther === true
      || this.considerdata.presentation === true || this.considerdata.passDepartmentFirst === true
      || this.considerdata.BooleannotApprove === true) && _checkflow === true && this.considerdata.considerText) {
      $('#confirmModal').modal('show');
    }
  }
  // fontend ==> backend updatesave send=true
  sendConsiderResult() {
    const data = {
      'send': true,
      'requestId': localStorage.getItem('requestId'),
      'know': this.considerdata.know,
      'knowText': this.considerdata.knowText,
      'agree': this.considerdata.agree,
      'agreeText': this.considerdata.agreeText,
      'approve': this.considerdata.approve,
      'approveText': this.considerdata.approveText,
      'operateOther': this.considerdata.operateOther,
      'operateOtherText': this.considerdata.operateOtherText,
      'presentation': this.considerdata.presentation,
      'presentationTime': null,
      'presentationText': this.considerdata.presentationText,
      'passDepartmentFirst': this.considerdata.passDepartmentFirst,
      'passDepartmentFirstText': this.considerdata.passDepartmentFirstText,
      'notApprove': this.considerdata.BooleannotApprove,
      'notApproveText': this.considerdata.notApproveText,
      'considerText': this.considerdata.considerText,
      'considerResultIssue': this.listConsider,
      'deleteconsiderResultIssue': this.dataid,
    };
    console.log('datasend', data);
    if (this.considerdata.presentationTime) {
      data.presentationTime = moment(this.considerdata.presentationTime).format().substring(0, 10);
    } else { data.presentationTime = null; }
    // console.log(data.presentationTime);

    this.considerBoardService.UpdateconsiderResult(data).subscribe(res => {
      console.log(res);
      if (res['status'] === 'success') {
        this.confirmState = 'success';
        setTimeout(() => {
          $('#confirmModal').modal('hide');
          this.confirmState = 'confirm';
          this.getConsiderResult();
          this.page = 'true';
        }, 3000);
      } else {
        Swal.fire({
          title: 'error',
          text: res['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 3000,
        });
      }
    }, err => {
      console.log(err);
    });
  }
  // adddata ปุ่มเพิ่มข้อมูล

  addData() {
    this.changeSaveDraft();
    const templateaddrow = {
      id: null,
      issueText: '',
      issuedata: [
        {
          title: 'Pending_1',
          value: 'Pending_1'
        },
        {
          title: 'Pending_2',
          value: 'Pending_2'
        },
        {
          title: 'Pending_3',
          value: 'Pending_3'
        },
        {
          title: 'Pending_4',
          value: 'Pending_4'
        },
        {
          title: 'No',
          value: 'No'
        }
      ],
    };
    this.considerdata.considerResultIssue.push(templateaddrow);
    console.log('this.considerdata.considerResultIssue', this.considerdata.considerResultIssue);
  }
  removedata(indexdata, dataid: number) {
    this.changeSaveDraft();
    this.dataid.push(dataid);
    console.log(this.dataid);
    console.log(indexdata);
    if (this.considerdata.considerResultIssue.length !== 1) {
      this.considerdata.considerResultIssue.splice(indexdata, 1);
      this.processSelected.splice(indexdata, 1);
      this.issuetexttest.splice(indexdata, 1);
    } else {
      this.issuetexttest = [];
      this.processSelected = [];
    }

  }

  backToDashboardPage() {
    // localStorage.setItem('requestId', '');
    this.router.navigate(['/dashboard1']);
  }

  changeSaveDraft() {
    this.saveDraftstatus_State = true;
    this.considerBoardService.status_state = true;
    console.log(this.saveDraftstatus_State);
  }
  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate');
    // return confirm();
    const subject = new Subject<boolean>();
    // const status = this.sidebarService.outPageStatus();
    console.log('000 sidebarService status:', status);
    if (this.saveDraftstatus_State === true) {
      // console.log('000 sidebarService status:', status);    let _textboxknowText = false;
      let _textboxknowText = false;
      let _textboxagreeText = false;
      let _textboxapproveText = false;
      let _textboxoperateOtherText = false;
      let _textboxpresentationText = false;
      let _textboxpassDepartmentFirstText = false;
      let _textboxnotApproveText = false;
      let _checknext = false;
      if (this.considerdata.know === true) {
        if (this.considerdata.knowText === '' || this.considerdata.knowText === null) {
          _textboxknowText = true;
        } else {
          _textboxknowText = false;
        }
      } else {
        this.considerdata.knowText = null;
      }
      if (this.considerdata.agree === true) {
        if (this.considerdata.agreeText === '' || this.considerdata.agreeText === null) {
          _textboxagreeText = true;
        } else {
          _textboxagreeText = false;
        }
      } else {
        this.considerdata.agreeText = null;
      }
      if (this.considerdata.approve === true) {
        if (this.considerdata.approveText === '' || this.considerdata.approveText === null) {
          _textboxapproveText = true;
        } else {
          _textboxapproveText = false;
        }
      } else {
        this.considerdata.approveText = null;
      }
      if (this.considerdata.operateOther === true) {
        if (this.considerdata.operateOtherText === '' || this.considerdata.operateOtherText === null) {
          _textboxoperateOtherText = true;
        } else {
          _textboxoperateOtherText = false;
        }
      } else {
        this.considerdata.operateOtherText = null;
      }
      if (this.considerdata.presentation === true) {
        if (this.considerdata.presentationText === '' || this.considerdata.presentationText === null) {
          _textboxpresentationText = true;
        } else {
          _textboxpresentationText = false;
        }
      } else {
        this.considerdata.presentationText = null;
      }
      if (this.considerdata.passDepartmentFirst === true) {
        if (this.considerdata.passDepartmentFirstText === '' || this.considerdata.passDepartmentFirstText === null) {
          _textboxpassDepartmentFirstText = true;
        } else {
          _textboxpassDepartmentFirstText = false;
        }
      } else {
        this.considerdata.passDepartmentFirstText = null;
      }
      if (this.considerdata.BooleannotApprove === true) {
        if (this.considerdata.notApproveText === '' || this.considerdata.notApproveText === null) {
          _textboxnotApproveText = true;
        } else {
          _textboxnotApproveText = false;
        }
      } else {
        this.considerdata.notApproveText = null;
      }

      if (_textboxknowText === true || _textboxagreeText === true
        || _textboxapproveText === true || _textboxoperateOtherText === true
        || _textboxpresentationText === true || _textboxpassDepartmentFirstText === true
        || _textboxnotApproveText === true) {
        // alert('กรุณาระบุรายละเอียดเพิ่มเติม');
        _checknext = true;
      }
      const dialogRef = this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        console.log('data::', data);
        if (data.returnStatus === true) {
          if (_checknext === true) {
            alert('กรุณาระบุรายละเอียดเพิ่มเติม');
            subject.next(false);
          } else {
            subject.next(true);
            this.saveConsiderResult();
            this.saveDraftstatus_State = false;
          }
        } else if (data.returnStatus === false) {
          console.log('000000 data::', data.returnStatus);
          // this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
        }
      });
      console.log('8889999', subject);
      return subject;
    } else {
      return true;
    }
  }

  autoGrowTextZone(e) {
    e.target.style.height = '0px';
    e.target.style.overflow = 'hidden';
    e.target.style.height = (e.target.scrollHeight + 0) + 'px';
  }
}
