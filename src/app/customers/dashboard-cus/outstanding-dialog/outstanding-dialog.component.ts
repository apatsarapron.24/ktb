import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { DashboardService } from '../../../services/dashboard/dashboard.service';
import { DashboardCusComponent } from '../dashboard-cus.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-outstanding-dialog',
  templateUrl: './outstanding-dialog.component.html',
  styleUrls: ['./outstanding-dialog.component.scss']
})
export class OutstandingDialogComponent implements OnInit {
  requestid: any;
  dataissue: any = [];
  owner: any;

  constructor(
    private dialogRef: MatDialogRef<DashboardCusComponent>,
    public _Service: DashboardService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.getsummaryIssue();
  }

  getsummaryIssue() {
    this._Service.getsummaryIssue(this.requestid).subscribe(res => {
      if (res['data']) {
        this.dataissue = res['data'];
        // console.log(this.dataissue)
      }
    });
  }

  closeDialog() {
    this.dialogRef.close();
  }

  openOutstanding() {
    this.closeDialog()
    localStorage.setItem('requestId', this.requestid);
    this.router.navigate(['outstanding']);
  }

}
