import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {
  AngularMyDatePickerDirective,
  IAngularMyDpOptions,
  IMyDateModel
} from 'angular-mydatepicker';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { DashboardService } from './../../services/dashboard/dashboard.service';
import { DashboardSearchService } from './../../services/dashboardSearch/dashboard-search.service';
import { CreatePermissionService } from './../../services/managePermission/create-permission/create-permission.service';
import { DateAdapter, MatDialog } from '@angular/material';
import { RequestService } from './../../services/request/request.service';
import { from } from 'rxjs';
import { constructDependencies } from '@angular/core/src/di/reflective_provider';
import { OutstandingDialogComponent } from './outstanding-dialog/outstanding-dialog.component';

export interface Data {
  status: Number;
  no: string;
  product: string;
  plan: string;
  object: string;
  team: string;
  group: string;
  department: string;
  date: string;
  status_po: string;
  Risk: string;
  Compliance: string;
  Finance: string;
  Operation: string;
  Retail: string;
  story: string;
  date_pc: string;
  date_an: string;
  date_t: string;
  date_t_update: string;
}

export interface Result {
  status: string;
  no: string;
  product: string;
  plan: string;
  object: string;
  team: string;
  group: string;
  department: string;
  date: string;
  status_po: string;
  Risk: string;
  Compliance: string;
  Finance: string;
  Operation: string;
  Retail: string;
  story: string;
  date_pc: string;
  date_an: string;
  date_t: string;
  date_t_update: string;
}
const Data: Data[] = [
  {
    status: 30,
    no: 'APP62024',
    product: 'Krungthai A',
    plan: 'Plan',
    object: 'ผลิตภัณฑ์ใหม่',
    team: 'ภาวนา สุดใจ',
    group: 'กลยุทธ์ SME',
    department: 'ธุรกิจขนาดเล็ก',
    date: '01/01/2563',
    status_po: 'PO จัดทำใบคำขอ',
    Risk: 'S',
    Compliance: 'S',
    Finance: 'S',
    Operation: 'S',
    Retail: 'S',
    story: '2/5',
    date_pc: '01/01/2563',
    date_an: '-',
    date_t: '-',
    date_t_update: '-',
  },
  {
    status: 40,
    no: 'APP62025',
    product: 'Krungthai B',
    plan: 'Plan',
    object: 'ผลิตภัณฑ์ใหม่',
    team: 'ภาวนา สุดใจ',
    group: 'กลยุทธ์ SME',
    department: 'ธุรกิจขนาดเล็ก',
    date: '01/01/2563',
    status_po: 'PO จัดทำใบคำขอ',
    Risk: 'S',
    Compliance: 'S',
    Finance: 'S',
    Operation: 'S',
    Retail: 'S',
    story: '2/5',
    date_pc: '01/01/2563',
    date_an: '-',
    date_t: '-',
    date_t_update: '-',
  },
  {
    status: 0,
    no: 'APP62025',
    product: 'Krungthai C',
    plan: 'Plan',
    object: 'ผลิตภัณฑ์ใหม่',
    team: 'ภาวนา สุดใจ',
    group: 'กลยุทธ์ SME',
    department: 'ธุรกิจขนาดเล็ก',
    date: '01/01/2563',
    status_po: 'PO จัดทำใบคำขอ',
    Risk: 'A',
    Compliance: 'S',
    Finance: 'S',
    Operation: 'S',
    Retail: 'S',
    story: '2/5',
    date_pc: '01/01/2563',
    date_an: '-',
    date_t: '-',
    date_t_update: '-',
  },
  {
    status: 60,
    no: 'APP62025',
    product: 'Krungthai D',
    plan: 'Plan',
    object: 'ผลิตภัณฑ์ใหม่',
    team: 'ภาวนา สุดใจ',
    group: 'กลยุทธ์ SME',
    department: 'ธุรกิจขนาดเล็ก',
    date: '01/01/2563',
    status_po: 'PO จัดทำใบคำขอ',
    Risk: 'D',
    Compliance: 'S',
    Finance: 'S',
    Operation: 'S',
    Retail: 'S',
    story: '2/7',
    date_pc: '01/01/2563',
    date_an: '-',
    date_t: '-',
    date_t_update: '-',
  },
  {
    status: 61,
    no: 'APP62025',
    product: 'Krungthai F',
    plan: 'Plan',
    object: 'ผลิตภัณฑ์ใหม่',
    team: 'ภาวนา สุดใจ',
    group: 'กลยุทธ์ SME',
    department: 'ธุรกิจขนาดเล็ก',
    date: '01/01/2563',
    status_po: 'PO จัดทำใบคำขอ',
    Risk: 'C',
    Compliance: 'S',
    Finance: 'S',
    Operation: 'S',
    Retail: 'S',
    story: '2/5',
    date_pc: '01/01/2563',
    date_an: '-',
    date_t: '-',
    date_t_update: '-',
  },
  {
    status: 52,
    no: 'APP62025',
    product: 'Krungthai G',
    plan: 'Plan',
    object: 'ผลิตภัณฑ์ใหม่',
    team: 'ภาวนา สุดใจ',
    group: 'กลยุทธ์ SME',
    department: 'ธุรกิจขนาดเล็ก',
    date: '01/01/2563',
    status_po: 'PO จัดทำใบคำขอ',
    Risk: 'G',
    Compliance: 'S',
    Finance: 'S',
    Operation: 'S',
    Retail: 'S',
    story: '2/5',
    date_pc: '01/01/2563',
    date_an: '-',
    date_t: '-',
    date_t_update: '-',
  },
  {
    status: 2,
    no: 'APP62025',
    product: 'Krungthai H',
    plan: 'Plan',
    object: 'ผลิตภัณฑ์ใหม่',
    team: 'ภาวนา สุดใจ',
    group: 'กลยุทธ์ SME',
    department: 'ธุรกิจขนาดเล็ก',
    date: '01/01/2563',
    status_po: 'PO จัดทำใบคำขอ',
    Risk: 'S',
    Compliance: 'S',
    Finance: 'S',
    Operation: 'S',
    Retail: 'S',
    story: '2/4',
    date_pc: '01/01/2563',
    date_an: '-',
    date_t: '-',
    date_t_update: '-',
  },
  {
    status: 2,
    no: 'APP62025',
    product: 'Krungthai R',
    plan: 'Plan',
    object: 'ผลิตภัณฑ์ใหม่',
    team: 'ภาวนา สุดใจ',
    group: 'กลยุทธ์ SME',
    department: 'ธุรกิจขนาดเล็ก',
    date: '01/01/2563',
    status_po: 'PO จัดทำใบคำขอ',
    Risk: 'S',
    Compliance: 'S',
    Finance: 'S',
    Operation: 'S',
    Retail: 'S',
    story: '2/5',
    date_pc: '01/01/2563',
    date_an: '-',
    date_t: '-',
    date_t_update: '-',
  },
  {
    status: 70,
    no: 'APP62025',
    product: 'Krungthai I',
    plan: 'Plan',
    object: 'ผลิตภัณฑ์ใหม่',
    team: 'ภาวนา สุดใจ',
    group: 'กลยุทธ์ SME',
    department: 'ธุรกิจขนาดเล็ก',
    date: '01/01/2563',
    status_po: 'PO จัดทำใบคำขอ',
    Risk: 'L',
    Compliance: 'S',
    Finance: 'S',
    Operation: 'S',
    Retail: 'S',
    story: '2/5',
    date_pc: '01/01/2563',
    date_an: '-',
    date_t: '-',
    date_t_update: '-',
  },
  {
    status: 52,
    no: 'APP62025',
    product: 'Krungthai Q',
    plan: 'Plan',
    object: 'ผลิตภัณฑ์ใหม่',
    team: 'ภาวนา สุดใจ',
    group: 'กลยุทธ์ SME',
    department: 'ธุรกิจขนาดเล็ก',
    date: '01/01/2563',
    status_po: 'PO จัดทำใบคำขอ',
    Risk: 'S',
    Compliance: 'S',
    Finance: 'S',
    Operation: 'S',
    Retail: 'S',
    story: '2/1',
    date_pc: '01/01/2563',
    date_an: '-',
    date_t: '-',
    date_t_update: '-',
  },
  {
    status: 70,
    no: 'APP62025',
    product: 'Krungthai Y',
    plan: 'Plan',
    object: 'ผลิตภัณฑ์ใหม่',
    team: 'ภาวนา สุดใจ',
    group: 'กลยุทธ์ SME',
    department: 'ธุรกิจขนาดเล็ก',
    date: '01/01/2563',
    status_po: 'PO จัดทำใบคำขอ',
    Risk: 'S',
    Compliance: 'S',
    Finance: 'S',
    Operation: 'S',
    Retail: 'S',
    story: '2/4',
    date_pc: '01/01/2563',
    date_an: '-',
    date_t: '-',
    date_t_update: '-',
  },
  {
    status: 52,
    no: 'APP62025',
    product: 'Krungthai T',
    plan: 'Plan',
    object: 'ผลิตภัณฑ์ใหม่',
    team: 'ภาวนา สุดใจ',
    group: 'กลยุทธ์ SME',
    department: 'ธุรกิจขนาดเล็ก',
    date: '01/01/2563',
    status_po: 'PO จัดทำใบคำขอ',
    Risk: 'S',
    Compliance: 'S',
    Finance: 'S',
    Operation: 'S',
    Retail: 'S',
    story: '2/6',
    date_pc: '01/01/2563',
    date_an: '-',
    date_t: '-',
    date_t_update: '-',
  },
];

@Component({
  selector: 'app-dashboard-cus',
  templateUrl: './dashboard-cus.component.html',
  styleUrls: ['./dashboard-cus.component.scss'],
})
export class DashboardCusComponent implements OnInit {
  displayedColumns: string[] = [
    'status1',
    'product1',
    'no1',
    'object1',
    'status_po1',
    'Risk',
    'Compliance',
    'Finance',
    'Operation',
    'Retail',
    'story1',
    'team1',
    'group1',
    'department1',
    'date1',
    'date_pc1',
    'date_an1',
    'plan1',
    'date_t1',
    'date_t_update1',
  ];
  // MatPaginator Inputs
  length = 100;
  pageSize = 10;
  pageSizeOptions: number[] = [10, 15, 25, 100];

  length_search = 100;
  pageSize_search = 10;
  pageSizeOptions_search: number[] = [10, 15, 25, 100];


  // MatPaginator Output
  pageEvent: PageEvent;
  pageEventSearch: PageEvent;

  dataSource: MatTableDataSource<Data>;
  Result_data: MatTableDataSource<Result>;

  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild('paginatorSearch') paginatorSearch: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  results: any;
  results_search: any;

  model_date: IMyDateModel = null;
  selectedProduct = '';
  department = '';
  workgroup = '';
  division = '';
  status_date = '';
  status_object = '';
  start_date: any;
  end_date: any;
  searchInput: any;

  search_data = false;
  status_loading = true;
  divisionList: any;
  workgroupList: any;
  departmentList: any;
  divisionList_fill: any;
  workgroupList_fill: any;
  departmentList_fill: any;
  fillterStatus = false;
  changResetStatus = true;
  fromSend_department = '';
  fromSend_workgroup = '';
  fromSend_division = '';
  department_ouCode = '';
  workgroup_ouCode = '';
  division_ouCode = '';

  @ViewChild('dp') mydp: AngularMyDatePickerDirective;

  result: any;
  data_s = false;
  requestId = '';
  dateRange = [];
  role = '';
  level = '';

  status_product: any = [
    // { name: '', status: 'รอสร้างใบคำขอ' },
    { name: 'DRAFT', status: 'PO จัดทำใบคำขอ' },
    { name: 'VERIFLY', status: 'ผู้บริหารของ PO พิจารณาใบคำขอ' },
    { name: 'BU_VERIFY', status: 'หน่วยงานที่เกี่ยวข้องให้ความเห็น' },
    { name: 'PO_COMMENT_OTHER', status: 'PO ให้ความเห็นเพิ่มเติม' },
    { name: 'VERIFY2', status: 'ผู้บริหารของ PO พิจารณาใบคำขอ' },
    { name: 'PC_AGENDA', status: 'PC พิจารณาบรรจุวาระ' },
    { name: 'PC_RESULTS', status: 'PC บันทึกผลการอนุมัติ' },
    { name: 'PO_CONCLUSIONS', status: 'PO สรุปผลการอนุมัติ' },
    { name: 'PC_CONCLUSIONS', status: 'PC พิจารณาการสรุปผลการอนุมัติ' },
    { name: 'PO_ISSUE', status: 'สรุปประเด็นคงค้าง' },
    { name: 'WAITING_ANNOUCEMENT', status: 'รอการประกาศใช้ผลิตภัณฑ์' },
    { name: 'ANNOUCEMENT', status: 'ประกาศใช้ผลิตภัณฑ์แล้ว' },
  ];
  constructor(
    private http: HttpClient,
    private router: Router,
    public dashboard: DashboardService,
    public dashboardsearch: DashboardSearchService,
    private createPermissionService: CreatePermissionService,
    private adapter: DateAdapter<any>,
    private statusRequest: RequestService,
    public dialog: MatDialog,
  ) {
    this.adapter.setLocale('th-TH');
    // Create 100 users
    // const users = Array.from({ length: 100 }, (_, k) => createNewUser(k + 1));
    // Assign the data to the data source for the table to render
    // this.dataSource = new MatTableDataSource(Data);
  }

  createRequest() {
    this.router.navigate(['/request/information-base/step1']);
    localStorage.setItem('requestId', this.requestId);
  }

  ngOnInit() {
    localStorage.setItem('requestId', '');
    this.role = localStorage.getItem('role');
    this.level = localStorage.getItem('level');
    this.setupCalendar(0);
    this.dashboard.getDashboard().subscribe((res) => {
      // console.log('pppppp', res);
      this.results = res;
      if (res['status'] === 'success') {
        this.status_loading = false;

      }
      console.log(this.results);
      this.results.data.forEach((data, index) => {
        // data.id = index + 1;
        if (data.createAt !== null) {
          data.createAt = moment((data.createAt).slice(0, 10)).format('DD/MM/YYYY');
        } else {
          data.createAt = '-';
        }
        if (data.releaseDate !== null) {
          data.releaseDate = moment((data.releaseDate).slice(0, 10)).format('DD/MM/YYYY');
        } else {
          data.releaseDate = '-';
        }
        if (data.presentationDate !== null) {
          data.presentationDate = moment((data.presentationDate).slice(0, 10)).format(
            'DD/MM/YYYY'
          );
        } else {
          data.presentationDate = '-';
        }
        if (data.lastReviewDate !== null) {
          data.lastReviewDate = moment((data.lastReviewDate).slice(0, 10)).format(
            'DD/MM/YYYY'
          );
        } else {
          data.lastReviewDate = '-';
        }
        if (data.nextReviewDate !== null) {
          data.nextReviewDate = moment((data.nextReviewDate).slice(0, 10)).format(
            'DD/MM/YYYY'
          );
        } else {
          data.nextReviewDate = '-';
        }
        // console.log()
      });

      // this.length = this.results.length;
      this.dataSource = new MatTableDataSource(this.results.data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });

    this.createPermissionService.divisionList().subscribe((res) => {
      this.divisionList = res['data'];
      this.divisionList_fill = JSON.parse(JSON.stringify(res['data']));
      // console.log('divisionList', this.divisionList);
    });
    this.createPermissionService.workgroupList().subscribe((res) => {
      this.workgroupList = res['data'];
      this.workgroupList_fill = JSON.parse(JSON.stringify(res['data']));
    });
    this.createPermissionService.departmentList().subscribe((res) => {
      this.departmentList = res['data'];
      this.departmentList_fill = JSON.parse(JSON.stringify(res['data']));
    });
  }

  seeDetail(element) {
    console.log('id ใบคำขอ', element);
    localStorage.setItem('requestId', element);
    // this.status_loading = true;
    if (element !== 0) {
      this.statusRequest.statusSibar(localStorage.getItem('requestId')).subscribe(res => {
        // console.log('>>>>', res['data'].repeat);
        if (res['data'].repeat === false) {
          this.router.navigate(['/request/information-base/step1']);
        } else if (res['data'].repeat === true) {
          this.router.navigate(['/request-summary/information-base/step1']);
        }
      });
    } else if (element === 0) {
      this.router.navigate(['/request/information-base/step1']);
      localStorage.setItem('requestId', this.requestId);
    }

  }

  onDateChanged(event: IMyDateModel): void {
    // console.log(event.dateRange);

    const start: moment.Moment = moment(event.dateRange.beginDate);
    const end: moment.Moment = moment(event.dateRange.endDate);
    // console.log(start.format('YYYY-MM-DD'));
    // console.log(end.format('YYYY-MM-DD'));

    this.start_date = start.format('YYYY-MM-DD');
    this.end_date = end.format('YYYY-MM-DD');
  }
  clearDate(): void {
    this.mydp.clearDate();
  }
  sendSearch() {
    const dataSend = {
      productDetail: this.searchInput ? this.searchInput : '',
      productStatus: this.selectedProduct ? this.selectedProduct : '',
      productDept: '',
      productSctr: '',
      productGrp: '',
      productObjective: '',
      productStartDate: '',
      productEndDate: '',
    };
    // console.log('>>>>>>>', dataSend);
    this.dashboardsearch.searchDashboard(dataSend).subscribe((res) => {
      // console.log('search', res);
      this.results_search = res;
      // this.length_search = this.results_search.length;
      console.log(this.dataSource);
      this.results_search.forEach((data, index) => {
        // co  nsole.log(data);
        // data.id = index + 1;
        if (data.createAt !== null) {
          data.createAt = moment((data.createAt).slice(0, 10)).format('DD/MM/YYYY');
        } else {
          data.createAt = '-';
        }
        if (data.releaseDate !== null) {
          data.releaseDate = moment((data.releaseDate).slice(0, 10)).format('DD/MM/YYYY');
        } else {
          data.releaseDate = '-';
        }
        if (data.presentationDate !== null) {
          data.presentationDate = moment((data.presentationDate).slice(0, 10)).format(
            'DD/MM/YYYY'
          );
        } else {
          data.presentationDate = '-';
        }
        if (data.lastReviewDate !== null) {
          data.lastReviewDate = moment((data.lastReviewDate).slice(0, 10)).format(
            'DD/MM/YYYY'
          );
        } else {
          data.lastReviewDate = '-';
        }
        if (data.nextReviewDate !== null) {
          data.nextReviewDate = moment((data.nextReviewDate).slice(0, 10)).format(
            'DD/MM/YYYY'
          );
        } else {
          data.nextReviewDate = '-';
        }
      });

      if (this.results_search.length === 0) {
        // console.log('empty.', this.results_search);
        this.Result_data = new MatTableDataSource(this.results_search);
        // this.Result_data.sort = this.sort;
        // this.Result_data.paginator = this.paginatorSearch;
        this.search_data = false;
      }
      // console.log('this.paginatorSearch', this.paginatorSearch);
      // this.dataSource = new MatTableDataSource(this.results.data);
      // this.dataSource.sort = this.sort;
      // this.dataSource.paginator = this.paginator;
      if (this.results_search.length !== 0) {
        console.log('not empty.', this.results_search);
        this.Result_data = new MatTableDataSource(this.results_search);
        this.Result_data.sort = this.sort;
        this.Result_data.paginator = this.paginatorSearch;
        this.search_data = true;
      }
    });
  }
  onSelectStart(event, calendarIndex) {
    // console.log(event);
    this.dateRange[calendarIndex].selectedStartDate = event;
    this.dateRange[calendarIndex].modifiedStartDate = this.dateToBuddha(event);
    console.log(this.dateRange[calendarIndex].modifiedStartDate);

    this.dateRange[calendarIndex].minEndDate = this.dateRange[calendarIndex].selectedStartDate;
  }

  openCalendar(calendarIndex) {
    this.dateRange[calendarIndex].calendarState = true;

    // check if already have value selected.Need to transform to normal format
    if (this.dateRange[calendarIndex].selectedDate) {
      // dd/mm/yyyy -> yyyy-mm-dd
      const splitData = this.dateRange[calendarIndex].selectedDate.replace(/\s/g, '').split('-');
      const convertYear = splitData.map(d => {
        return `${(+d.split('/')[2] - 543)}-${d.split('/')[1]}-${d.split('/')[0]}`;
      });

      // set current date when open calendar
      this.dateRange[calendarIndex].selectedStartDate = new Date(convertYear[0]);
      this.dateRange[calendarIndex].selectedEndDate = new Date(convertYear[1]);
      this.dateRange[calendarIndex].modifiedStartDate = splitData[0];
      this.dateRange[calendarIndex].modifiedEndDate = splitData[1];
    }
  }
  onSelectEnd(event, calendarIndex) {
    // console.log(event);
    this.dateRange[calendarIndex].selectedEndDate = event;
    this.dateRange[calendarIndex].modifiedEndDate = this.dateToBuddha(event);

    this.dateRange[calendarIndex].maxStartDate = this.dateRange[calendarIndex].selectedEndDate;
    console.log(this.dateRange[calendarIndex].modifiedEndDate);
  }

  setupCalendar(calendarIndex, option?) {
    // set min and max date : 'minStartDate' , 'maxStartDate' , 'minEndDate' , 'maxEndDate'
    // set defalut ngModule date : 'selectedDate' , 'selectedStartDate' , 'selectedEndDate'
    // buddha year : 'modifiedStartDate' 'modifiedEndDate'
    // set state calendar show or not : 'calendarState'
    // 'DKKs' Author
    // *** example option ***
    const templateCalendar = {
      minStartDate: new Date(1900, 0, 1),
      maxStartDate: undefined,
      minEndDate: new Date(1900, 0, 1),
      maxEndDate: undefined,
      selectedDate: '',
      selectedStartDate: new Date(),
      selectedEndDate: new Date(),
      modifiedStartDate: this.dateToBuddha(new Date()),
      modifiedEndDate: this.dateToBuddha(new Date()),
      calendarState: false
    };

    this.dateRange[calendarIndex] = option ? option : templateCalendar;

  }
  getLogUserSearchData() {
    if (this.dateRange[0].selectedDate !== '') {
      this.start_date = moment(this.dateRange[0].selectedStartDate).format('YYYY-MM-DD');
      this.end_date = moment(this.dateRange[0].selectedEndDate).format('YYYY-MM-DD');
      this.sendSearchAdvanced();
    } else {
      this.start_date = null;
      this.end_date = null;
      console.log('null');
      this.sendSearchAdvanced();
    }
  }
  closeCalendar(calendarIndex) {
    this.dateRange[calendarIndex].calendarState = false;
  }

  clearCalendar(calendarIndex) {
    console.log('clearCalendar');
    this.start_date = null;
    this.end_date = null;
    this.sendSearchAdvanced();
    this.setupCalendar(calendarIndex);
  }

  applyCalendar(calendarIndex) {
    this.dateRange[calendarIndex].selectedDate
      = `${this.dateRange[calendarIndex].modifiedStartDate} - ${this.dateRange[calendarIndex].modifiedEndDate}`;
    this.dateRange[calendarIndex].calendarState = false;
    console.log('applyCalendar', this.dateRange[calendarIndex].selectedDate);
    this.getLogUserSearchData();
    
  }

  dateToBuddha(dateNormal) {
    console.log('dateNormal', dateNormal);
    const now = moment(dateNormal);
    now.locale('th');
    const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();
    return now.format('DD/MM') + '/' + buddhishYear;
  }

  sendSearchAdvanced() {

    // if (this.model_date != null) {
    //   const start: moment.Moment = moment(
    //     this.model_date.dateRange.beginJsDate
    //   );
    //   const end: moment.Moment = moment(this.model_date.dateRange.endJsDate);

    //   this.start_date = start.format('YYYY-MM-DD');
    //   this.end_date = end.format('YYYY-MM-DD');
    // } else {
    //   this.start_date = '';
    //   this.end_date = '';
    //   console.log('null');
    // }

    // console.log('send this.department', this.department['ouName']);
    // console.log('send this.workgroup', this.workgroup['ouName']);
    console.log('this.start_date', this.start_date);
    console.log(' this.end_date',  this.end_date);

    const dataSend = {
      productDetail: this.searchInput ? this.searchInput : '',
      productStatus: this.selectedProduct ? this.selectedProduct : '',
      productDept: this.department ? this.department['ouName'] : '',
      productSctr: this.workgroup ? this.workgroup['ouName'] : '',
      productGrp: this.division ? this.division['ouName'] : '',
      productObjective: this.status_object ? this.status_object : '',
      productStartDate: (this.start_date !== null && this.start_date !== undefined) ? this.start_date + 'T00:00:00.00' : '',
      productEndDate: (this.end_date !== null && this.end_date !== undefined)  ? this.end_date + 'T23:59:00.00' : '',
    };
    // if (dataSend.productDetail !== ''
    // || dataSend.productStatus !== ''
    // || dataSend.productDept !== ''
    // || dataSend.productSctr !== ''
    // || dataSend.productGrp !== ''
    // || dataSend.productObjective !== ''
    // || dataSend.productStartDate !== ''
    // || dataSend.productStartDate !== ''
    // || dataSend.productEndDate !== '') {
    //   console.log('dataSend have date');
    // } else {
    //   console.log('dataSend all no data', );
    // }
    this.dashboardsearch.searchDashboard(dataSend).subscribe((res) => {
      console.log('search', res);
      this.results_search = res;
      console.log(this.results_search.length);
      this.results_search.forEach((data, index) => {
        // data.id = index + 1;
        if (data.createAt !== null) {
          data.createAt = moment((data.createAt).slice(0, 10)).format('DD/MM/YYYY');
        } else {
          data.createAt = '-';
        }
        if (data.releaseDate !== null) {
          data.releaseDate = moment((data.releaseDate).slice(0, 10)).format('DD/MM/YYYY');
        } else {
          data.releaseDate = '-';
        }
        if (data.presentationDate !== null) {
          data.presentationDate = moment((data.presentationDate).slice(0, 10)).format(
            'DD/MM/YYYY'
          );
        } else {
          data.presentationDate = '-';
        }
        if (data.lastReviewDate !== null) {
          data.lastReviewDate = moment((data.lastReviewDate).slice(0, 10)).format(
            'DD/MM/YYYY'
          );
        } else {
          data.lastReviewDate = '-';
        }
        if (data.nextReviewDate !== null) {
          data.nextReviewDate = moment((data.nextReviewDate).slice(0, 10)).format(
            'DD/MM/YYYY'
          );
        } else {
          data.nextReviewDate = '-';
        }
      });
      // console.log('date >>>>', this.results_search);

      if (this.results_search.length === 0) {
        console.log('empty.', this.results_search);
        this.Result_data = new MatTableDataSource(this.results_search);
        this.Result_data.sort = this.sort;
        this.Result_data.paginator = this.paginatorSearch;
        this.search_data = false;
      }

      if (this.results_search.length !== 0) {
        console.log('not empty.', this.results_search);
        this.Result_data = new MatTableDataSource(this.results_search);
        this.Result_data.sort = this.sort;
        this.Result_data.paginator = this.paginatorSearch;
        this.search_data = true;
      }
    });
  }

  convertDataTo2563(date: any) {
    // console.log('convertDataTo2563', date);
    if (date !== null && date !== '-' && date !== '') {
      const st = date;
      const pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
      const dt = new Date(st.replace(pattern, '$3-$2-$1'));
      // console.log(dt);
      // tslint:disable-next-line: prefer-const
      const year2563 = Number(Number(moment(dt).format('YYYY')) + 543);
      // console.log(year2563);
      const Date3 = moment(dt).format('DD/MM') + '/' + year2563;
      return Date3;
    } else {
      return '-';
    }
  }

  checkOwner(_id, _owner) {
    this.openDialog(_id, _owner);
  }

  openDialog(_id, _owner) {
    const dialogRef = this.dialog.open(OutstandingDialogComponent, {
      width: '1300px',
      height: '600px',
      data: {}
    });

    dialogRef.componentInstance.requestid = _id;
    dialogRef.componentInstance.owner = _owner;
  }

  filterSelection(type, value) {
    // type คือ ฝาย(departmentList) กลุ่ม(workgroupList) สาย(divisionList);
    // value คาที่ต้องนำมาหา
    // ฝ่าย:dept - กลุ่ม:sctr - สาย:grp
    console.log('====== filterSelection =====');
    console.log('filterSelection type', type);
    console.log('filterSelection value', value);
    if (type === 'department' && value !== 'resetFilter') {
      // ================================= department ==================================
      this.fillterStatus = true;
      this.department_ouCode = value.ouCode;
      console.log('001 this.workgroup:', this.workgroup );
      console.log('001 this.division:', this.division );
      if ((this.workgroup === null || this.workgroup === undefined || this.workgroup === '')
      && (this.division === null || this.division === undefined || this.division === '') ) {
         // ( department : select / workgroup : null / division : null )
         this.set_list_workgroup('ouCode', value.sctrOuCode);
         this.set_list_division('ouCode', value.groupOuCode);
      } else if ((this.workgroup !== null && this.workgroup !== undefined && this.workgroup !== '')
      && (this.division === null || this.division === undefined || this.division === '')) {
        // ( department : select / workgroup : not null / division : null )
        if (this.workgroup['ouCode'] !== value.sctrOuCode) {
          if (this.changResetStatus === true) {
            this.workgroup = '';
          } else {
            this.workgroup = undefined;
          }
          this.set_list_workgroup('ouCode', value.sctrOuCode);
          this.set_list_division('ouCode', value.groupOuCode);
        } else {
          this.set_list_division('ouCode_ouCode', this.workgroup['groupOuCode'], value.groupOuCode);
        }
      } else if ((this.workgroup === null || this.workgroup === undefined || this.workgroup === '')
      && (this.division !== null &&  this.division !== undefined &&  this.division !== '')) {
        // ( department : select / workgroup : null / division : not null )
        if (this.division['ouCode'] !== value.groupOuCode) {
          if (this.changResetStatus === true) {
            this.division = '';
          } else {
            this.division = undefined;
          }
          this.set_list_workgroup('ouCode', value.sctrOuCode);
          this.set_list_division('ouCode', value.groupOuCode);
        } else {
          this.set_list_workgroup('ouCode_groupOuCode', value.sctrOuCode, this.division['ouCode']);
        }
      } else if ((this.workgroup !== null && this.workgroup !== undefined && this.workgroup !== '')
      && (this.division !== null &&  this.division !== undefined &&  this.division !== '')) {
        // ( department : select / workgroup : not null / division : not null )
      }
      this.sendSearchAdvanced();
    } else if (type === 'workgroup' && value !== 'resetFilter') {
      // ================================= workgroup ==================================
      this.fillterStatus = true;
      this.workgroup_ouCode = value.ouCode;
      if ((this.department === null || this.department === undefined || this.department === '')
      && (this.division === null || this.division === undefined || this.division === '')) {
        // ( department : null  / workgroup : select / division : null )
        this.set_list_department('sctrOuCode', value.ouCode);
        this.set_list_division('ouCode', value.groupOuCode);
      } else if ((this.department !== null && this.department !== undefined && this.department !== '')
      && (this.division === null || this.division === undefined || this.division === '') ) {
        // ( department : not null  / workgroup : select / division : null )
        if (this.department['sctrOuCode'] !== value.ouCode) {
          if (this.changResetStatus === true) {
            this.department = '';
          } else {
            this.department = undefined;
          }
          this.set_list_department('sctrOuCode', value.ouCode);
          this.set_list_division('ouCode', value.groupOuCode);
        } else {
          this.set_list_division('ouCode_ouCode', this.department['groupOuCode'], value.groupOuCode);
        }
      } else if ((this.department === null || this.department === undefined || this.department === '')
      && (this.division !== null && this.division !== undefined && this.division !== '')) {
        // ( department : null / workgroup : select / division : not null )
        if (this.division['ouCode'] !== value.groupOuCode) {
          if (this.changResetStatus === true) {
            this.division = '';
          } else {
            this.division = undefined;
          }
          this.set_list_department('sctrOuCode', value.ouCode);
          this.set_list_division('ouCode', value.groupOuCode);
        } else {
          this.set_list_department('sctrOuCode_groupOuCode', value.ouCode, this.division['ouCode']);
        }
      } else if ((this.department !== null && this.department !== undefined && this.department !== '')
      && (this.division !== null && this.division !== undefined && this.division !== '')) {
        // ( department : not null / workgroup : select / division : not null )
      }
      this.sendSearchAdvanced();
    }  else if (type === 'division' && value !== 'resetFilter') {
      // ================================= division ==================================
      this.fillterStatus = true;
      this.department_ouCode = value.ouCode;
      if ((this.department === null || this.department === undefined || this.department === '')
      && (this.workgroup === null || this.workgroup === undefined || this.workgroup === '')) {
        // ( department : null / workgroup : null / division : select)
        this.set_list_department('groupOuCode', value.ouCode);
        this.set_list_workgroup('groupOuCode', value.ouCode);
      } else if ((this.department !== null && this.department !== undefined && this.department !== '')
      && (this.workgroup === null || this.workgroup === undefined || this.workgroup === '')) {
        // ( department : not null / workgroup : null / division : select)
        if (this.department['groupOuCode'] !== value.ouCode) {
          if (this.changResetStatus === true) {
            this.department = '';
          } else {
            this.department = undefined;
          }
          this.set_list_department('groupOuCode', value.ouCode);
          this.set_list_workgroup('groupOuCode', value.ouCode);
        } else {
          this.set_list_workgroup('groupOuCode', value.ouCode);
        }
      } else if ((this.department === null || this.department === undefined || this.department === '')
      && (this.workgroup !== null && this.workgroup !== undefined && this.workgroup !== '')) {
         // ( department : null / workgroup : not null / division : select)
         if (this.workgroup['groupOuCode'] !== value.ouCode) {
          if (this.changResetStatus === true) {
            this.workgroup = '';
          } else {
            this.workgroup = undefined;
          }
          this.set_list_workgroup('groupOuCode', value.ouCode);
          this.set_list_department('groupOuCode', value.ouCode);
        } else {
          this.set_list_department('sctrOuCode_groupOuCode', this.workgroup['ouCode'], value.ouCode );
        }
      } else if ((this.department !== null && this.department !== undefined && this.department !== '')
      && (this.workgroup !== null && this.workgroup !== undefined && this.workgroup !== '')) {
        // ( department : not null / workgroup : not null / division : select)
        if (this.department['groupOuCode'] !== value.ouCode) {
          if (this.changResetStatus === true) {
            this.department = '';
          } else {
            this.department = undefined;
          }
          this.set_list_department('groupOuCode', value.ouCode);
        }
        if (this.workgroup['groupOuCode'] !== value.ouCode) {
          if (this.changResetStatus === true) {
            this.workgroup = '';
          } else {
            this.workgroup = undefined;
          }
          this.set_list_workgroup('groupOuCode', value.ouCode);
        }
      }
      this.sendSearchAdvanced();
    } else if ( (type === 'department' || type === 'workgroup' || type === 'division') && value === 'resetFilter') {

      if (this.changResetStatus === true) {
        console.log('00000-1 this.changResetStatus', this.changResetStatus);
        this.department = undefined;
        this.workgroup = undefined;
        this.division = undefined;
        this.fillterStatus = false;
        this.changResetStatus = !this.changResetStatus;
      } else {
        console.log('00000-2 this.changResetStatus', this.changResetStatus);
        this.department = '';
        this.workgroup = '';
        this.division = '';
        this.fillterStatus = false;
        this.changResetStatus = !this.changResetStatus;
      }
      this.sendSearchAdvanced();
      this.departmentList_fill = JSON.parse(JSON.stringify(this.departmentList));
      this.workgroupList_fill = JSON.parse(JSON.stringify(this.workgroupList));
      this.divisionList_fill = JSON.parse(JSON.stringify(this.divisionList));
    }
    // this.departmentList_fill = [];
    // this.workgroupList_fill = [];
    // this.divisionList_fill = [];
    console.log('====== filterSelection end =====');
  }
  set_list_department(search_at, search_ouCode1, search_ouCode2?) {
    if (search_at === 'sctrOuCode') {
      this.departmentList_fill = this.departmentList.filter( (ele) => {
        return ele.sctrOuCode === search_ouCode1;
      });
      if (this.departmentList_fill.length === 0) {
        this.departmentList_fill = 'nondata';
        // this.department = null;
      }
    } else if (search_at === 'groupOuCode') {
      this.departmentList_fill = this.departmentList.filter( (ele) => {
        return ele.groupOuCode === search_ouCode1 ;
      });
      if (this.departmentList_fill.length === 0) {
        this.departmentList_fill = 'nondata';
        // this.department = null;
      }
    } else if (search_at === 'sctrOuCode_groupOuCode') {
      this.departmentList_fill = this.departmentList.filter( (ele) => {
        return ele.sctrOuCode === search_ouCode1 && ele.groupOuCode === search_ouCode2;
      });
      if (this.departmentList_fill.length === 0) {
        this.departmentList_fill = 'nondata';
        // this.department = null;
      }
    }
  }
  set_list_workgroup(search_at, search_ouCode1, search_ouCode2?) {
    if (search_at === 'ouCode') {
      this.workgroupList_fill = this.workgroupList.filter( (ele) => {
        return ele.ouCode === search_ouCode1;
      });
      if (this.workgroupList_fill.length === 0) {
        this.workgroupList_fill = 'nondata';
        // this.workgroup = null;
      }
    } else if (search_at === 'groupOuCode') {
      this.workgroupList_fill = this.workgroupList.filter( (ele) => {
        return ele.groupOuCode === search_ouCode1 ;
      });
      if (this.workgroupList_fill.length === 0) {
        this.workgroupList_fill = 'nondata';
        // this.workgroup = null;
      }
    } else if (search_at === 'ouCode_groupOuCode') {
      this.workgroupList_fill = this.workgroupList.filter( (ele) => {
        return ele.ouCode === search_ouCode1 && ele.groupOuCode === search_ouCode2;
      });
      if (this.workgroupList_fill.length === 0) {
        this.workgroupList_fill = 'nondata';
        // this.workgroup = null;
      }
    }
  }

  set_list_division(search_at, search_ouCode1, search_ouCode2?) {
    if (search_at === 'ouCode') {
      this.divisionList_fill = this.divisionList.filter( (ele) => {
        return ele.ouCode === search_ouCode1;
      });
      if (this.divisionList_fill.length === 0) {
        this.divisionList_fill = 'nondata';
        // this.division = null;
      }
    } else if (search_at === 'ouCode_ouCode') {
      this.divisionList_fill = this.divisionList.filter( (ele) => {
        return ele.ouCode === search_ouCode1 && ele.ouCode === search_ouCode2;
      });
      if (this.divisionList_fill.length === 0) {
        this.divisionList_fill = 'nondata';
        // this.division = null;
      }
    }
  }

  filterSelection_byCode(type, code) {
    console.log('filterSelection_byCode type>>', type);
    console.log('filterSelection_byCode code>>', code);
    if (type === 'department') {

    }
  }

}
