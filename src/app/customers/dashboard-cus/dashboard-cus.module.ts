
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardCusRoutingModule } from './dashboard-cus-routing.module';
import { DashboardCusComponent } from './dashboard-cus.component';
import { from } from 'rxjs';
// ====== chart ========
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatSelectModule } from '@angular/material/select';

import {
  MatMenuModule,
  MatIconModule,
  MatDatepickerModule,
  MatNativeDateModule
} from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { DateRangePickerModule } from '@syncfusion/ej2-angular-calendars';
import { AngularMyDatePickerModule } from 'angular-mydatepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { OutstandingDialogComponent } from './outstanding-dialog/outstanding-dialog.component';


@NgModule({
  declarations: [DashboardCusComponent, OutstandingDialogComponent],
  imports: [
    CommonModule,
    DashboardCusRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatSelectModule,
    MatDatepickerModule,
    ModalModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    DateRangePickerModule,
    AngularMyDatePickerModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatNativeDateModule,
    MatDialogModule
  ],
  providers: [
    DatePipe,
    MatDatepickerModule,
    MatNativeDateModule,

  ],
  entryComponents: [
    OutstandingDialogComponent
  ],
})
export class DashboardCusModule { }
