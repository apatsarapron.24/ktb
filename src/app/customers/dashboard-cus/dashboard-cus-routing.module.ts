import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardCusComponent } from './dashboard-cus.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardCusComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardCusRoutingModule { }
