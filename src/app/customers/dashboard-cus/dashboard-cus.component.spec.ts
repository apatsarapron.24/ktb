import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardCusComponent } from './dashboard-cus.component';

describe('DashboardCusComponent', () => {
  let component: DashboardCusComponent;
  let fixture: ComponentFixture<DashboardCusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardCusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardCusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
