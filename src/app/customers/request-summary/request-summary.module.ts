import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatRadioModule } from '@angular/material/radio';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { AngularMyDatePickerModule } from 'angular-mydatepicker';
import { BrowserModule } from '@angular/platform-browser';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClickOutsideModule } from 'ng-click-outside';
import { MatExpansionModule } from '@angular/material/expansion';



import { RequestSummaryRoutingModule } from './request-summary-routing.module';
import { RequestSummaryComponent } from './request-summary.component';
import { FirstStepSummaryComponent } from './information-base/first-step/first-step.component';
import { StepTwoSummaryComponent } from './information-base/step-two/step-two.component';
import { StepThreeSummaryComponent } from './information-base/step-three/step-three.component';
import { StepFourSummaryComponent } from './information-base/step-four/step-four.component';

import { StepOneSummaryComponent } from './product-detail/step-one/step-one.component';
import { StepTwoProductSummaryComponent } from './product-detail/step-two/step-two.component';
import { StepThreeProductSummaryComponent } from './product-detail/step-three/step-three.component';
import { StepFourProductSummaryComponent } from './product-detail/step-four/step-four.component';
import { StepFiveSummaryComponent } from './product-detail/step-five/step-five.component';
import { StepSixSummaryComponent } from './product-detail/step-six/step-six.component';
import { StepSevenSummaryComponent } from './product-detail/step-seven/step-seven.component';
import { StepEightSummaryComponent } from './product-detail/step-eight/step-eight.component';
import { StepNineSummaryComponent } from './product-detail/step-nine/step-nine.component';
import { StepTenSummaryComponent } from './product-detail/step-ten/step-ten.component';
import { StepElevenSummaryComponent } from './product-detail/step-eleven/step-eleven.component';
import { StepTwelveSummaryComponent } from './product-detail/step-twelve/step-twelve.component';
import { BusinessLoanSummaryComponent } from './product-detail/step-four/business-loan/business-loan.component';
import { PersonalLoanSummaryComponent } from './product-detail/step-four/personal-loan/personal-loan.component';
import { HousingLoanSummaryComponent } from './product-detail/step-four/housing-loan/housing-loan.component';
import { OtherNonCreaditSummaryComponent } from './product-detail/step-four/other-non-creadit/other-non-creadit.component';
// ================================Call Request Component ======================================//
import { RequestModule } from '../request/request.module';
import { OtherSummaryComponent } from './product-detail/step-four/other/other.component';
import { NonCreditSummaryComponent } from './product-detail/step-four/non-credit/non-credit.component';
import { StepFourtSummaryeenComponent } from './product-detail/step-fourteen/step-fourteen.component';
import { StepThirteenSummaryComponent } from './product-detail/step-thirteen/step-thirteen.component';

import { DateModule } from '../datepicker/date/date.module';
import { TextareaAutosizeModule } from 'ngx-textarea-autosize';
import { TextareaAutoresizeDirectiveModule } from '../../textarea-autoresize.directive/textarea-autoresize.directive.module'
import { CanDeactivateGuard } from '../../services/configGuard/config-guard.service';
@NgModule({
  declarations: [
    RequestSummaryComponent,
    FirstStepSummaryComponent,
    StepTwoSummaryComponent,
    StepThreeSummaryComponent,
    StepFourSummaryComponent,
    StepOneSummaryComponent,
    StepTwoProductSummaryComponent,
    StepThreeProductSummaryComponent,
    StepFourProductSummaryComponent,
    StepFiveSummaryComponent,
    StepSixSummaryComponent,
    StepSevenSummaryComponent,
    StepEightSummaryComponent,
    StepEightSummaryComponent,
    StepNineSummaryComponent,
    StepTenSummaryComponent,
    StepElevenSummaryComponent,
    StepTwelveSummaryComponent,
    BusinessLoanSummaryComponent,
    PersonalLoanSummaryComponent,
    HousingLoanSummaryComponent,
    OtherSummaryComponent,
    OtherNonCreaditSummaryComponent,
    NonCreditSummaryComponent,
    StepFourtSummaryeenComponent,
    StepThirteenSummaryComponent
  ],
  imports: [
    CommonModule,
    RequestSummaryRoutingModule,
    RequestModule,
    FormsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatRadioModule,
    MatCheckboxModule,
    AngularMyDatePickerModule,
    MatTableModule,
    MatInputModule,
    DragDropModule,
    ClickOutsideModule,
    MatExpansionModule,
    DateModule,
    TextareaAutosizeModule,
    TextareaAutoresizeDirectiveModule
  ],
  exports: [
    StepTwoProductSummaryComponent,
    StepTenSummaryComponent,
    StepThirteenSummaryComponent,
    StepFourtSummaryeenComponent,
  ],
  providers: [CanDeactivateGuard],

})
export class RequestSummaryModule { }
