import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstStepSummaryComponent } from './first-step.component';

describe('FirstStepSummaryComponent', () => {
  let component: FirstStepSummaryComponent;
  let fixture: ComponentFixture<FirstStepSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstStepSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstStepSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
