import { Component, OnInit, ViewChild } from '@angular/core';
import { CanComponentDeactivate } from './../../../../services/configGuard/config-guard.service';
import { informationBaseFirstStepComponent } from '../../../request/information-base/first-step/first-step.component';
import { Router } from '@angular/router';
import { RequestService } from '../../../../services/request/request.service';
import {
  AngularMyDatePickerDirective,
  IAngularMyDpOptions,
  IMyDateModel,
} from 'angular-mydatepicker';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import * as moment from 'moment';
import { SendWorkService } from './../../../../services/request/product-detail/send-work/send-work.service';
import { InFoFirstStepSummaryService } from '../../../../services/request-summary/information-base/first-step/first-step.service';
import Swal from 'sweetalert2';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogSaveStatusComponent } from '../../../request/dialog/dialog-save-status/dialog-save-status.component';

// tslint:disable-next-line: no-unused-expression

@Component({
  selector: 'app-first-step-summary',
  templateUrl: './first-step.component.html',
  styleUrls: ['./first-step.component.scss']
})

export class FirstStepSummaryComponent implements OnInit, CanComponentDeactivate {

  @ViewChild(informationBaseFirstStepComponent) StepOne: informationBaseFirstStepComponent;

  dateRange = [];
  start_date: any;
  end_date: any;
  model: IMyDateModel = null;

  validate = false;
  userRole = null;

  pageAction = 'save';
  checkDataSuccess = false;
  pageStatus: boolean;

  constructor(
    private router: Router,
    private sidebarService: RequestService,
    private sendWorkService: SendWorkService,
    private inFoFirstStepSummaryService: InFoFirstStepSummaryService,
    public dialog: MatDialog,
  ) { }

  viewNext() {
    this.router.navigate(['request-summary/information-base/step2']);
  }
  ngOnInit() {
    this.checkValidate();
    this.sidebarService.checkDefault(localStorage.getItem('requestId')).subscribe(page => {
      this.pageStatus = page['data']
    })
  }
  saveDraft() {
    const save = this.StepOne.saveDraft();
    console.log('100001 summary saveDraft info step1', save);
    if (save) {
      return 'success';
    }
  }
  sandwork() {
    console.log('save summary base-step-1');
    this.sendWorkService.sendWork(localStorage.getItem('requestId')).subscribe(res_sendwork => {
      // this.sidebarService.sendEvent();
      if (res_sendwork['message'] === 'success') {
        this.router.navigate(['/comment']);
      }
      if (res_sendwork['message'] === 'กรุณากรอกข้อมูลผู้ดำเนินการ') {
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
        this.router.navigate(['/save-manager']);
      } else if (res_sendwork['status'] === 'fail' || res_sendwork['status'] === 'error') {
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
      } else {
        this.router.navigate(['/comment']);
      }
    });
  }
  next_step() {
    this.pageAction = 'next';
    this.StepOne.pageAction = 'next';
    this.router.navigate(['/request-summary/information-base/step2']);
  }

  checkValidate() {
    const id = Number(localStorage.getItem('requestId'));
    this.inFoFirstStepSummaryService.getDefultFirstStep(id).subscribe(res => {
      if (res['status'] === 'success') {
        this.validate = res['data'].validate;
        console.log('validate summary info step1', this.validate);
        this.userRole = localStorage.getItem('role');
        console.log('userRole summary info step1', this.userRole);
      }
    });
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate summary');
    console.log('pageAction', this.pageAction);
    var gotolink = this.sidebarService.goToLink
    console.log('true', gotolink)

    if (this.pageAction == 'next') {
      gotolink = '/request-summary/information-base/step2'
    }
    console.log('LINK:::', gotolink)
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef = this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        console.log('gotolink:::', gotolink);
        const search = gotolink.search('request-summary');
        // const getDataStatus = this.StepOne.checkDataStatus();
        // this.checkDataSuccess = this.StepOne.checkDataSuccess;
        this.checkDataSuccess = this.StepOne.checkDataStatus();
        
        if (data.returnStatus === true) {
          const saveReturn = this.StepOne.upDateDataFirstStep(localStorage.getItem('requestId'), this.pageAction);
          if (saveReturn) {
            if (search == -1) {
              console.log('header-summary')
              subject.next(true);
            } else {
              if (
                gotolink == '/request-summary/information-base/step2' ||
                gotolink == '/request-summary/information-base/step3' ||
                gotolink == '/request-summary/information-base/step4') {
                subject.next(true);
              }
              else {
                if (this.pageStatus && this.checkDataSuccess) {
                  subject.next(true);
                }
                else if (this.pageStatus && this.checkDataSuccess == false) {
                  alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน1');
                  subject.next(false);
                } else {
                  alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน2');
                  subject.next(false);
                }
              }
            }
          } else {
            subject.next(false);
          }

        } else if (data.returnStatus === false) {
          // this.sidebarService.inPageStatus(false);
          // subject.next(true);

          console.log('false', gotolink)
          if (search == -1) {
            console.log('header-summary')
            subject.next(true);
          } else {
            if (
              gotolink == '/request-summary/information-base/step2' ||
              gotolink == '/request-summary/information-base/step3' ||
              gotolink == '/request-summary/information-base/step4') {
              this.ngOnInit();
              subject.next(true);
              return true;
            }
            else {
              console.log('this.pageStatus', this.pageStatus)
              console.log('this.checkDataSuccess', this.checkDataSuccess)
              if (this.pageStatus) {
                subject.next(true);
              } else {
                if (this.checkDataSuccess == false) {
                  alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                  subject.next(false);
                } else {
                  this.ngOnInit()
                  // alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                  subject.next(true);
                }
              }
            }
          }
        } else {
          this.pageAction = 'save';
          subject.next(false);
        }
      });
      return subject;
    } else {
      return true;
    }
  }
}
