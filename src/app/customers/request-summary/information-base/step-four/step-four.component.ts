import { Component, OnInit, ViewChild } from '@angular/core';
import { informationBaseStepFourComponent } from '../../../request/information-base/step-four/step-four.component';
import { Router } from '@angular/router';
import { ProductDefultStepFourService } from '../../../../services/request/information-base/step-four/product-defult-step-four.service';
import { RequestService } from '../../../../services/request/request.service';
import { SendWorkService } from './../../../../services/request/product-detail/send-work/send-work.service';
import Swal from 'sweetalert2';
import { MatDialog } from '@angular/material';
import { DialogSaveStatusComponent } from '../../../request/dialog/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from './../../../../services/configGuard/config-guard.service';


@Component({
  selector: 'app-step-four-summary',
  templateUrl: './step-four.component.html',
  styleUrls: ['./step-four.component.scss']
})
export class StepFourSummaryComponent implements OnInit, CanComponentDeactivate {
  validate = false;
  userRole = null;

  pageAction = 'save'
  pageStatus = false;
  checkDataSuccess = false;
  page4Step: any;

  @ViewChild(informationBaseStepFourComponent)
  StepFour: informationBaseStepFourComponent;

  constructor(private router: Router,
    private productDefultStepFour: ProductDefultStepFourService,
    private sidebarService: RequestService,
    private sendWorkService: SendWorkService,
    public dialog: MatDialog,) { }

  viewNext() {
    this.router.navigate(['request-summary/product-detail/step1']);
  }
  ngOnInit() {
    this.checkValidate();
    this.sidebarService.checkDefault(localStorage.getItem('requestId')).subscribe(page => {
      this.pageStatus = page['data']
    })
    this.sidebarService.statusSibar(localStorage.getItem('requestId')).subscribe(bar => {
      console.log('bar::', bar['data']['pageDefault'])
      this.page4Step = bar['data']['pageDefault']
    })
  }
  checkValidate() {
    const requestId = Number(localStorage.getItem('requestId'));
    this.productDefultStepFour.getDefultFirstStep(requestId).subscribe(res => {
      if (res['status'] === 'success') {
        const data = res['data'];
        this.validate = data.validate;
        console.log('validate summary info step4', this.validate);
        this.userRole = localStorage.getItem('role');
        console.log('userRole summary info step4', this.userRole);
      }
    });
  }
  saveDraftData() {
    this.StepFour.saveDraft();
  }
  sandwork() {
    console.log('save summary base-step-4');
    this.sendWorkService.sendWork(localStorage.getItem('requestId')).subscribe(res_sendwork => {
      // this.sidebarService.sendEvent();
      if (res_sendwork['message'] === 'success') {
        this.router.navigate(['/comment']);
      }
      if (res_sendwork['message'] === 'กรุณากรอกข้อมูลผู้ดำเนินการ') {
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
        this.router.navigate(['/save-manager']);
      } else if (res_sendwork['status'] === 'fail' || res_sendwork['status'] === 'error') {
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
      } else {
        this.router.navigate(['/comment']);
      }
    });
  }
  next_step() {
    let statusExecutiveName = false;
    let statusExecutiveName_phone = false;
    let statusCoordinatorName = false;
    let statusCoordinatorName_phone = false;
    let statusSuccess = false;

    if (this.StepFour.stepFourData.executiveName) {
      statusExecutiveName = true;
    }
    if (this.StepFour.stepFourData.executiveName_phoneNumber) {
      statusExecutiveName_phone = true;
    }
    if (this.StepFour.stepFourData.coordinatorName) {
      statusCoordinatorName = true;
    }
    if (this.StepFour.stepFourData.coordinatorName_phoneNumber) {
      statusCoordinatorName_phone = true;
    }

    console.log(statusExecutiveName, statusExecutiveName_phone, statusCoordinatorName, statusCoordinatorName_phone);

    if (statusExecutiveName &&
      statusExecutiveName_phone &&
      statusCoordinatorName &&
      statusCoordinatorName_phone
    ) {
      statusSuccess = true;
    }
    //  +++++++++ send API ++++++++++++++++
    let documentId = null;
    if (localStorage.getItem('requestId')) {
      documentId = localStorage.getItem('requestId');
    }

    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef = this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        if (data.returnStatus === true) {
          this.StepFour.saveDraftstatus = null;
          const dataInfo = {
            'id': documentId,
            'teamManager': this.StepFour.stepFourData.executiveName,
            'teamManagerPhone': this.StepFour.stepFourData.executiveName_phoneNumber,
            'coordinator': this.StepFour.stepFourData.coordinatorName,
            'coordinatorPhone': this.StepFour.stepFourData.coordinatorName_phoneNumber,
            'status': statusSuccess
          };

          console.log('data summary: ', dataInfo);
          this.productDefultStepFour.upDateDefultFirstStep(dataInfo).subscribe(res => {
            console.log('res send API', res['status']);
            if (res['status'] === 'success') {
              this.StepFour.status_loading = false;
              this.StepFour.saveDraftstatus = 'success';
              localStorage.setItem('requestId', res['data'].id);
              this.sidebarService.inPageStatus(false);
              setTimeout(() => {
                this.StepFour.saveDraftstatus = null;
              }, 3000);
              this.sidebarService.sendEvent();
              this.StepFour.GetDataStepFour(localStorage.getItem('requestId'));

              this.sidebarService.statusSibar(localStorage.getItem('requestId')).subscribe(bar => {
                console.log('bar 2::', bar['data']['pageDefault']);
                this.page4Step = bar['data']['pageDefault'];
                console.log('S page 1::', this.page4Step['page1']);
                console.log('S page 2::', this.page4Step['page2']);
                console.log('S page 3::', this.page4Step['page3']);
                console.log('S page 4::', this.page4Step['page4']);
                if (this.page4Step['page1'] === true
                && this.page4Step['page2'] === true
                && this.page4Step['page3'] === true
                && this.page4Step['page4'] === true) {
                  this.router.navigate(['request-summary/product-detail/step1']);
                } else {
                  alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                }
              });

            } else {
              alert(res['message']);
            }

          }, err => {
            alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
            console.log(err);
          });

        } else if (data.returnStatus === false) {
          this.sidebarService.inPageStatus(false);
          this.StepFour.GetDataStepFour(localStorage.getItem('requestId'));
          this.sidebarService.statusSibar(localStorage.getItem('requestId')).subscribe(bar => {
            console.log('bar 2::', bar['data']['pageDefault']);
            this.page4Step = bar['data']['pageDefault'];
            console.log('S page 1::', this.page4Step['page1']);
            console.log('S page 2::', this.page4Step['page2']);
            console.log('S page 3::', this.page4Step['page3']);
            console.log('S page 4::', this.page4Step['page4']);
            if (this.page4Step['page1'] === true
            && this.page4Step['page2'] === true
            && this.page4Step['page3'] === true
            && this.page4Step['page4'] === true) {
              this.router.navigate(['request-summary/product-detail/step1']);
            } else {
              alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
            }
          });
        } else {
          console.log('test(2S)');
        }
      });
    } else {
      console.log('test(3S)');
      if (localStorage.getItem('requestId')) {
        this.sidebarService.statusSibar(localStorage.getItem('requestId')).subscribe(bar => {
          console.log('bar 2::', bar['data']['pageDefault']);
          this.page4Step = bar['data']['pageDefault'];
          console.log('S page 1::', this.page4Step['page1']);
          console.log('S page 2::', this.page4Step['page2']);
          console.log('S page 3::', this.page4Step['page3']);
          console.log('S page 4::', this.page4Step['page4']);
          if (this.page4Step['page1'] === true
          && this.page4Step['page2'] === true
          && this.page4Step['page3'] === true
          && this.page4Step['page4'] === true) {
            this.router.navigate(['request-summary/product-detail/step1']);
          } else {
            alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
          }
        });
      } else {
        alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
      }

    }



    this.pageAction = 'next';

    // if (statusSuccess == false) {
    //   alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
    // } else {
    //   this.router.navigate(['request-summary/product-detail/step1']);
    // }

  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate request');
    console.log('pageAction', this.pageAction);
    let gotolink = this.sidebarService.goToLink;
    if (this.pageAction === 'next') {
      gotolink = '/request-summary/product-detail/step1';
    }
    console.log('LINK:::', gotolink)
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef = this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        console.log('gotolink:::', gotolink);
        const search = gotolink.search('request-summary');
        this.checkDataSuccess = this.StepFour.checkDataSuccess;
        if (data.returnStatus === true) {
          console.log('true', gotolink);
          const raturnStatus = this.StepFour.saveDraft();
          console.log('request raturnStatus', raturnStatus);
          // if (raturnStatus) {
          //   console.log('raturnStatus T', this.StepFour.saveDraftstatus);
          //   subject.next(true);
          // } else {
          //   console.log('raturnStatus F', this.StepFour.saveDraftstatus);
          //   subject.next(false);
          // }
          this.StepFour.saveDraftstatus = null;
          if (search === -1) {
            console.log('request-summary');
            subject.next(true);
          } else {
            if (
              gotolink === '/request-summary/information-base/step1' ||
              gotolink === '/request-summary/information-base/step2' ||
              gotolink === '/request-summary/information-base/step3') {
              subject.next(true);
            } else {
              if (this.page4Step.page1 && this.page4Step.page2 &&
                this.page4Step.page3 && this.StepFour.checkDataSuccess) {
                subject.next(true);
              } else if (this.StepFour.checkDataSuccess == false) {
                alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                subject.next(false);
              } else {
                alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                subject.next(false);
              }
            }
          }
        } else if (data.returnStatus === false) {
          // this.sidebarService.inPageStatus(false);
          // subject.next(true);
          // return true;
          console.log('false', gotolink);
          if (search === -1) {
            console.log('request-summary');
            subject.next(true);
          } else {
            if (
              gotolink === '/request-summary/information-base/step1' ||
              gotolink === '/request-summary/information-base/step2' ||
              gotolink === '/request-summary/information-base/step3') {
              this.ngOnInit();
              this.sidebarService.resetSaveStatusInfo();
              this.StepFour.saveDraftstatus = null;
              subject.next(true);
              return true;
            } else {
              console.log('this.pageStatus', this.pageStatus);
              console.log('this.checkDataSuccess', this.checkDataSuccess);
              if (this.pageStatus) {
                if (this.checkDataSuccess) {
                  subject.next(true);
                } else {
                  alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                  subject.next(false);
                }
              } else {
                if (this.checkDataSuccess === false) {
                  alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                  subject.next(false);
                } else {
                  this.sidebarService.resetSaveStatusInfo();
                  this.StepFour.saveDraftstatus = null;
                  this.ngOnInit();
                  // alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                  subject.next(true);
                }
              }
            }
          }
        } else {
          this.sidebarService.inPageStatus(false)
          console.log('test(2)')
          subject.next(false);
        }
      });
      return subject;
    } else {
      console.log('test(3)');
      return true;
    }
  }
}

