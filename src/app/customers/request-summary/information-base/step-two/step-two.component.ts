import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { informationBaseStepTwoComponent } from '../../../request/information-base/step-two/step-two.component';
import { Router } from '@angular/router';
import { SendWorkService } from './../../../../services/request/product-detail/send-work/send-work.service';
import Swal from 'sweetalert2';
import { StepTwoService } from '../../../../services/request/information-base/step-two/step-two.service';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from './../../../../services/configGuard/config-guard.service';
import { DialogSaveStatusComponent } from '../../../request/dialog/dialog-save-status/dialog-save-status.component';
import { RequestService } from '../../../../services/request/request.service';

@Component({
  selector: 'app--steptwo-summary',
  templateUrl: './step-two.component.html',
  styleUrls: ['./step-two.component.scss']
})
export class StepTwoSummaryComponent implements OnInit, CanComponentDeactivate {

  validate = false;
  userRole = null;

  pageAction = 'save';
  checkDataSuccess = false;
  pageStatus: boolean;

  @ViewChild(informationBaseStepTwoComponent) StepTwo: informationBaseStepTwoComponent;

  constructor(private router: Router,
    public dialog: MatDialog,
    private sidebarService: RequestService,
    private saveData: StepTwoService,
    private sendWorkService: SendWorkService) { }

  viewNext() {
    this.router.navigate(['request-summary/information-base/step3']);
  }

  ngOnInit() {
    this.checkValidate();
    this.sidebarService.checkDefault(localStorage.getItem('requestId')).subscribe(page => {
      this.pageStatus = page['data']
    })
  }

  checkValidate() {
    const requestId = Number(localStorage.getItem('requestId'));
    const body = { id: requestId };
    this.saveData.getPage2(body).subscribe(res => {
      if (res['status'] === 'success') {
        const data = res['data'];
        this.validate = data.validate;
        console.log('validate summary info step2', this.validate);
        this.userRole = localStorage.getItem('role');
        console.log('userRole summary info step2', this.userRole);
      }
    });
  }

  saveDraftData() {
    this.StepTwo.saveDraft('summary', 'save');
  }
  sandwork() {
    console.log('save summary base-step-2');
    this.sendWorkService.sendWork(localStorage.getItem('requestId')).subscribe(res_sendwork => {
      // this.sidebarService.sendEvent();
      if (res_sendwork['message'] === 'success') {
        this.router.navigate(['/comment']);
      }
      if (res_sendwork['message'] === 'กรุณากรอกข้อมูลผู้ดำเนินการ') {
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
        this.router.navigate(['/save-manager']);
      } else if (res_sendwork['status'] === 'fail' || res_sendwork['status'] === 'error') {
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
      } else {
        this.router.navigate(['/comment']);
      }
    });
  }
  next_step() {
    this.pageAction = 'next';
    //  this.StepTwo.next_step('summary', 'next');
    this.router.navigate(['/request-summary/information-base/step3']);
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate summary');
    console.log('pageAction', this.pageAction);
    var gotolink = this.sidebarService.goToLink
    console.log('true', gotolink)

    if (this.pageAction == 'next') {
      gotolink = '/request-summary/information-base/step3'
    }
    console.log('LINK:::', gotolink)
    console.log('canDeactivate request');
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef = this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        const search = gotolink.search('request-summary');
        this.checkDataSuccess = this.StepTwo.check_require();
        if (data.returnStatus === true) {
          const saveReturn = this.StepTwo.saveDraft();
          if (saveReturn) {
            if (search == -1) {
              console.log('header-summary')
              subject.next(true);
            } else {
              if (
                gotolink == '/request-summary/information-base/step1' ||
                gotolink == '/request-summary/information-base/step3' ||
                gotolink == '/request-summary/information-base/step4') {
                subject.next(true);
              }
              else {
                if (this.pageStatus && this.checkDataSuccess) {
                  subject.next(true);
                }
                else if (this.pageStatus && this.checkDataSuccess == false) {
                  alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                  subject.next(false);
                } else {
                  alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                  subject.next(false);
                }
              }
            }
          } else {
            subject.next(false);
          }
          
        } else if (data.returnStatus === false) {
          // this.sidebarService.inPageStatus(false);
          // subject.next(true);
          // return true;
          console.log('false', gotolink)
          if (search == -1) {
            console.log('header')
            subject.next(true);
          } else {
            if (
              gotolink == '/request-summary/information-base/step1' ||
              gotolink == '/request-summary/information-base/step3' ||
              gotolink == '/request-summary/information-base/step4') {
              this.ngOnInit();
              this.sidebarService.resetSaveStatusInfo();
              this.StepTwo.saveDraftstatus = null;
              subject.next(true);
              return true;
            }
            else {
              console.log('this.pageStatus', this.pageStatus)
              console.log('this.checkDataSuccess', this.checkDataSuccess)
              if (this.pageStatus) {
                subject.next(true);
              } else {
                if (this.checkDataSuccess == false) {
                  alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                  subject.next(false);
                } else {
                  this.sidebarService.resetSaveStatusInfo();
                  this.StepTwo.saveDraftstatus = null;
                  this.ngOnInit();
                  // alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                  subject.next(true);
                }
              }
            }
          }
        } else {
          subject.next(false);
        }
      });
      return subject;
    } else {
      return true;
    }
  }
}
