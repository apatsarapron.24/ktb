import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepTwoSummaryComponent } from './step-two.component';

describe('StepTwoSummaryComponent', () => {
  let component: StepTwoSummaryComponent;
  let fixture: ComponentFixture<StepTwoSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepTwoSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepTwoSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
