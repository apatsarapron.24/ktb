import { Component, OnInit, ViewChild } from '@angular/core';
import { informationBaseStepThreeComponent } from '../../../request/information-base/step-three/step-three.component';
import { Router } from '@angular/router';
import { SendWorkService } from './../../../../services/request/product-detail/send-work/send-work.service';
import Swal from 'sweetalert2';
import { StepThreeService } from '../../../../services/request/information-base/step-three/step-three.service';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RequestService } from '../../../../services/request/request.service';
import { DialogSaveStatusComponent } from '../../../request/dialog/dialog-save-status/dialog-save-status.component';
import { CanComponentDeactivate } from '../../../../services/configGuard/config-guard.service';


@Component({
  selector: 'app-step-three-summary',
  templateUrl: './step-three.component.html',
  styleUrls: ['./step-three.component.scss'],
})
export class StepThreeSummaryComponent implements OnInit, CanComponentDeactivate {

  validate = false;
  userRole = null;
  pageAction = 'save'
  pageStatus = false;
  checkDataSuccess = false;
  @ViewChild(informationBaseStepThreeComponent)
  StepThree: informationBaseStepThreeComponent;

  constructor(private router: Router,
    private saveData: StepThreeService,
    private sendWorkService: SendWorkService,
    public dialog: MatDialog,
    private sidebarService: RequestService) { }
  viewNext() {
    this.router.navigate(['request-summary/information-base/step4']);
  }
  ngOnInit() {
    this.checkValidate();
    this.sidebarService.checkDefault(localStorage.getItem('requestId')).subscribe(page => {
      this.pageStatus = page['data']
    })
  }
  checkValidate() {
    const requestId = Number(localStorage.getItem('requestId'));
    const body = { id: requestId };
    this.saveData.getPage3(body).subscribe(res => {
      if (res['status'] === 'success') {
        const data = res['data'];
        this.validate = data.validate;
        console.log('validate summary info step3', this.validate);
        this.userRole = localStorage.getItem('role');
        console.log('userRole summary info step3', this.userRole);
      }
    });
  }
  saveDraftData() {
    this.StepThree.saveDraft();
  }
  sandwork() {
    console.log('save summary base-step-3');
    this.sendWorkService.sendWork(localStorage.getItem('requestId')).subscribe(res_sendwork => {
      // this.sidebarService.sendEvent();
      if (res_sendwork['message'] === 'success') {
        this.router.navigate(['/comment']);
      }
      if (res_sendwork['message'] === 'กรุณากรอกข้อมูลผู้ดำเนินการ') {
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
        this.router.navigate(['/save-manager']);
      } else if (res_sendwork['status'] === 'fail' || res_sendwork['status'] === 'error') {
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
      } else {
        this.router.navigate(['/comment']);
      }
    });
  }
  next_step() {
    // this.StepThree.next_step('nextSummary');
    this.pageAction = 'next'
    this.router.navigate(['/request-summary/information-base/step4']);
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate request');
    var gotolink = this.sidebarService.goToLink
    if (this.pageAction == 'next') {
      gotolink = '/request-summary/information-base/step4'
    }
    console.log('LINK:::', gotolink)
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef = this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        console.log('gotolink:::', gotolink)
        const search = gotolink.search('request-summary');
        this.checkDataSuccess = this.StepThree.check_require();
        if (data.returnStatus === true) {
          const saveReturn = this.StepThree.saveDraft();
          if (saveReturn) {
            if (search == -1) {
              console.log('header-summary')
              subject.next(true);
            } else {
              if (
                gotolink == '/request-summary/information-base/step2' ||
                gotolink == '/request-summary/information-base/step1' ||
                gotolink == '/request-summary/information-base/step4') {
                subject.next(true);
              }
              else {
                if (this.pageStatus && this.checkDataSuccess) {
                  subject.next(true);
                }
                else if (this.pageStatus && this.checkDataSuccess == false) {
                  alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                  subject.next(false);
                } else {
                  // alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                  subject.next(true);
                }
              }
            }
          } else {
            subject.next(false);
          }

        } else if (data.returnStatus === false) {
          // this.sidebarService.inPageStatus(false);
          // subject.next(true);
          // return true;
          console.log('false', gotolink)
          if (search == -1) {
            console.log('request-summary')
            subject.next(true);
          } else {
            if (
              gotolink == '/request-summary/information-base/step2' ||
              gotolink == '/request-summary/information-base/step1' ||
              gotolink == '/request-summary/information-base/step4') {
              this.ngOnInit()
              this.sidebarService.resetSaveStatusInfo();
              this.StepThree.saveDraftstatus = null;
              subject.next(true);
              return true;
            }
            else {
              console.log('this.pageStatus', this.pageStatus)
              console.log('this.checkDataSuccess', this.checkDataSuccess)
              if (this.pageStatus) {
                subject.next(true);
              } else {
                if (this.checkDataSuccess == false) {
                  alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                  subject.next(false);
                } else {
                  this.ngOnInit()
                  this.sidebarService.resetSaveStatusInfo();
                  this.StepThree.saveDraftstatus = null;
                  // alert('กรุณากรอกข้อมูลในหัวข้อ "ข้อมูลพื้นฐาน" ให้ครบถ้วนก่อน');
                  subject.next(true);
                }
              }
            }
          }
        } else {
          subject.next(false);
        }
      });
      return subject;
    } else {
      return true;
    }
  }
}
