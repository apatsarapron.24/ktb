import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RequestSummaryComponent } from './request-summary.component';
import { FirstStepSummaryComponent } from './information-base/first-step/first-step.component';
import { StepTwoSummaryComponent } from './information-base/step-two/step-two.component';
import { StepThreeSummaryComponent } from './information-base/step-three/step-three.component';
import { StepFourSummaryComponent } from './information-base/step-four/step-four.component';


import { StepOneSummaryComponent } from './product-detail/step-one/step-one.component';
import { StepTwoProductSummaryComponent } from './product-detail/step-two/step-two.component';
import { StepThreeProductSummaryComponent } from './product-detail/step-three/step-three.component';
import { StepFourProductSummaryComponent } from './product-detail/step-four/step-four.component';
import { StepFiveSummaryComponent } from './product-detail/step-five/step-five.component';
import { StepSixSummaryComponent } from './product-detail/step-six/step-six.component';
import { StepSevenSummaryComponent } from './product-detail/step-seven/step-seven.component';
import { StepEightSummaryComponent } from './product-detail/step-eight/step-eight.component';
import { StepNineSummaryComponent } from './product-detail/step-nine/step-nine.component';
import { StepTenSummaryComponent } from './product-detail/step-ten/step-ten.component';
import { StepElevenSummaryComponent } from './product-detail/step-eleven/step-eleven.component';
import { StepTwelveSummaryComponent } from './product-detail/step-twelve/step-twelve.component';
import { BusinessLoanSummaryComponent } from './product-detail/step-four/business-loan/business-loan.component';
import { PersonalLoanSummaryComponent } from './product-detail/step-four/personal-loan/personal-loan.component';
import { HousingLoanSummaryComponent } from './product-detail/step-four/housing-loan/housing-loan.component';
import { NonCreditSummaryComponent } from './product-detail/step-four/non-credit/non-credit.component';
import { OtherSummaryComponent } from './product-detail/step-four/other/other.component';
import { StepFourtSummaryeenComponent } from './product-detail/step-fourteen/step-fourteen.component';
import { StepThirteenSummaryComponent } from './product-detail/step-thirteen/step-thirteen.component';
import { OtherNonCreaditSummaryComponent } from './product-detail/step-four/other-non-creadit/other-non-creadit.component';
import { CanDeactivateGuard } from '../../services/configGuard/config-guard.service';

const routes: Routes = [
  {
    path: '',
    component: RequestSummaryComponent,
  },
  {
    path: 'information-base',
    children: [
      {
        path: 'step1',
        component: FirstStepSummaryComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'step2',
        component: StepTwoSummaryComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'step3',
        component: StepThreeSummaryComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'step4',
        component: StepFourSummaryComponent,
        canDeactivate: [CanDeactivateGuard]
      }
    ],
  },
  {
    path: 'product-detail',
    children: [
      {
        // Business Model & Risk
        path: 'step1',
        component: StepOneSummaryComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
         // ปัญหาอุปสรรคและการจัดการ
        path: 'step2',
        component: StepTwoProductSummaryComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
         // สภาพตลาดและการแข่งขัน
        path: 'step3',
        component: StepThreeProductSummaryComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        // รูปแแบบผลิตภัณฑ์
        path: 'step4',
        component: StepFourProductSummaryComponent,
        children: [
          {
            path: '',
            component: StepFourProductSummaryComponent,
          },
          {
            path: 'business_loan',
            component: BusinessLoanSummaryComponent,
            canDeactivate: [CanDeactivateGuard]
          },
          {
            path: 'persenal_loan',
            component: PersonalLoanSummaryComponent,
            canDeactivate: [CanDeactivateGuard]
          },
          {
            path: 'housing_loan',
            component: HousingLoanSummaryComponent,
            canDeactivate: [CanDeactivateGuard]
          },
          {
            path: 'other',
            component: OtherSummaryComponent,
            canDeactivate: [CanDeactivateGuard]
          },
          {
            path: 'non_credit_product',
            component: NonCreditSummaryComponent,
            canDeactivate: [CanDeactivateGuard]
          },
          {
            path: 'other_non_creadit',
            component: OtherNonCreaditSummaryComponent,
            canDeactivate: [CanDeactivateGuard]
          },
        ]
      },
      {
        // กระบวนการทำงาน
        path: 'step5',
        component: StepFiveSummaryComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        // ความพร้อมด้านการดำเนินการ(Operation)
        path: 'step6',
        component: StepSixSummaryComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
         // ความพร้อมด้านการให้บริการลูกค้าอย่างเป็นธรรม (Market Conduct)
        path: 'step7',
        component: StepSevenSummaryComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
         // ความพร้อมด้านเทคโนโลยีสารสนเทศ
        path: 'step8',
        component: StepEightSummaryComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
         // Cost & Benefit Analysis
        path: 'step9',
        component: StepNineSummaryComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
         // เป้าหมาย/ตัวชี้วัด
        path: 'step10',
        component: StepTenSummaryComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
         // ข้อมูลด้าน Finance
        path: 'step11',
        component: StepElevenSummaryComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
         // ข้อมูลด้าน Compliance
        path: 'step12',
        component: StepTwelveSummaryComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        // ข้อมูลด้าน ประวัติการปรับปรุงผลิตภัณฑ์
       path: 'step13',
       component: StepThirteenSummaryComponent,
       canDeactivate: [CanDeactivateGuard]
     },
      {
        // ข้อมูลด้าน ประวัติการปรับปรุงผลิตภัณฑ์
       path: 'step14',
       component: StepFourtSummaryeenComponent,
     },
    ]
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestSummaryRoutingModule { }
