import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepSevenSummaryComponent } from './step-seven.component';

describe('StepSevenSummaryComponent', () => {
  let component: StepSevenSummaryComponent;
  let fixture: ComponentFixture<StepSevenSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepSevenSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepSevenSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
