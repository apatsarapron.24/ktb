import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {
  AngularMyDatePickerDirective,
  IAngularMyDpOptions,
  IMyDateModel,
} from 'angular-mydatepicker';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { StepSixService } from './../../../../services/request/product-detail/step-six/step-six.service';
import { RequestService } from '../../../../services/request/request.service';
import { StepSixComponent } from '../../../request/product-detail/step-six/step-six.component';
import { saveAs } from 'file-saver';
import { SendWorkService } from './../../../../services/request/product-detail/send-work/send-work.service';
import Swal from 'sweetalert2';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogSaveStatusComponent } from '../../../request/dialog/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from '../../../../services/configGuard/config-guard.service';

export interface FileList {
  fileName: string;
  fileSize: string;
  base64File: string;
}


@Component({
  selector: 'app-step-seven-summary',
  templateUrl: './step-seven.component.html',
  styleUrls: ['./step-seven.component.scss']
})
export class StepSevenSummaryComponent implements OnInit, CanComponentDeactivate {
  saveDraftstatus = null;
  testfile = {};
  @ViewChild(StepSixComponent) RequestStepSix: StepSixComponent;
  @Output() saveStatus = new EventEmitter<boolean>();
  displayedColumns: string[] = [
    'title1',
    'dont_create',
    'new_create',
    'old_doc',
    'explain1',
    'due_date1',
    'delete1'
  ];

  status_loading = true;


  // MatPaginator Inputs

  description = '';

  requestServiceDetailModel = [{
    id: 9,
    title: 'Master sales process',
    serviceType: '',
    reason: '',
    dueDate: ''
  },
  {
    id: 20,
    title: 'Term & Condition',
    serviceType: '',
    reason: '',
    dueDate: ''
  },
  {
    id: 21,
    title: 'Product Catalog',
    serviceType: '',
    reason: '',
    dueDate: ''
  },
  {
    id: 22,
    title: 'Sales Sheet',
    serviceType: '',
    reason: '',
    dueDate: ''
  }];

  step_six = [];

  mock_up = [{
    id: 9,
    title: 'Master sales process',
    dont_create: false,
    new_doc: false,
    old_doc: false,
    reason: '',
    dueDate: ''
  },
  {
    id: 20,
    title: 'Term & Condition',
    dont_create: false,
    new_doc: false,
    old_doc: false,
    reason: '',
    dueDate: ''
  },
  {
    id: 21,
    title: 'Product Catalog',
    dont_create: false,
    new_doc: false,
    old_doc: false,
    reason: '',
    dueDate: ''
  },
  {
    id: 22,
    title: 'Sales Sheet',
    dont_create: false,
    new_doc: false,
    old_doc: false,
    reason: '',
    dueDate: ''
  }
  ];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  dont_create: boolean;
  new_doc: boolean;
  old_doc: boolean;

  attachments: any = {
    file: [],
    fileDelete: []
  };
  isUpdate: boolean;


  status = [false, false, false, false];
  deleteDetail: [];
  displayedColumns_file: string[] = ['name', 'size', 'delete'];
  dataSource_file: MatTableDataSource<FileList>;

  displayedColumns_file_summary: string[] = ['name', 'size', 'download'];
  dataSource_file_summary: MatTableDataSource<FileList>;

  serviceType: any = '';

  model_date: IMyDateModel = null;
  start: any;
  end_date: any;
  results: any;
  api_response: any;
  dateRead: any = [];

  listSummaryStep7: any = [];
  showsearch: any = [];
  showData = false;

  outOfPageSave = false;
  linkTopage = null;
  pageChange = null;
  openDoCheckc = false;

  validate = false;
  userRole = null;
  downloadfile_by = [];

  constructor(private router: Router,
    public stepSixService: StepSixService,
    private sidebarService: RequestService,
    private sendWorkService: SendWorkService,
    public dialog: MatDialog,
  ) { }



  next_step() {
    if (this.isUpdate === true) {
      this.router.navigate(['/request-summary/product-detail/step8']);
    } else if (this.isUpdate === false) {
      this.router.navigate(['/request-summary/product-detail/step9']);
    }
  }

  viewNext() {
    if (this.isUpdate === true) {
      this.router.navigate(['/request-summary/product-detail/step8']);
    } else if (this.isUpdate === false) {
      this.router.navigate(['/request-summary/product-detail/step9']);
    }
  }

  ngOnInit() {
    this.openDoCheckc = true;
    this.step_six = this.mock_up;
    if (localStorage) {
      if (localStorage.getItem('requestId')) {
        this.sidebarService.sendEvent();
        this.stepSixService.getDetail(localStorage.getItem('requestId')).subscribe((ress) => {
          console.log('resssssss;', ress);
          this.isUpdate = ress['isUpdate'];
          if (ress['data'] !== null) {
            this.dataSource_file = new MatTableDataSource(ress['data'][0].fileUplode.file);
            this.hideDownloadFile();
            const data = ress['data'][0];
            this.validate = data.validate;
            console.log('validate summary step2', this.validate);
          } else {
            this.dataSource_file = new MatTableDataSource([]);
            this.validate = ress['validate'];
          }

          this.userRole = localStorage.getItem('role');
          console.log('userRole summary step2', this.userRole);
        });
        this.getDetail(localStorage.getItem('requestId'));
        this.getList();
      } else {
        this.status_loading = false;
      }
    } else {
      console.log('Brownser not support');
    }
  }
  getList() {
    this.stepSixService.getDetail(localStorage.getItem('requestId')).subscribe(res => {
      if (res['parentRequestId'] !== null) {
        this.listSummaryStep7 = res['parentRequestId'];
        for (let index = 0; index < this.listSummaryStep7.length; index++) {
          this.showsearch.push({ index: index, value: true });
        }
      }
    });
  }
  sendSaveStatus() {
    if (this.saveDraftstatus === 'success') {
      this.saveStatus.emit(true);
    } else {
      this.saveStatus.emit(false);
    }
  }

  changeSaveDraft() {
    this.saveDraftstatus = null;
    this.sendSaveStatus();
    this.sidebarService.inPageStatus(true);
  }

  getDetail(id) {
    this.stepSixService.getDetail(id).subscribe(res => {
      console.log('res : ', res);
      this.results = res;
      if (res['status'] === 'success') {
        this.status_loading = false;
      }
      if (res['data'] !== null) {
        this.attachments.fileDelete = [];
        this.attachments.file = [];
        console.log(this.results.data[0]);
        if (this.results.data !== null && this.results.data[0].requestServiceDetailModel !== null) {
          this.step_six = [];
          this.requestServiceDetailModel = [];
          this.description = this.results.data[0].compliance;
          for (let i = 0; i < this.results.data[0].requestServiceDetailModel.length; i++) {
            if (this.results.data[0].requestServiceDetailModel[i].serviceType) {
              if (this.results.data[0].requestServiceDetailModel[i].serviceType === 'ไม่ต้องจัดทำ') {
                this.dont_create = true;
                this.new_doc = false;
                this.old_doc = false;
              }
              if (this.results.data[0].requestServiceDetailModel[i].serviceType === 'จัดทำใหม่') {
                this.dont_create = false;
                this.new_doc = true;
                this.old_doc = false;
              }
              if (this.results.data[0].requestServiceDetailModel[i].serviceType === 'ใช้เอกสารชุดเดิม') {
                this.dont_create = false;
                this.new_doc = false;
                this.old_doc = true;
              }
            }
            if (this.results.data[0].requestServiceDetailModel[i].dueDate !== null) {
              const dateIso = this.results.data[0].requestServiceDetailModel[i].dueDate;
              const dateStamp = { isRange: false, singleDate: { jsDate: new Date(dateIso.toString()) } };
              const template = {
                id: this.results.data[0].requestServiceDetailModel[i].id,
                title: this.results.data[0].requestServiceDetailModel[i].title,
                dont_create: this.dont_create,
                new_doc: this.new_doc,
                old_doc: this.old_doc,
                reason: this.results.data[0].requestServiceDetailModel[i].reason,
                dueDate: dateStamp
              };
              const dateRead = moment(dateStamp.singleDate.jsDate).format('DD/MM') + '/' +
                Number(Number(moment(dateStamp.singleDate.jsDate).format('YYYY')) + 543);
              this.dateRead.push(dateRead);
              this.step_six.push(template);
            } else {
              const template = {
                id: this.results.data[0].requestServiceDetailModel[i].id,
                title: this.results.data[0].requestServiceDetailModel[i].title,
                dont_create: this.dont_create,
                new_doc: this.new_doc,
                old_doc: this.old_doc,
                reason: this.results.data[0].requestServiceDetailModel[i].reason,
                dueDate: ''
              };
              this.dateRead.push('');
              this.step_six.push(template);
            }
            const requestServiceDetail = {
              id: this.results.data[0].requestServiceDetailModel[i].id,
              title: this.results.data[0].requestServiceDetailModel[i].title,
              serviceType: this.results.data[0].requestServiceDetailModel[i].serviceType,
              reason: this.results.data[0].requestServiceDetailModel[i].reason,
              dueDate: this.results.data[0].requestServiceDetailModel[i].dueDate,
            };
            this.requestServiceDetailModel.push(requestServiceDetail);
          }
          for (let i = 0; i < this.results.data[0].fileUplode.file.length; i++) {
            this.attachments.file.push(this.results.data[0].fileUplode.file[i]);
          }
          // this.dataSource_file = new MatTableDataSource(this.RequestStepSix.attachments.file);
          this.dataSource_file_summary = new MatTableDataSource(
            this.attachments.file
          );
        } else {
          console.log('data null');
        }
      }
    });
  }

  checkbox(data, i) {
    if (data === 'ไม่ต้องจัดทำ') {
      console.log('hrhrhrhrhrhrh', this.step_six[i].dont_create);
      if (this.step_six[i].dont_create === true) {
        this.requestServiceDetailModel[i].serviceType = data;
        this.step_six[i].new_doc = false;
        this.step_six[i].old_doc = false;
      } else {
        this.requestServiceDetailModel[i].serviceType = '';
        this.requestServiceDetailModel[i].reason = '';
        this.requestServiceDetailModel[i].dueDate = '';
        this.step_six[i].new_doc = false;
        this.step_six[i].old_doc = false;
        this.step_six[i].reason = '';
        this.step_six[i].dueDate = '';
      }
    }
    if (data === 'จัดทำใหม่') {
      if (this.step_six[i].new_doc === true) {
        this.requestServiceDetailModel[i].serviceType = data;
        this.step_six[i].dont_create = false;
        this.step_six[i].old_doc = false;
      } else {
        this.requestServiceDetailModel[i].serviceType = '';
        this.requestServiceDetailModel[i].reason = '';
        this.requestServiceDetailModel[i].dueDate = '';
        this.step_six[i].dont_create = false;
        this.step_six[i].old_doc = false;
        this.step_six[i].reason = '';
        this.step_six[i].dueDate = '';
      }
    }
    if (data === 'ใช้เอกสารชุดเดิม') {
      if (this.step_six[i].new_doc === true) {
        this.requestServiceDetailModel[i].serviceType = data;
        this.step_six[i].dont_create = false;
        this.step_six[i].new_doc = false;

      } else {
        this.requestServiceDetailModel[i].serviceType = '';
        this.requestServiceDetailModel[i].reason = '';
        this.requestServiceDetailModel[i].dueDate = '';
        this.step_six[i].dont_create = false;
        this.step_six[i].new_doc = false;

        this.step_six[i].reason = '';
        this.step_six[i].dueDate = '';
      }
    }
  }

  checkStatus(i) {
    if (this.requestServiceDetailModel[0].serviceType ||
      this.requestServiceDetailModel[1].serviceType ||
      this.requestServiceDetailModel[2].serviceType ||
      this.requestServiceDetailModel[3].serviceType !== '') {
      if (this.requestServiceDetailModel[i].serviceType === 'ไม่ต้องจัดทำ') {
        if (this.requestServiceDetailModel[i].reason !== '') {
          this.status[i] = true;
        } else {
          this.status[i] = false;
        }
      }
      if (this.requestServiceDetailModel[i].serviceType === 'จัดทำใหม่') {
        if (this.requestServiceDetailModel[i].reason && this.requestServiceDetailModel[i].reason !== '') {
          this.status[i] = true;
        } else {
          this.status[i] = false;
        }
      }
      if (this.requestServiceDetailModel[i].serviceType === 'ใช้เอกสารชุดเดิม') {
        this.status[i] = true;
      }
    } else {
      this.status[i] = false;
    }
  }
  connectExplain(i) {
    this.requestServiceDetailModel[i].reason = this.step_six[i].reason;
  }

  onDateChanged(event: IMyDateModel, i): void {
    const start_date = moment(event.singleDate.jsDate);
    this.requestServiceDetailModel[i].dueDate = start_date.format('YYYY-MM-DD');
    this.changeSaveDraft();
  }


  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      const file = event.target.files;
      for (let i = 0; i < filesAmount; i++) {
        console.log('type:', file[i]);
        if (
          // file[i].type === 'image/svg+xml' ||
          file[i].type === 'image/jpeg' ||
          // file[i].type === 'image/raw' ||
          // file[i].type === 'image/svg' ||
          // file[i].type === 'image/tif' ||
          // file[i].type === 'image/gif' ||
          file[i].type === 'image/jpg' ||
          file[i].type === 'image/png' ||
          file[i].type === 'application/doc' ||
          file[i].type === 'application/pdf' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
          file[i].type === 'application/pptx' ||
          file[i].type === 'application/docx' ||
          (file[i].type === 'application/ppt' && file[i].size)
        ) {
          if (file[i].size <= 5000000) {
            this.multiple_file(file[i], i);
          }

        } else {
          this.status_loading = false;
          alert('File Not Support');
        }
      }
    }
  }

  multiple_file(file, index) {
    console.log('download');
    const reader = new FileReader();
    if (file) {
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');
        // this.urls.push({
        const x = {
          fileName: file.name,
          fileSize: file.size,
          base64File: splitFile[1],
        };
        // });
        this.testfile = x;
        this.attachments.file.push(x);
        this.RequestStepSix.attachments.file.push(x);
        // console.log('this.urls', this.attachments.file);
        this.dataSource_file = new MatTableDataSource(this.attachments.file);
        this.hideDownloadFile();
        console.log(this.attachments.file);
      };
    }
    this.changeSaveDraft();
  }

  delete_mutifile(data: any, index: any) {
    console.log('data:', data, 'index::', index);
    if ('id' in this.attachments.file[index]) {
      const id = this.attachments.file[index].id;
      console.log('found id');
      if ('fileDelete' in this.attachments) {
        this.attachments.fileDelete.push(id);
        this.RequestStepSix.attachments.fileDelete.push(id);
      }
    }
    console.log('fileDelete attachments : ', this.attachments.fileDelete);
    this.attachments.file.splice(index, 1);
    this.RequestStepSix.attachments.fileDelete.splice(index, 1);
    this.dataSource_file = new MatTableDataSource(this.attachments.file);
    console.log(this.attachments);
    this.changeSaveDraft();
  }


  downloadFile(pathdata: any) {
    this.status_loading = true;
    const contentType = '';
    const sendpath = pathdata;
    console.log('path', sendpath);
    this.sidebarService.getfile(sendpath).subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        this.status_loading = false;
        const datagetfile = res['data'];
        const b64Data = datagetfile['data'];
        const filename = datagetfile['fileName'];
        console.log('base64', b64Data);
        const config = this.b64toBlob(b64Data, contentType);
        saveAs(config, filename);
      }

    });
  }

  b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const convertbyte = atob(b64Data);
    const byteCharacters = atob(convertbyte);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  downloadFile64(data: any) {
    this.status_loading = true;
    const contentType = '';
    this.status_loading = false;
    const b64Data = data.base64File;
    const filename = data.fileName;
    console.log('base64', b64Data);

    const config = this.base64(b64Data, contentType);
    saveAs(config, filename);
  }

  base64(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  hideDownloadFile() {
    if (this.dataSource_file.data.length > 0) {
      this.downloadfile_by = [];

      for (let index = 0; index < this.dataSource_file.data.length; index++) {
        const element = this.dataSource_file.data[index];
        console.log('ele', element);

        if ('id' in element) {
          this.downloadfile_by.push('path');
        } else {
          this.downloadfile_by.push('base64');
        }
      }
    }
    console.log('by:', this.downloadfile_by);
  }

  async saveDraftData(page?, action?) {
    // set pages
    let pages = 'summary';
    if (page) {
      pages = page;
    }
    // set actions
    let actions = 'save';
    if (action) {
      actions = action;
    }
    // ======================================
    console.log('saved');
    const result1 = await this.save(pages, actions);
    const result2 = await this.result();
    console.log('result2', result2);
    this.saveDraftstatus = result2;
    if (this.saveDraftstatus === 'success') {
      const after = await this.AfterStart();
      return true;
    } else {
      return false;
    }
  }

  save(page?, action?): Promise<any> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {

        this.RequestStepSix.attachments.file = this.attachments.file;
        this.RequestStepSix.attachments.fileDelete = this.attachments.fileDelete;

        return resolve(this.RequestStepSix.saveDraft(page, action));
      }, 100);
    });
  }
  result() {
    return new Promise((resolve, reject) => {
      setTimeout(() =>
        console.log('resuil:', resolve(this.RequestStepSix.saveDraftstatus)), 500);
    });
  }
  AfterStart() {
    return new Promise((resolve, reject) => {
      setTimeout(
        () => {
          this.dateRead = [];
          this.getDetail(localStorage.getItem('requestId'));
          this.getList();
        },
        500
      );
    });
  }
  sandwork() {
    this.status_loading = true;
    console.log('save summary base-step-2');
    this.sendWorkService.sendWork(localStorage.getItem('requestId')).subscribe(res_sendwork => {
      // this.sidebarService.sendEvent();
      if (res_sendwork['message'] === 'success') {
        this.status_loading = false;
        this.router.navigate(['/comment']);
      }
      if (res_sendwork['message'] === 'กรุณากรอกข้อมูลผู้ดำเนินการ') {
        this.status_loading = false;
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
        this.router.navigate(['/save-manager']);
      } else if (res_sendwork['status'] === 'fail' || res_sendwork['status'] === 'error') {
        this.status_loading = false;
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
      } else {
        this.router.navigate(['/comment']);
      }
    });
  }

  show() {
    if (this.showData === true) {
      this.showData = false;
    } else {
      this.showData = true;
    }
  }
  showSearch(index: any) {
    if (this.showsearch[index].value === true) {
      this.showsearch[index].value = false;
    } else {
      this.showsearch[index].value = true;
    }
  }
  getSummarNew(index: any, requestId: any) {
    console.log(index);
    console.log(requestId);
    this.getDetail(requestId);
    for (let i = 0; i < this.showsearch.length; i++) {
      if (index !== this.showsearch[i].index) {
        this.showsearch[i].value = true;
      }
    }
  }

  // ngDoCheck() {
  //   if (this.openDoCheckc === true) {
  //     this.pageChange = this.sidebarService.changePageGoToLink();
  //     if (this.pageChange) {
  //       const data = this.sidebarService.getsavePage();
  //       let saveInFoStatus = false;
  //       saveInFoStatus = data.saveStatusInfo;
  //       if (saveInFoStatus === true) {
  //         this.outOfPageSave = true;
  //         this.linkTopage = data.goToLink;
  //         this.saveDraftData('navbarSummary', this.linkTopage);
  //         this.sidebarService.resetSaveStatu();
  //       }
  //     }
  //   }
  // }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate request');
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef =  this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        if (data.returnStatus === true) {
          const raturnStatus = this.RequestStepSix.saveDraft();
          console.log('summary raturnStatus', raturnStatus);
            if (raturnStatus) {
              console.log('raturnStatus T', this.RequestStepSix.saveDraftstatus);
              subject.next(true);
            } else {
              console.log('raturnStatus F', this.RequestStepSix.saveDraftstatus);
              subject.next(false);
            }
        } else if (data.returnStatus === false) {
          this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
        }
      });
      return subject;
    } else {
      return true;
    }
  }
}
