import { Component, OnInit, ViewChild } from '@angular/core';
import { StepTwoComponent } from '../../../request/product-detail/step-two/step-two.component';
import { MatTableDataSource } from '@angular/material';
import { StepTwoService } from '../../../../services/request/product-detail/step-two/step-two.service';
import { Router } from '@angular/router';
import { RequestService } from '../../../../services/request/request.service';
import { saveAs } from 'file-saver';
import { SendWorkService } from './../../../../services/request/product-detail/send-work/send-work.service';
import Swal from 'sweetalert2';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogSaveStatusComponent } from '../../../request/dialog/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from './../../../../services/configGuard/config-guard.service';

@Component({
  selector: 'app-step-three-summary',
  templateUrl: './step-three.component.html',
  styleUrls: ['./step-three.component.scss']
})
export class StepThreeProductSummaryComponent implements OnInit, CanComponentDeactivate {

  saveDraftstatus = null;
  displayedColumns_file: string[] = ['name', 'size', 'delete'];
  dataSource_file: MatTableDataSource<FileList>;

  displayedColumns_file_download: string[] = ['name', 'size', 'download'];
  dataSource_file_download: MatTableDataSource<FileList>;

  urls: any = [];

  @ViewChild(StepTwoComponent) RequestStepTwo: StepTwoComponent;
  stepTwo = {
    requestId: null,
    markting: {
      file: [],
      fileDelete: []
    },
    marktingText: null,
    competitor: {
      file: [],
      fileDelete: []
    },
    competitorText: null,
    similar: {
      file: [],
      fileDelete: []
    },
    similarText: null,
    compensate: {
      file: [],
      fileDelete: []
    },
    compensateText: null,
    target: {
      file: [],
      fileDelete: []
    },
    targetText: null,
    attachments: {
      file: [],
      fileDelete: []
    },
    status: false,
  };

  file1_1: any;
  file1_2: any;
  file2_1: any;
  file2_2: any;
  file3_1: any;
  file3_2: any;
  file4_1: any;
  file4_2: any;
  file5_1: any;
  file5_2: any;
  document: any;
  Agreement = null;
  file_name: any;
  file_size: number;

  imgURL: any;
  image1 = [];
  image2 = [];
  image3 = [];
  image4 = [];
  image5 = [];
  showImage1 = false;
  message = null;
  FileOverSize: any;
  // Model
  showModel = false;
  modalmainImageIndex: number;
  modalmainImagedetailIndex: number;
  indexSelect: number;
  modalImage = [];
  modalImagedetail = [];
  fileValue: number;
  testfile: {};

  listSummaryStep3: any = [];
  showsearch: any = [];
  showData = false;
  validate = false;
  userRole = null;
  status_loading = true;
  success: any;
  downloadfile_by = [];

  constructor(
    private productDetailStepTwo: StepTwoService,
    private router: Router,
    private sidebarService: RequestService,
    private sendWorkService: SendWorkService,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    if (localStorage.getItem('requestId')) {
      this.sidebarService.sendEvent();
    }
    if (localStorage) {
      if (localStorage.getItem('requestId')) {
        console.log('localStorage', localStorage.getItem('requestId'));
        this.getDataProductDetailStepTwo(localStorage.getItem('requestId'));
        this.productDetailStepTwo.getProductDetailStepTwo(Number(localStorage.getItem('requestId'))).subscribe(ress => {
          console.log('ress:', ress);
          this.validate = ress['validate'];
          this.userRole = localStorage.getItem('role');
          if (ress['status'] === 'success') {
            this.status_loading = false;
            const data = ress['data'];
            console.log('validate summary step3', this.validate);
            console.log('userRole summary step3', this.userRole);
          }
          if (ress['data'] !== null) {
            this.dataSource_file = new MatTableDataSource(ress['data'].attachments.file);
            this.hideDownloadFile();
          } else if (ress['data'] === null) {
            this.dataSource_file = new MatTableDataSource([]);
          }

        });
        this.getList();
      }
    } else {
      this.status_loading = false;
      console.log('Brownser not support');
    }
  }
  getList() {
    this.productDetailStepTwo.getProductDetailStepTwo(Number(localStorage.getItem('requestId'))).subscribe(res => {
      this.listSummaryStep3 = res['parentRequestId'];
      if (this.listSummaryStep3) {
        for (let index = 0; index < this.listSummaryStep3.length; index++) {
          this.showsearch.push({ index: index, value: true });
        }
      }
    });
  }
  changeSaveDraft() {
    this.saveDraftstatus = null;
    this.sidebarService.inPageStatus(true);
  }
  viewNext() {
    this.router.navigate(['request-summary/product-detail/step4']);
  }
  // ================================================================================

  image_click(colId, id) {
    this.showModel = true;
    console.log('colId:', colId);
    console.log('id:', id);
    // this.showCircle_colId = colId;
    // this.showCircle_id = id;
    document.getElementById('myModalSummaryStep3').style.display = 'block';
    this.imageModal(colId, id);
  }
  image_bdclick() {
    document.getElementById('myModalSummaryStep3').style.display = 'block';
  }

  closeModal() {
    this.showModel = false;
    document.getElementById('myModalSummaryStep3').style.display = 'none';
  }

  // getFileDetails ================================================================
  getFileDetails(fileIndex, event) {

    const files = event.target.files;
    for (let i = 0; i < files.length; i++) {
      const mimeType = files[i].type;
      if (mimeType.match(/image\/*/) == null) {
        this.message = 'Only images are supported.';
        alert(this.message);
        return;
      }
      const file = files[i];
      // this.file_name = file.name;
      // this.file_type = file.type;
      // this.file_size = file.size;
      // this.modifiedDate = file.lastModifiedDate;
      console.log('file:', file);
      const reader = new FileReader();
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');

        const templateFile = {
          fileName: file.name,
          base64File: splitFile[1],
        };
        console.log('templateFile123456', templateFile);

        if (fileIndex === 1) {
          console.log('00000000000000000000000000000001');
          this.stepTwo.markting.file.push(templateFile);

          for (let index = 0; index < this.stepTwo.markting.file.length; index++) {
            this.preview(fileIndex, this.stepTwo.markting.file[index], index);
          }
          this.file1_1 = undefined;
          this.file1_2 = undefined;
        } else if (fileIndex === 2) {
          console.log('00000000000000000000000000000002');
          this.stepTwo.competitor.file.push(templateFile);


          for (let index = 0; index < this.stepTwo.competitor.file.length; index++) {
            this.preview(fileIndex, this.stepTwo.competitor.file[index], index);
            this.file2_1 = undefined;
            this.file2_2 = undefined;
          }
        } else if (fileIndex === 3) {
          console.log('00000000000000000000000000000003');
          this.stepTwo.similar.file.push(templateFile);

          for (let index = 0; index < this.stepTwo.similar.file.length; index++) {
            this.preview(fileIndex, this.stepTwo.similar.file[index], index);
          }
          this.file3_1 = undefined;
          this.file3_2 = undefined;
        } else if (fileIndex === 4) {
          console.log('00000000000000000000000000000004');
          this.stepTwo.compensate.file.push(templateFile);

          for (let index = 0; index < this.stepTwo.compensate.file.length; index++) {
            this.preview(fileIndex, this.stepTwo.compensate.file[index], index);
          }
          this.file4_1 = undefined;
          this.file4_2 = undefined;
        } else if (fileIndex === 5) {
          console.log('00000000000000000000000000000005');
          this.stepTwo.target.file.push(templateFile);

          for (let index = 0; index < this.stepTwo.target.file.length; index++) {
            console.log('ffff', this.stepTwo.target.file[index]);
            this.preview(fileIndex, this.stepTwo.target.file[index], index);
          }
          this.file5_1 = undefined;
          this.file5_2 = undefined;
        }
      };
      reader.readAsDataURL(file);
    }
    this.changeSaveDraft();
  }

  preview(fileIndex, file, index) {
    const name = file.fileName;
    const typeFiles = name.match(/\.\w+$/g);
    const typeFile = typeFiles[0].replace('.', '');

    const tempPicture = `data:image/${typeFile};base64,${file.base64File}`;

    if (fileIndex === 1) {
      if (file) {
        this.image1[index] = tempPicture;
        // console.log('preview image1', this.image1[0]);
      }
    } else if (fileIndex === 2) {
      if (file) {
        this.image2[index] = tempPicture;
      }
    } else if (fileIndex === 3) {
      if (file) {
        this.image3[index] = tempPicture;
      }
    } else if (fileIndex === 4) {
      if (file) {
        this.image4[index] = tempPicture;
      }
    } else if (fileIndex === 5) {
      if (file) {
        this.image5[index] = tempPicture;
      }
    }

  }

  onSelectFile(event) {
    this.FileOverSize = [];
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      const file = event.target.files;
      for (let i = 0; i < filesAmount; i++) {
        console.log('type:', file[i]);
        if (
          // file[i].type === 'image/svg+xml' ||
          file[i].type === 'image/jpeg' ||
          // file[i].type === 'image/raw' ||
          // file[i].type === 'image/svg' ||
          // file[i].type === 'image/tif' ||
          // file[i].type === 'image/gif' ||
          file[i].type === 'image/jpg' ||
          file[i].type === 'image/png' ||
          file[i].type === 'application/doc' ||
          file[i].type === 'application/pdf' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
          file[i].type === 'application/pptx' ||
          file[i].type === 'application/docx' ||
          (file[i].type === 'application/ppt' && file[i].size)
        ) {
          if (file[i].size <= 5000000) {
            this.multiple_file(file[i], i);
          } else {
            this.FileOverSize.push(file[i].name);
          }
        } else {
          this.status_loading = false;
          alert('File Not Support');
        }
      }
      if (this.FileOverSize.length !== 0) {
        console.log('open modal');
        document.getElementById('testttt').click();
      }
    }
    this.changeSaveDraft();
  }

  multiple_file(file, index) {
    // console.log('download');
    const reader = new FileReader();
    if (file) {
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');

        const templateFile = {
          fileName: file.name,
          fileSize: file.size,
          base64File: splitFile[1],
        };
        this.testfile = templateFile;
        this.RequestStepTwo.stepTwo.attachments.file.push(templateFile);
        this.dataSource_file = new MatTableDataSource(this.RequestStepTwo.stepTwo.attachments.file);
        this.hideDownloadFile();
      };
    }
    this.changeSaveDraft();
  }

  delete_mutifile(data: any, index: any) {
    console.log('data:', data, 'index::', index);
    const id = this.RequestStepTwo.stepTwo.attachments.file[index].id;
    if (id) {
      this.RequestStepTwo.stepTwo.attachments.fileDelete.push(id);
    }
    console.log('fileDelete attachments : ', this.RequestStepTwo.stepTwo.attachments.fileDelete);
    this.RequestStepTwo.stepTwo.attachments.file.splice(index, 1);
    this.dataSource_file = new MatTableDataSource(this.RequestStepTwo.stepTwo.attachments.file);
    this.changeSaveDraft();
  }



  // ============================== Modal ==========================================
  imageModal(fileIndex, index) {
    if (fileIndex === 1) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image1;
      this.modalImagedetail = this.stepTwo.markting.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 2) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image2;
      this.modalImagedetail = this.stepTwo.competitor.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 3) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image3;
      this.modalImagedetail = this.stepTwo.similar.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 4) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image4;
      this.modalImagedetail = this.stepTwo.compensate.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 5) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image5;
      this.modalImagedetail = this.stepTwo.target.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    }
  }
  // ===============================================================================
  // ======================================= API ===================================
  getDataProductDetailStepTwo(documentID) {
    this.productDetailStepTwo.getProductDetailStepTwo(documentID).subscribe(res => {
      console.log('res', res);
      if (res['data'] !== null) {
        this.stepTwo = res['data'];

        // set fileDelete to Object stepOne
        this.stepTwo.markting.fileDelete = [];
        this.stepTwo.competitor.fileDelete = [];
        this.stepTwo.similar.fileDelete = [];
        this.stepTwo.compensate.fileDelete = [];
        this.stepTwo.target.fileDelete = [];
        this.stepTwo.attachments.fileDelete = [];


        console.log('set fileDelete', this.stepTwo);

        // set picture preview to all input
        for (let i = 0; i < this.stepTwo.markting.file.length; i++) {
          this.preview(1, this.stepTwo.markting.file[i], i);
        }

        for (let i = 0; i < this.stepTwo.competitor.file.length; i++) {
          this.preview(2, this.stepTwo.competitor.file[i], i);
        }

        for (let i = 0; i < this.stepTwo.similar.file.length; i++) {
          this.preview(3, this.stepTwo.similar.file[i], i);
        }

        for (let i = 0; i < this.stepTwo.compensate.file.length; i++) {
          this.preview(4, this.stepTwo.compensate.file[i], i);
        }

        for (let i = 0; i < this.stepTwo.target.file.length; i++) {
          this.preview(5, this.stepTwo.target.file[i], i);
        }
        this.dataSource_file_download = new MatTableDataSource(this.stepTwo.attachments.file);

      }
    }, err => {
      this.status_loading = false;
      console.log(err);
    });
  }

  next_step_summary() {
    this.router.navigate(['/request-summary/product-detail/step4']);
  }

  async saveDraftData() {
    const data = await this.RequestStepTwo.saveDraft('summary', 'save');
    await this.AfterStart();
    return data;
  }

  functionWaitSuccess(success) {
    if (this.RequestStepTwo.saveDraftstatus !== 'success') {
      console.log('wait');
      success = true;
    } else {
      success = false;
      clearInterval(this.success);
      if (this.RequestStepTwo.saveDraftstatus === 'success') {
        this.router.navigate(['/request-summary/product-detail/step4']);
      }
    }
  }
  AfterStart(): Promise<any> {
    return new Promise((resolve, reject) => {
      setTimeout(() =>

        this.productDetailStepTwo.getProductDetailStepTwo(Number(localStorage.getItem('requestId'))).subscribe(res => {
          console.log('res', res);
          if (res['data'] !== null) {
            this.stepTwo = res['data'];

            // set fileDelete to Object stepOne
            this.stepTwo.markting.fileDelete = [];
            this.stepTwo.competitor.fileDelete = [];
            this.stepTwo.similar.fileDelete = [];
            this.stepTwo.compensate.fileDelete = [];
            this.stepTwo.target.fileDelete = [];
            this.stepTwo.attachments.fileDelete = [];


            console.log('set fileDelete', this.stepTwo);
            this.stepTwo.markting.file = this.RequestStepTwo.stepTwo.markting.file;
            this.stepTwo.competitor.file = this.RequestStepTwo.stepTwo.competitor.file;
            this.stepTwo.similar.file = this.RequestStepTwo.stepTwo.similar.file;
            this.stepTwo.compensate.file = this.RequestStepTwo.stepTwo.compensate.file;
            this.stepTwo.target.file = this.RequestStepTwo.stepTwo.target.file;
            this.image1 = this.RequestStepTwo.image1;
            this.image2 = this.RequestStepTwo.image2;
            this.image3 = this.RequestStepTwo.image3;
            this.image4 = this.RequestStepTwo.image4;
            this.image5 = this.RequestStepTwo.image5;

            // set file attachments
            this.dataSource_file = new MatTableDataSource(this.RequestStepTwo.stepTwo.attachments.file);

            this.dataSource_file_download = new MatTableDataSource(this.stepTwo.attachments.file);
            this.getList();
          }
        }, err => {
          this.status_loading = false;
          console.log(err);
        })

        , 500);
    });
  }

  show() {
    if (this.showData === true) {
      this.showData = false;
    } else {
      this.showData = true;
    }
  }
  showSearch(index: any) {
    if (this.showsearch[index].value === true) {
      this.showsearch[index].value = false;
    } else {
      this.showsearch[index].value = true;
    }
  }
  getSummarNew(index: any, requestId: any) {
    console.log(index);
    console.log(requestId);
    this.image1 = [];
    this.image2 = [];
    this.image3 = [];
    this.image4 = [];
    this.image5 = [];
    this.getDataProductDetailStepTwo(requestId);
    for (let i = 0; i < this.showsearch.length; i++) {
      if (index !== this.showsearch[i].index) {
        this.showsearch[i].value = true;
      }
    }
  }


  hideDownloadFile() {
    if (this.dataSource_file.data.length > 0) {
      this.downloadfile_by = [];

      for (let index = 0; index < this.dataSource_file.data.length; index++) {
        const element = this.dataSource_file.data[index];
        console.log('ele', element);

        if ('id' in element) {
          this.downloadfile_by.push('path');
        } else {
          this.downloadfile_by.push('base64');
        }
      }
    }
    console.log('by:', this.downloadfile_by);

  }
  downloadFile(pathdata: any) {
    this.status_loading = true;
    const contentType = '';
    const sendpath = pathdata;
    console.log('path', sendpath);
    this.sidebarService.getfile(sendpath).subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        this.status_loading = false;
        const datagetfile = res['data'];
        const b64Data = datagetfile['data'];
        const filename = datagetfile['fileName'];
        console.log('base64', b64Data);
        const config = this.b64toBlob(b64Data, contentType);
        saveAs(config, filename);
      }

    });
  }

  b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const convertbyte = atob(b64Data);
    const byteCharacters = atob(convertbyte);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  downloadFile64(data: any) {
    this.status_loading = true;
    const contentType = '';
    this.status_loading = false;
    const b64Data = data.base64File;
    const filename = data.fileName;
    console.log('base64', b64Data);
    console.log('filename', filename);
    const config = this.base64(b64Data, contentType);
    saveAs(config, filename);
  }

  base64(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  sandwork() {
    this.status_loading = true;
    console.log('save summary base-step-2');
    this.sendWorkService.sendWork(localStorage.getItem('requestId')).subscribe(res_sendwork => {
      // this.sidebarService.sendEvent();
      if (res_sendwork['message'] === 'success') {
        this.status_loading = false;
        this.router.navigate(['/comment']);
      }
      if (res_sendwork['message'] === 'กรุณากรอกข้อมูลผู้ดำเนินการ') {
        this.status_loading = false;
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
        this.router.navigate(['/save-manager']);
      } else if (res_sendwork['status'] === 'fail' || res_sendwork['status'] === 'error') {
        this.status_loading = false;
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
      } else {
        this.router.navigate(['/comment']);
      }
    });
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate request');
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef =  this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        if (data.returnStatus === true) {
          const raturnStatus = this.saveDraftData();
          console.log('summary raturnStatus', raturnStatus);
            if (raturnStatus) {
              console.log('raturnStatus T', this.RequestStepTwo.saveDraftstatus);
              subject.next(true);
            } else {
              console.log('raturnStatus F', this.RequestStepTwo.saveDraftstatus);
              subject.next(false);
            }
        } else if (data.returnStatus === false) {
          this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
        }
      });
      return subject;
    } else {
      return true;
    }
  }
}
