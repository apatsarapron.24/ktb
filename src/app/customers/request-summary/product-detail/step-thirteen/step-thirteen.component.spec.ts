import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepThirteenSummaryComponent } from './step-thirteen.component';

describe('StepThirteenSummaryComponent', () => {
  let component: StepThirteenSummaryComponent;
  let fixture: ComponentFixture<StepThirteenSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepThirteenSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepThirteenSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
