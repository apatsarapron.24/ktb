import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
// tslint:disable-next-line:max-line-length
import { StepThirteenSummaryService } from '../../../../services/request-summary/product-detail/step-thirteen/step-thirteen-summary.service';
import { forEach } from '@angular/router/src/utils/collection';
import { summaryFileName } from '@angular/compiler/src/aot/util';
import { Router } from '@angular/router';
import { SendWorkService } from './../../../../services/request/product-detail/send-work/send-work.service';
import Swal from 'sweetalert2';
import { RequestService } from './../../../../services/request/request.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogSaveStatusComponent } from '../../../request/dialog/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from '../../../../services/configGuard/config-guard.service';

@Component({
  selector: 'app-step-thirteen-summary',
  templateUrl: './step-thirteen.component.html',
  styleUrls: ['./step-thirteen.component.scss']
})
export class StepThirteenSummaryComponent implements OnInit, CanComponentDeactivate {
  @Input() template_manage = { pageShow: null };
  @Output() saveStatus = new EventEmitter<boolean>();

  status_loading = true;
  stepThirteenTable = [
    {
      id: null,
      topic: null,
      oldDetail: null,
      newDetail: null,
      reason: null
    }
  ];

  new_for_comment: any;
  new_for_know: any;
  new_for_approval: any;
  saveDraftstatus: any;
  outOfPageSave = false;
  linkTopage = null;
  pageChange = null;
  openDoCheckc = false;
  consideration: any = null;


  // status
  summaryStatus = false;
  tableStatus = false;
  textStatus = false;

  showData = false;
  validate = null;
  userRole = null;
  DataList: any;

  actionNext = 'save';
  show() {
    if (this.showData === true) {
      this.showData = false;
    } else {
      this.showData = true;
    }
  }

  addTable() {
    const newData = {
      id: null,
      topic: null,
      oldDetail: null,
      newDetail: null,
      reason: null
    };
    this.stepThirteenTable.push(newData);
    console.log('addTable step13', this.stepThirteenTable);
  }
  removeItem(index) {
    this.stepThirteenTable.splice(index, 1);
  }
  viewNext() {
    this.router.navigate(['request-summary/product-detail/step14']);
  }
  sandwork() {
    this.status_loading = true;
    console.log('save summary base-step-13');
    this.sendWorkService.sendWork(localStorage.getItem('requestId')).subscribe(res_sendwork => {
      // this.sidebarService.sendEvent();
      if (res_sendwork['message'] === 'success') {
        this.status_loading = false;
        this.router.navigate(['/comment']);
      }
      if (res_sendwork['message'] === 'กรุณากรอกข้อมูลผู้ดำเนินการ') {
        this.status_loading = false;
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
        this.router.navigate(['/save-manager']);
      } else if (res_sendwork['status'] === 'fail' || res_sendwork['status'] === 'error') {
        this.status_loading = false;
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
      } else {
        this.router.navigate(['/comment']);
      }
    });
  }

  changeSaveDraft() {
    this.saveDraftstatus = null;
    this.sendSaveStatus();
    this.sidebarService.inPageStatus(true);
    console.log(this.consideration)
    console.log(this.DataList)
  }
  next_step() {
    this.actionNext = 'next';
    this.router.navigate(['/request-summary/product-detail/step14']);
  }

  constructor(
    public stepThirteenSummaryService: StepThirteenSummaryService,
    private router: Router,
    private sendWorkService: SendWorkService,
    private sidebarService: RequestService,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.openDoCheckc = true;
    if (localStorage.getItem('requestId')) {
      // this.sidebarService.sendEvent();
    }
    if (localStorage) {
      if (localStorage.getItem('requestId')) {
        this.getDetail(localStorage.getItem('requestId'));
        this.userRole = localStorage.getItem('role');
        console.log('userRole summary step13', this.userRole);
      } else {
        // this.status_loading = false;
      }
    } else {
      this.status_loading = false;
      console.log('Brownser not support');
    }
  }

  getDetail(requestId: any) {
    this.stepThirteenSummaryService.getStepThirteen(requestId).subscribe(res => {
      console.log('res step13', res);

      if (res['status'] === 'success') {
        this.status_loading = false;
        // if (res['data'] !== null) {
        const getdetail = res['data'];
        this.DataList = res['data'];
        this.validate = getdetail.validate;
        console.log('validate summary step13', this.validate);
        if (getdetail.improve !== null) {
          // this.stepThirteenTable = getdetail.improve;
          this.consideration = getdetail.summary;
          this.stepThirteenTable = [];
          getdetail.improve.forEach(ele => {
            if ((ele.newDetail === null || ele.newDetail === '')
              && (ele.oldDetail === null || ele.oldDetail === '')
              && (ele.reason === null || ele.reason === '')
              && (ele.topic === null || ele.topic === '')
            ) { } else {
              this.stepThirteenTable.push({
                id: ele.id,
                topic: ele.topic,
                oldDetail: ele.oldDetail,
                newDetail: ele.newDetail,
                reason: ele.reason
              });
            }
          });
        }
        this.new_for_comment = getdetail.fyi;
        this.new_for_know = getdetail.fyr;
        // } else {
        //   this.validate = ['validate'];
        // }
      }


    });
  }


  saveDraftData(page?, action?) {
    console.log('saveDraftData step13');
    const newtable = [];
    const add = false;
    this.stepThirteenTable.forEach(ele => {
      console.log(ele);
      if ((ele.newDetail === null || ele.newDetail === '')
        && (ele.oldDetail === null || ele.oldDetail === '')
        && (ele.reason === null || ele.reason === '')
        && (ele.topic === null || ele.topic === '')
      ) {
        if (ele.id !== null) {
          newtable.push({
            id: ele.id,
            topic: ele.topic,
            oldDetail: ele.oldDetail,
            newDetail: ele.newDetail,
            reason: ele.reason
          });
          console.log(newtable)
        }
      } else {
        newtable.push({
          id: ele.id,
          topic: ele.topic,
          oldDetail: ele.oldDetail,
          newDetail: ele.newDetail,
          reason: ele.reason
        });
        console.log(newtable);
      }
    });

    // checkSummaryStatus
    if (this.consideration === 'continue' || this.consideration === 'improve' || this.consideration === 'cancle') {
      this.summaryStatus = true;
    } else {
      this.summaryStatus = false;
    }

    // checkTableStatus
    this.tableStatus = true;
    newtable.forEach(ele => {
      if ((ele.newDetail === null || ele.newDetail === '')
        || (ele.oldDetail === null || ele.oldDetail === '')
        || (ele.reason === null || ele.reason === '')
        || (ele.topic === null || ele.topic === '')
      ) {
        if (ele.id !== null) {

        } else {
          this.tableStatus = false;
        }
      }
    });

    // textStatus
    if ((this.new_for_comment !== '' && this.new_for_comment !== null)
      && (this.new_for_know !== '' && this.new_for_know !== null)) {
      this.textStatus = true;
    } else {
      this.textStatus = false;
    }
    let dataStatus = false;
    if (this.summaryStatus === true
      && this.tableStatus === true
      && this.textStatus === true) {
      dataStatus = true;
    } else {
      dataStatus = true;
    }

    if (this.consideration == null || this.consideration == '') {
      dataStatus = false
    }
    const updataData = {
      requestId: localStorage.getItem('requestId'),
      summary: this.consideration,
      status: dataStatus,
      fyi: this.new_for_comment,
      fyr: this.new_for_know,
      fya: '',
      improve: this.stepThirteenTable
    };

    const returnUpdateData = new Subject<any>();
    console.log('data::', updataData);
    this.stepThirteenSummaryService.updateStepThirteen(updataData).subscribe(res => {
      if (res['status'] === 'success') {
        this.status_loading = false;
        this.saveDraftstatus = 'success';
        if (this.actionNext === 'next') {
         
          this.router.navigate(['/request-summary/product-detail/step14']);
        }
        else {
          // this.sidebarService.pathname
          // this.sidebarService.goToLink
          this.router.navigate([this.sidebarService.goToLink]);
        }
        this.sidebarService.sendEvent();
        setTimeout(() => {
          this.saveDraftstatus = 'fail';
        }, 3000);
        returnUpdateData.next(true);
        this.sidebarService.inPageStatus(false);
        this.sendSaveStatus();
        this.getDetail(localStorage.getItem('requestId'));
      } else {
        this.status_loading = false;
        alert(res['message']);
        returnUpdateData.next(false);
      }
    });
    if (returnUpdateData) {
      console.log('returnUpdateData T');
      return returnUpdateData;
    } else {
      console.log('returnUpdateData F');
      return false;
    }

  }

  sendSaveStatus() {
    if (this.saveDraftstatus === 'success') {
      this.saveStatus.emit(true);
    } else {
      this.saveStatus.emit(false);
    }
  }

  // ngOnDestroy(): void {
  //   const data = this.sidebarService.getsavePage();
  //   let saveInFoStatus = false;
  //   saveInFoStatus = data.saveStatusInfo;
  //   if (saveInFoStatus === true) {
  //     this.outOfPageSave = true;
  //     this.linkTopage = data.goToLink;
  //     console.log('ngOnDestroy', this.linkTopage);
  //     this.saveDraftData();
  //     this.sidebarService.resetSaveStatusInfo();
  //   }
  // }
  // ngDoCheck() {
  //   if (this.openDoCheckc === true) {
  //     this.pageChange = this.sidebarService.changePageGoToLink();
  //     if (this.pageChange) {
  //       const data = this.sidebarService.getsavePage();
  //       let saveInFoStatus = false;
  //       saveInFoStatus = data.saveStatusInfo;
  //       if (saveInFoStatus === true) {
  //         this.outOfPageSave = true;
  //         this.linkTopage = data.goToLink;
  //         this.saveDraftData('navbarSummary', this.linkTopage);
  //         this.sidebarService.resetSaveStatu();
  //       }
  //     }
  //   }
  // }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate request');
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef = this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        if (data.returnStatus === true) {
          const raturnStatus = this.saveDraftData();
          console.log('summary raturnStatus', raturnStatus);
          if (raturnStatus) {
            // console.log('raturnStatus T', this.saveDraftstatus);
            subject.next(false);
          } else {
            console.log('raturnStatus F', this.saveDraftstatus);
            subject.next(false);
          }
        } else if (data.returnStatus === false) {
          this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
        }
      });
      return subject;
    } else {
      return true;
    }
  }

}
