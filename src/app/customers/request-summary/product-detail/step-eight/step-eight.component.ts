import { StepSevenComponent } from '../../../request/product-detail/step-seven/step-seven.component';
import { RequestService } from './../../../../services/request/request.service';
import { ReplaySubject } from 'rxjs';
import { StepSevenService } from '../../../../services/request/product-detail/step-seven/step-seven.service';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { CdkDragDrop, moveItemInArray, transferArrayItem, CdkDrag, CdkDragExit, CdkDragEnter, CdkDragStart } from '@angular/cdk/drag-drop';
import {
  AngularMyDatePickerDirective,
  IAngularMyDpOptions,
  IMyDateModel
} from 'angular-mydatepicker';
import { saveAs } from 'file-saver';
import { SendWorkService } from './../../../../services/request/product-detail/send-work/send-work.service';
import Swal from 'sweetalert2';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogSaveStatusComponent } from '../../../request/dialog/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from '../../../../services/configGuard/config-guard.service';


@Component({
  selector: 'app-step-eight-summary',
  templateUrl: './step-eight.component.html',
  styleUrls: ['./step-eight.component.scss']
})
export class StepEightSummaryComponent implements OnInit, CanComponentDeactivate {

  saveDraftstatus = null;
  testfile = {};
  @Output() saveStatus = new EventEmitter<boolean>();
  @ViewChild(StepSevenComponent) RequestStepSeven: StepSevenComponent;

  addData = {
    readiness: []
  };

  dropIndex: number;
  staticTitleIndex = [];

  name: any;
  status_loading = true;

  displayedColumns_file: string[] = ['name', 'size', 'delete'];
  dataSource_file: MatTableDataSource<FileList>;

  displayedColumns_file_summary: string[] = ['name', 'size', 'download'];
  dataSource_file_summary: MatTableDataSource<FileList>;

  showCircle_colId = null;
  showCircle_id = null;
  enter = 3;
  stepSeven: any = {
    ux: {
      file: [],
      fileDelete: []
    },
    uxText: null,
    impactAnalysis: {
      file: [],
      fileDelete: []
    },
    impactAnalysisText: null,
    networkDiagram: {
      file: [],
      fileDelete: []
    },
    networkDiagramText: null,
    dataFlowDiagram: {
      file: [],
      fileDelete: []
    },
    dataFlowDiagramText: null,
    attachments:
    {
      file: [],
      fileDelete: []
    }
    ,
    readiness: [],
    readinessDelete: []
  };

  agreement: any;
  file1_1: any;
  file1_2: any;
  file2_1: any;
  file2_2: any;
  file3_1: any;
  file3_2: any;
  file4_1: any;
  file4_2: any;
  document: any;
  Agreement = null;
  file_name: any;
  file_size: number;

  imgURL: any;
  image1 = [];
  image2 = [];
  image3 = [];
  image4 = [];
  showImage1 = false;
  message = null;

  urls: any = [];

  // Model
  showModel = false;
  modalmainImageIndex: number;
  modalmainImagedetailIndex: number;
  indexSelect: number;
  modalImage = [];
  modalImagedetail = [];
  fileValue: number;

  listSummaryStep8: any = [];
  showsearch: any = [];
  showData = false;

  outOfPageSave = false;
  linkTopage = null;
  pageChange = null;
  openDoCheckc = false;

  validate = false;
  userRole = null;
  downloadfile_by = [];

  constructor(
    private router: Router,
    private stepSevenService: StepSevenService,
    private sidebarService: RequestService,
    private sendWorkService: SendWorkService,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.openDoCheckc = true;
    if (localStorage) {
      if (localStorage.getItem('requestId')) {
        this.sidebarService.sendEvent();
        this.stepSevenService.getDetail(localStorage.getItem('requestId')).subscribe((ress) => {
          console.log('resssssss;', ress);
          if (ress['data'] !== null) {
            this.dataSource_file = new MatTableDataSource(ress['data'].attachments.file);
            this.hideDownloadFile();
            const data = ress['data'];
            this.validate = data.validate;
            console.log('validate summary step8', this.validate);
          } else {
            this.dataSource_file = new MatTableDataSource([]);
            this.validate = ress['validate'];
          }
          this.userRole = localStorage.getItem('role');
          console.log('userRole summary step8', this.userRole);
        });
        this.getDetail(localStorage.getItem('requestId'));
        this.getList();
      } else {
        this.status_loading = false;
      }
    } else {
      console.log('Brownser not support');
    }
  }
  getList() {
    this.stepSevenService.getDetail(localStorage.getItem('requestId')).subscribe(res => {
      this.listSummaryStep8 = res['parentRequestId'];
      if (this.listSummaryStep8) {
        for (let index = 0; index < this.listSummaryStep8.length; index++) {
          this.showsearch.push({ index: index, value: true });
        }
      }

    });
  }
  sendSaveStatus() {
    if (this.saveDraftstatus === 'success') {
      this.saveStatus.emit(true);
    } else {
      this.saveStatus.emit(false);
    }
  }
  viewNext() {
    this.router.navigate(['request-summary/product-detail/step9']);
  }
  // ====================== function table ============================
  checkStaticTitle(index) {
    return this.staticTitleIndex.includes(index);
  }

  changeSaveDraft() {
    this.saveDraftstatus = null;
    this.sendSaveStatus();
    this.sidebarService.inPageStatus(true);
  }


  // ====================== function text image ============================

  getDetail(requestId) {
    console.log('getDetail id', requestId);
    this.stepSevenService.getDetail(requestId).subscribe(res => {
      if (res['status'] === 'success') {
        console.log('res data : ', res['data']);
        this.status_loading = false;

        if (res['data'] !== null) {
          this.stepSeven = res['data'];

          // set fileDelete to Object stepSeven
          this.stepSeven.ux.fileDelete = [];
          this.stepSeven.impactAnalysis.fileDelete = [];
          this.stepSeven.networkDiagram.fileDelete = [];
          this.stepSeven.dataFlowDiagram.fileDelete = [];
          this.stepSeven.attachments.fileDelete = [];

          // set picture preview to all input
          for (let i = 0; i < this.stepSeven.ux.file.length; i++) {
            this.preview(1, this.stepSeven.ux.file[i], i);
          }

          for (let i = 0; i < this.stepSeven.impactAnalysis.file.length; i++) {
            this.preview(2, this.stepSeven.impactAnalysis.file[i], i);
          }

          for (let i = 0; i < this.stepSeven.networkDiagram.file.length; i++) {
            this.preview(3, this.stepSeven.networkDiagram.file[i], i);
          }

          for (let i = 0; i < this.stepSeven.dataFlowDiagram.file.length; i++) {
            this.preview(4, this.stepSeven.dataFlowDiagram.file[i], i);
          }

          // set detailDelete Object stepSeven
          console.log('detailDelete', this.stepSeven.readiness);
          this.stepSeven.readiness.forEach((e, index) => {
            this.stepSeven.readiness[index].detailDelete = [];
          });
          // set readinessDeleteto Object stepSeven
          this.stepSeven.readinessDelete = [];

          // set dateFormat
          this.stepSeven.readiness.forEach((elements, readinessIndex) => {
            elements.detail.forEach((e, detailIndex) => {
              // console.log('dueDate', e);
              const dateIso = this.stepSeven.readiness[readinessIndex].detail[detailIndex]['dueDate'];
              console.log('dateIso', dateIso);
              let dateStamp: any;
              let date: any;
              if (dateIso) {
                dateStamp = { isRange: false, singleDate: { jsDate: new Date(dateIso.toString()) } };
                this.stepSeven.readiness[readinessIndex].detail[detailIndex].dueDate = dateStamp;
                date = moment(dateStamp.singleDate.jsDate).format('DD/MM') + '/' +
                  Number(Number(moment(dateStamp.singleDate.jsDate).format('YYYY')) + 543);
                this.stepSeven.readiness[readinessIndex].detail[detailIndex].dueDate = date;
              } else {
                this.stepSeven.readiness[readinessIndex].detail[detailIndex].dueDate = null;
              }
            });
          });


          // set file attachments
          // this.dataSource_file = new MatTableDataSource(this.RequestStepSeven.stepSeven.attachments.file);
          this.dataSource_file_summary = new MatTableDataSource(this.stepSeven.attachments.file);
          // set status to stepData
          this.stepSeven.status = false;
        } else {
          console.log('data null');
        }

      }
    }, err => {
      console.log(err);
    });
  }

  next_step() {
    this.router.navigate(['/request-summary/product-detail/step9']);
  }



  validateStatus() {
    const checkForm =
      this.stepSeven.uxText !== '' &&
      this.stepSeven.impactAnalysisText !== '' &&
      this.stepSeven.networkDiagramText !== '' &&
      this.stepSeven.dataFlowDiagramText !== '';
    return checkForm;
  }

  delete_image(fileIndex, index) {
    console.log('fileIndex:', fileIndex);
    console.log('index:', index);
    if (fileIndex === 1) {
      if ('id' in this.stepSeven.ux.file[index]) {
        const id = this.stepSeven.ux.file[index].id;
        console.log('found id');
        if ('fileDelete' in this.stepSeven.ux) {
          this.stepSeven.ux.fileDelete.push(id);
        }
      }
      console.log('fileDelete ux : ', this.stepSeven.ux.fileDelete);
      this.stepSeven.ux.file.splice(index, 1);
      this.image1.splice(index, 1);

      console.log('this.stepSeven.ux.file.length2:', this.stepSeven.ux.file.length);
      for (let i = 0; i < this.stepSeven.ux.file.length; i++) {
        this.preview(fileIndex, this.stepSeven.ux.file[i], i);
      }
    } else if (fileIndex === 2) {
      if ('id' in this.stepSeven.impactAnalysis.file[index]) {
        const id = this.stepSeven.impactAnalysis.file[index].id;
        console.log('found id');
        if ('fileDelete' in this.stepSeven.impactAnalysis) {
          this.stepSeven.impactAnalysis.fileDelete.push(id);
        }
      }
      console.log('fileDelete impactAnalysis : ', this.stepSeven.impactAnalysis.fileDelete);
      this.stepSeven.impactAnalysis.file.splice(index, 1);
      this.image2.splice(index, 1);

      console.log('this.stepSeven.impactAnalysis.file.length2:', this.stepSeven.impactAnalysis.file.length);
      for (let i = 0; i < this.stepSeven.impactAnalysis.file.length; i++) {
        this.preview(fileIndex, this.stepSeven.impactAnalysis.file[i], i);
      }
    } else if (fileIndex === 3) {
      if ('id' in this.stepSeven.networkDiagram.file[index]) {
        const id = this.stepSeven.networkDiagram.file[index].id;
        console.log('found id');
        if ('fileDelete' in this.stepSeven.networkDiagram) {
          this.stepSeven.networkDiagram.fileDelete.push(id);
        }
      }
      console.log('fileDelete networkDiagram : ', this.stepSeven.networkDiagram.fileDelete);
      this.stepSeven.networkDiagram.file.splice(index, 1);
      this.image3.splice(index, 1);

      console.log('this.file.networkDiagram.uploadFile.length2:', this.stepSeven.networkDiagram.file.length);
      for (let i = 0; i < this.stepSeven.networkDiagram.file.length; i++) {
        this.preview(fileIndex, this.stepSeven.networkDiagram.file[i], i);
      }
    } else if (fileIndex === 4) {
      if ('id' in this.stepSeven.dataFlowDiagram.file[index]) {
        const id = this.stepSeven.dataFlowDiagram.file[index].id;
        console.log('found id');
        if ('fileDelete' in this.stepSeven.dataFlowDiagram) {
          this.stepSeven.dataFlowDiagram.fileDelete.push(id);
        }
      }
      console.log('fileDelete dataFlowDiagram : ', this.stepSeven.dataFlowDiagram.fileDelete);
      this.stepSeven.dataFlowDiagram.file.splice(index, 1);
      this.image4.splice(index, 1);

      console.log('this.file.dataFlowDiagram.uploadFile.length2:', this.stepSeven.dataFlowDiagram.file.length);
      for (let i = 0; i < this.stepSeven.dataFlowDiagram.file.length; i++) {
        this.preview(fileIndex, this.stepSeven.dataFlowDiagram.file[i], i);
      }
    }

  }

  image_click(colId, id) {
    this.showModel = true;
    console.log('colId:', colId);
    console.log('id:', id);
    // this.showCircle_colId = colId;
    // this.showCircle_id = id;
    document.getElementById('myModalEight').style.display = 'block';
    this.imageModal(colId, id);
  }
  image_bdclick() {
    document.getElementById('myModalEight').style.display = 'block';
    console.log('2222');
  }

  closeModal() {
    this.showModel = false;
    document.getElementById('myModalEight').style.display = 'none';
  }



  // getFileDetails ================================================================
  getFileDetails(fileIndex, event) {
    const files = event.target.files;

    for (let i = 0; i < files.length; i++) {
      const mimeType = files[i].type;
      console.log(mimeType);
      if (mimeType.match(/image\/*/) == null) {
        this.message = 'Only images are supported.';
        this.status_loading = false;
        alert(this.message);
        return;
      }
      const file = files[i];
      // this.file_name = file.name;
      // this.file_type = file.type;
      // this.file_size = file.size;
      // this.modifiedDate = file.lastModifiedDate;
      console.log('file:', file);
      const reader = new FileReader();
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');

        const templateFile = {
          fileName: file.name,
          base64File: splitFile[1],
        };
        console.log(templateFile);

        if (fileIndex === 1) {
          console.log('00000000000000000000000000000001');
          this.stepSeven.ux.file.push(templateFile);
          // console.log('this.stepSeven.ux.file : ' , this.stepSeven.ux.file)

          for (let index = 0; index < this.stepSeven.ux.file.length; index++) {
            this.preview(fileIndex, this.stepSeven.ux.file[index], index);
          }
          this.file1_1 = undefined;
          this.file1_2 = undefined;
        } else if (fileIndex === 2) {
          console.log('00000000000000000000000000000002');
          console.log('templateFile : ', templateFile);
          this.stepSeven.impactAnalysis.file.push(templateFile);

          for (let index = 0; index < this.stepSeven.impactAnalysis.file.length; index++) {
            this.preview(fileIndex, this.stepSeven.impactAnalysis.file[index], index);
            this.file2_1 = undefined;
            this.file2_2 = undefined;
          }
        } else if (fileIndex === 3) {
          console.log('00000000000000000000000000000003');
          this.stepSeven.networkDiagram.file.push(templateFile);

          for (let index = 0; index < this.stepSeven.networkDiagram.file.length; index++) {
            this.preview(fileIndex, this.stepSeven.networkDiagram.file[index], index);
          }
          this.file3_1 = undefined;
          this.file3_2 = undefined;
        } else if (fileIndex === 4) {
          console.log('00000000000000000000000000000004');
          this.stepSeven.dataFlowDiagram.file.push(templateFile);

          for (let index = 0; index < this.stepSeven.dataFlowDiagram.file.length; index++) {
            this.preview(fileIndex, this.stepSeven.dataFlowDiagram.file[index], index);
          }
          this.file4_1 = undefined;
          this.file4_2 = undefined;
        }

      };
      reader.readAsDataURL(file);

    }

  }

  preview(fileIndex, file, index) {
    //  console.log('file : ', file);

    const name = file.fileName;
    const typeFiles = name.match(/\.\w+$/g);
    const typeFile = typeFiles[0].replace('.', '');

    const tempPicture = `data:image/${typeFile};base64,${file.base64File}`;

    if (fileIndex === 1) {
      if (file) {
        this.image1[index] = tempPicture;
      }
    } else if (fileIndex === 2) {
      if (file) {
        this.image2[index] = tempPicture;
      }
    } else if (fileIndex === 3) {
      if (file) {
        this.image3[index] = tempPicture;
      }
    } else if (fileIndex === 4) {
      if (file) {
        this.image4[index] = tempPicture;
      }
    }
  }

  // getFileDocument ================================================================
  getFileDocument(event) {
    const file = event.target.files[0];
    const mimeType = file.type;
    if (mimeType.match(/pdf\/*/) == null) {
      this.message = 'Only PDF are supported.';
      this.status_loading = false;
      alert(this.message);
      return;
    }
    console.log('11111', file);
    this.file_name = file.name;
    // this.file_type = file.type;
    this.file_size = file.size / 1024;
    // this.modifiedDate = file.lastModifiedDate;
    // console.log('file:', file);

    this.stepSeven.attachments = file;
    // this.document = undefined;
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      const file = event.target.files;
      for (let i = 0; i < filesAmount; i++) {
        console.log('type:', file[i]);
        if (
          // file[i].type === 'image/svg+xml' ||
          file[i].type === 'image/jpeg' ||
          // file[i].type === 'image/raw' ||
          // file[i].type === 'image/svg' ||
          // file[i].type === 'image/tif' ||
          // file[i].type === 'image/gif' ||
          file[i].type === 'image/jpg' ||
          file[i].type === 'image/png' ||
          file[i].type === 'application/doc' ||
          file[i].type === 'application/pdf' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
          file[i].type === 'application/pptx' ||
          file[i].type === 'application/docx' ||
          (file[i].type === 'application/ppt' && file[i].size)
        ) {
          if (file[i].size <= 5000000) {
            this.multiple_file(file[i], i);
          }
        } else {
          this.status_loading = false;
          alert('File Not Support');
        }
      }
    }
  }

  multiple_file(file, index) {
    // console.log('download');
    const reader = new FileReader();
    if (file) {
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');

        const templateFile = {
          fileName: file.name,
          fileSize: file.size,
          base64File: splitFile[1],
        };
        this.testfile = templateFile;
        this.stepSeven.attachments.file.push(templateFile);
        this.RequestStepSeven.stepSeven.attachments.file.push(templateFile);
        this.dataSource_file = new MatTableDataSource(this.stepSeven.attachments.file);
        this.hideDownloadFile();
      };
    }
    this.changeSaveDraft();
  }

  delete_mutifile(data: any, index: any) {
    console.log('data:', data, 'index::', index);
    if ('id' in this.stepSeven.attachments.file[index]) {
      const id = this.stepSeven.attachments.file[index].id;
      console.log('found id');
      if ('fileDelete' in this.stepSeven.attachments) {
        this.stepSeven.attachments.fileDelete.push(id);
        this.RequestStepSeven.stepSeven.attachments.fileDelete.push(id);
      }
    }
    console.log('fileDelete attachments : ', this.stepSeven.attachments.fileDelete);
    this.stepSeven.attachments.file.splice(index, 1);
    this.RequestStepSeven.stepSeven.attachments.file.splice(index, 1);
    this.dataSource_file = new MatTableDataSource(this.stepSeven.attachments.file);
    this.changeSaveDraft();
  }




  // ============================== Modal ==========================================
  imageModal(fileIndex, index) {
    if (fileIndex === 1) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image1;
      this.modalImagedetail = this.stepSeven.ux.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 2) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image2;
      this.modalImagedetail = this.stepSeven.impactAnalysis.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 3) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image3;
      this.modalImagedetail = this.stepSeven.networkDiagram.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 4) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image4;
      this.modalImagedetail = this.stepSeven.dataFlowDiagram.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    }
  }
  // ===============================================================================
  downloadFile(pathdata: any) {
    this.status_loading = true;
    const contentType = '';
    const sendpath = pathdata;
    console.log('path', sendpath);
    this.sidebarService.getfile(sendpath).subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        this.status_loading = false;
        const datagetfile = res['data'];
        const b64Data = datagetfile['data'];
        const filename = datagetfile['fileName'];
        console.log('base64', b64Data);
        const config = this.b64toBlob(b64Data, contentType);
        saveAs(config, filename);
      }

    });
  }

  b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const convertbyte = atob(b64Data);
    const byteCharacters = atob(convertbyte);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  downloadFile64(data: any) {
    this.status_loading = true;
    const contentType = '';
    this.status_loading = false;
    const b64Data = data.base64File;
    const filename = data.fileName;
    console.log('base64', b64Data);

    const config = this.base64(b64Data, contentType);
    saveAs(config, filename);
  }

  base64(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  hideDownloadFile() {
    if (this.dataSource_file.data.length > 0) {
      this.downloadfile_by = [];

      for (let index = 0; index < this.dataSource_file.data.length; index++) {
        const element = this.dataSource_file.data[index];
        console.log('ele', element);

        if ('id' in element) {
          this.downloadfile_by.push('path');
        } else {
          this.downloadfile_by.push('base64');
        }
      }
    }
    console.log('by:', this.downloadfile_by);
  }

  autoGrowTextZone(e) {
    e.target.style.height = '0px';
    e.target.style.height = (e.target.scrollHeight + 5) + 'px';
  }

  async saveDraftData(page?, action?) {
    // set pages
    let pages = 'summary';
    if (page) {
      pages = page;
    }
    // set actions
    let actions = 'save';
    if (action) {
      actions = action;
    }
    // ======================================
    console.log('saved');
    this.RequestStepSeven.stepSeven.attachments.file = [];
    this.RequestStepSeven.stepSeven.attachments.fileDelete = [];
    const result1 = await this.save(pages, actions);
    const result2 = await this.result();
    console.log('result2', result2);
    this.saveDraftstatus = result2;
    if (this.saveDraftstatus === 'success') {
      const after = await this.AfterStart();
    }
  }

  save(page?, action?): Promise<any> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        this.RequestStepSeven.stepSeven.attachments.file = this.stepSeven.attachments.file;
        this.RequestStepSeven.stepSeven.attachments.fileDelete = this.stepSeven.attachments.fileDelete;

        return resolve(this.RequestStepSeven.saveDraft(page, action));
      }, 100);
    });
  }
  result() {
    return new Promise((resolve, reject) => {
      setTimeout(
        () => console.log('result:', resolve(this.RequestStepSeven.saveDraftstatus)),
        1000
      );
    });
  }
  AfterStart() {
    return new Promise((resolve, reject) => {
      setTimeout(
        () => {
          this.stepSeven.attachments.file = [];
          this.stepSeven.attachments.fileDelete = [];
          this.image1 = this.RequestStepSeven.image1;
          this.image2 = this.RequestStepSeven.image2;
          this.image3 = this.RequestStepSeven.image3;
          this.image4 = this.RequestStepSeven.image4;
          // this.date_summary = [];
          this.getDetail(
            localStorage.getItem('requestId')
          );
          this.saveDraftstatus = 'success';
          setTimeout(() => {
            this.saveDraftstatus = 'fail';
          }, 3000);
          this.getList();
        }
        ,
        500
      );
    });
  }
  sandwork() {
    this.status_loading = true;
    console.log('save summary base-step-2');
    this.sendWorkService.sendWork(localStorage.getItem('requestId')).subscribe(res_sendwork => {
      // this.sidebarService.sendEvent();
      if (res_sendwork['message'] === 'success') {
        this.status_loading = false;
        this.router.navigate(['/comment']);
      }
      if (res_sendwork['message'] === 'กรุณากรอกข้อมูลผู้ดำเนินการ') {
        this.status_loading = false;
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
        this.router.navigate(['/save-manager']);
      } else if (res_sendwork['status'] === 'fail' || res_sendwork['status'] === 'error') {
        this.status_loading = false;
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
      } else {
        this.router.navigate(['/comment']);
      }
    });
  }

  show() {
    if (this.showData === true) {
      this.showData = false;
    } else {
      this.showData = true;
    }
  }
  showSearch(index: any) {
    if (this.showsearch[index].value === true) {
      this.showsearch[index].value = false;
    } else {
      this.showsearch[index].value = true;
    }
  }
  getSummarNew(index: any, requestId: any) {
    console.log(index);
    console.log(requestId);
    this.image1 = [];
    this.image2 = [];
    this.image3 = [];
    this.image4 = [];

    this.getDetail(requestId);
    for (let i = 0; i < this.showsearch.length; i++) {
      if (index !== this.showsearch[i].index) {
        this.showsearch[i].value = true;
      }
    }
  }

  // ngDoCheck() {
  //   if (this.openDoCheckc === true) {
  //     this.pageChange = this.sidebarService.changePageGoToLink();
  //     if (this.pageChange) {
  //       const data = this.sidebarService.getsavePage();
  //       let saveInFoStatus = false;
  //       saveInFoStatus = data.saveStatusInfo;
  //       if (saveInFoStatus === true) {
  //         this.outOfPageSave = true;
  //         this.linkTopage = data.goToLink;
  //         this.saveDraftData('navbarSummary', this.linkTopage);
  //         this.sidebarService.resetSaveStatu();
  //       }
  //     }
  //   }
  // }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate request');
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef =  this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        if (data.returnStatus === true) {
          const raturnStatus = this.RequestStepSeven.saveDraft();
          console.log('summary raturnStatus', raturnStatus);
            if (raturnStatus) {
              console.log('raturnStatus T', this.RequestStepSeven.saveDraftstatus);
              subject.next(true);
            } else {
              console.log('raturnStatus F', this.RequestStepSeven.saveDraftstatus);
              subject.next(false);
            }
        } else if (data.returnStatus === false) {
          this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
        }
      });
      return subject;
    } else {
      return true;
    }
  }
}
