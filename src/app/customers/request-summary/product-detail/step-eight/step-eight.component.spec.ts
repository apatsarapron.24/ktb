import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepEightSummaryComponent } from './step-eight.component';

describe('StepEightSummaryComponent', () => {
  let component: StepEightSummaryComponent;
  let fixture: ComponentFixture<StepEightSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepEightSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepEightSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
