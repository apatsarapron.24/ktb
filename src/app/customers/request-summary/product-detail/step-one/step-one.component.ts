import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { StepOneService } from '../../../../services/request/product-detail/step-one/step-one.service';
import { Router } from '@angular/router';
import { RequestService } from '../../../../services/request/request.service';
import { MatTableDataSource } from '@angular/material';
import { saveAs } from 'file-saver';
import { StepOneComponent } from '../../../request/product-detail/step-one/step-one.component';
import { SendWorkService } from './../../../../services/request/product-detail/send-work/send-work.service';
import Swal from 'sweetalert2';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogSaveStatusComponent } from '../../../request/dialog/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from './../../../../services/configGuard/config-guard.service';

@Component({
  selector: 'app-step-one-summary',
  templateUrl: './step-one.component.html',
  styleUrls: ['./step-one.component.scss'],
})
export class StepOneSummaryComponent implements OnInit, CanComponentDeactivate {
  displayedColumns_file: string[] = ['name', 'size', 'delete'];
  dataSource_file: MatTableDataSource<FileList>;

  displayedColumns_file_summary: string[] = ['name', 'size', 'download'];
  dataSource_file_summary: MatTableDataSource<FileList>;

  saveDraftstatus = null;
  showCircle_colId = null;
  showCircle_id = null;
  enter = 3;

  oldStepOne = [
    {
      objective: {
        file: [],
        fileDelete: [],
      },
      objectiveText: null,
      businessModel: {
        file: [],
        fileDelete: [],
      },
      businessModelText: null,
      detail: {
        file: [],
        fileDelete: [],
      },
      detailText: null,
      externalCommit: {
        no: false,
        yesDetail: {
          KTB: '',
          external: '',
        },
        yes: false,
      },
      attachments: {
        file: [],
        fileDelete: [],
      },
    }
  ];
  stepOne: any = {
    objective: {
      file: [],
      fileDelete: [],
    },
    objectiveText: null,
    businessModel: {
      file: [],
      fileDelete: [],
    },
    businessModelText: null,
    detail: {
      file: [],
      fileDelete: [],
    },
    detailText: null,
    externalCommit: {
      no: false,
      yesDetail: {
        KTB: '',
        external: '',
      },
      yes: false,
    },
    attachments: {
      file: [],
      fileDelete: [],
    },
  };

  agreement: any;
  file1_1: any;
  file1_2: any;
  file2_1: any;
  file2_2: any;
  file3_1: any;
  file3_2: any;
  document: any;
  Agreement = null;
  file_name: any;
  file_size: number;

  imgURL: any;
  image1 = [];
  image2 = [];
  image3 = [];
  showImage1 = false;
  message = null;

  urls: any = [];

  // Model
  showModel = false;
  modalmainImageIndex: number;
  modalmainImagedetailIndex: number;
  indexSelect: number;
  modalImage = [];
  modalImagedetail = [];
  fileValue: number;
  testfile = {};


  validate = false;
  userRole = null;
  parentRequestId = [];
  showsearch: any = [];
  showData = false;
  status_loading = true;
  FileOverSize: any;
  downloadfile_by = [];

  @ViewChild(StepOneComponent) RequestStepOne: StepOneComponent;

  constructor(
    private router: Router,
    private stepOneService: StepOneService,
    private sidebarService: RequestService,
    private sendWorkService: SendWorkService,
    public dialog: MatDialog,
  ) { }

  async ngOnInit() {
    this.sidebarService.sendEvent();
    if (localStorage) {
      if (localStorage.getItem('requestId')) {
        this.stepOneService.getDetail(localStorage.getItem('requestId')).subscribe(
          (ress) => {
            if (ress['data'] !== null) {
              this.dataSource_file = new MatTableDataSource(ress['data']['attachments'].file);
              this.hideDownloadFile();
            } else {
              this.dataSource_file = new MatTableDataSource([]);
            }

          });
        await this.RequestStepOne.ngOnInit();
        await this.getParentRequestId();
        // await this.getDetail(localStorage.getItem('requestId'));
      }
    } else {
      console.log('Brownser not support');
    }
  }

  show() {
    if (this.showData === true) {
      this.showData = false;
    } else {
      this.showData = true;
    }
  }
  showSearch(index: any) {
    if (this.showsearch[index].value === true) {
      this.showsearch[index].value = false;
    } else {
      this.showsearch[index].value = true;
    }
    for (let i = 0; i < this.showsearch.length; i++) {
      if (index !== this.showsearch[i].index) {
        this.showsearch[i].value = true;
      }
    }
  }

  getParentRequestId() {
    this.status_loading = true;
    this.stepOneService.getDetail(localStorage.getItem('requestId')).subscribe(res => {
      if (res['status'] === 'success') {
        this.status_loading = false;
        console.log('getParentRequestId res', res['data']);
        if (res['data'] !== null) {
          const parentRequestId = res['parentRequestId'];
          this.validate = res['validate'];
          console.log('validate summary step1', this.validate);
          this.userRole = localStorage.getItem('role');
          console.log('userRole summary step1', this.userRole);
          if (parentRequestId) {
            this.parentRequestId = parentRequestId;
            for (let index = 0; index < this.parentRequestId.length; index++) {
              this.showsearch.push({ index: index, value: true });
            }
            console.log('getParentRequestId res 2', this.parentRequestId);
            if (this.parentRequestId) {
              for (let index = 0; index < this.parentRequestId.length; index++) {
                this.showsearch.push({ index: index, value: true });
              }
            }
          }
        } else {
          this.stepOne = this.stepOne;
          this.validate = res['validate'];
        }
      }
    });
  }

  getOldDetail(requestId) {
    console.log('getOldDetail requestId', requestId);
    this.stepOneService.getDetail(requestId).subscribe(res => {
      if (res['status'] === 'success') {
        this.status_loading = false;
        console.log('summary old res', res['data']);
        if (res['data']) {
          this.oldStepOne = res['data'];
          console.log('this.oldStepOne', this.oldStepOne);
        }
      }
    });
  }

  getDetail(requestId, index) {
    console.log(requestId)
    console.log(index)
    this.image1 = [];
    this.image2 = [];
    this.image3 = [];
    for (let i = 0; i < this.showsearch.length; i++) {
      if (index !== this.showsearch[i].index) {
        this.showsearch[i].value = true;
      }
    }
    this.stepOneService.getDetail(requestId).subscribe(
      (res) => {
        if (res['status'] === 'success') {
          this.status_loading = false;
          console.log('summary res', res['data']);
          this.stepOne = {
            objective: {
              file: [],
              fileDelete: [],
            },
            objectiveText: null,
            businessModel: {
              file: [],
              fileDelete: [],
            },
            businessModelText: null,
            detail: {
              file: [],
              fileDelete: [],
            },
            detailText: null,
            externalCommit: {
              no: false,
              yesDetail: {
                KTB: '',
                external: '',
              },
              yes: false,
            },
            attachments: {
              file: [],
              fileDelete: [],
            },
          };
          if (res['data']) {
            this.stepOne = res['data'];

            // set fileDelete to Object stepOne
            this.stepOne.objective.fileDelete = [];
            this.stepOne.businessModel.fileDelete = [];
            this.stepOne.detail.fileDelete = [];
            this.stepOne.attachments.fileDelete = [];

            // set picture preview to all input
            for (let i = 0; i < this.stepOne.objective.file.length; i++) {
              this.preview(1, this.stepOne.objective.file[i], i);
            }

            for (let i = 0; i < this.stepOne.businessModel.file.length; i++) {
              this.preview(2, this.stepOne.businessModel.file[i], i);
            }

            for (let i = 0; i < this.stepOne.detail.file.length; i++) {
              this.preview(3, this.stepOne.detail.file[i], i);
            }

            // set first time ratio
            if (this.stepOne.externalCommit.yes === true) {
              this.agreement = '1';
            }
            if (this.stepOne.externalCommit.no === true) {
              this.agreement = '2';
            }

            // set file attachments
            // this.dataSource_file = new MatTableDataSource(
            //   this.stepOne.attachments.file
            // );
            this.dataSource_file_summary = new MatTableDataSource(
              this.stepOne.attachments.file
            );
            this.hideDownloadFile();
            // set status to stepData
            this.stepOne.status = false;
          }
        }
      },
      (err) => {
        console.log(err);
      }
    );
    console.log('this.stepOne 001', this.stepOne);
  }
  ratioChange() {
    if (this.agreement === '1') {
      this.stepOne.externalCommit.yes = true;
      this.stepOne.externalCommit.no = false;
    } else if (this.agreement === '2') {
      this.stepOne.externalCommit.yes = false;
      this.stepOne.externalCommit.no = true;
      this.stepOne.externalCommit.yesDetail.KTB = '';
      this.stepOne.externalCommit.yesDetail.external = '';
    }
  }
  async next_step_summary() {
    this.router.navigate(['/request-summary/product-detail/step2']);
  }
  async saveDraftData() {
    console.log('saved');
    const result1 = await this.save();
    const result2 = await this.result();
    console.log('result2', result2);
    this.saveDraftstatus = result2;
    if (this.saveDraftstatus === 'success') {
      const after = await this.AfterStart();
      return result1;
    } else {
      return false;
    }
  }

  save(page?, action?): Promise<any> {
    return new Promise((resolve, reject) => {
      let pages = 'summary';
      if (action === 'next') {
        pages = 'next';
      }
      setTimeout(() => {

        // this.RequestStepOne.stepOne.objective.file = this.stepOne.objective.file;
        // this.RequestStepOne.stepOne.businessModel.file = this.stepOne.businessModel.file;
        // this.RequestStepOne.stepOne.detail.file = this.stepOne.detail.file;
        // this.RequestStepOne.stepOne.attachments.file = this.stepOne.attachments.file;
        // this.RequestStepOne.stepOne.attachments.fileDelete = this.stepOne.attachments.fileDelete;
        return resolve(this.RequestStepOne.saveDraft('summary', pages));
      }, 100);
    });
  }
  result() {
    return new Promise((resolve, reject) => {
      setTimeout(() =>
        console.log('resuil:', resolve(this.RequestStepOne.saveDraftstatus)), 500);
    });
  }
  AfterStart() {
    return new Promise((resolve, reject) => {
      setTimeout(
        () =>
          this.stepOneService
            .getDetail(localStorage.getItem('requestId'))
            .subscribe(
              (res) => {
                if (res['status'] === 'success') {
                  this.status_loading = false;
                  this.saveDraftstatus = 'success';
                  setTimeout(() => {
                    this.saveDraftstatus = 'fail';
                  }, 3000);
                  console.log('summary res', res['data']);
                  if (res['data']) {
                    this.stepOne = res['data'];
                    // set status to stepData
                    this.stepOne.objective.fileDelete = [];
                    this.stepOne.businessModel.fileDelete = [];
                    this.stepOne.detail.fileDelete = [];
                    this.stepOne.attachments.fileDelete = [];
                    // this.stepOne.attachments.file = [];
                    // this.stepOne.objective.file = this.RequestStepOne.stepOne.objective.file;
                    // this.stepOne.businessModel.file = this.RequestStepOne.stepOne.businessModel.file;
                    // this.stepOne.detail.file = this.RequestStepOne.stepOne.detail.file;
                    // this.stepOne.attachments.file = this.RequestStepOne.stepOne.attachments.file;
                    // this.image1 = this.RequestStepOne.image1;
                    // this.image2 = this.RequestStepOne.image2;
                    // this.image3 = this.RequestStepOne.image3;
                    if (this.stepOne.externalCommit.yes === true) {
                      this.agreement = '1';
                    }
                    if (this.stepOne.externalCommit.no === true) {
                      this.agreement = '2';
                    }
                    this.stepOne.status = false;
                    this.dataSource_file = new MatTableDataSource(
                      this.RequestStepOne.stepOne.attachments.file
                    );
                    this.hideDownloadFile();
                    this.dataSource_file_summary = new MatTableDataSource(
                      this.stepOne.attachments.file
                    );
                  }
                }
              },
              (err) => {
                this.status_loading = false;
                console.log(err);
              }
            ),
        500
      );
    });
  }

  image_click(colId, id) {
    this.showModel = true;
    document.getElementById('myModalSummary').style.display = 'block';
    this.imageModal(colId, id);
  }
  image_bdclick() {
    document.getElementById('myModalSummary').style.display = 'block';
  }

  closeModal() {
    this.showModel = false;
    document.getElementById('myModalSummary').style.display = 'none';
  }

  // getFileDetails ================================================================
  getFileDetails(fileIndex, event) {
    const files = event.target.files;

    for (let i = 0; i < files.length; i++) {
      const mimeType = files[i].type;
      console.log(mimeType);
      if (mimeType.match(/image\/*/) == null) {
        this.message = 'Only images are supported.';
        alert(this.message);
        return;
      }
      const file = files[i];
      console.log('file:', file);
      const reader = new FileReader();
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');

        const templateFile = {
          fileName: file.name,
          base64File: splitFile[1],
        };
        this.testfile = templateFile;
        if (fileIndex === 1) {
          console.log('00000000000000000000000000000001');
          this.stepOne.objective.file.push(templateFile);

          for (
            let index = 0;
            index < this.stepOne.objective.file.length;
            index++
          ) {
            this.preview(fileIndex, this.stepOne.objective.file[index], index);
          }
          this.file1_1 = undefined;
          this.file1_2 = undefined;
        } else if (fileIndex === 2) {
          console.log('00000000000000000000000000000002');
          this.stepOne.businessModel.file.push(templateFile);

          for (
            let index = 0;
            index < this.stepOne.businessModel.file.length;
            index++
          ) {
            this.preview(
              fileIndex,
              this.stepOne.businessModel.file[index],
              index
            );
            this.file2_1 = undefined;
            this.file2_2 = undefined;
          }
        } else if (fileIndex === 3) {
          console.log('00000000000000000000000000000003');
          this.stepOne.detail.file.push(templateFile);

          for (
            let index = 0;
            index < this.stepOne.detail.file.length;
            index++
          ) {
            this.preview(fileIndex, this.stepOne.detail.file[index], index);
          }
          this.file3_1 = undefined;
          this.file3_2 = undefined;
        }
      };
      reader.readAsDataURL(file);
    }
  }

  preview(fileIndex, file, index) {
    const name = file.fileName;
    const typeFiles = name.match(/\.\w+$/g);
    const typeFile = typeFiles[0].replace('.', '');

    const tempPicture = `data:image/${typeFile};base64,${file.base64File}`;

    if (fileIndex === 1) {
      if (file) {
        this.image1[index] = tempPicture;
      }
    } else if (fileIndex === 2) {
      if (file) {
        this.image2[index] = tempPicture;
      }
    } else if (fileIndex === 3) {
      if (file) {
        this.image3[index] = tempPicture;
      }
    }
  }

  // getFileDocument ================================================================

  onSelectFile(event) {
    this.FileOverSize = [];
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      const file = event.target.files;
      for (let i = 0; i < filesAmount; i++) {
        if (
          // file[i].type === 'image/svg+xml' ||
          file[i].type === 'image/jpeg' ||
          // file[i].type === 'image/raw' ||
          // file[i].type === 'image/svg' ||
          // file[i].type === 'image/tif' ||
          // file[i].type === 'image/gif' ||
          file[i].type === 'image/jpg' ||
          file[i].type === 'image/png' ||
          file[i].type === 'application/doc' ||
          file[i].type === 'application/pdf' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
          file[i].type === 'application/pptx' ||
          file[i].type === 'application/docx' ||
          (file[i].type === 'application/ppt' && file[i].size)
        ) {
          if (file[i].size <= 5000000) {
            this.multiple_file(file[i], i);
          } else {
            this.FileOverSize.push(file[i].name);
          }
        } else {
          this.status_loading = false;
          alert('File Not Support');
        }
      }
      if (this.FileOverSize.length !== 0) {
        console.log('open modal');
        document.getElementById('testttt').click();
      }
    }
  }
  viewNext() {
    this.router.navigate(['request-summary/product-detail/step2']);
  }

  multiple_file(file, index) {
    console.log(file);
    const reader = new FileReader();
    if (file) {
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');

        const templateFile = {
          fileName: file.name,
          fileSize: file.size,
          base64File: splitFile[1],
        };
        this.RequestStepOne.stepOne.attachments.file.push(templateFile);
        this.dataSource_file = new MatTableDataSource(
          this.RequestStepOne.stepOne.attachments.file
        );
        this.hideDownloadFile();
      };
    }
    this.changeSaveDraft();
  }

  delete_mutifile(data: any, index: any) {
    console.log('data:', data, 'index::', index);
    const id = this.RequestStepOne.stepOne.attachments.file[index].id;
    if (id) {
      console.log('5555555555555555', this.RequestStepOne.stepOne.attachments.file[index]);
      if ('fileDelete' in this.stepOne.attachments) {
        this.RequestStepOne.stepOne.attachments.fileDelete.push(id);
      }
    }
    this.RequestStepOne.stepOne.attachments.file.splice(index, 1);
    console.log('66666666666666666 : ', this.RequestStepOne.stepOne.attachments.fileDelete);
    console.log('RequestStepOne : ', this.RequestStepOne.stepOne.attachments.file);
    this.dataSource_file = new MatTableDataSource(
      this.RequestStepOne.stepOne.attachments.file
    );
    this.changeSaveDraft();
  }

  changeSaveDraft() {
    this.saveDraftstatus = null;
    this.sidebarService.inPageStatus(true);
  }

  // ============================== Modal ==========================================
  imageModal(fileIndex, index) {
    if (fileIndex === 1) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image1;
      this.modalImagedetail = this.stepOne.objective.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 2) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image2;
      this.modalImagedetail = this.stepOne.businessModel.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 3) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image3;
      this.modalImagedetail = this.stepOne.detail.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    }
  }
  // ===============================================================================

  hideDownloadFile() {
    if (this.dataSource_file.data.length > 0) {
      this.downloadfile_by = [];

      for (let index = 0; index < this.dataSource_file.data.length; index++) {
        const element = this.dataSource_file.data[index];
        console.log('ele', element);

        if ('id' in element) {
          this.downloadfile_by.push('path');
        } else {
          this.downloadfile_by.push('base64');
        }
      }
    }
    console.log('by:', this.downloadfile_by);

  }
  downloadFile(pathdata: any) {
    this.status_loading = true;
    const contentType = '';
    const sendpath = pathdata;
    console.log('path', sendpath);
    this.sidebarService.getfile(sendpath).subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        this.status_loading = false;
        const datagetfile = res['data'];
        const b64Data = datagetfile['data'];
        const filename = datagetfile['fileName'];
        console.log('base64', b64Data);
        const config = this.b64toBlob(b64Data, contentType);
        saveAs(config, filename);
      }

    });
  }

  b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const convertbyte = atob(b64Data);
    const byteCharacters = atob(convertbyte);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  downloadFile64(data: any) {
    this.status_loading = true;
    const contentType = '';
    this.status_loading = false;
    const b64Data = data.base64File;
    const filename = data.fileName;
    console.log('base64', b64Data);
    console.log('filename', filename);
    const config = this.base64(b64Data, contentType);
    saveAs(config, filename);
  }

  base64(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }
  // ===============================================================================
  sandwork() {
    this.status_loading = true;
    console.log('save summary base-step-2');
    this.sendWorkService.sendWork(localStorage.getItem('requestId')).subscribe(res_sendwork => {
      // this.sidebarService.sendEvent();
      if (res_sendwork['message'] === 'success') {
        this.status_loading = false;
        this.router.navigate(['/comment']);
      }
      if (res_sendwork['message'] === 'กรุณากรอกข้อมูลผู้ดำเนินการ') {
        this.status_loading = false;
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
        this.router.navigate(['/save-manager']);
      } else if (res_sendwork['status'] === 'fail' || res_sendwork['status'] === 'error') {
        this.status_loading = false;
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
      } else {
        this.router.navigate(['/comment']);
      }
    });
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate request');
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef =  this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        if (data.returnStatus === true) {
          const raturnStatus = this.RequestStepOne.saveDraft();
          console.log('summary raturnStatus', raturnStatus);
            if (raturnStatus) {
              console.log('raturnStatus T', this.RequestStepOne.saveDraftstatus);
              subject.next(true);
            } else {
              console.log('raturnStatus F', this.RequestStepOne.saveDraftstatus);
              subject.next(false);
            }
        } else if (data.returnStatus === false) {
          this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
        }
      });
      return subject;
    } else {
      return true;
    }
  }
}
