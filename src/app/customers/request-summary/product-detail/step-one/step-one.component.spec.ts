import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepOneSummaryComponent } from './step-one.component';

describe('StepOneSummaryComponent', () => {
  let component: StepOneSummaryComponent;
  let fixture: ComponentFixture<StepOneSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepOneSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepOneSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
