import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepElevenSummaryComponent } from './step-eleven.component';

describe('StepElevenSummaryComponent', () => {
  let component: StepElevenSummaryComponent;
  let fixture: ComponentFixture<StepElevenSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepElevenSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepElevenSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
