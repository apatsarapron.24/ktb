import { Component, OnInit, Input, Output, EventEmitter, ViewChild, DoCheck } from '@angular/core';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { StepTenService } from './../../../../services/request/product-detail/step-ten/step-ten.service';
import { RequestService } from './../../../../services/request/request.service';
import { StepTenComponent } from '../../../request/product-detail/step-ten/step-ten.component';
import { saveAs } from 'file-saver';
import { SendWorkService } from './../../../../services/request/product-detail/send-work/send-work.service';
import Swal from 'sweetalert2';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogSaveStatusComponent } from '../../../request/dialog/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from '../../../../services/configGuard/config-guard.service';

export interface FileList {
  fileName: string;
  fileSize: string;
  base64File: string;
}

const File_data: FileList[] = [];

@Component({
  selector: 'app-step-eleven-summary',
  templateUrl: './step-eleven.component.html',
  styleUrls: ['./step-eleven.component.scss']
})
export class StepElevenSummaryComponent implements OnInit, DoCheck {

  saveDraftstatus = null;
  outOfPageSave = false;
  linkTopage = null;
  pageChange = null;
  openDoCheckc = false;

  testfile = {};
  @Output() saveStatus = new EventEmitter<boolean>();
  @ViewChild(StepTenComponent) RequestStepTen: StepTenComponent;
  finance_1: any;
  finance_2: any;
  finance_3: any;
  finance_4: any;
  finance_5: any = '';
  input_finance_5: any = '';
  finance_5_sub: any = '';
  status_loading = true;

  list_finance_1: string[] = ['ตามหลักเกณฑ์ธนาคาร', 'กำหนดใหม่'];
  list_finance_2: string[] = ['ชุด Field เดิม', 'เพิ่ม Field'];
  list_finance_3: string[] = [
    'ได้รับความเห็นชอบจากสายงานบริหารการเงิน / MIS',
    'ไม่กระทบต่อการจัดสรรรายได้และค่าใช้จ่าย',
  ];
  list_finance_4: string[] = ['มี', 'ไม่มี'];
  list_finance_5: string[] = [
    'ไม่จัดทำ IFRS 9 Chectlist',
    'จัดทำ IFRS 9 Chectlist',
  ];
  list_finance_5_sub: string[] = [
    'ไม่ใช้ทางเลือก (จัดทำเฉพาะแบบฟอร์ม TFRS9)',
    'ใช้สิทธิเลือก การวัดมูลค่ายุติธรรมผ่านกำไรหรือขาดทุน (จัดทำแบบฟอร์ม FVTPL Option เพิ่มเติม)',
    'ใช้สิทธิเลือก การวัดมูลค่ายุติธรรมผ่านกำไรหรือขาดทุนเบ็ดเสร็จอื่น (จัดทำแบบฟอร์ม FVTOCI Option เพิ่มเติม)',
  ];

  keep_list_select: any = {
    radio_1: '',
    radio_2: '',
    radio_3: '',
    radio_4: '',
    radio_5: {
      check: '',
      input: '',
      select: '',
    },
  };

  topic5: any = {
    'NotIFRS9': {
      'flag': false,
      'reason': ''
    },
    'CreateIFRS9': {
      'flag': false,
      'TFRS9': false,
      'FVTPL': false,
      'FVTOCI': false
    }
  };
  fileUpload: any = {
    'file': [],
    'fileDelete': []
  };

  urls: any = [];
  displayedColumns_file: string[] = ['name', 'size', 'delete'];
  dataSource_file: MatTableDataSource<FileList>;

  displayedColumns_file_summary: string[] = ['name', 'size', 'download'];
  dataSource_file_summary: MatTableDataSource<FileList>;


  results: any;
  api_response: any;
  validate: any;
  listSummaryStep11: any = [];
  showsearch: any = [];
  showData = false;
  userRole = null;
  downloadfile_by = [];

  constructor(private router: Router,
    public stepTenService: StepTenService,
    private sidebarService: RequestService,
    private sendWorkService: SendWorkService,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.openDoCheckc = true;
    if (localStorage.getItem('requestId')) {
      this.sidebarService.sendEvent();
    }
    if (localStorage) {
      if (localStorage.getItem('requestId')) {
        this.stepTenService.getDetail(localStorage.getItem('requestId')).subscribe(ress => {
          if (ress['data'] !== null) {
            this.dataSource_file = new MatTableDataSource(ress['data']['fileUplode'].file);
            this.hideDownloadFile();
            this.userRole = localStorage.getItem('role');
            console.log('userRole summary step8', this.userRole);
          } else {
            this.dataSource_file = new MatTableDataSource([]);
          }

        });
        this.getDetail(localStorage.getItem('requestId'));
        this.getList();
      } else {
        this.status_loading = false;
      }
    } else {
      console.log('Brownser not support');
    }
  }
  getList() {
    this.stepTenService.getDetail(localStorage.getItem('requestId')).subscribe(res => {
      if (res['parentRequestId'] !== null) {
        this.listSummaryStep11 = res['parentRequestId'];
        for (let index = 0; index < this.listSummaryStep11.length; index++) {
          this.showsearch.push({ index: index, value: true });
        }
      } else {
        this.listSummaryStep11 = [];
      }


    });
  }
  sendSaveStatus() {
    if (this.saveDraftstatus === 'success') {
      this.saveStatus.emit(true);
    } else {
      this.saveStatus.emit(false);
    }
  }

  changeSaveDraft() {
    this.saveDraftstatus = null;
    this.sendSaveStatus();
    this.sidebarService.inPageStatus(true);
  }

  next_step() {
    this.router.navigate(['/request-summary/product-detail/step12']);
  }
  viewNext() {
    this.router.navigate(['request-summary/product-detail/step12']);
  }

  getDetail(requestId) {

    this.stepTenService.getDetail(requestId).subscribe(res => {
      console.log('pppppp', res);
      this.results = res;
      console.log('data', this.results.data);
      if (res['status'] === 'success') {
        this.status_loading = false;
        this.validate = res['validate'];
      }
      if (this.results.data !== null) {
        this.finance_1 = this.results.data.topic1;
        this.finance_2 = this.results.data.topic2;
        this.finance_3 = this.results.data.topic3;
        this.finance_4 = this.results.data.topic4;
        this.topic5 = this.results.data.topic5;
        this.urls = [];
        for (let i = 0; i < this.results.data.fileUplode.file.length; i++) {
          this.urls.push(this.results.data.fileUplode.file[i]);
        }
        this.fileUpload.file = this.urls;
        console.log(this.fileUpload);
        if (this.results.data.topic5.NotIFRS9.flag === true) {
          this.finance_5 = 'sub_1';
          this.input_finance_5 = this.results.data.topic5.NotIFRS9.reason;
        }
        if (this.results.data.topic5.CreateIFRS9.flag === true) {
          this.finance_5 = 'sub_2';
          if (this.results.data.topic5.CreateIFRS9.TFRS9 === true) {
            this.finance_5_sub = 'ไม่ใช้ทางเลือก (จัดทำเฉพาะแบบฟอร์ม TFRS9)';
          }
          if (this.results.data.topic5.CreateIFRS9.FVTPL === true) {
            this.finance_5_sub = 'ใช้สิทธิเลือก การวัดมูลค่ายุติธรรมผ่านกำไรหรือขาดทุน (จัดทำแบบฟอร์ม FVTPL Option เพิ่มเติม)';
          }
          if (this.results.data.topic5.CreateIFRS9.FVTOCI === true) {
            // tslint:disable-next-line:max-line-length
            this.finance_5_sub = 'ใช้สิทธิเลือก การวัดมูลค่ายุติธรรมผ่านกำไรหรือขาดทุนเบ็ดเสร็จอื่น (จัดทำแบบฟอร์ม FVTOCI Option เพิ่มเติม)';
          }
        }
        console.log('url', this.urls);
        // this.dataSource_file = new MatTableDataSource(this.RequestStepTen.urls);
        this.dataSource_file_summary = new MatTableDataSource(this.urls);
        console.log('datasource', this.dataSource_file);
      } else {
        this.validate = res['validate'];

      }
    });
  }

  validateStatus() {
    let status = false;
    if (this.finance_1 &&
      this.finance_2 &&
      this.finance_3 &&
      this.finance_4 &&
      (this.finance_5 && (this.input_finance_5 || this.finance_5_sub))) {
      status = true;
    } else {
      status = false;
    }
    console.log('status : ', status);
    return status;
  }

  select_finance(index: any, event) {
    console.log('index:', index, 'select:', event.value);

    if (index === 1) {
      this.finance_1 = event.value;
      this.keep_list_select.radio_1 = this.finance_1;
    } else if (index === 2) {
      this.finance_2 = event.value;
      this.keep_list_select.radio_2 = this.finance_2;
    } else if (index === 3) {
      this.finance_3 = event.value;
      this.keep_list_select.radio_3 = this.finance_3;
    } else if (index === 4) {
      this.finance_4 = event.value;
      this.keep_list_select.radio_4 = this.finance_4;
    } else if (index === 5) {
      this.finance_5 = event.value;
      console.log('5:', this.finance_5);
      // this.keep_list_select.radio_5.check =this.finance_5
      if (this.finance_5 === 'sub_1') {
        this.keep_list_select.radio_5.check = this.list_finance_5[0];
        this.keep_list_select.radio_5.select = '';
        this.finance_5_sub = '';
        this.topic5.NotIFRS9.flag = true;
        this.topic5.CreateIFRS9.flag = !this.topic5.NotIFRS9.flag;
      } else {
        this.keep_list_select.radio_5.check = this.list_finance_5[1];
        this.keep_list_select.radio_5.input = '';
        this.input_finance_5 = '';
        this.topic5.CreateIFRS9.flag = true;
        this.topic5.NotIFRS9.flag = !this.topic5.CreateIFRS9.flag;
        // this.sub_select(event);
      }
    }

    console.log('list_radio:', this.keep_list_select);
  }

  sub_select(event) {
    // this.finance_5_sub = event.value;
    // console.log("e:", event);
    console.log('55555:', this.finance_5);
    console.log('dkdkdk', event);
    if (this.finance_5 === 'sub_2') {
      this.keep_list_select.radio_5.select = '';
      this.topic5.NotIFRS9.reason = '';
      if (event.value === 'ไม่ใช้ทางเลือก (จัดทำเฉพาะแบบฟอร์ม TFRS9)') {
        this.topic5.CreateIFRS9.TFRS9 = true;
        this.topic5.CreateIFRS9.FVTPL = !this.topic5.CreateIFRS9.TFRS9;
        this.topic5.CreateIFRS9.FVTOCI = !this.topic5.CreateIFRS9.TFRS9;
      }
      if (event.value === 'ใช้สิทธิเลือก การวัดมูลค่ายุติธรรมผ่านกำไรหรือขาดทุน (จัดทำแบบฟอร์ม FVTPL Option เพิ่มเติม)') {
        this.topic5.CreateIFRS9.FVTPL = true;
        this.topic5.CreateIFRS9.TFRS9 = !this.topic5.CreateIFRS9.FVTPL;
        this.topic5.CreateIFRS9.FVTOCI = !this.topic5.CreateIFRS9.FVTPL;
      }
      if (event.value === 'ใช้สิทธิเลือก การวัดมูลค่ายุติธรรมผ่านกำไรหรือขาดทุนเบ็ดเสร็จอื่น (จัดทำแบบฟอร์ม FVTOCI Option เพิ่มเติม)') {
        this.topic5.CreateIFRS9.FVTOCI = true;
        this.topic5.CreateIFRS9.TFRS9 = !this.topic5.CreateIFRS9.FVTOCI;
        this.topic5.CreateIFRS9.FVTPL = !this.topic5.CreateIFRS9.FVTOCI;
      }
    } else {
      this.keep_list_select.radio_5.select = event.value;
      this.keep_list_select.radio_5.input = this.input_finance_5;
      this.topic5.NotIFRS9.reason = this.input_finance_5;
      this.topic5.CreateIFRS9.TFRS9 = this.topic5.CreateIFRS9.FVTPL = this.topic5.CreateIFRS9.FVTOCI = false;
    }

    console.log('list_radio_222:', this.keep_list_select);
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      // tslint:disable-next-line: prefer-const
      let filesAmount = event.target.files.length;
      const file = event.target.files;
      for (let i = 0; i < filesAmount; i++) {
        console.log('type:', file[i]);
        if (
          // file[i].type === 'image/svg+xml' ||
          file[i].type === 'image/jpeg' ||
          // file[i].type === 'image/raw' ||
          // file[i].type === 'image/svg' ||
          // file[i].type === 'image/tif' ||
          // file[i].type === 'image/gif' ||
          file[i].type === 'image/jpg' ||
          file[i].type === 'image/png' ||
          file[i].type === 'application/doc' ||
          file[i].type === 'application/pdf' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
          file[i].type === 'application/pptx' ||
          file[i].type === 'application/docx' ||
          (file[i].type === 'application/ppt' && file[i].size)
        ) {
          if (file[i].size <= 5000000) {
            this.multiple_file(file[i], i);
          }

        } else {
          this.status_loading = false;
          alert('File Not Support');
        }
      }
    }
  }

  multiple_file(file, index) {
    console.log('download');
    const reader = new FileReader();
    if (file) {
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');
        // this.urls.push({
        const x = {
          fileName: file.name,
          fileSize: file.size,
          base64File: splitFile[1],
        };
        this.testfile = x;
        // });
        this.urls.push(x);
        this.RequestStepTen.fileUpload.file.push(x);

        console.log('this.urls', this.urls);
        this.dataSource_file = new MatTableDataSource(this.urls);
        this.hideDownloadFile();
      };
    }
    this.changeSaveDraft();
  }
  // delete_mutifile(data: any, index: any) {
  //   console.log('data:', data, 'index::', index);
  //   this.dataSource_file.data.splice(index, 1)
  //   this.dataSource_file = new MatTableDataSource(this.dataSource_file.data);
  // }
  delete_mutifile(data: any, index: any) {
    console.log('data:', data, 'index::', index);
    if ('id' in this.urls[index]) {
      const id = this.fileUpload.file[index].id;
      console.log('found id');
      if ('fileDelete' in this.fileUpload) {
        this.fileUpload.fileDelete.push(id);
        this.RequestStepTen.fileUpload.fileDelete.push(id);
      }
    }
    console.log('fileDelete attachments : ', this.fileUpload.fileDelete);
    this.urls.splice(index, 1);
    this.RequestStepTen.fileUpload.file.splice(index, 1);
    this.dataSource_file = new MatTableDataSource(this.urls);
    this.changeSaveDraft();
  }

  downloadFile(pathdata: any) {
    this.status_loading = true;
    const contentType = '';
    const sendpath = pathdata;
    console.log('path', sendpath);
    this.sidebarService.getfile(sendpath).subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        this.status_loading = false;
        const datagetfile = res['data'];
        const b64Data = datagetfile['data'];
        const filename = datagetfile['fileName'];
        console.log('base64', b64Data);
        const config = this.b64toBlob(b64Data, contentType);
        saveAs(config, filename);
      }

    });
  }

  b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const convertbyte = atob(b64Data);
    const byteCharacters = atob(convertbyte);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  downloadFile64(data: any) {
    this.status_loading = true;
    const contentType = '';
    this.status_loading = false;
    const b64Data = data.base64File;
    const filename = data.fileName;
    console.log('base64', b64Data);

    const config = this.base64(b64Data, contentType);
    saveAs(config, filename);
  }

  base64(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  hideDownloadFile() {
    if (this.dataSource_file.data.length > 0) {
      this.downloadfile_by = [];

      for (let index = 0; index < this.dataSource_file.data.length; index++) {
        const element = this.dataSource_file.data[index];
        console.log('ele', element);

        if ('id' in element) {
          this.downloadfile_by.push('path');
        } else {
          this.downloadfile_by.push('base64');
        }
      }
    }
    console.log('by:', this.downloadfile_by);
  }

  async saveDraftData(page?, action?) {
    // set pages
    let pages = 'summary';
    if (page) {
      pages = page;
    }
    // set actions
    let actions = 'save';
    if (action) {
      actions = action;
    }
    // ======================================
    console.log('saved');
    this.RequestStepTen.fileUpload.file = [];
    this.RequestStepTen.fileUpload.fileDelete = [];
    const result1 = await this.save(pages, actions);
    const result2 = await this.result();
    console.log('result2', result2);
    this.saveDraftstatus = result2;
    if (this.saveDraftstatus === 'success') {
      const after = await this.AfterStart();
    }
  }

  save(page?, action?): Promise<any> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        this.RequestStepTen.fileUpload.file = this.fileUpload.file;
        this.RequestStepTen.fileUpload.fileDelete = this.fileUpload.fileDelete;

        return resolve(this.RequestStepTen.saveDraft(page, action));
      }, 100);
    });
  }
  result() {
    return new Promise((resolve, reject) => {
      setTimeout(
        () => console.log('result:', resolve(this.RequestStepTen.saveDraftstatus)),
        1000
      );
    });
  }
  AfterStart() {
    return new Promise((resolve, reject) => {
      setTimeout(
        () => {
          this.fileUpload.file = [];
          this.fileUpload.fileDelete = [];
          this.getList();
          // this.date_summary = [];
          this.getDetail(
            localStorage.getItem('requestId')
          );
          this.saveDraftstatus = 'success';
          setTimeout(() => {
            this.saveDraftstatus = 'fail';
          }, 3000);
        }
        ,
        500
      );
    });
  }

  sandwork() {
    this.status_loading = true;
    console.log('save summary base-step-2');
    this.sendWorkService.sendWork(localStorage.getItem('requestId')).subscribe(res_sendwork => {
      // this.sidebarService.sendEvent();
      if (res_sendwork['message'] === 'success') {
        this.status_loading = false;
        this.router.navigate(['/comment']);
      }
      if (res_sendwork['message'] === 'กรุณากรอกข้อมูลผู้ดำเนินการ') {
        this.status_loading = false;
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
        this.router.navigate(['/save-manager']);
      } else if (res_sendwork['status'] === 'fail' || res_sendwork['status'] === 'error') {
        this.status_loading = false;
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
      } else {
        this.router.navigate(['/comment']);
      }
    });
  }

  show() {
    if (this.showData === true) {
      this.showData = false;
    } else {
      this.showData = true;
    }
  }
  showSearch(index: any) {
    if (this.showsearch[index].value === true) {
      this.showsearch[index].value = false;
    } else {
      this.showsearch[index].value = true;
    }
  }
  getSummarNew(index: any, requestId: any) {
    console.log(index);
    console.log(requestId);
    this.getDetail(requestId);
    for (let i = 0; i < this.showsearch.length; i++) {
      if (index !== this.showsearch[i].index) {
        this.showsearch[i].value = true;
      }
    }
  }

  // ngOnDestroy(): void {
  //   const data = this.sidebarService.getsavePage();
  //   let saveInFoStatus = false;
  //   saveInFoStatus = data.saveStatusInfo;
  //   if (saveInFoStatus === true) {
  //     this.outOfPageSave = true;
  //     this.linkTopage = data.goToLink;
  //     console.log('ngOnDestroy', this.linkTopage);
  //     this.saveDraft();
  //     this.sidebarService.resetSaveStatusInfo();
  //   }
  // }

  ngDoCheck() {
    if (this.openDoCheckc === true) {
      this.pageChange = this.sidebarService.changePageGoToLink();
      if (this.pageChange) {
        const data = this.sidebarService.getsavePage();
        let saveInFoStatus = false;
        saveInFoStatus = data.saveStatusInfo;
        if (saveInFoStatus === true) {
          this.outOfPageSave = true;
          this.linkTopage = data.goToLink;
          this.saveDraftData('navbarSummary', this.linkTopage);
          console.log('saveInFoStatus 11', saveInFoStatus);
          this.sidebarService.resetSaveStatu();
        }
      }
    }
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate request');
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef =  this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        if (data.returnStatus === true) {
          const raturnStatus = this.RequestStepTen.saveDraft();
          console.log('summary raturnStatus', raturnStatus);
            if (raturnStatus) {
              console.log('raturnStatus T', this.RequestStepTen.saveDraftstatus);
              subject.next(true);
            } else {
              console.log('raturnStatus F', this.RequestStepTen.saveDraftstatus);
              subject.next(false);
            }
        } else if (data.returnStatus === false) {
          this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
        }
      });
      return subject;
    } else {
      return true;
    }
  }
}
