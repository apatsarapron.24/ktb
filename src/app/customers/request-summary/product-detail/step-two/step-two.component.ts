import { dateFormat } from 'highcharts';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { IMyDateModel, AngularMyDatePickerDirective, IAngularMyDpOptions } from 'angular-mydatepicker';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { StepTwoSummaryService } from '../../../../services/request-summary/product-detail/step-two/step-two.service';
import { RequestService } from '../../../../services/request/request.service';
import { DateComponent } from '../../../datepicker/date/date.component';
import { SendWorkService } from './../../../../services/request/product-detail/send-work/send-work.service';
import Swal from 'sweetalert2';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogSaveStatusComponent } from '../../../request/dialog/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from './../../../../services/configGuard/config-guard.service';

@Component({
  selector: 'app-step-two-summary',
  templateUrl: './step-two.component.html',
  styleUrls: ['./step-two.component.scss']
})
export class StepTwoProductSummaryComponent implements OnInit, CanComponentDeactivate {
  @Input() template_manage = { pageShow: null };
  @Output() saveStatus = new EventEmitter<boolean>();

  stepTwoData = [{
    status: false,
    requestId: null,
    complaintMarketConduct: [{
      id: null,
      complaint: null,
      modify: null,
      result: null,
      process: null,
      modifyDate: null,
    }],
    complaintOther: [{
      id: null,
      complaint: null,
      modify: null,
      result: null,
      process: null,
      modifyDate: null,
    }],
    complaintCommon: [{
      id: null,
      complaint: null,
      modify: null,
      result: null,
      process: null,
      modifyDate: null,
    }],
    complaintMarketConductDelete: [],
    complaintOtherDelete: [],
    complaintCommonDelete: [],
  }];
  // status
  saveDraftstatus = null;
  outOfPageSave = false;
  linkTopage = null;
  pageChange = null;
  openDoCheckc = false;

  loopname = 'a1';
  validate = false;
  userRole = null;

  parentRequestId = [];

  showsearch: any = [];
  showData = false;
  status_loading = true;
  model: IMyDateModel = null;


  @ViewChild('dp') mydp: AngularMyDatePickerDirective;
  @ViewChild(DateComponent) Date: DateComponent;


  saveDraftData(page?, action?) {
    let MarketConductStatus = false;
    let OtherStatus = false;
    let CommonStatus = false;

    for (let index = 0; index < this.stepTwoData[0].complaintMarketConduct.length; index++) {
      const ele = this.stepTwoData[0].complaintMarketConduct[index];

      if (ele) {
        if (ele.complaint === null || ele.complaint === '') {
          MarketConductStatus = false;
          break;
        } else if (ele.modify === null || ele.modify === '') {
          MarketConductStatus = false;
          break;
        } else if (ele.result === null || ele.result === '') {
          MarketConductStatus = false;
          break;
        } else if (ele.process === null || ele.process === '') {
          MarketConductStatus = false;
          break;
        } else {
          MarketConductStatus = true;
        }
      }
    }

    for (let index = 0; index < this.stepTwoData[0].complaintOther.length; index++) {
      const ele = this.stepTwoData[0].complaintOther[index];

      if (ele) {
        if (ele.complaint === null || ele.complaint === '') {
          OtherStatus = false;
          break;
        } else if (ele.modify === null || ele.modify === '') {
          OtherStatus = false;
          break;
        } else if (ele.result === null || ele.result === '') {
          OtherStatus = false;
          break;
        } else if (ele.process === null || ele.process === '') {
          OtherStatus = false;
          break;
        } else {
          OtherStatus = true;
        }
      }
    }

    for (let index = 0; index < this.stepTwoData[0].complaintCommon.length; index++) {
      const ele = this.stepTwoData[0].complaintCommon[index];

      if (ele) {
        if (ele.complaint === null || ele.complaint === '') {
          CommonStatus = false;
          break;
        } else if (ele.modify === null || ele.modify === '') {
          CommonStatus = false;
          break;
        } else if (ele.result === null || ele.result === '') {
          CommonStatus = false;
          break;
        } else if (ele.process === null || ele.process === '') {
          CommonStatus = false;
          break;
        } else {
          CommonStatus = true;
        }
      }
    }

    if (MarketConductStatus !== false && OtherStatus !== false && CommonStatus !== false) {
      this.stepTwoData[0].status = true;
    } else {
      this.stepTwoData[0].status = false;
    }
    this.stepTwoData[0].requestId = localStorage.getItem('requestId');
    console.log('senddata stepTwoData', this.stepTwoData[0]);
    const returnUpdateData = new Subject<any>();
    this.stepTwoSummaryService.updateStepTwo(this.stepTwoData[0]).subscribe(res => {
      if (res['status'] === 'success') {
        this.status_loading = false;
        this.saveDraftstatus = 'success';
        returnUpdateData.next(true);
        this.sidebarService.inPageStatus(false);
        setTimeout(() => {
          this.saveDraftstatus = 'fail';
        }, 3000);
        this.sidebarService.sendEvent();
        this.sendSaveStatus();
        this.getStepTwo(localStorage.getItem('requestId'));
        if (page === 'navbar') {
          this.router.navigate([this.linkTopage]);
        }
        returnUpdateData.next(false);
      } else {
        this.status_loading = false;
        alert(res['message']);
        returnUpdateData.next(false);
      }
    });
    return returnUpdateData;

  }

  next_step_summary() {
    this.router.navigate(['/request-summary/product-detail/step3']);
  }

  addTable(indexTable) {
    if (indexTable === 0) {
      this.stepTwoData[0].complaintMarketConduct.push({
        id: null,
        complaint: null,
        modify: null,
        result: null,
        process: null,
        modifyDate: null,
      });
    } else if (indexTable === 1) {
      this.stepTwoData[0].complaintOther.push({
        id: null,
        complaint: null,
        modify: null,
        result: null,
        process: null,
        modifyDate: null,
      });
    } else if (indexTable === 2) {
      this.stepTwoData[0].complaintCommon.push({
        id: null,
        complaint: null,
        modify: null,
        result: null,
        process: null,
        modifyDate: null,
      });
    }
  }

  deleteTable(indexTable, index) {
    console.log('5555555555555555 indexTable', indexTable);
    console.log('5555555555555555 index', index);
    if (indexTable === 0) {
      if (this.stepTwoData[0].complaintMarketConduct[index].id) {
        this.stepTwoData[0].complaintMarketConductDelete.push(this.stepTwoData[0].complaintMarketConduct[index].id);
      }
      if (this.stepTwoData[0].complaintMarketConduct.length > 1) {
        this.stepTwoData[0].complaintMarketConduct.splice(index, 1);
      } else {
        this.stepTwoData[0].complaintMarketConduct[index].id = null;
        this.stepTwoData[0].complaintMarketConduct[index].complaint = null;
        this.stepTwoData[0].complaintMarketConduct[index].modify = null;
        this.stepTwoData[0].complaintMarketConduct[index].result = null;
        this.stepTwoData[0].complaintMarketConduct[index].process = null;
        this.stepTwoData[0].complaintMarketConduct[index].modifyDate = null;
      }
    } else if (indexTable === 1) {
      if (this.stepTwoData[0].complaintOther[index].id) {
        this.stepTwoData[0].complaintOtherDelete.push(this.stepTwoData[0].complaintOther[index].id);
      }
      if (this.stepTwoData[0].complaintOther.length > 1) {
        this.stepTwoData[0].complaintOther.splice(index, 1);
      } else {
        this.stepTwoData[0].complaintOther[index].id = null;
        this.stepTwoData[0].complaintOther[index].complaint = null;
        this.stepTwoData[0].complaintOther[index].modify = null;
        this.stepTwoData[0].complaintOther[index].result = null;
        this.stepTwoData[0].complaintOther[index].process = null;
        this.stepTwoData[0].complaintOther[index].modifyDate = null;
      }
    } else if (indexTable === 2) {
      if (this.stepTwoData[0].complaintCommon[index].id) {
        this.stepTwoData[0].complaintCommonDelete.push(this.stepTwoData[0].complaintCommon[index].id);
      }
      console.log('5555555555555555', this.stepTwoData[0].complaintCommon.length);
      if (this.stepTwoData[0].complaintCommon.length > 1) {
        this.stepTwoData[0].complaintCommon.splice(index, 1);
      } else {
        this.stepTwoData[0].complaintCommon[index].id = null;
        this.stepTwoData[0].complaintCommon[index].complaint = null;
        this.stepTwoData[0].complaintCommon[index].modify = null;
        this.stepTwoData[0].complaintCommon[index].result = null;
        this.stepTwoData[0].complaintCommon[index].process = null;
        this.stepTwoData[0].complaintCommon[index].modifyDate = null;
      }
    }
    this.changeSaveDraft();
  }
  EvaluationDrop(event: CdkDragDrop<string[]>, index) {
    if (index === 0) {
      moveItemInArray(
        this.stepTwoData[0].complaintMarketConduct,
        event.previousIndex,
        event.currentIndex
      );
    } else if (index === 1) {
      moveItemInArray(
        this.stepTwoData[0].complaintOther,
        event.previousIndex,
        event.currentIndex
      );
    } else if (index === 2) {
      moveItemInArray(
        this.stepTwoData[0].complaintCommon,
        event.previousIndex,
        event.currentIndex
      );
    }
    this.changeSaveDraft();
  }
  changeSaveDraft() {
    this.saveDraftstatus = null;
    this.sendSaveStatus();
    this.sidebarService.inPageStatus(true);
  }


  // dateFormats(date: any) {
  //   let  dateForm = '';
  //   if ( Object.keys(date).includes('singleDate')) {
  //    dateForm = moment(date.singleDate.jsDate).format('DD/MM/YY');
  //   }
  //    return dateForm;
  //  }

  constructor(
    private router: Router,
    private stepTwoSummaryService: StepTwoSummaryService,
    private sidebarService: RequestService,
    private sendWorkService: SendWorkService,
    public dialog: MatDialog,
  ) { }

  async ngOnInit() {
    window.scrollTo(0, 0);
    this.openDoCheckc = true;
    this.sidebarService.sendEvent();
    if (localStorage) {
      if (localStorage.getItem('requestId')) {
        await this.getParentRequestId();
        await this.getStepTwo(localStorage.getItem('requestId'));
      }
    } else {
      this.status_loading = false;
      console.log('Brownser not support');
    }
  }

  getStepTwo(requestId) {
    this.stepTwoSummaryService.getStepTwo(requestId).subscribe(res => {
      if (res['status'] === 'success') {
        this.status_loading = false;
        const getdata = res['data'];
        this.validate = getdata.validate;
        console.log('validate summary step2', this.validate);
        this.userRole = localStorage.getItem('role');
        console.log('userRole summary step2', this.userRole);
        console.log('getStepTwo res22222', getdata);

        if (getdata['requestId'] !== null) {
          console.log('getStepTwo res', getdata);

          this.stepTwoData[0] = getdata;
          if (this.validate) {
            if (this.stepTwoData[0].complaintMarketConduct.length === 0) {
              this.addTable(0);
            }
            if (this.stepTwoData[0].complaintOther.length === 0) {
              this.addTable(1);
            }
            if (this.stepTwoData[0].complaintCommon.length === 0) {
              this.addTable(2);
            }
          }
          

          // set item Delete stepTwoData
          this.stepTwoData[0].complaintMarketConductDelete = [];
          this.stepTwoData[0].complaintOtherDelete = [];
          this.stepTwoData[0].complaintCommonDelete = [];
          console.log('this.stepTwoData[0] res', this.stepTwoData[0]);
        }
        if (this.template_manage.pageShow === 'executive') {
          this.validate = false;
          console.log('validate summary step2', this.validate);
        }
      }
      console.log('summary step2', this.stepTwoData);
    });
  }

  getParentRequestId() {
    this.stepTwoSummaryService.getStepTwo(localStorage.getItem('requestId')).subscribe(res => {
      if (res['status'] === 'success') {
        console.log('getParentRequestId res', res['data']);
        const getData = res['data'];
        if (getData['parentRequestId']) {
          this.parentRequestId = getData.parentRequestId;
          for (let index = 0; index < this.parentRequestId.length; index++) {
            this.showsearch.push({ index: index, value: true });
          }
          console.log('getParentRequestId res 2', this.parentRequestId);
          this.parentRequestId.forEach(ele => {
            this.stepTwoData.push({
              status: false,
              requestId: null,
              complaintMarketConduct: [{
                id: null,
                complaint: null,
                modify: null,
                result: null,
                process: null,
                modifyDate: null,
              }],
              complaintOther: [{
                id: null,
                complaint: null,
                modify: null,
                result: null,
                process: null,
                modifyDate: null,
              }],
              complaintCommon: [{
                id: null,
                complaint: null,
                modify: null,
                result: null,
                process: null,
                modifyDate: null,
              }],
              complaintMarketConductDelete: [],
              complaintOtherDelete: [],
              complaintCommonDelete: [],
            });
          });
          if (this.parentRequestId) {
            for (let index = 0; index < this.parentRequestId.length; index++) {
              this.showsearch.push({ index: index, value: true });
            }
          }
        }
      }
    });
  }
  showSearch(index: any) {
    if (this.showsearch[index].value === true) {
      this.showsearch[index].value = false;
    } else {
      this.showsearch[index].value = true;
    }
  }
  viewNext() {
    this.router.navigate(['request-summary/product-detail/step3']);
  }
  getDetailParent(requestId, index) {
    console.log('getDetail requestId', requestId);
    console.log('getDetail index', index);
    for (let i = 0; i < this.showsearch.length; i++) {
      if (index !== this.showsearch[i].index) {
        this.showsearch[i].value = true;
      }
    }
    this.stepTwoSummaryService.getStepTwo(requestId).subscribe(res => {
      if (res['status'] === 'success') {
        this.status_loading = false;
        const getdata = res['data'];
        if (getdata['requestId'] !== null) {
          console.log('getStepTwo res', getdata);
          this.stepTwoData[index + 1] = getdata;
          console.log('this.stepTwoData[indax] res', this.stepTwoData[index]);



        }
      }
    });

  }

  changeDate(date: any) {
    console.log('8888', date);
    const SendI = this.Date.changeDate(date);
    this.changeSaveDraft();
    console.log('8888 R', SendI);
    return SendI;
  }

  dateFormat(date: any) {
    if (date) {
      let dateForm = null;
      const year = Number(moment(date).format('YYYY')) + Number(543);
      dateForm = moment(date).format('DD/MM') + '/' + year;
      return dateForm;
    }
    return '';
  }
  sandwork() {
    this.status_loading = true;
    console.log('save summary base-step-2');
    this.sendWorkService.sendWork(localStorage.getItem('requestId')).subscribe(res_sendwork => {
      // this.sidebarService.sendEvent();
      if (res_sendwork['message'] === 'success') {
        this.status_loading = false;
        this.router.navigate(['/comment']);
      }
      if (res_sendwork['message'] === 'กรุณากรอกข้อมูลผู้ดำเนินการ') {
        this.status_loading = false;
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
        this.router.navigate(['/save-manager']);
      } else if (res_sendwork['status'] === 'fail' || res_sendwork['status'] === 'error') {
        this.status_loading = false;
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
      } else {
        this.router.navigate(['/comment']);
      }
    });
  }
  autoGrowTextZone(e) {
    e.target.style.height = '0px';
    e.target.style.height = (e.target.scrollHeight + 5) + 'px';
  }
  show() {
    if (this.showData === true) {
      this.showData = false;
    } else {
      this.showData = true;
    }
  }

  sendSaveStatus() {
    if (this.saveDraftstatus === 'success') {
      this.saveStatus.emit(true);
    } else {
      this.saveStatus.emit(false);
    }
  }

  // ngDoCheck() {
  //   if (this.openDoCheckc === true) {
  //     this.pageChange = this.sidebarService.changePageGoToLink();
  //     if (this.pageChange) {
  //       const data = this.sidebarService.getsavePage();
  //       let saveInFoStatus = false;
  //       saveInFoStatus = data.saveStatusInfo;
  //       if (saveInFoStatus === true) {
  //         this.outOfPageSave = true;
  //         this.linkTopage = data.goToLink;
  //         console.log('ngDoCheck', this.linkTopage);
  //         this.saveDraftData('navbar', null);
  //         this.sidebarService.resetSaveStatu();
  //       }
  //     }
  //   }
  // }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate request');
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef =  this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        if (data.returnStatus === true) {
          const raturnStatus = this.saveDraftData();
          console.log('request raturnStatus', raturnStatus);
            if (raturnStatus) {
              console.log('raturnStatus T', this.saveDraftstatus);
              subject.next(true);
            } else {
              console.log('raturnStatus F', this.saveDraftstatus);
              subject.next(false);
            }
        } else if (data.returnStatus === false) {
          this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
        }
      });
      return subject;
    } else {
      return true;
    }
  }

}
