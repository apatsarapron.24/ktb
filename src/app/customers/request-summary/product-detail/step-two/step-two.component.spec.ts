import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepTwoProductSummaryComponent } from './step-two.component';

describe('StepTwoProductSummaryComponent', () => {
  let component: StepTwoProductSummaryComponent;
  let fixture: ComponentFixture<StepTwoProductSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepTwoProductSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepTwoProductSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
