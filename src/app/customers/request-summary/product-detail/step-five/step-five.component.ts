
import { StepFourComponent } from '../../../request/product-detail/step-four/step-four.component';
import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import {
  AngularMyDatePickerDirective,
  IAngularMyDpOptions,
  IMyDateModel,
} from 'angular-mydatepicker';
import { Router } from '@angular/router';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
  CdkDragExit,
  CdkDragEnter,
  CdkDragStart,
  CdkDragEnd,
} from '@angular/cdk/drag-drop';
import { template } from '@angular/core/src/render3';
import * as moment from 'moment';
import { RequestService } from '../../../../services/request/request.service';
import { StepFourService } from './../../../../services/request/product-detail/step-four/step-four.service';
import { saveAs } from 'file-saver';
import { SendWorkService } from './../../../../services/request/product-detail/send-work/send-work.service';
import Swal from 'sweetalert2';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogSaveStatusComponent } from '../../../request/dialog/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from '../../../../services/configGuard/config-guard.service';

@Component({
  selector: 'app-step-five-summary',
  templateUrl: './step-five.component.html',
  styleUrls: ['./step-five.component.scss']
})
export class StepFiveSummaryComponent implements OnInit, CanComponentDeactivate {

  testfile = {};
  saveDraftstatus = null;
  @ViewChild(StepFourComponent) RequestStepFour: StepFourComponent;
  @Output() saveStatus = new EventEmitter<boolean>();
  displayedColumns_file_sub: string[] = ['name', 'size'];
  dataSource_file_sub: MatTableDataSource<FileList>;


  urls_sub: any = [];

  displayedColumns_file: string[] = ['name', 'size', 'delete'];
  dataSource_file: MatTableDataSource<FileList>;

  displayedColumns_file_summary: string[] = ['name', 'size', 'download'];
  dataSource_file_summary: MatTableDataSource<FileList>;

  urls: any = [];

  nonSOP_document: any;
  nonSOP_file_name: any;
  nonSOP_file_size: number;
  message: string;
  status_loading = true;
  document: any;
  file_name: any;
  file_size: number;

  outOfPageSave = false;
  linkTopage = null;
  pageChange = null;
  openDoCheckc = false;

  model: IMyDateModel = null;
  @ViewChild('dp') mydp: AngularMyDatePickerDirective;
  modelReset = this.model;
  image1 = [];
  file1_1: any;
  file1_2: any;
  // Model
  showModel = false;
  modalmainImageIndex: number;
  modalmainImagedetailIndex: number;
  indexSelect: number;
  modalImage = [];
  modalImagedetail = [];
  fileValue: number;
  add_mainProcess: null;

  SOP: any = '';
  SOPSummary: any = [];
  // new table
  dropIndex: number;
  staticTitleIndex = []; // !important ex [0,1,2] mean set fix row index 0 1 2

  // Risk sub_table
  Risk_dropIndex = 0;
  staticTitleIndexRisk = [];

  stepFour = {
    basicInfo: {
      haveSOP: true,
      noSOP: false,
      newProceeses: false,
    },
    responseProcessSop: {
      refNo: '',
      effectiveDate: '',
      fileUpload: {
        file: [],
        fileDelete: [],
      },
    },
    workFlowText: '',
    workFlow: {
      file: [],
      fileDelete: [],
    },
    requestProcessConsiders: [
      {
        id: '',
        mainProcess: 'ขั้นตอน A',
        subProcess: [
          {
            id: '',
            subTitle: '',
            inOut: null,
            itRelate: null,
            subProcessRisk: [
              {
                id: null,
                risk: null,
                riskControl: '',
                riskControlOther: null,
              },
            ],
          },
        ],
      },
      {
        id: '',
        mainProcess: 'ขั้นตอน B',
        subProcess: [
          {
            id: '',
            subTitle: '',
            inOut: null,
            itRelate: null,
            subProcessRisk: [
              {
                id: null,
                risk: null,
                riskControl: '',
                riskControlOther: null,
              },
            ],
          },
        ],
      },
      {
        id: '',
        mainProcess: 'ขั้นตอน C',
        subProcess: [
          {
            id: '',
            subTitle: '',
            inOut: null,
            itRelate: null,
            subProcessRisk: [
              {
                id: null,
                risk: null,
                riskControl: '',
                riskControlOther: null,
              },
            ],
          },
        ],
      },
      {
        id: '',
        mainProcess: '',
        subProcess: [
          {
            id: '',
            subTitle: '',
            inOut: null,
            itRelate: null,
            subProcessRisk: [
              {
                id: null,
                risk: null,
                riskControl: '',
                riskControlOther: null,
              },
            ],
          },
        ],
      },
    ],
    requestProcessConsidersDelete: [],
    requestProcessConsidersSubDelete: [],
    requestProcessConsidersSubRiskDelete: [],
    fileUpload: {
      file: [],
      fileDelete: [],
    },
    status: false,
  };


  date_summary: any;

  imageList = [];
  data_List: any = [];
  listSummaryStep5: any = [];
  showsearch: any = [];
  showData = false;
  userRole = null;
  downloadfile_by = [];

  constructor(
    private router: Router,
    private stepFourService: StepFourService,
    private sidebarService: RequestService,
    private sendWorkService: SendWorkService,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.openDoCheckc = true;
    if (localStorage) {
      if (localStorage.getItem('requestId')) {
        this.stepFourService.getPage4(localStorage.getItem('requestId')).subscribe((ress) => {
          console.log('resssssss;', ress);
          if (ress['data'] !== null) {
            this.dataSource_file = new MatTableDataSource(ress['data'].fileUpload.file);
            this.hideDownloadFile();
          } else {
            this.stepFour['validate'] = ress['validate'];
            this.dataSource_file = new MatTableDataSource([]);
          }
          this.userRole = localStorage.getItem('role');
          console.log('userRole summary step8', this.userRole);
        });
        this.getDetail(localStorage.getItem('requestId'));
        this.getList();
        this.sidebarService.sendEvent();
      } else {
        this.status_loading = false;
      }
    } else {
      console.log('Brownser not support');
    }
  }
  getList() {
    this.stepFourService.getPage4(localStorage.getItem('requestId')).subscribe((res) => {
      this.listSummaryStep5 = res['parentRequestId'];
      if (this.listSummaryStep5) {
        for (let index = 0; index < this.listSummaryStep5.length; index++) {
          this.showsearch.push({ index: index, value: true });
        }
      }
    });
  }
  sendSaveStatus() {
    if (this.saveDraftstatus === 'success') {
      this.saveStatus.emit(true);
    } else {
      this.saveStatus.emit(false);
    }
  }


  changeSaveDraft() {
    this.saveDraftstatus = null;
    this.sendSaveStatus();
    this.sidebarService.inPageStatus(true);
  }

  getDetail(requestId) {
    this.stepFourService.getPage4(requestId).subscribe((res) => {
      console.log(res);
      if (res['status'] === 'success') {
        this.status_loading = false;
      }
      if (res['status'] === 'success' && res['data'] !== null) {
        console.log(res['data']);
        this.stepFour = res['data'];
        this.stepFour.requestProcessConsidersSubRiskDelete = [];
        // set fileDelete to Object stepFour
        this.stepFour.responseProcessSop.fileUpload.fileDelete = [];
        this.stepFour.workFlow.fileDelete = [];
        this.stepFour.fileUpload.fileDelete = [];
        this.stepFour.requestProcessConsidersSubDelete = [];
        this.stepFour.requestProcessConsidersDelete = [];

        // set datepicker
        if (this.stepFour.responseProcessSop.effectiveDate) {
          const dateString = this.stepFour.responseProcessSop.effectiveDate;
          this.model = {
            isRange: false,
            singleDate: {
              jsDate: new Date(dateString),
            },
          };
          const dateRead = moment(this.model.singleDate.jsDate).format('DD/MM') + '/' +
            Number(Number(moment(this.model.singleDate.jsDate).format('YYYY')) + 543);
          this.date_summary = dateRead;
          console.log('date:', this.date_summary);

        }

        // set hardcode if child length = 0
        this.stepFour.requestProcessConsiders.forEach((mainProcess) => {
          if (mainProcess.subProcess.length === 0) {
            const templateSubProcess = {
              id: '',
              subTitle: '',
              inOut: null,
              itRelate: null,
              subProcessRisk: [
                {
                  id: null,
                  risk: null,
                  riskControl: '',
                  riskControlOther: null,
                },
              ],
            };
            mainProcess.subProcess.push(templateSubProcess);
          }
        });

        // set picture preview to all input
        for (let i = 0; i < this.stepFour.workFlow.file.length; i++) {
          this.preview(1, this.stepFour.workFlow.file[i], i);
        }

        console.log('SOP : ', this.SOP);
        console.log('basicInfo : ', this.stepFour.basicInfo);
        // set first time ratio
        if (this.stepFour.basicInfo.haveSOP === true) {
          this.SOP = '1';
        }
        if (this.stepFour.basicInfo.noSOP === true) {
          this.SOP = '2';
        }
        if (this.stepFour.basicInfo.newProceeses === true) {
          this.SOP = '3';
        }
        console.log('SOP after : ', this.SOP);
        // set file attachments
        this.dataSource_file_sub = new MatTableDataSource(
          this.stepFour.responseProcessSop.fileUpload.file
        );
        // this.dataSource_file = new MatTableDataSource(
        //   this.RequestStepFour.stepFour.fileUpload.file
        // );
        this.dataSource_file_summary = new MatTableDataSource(
          this.stepFour.fileUpload.file
        );
        // set status to stepData
        this.stepFour.status = false;
      }

      console.log('parentRequestId:', this.stepFour['parentRequestId']);

    });
  }


  next_step() {
    this.router.navigate(['/request-summary/product-detail/step6']);
  }

  validateStatus() {
    let status = false;
    const checkForm =
      this.stepFour.workFlowText &&
      (this.stepFour.basicInfo.haveSOP === true ||
        this.stepFour.basicInfo.noSOP === true ||
        this.stepFour.basicInfo.newProceeses === true ||
        this.stepFour.requestProcessConsiders.length > 0);

    console.log('checkForm : ', checkForm);

    status = checkForm;

    console.log('status : ', status);
    return status;
  }

  changeDatepicker(e) {
    console.log('change datepicker : ', e);
    console.log('date single : ', e.singleDate.jsDate);
    const date = moment(e.singleDate.jsDate).format('YYYY-MM-DD');
    console.log(date);
    this.stepFour.responseProcessSop.effectiveDate = date;
  }

  // ratio sop
  clearWorkFlow() {
    console.log(this.stepFour);
    if (this.SOP === '1') {
      this.stepFour.basicInfo.haveSOP = true;
      this.stepFour.basicInfo.noSOP = false;
      this.stepFour.basicInfo.newProceeses = false;
      this.stepFour.responseProcessSop.refNo = null;
      this.stepFour.responseProcessSop.effectiveDate = null;
      this.stepFour.responseProcessSop.fileUpload.file = [];
      this.stepFour.responseProcessSop.fileUpload.fileDelete = [];

      this.dataSource_file_sub = new MatTableDataSource();
      console.log(' this.stepFour', this.stepFour);
    } else if (this.SOP === '2') {
      this.stepFour.basicInfo.haveSOP = false;
      this.stepFour.basicInfo.noSOP = true;
      this.stepFour.basicInfo.newProceeses = false;
    } else if (this.SOP === '3') {
      this.stepFour.basicInfo.haveSOP = false;
      this.stepFour.basicInfo.noSOP = false;
      this.stepFour.basicInfo.newProceeses = true;
      this.stepFour.responseProcessSop.refNo = null;
      this.stepFour.responseProcessSop.effectiveDate = null;
      this.stepFour.responseProcessSop.fileUpload.file = [];
      this.stepFour.responseProcessSop.fileUpload.fileDelete = [];
      this.dataSource_file_sub = new MatTableDataSource();
    }
    this.changeSaveDraft();
  }

  delete_image(fileIndex, index) {
    console.log('fileIndex:', fileIndex);
    console.log('index:', index);
    if (fileIndex === 1) {
      if ('id' in this.stepFour.workFlow.file[index]) {
        const id = this.stepFour.workFlow.file[index].id;
        console.log('found id');
        if ('fileDelete' in this.stepFour.workFlow) {
          this.stepFour.workFlow.fileDelete.push(id);
        }
      }
      console.log('fileDelete objective : ', this.stepFour.workFlow.fileDelete);
      this.stepFour.workFlow.file.splice(index, 1);
      this.image1.splice(index, 1);

      console.log(
        'this.stepFour.objective.file.length2:',
        this.stepFour.workFlow.file.length
      );
      for (let i = 0; i < this.stepFour.workFlow.file.length; i++) {
        this.preview(fileIndex, this.stepFour.workFlow.file[i], i);
      }
    }
    this.changeSaveDraft();
  }

  image_click(colId, id) {
    this.showModel = true;
    console.log('colId:', colId);
    console.log('id:', id);
    // this.showCircle_colId = colId;
    // this.showCircle_id = id;
    document.getElementById('myModalStepFour').style.display = 'block';
    this.imageModal(colId, id);
  }

  image_bdclick() {
    document.getElementById('myModalStepFour').style.display = 'block';
    console.log('2222');
  }

  // ============================== Modal ==========================================
  imageModal(fileIndex, index) {
    if (fileIndex === 1) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image1;
      this.modalImagedetail = this.stepFour.workFlow.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    }
  }
  // ===============================================================================

  closeModal() {
    this.showModel = false;
    document.getElementById('myModalStepFour').style.display = 'none';
  }

  // getFileDetails ================================================================
  getFileDetails(fileIndex, event) {
    const files = event.target.files;

    for (let i = 0; i < files.length; i++) {
      const mimeType = files[i].type;
      console.log(mimeType);
      if (mimeType.match(/image\/*/) == null) {
        this.message = 'Only images are supported.';
        this.status_loading = false;
        alert(this.message);
        return;
      }
      const file = files[i];

      console.log('file:', file);
      const reader = new FileReader();
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');

        const templateFile = {
          fileName: file.name,
          base64File: splitFile[1],
        };
        console.log(templateFile);

        if (fileIndex === 1) {
          console.log('00000000000000000000000000000001');
          this.stepFour.workFlow.file.push(templateFile);
          // console.log('this.stepFour.objective.file : ' , this.stepFour.objective.file)

          for (
            let index = 0;
            index < this.stepFour.workFlow.file.length;
            index++
          ) {
            this.preview(fileIndex, this.stepFour.workFlow.file[index], index);
          }
        }
      };
      reader.readAsDataURL(file);
    }
    this.changeSaveDraft();
  }

  preview(fileIndex, file, index) {
    //  console.log('file : ', file);

    const name = file.fileName;
    const typeFiles = name.match(/\.\w+$/g);
    const typeFile = typeFiles[0].replace('.', '');

    const tempPicture = `data:image/${typeFile};base64,${file.base64File}`;

    if (fileIndex === 1) {
      if (file) {
        this.image1[index] = tempPicture;
      }
    }
  }

  // ======================= Table ==========================

  deleteTable(index) {
    // this.stepFour.loanCriteria.splice(index, 1);
  }

  onSelectFile(fileIndex, event) {
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      const file = event.target.files;
      for (let i = 0; i < filesAmount; i++) {
        console.log('type:', file[i]);
        if (
          // file[i].type === 'image/svg+xml' ||
          file[i].type === 'image/jpeg' ||
          // file[i].type === 'image/raw' ||
          // file[i].type === 'image/svg' ||
          // file[i].type === 'image/tif' ||
          // file[i].type === 'image/gif' ||
          file[i].type === 'image/jpg' ||
          file[i].type === 'image/png' ||
          file[i].type === 'application/doc' ||
          file[i].type === 'application/pdf' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
          file[i].type === 'application/pptx' ||
          file[i].type === 'application/docx' ||
          (file[i].type === 'application/ppt' && file[i].size)
        ) {
          if (file[i].size <= 5000000) {
            this.multiple_file(fileIndex, file[i], i);
          }
        } else {
          this.status_loading = false;
          alert('File Not Support');
        }
      }
    }
    this.changeSaveDraft();
  }

  multiple_file(fileIndex, file, index) {
    console.log('download');
    const reader = new FileReader();
    if (file) {
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');

        const templateFile = {
          fileName: file.name,
          fileSize: file.size,
          base64File: splitFile[1],
        };
        this.testfile = templateFile;

        if (fileIndex === 1) {
          this.stepFour.responseProcessSop.fileUpload.file.push(templateFile);
          this.dataSource_file_sub = new MatTableDataSource(
            this.stepFour.responseProcessSop.fileUpload.file
          );
        }
        if (fileIndex === 2) {
          this.stepFour.fileUpload.file.push(templateFile);
          this.RequestStepFour.stepFour.fileUpload.file.push(templateFile);
          this.dataSource_file = new MatTableDataSource(
            this.stepFour.fileUpload.file
          );
          this.hideDownloadFile();
        }
      };
    }
  }
  delete_mutifile(fileIndex: any, data: any, index: any) {
    console.log('data:', data, 'index::', index);
    if (fileIndex === 1) {
      // check if id exiting
      if ('id' in this.stepFour.responseProcessSop.fileUpload.file[index]) {
        const id = this.stepFour.responseProcessSop.fileUpload.file[index].id;
        console.log('found id');
        if ('fileDelete' in this.stepFour.responseProcessSop.fileUpload) {
          this.stepFour.responseProcessSop.fileUpload.fileDelete.push(id);
        }
      }
      this.stepFour.responseProcessSop.fileUpload.file.splice(index, 1);
      this.dataSource_file_sub = new MatTableDataSource(
        this.stepFour.responseProcessSop.fileUpload.file
      );
    }
    if (fileIndex === 2) {
      // check if id exiting
      console.log('found id 222', this.stepFour.fileUpload.file[index].id);
      if ('id' in this.stepFour.fileUpload.file[index]) {
        const id = this.stepFour.fileUpload.file[index].id;
        console.log('found id', id);
        if ('fileDelete' in this.stepFour.fileUpload) {
          this.stepFour.fileUpload.fileDelete.push(id);
          this.RequestStepFour.stepFour.fileUpload.fileDelete.push(id);
        }
      }

      this.stepFour.fileUpload.file.splice(index, 1);
      this.RequestStepFour.stepFour.fileUpload.file.splice(index, 1);
      this.dataSource_file = new MatTableDataSource(
        this.stepFour.fileUpload.file
      );
    }
    this.changeSaveDraft();
  }

  checkStaticTitle(index) {
    return this.staticTitleIndex.includes(index);
  }
  // ============================================================================================> drag and drop
  // ===============================================================================

  downloadFile(pathdata: any) {
    this.status_loading = true;
    const contentType = '';
    const sendpath = pathdata;
    console.log('path', sendpath);
    this.sidebarService.getfile(sendpath).subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        this.status_loading = false;
        const datagetfile = res['data'];
        const b64Data = datagetfile['data'];
        const filename = datagetfile['fileName'];
        console.log('base64', b64Data);
        const config = this.b64toBlob(b64Data, contentType);
        saveAs(config, filename);
      }

    });
  }

  b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const convertbyte = atob(b64Data);
    const byteCharacters = atob(convertbyte);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  downloadFile64(data: any) {
    this.status_loading = true;
    const contentType = '';
    this.status_loading = false;
    const b64Data = data.base64File;
    const filename = data.fileName;
    console.log('base64', b64Data);

    const config = this.base64(b64Data, contentType);
    saveAs(config, filename);
  }

  base64(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  hideDownloadFile() {
    if (this.dataSource_file.data.length > 0) {
      this.downloadfile_by = [];

      for (let index = 0; index < this.dataSource_file.data.length; index++) {
        const element = this.dataSource_file.data[index];
        console.log('ele', element);

        if ('id' in element) {
          this.downloadfile_by.push('path');
        } else {
          this.downloadfile_by.push('base64');
        }
      }
    }
    console.log('by:', this.downloadfile_by);
  }

  // ===============================================================================
  async saveDraftData(page?, action?) {
    // set pages
    let pages = 'summary';
    if (page) {
      pages = page;
    }
    // set actions
    let actions = 'save';
    if (action) {
      actions = action;
    }
    // ======================================
    console.log('saved');
    this.RequestStepFour.stepFour.fileUpload.file = [];
    this.RequestStepFour.stepFour.fileUpload.fileDelete = [];
    const result1 = await this.save(pages, actions);
    const result2 = await this.result();
    console.log('result2', result2);
    this.saveDraftstatus = result2;
    if (this.saveDraftstatus === 'success') {
      const after = await this.AfterStart();
    }
  }

  save(page?, action?): Promise<any> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        this.RequestStepFour.stepFour.fileUpload.file = this.stepFour.fileUpload.file;
        this.RequestStepFour.stepFour.fileUpload.fileDelete = this.stepFour.fileUpload.fileDelete;

        return resolve(this.RequestStepFour.saveDraft(page, action));
      }, 100);
    });
  }
  result() {
    return new Promise((resolve, reject) => {
      setTimeout(
        () => console.log('result:', resolve(this.RequestStepFour.saveDraftstatus)),
        500
      );
    });
  }
  AfterStart() {
    return new Promise((resolve, reject) => {
      setTimeout(
        () => {
          this.stepFour.fileUpload.file = [];
          this.stepFour.fileUpload.fileDelete = [];
          this.image1 = this.RequestStepFour.image1;

          this.getDetail(
            localStorage.getItem('requestId')
          );
          this.getList();
        }
        ,
        500
      );
    });
  }
  viewNext() {
    this.router.navigate(['request-summary/product-detail/step6']);
  }
  sandwork() {
    this.status_loading = true;
    console.log('save summary base-step-2');
    this.sendWorkService.sendWork(localStorage.getItem('requestId')).subscribe(res_sendwork => {
      // this.sidebarService.sendEvent();
      if (res_sendwork['message'] === 'success') {
        this.router.navigate(['/comment']);
      }
      if (res_sendwork['message'] === 'กรุณากรอกข้อมูลผู้ดำเนินการ') {
        this.status_loading = false;
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
        this.router.navigate(['/save-manager']);
      } else if (res_sendwork['status'] === 'fail' || res_sendwork['status'] === 'error') {
        this.status_loading = false;
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
      } else {
        this.router.navigate(['/comment']);
      }
    });
  }
  show() {
    if (this.showData === true) {
      this.showData = false;
    } else {
      this.showData = true;
    }
  }
  showSearch(index: any) {
    if (this.showsearch[index].value === true) {
      this.showsearch[index].value = false;
    } else {
      this.showsearch[index].value = true;
    }
  }
  getSummarNew(index: any, requestId: any) {
    console.log(index);
    console.log(requestId);
    this.image1 = [];
    this.getDetail(requestId);
    for (let i = 0; i < this.showsearch.length; i++) {
      if (index !== this.showsearch[i].index) {
        this.showsearch[i].value = true;
      }
    }
  }

  // ngDoCheck() {
  //   if (this.openDoCheckc === true) {
  //     this.pageChange = this.sidebarService.changePageGoToLink();
  //     if (this.pageChange) {
  //       const data = this.sidebarService.getsavePage();
  //       let saveInFoStatus = false;
  //       saveInFoStatus = data.saveStatusInfo;
  //       if (saveInFoStatus === true) {
  //         this.outOfPageSave = true;
  //         this.linkTopage = data.goToLink;
  //         this.saveDraftData('navbarSummary', this.linkTopage);
  //         this.sidebarService.resetSaveStatu();
  //       }
  //     }
  //   }
  // }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate request');
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef =  this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        if (data.returnStatus === true) {
          const raturnStatus = this.RequestStepFour.saveDraft();
          console.log('summary raturnStatus', raturnStatus);
            if (raturnStatus) {
              console.log('raturnStatus T', this.RequestStepFour.saveDraftstatus);
              subject.next(true);
            } else {
              console.log('raturnStatus F', this.RequestStepFour.saveDraftstatus);
              subject.next(false);
            }
        } else if (data.returnStatus === false) {
          this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
        }
      });
      return subject;
    } else {
      return true;
    }
  }

}
