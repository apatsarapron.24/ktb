import { map } from 'rxjs/operators';
import { registerLocaleData } from '@angular/common';
import { RequestService } from './../../../../services/request/request.service';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validator } from '@angular/forms';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
  CdkDrag,
  CdkDragExit,
  CdkDragEnter,
} from '@angular/cdk/drag-drop';
import { element } from 'protractor';
import { Router } from '@angular/router';
import { StepNineService } from '../../../../services/request/product-detail/step-nine/step-nine.service';
import { ThrowStmt } from '@angular/compiler';
import { StepNineComponent } from '../../../request/product-detail/step-nine/step-nine.component';
import { StepTenService } from '../../../../services/request-summary/product-detail/step-ten/step-ten.service';
import { SendWorkService } from './../../../../services/request/product-detail/send-work/send-work.service';
import Swal from 'sweetalert2';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogSaveStatusComponent } from '../../../request/dialog/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from '../../../../services/configGuard/config-guard.service';

// mock data
const step9_DATA = [
  {
    tableName: 'ปริมาณธุรกรรม',
    data: [{
      KPI_id: null,
      KPI_dataDraft: true,
      KPI_disabledCol: true,
      KPI_name: '',
      KPI_year: [{
        year: '',
        detail: '',
      },
      {
        year: '',
        detail: '',
      },
      {
        year: '',
        detail: '',
      }],
      cumulative: '',
      now: ''
    }],
  },
  {
    tableName: 'ผลตอบแทน',
    data: [{
      KPI_id: null,
      KPI_dataDraft: true,
      KPI_disabledCol: true,
      KPI_name: '',
      KPI_year: [{
        year: '',
        detail: '',
      },
      {
        year: '',
        detail: '',
      },
      {
        year: '',
        detail: '',
      }],
      cumulative: '',
      now: ''
    }],
  },
  {
    tableName: 'การควบคุม',
    data: [{
      KPI_id: null,
      KPI_dataDraft: true,
      KPI_disabledCol: true,
      KPI_name: '',
      KPI_year: [{
        year: '',
        detail: '',
      },
      {
        year: '',
        detail: '',
      },
      {
        year: '',
        detail: '',
      }],
      cumulative: '',
      now: ''
    }],
  },
];

// มีสอง array 0 -> คือตารางบนสุดที่สามารถกดแก้ไขได้
// ,          1 -> คือตารางล่างเป็นต้นไป (ที่เป็น collepse ทั้งหมด)
// original คือ ข้อมูล เป้าหมายเดิม (เดิม)
// newContent คือ ข้อมูล เป้าหมาย (ใหม่)
// หัวข้อ 'เกิดขึ้นจริง' ถูกเก็บไว้ใน transaction,compensation, control ชื่อ 'cumulative','now'
const step10_DATA = [
  {
    vertion: 'new',
    dateVertion: '',
    reason: null,
    original: {
      year: ['', '', ''],
      transaction: [
        {
          id: null,
          results: null,
          dataDraft: false,
          disabledCol: true,
          title: 'วงเงินโครงการ',
          parentTranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: false,
          disabledCol: true,
          title: 'Limit',
          parentTranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: false,
          disabledCol: true,
          title: 'Outstanding',
          parentTranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: false,
          disabledCol: true,
          title: 'A/R turnover',
          parentTranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: false,
          disabledCol: true,
          title: 'Advance Payment',
          parentTranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: false,
          disabledCol: true,
          title: 'Utilization/Turnover',
          parentTranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: false,
          disabledCol: true,
          title: 'จำนวนลูกค้า',
          parentTranx: ['', '', ''],
          cumulative: '',
          now: ''
        }
      ],
      compensation: [
        {
          id: null,
          results: null,
          dataDraft: false,
          disabledCol: true,
          title: 'NII',
          parentTranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: false,
          disabledCol: true,
          title: 'Non-NII',
          parentTranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: false,
          disabledCol: true,
          title: 'RAROC',
          parentTranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: false,
          disabledCol: true,
          title: 'ROI',
          parentTranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: false,
          disabledCol: true,
          title: 'EP',
          parentTranx: ['', '', ''],
          cumulative: '',
          now: ''
        }
      ],
      control: [
        {
          id: null,
          results: null,
          dataDraft: false,
          disabledCol: true,
          title: 'NPL',
          parentTranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: false,
          disabledCol: true,
          title: 'Loss(Writeoff)',
          parentTranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: false,
          disabledCol: true,
          title: '1st Year Default rate',
          parentTranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: false,
          disabledCol: true,
          title: 'Override of Total port',
          parentTranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
      ]
    },
    newContent: {
      year: ['2222', '3333', '4444'],
      transaction: [
        {
          id: null,
          results: null,
          dataDraft: true,
          disabledCol: false,
          title: 'วงเงินโครงการ',
          tranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: true,
          disabledCol: false,
          title: 'Limit',
          tranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: true,
          disabledCol: false,
          title: 'Outstanding',
          tranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: true,
          disabledCol: false,
          title: 'A/R turnover',
          tranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: true,
          disabledCol: false,
          title: 'Advance Payment',
          tranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: true,
          disabledCol: false,
          title: 'Utilization/Turnover',
          tranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: true,
          disabledCol: false,
          title: 'จำนวนลูกค้า',
          tranx: ['', '', ''],
          cumulative: '',
          now: ''
        }
      ],
      compensation: [
        {
          id: null,
          results: null,
          dataDraft: true,
          disabledCol: false,
          title: 'NII',
          tranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: true,
          disabledCol: false,
          title: 'Non-NII',
          tranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: true,
          disabledCol: false,
          title: 'RAROC',
          tranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: true,
          disabledCol: false,
          title: 'ROI',
          tranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: true,
          disabledCol: false,
          title: 'EP',
          tranx: ['', '', ''],
          cumulative: '',
          now: ''
        }
      ],
      control: [
        {
          id: null,
          results: null,
          dataDraft: true,
          disabledCol: false,
          title: 'NPL',
          tranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: true,
          disabledCol: false,
          title: 'Loss(Writeoff)',
          tranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: true,
          disabledCol: false,
          title: '1st Year Default rate',
          tranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: true,
          disabledCol: false,
          title: 'Override of Total port',
          tranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
      ]
    },
  },
  {
    vertion: 'old',
    dateVertion: 'วันที่ 8 เดือนมกราคม พ.ศ.2562',
    reason: null,
    original: {
      year: ['2222', '3333', '4444'],
      transaction: [
        {
          id: null,
          results: null,
          dataDraft: false,
          disabledCol: true,
          title: 'วงเงินโครงการ',
          parentTranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: false,
          disabledCol: true,
          title: 'Limit',
          parentTranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: false,
          disabledCol: true,
          title: 'Outstanding',
          parentTranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: false,
          disabledCol: true,
          title: 'A/R turnover',
          parentTranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: false,
          disabledCol: true,
          title: 'Advance Payment',
          parentTranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: false,
          disabledCol: true,
          title: 'Utilization/Turnover',
          parentTranx: ['', '', ''],
          cumulative: '',
          now: ''
        },
        {
          id: null,
          results: null,
          dataDraft: false,
          disabledCol: true,
          title: 'จำนวนลูกค้า',
          parentTranx: ['', '', ''],
          cumulative: '',
          now: ''
        }
      ],
      compensation: [{
        id: null,
        results: null,
        dataDraft: true,
        disabledCol: true,
        title: 'วงเงินโครงการ',
        parentTranx: ['qtwert', 'wert', '3qw4e5r'],
        cumulative: '',
        now: ''
      }],
      control: [{
        id: null,
        results: null,
        dataDraft: true,
        disabledCol: true,
        title: 'วงเงินโครงการ',
        parentTranx: ['qtwert', 'wert', '3qw4e5r'],
        cumulative: '',
        now: ''
      }]
    },
    newContent: {
      year: ['2222', '3333', '4444'],
      transaction: [{
        id: null,
        results: null,
        dataDraft: false,
        disabledCol: true,
        title: 'วงเงินโครงการ',
        tranx: ['qtwert', 'wert', '3qw4e5r'],
        cumulative: '',
        now: ''
      }],
      compensation: [{
        id: null,
        results: null,
        dataDraft: false,
        disabledCol: true,
        title: 'วงเงินโครงการ',
        tranx: ['qtwert', 'wert', '3qw4e5r'],
        cumulative: '',
        now: ''
      }],
      control: [{
        id: null,
        results: null,
        dataDraft: false,
        disabledCol: true,
        title: 'วงเงินโครงการ',
        tranx: ['qtwert', 'wert', '3qw4e5r'],
        cumulative: '',
        now: ''
      }]
    },
  }
];




@Component({
  selector: 'app-step-ten-summary',
  templateUrl: './step-ten.component.html',
  styleUrls: ['./step-ten.component.scss']
})
export class StepTenSummaryComponent implements OnInit, CanComponentDeactivate {
  @Input() template_manage = { pageShow: null };
  @Output() saveStatus = new EventEmitter<boolean>();

  validate = false;
  userRole = null;
  editTitleStatus = false;

  saveDraftstatus = null;
  outOfPageSave = false;
  linkTopage = null;
  pageChange = null;
  openDoCheckc = false;
  testfile = {};
  @ViewChild(StepNineComponent) RequestStepNine: StepNineComponent;

  status_loading = true;
  colZiseOriginal = 3;
  colZiseNew = 3;

  // init data
  stepNine = step9_DATA;
  stepTen = step10_DATA;

  rowYear = [];
  popUpShowRow = null;
  popUpShowCol = [{ indextable: null, indexCol: null }];
  attachOutsideOnClick = false;
  enabled = false;
  enabledCount = false;
  clickedOutsideCount: number;
  clickedOutsideDetalCount: number;
  disabledRowAll = false;
  disabledRow = true;
  disabledCol = [];
  // dataDraft = [{
  //   tableindex: null,
  //   dataDteil: null,
  // }];
  requestId: any;
  res: any;
  get_data: any;

  showsearch: any = [];
  showData = false;

  // ข้อมูลที่ใช้ส่งให้ api save
  sendData: any = {
    requestId: '',
    status: false,
    goal: {
      year: [],
      transaction: [],
      compensation: [],
      control: [],
    },
    goalDelete: [],
    reason: '',
    conclusions: null
  };


  check_year = true;
  check_transaction = true;
  check_compensation = true;
  check_control = true;

  listSummaryStep10 = [];

  // create row in table by use these variables here.
  transaction_row = [
    {
      data: []
    }
  ];
  compensation_row = [
    {
      data: []
    }
  ];
  control_row = [
    {
      data: []
    }
  ];

  collapseStateIndex = [];
  parentStepTen = [];

  show() {
    if (this.showData === true) {
      this.showData = false;
    } else {
      this.showData = true;
    }
  }
  showSearch(index: any) {
    if (this.showsearch[index].value === true) {
      this.showsearch[index].value = false;
    } else {
      this.showsearch[index].value = true;
    }
  }

  sendSaveStatus() {
    if (this.saveDraftstatus === 'success') {
      this.saveStatus.emit(true);
    } else {
      this.saveStatus.emit(false);
    }
  }

  viewNext() {
    this.router.navigate(['request-summary/product-detail/step11']);
  }
  next_step() {
    this.router.navigate(['/request-summary/product-detail/step11']);
  }

  check_status() { }

  // ================= disabled ==========================
  checkDisabledYear() {
    console.log('777777777=>', this.stepTen[0].newContent.year);
    this.stepTen[0].newContent.transaction[0].tranx.forEach((ele, index) => {
      this.rowYear[index] = this.rowYear[0] + index;
      this.stepTen[0].newContent.year[index] = (this.rowYear[0] + index).toString();
      // this.rowYear[index] = null
      // this.stepTen[0].newContent.year[index] = null;
    });
    for (let index = 0; index < this.stepTen[0].newContent.year.length; index++) {
      if (this.stepTen[0].newContent.year[0] == '0' ||
        this.stepTen[0].newContent.year[0] == 'NaN') {
        this.rowYear[index] = null
        this.stepTen[0].newContent.year[index] = null
      }
    }

    console.log(this.rowYear)
    console.log('pp', this.stepTen[0].newContent.year);
    // stepTen[0].newContent.year[0]
  }

  checkDisabledRow(indextable, indexCol) {
    console.log('indextable ', indextable, ' indexCol ', indexCol);
    //   console.log('indextable =>', indexCol);
    //   console.log('indexCol =>', indexCol);
    //   let checkValue = 0;
    //   this.stepNine[indextable].data[indexCol].KPI_year.forEach((elements) => {
    //     if (elements.detail) {
    //       checkValue = checkValue + 1;
    //     }
    //   });
    //   if (this.stepNine[indextable].data[indexCol].KPI_name !== null) {
    //     checkValue = checkValue + 1;
    //   }
    //   if (checkValue === 0) {
    //     this.stepNine[indextable].data[indexCol].KPI_disabledCol = true;
    //   } else {
    //     this.stepNine[indextable].data[indexCol].KPI_disabledCol = false;
    //   }

    //   this.changeSaveDraft();
  }

  // ============== row function ==========================
  addYear(index) {
    this.changeSaveDraft()
    console.log('--002 stepTen', this.stepTen);
    this.rowYear.push(null);
    this.stepTen[index].newContent.year.push(null);
    this.stepTen[index].newContent.transaction.forEach(ele => {
      ele.tranx.push('');
    });
    this.stepTen[index].newContent.compensation.forEach(ele => {
      ele.tranx.push('');
    });
    this.stepTen[index].newContent.control.forEach(ele => {
      ele.tranx.push('');
    });
    this.transaction_row.forEach(ele => {
      ele.data.push('');
    });
    this.compensation_row.forEach(ele => {
      ele.data.push('');
    });
    this.control_row.forEach(ele => {
      ele.data.push('');
    });
    console.log('row', this.rowYear);
    console.log('001 addYear', this.stepTen[index].newContent.year);
    console.log('002 stepTen', this.stepTen);
    console.log('003 transaction_row', this.transaction_row);
    console.log('004 compensation_row', this.compensation_row);
    console.log('005 control_row', this.control_row);
  }

  deleteRow(index) {
    this.changeSaveDraft()
    this.rowYear.splice(index, 1);
    this.stepTen[0].newContent.year.splice(index, 1);
    this.stepTen[0].newContent.transaction.forEach(ele => {
      ele.tranx.splice(index, 1);
    });
    this.stepTen[0].newContent.compensation.forEach(ele => {
      ele.tranx.splice(index, 1);
    });
    this.stepTen[0].newContent.control.forEach(ele => {
      ele.tranx.splice(index, 1);
    });
    this.transaction_row.forEach(ele => {
      ele.data.splice(index, 1);
    });
    this.compensation_row.forEach(ele => {
      ele.data.splice(index, 1);
    });
    this.control_row.forEach(ele => {
      ele.data.splice(index, 1);
    });

    // this.stepNine.forEach((elements) => {
    //   elements.data.forEach((sudElements) => {
    //     sudElements.KPI_year.splice(index, 1);
    //   });
    // });
    // this.rowYear.splice(index, 1);
    // this.colZise = this.colZise - 1;
    this.popUpShowRow = null;
    // this.checkDisabledYear();
  }
  deleteRowDetail(index) {
    this.changeSaveDraft()
    this.stepTen[0].newContent.transaction.forEach(ele => {
      ele.tranx[index] = null;
    });
    this.stepTen[0].newContent.compensation.forEach(ele => {
      ele.tranx[index] = null;
    });
    this.stepTen[0].newContent.control.forEach(ele => {
      ele.tranx[index] = null;
    });
    this.transaction_row.forEach(ele => {
      ele.data[index] = null;
    });
    this.compensation_row.forEach(ele => {
      ele.data[index] = null;
    });
    this.control_row.forEach(ele => {
      ele.data[index] = null;
    });
  }

  // yearInput() {

  //   this.stepNine.forEach((elements) => {
  //     elements.data.forEach((sudElements) => {

  //       this.rowYear.forEach((year, index) => {

  //         sudElements.KPI_year[index].year = year;

  //       });

  //     });
  //   });
  // }

  // ============== col function ==========================
  addTable(table) {
    this.changeSaveDraft()
    if (table === 'transaction') {
      console.log('000000addTable00001');
      const tranxOriginal = [];
      const tranxNewContent = [];
      // console.log('original:', this.stepTen[0].original.transaction)
      // console.log('newContent:', this.stepTen[0].newContent.transaction)
      // console.log('year:', this.rowYear)
      if (this.stepTen[0].newContent.transaction.length != 0) {
        this.stepTen[0].newContent.transaction[0].tranx.forEach(ele => {
          tranxNewContent.push(null);
        });

        const dataAddOriginal = {
          id: null,
          results: null,
          dataDraft: false,
          disabledCol: false,
          title: null,
          parentTranx: tranxOriginal,
          cumulative: '',
          now: ''
        };
        this.stepTen[0].original.transaction.push(dataAddOriginal);
        const dataAddNewContent = {
          id: null,
          results: null,
          dataDraft: true,
          disabledCol: false,
          title: null,
          tranx: tranxNewContent,
          cumulative: '',
          now: ''
        };
        this.stepTen[0].newContent.transaction.push(dataAddNewContent);
        const addNew_row = [];
        this.stepTen[0].newContent.transaction[0].tranx.forEach(ele => {
          addNew_row.push(null);
        });
        const dataTransaction_row = {
          data: addNew_row
        };
        this.transaction_row.push(dataTransaction_row);
      }
      else {

        console.log(this.stepTen[0].original.year)
        this.stepTen[0].original.year.forEach(ele => {
          tranxOriginal.push(null)
        })

        const dataAddOriginal = {
          id: null,
          results: null,
          dataDraft: false,
          disabledCol: false,
          title: null,
          parentTranx: tranxOriginal,
          cumulative: '',
          now: ''
        };

        this.stepTen[0].original.transaction.push(dataAddOriginal)
        this.stepTen[0].newContent.transaction.push({
          id: null,
          results: null,
          dataDraft: true,
          disabledCol: false,
          title: null,
          tranx: tranxNewContent,
          cumulative: '',
          now: ''
        })

        console.log(this.stepTen[0].newContent.transaction)
        this.rowYear.forEach(ele => {
          this.stepTen[0].newContent.transaction[0].tranx.push(null)
        });

        const addNew_row = [];
        this.stepTen[0].newContent.transaction[0].tranx.forEach(ele => {
          addNew_row.push(null);
        });
        const dataTransaction_row = {
          data: addNew_row
        };
        this.transaction_row.push(dataTransaction_row);

        console.log(this.stepTen[0].original)
        console.log(this.stepTen[0].newContent)
      }

    } else if (table === 'compensation') {
      const tranxOriginal = [];
      const tranxNewContent = [];
      if (this.stepTen[0].newContent.compensation.length != 0) {
        this.stepTen[0].newContent.compensation[0].tranx.forEach(ele => {
          tranxNewContent.push(null);
        });
        const dataAddOriginal = {
          id: null,
          results: null,
          dataDraft: false,
          disabledCol: false,
          title: null,
          parentTranx: tranxOriginal,
          cumulative: '',
          now: ''
        };
        this.stepTen[0].original.compensation.push(dataAddOriginal);
        const dataAddNewContent = {
          id: null,
          results: null,
          dataDraft: true,
          disabledCol: false,
          title: null,
          tranx: tranxNewContent,
          cumulative: '',
          now: ''
        };
        this.stepTen[0].newContent.compensation.push(dataAddNewContent);
        const addNew_row = [];
        this.stepTen[0].newContent.compensation[0].tranx.forEach(ele => {
          addNew_row.push(null);
        });
        const dataCompensation_row = {
          data: addNew_row
        };
        this.compensation_row.push(dataCompensation_row);

      }
      else {
        console.log(this.stepTen[0].original.year)
        this.stepTen[0].original.year.forEach(ele => {
          tranxOriginal.push(null)
        })

        const dataAddOriginal = {
          id: null,
          results: null,
          dataDraft: false,
          disabledCol: false,
          title: null,
          parentTranx: tranxOriginal,
          cumulative: '',
          now: ''
        };

        this.stepTen[0].original.compensation.push(dataAddOriginal)
        this.stepTen[0].newContent.compensation.push({
          id: null,
          results: null,
          dataDraft: true,
          disabledCol: false,
          title: null,
          tranx: tranxNewContent,
          cumulative: '',
          now: ''
        })

        console.log(this.stepTen[0].newContent.compensation)
        this.rowYear.forEach(ele => {
          this.stepTen[0].newContent.compensation[0].tranx.push(null)
        });

        const addNew_row = [];
        this.stepTen[0].newContent.compensation[0].tranx.forEach(ele => {
          addNew_row.push(null);
        });
        const dataCompensation_row = {
          data: addNew_row
        };
        this.compensation_row.push(dataCompensation_row);

        console.log(this.stepTen[0].original)
        console.log(this.stepTen[0].newContent)
      }

    } else if (table === 'control') {
      const tranxOriginal = [];
      const tranxNewContent: string[] = [];
      // this.stepTen[0].original.control[0].parentTranx.forEach(ele => {
      //   tranxOriginal.push(null);
      // });

      if (this.stepTen[0].newContent.control.length != 0) {
        this.stepTen[0].newContent.control[0].tranx.forEach(ele => {
          tranxNewContent.push(null);
        });
        const dataAddOriginal = {
          id: null,
          results: null,
          dataDraft: false,
          disabledCol: false,
          title: null,
          parentTranx: tranxOriginal,
          cumulative: '',
          now: ''
        };
        this.stepTen[0].original.control.push(dataAddOriginal);
        const dataAddNewContent = {
          id: null,
          results: null,
          dataDraft: true,
          disabledCol: false,
          title: null,
          tranx: tranxNewContent,
          cumulative: '',
          now: ''
        };
        this.stepTen[0].newContent.control.push(dataAddNewContent);
        const addNew_row = [];
        this.stepTen[0].newContent.control[0].tranx.forEach(ele => {
          addNew_row.push(null);
        });
        const dataControl_row = {
          data: addNew_row
        };
        this.control_row.push(dataControl_row);
      }
      else {

        console.log(this.stepTen[0].original.year)
        this.stepTen[0].original.year.forEach(ele => {
          tranxOriginal.push(null)
        })

        const dataAddOriginal = {
          id: null,
          results: null,
          dataDraft: false,
          disabledCol: false,
          title: null,
          parentTranx: tranxOriginal,
          cumulative: '',
          now: ''
        };

        this.stepTen[0].original.control.push(dataAddOriginal)
        this.stepTen[0].newContent.control.push({
          id: null,
          results: null,
          dataDraft: true,
          disabledCol: false,
          title: null,
          tranx: tranxNewContent,
          cumulative: '',
          now: ''
        })

        console.log(this.stepTen[0].newContent.control)
        this.rowYear.forEach(ele => {
          this.stepTen[0].newContent.control[0].tranx.push(null)
        });

        const addNew_row = [];
        this.stepTen[0].newContent.control[0].tranx.forEach(ele => {
          addNew_row.push(null);
        });
        const dataControl_row = {
          data: addNew_row
        };
        this.control_row.push(dataControl_row);

        console.log(this.stepTen[0].original)
        console.log(this.stepTen[0].newContent)

      }

    }

  }

  deleteCol(table, indexCol) {
    this.changeSaveDraft()
    if (table === 'transaction') {
      // check key id
      if ('id' in this.stepTen[0].newContent.transaction[indexCol]) {
        this.sendData.goalDelete.push(this.stepTen[0].newContent.transaction[indexCol].id);
      }
      this.stepTen[0].newContent.transaction.splice(indexCol, 1);
      this.transaction_row.splice(indexCol, 1);
    } else if (table === 'compensation') {
      // check key id
      if ('id' in this.stepTen[0].newContent.compensation[indexCol]) {
        this.sendData.goalDelete.push(this.stepTen[0].newContent.compensation[indexCol].id);
      }
      this.stepTen[0].newContent.compensation.splice(indexCol, 1);
      this.compensation_row.splice(indexCol, 1);
    } else if (table === 'control') {
      if ('id' in this.stepTen[0].newContent.control[indexCol]) {
        this.sendData.goalDelete.push(this.stepTen[0].newContent.control[indexCol].id);
      }
      this.stepTen[0].newContent.control.splice(indexCol, 1);
      this.control_row.splice(indexCol, 1);
    }

    this.popUpShowCol[0].indexCol = null;
    this.popUpShowCol[0].indextable = null;
  }

  deleteColDetail(table, indexCol) {
    this.changeSaveDraft()
    if (table === 'transaction') {
      this.stepTen[0].newContent.transaction[indexCol].title = null;
      this.stepTen[0].newContent.transaction[indexCol].tranx.forEach(ele => {
        ele = null;
      });
      console.log('5555>', this.transaction_row[indexCol]);
      this.transaction_row[indexCol].data.forEach((ele, indax) => {
        this.transaction_row[indexCol].data[indax] = null;
      });
    } else if (table === 'compensation') {
      this.stepTen[0].newContent.compensation[indexCol].title = null;
      this.stepTen[0].newContent.compensation[indexCol].tranx.forEach(ele => {
        ele = null;
      });
      console.log('5555>', this.compensation_row[indexCol]);
      this.compensation_row[indexCol].data.forEach((ele, indax) => {
        this.compensation_row[indexCol].data[indax] = null;
      });
    } else if (table === 'control') {
      this.stepTen[0].newContent.control[indexCol].title = null;
      this.stepTen[0].newContent.control[indexCol].tranx.forEach(ele => {
        ele = null;
      });
      console.log('5555>', this.control_row[indexCol]);
      this.control_row[indexCol].data.forEach((ele, indax) => {
        this.control_row[indexCol].data[indax] = null;
      });
    }

    this.popUpShowCol[0].indexCol = null;
    this.popUpShowCol[0].indextable = null;
  }

  placeholder(text: string) {
    if (text) {
      if (text[0].search(/[^a-zA-Z]+/) === -1) {
        return ' ' + text;
      } else {
        return text;
      }
    } else {
      return '';
    }
  }


  // =========================== Modal popup ==========================
  popUpRow(index) {
    //   console.log('popUpRow', index);
    if (this.popUpShowRow === index) {
      this.popUpShowRow = null;
    } else {
      this.popUpShowRow = index;
    }
    this.enabled = true;
    this.enabledCount = true;

    this.popUpShowCol[0].indextable = null;
    this.popUpShowCol[0].indexCol = null;
    this.clickedOutsideCount = this.stepTen[0].newContent.transaction[0].tranx.length;
  }

  popUpCol(indextable, indexCol) {
    // console.log('popUpCol');
    if (
      this.popUpShowCol[0].indextable === indextable &&
      this.popUpShowCol[0].indexCol === indexCol
    ) {
      this.popUpShowCol[0].indextable = null;
      this.popUpShowCol[0].indexCol = null;
    } else {
      this.popUpShowCol[0].indextable = indextable;
      this.popUpShowCol[0].indexCol = indexCol;
    }
    this.popUpShowRow = null;
    this.clickedOutsideDetalCount = (
      this.stepTen[0].newContent.transaction.length
      + this.stepTen[0].newContent.compensation.length
      + this.stepTen[0].newContent.control.length
    );
  }


  onClickedOutsideYear(indextable) {
    // console.log('indextableYear', indextable);
    // console.log('indextableYear - enabled', this.enabled);
    // console.log('indextableYear - clickedOutsideCount - 2', this.clickedOutsideCount);
    if (this.clickedOutsideCount > 0) {
      this.clickedOutsideCount = this.clickedOutsideCount - 1;
    } else if (this.clickedOutsideCount === 0) {
      this.popUpShowRow = null;
    }
    // console.log(' this.clickedOutsideCount --',  this.clickedOutsideCount);
  }
  onClickedOutsideDetal(indextable, indexCol) {
    // console.log('Detalindextable', indextable);
    // console.log('DetalindexCol', indexCol);
    if (this.clickedOutsideDetalCount > 0) {
      this.clickedOutsideDetalCount = this.clickedOutsideDetalCount - 1;
    } else if (this.clickedOutsideDetalCount === 0) {
      this.popUpShowCol[0].indexCol = null;
      this.popUpShowCol[0].indextable = null;
    }
  }

  // ============================ drop table ==========================
  dropTransaction(event: CdkDragDrop<string[]>) {
    console.log('dropTransaction', event);
    moveItemInArray(
      event.container.data,
      event.previousIndex,
      event.currentIndex
    );
    moveItemInArray(
      this.transaction_row,
      event.previousIndex,
      event.currentIndex
    );
  }

  dropCompensation(event: CdkDragDrop<string[]>) {
    console.log('dropCompensation', event);
    moveItemInArray(
      event.container.data,
      event.previousIndex,
      event.currentIndex
    );
    moveItemInArray(
      this.compensation_row,
      event.previousIndex,
      event.currentIndex
    );
  }

  dropControl(event: CdkDragDrop<string[]>) {
    console.log('dropCompensation', event);
    moveItemInArray(
      event.container.data,
      event.previousIndex,
      event.currentIndex
    );
    moveItemInArray(
      this.control_row,
      event.previousIndex,
      event.currentIndex
    );
  }

  // drop(event: CdkDragDrop<string[]>) {
  //   if (event.previousContainer === event.container) {
  //     moveItemInArray(
  //       event.container.data,
  //       event.previousIndex,
  //       event.currentIndex
  //     );
  //   } else {
  //     transferArrayItem(
  //       event.previousContainer.data,
  //       event.container.data,
  //       event.previousIndex,
  //       event.currentIndex
  //     );
  //   }
  //   this.changeSaveDraft();
  // }

  constructor(private router: Router, private step_Ten: StepTenService,
    private sidebarService: RequestService,
    private sendWorkService: SendWorkService,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.openDoCheckc = true;
    if (localStorage.getItem('requestId')) {
      this.sidebarService.sendEvent();
    } else {
      this.status_loading = false;
    }
    this.requestId = localStorage.getItem('requestId');
    this.userRole = localStorage.getItem('role');
    console.log('userRole summary step2', this.userRole);
    this.step_Ten.getDetail(this.requestId, this.template_manage.pageShow).subscribe((res) => {
      const data = res['validate'];
      this.validate = data;
      console.log('summary validate step10', this.validate);
      console.log('summary step10 res : ', res);
      if (res['data'] !== null) {
        if (res['status'] === 'success') {
          this.status_loading = false;
          this.res = res;
          this.GetData();
          this.status_loading = false;
          if (res['parentRequestId'] !== null) {
            this.listSummaryStep10 = res['parentRequestId'];
          }
          if (this.listSummaryStep10) {
            for (let index = 0; index < this.listSummaryStep10.length; index++) {
              this.showsearch.push({ index: index, value: true });
            }
          }
        }
      } else if (res['data'] === null) {
        this.validate = res['validate'];
        this.transaction_row = [];
        this.compensation_row = [];
        this.control_row = [];
        const  step10_DATA_set = [
          {
            vertion: 'new',
            dateVertion: '',
            reason: null,
            original: {
              year: ['', '', ''],
              transaction: [
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'วงเงินโครงการ',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'Limit',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'Outstanding',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'A/R turnover',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'Advance Payment',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'Utilization/Turnover',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'จำนวนลูกค้า',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                }
              ],
              compensation: [
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'NII',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'Non-NII',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'RAROC',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'ROI',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'EP',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                }
              ],
              control: [
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'NPL',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'Loss(Writeoff)',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: '1st Year Default rate',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'Override of Total port',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
              ]
            },
            newContent: {
              year: [null, null, null],
              transaction: [
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'วงเงินโครงการ',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'Limit',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'Outstanding',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'A/R turnover',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'Advance Payment',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'Utilization/Turnover',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'จำนวนลูกค้า',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                }
              ],
              compensation: [
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'NII',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'Non-NII',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'RAROC',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'ROI',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'EP',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                }
              ],
              control: [
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'NPL',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'Loss(Writeoff)',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: '1st Year Default rate',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'Override of Total port',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
              ]
            },
          },
          {
            vertion: 'old',
            dateVertion: 'วันที่ 8 เดือนมกราคม พ.ศ.2562',
            reason: null,
            original: {
              year: [null, null, null],
              transaction: [
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'วงเงินโครงการ',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'Limit',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'Outstanding',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'A/R turnover',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'Advance Payment',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'Utilization/Turnover',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'จำนวนลูกค้า',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                }
              ],
              compensation: [{
                id: null,
                results: null,
                dataDraft: true,
                disabledCol: true,
                title: 'วงเงินโครงการ',
                parentTranx: ['', '', ''],
                cumulative: '',
                now: ''
              }],
              control: [{
                id: null,
                results: null,
                dataDraft: true,
                disabledCol: true,
                title: 'วงเงินโครงการ',
                parentTranx: ['', '', ''],
                cumulative: '',
                now: ''
              }]
            },
            newContent: {
              year: [null, null, null],
              transaction: [{
                id: null,
                results: null,
                dataDraft: false,
                disabledCol: true,
                title: 'วงเงินโครงการ',
                tranx: ['', '', ''],
                cumulative: '',
                now: ''
              }],
              compensation: [{
                id: null,
                results: null,
                dataDraft: false,
                disabledCol: true,
                title: 'วงเงินโครงการ',
                tranx: ['', '', ''],
                cumulative: '',
                now: ''
              }],
              control: [{
                id: null,
                results: null,
                dataDraft: false,
                disabledCol: true,
                title: 'วงเงินโครงการ',
                tranx: ['', '', ''],
                cumulative: '',
                now: ''
              }]
            },
          }
        ]
        this.stepTen = step10_DATA_set
        const rowKeys = ['transaction', 'compensation', 'control'];
        for (const key of rowKeys) {
          this.stepTen[0].newContent[key].forEach((ele) => {
            const templateData = {
              data: [...ele.tranx]
            };
            if (key === 'transaction') {
              this.transaction_row.push(templateData);
            }
            if (key === 'compensation') {
              this.compensation_row.push(templateData);
            }
            if (key === 'control') {
              this.control_row.push(templateData);
            }

          });
        }
      }
      if (this.template_manage.pageShow === 'executive') {
        this.validate = false;
      }
    });
    if (this.template_manage.pageShow === 'executive') {
      this.validate = false;
    }
    // console.log('002 this.rowYear', this.rowYear);
    console.log('stepTen : ', this.stepTen);
    console.log('stepTen Year length : ', this.stepTen[0].original.year.length);
    console.log('validate summary step10', this.validate);
  }

  GetData() {
    this.step_Ten.getDetail(this.requestId, this.template_manage.pageShow).subscribe((res) => {
      this.get_data = res['data']; // รับค่าจาก api
      console.log('get_data => ', this.get_data);
      this.sendData.requestId = Number(this.get_data.requestId);
      // ยังไม่ได้ดึง API มาใช้
      // set data from api
      // console.log('copy year : ', [...this.get_data.goal.parentYear]);
      const tableKeys = ['original', 'newContent'];
      const rowKeys = ['transaction', 'compensation', 'control'];
      // วนสร้าง array ใหม่ที่ map กับข้อมูลที่กำลังใช้อยู่
      for (const tableKey of tableKeys) {
        if (tableKey === 'original') {
          console.log('stepTen Year length0001 : ', this.stepTen[0].original.year.length);
          console.log('stepTen Year length001-- : ', ...this.get_data);
          this.stepTen[0][tableKey].year = [...this.get_data.goal.parentYear];
          console.log('stepTen Year length001 : ', this.stepTen[0].original.year.length);
        }
        if (tableKey === 'newContent') {
          this.stepTen[0][tableKey].year = [...this.get_data.goal.year];
        }
        for (const rowKey of rowKeys) {
          this.stepTen[0][tableKey][rowKey] = [...this.get_data.goal[rowKey]];
          this.stepTen[0][tableKey][rowKey].forEach(tranItem => {
            tranItem['dataDraft'] = false;
          });
          this.stepTen[0].original[rowKey].forEach(tranItem => {
            if (tableKey === 'newContent') {
              tranItem['disabledCol'] = false;
            } else {
              tranItem['disabledCol'] = true;
            }
          });
        }
      }

      this.transaction_row = [];
      this.compensation_row = [];
      this.control_row = [];
      // set data to newContent Table (input ngModule)
      for (const key of rowKeys) {
        console.log('key::', this.stepTen[0])
        if (this.stepTen[0].newContent[key].length != 0) {
          this.stepTen[0].newContent[key].forEach((ele) => {
            const templateData = {
              data: [...ele.tranx]
            };
            console.log('0000007>>', templateData);
            if (key === 'transaction') {
              this.transaction_row.push(templateData);
            }
            if (key === 'compensation') {
              this.compensation_row.push(templateData);
            }
            if (key === 'control') {
              this.control_row.push(templateData);
            }

          });
        }

      }

      // set yaer
      this.stepTen[0].newContent.year.forEach(ele => {
        this.rowYear.push(Number(ele));
      });
      // set reason
      this.stepTen[0].reason = this.get_data.reason;

      // กรณีตารางล่างสร้างตัวแปรเก็บสถานะของ collepse ไว้
      // ex. collapseStateIndex = [false,false,false] ก็คือทุกตัว init ด้วย false อยู่
      console.log('this.res : ', this.res);
      if (this.res['parentRequestId'] !== null) {
        this.collapseStateIndex = this.res['parentRequestId'].map(m => false);
      }

      console.log(this.stepTen[0].newContent.year.length);
      console.log('transaction_row : ', this.transaction_row);
      console.log('stepTen before update : ', this.stepTen[0]);

      this.stepTen[0].original.transaction.forEach((ele, index) => {
        console.log('001', index);
        console.log('002', this.stepTen[0].original.transaction[index].title);
      });

      console.log('stepTen Year length2 : ', this.stepTen[0].original.year.length);
      // document.getElementById('chckDisableDelete').click()
    });
  }
  chckDisableDelete(data) {
    let disable = false;
    // console.log('Disable:',data.parentTranx)
    if (data.parentTranx !== undefined) {
      for (let p = 0; p < data.parentTranx.length; p++) {
        if (data.parentTranx[p] != '   ') {
          disable = true
        }
        else {
          disable = false
        }
      }
    } else {
      return disable
    }

    return disable
  }
  createTableParent(requestId: any, stateCollapse: boolean, index: number) {
    console.log('index : ', index);
    console.log('stateCollapse : ', stateCollapse);

    if (stateCollapse) {
      // if click collepse mean we need to close
      this.collapseStateIndex[index] = false;
      return;
    }
    this.collapseStateIndex[index] = true;
    // call api
    this.step_Ten.getDetail(requestId).subscribe(res => {
      console.log('res parent:', res);
      if (res['status'] === 'success') {
        this.parentStepTen[index] = res;
        console.log('parentStepTen :', this.parentStepTen);
      } else {
        console.log('not in success case');
      }
    }, err => {
      console.log(err);
    });

    for (let i = 0; i < this.showsearch.length; i++) {
      if (index !== this.showsearch[i].index) {
        this.showsearch[i].value = true;
      }
    }

  }

  changeTransaction(table, index) {
    this.stepTen[0].newContent.transaction[table].tranx[index] = this.transaction_row[table].data[index];
  }
  changeCompensation(table, index) {
    this.stepTen[0].newContent.compensation[table].tranx[index] = this.compensation_row[table].data[index];
  }
  changeControl(table, index) {
    this.stepTen[0].newContent.control[table].tranx[index] = this.control_row[table].data[index];
  }

  changeSaveDraft() {
    this.saveDraftstatus = null;
    this.sendSaveStatus();
    this.sidebarService.inPageStatus(true);
  }

  saveDraftData(page?, action?) {
    console.log('data::', this.stepTen)

    this.status_loading = true;
    if (this.stepTen[0].newContent.transaction.length != 0) {
      this.stepTen[0].newContent.transaction[0].tranx.forEach((ele, index) => {
        this.rowYear[index] = this.rowYear[0] + index;
        this.stepTen[0].newContent.year[index] = (this.rowYear[0] + index).toString();
      });
    }

    // console.log('stepTen-saveDraftData-stepTen', this.stepTen);
    // console.log('stepTen-saveDraftData-transaction_row', this.transaction_row);
    // console.log('stepTen-saveDraftData-compensation_row', this.compensation_row);
    // console.log('stepTen-saveDraftData-control_row', this.control_row);

    // set data before send
    this.sendData.requestId = this.requestId;
    this.sendData.status = true;
    this.sendData.goal.year = this.stepTen[0].newContent.year;
    this.sendData.goal.transaction = [...this.stepTen[0].newContent.transaction];
    this.sendData.goal.compensation = [...this.stepTen[0].newContent.compensation];
    this.sendData.goal.control = [...this.stepTen[0].newContent.control];
    this.sendData.reason = this.stepTen[0].reason;
    console.log('sendData::', this.sendData.goal)
    console.log('transaction_row::', this.transaction_row)
    console.log('rowYear::', this.sendData.goal.year)

    if (this.transaction_row.length != 0) {
      this.sendData.goal.transaction.forEach((item, index) => {
        item.tranx = this.transaction_row[index].data;
      });
    }
    if (this.compensation_row.length != 0) {
      this.sendData.goal.compensation.forEach((item, index) => {
        item.tranx = this.compensation_row[index].data;
      });
    }
    if (this.control_row.length != 0) {
      this.sendData.goal.control.forEach((item, index) => {
        item.tranx = this.control_row[index].data;
      });

    }

    if (this.template_manage.pageShow === 'summary') {
      this.sendData.conclusions = true;
    } else {
      this.sendData.conclusions = null;
    }

    if (this.rowYear.length != 0) {
    
      if (this.rowYear[0] == 0 || String(this.rowYear[0]) == 'NaN') {
        // console.log('test', this.rowYear[0])
        for (let k = 0; k < this.sendData.goal.year.length; k++) {
          this.sendData.goal.year[k] = null
          // console.log(this.sendData.goal.year[k])
        }
      }
    }
    // console.log(this.rowYear.length)
    // console.log(this.sendData.goal.year)
    console.log('summary step10 sendData ', this.sendData);
    //     compensation: []
    // control: [{…}]
    // transaction:
    // console.log(this.sendData.goal)
    let check = this.checkNewGoal(this.sendData.goal)
    // if (this.sendData.goal.compensation.length == 0 ||
    //   this.sendData.goal.transaction.length == 0) {
    //   this.sendData.status = false
    // }
    if (check == false) {
      this.sendData.status = false
    }

    const returnUpdateData = new Subject<any>();
    this.step_Ten.updateDetail(this.sendData).subscribe(res => {
      console.log('res : ', res);
      if (res['status'] === 'success') {
        this.saveDraftstatus = 'success';
        returnUpdateData.next(true);
        this.sidebarService.inPageStatus(false);
        setTimeout(() => {
          this.saveDraftstatus = 'fail';
        }, 3000);
        this.status_loading = false;
        this.saveDraftstatus = 'success';
        this.sendData = {
          requestId: '',
          status: false,
          goal: {
            year: [],
            transaction: [],
            compensation: [],
            control: [],
          },
          goalDelete: [],
          reason: '',
          conclusions: null
        };
        this.rowYear = []
        const  step10_DATA_set = [
          {
            vertion: 'new',
            dateVertion: '',
            reason: null,
            original: {
              year: ['', '', ''],
              transaction: [
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'วงเงินโครงการ',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'Limit',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'Outstanding',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'A/R turnover',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'Advance Payment',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'Utilization/Turnover',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'จำนวนลูกค้า',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                }
              ],
              compensation: [
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'NII',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'Non-NII',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'RAROC',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'ROI',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'EP',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                }
              ],
              control: [
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'NPL',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'Loss(Writeoff)',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: '1st Year Default rate',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'Override of Total port',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
              ]
            },
            newContent: {
              year: [null, null, null],
              transaction: [
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'วงเงินโครงการ',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'Limit',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'Outstanding',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'A/R turnover',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'Advance Payment',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'Utilization/Turnover',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'จำนวนลูกค้า',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                }
              ],
              compensation: [
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'NII',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'Non-NII',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'RAROC',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'ROI',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'EP',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                }
              ],
              control: [
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'NPL',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'Loss(Writeoff)',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: '1st Year Default rate',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: true,
                  disabledCol: false,
                  title: 'Override of Total port',
                  tranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
              ]
            },
          },
          {
            vertion: 'old',
            dateVertion: 'วันที่ 8 เดือนมกราคม พ.ศ.2562',
            reason: null,
            original: {
              year: [null, null, null],
              transaction: [
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'วงเงินโครงการ',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'Limit',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'Outstanding',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'A/R turnover',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'Advance Payment',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'Utilization/Turnover',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                },
                {
                  id: null,
                  results: null,
                  dataDraft: false,
                  disabledCol: true,
                  title: 'จำนวนลูกค้า',
                  parentTranx: ['', '', ''],
                  cumulative: '',
                  now: ''
                }
              ],
              compensation: [{
                id: null,
                results: null,
                dataDraft: true,
                disabledCol: true,
                title: 'วงเงินโครงการ',
                parentTranx: ['', '', ''],
                cumulative: '',
                now: ''
              }],
              control: [{
                id: null,
                results: null,
                dataDraft: true,
                disabledCol: true,
                title: 'วงเงินโครงการ',
                parentTranx: ['', '', ''],
                cumulative: '',
                now: ''
              }]
            },
            newContent: {
              year: [null, null, null],
              transaction: [{
                id: null,
                results: null,
                dataDraft: false,
                disabledCol: true,
                title: 'วงเงินโครงการ',
                tranx: ['', '', ''],
                cumulative: '',
                now: ''
              }],
              compensation: [{
                id: null,
                results: null,
                dataDraft: false,
                disabledCol: true,
                title: 'วงเงินโครงการ',
                tranx: ['', '', ''],
                cumulative: '',
                now: ''
              }],
              control: [{
                id: null,
                results: null,
                dataDraft: false,
                disabledCol: true,
                title: 'วงเงินโครงการ',
                tranx: ['', '', ''],
                cumulative: '',
                now: ''
              }]
            },
          }
        ]
        this.stepTen = step10_DATA_set
        this.GetData();
        this.sidebarService.sendEvent();
        this.sendSaveStatus();
      } else {
        alert(res['message']);
        returnUpdateData.next(false);
      }
    }, err => {
      console.log(err);
      this.status_loading = false;
      alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
      returnUpdateData.next(false);
    });
    return returnUpdateData;
  }
  checkNewGoal(data) {
    let transaction = true
    let compensation = true
    let control = true
    // ==========================transaction========================================//
    for (let index = 0; index < data.transaction.length; index++) {
      if (data.transaction[index].title == null ||
        data.transaction[index].title == '' ||
        data.transaction[index].cumulative == null ||
        data.transaction[index].cumulative == '' ||
        data.transaction[index].now == null ||
        data.transaction[index].now == '') {
        transaction = false
        break;
      }
      else {
        for (let i = 0; i < data.transaction[index].tranx.length; i++) {
          if (data.transaction[index].tranx[i] == null ||
            data.transaction[index].tranx[i] == '') {
            transaction = false
            break;
          }
        }
      }
    }
    // =========================compensation========================================//
    for (let index = 0; index < data.compensation.length; index++) {
      if (data.compensation[index].title == null ||
        data.compensation[index].title == '' ||
        data.compensation[index].cumulative == null ||
        data.compensation[index].cumulative == '' ||
        data.compensation[index].now == null ||
        data.compensation[index].now == '') {
        compensation = false
        break;
      }
      else {
        for (let i = 0; i < data.compensation[index].tranx.length; i++) {
          if (data.compensation[index].tranx[i] == null ||
            data.compensation[index].tranx[i] == '') {
            compensation = false
            break;
          }
        }
      }
    }
    // ==============================control========================================//
    for (let index = 0; index < data.control.length; index++) {
      if (data.control[index].title == null ||
        data.control[index].title == '' ||
        data.control[index].cumulative == null ||
        data.control[index].cumulative == '' ||
        data.control[index].now == null ||
        data.control[index].now == ''
      ) {
        control = false
        break;
      }
      else {
        for (let i = 0; i < data.control[index].tranx.length; i++) {
          if (data.control[index].tranx[i] == null ||
            data.control[index].tranx[i] == '') {
            control = false
            break;
          }
        }
      }
    }
    //==============================================================================// 
    if (transaction == false ||
      compensation == false ||
      control == false) {
      return false
    }
    else {
      return true
    }
  }
  sandwork() {
    this.status_loading = true;
    console.log('save summary base-step-2');
    this.sendWorkService.sendWork(localStorage.getItem('requestId')).subscribe(res_sendwork => {
      // this.sidebarService.sendEvent();
      if (res_sendwork['message'] === 'success') {
        this.status_loading = false;
        this.router.navigate(['/comment']);
      }
      if (res_sendwork['message'] === 'กรุณากรอกข้อมูลผู้ดำเนินการ') {
        this.status_loading = false;
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
        this.router.navigate(['/save-manager']);
      } else if (res_sendwork['status'] === 'fail' || res_sendwork['status'] === 'error') {
        this.status_loading = false;
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
      } else {
        this.router.navigate(['/comment']);
      }
    });
  }

  // ใช้เมื่อต้องการไม่ให้ input lost focus เวลาพิมพ์ตอน fetch api ทับข้อมูลเก่า
  trackByFn(index: any, item: any) {
    return index;
  }

  //  ngOnDestroy(): void {
  //   const data = this.sidebarService.getsavePage();
  //   let saveInFoStatus = false;
  //   saveInFoStatus = data.saveStatusInfo;
  //   if (saveInFoStatus === true) {
  //     this.outOfPageSave = true;
  //     this.linkTopage = data.goToLink;
  //     console.log('ngOnDestroy', this.linkTopage);
  //     this.saveDraftData();
  //     this.sidebarService.resetSaveStatusInfo();
  //   }
  // }

  // ngDoCheck() {
  //   if (this.openDoCheckc === true) {

  //     this.pageChange = this.sidebarService.changePageGoToLink();
  //     if (this.pageChange) {
  //       // this.getNavbar();
  //       const data = this.sidebarService.getsavePage();
  //       let saveInFoStatus = false;
  //       saveInFoStatus = data.saveStatusInfo;
  //       if (saveInFoStatus === true) {
  //         this.outOfPageSave = true;
  //         this.linkTopage = data.goToLink;
  //         this.saveDraftData('navbarSummary', this.linkTopage);
  //         this.sidebarService.resetSaveStatu();
  //       }
  //     }
  //   }
  // }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate request');
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef = this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        if (data.returnStatus === true) {
          const raturnStatus = this.saveDraftData();
          console.log('summary raturnStatus', raturnStatus);
          if (raturnStatus) {
            console.log('raturnStatus T', this.saveDraftstatus);
            subject.next(true);
          } else {
            console.log('raturnStatus F', this.saveDraftstatus);
            subject.next(false);
          }
        } else if (data.returnStatus === false) {
          this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
        }
      });
      return subject;
    } else {
      return true;
    }
  }

}
