import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepSixSummaryComponent } from './step-six.component';

describe('StepSixSummaryComponent', () => {
  let component: StepSixSummaryComponent;
  let fixture: ComponentFixture<StepSixSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepSixSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepSixSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
