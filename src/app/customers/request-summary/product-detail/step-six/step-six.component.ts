import {
  Component,
  OnInit,
  ViewChild,
  Output,
  EventEmitter,
  DoCheck
} from '@angular/core';
import { StepFiveComponent } from '../../../request/product-detail/step-five/step-five.component';
import { MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { StepFiveService } from '../../../../services/request/product-detail/step-five/step-five.service';
import { RequestService } from '../../../../services/request/request.service';
import * as moment from 'moment';
import { IAngularMyDpOptions } from 'angular-mydatepicker';
import { saveAs } from 'file-saver';
import { SendWorkService } from './../../../../services/request/product-detail/send-work/send-work.service';
import Swal from 'sweetalert2';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogSaveStatusComponent } from '../../../request/dialog/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from '../../../../services/configGuard/config-guard.service';


@Component({
  selector: 'app-step-six-summary',
  templateUrl: './step-six.component.html',
  styleUrls: ['./step-six.component.scss'],
})
export class StepSixSummaryComponent implements OnInit, CanComponentDeactivate {
  saveDraftstatus = null;
  testfile = {};
  @ViewChild(StepFiveComponent) RequestStepFive: StepFiveComponent;
  @Output() saveStatus = new EventEmitter<boolean>();

  stepFive = {
    requestId: null,
    status: false,
    operations: [
      {
        process: 'การพัฒนาระบบ',
        detail: [
          {
            subProcess: '',
            management: '',
            responsible: '',
            dueDate: {},
          },
        ],
        detailDelete: [],
      },
      {
        process: 'ความพร้อมของอุปกรณ์/กระบวนการ (End to End workflow)',
        detail: [
          {
            subProcess: '',
            management: '',
            responsible: '',
            dueDate: {},
          },
        ],
        detailDelete: [],
      },
      {
        id: '',
        process: 'ความพร้อมของระบบงาน IT',
        detail: [
          {
            subProcess: '',
            management: '',
            responsible: '',
            dueDate: {},
          },
        ],
        detailDelete: [],
      },
      {
        id: '',
        process: 'ความพร้อมของ ระเบียบ /คู่มือ /การฝึกอบรม /รายงาน',
        detail: [
          {
            subProcess: '',
            management: '',
            responsible: '',
            dueDate: {},
          },
        ],
        detailDelete: [],
      },
      {
        id: '',
        process: 'การจัดซื้อจัดจ้าง (ถ้ามี)',
        detail: [
          {
            subProcess: '',
            management: '',
            responsible: '',
            dueDate: {},
          },
        ],
        detailDelete: [],
      },
    ],
    operationsDelete: [],
    documentFile: [],
    attachments: {
      file: [],
      fileDelete: [],
    },
  };

  addData = {
    operations: [],
  };
  // dropIndex: number;
  status_loading = true;

  file1: any;
  file2: any;
  file3: any;
  document: any;
  Agreement = null;
  file_name: any;
  file_size: number;

  imgURL: any;
  image1 = [];
  image2 = [];
  image3 = [];
  showImage1 = false;
  message = null;

  outOfPageSave = false;
  linkTopage = null;
  pageChange = null;
  openDoCheckc = false;

  // Model
  showModel = false;
  modalmainImageIndex: number;
  modalmainImagedetailIndex: number;
  indexSelect: number;
  modalImage = [];
  modalImagedetail = [];
  fileValue: number;
  // uploadfile
  displayedColumns_file: string[] = ['name', 'size', 'delete'];
  dataSource_file: MatTableDataSource<FileList>;
  urls: any = [];

  displayedColumns_file_summary: string[] = ['name', 'size', 'download'];
  dataSource_file_summary: MatTableDataSource<FileList>;

  // new table
  dropIndex: number;
  add_Process: null;
  staticTitleIndex = [0, 1, 2, 3, 4]; // !important ex [0,1,2] mean set fix row index 0 1 2

  // status page
  stepFiveStatus = true;
  date: any = [];

  listSummaryStep6: any = [];
  showsearch: any = [];
  showData = false;

  validate = false;
  userRole = null;
  downloadfile_by = [];

  constructor(
    private router: Router,
    private stepFiveService: StepFiveService,
    private sidebarService: RequestService,
    private sendWorkService: SendWorkService,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.openDoCheckc = true;
    this.sidebarService.sendEvent();
    if (localStorage) {
      this.stepFiveService
        .getProduct_detail_step_five(localStorage.getItem('requestId'))
        .subscribe((ress) => {
          console.log('resssssss;', ress);
          if (ress['data'] !== null) {
            this.dataSource_file = new MatTableDataSource(ress['data'].attachments.file);
            this.hideDownloadFile();
            const data = ress['data'];
            this.validate = data.validate;
            console.log('validate summary step6', this.validate);
          } else {
            this.dataSource_file = new MatTableDataSource([]);
            this.validate = ress['validate'];
          }

          this.userRole = localStorage.getItem('role');
          console.log('userRole summary step6', this.userRole);
        });
      this.getData(localStorage.getItem('requestId'));
      this.getList();
    } else {
      this.status_loading = false;
    }
  }
  getList() {
    this.stepFiveService
      .getProduct_detail_step_five(localStorage.getItem('requestId'))
      .subscribe((res) => {
        if (res['parentRequestId'] !== null) {
          this.listSummaryStep6 = res['parentRequestId'];
          for (let index = 0; index < this.listSummaryStep6.length; index++) {
            this.showsearch.push({ index: index, value: true });
          }
        }
      });
  }
  sendSaveStatus() {
    if (this.saveDraftstatus === 'success') {
      this.saveStatus.emit(true);
    } else {
      this.saveStatus.emit(false);
    }
  }

  getData(requestId) {
    // this.dataSource_file = new MatTableDataSource(File_data);
    this.stepFiveService
      .getProduct_detail_step_five(requestId)
      .subscribe((res) => {
        if (res['status'] === 'success') {
          this.status_loading = false;
        }
        if (res['data'] !== null) {
          this.stepFive = res['data'];

          // set detailDelete Object stepFive
          this.stepFive.operations.forEach((e, index) => {
            this.stepFive.operations[index].detailDelete = [];
          });
          // set operationsDeleteto Object stepFive
          this.stepFive.operationsDelete = [];
          this.stepFive.attachments.fileDelete = [];
          // set dateFormat
          this.stepFive.operations.forEach((elements, operationsIndex) => {
            elements.detail.forEach((e, detailIndex) => {
              const dateIso = this.stepFive.operations[operationsIndex].detail[
                detailIndex
              ]['dueDate'];
              if (dateIso === null) {
                this.stepFive.operations[operationsIndex].detail[
                  detailIndex
                ].dueDate = null;
              } else {
                const dateStamp = {
                  isRange: false,
                  singleDate: { jsDate: new Date(dateIso.toString()) },
                };
                this.stepFive.operations[operationsIndex].detail[
                  detailIndex
                ].dueDate = dateStamp;
                const dateRead = moment(dateStamp.singleDate.jsDate).format('DD/MM') + '/' +
                  Number(Number(moment(dateStamp.singleDate.jsDate).format('YYYY')) + 543);
                this.date.push(dateRead);
              }
            });
          });

          // set uploadfile
          // this.dataSource_file = new MatTableDataSource(
          //   this.RequestStepFive.stepFive.attachments.file
          // );
          this.dataSource_file_summary = new MatTableDataSource(
            this.stepFive.attachments.file
          );
        }
      });
  }

  next_step() {
    this.router.navigate(['/request-summary/product-detail/step7']);
  }

  changeSaveDraft() {
    this.saveDraftstatus = null;
    this.sendSaveStatus();
    this.sidebarService.inPageStatus(true);
  }

  // new table ================================================================================
  checkStaticTitle(index) {
    return this.staticTitleIndex.includes(index);
  }

  // Uplode File ================================================================
  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      const file = event.target.files;
      for (let i = 0; i < filesAmount; i++) {
        console.log('type:', file[i]);
        if (
          // file[i].type === 'image/svg+xml' ||
          file[i].type === 'image/jpeg' ||
          // file[i].type === 'image/raw' ||
          // file[i].type === 'image/svg' ||
          // file[i].type === 'image/tif' ||
          // file[i].type === 'image/gif' ||
          file[i].type === 'image/jpg' ||
          file[i].type === 'image/png' ||
          file[i].type === 'application/doc' ||
          file[i].type === 'application/pdf' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
          file[i].type === 'application/pptx' ||
          file[i].type === 'application/docx' ||
          (file[i].type === 'application/ppt' && file[i].size)
        ) {
          if (file[i].size <= 5000000) {
            this.multiple_file(file[i], i);
          }
        }
        else {
          this.status_loading = false;
          alert('File Not Support');
        }
      }
    }
  }
  viewNext() {
    this.router.navigate(['request-summary/product-detail/step7']);
  }
  multiple_file(file, index) {
    console.log('download');
    const reader = new FileReader();
    if (file) {
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');

        const templateFile = {
          fileName: file.name,
          fileSize: file.size,
          base64File: splitFile[1],
        };
        this.testfile = templateFile;

        this.stepFive.attachments.file.push(templateFile);
        this.RequestStepFive.stepFive.attachments.file.push(templateFile);
        this.dataSource_file = new MatTableDataSource(
          this.stepFive.attachments.file
        );
        this.hideDownloadFile();
      };
    }
    this.changeSaveDraft();
  }

  delete_mutifile(data: any, index: any) {
    console.log('data:', data, 'index::', index);
    if ('id' in this.stepFive.attachments.file[index]) {
      const id = this.stepFive.attachments.file[index].id;
      console.log('found id');
      if (id !== '' || id !== null) {
        this.stepFive.attachments.fileDelete.push(id);
        this.RequestStepFive.stepFive.attachments.fileDelete.push(id);
      }
    }
    this.stepFive.attachments.file.splice(index, 1);
    this.RequestStepFive.stepFive.attachments.file.splice(index, 1);
    this.dataSource_file = new MatTableDataSource(
      this.stepFive.attachments.file
    );

    console.log('dddddddddddddddddd1', this.stepFive.attachments);
    console.log('dddddddddddddddddd2', this.RequestStepFive.stepFive.attachments);
    this.changeSaveDraft();
  }

  meageData() {
    const mergeData = {
      process: [],
    };
    mergeData.process = [].concat(
      this.stepFive.operations,
      this.addData.operations
    );
    console.log(mergeData);
  }

  downloadFile(pathdata: any) {
    this.status_loading = true;
    const contentType = '';
    const sendpath = pathdata;
    console.log('path', sendpath);
    this.sidebarService.getfile(sendpath).subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        this.status_loading = false;
        const datagetfile = res['data'];
        const b64Data = datagetfile['data'];
        const filename = datagetfile['fileName'];
        console.log('base64', b64Data);
        const config = this.b64toBlob(b64Data, contentType);
        saveAs(config, filename);
      }

    });
  }

  b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const convertbyte = atob(b64Data);
    const byteCharacters = atob(convertbyte);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  downloadFile64(data: any) {
    this.status_loading = true;
    const contentType = '';
    this.status_loading = false;
    const b64Data = data.base64File;
    const filename = data.fileName;
    console.log('base64', b64Data);

    const config = this.base64(b64Data, contentType);
    saveAs(config, filename);
  }

  base64(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  hideDownloadFile() {
    if (this.dataSource_file.data.length > 0) {
      this.downloadfile_by = [];

      for (let index = 0; index < this.dataSource_file.data.length; index++) {
        const element = this.dataSource_file.data[index];
        console.log('ele', element);

        if ('id' in element) {
          this.downloadfile_by.push('path');
        } else {
          this.downloadfile_by.push('base64');
        }
      }
    }
    console.log('by:', this.downloadfile_by);
  }

  async saveDraftData(page?, action?) {
    // set pages
    let pages = 'summary';
    if (page) {
      pages = page;
    }
    // set actions
    let actions = 'save';
    if (action) {
      actions = action;
    }
    console.log('saved');
    const result1 = await this.save(pages, actions);
    const result2 = await this.result();
    console.log('result2', result2);
    this.saveDraftstatus = result2;
    if (this.saveDraftstatus === 'success') {
      const after = await this.AfterStart();
    }
  }

  save(page?, action?): Promise<any> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        this.RequestStepFive.stepFive.attachments.file = this.stepFive.attachments.file;
        this.RequestStepFive.stepFive.attachments.fileDelete = this.stepFive.attachments.fileDelete;

        return resolve(this.RequestStepFive.saveDraft(page, action));
      }, 100);
    });
  }
  result() {
    return new Promise((resolve, reject) => {
      setTimeout(
        () =>
          console.log('resuil:', resolve(this.RequestStepFive.saveDraftstatus)),
        500
      );
    });
  }
  AfterStart() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        this.date = [];
        this.getData(localStorage.getItem('requestId'));
        this.saveDraftstatus = 'success';
        setTimeout(() => {
          this.saveDraftstatus = 'fail';
        }, 3000);
        this.getList();
      }, 500);
    });
  }
  sandwork() {
    this.status_loading = true;
    console.log('save summary base-step-2');
    this.sendWorkService.sendWork(localStorage.getItem('requestId')).subscribe(res_sendwork => {
      // this.sidebarService.sendEvent();
      if (res_sendwork['message'] === 'success') {
        this.status_loading = false;
        this.router.navigate(['/comment']);
      }
      if (res_sendwork['message'] === 'กรุณากรอกข้อมูลผู้ดำเนินการ') {
        this.status_loading = false;
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
        this.router.navigate(['/save-manager']);
      } else if (res_sendwork['status'] === 'fail' || res_sendwork['status'] === 'error') {
        this.status_loading = false;
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
      } else {
        this.router.navigate(['/comment']);
      }
    });
  }
  show() {
    if (this.showData === true) {
      this.showData = false;
    } else {
      this.showData = true;
    }
  }
  showSearch(index: any) {
    if (this.showsearch[index].value === true) {
      this.showsearch[index].value = false;
    } else {
      this.showsearch[index].value = true;
    }
  }
  getSummarNew(index: any, requestId: any) {
    console.log(index);
    console.log(requestId);
    this.getData(requestId);
    for (let i = 0; i < this.showsearch.length; i++) {
      if (index !== this.showsearch[i].index) {
        this.showsearch[i].value = true;
      }
    }
  }


  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate request');
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef = this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        if (data.returnStatus === true) {
          const raturnStatus = this.RequestStepFive.saveDraft();
          console.log('summary raturnStatus', raturnStatus);
          if (raturnStatus) {
            console.log('raturnStatus T', this.RequestStepFive.saveDraftstatus);
            subject.next(true);
          } else {
            console.log('raturnStatus F', this.RequestStepFive.saveDraftstatus);
            subject.next(false);
          }
        } else if (data.returnStatus === false) {
          this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
        }
      });
      return subject;
    } else {
      return true;
    }
  }

}
