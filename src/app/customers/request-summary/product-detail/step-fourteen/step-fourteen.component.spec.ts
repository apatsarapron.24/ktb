import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepFourtSummaryeenComponent } from './step-fourteen.component';

describe('StepFourteenComponent', () => {
  let component: StepFourtSummaryeenComponent;
  let fixture: ComponentFixture<StepFourtSummaryeenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepFourtSummaryeenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepFourtSummaryeenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
