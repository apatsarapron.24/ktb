import { forEach } from '@angular/router/src/utils/collection';
import { finalize } from 'rxjs/operators';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { StepFourteenService } from '../../../../services/request-summary/product-detail/step-fourteen/step-fourteen.service'
import { SendWorkService } from './../../../../services/request/product-detail/send-work/send-work.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-step-fourteen-summary',
  templateUrl: './step-fourteen.component.html',
  styleUrls: ['./step-fourteen.component.scss']
})
export class StepFourtSummaryeenComponent implements OnInit {
  @Input() template_manage = { pageShow: null };
  @Output() saveStatus = new EventEmitter<boolean>();

  saveDraftstatus = null;
  stepFourteen = [
    {
      id: null,
      topic: null,
      oldDetail: null,
      newDetail: null,
      presentDate: null
    },
    {
      id: null,
      topic: null,
      oldDetail: null,
      newDetail: null,
      presentDate: null
    },
    {
      id: null,
      topic: null,
      oldDetail: null,
      newDetail: null,
      presentDate: null
    }
  ];

  validate = false;
  userRole = null;
  status_loading = true;
  displayedColumns: string[] = ['title', 'detail', 'newDtail', 'date'];

  saveDraftData() {
    console.log('saveDraftData');
    this.saveDraftstatus = 'success';
    setTimeout(() => {
      this.saveDraftstatus = 'fail';
    }, 3000);
    this.sendSaveStatus();
  }

  sandwork() {
    console.log('ฦฦฦ14');
    this.status_loading = true;
    console.log('14');
    if (localStorage.getItem('level') === 'พนักงาน') {
      this.sendWorkService.sendWork(localStorage.getItem('requestId')).subscribe(res_sendwork => {
        // this.sidebarService.sendEvent();
        if (res_sendwork['status'] === 'success') {
          this.status_loading = false;
          Swal.fire({
            title: 'success',
            text: 'ส่งงานเรียบร้อย',
            icon: 'success',
            showConfirmButton: false,
            timer: 3000
          });
          this.router.navigate(['/comment']);
        }
        if (res_sendwork['message'] === 'กรุณากรอกข้อมูลผู้ดำเนินการ') {
          this.status_loading = false;
          Swal.fire({
            title: 'error',
            text: res_sendwork['message'],
            icon: 'error',
            showConfirmButton: false,
            timer: 2000,
          });
          this.router.navigate(['/save-manager']);
        } else if (res_sendwork['status'] === 'fail' || res_sendwork['status'] === 'error') {
          this.status_loading = false;
          Swal.fire({
            title: 'error',
            text: res_sendwork['message'],
            icon: 'error',
            showConfirmButton: false,
            timer: 2000,
          });
        } else {
          this.router.navigate(['/comment']);
        }
      });
    }
    else {
      this.status_loading = false;
      this.router.navigate(['/comment']);
    }
  }

  constructor(
    private router: Router,
    private stepFourteenService: StepFourteenService,
    private sendWorkService: SendWorkService
  ) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.userRole = localStorage.getItem('role');
    console.log('userRole summary step14', this.userRole);
    this.stepFourteenService.getStepFourteen(localStorage.getItem('requestId')).subscribe((res) => {
      console.log('>>>', res);
      const data = res['validate'];
      this.validate = data;
      console.log('validate summary step14', this.validate);
      const dataFourteen = res['data'];
      const filData = [];
      dataFourteen.forEach(ele => {
        console.log('000000 =>', ele);
        if ((ele.topic === null || ele.topic === '')
          && (ele.oldDetail === null || ele.oldDetail === '')
          && (ele.newDetail === null || ele.newDetail === '')
        ) { } else {
          filData.push(ele);
        }
      });
      this.stepFourteen = filData;

    });

  }

  sendSaveStatus() {
    if (this.saveDraftstatus === 'success') {
      this.saveStatus.emit(true);
    } else {
      this.saveStatus.emit(false);
    }
  }

}
