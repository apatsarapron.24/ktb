import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepNineSummaryComponent } from './step-nine.component';

describe('StepNineSummaryComponent', () => {
  let component: StepNineSummaryComponent;
  let fixture: ComponentFixture<StepNineSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepNineSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepNineSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
