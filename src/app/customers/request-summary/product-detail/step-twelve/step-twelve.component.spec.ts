import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepTwelveComponent } from './step-twelve.component';

describe('StepTwelveComponent', () => {
  let component: StepTwelveComponent;
  let fixture: ComponentFixture<StepTwelveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepTwelveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepTwelveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
