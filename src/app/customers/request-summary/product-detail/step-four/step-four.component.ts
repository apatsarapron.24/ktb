
import { Component, OnInit } from '@angular/core';
import { RequestService } from '../../../../services/request/request.service';
import { StepThreeService } from '../../../../services/request/product-detail/step-three/step-three.service';
import { Router } from '@angular/router';
import { stringify } from '@angular/compiler/src/util';
@Component({
  selector: 'app-step-four-summary',
  templateUrl: './step-four.component.html',
  styleUrls: ['./step-four.component.scss']
})
export class StepFourProductSummaryComponent implements OnInit {

  constructor(private sidebarService: RequestService,
    private getPage: StepThreeService,
    private router: Router) { }

  ngOnInit() {
    localStorage.setItem('check', 'A');
    sessionStorage.setItem('page', '');
    if (localStorage) {
      if (localStorage.getItem('requestId')) {
        console.log('localStorage', localStorage.getItem('requestId'));
        this.sidebarService.sendEvent();
        this.CheckNextPage(localStorage.getItem('check'), 'next');
      }
    } else {
      console.log('Brownser not support');
    }
  }

  CheckNextPage(page, action) {
    this.getPage.checkDefault(localStorage.getItem('requestId')).subscribe(res => {
      console.log('this.getPage.checkDefault', res['data']);
      console.log(action);
      if (res['data'].product.length === 1) {
        sessionStorage.setItem('page', 'last');
      }
      if (Number(page) < res['data'].product.length - 1 && action === 'next') {
        const number = stringify(Number(page) + 1);
        this.router.navigate(['/request-summary/product-detail/step4/' + res['data'].product[Number(number)]]);
        localStorage.setItem('check', number);
      } else if (Number(page) < res['data'].product.length && action === 'back' && Number(page) !== 0) {
        const number = stringify(Number(page) - 1);
        this.router.navigate(['/request-summary/product-detail/step4/' + res['data'].product[Number(number)]]);
        localStorage.setItem('check', number);
      } else if (page === 'A') {
        this.router.navigate(['/request-summary/product-detail/step4/' + res['data'].product[0]]);
        localStorage.setItem('check', '0');
      } else {
        console.log('ครบแล้ว');
        this.router.navigate(['/request-summary/product-detail/step5']);
      }
    });
  }
}
