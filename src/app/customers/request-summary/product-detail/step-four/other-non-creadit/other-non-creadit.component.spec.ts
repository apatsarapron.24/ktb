import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherNonCreaditSummaryComponent } from './other-non-creadit.component';

describe('OtherNonCreaditSummaryComponent', () => {
  let component: OtherNonCreaditSummaryComponent;
  let fixture: ComponentFixture<OtherNonCreaditSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherNonCreaditSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherNonCreaditSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
