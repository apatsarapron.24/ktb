import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { MatTableDataSource, MatTable } from '@angular/material/table';
// tslint:disable-next-line: max-line-length
import { StepThreeService } from '../../../../../services/request/product-detail/step-three/step-three.service';
import { Router } from '@angular/router';
import { RequestService } from '../../../../../services/request/request.service';
// import { StepThreeComponent } from '../step-three.component';
import { FormControl, FormGroup } from '@angular/forms';
// tslint:disable-next-line: max-line-length
import { OtherNonCreaditComponent } from '../../../../request/product-detail/step-three/other-non-creadit/other-non-creadit.component';
import { saveAs } from 'file-saver';
import { SendWorkService } from '.././../../../../services/request/product-detail/send-work/send-work.service';
import Swal from 'sweetalert2';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogSaveStatusComponent } from '../../../../request/dialog/dialog-save-status/dialog-save-status.component';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CanComponentDeactivate } from '../../../../../services/configGuard/config-guard.service';

@Component({
  selector: 'app-other-non-creadit-summary',
  templateUrl: './other-non-creadit.component.html',
  styleUrls: ['./other-non-creadit.component.scss']
})
export class OtherNonCreaditSummaryComponent implements OnInit, CanComponentDeactivate {

  @ViewChild(OtherNonCreaditComponent) RequestOtherNonCreadit: OtherNonCreaditComponent;

  @Input() template_manage = { role: null, validate: null, caaStatus: null, checkNon: null, pageShow: null };
  @Output() saveStatus = new EventEmitter<boolean>();
  displayedColumns_file: string[] = ['fileName', 'fileSize', 'delete'];
  dataSource_file: MatTableDataSource<FileList>;

  dataSource_file_summary: MatTableDataSource<FileList>;
  displayedColumns_file_summary: string[] = [
    'fileName',
    'fileSize',
    'download',
  ];

  urls: any = [];
  status_loading = true;
  pageLast: any;


  saveDraftstatus = null;
  outOfPageSave = false;
  linkTopage = null;
  pageChange = null;
  openDoCheckc = false;
  showCircle_colId = null;
  showCircle_id = null;
  enter = 3;
  stepThreeOther = {
    validate: null,
    requestId: null,
    type: null,
    product: {
      file: [],
      fileDelete: []
    },
    productText: null,
    target: {
      file: [],
      fileDelete: []
    },
    targetText: null,
    property: {
      file: [],
      fileDelete: []
    },
    propertyText: null,
    other: {
      file: [],
      fileDelete: []
    },
    otherText: null,
    attachments: {
      file: [],
      fileDelete: []
    },
    status: false,
  };

  file1_1: any;
  file1_2: any;
  file2_1: any;
  file2_2: any;
  file3_1: any;
  file3_2: any;
  file4_1: any;
  file4_2: any;
  file5_1: any;
  file5_2: any;
  fileAttachments: any;
  document: any;
  Agreement = null;
  file_name: any;
  file_size: number;

  imgURL: any;
  image1 = [];
  image2 = [];
  image3 = [];
  image4 = [];
  image5 = [];

  showImage1 = false;
  message = null;

  // Model
  showModel = false;
  modalmainImageIndex: number;
  modalmainImagedetailIndex: number;
  indexSelect: number;
  modalImage = [];
  modalImagedetail = [];
  fileValue: number;


  // textareaForm = new FormGroup({
  //   productText: new FormControl(this.stepThreeOther.productText),
  //   targetText: new FormControl(this.stepThreeOther.targetText),
  //   propertyText: new FormControl(this.stepThreeOther.propertyText),
  //   otherText: new FormControl(this.stepThreeOther.otherText),
  // });
  testfile = {};
  stepPage: any;
  checkLocal: any;
  listSummaryStep4_6: any = [];
  showsearch: any = [];
  showData = false;
  downloadfile_by = [];

  constructor(
    private StepThreeOtherService: StepThreeService,
    private sidebarService: RequestService,
    private router: Router,
    private sendWorkService: SendWorkService,
    public dialog: MatDialog,
    // private CheckPage: StepThreeComponent,
  ) { }

  checkPageNon: any;
  pageShow: any;

  validate = null;
  userRole = null;

  ngOnInit() {
    this.openDoCheckc = true;
    this.stepPage = localStorage.getItem('check');
    this.checkLocal = localStorage.getItem('check');
    this.pageLast = (sessionStorage.getItem('page'));
    console.log('pageeee', this.pageLast);

    if (this.template_manage.checkNon === 'Credit' || this.template_manage.checkNon === 'NonCredit') {
      if (this.template_manage.checkNon === 'Credit') {
        this.checkPageNon = '';
      } else if (this.template_manage.checkNon === 'NonCredit') {
        this.checkPageNon = 'NonCredit';
      }
      // console.log('this.checkPageNon', this.checkPageNon);
      if (this.checkPageNon === '') {
        this.pageShow = 'Credit';
      } else if (this.checkPageNon === 'NonCredit') {
        this.pageShow = 'Non-Credit Product';
      }
    } else if (this.template_manage.checkNon === null) {
      this.checkPageNon = sessionStorage.getItem('check_non');
      // console.log('this.checkPageNon', this.checkPageNon);
      if (this.checkPageNon === '') {
        this.pageShow = 'Credit';
      } else if (this.checkPageNon === 'NonCredit') {
        this.pageShow = 'Non-Credit Product';
      }
    }

    this.checkLocal = localStorage.getItem('check');
    // localStorage.setItem('requestId', '1');
    if (localStorage) {
      if (localStorage.getItem('requestId')) {
        console.log('localStorage', localStorage.getItem('requestId'));
        console.log('checkPageNon:', this.checkPageNon);
        console.log('RequestOtherNonCreadit:', this.RequestOtherNonCreadit.stepThreeOther.attachments.file);
        this.sidebarService.sendEvent();
        // set userRole
        this.userRole = localStorage.getItem('role');
        console.log('userRole summary step4.6', this.userRole);

        this.getDataProductDetailstepThreeOther(localStorage.getItem('requestId'));
        this.StepThreeOtherService.get_step_three_other(Number(localStorage.getItem('requestId')), 'NonCredit').subscribe(
          (ress) => {
            console.log('resssssss;', ress);
            if (ress['data'] !== null) {
              this.dataSource_file = new MatTableDataSource(ress['data']['attachments']['file']);
              this.hideDownloadFile();
            } else {
              this.dataSource_file = new MatTableDataSource([]);
            }
            const data = ress['data'];
            this.validate = ress['validate'];
            console.log('validate summary step4.6', this.validate);
          });
        this.getList();
      } else {
        this.status_loading = false;
      }
    } else {
      console.log('Brownser not support');
    }
  }
  getList() {
    this.StepThreeOtherService.get_step_three_non_credit(localStorage.getItem('requestId')).subscribe(
      (res) => {
        if (res['parentRequestId'] !== null) {
          this.listSummaryStep4_6 = res['parentRequestId'];
          for (let index = 0; index < this.listSummaryStep4_6.length; index++) {
            this.showsearch.push({ index: index, value: true });
          }
        }
      });
  }
  sendSaveStatus() {
    if (this.saveDraftstatus === 'success') {
      this.saveStatus.emit(true);
    } else {
      this.saveStatus.emit(false);
    }
    console.log('aaaaaa =>>', this.saveStatus);
  }
  autogrow(number: any) {
    if (number === 1) {
      const productText = document.getElementById('productText');
      productText.style.overflow = 'hidden';
      productText.style.height = 'auto';
      productText.style.height = productText.scrollHeight + 'px';
    }

    if (number === 2) {
      const targetText = document.getElementById('targetText');
      targetText.style.overflow = 'hidden';
      targetText.style.height = 'auto';
      targetText.style.height = targetText.scrollHeight + 'px';
    }
    if (number === 3) {
      const propertyText = document.getElementById('propertyText');
      propertyText.style.overflow = 'hidden';
      propertyText.style.height = 'auto';
      propertyText.style.height = propertyText.scrollHeight + 'px';
    }
    if (number === 4) {
      const otherText = document.getElementById('otherText');
      otherText.style.overflow = 'hidden';
      otherText.style.height = 'auto';
      otherText.style.height = otherText.scrollHeight + 'px';
    }
  }
  saveDraft() {

    // this.stepThreeOther.productText = document.getElementById('productText').innerHTML;
    // this.stepThreeOther.targetText = document.getElementById('targetText').innerHTML;
    // this.stepThreeOther.propertyText = document.getElementById('propertyText').innerHTML;
    // this.stepThreeOther.otherText = document.getElementById('otherText').innerHTML;

    console.log('data other to save', this.stepThreeOther);
    if (localStorage.getItem('requestId')) {
      this.stepThreeOther.requestId = localStorage.getItem('requestId');
      if (this.template_manage.checkNon === null) {
        this.stepThreeOther.type = sessionStorage.getItem('check_non');
      } else if (this.template_manage.checkNon !== null) {
        this.stepThreeOther.type = this.checkPageNon;
      }
    }
    if ((((this.stepThreeOther.productText
      && this.stepThreeOther.targetText)
      && this.stepThreeOther.propertyText)
    )
    ) {
      this.stepThreeOther.status = true;
    } else { this.stepThreeOther.status = false; }

    this.StepThreeOtherService.update_step_three_other(this.stepThreeOther).subscribe(res => {
      if (res['status'] === 'success') {
        this.status_loading = false;
        this.saveDraftstatus = 'success';
        this.stepThreeOther.attachments.file = [];
        this.stepThreeOther.attachments.fileDelete = [];
        this.sendSaveStatus();
        this.sidebarService.sendEvent();
        this.getDataProductDetailstepThreeOther(localStorage.getItem('requestId'));
      } else {
        this.status_loading = false;
        alert(res['message']);
      }
    }, err => {
      this.status_loading = false;
      console.log(err);
      this.sendSaveStatus();
      alert(err['message']);
      // alert('เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้');
    });
    console.log('this.stepThreeOther', this.stepThreeOther);
  }

  next_step(action) {
    this.StepThreeOtherService.CheckNextPageSummary(localStorage.getItem('check'), action);
  }

  viewNext() {
    this.StepThreeOtherService.CheckNextPageSummary(localStorage.getItem('check'), 'next');
  }

  changeSaveDraft() {
    this.saveDraftstatus = null;
    this.sendSaveStatus();
    this.sidebarService.inPageStatus(true);
  }

  // ================================================================================

  delete_image(fileIndex, index) {
    console.log('fileIndex:', fileIndex);
    console.log('index:', index);
    if (fileIndex === 1) {
      if ('id' in this.stepThreeOther.product.file[index]) {
        const id = this.stepThreeOther.product.file[index].id;
        console.log('found id');
        if ('fileDelete' in this.stepThreeOther.product) {
          this.stepThreeOther.product.fileDelete.push(id);
        }
      }
      this.stepThreeOther.product.file.splice(index, 1);
      this.image1.splice(index, 1);

      for (let i = 0; i < this.stepThreeOther.product.file.length; i++) {
        this.preview(fileIndex, this.stepThreeOther.product.file[i], i);
      }
    } else if (fileIndex === 2) {
      if ('id' in this.stepThreeOther.target.file[index]) {
        const id = this.stepThreeOther.target.file[index].id;
        console.log('found id');
        if ('fileDelete' in this.stepThreeOther.target) {
          this.stepThreeOther.target.fileDelete.push(id);
        }
      }
      this.stepThreeOther.target.file.splice(index, 1);
      this.image2.splice(index, 1);

      for (let i = 0; i < this.stepThreeOther.target.file.length; i++) {
        this.preview(fileIndex, this.stepThreeOther.target.file[i], i);
      }
    } else if (fileIndex === 3) {
      if ('id' in this.stepThreeOther.property.file[index]) {
        const id = this.stepThreeOther.property.file[index].id;
        console.log('found id');
        if ('fileDelete' in this.stepThreeOther.property) {
          this.stepThreeOther.property.fileDelete.push(id);
        }
      }
      this.stepThreeOther.property.file.splice(index, 1);
      this.image3.splice(index, 1);

      for (let i = 0; i < this.stepThreeOther.property.file.length; i++) {
        this.preview(fileIndex, this.stepThreeOther.property.file[i], i);
      }
    } else if (fileIndex === 4) {
      if ('id' in this.stepThreeOther.other.file[index]) {
        const id = this.stepThreeOther.other.file[index].id;
        console.log('found id');
        if ('fileDelete' in this.stepThreeOther.other) {
          this.stepThreeOther.other.fileDelete.push(id);
        }
      }
      this.stepThreeOther.other.file.splice(index, 1);
      this.image4.splice(index, 1);

      for (let i = 0; i < this.stepThreeOther.other.file.length; i++) {
        this.preview(fileIndex, this.stepThreeOther.other.file[i], i);
      }
    }
    this.changeSaveDraft();
  }

  image_click(colId, id) {
    this.showModel = true;
    console.log('colId:', colId);
    console.log('id:', id);
    document.getElementById('myModal3_55').style.display = 'block';
    this.imageModal(colId, id);
  }
  image_bdclick() {
    document.getElementById('myModal3_55').style.display = 'block';
  }

  closeModal() {
    this.showModel = false;
    document.getElementById('myModal3_55').style.display = 'none';
  }

  // getFileDetails ================================================================
  getFileDetails(fileIndex, event) {

    const files = event.target.files;
    for (let i = 0; i < files.length; i++) {
      const mimeType = files[i].type;
      if (mimeType.match(/image\/*/) == null) {
        this.message = 'Only images are supported.';
        alert(this.message);
        return;
      }
      const file = files[i];

      console.log('file:', file);
      const reader = new FileReader();
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');

        const templateFile = {
          fileName: file.name,
          base64File: splitFile[1],
        };
        console.log('templateFile123456', templateFile);

        if (fileIndex === 1) {
          console.log('00000000000000000000000000000001');
          this.stepThreeOther.product.file.push(templateFile);

          for (let index = 0; index < this.stepThreeOther.product.file.length; index++) {
            this.preview(fileIndex, this.stepThreeOther.product.file[index], index);
          }
          this.file1_1 = undefined;
          this.file1_2 = undefined;
        } else if (fileIndex === 2) {
          console.log('00000000000000000000000000000002');
          this.stepThreeOther.target.file.push(templateFile);


          for (let index = 0; index < this.stepThreeOther.target.file.length; index++) {
            this.preview(fileIndex, this.stepThreeOther.target.file[index], index);
            this.file2_1 = undefined;
            this.file2_2 = undefined;
          }
        } else if (fileIndex === 3) {
          console.log('00000000000000000000000000000003');
          this.stepThreeOther.property.file.push(templateFile);

          for (let index = 0; index < this.stepThreeOther.property.file.length; index++) {
            this.preview(fileIndex, this.stepThreeOther.property.file[index], index);
          }
          this.file3_1 = undefined;
          this.file3_2 = undefined;
        } else if (fileIndex === 4) {
          console.log('00000000000000000000000000000004');
          this.stepThreeOther.other.file.push(templateFile);

          for (let index = 0; index < this.stepThreeOther.other.file.length; index++) {
            this.preview(fileIndex, this.stepThreeOther.other.file[index], index);
          }
          this.file4_1 = undefined;
          this.file4_2 = undefined;
        }
      };
      reader.readAsDataURL(file);
    }
    this.changeSaveDraft();
  }

  preview(fileIndex, file, index) {
    const name = file.fileName;
    const typeFiles = name.match(/\.\w+$/g);
    const typeFile = typeFiles[0].replace('.', '');

    const tempPicture = `data:image/${typeFile};base64,${file.base64File}`;

    if (fileIndex === 1) {
      if (file) {
        this.image1[index] = tempPicture;
        // console.log('preview image1', this.image1[0]);
      }
    } else if (fileIndex === 2) {
      if (file) {
        this.image2[index] = tempPicture;
      }
    } else if (fileIndex === 3) {
      if (file) {
        this.image3[index] = tempPicture;
      }
    } else if (fileIndex === 4) {
      if (file) {
        this.image4[index] = tempPicture;
      }
    } else if (fileIndex === 5) {
      if (file) {
        this.image5[index] = tempPicture;
      }
    }

  }

  // getFileDocument ================================================================
  // รอ API
  // getFileDocument(event) {
  //   const file = event.target.files[0];
  //   const mimeType = file.type;
  //   if (mimeType.match(/pdf\/*/) == null) {
  //     this.message = 'Only PDF are supported.';
  //     alert(this.message);
  //     return;
  //   }
  //   console.log('11111', file);
  //   this.file_name = file.name;
  //   // this.file_type = file.type;
  //   this.file_size = file.size / 1024;
  //   // this.modifiedDate = file.lastModifiedDate;
  //   // console.log('file:', file);

  //   this.stepThreeOther.attachments = file;
  //   // this.document = undefined;
  // }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      const file = event.target.files;
      for (let i = 0; i < filesAmount; i++) {
        console.log('type:', file[i]);
        if (
          // file[i].type === 'image/svg+xml' ||
          file[i].type === 'image/jpeg' ||
          // file[i].type === 'image/raw' ||
          // file[i].type === 'image/svg' ||
          // file[i].type === 'image/tif' ||
          // file[i].type === 'image/gif' ||
          file[i].type === 'image/jpg' ||
          file[i].type === 'image/png' ||
          file[i].type === 'application/doc' ||
          file[i].type === 'application/pdf' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
          file[i].type === 'application/pptx' ||
          file[i].type === 'application/docx' ||
          (file[i].type === 'application/ppt' && file[i].size)
        ) {
        if (file[i].size <= 5000000) {
          this.multiple_file(file[i], i);
        }
        } else {
          this.status_loading = false;
          alert('File Not Support');
        }
      }
    }
  }

  multiple_file(file, index) {
    // console.log('download');
    const reader = new FileReader();
    if (file) {
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');

        const templateFile = {
          fileName: file.name,
          fileSize: file.size,
          base64File: splitFile[1],
        };

        this.testfile = templateFile;

        this.stepThreeOther.attachments.file.push(templateFile);
        this.RequestOtherNonCreadit.stepThreeOther.attachments.file.push(templateFile);
        this.dataSource_file = new MatTableDataSource(this.stepThreeOther.attachments.file);
        this.hideDownloadFile();
      };
    }
    this.fileAttachments = undefined;
    this.changeSaveDraft();
  }

  delete_mutifile(data: any, index: any) {
    console.log('data:', data, 'index::', index);
    if ('id' in this.stepThreeOther.attachments.file[index]) {
      const id = this.stepThreeOther.attachments.file[index].id;
      console.log('found id');
      if ('fileDelete' in this.stepThreeOther.attachments) {
        this.stepThreeOther.attachments.fileDelete.push(id);
        this.RequestOtherNonCreadit.stepThreeOther.attachments.fileDelete.push(id);
      }
    }
    console.log('fileDelete attachments : ', this.stepThreeOther.attachments.fileDelete);
    this.stepThreeOther.attachments.file.splice(index, 1);
    this.RequestOtherNonCreadit.stepThreeOther.attachments.file.splice(index, 1);
    this.dataSource_file = new MatTableDataSource(this.stepThreeOther.attachments.file);
    this.changeSaveDraft();
  }



  // ============================== Modal ==========================================
  imageModal(fileIndex, index) {
    if (fileIndex === 1) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image1;
      this.modalImagedetail = this.stepThreeOther.product.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 2) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image2;
      this.modalImagedetail = this.stepThreeOther.target.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 3) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image3;
      this.modalImagedetail = this.stepThreeOther.property.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    } else if (fileIndex === 4) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image4;
      this.modalImagedetail = this.stepThreeOther.other.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    }
  }
  // ===============================================================================
  // ======================================= API ===================================
  getDataProductDetailstepThreeOther(documentID) {
    this.StepThreeOtherService.get_step_three_other(documentID, 'NonCredit').subscribe(res => {
      console.log('res', res);
      if (res['status'] === 'success') {
        this.status_loading = false;
        this.status_loading = false;
      }
      if (res['data'] !== null) {
        this.stepThreeOther = res['data'];

        // set fileDelete to Object stepOne
        this.stepThreeOther.product.fileDelete = [];
        this.stepThreeOther.target.fileDelete = [];
        this.stepThreeOther.property.fileDelete = [];
        this.stepThreeOther.other.fileDelete = [];
        this.stepThreeOther.attachments.fileDelete = [];


        // this.textareaForm = new FormGroup({
        //   productText: new FormControl(this.stepThreeOther.productText),
        //   targetText: new FormControl(this.stepThreeOther.targetText),
        //   propertyText: new FormControl(this.stepThreeOther.propertyText),
        //   otherText: new FormControl(this.stepThreeOther.otherText),
        // });

        console.log('set fileDelete', this.stepThreeOther);

        // set picture preview to all input
        for (let i = 0; i < this.stepThreeOther.product.file.length; i++) {
          this.preview(1, this.stepThreeOther.product.file[i], i);
        }

        for (let i = 0; i < this.stepThreeOther.target.file.length; i++) {
          this.preview(2, this.stepThreeOther.target.file[i], i);
        }

        for (let i = 0; i < this.stepThreeOther.property.file.length; i++) {
          this.preview(3, this.stepThreeOther.property.file[i], i);
        }

        for (let i = 0; i < this.stepThreeOther.other.file.length; i++) {
          this.preview(4, this.stepThreeOther.other.file[i], i);
        }

        // set file attachments
        // this.dataSource_file = new MatTableDataSource(this.RequestOtherNonCreadit.stepThreeOther.attachments.file);
        this.dataSource_file_summary = new MatTableDataSource(
          this.stepThreeOther.attachments.file
        );
      }

    }, err => {
      console.log(err);
    });
  }

  async saveDraftData(page?, action?) {
    // set pages
    let pages = 'summary';
    if (page) {
      pages = page;
    }
    // set actions
    let actions = 'save';
    if (action) {
      actions = action;
    }
    // ======================================
    console.log('saved');
    this.RequestOtherNonCreadit.stepThreeOther.attachments.file = [];
    this.RequestOtherNonCreadit.stepThreeOther.attachments.fileDelete = [];
    const result1 = await this.save(pages, actions);
    const result2 = await this.result();
    console.log('result2', result2);
    this.saveDraftstatus = result2;
    this.stepThreeOther.attachments.file = [];
    this.stepThreeOther.attachments.fileDelete = [];
    if (this.saveDraftstatus === 'success') {
      const after = await this.AfterStart();
    }
  }

  save(page?, action?): Promise<any> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        this.RequestOtherNonCreadit.stepThreeOther.attachments.file = this.stepThreeOther.attachments.file;
        this.RequestOtherNonCreadit.stepThreeOther.attachments.fileDelete = this.stepThreeOther.attachments.fileDelete;

        return resolve(this.RequestOtherNonCreadit.saveDraft(page, action));
      }, 100);
    });
  }
  result() {
    return new Promise((resolve, reject) => {
      setTimeout(
        () => console.log('result:', resolve(this.RequestOtherNonCreadit.saveDraftstatus)),
        1000
      );
    });
  }
  AfterStart() {
    return new Promise((resolve, reject) => {
      setTimeout(
        () => {
          this.stepThreeOther.attachments.file = [];
          this.stepThreeOther.attachments.fileDelete = [];
          this.image1 = this.RequestOtherNonCreadit.image1;
          this.image2 = this.RequestOtherNonCreadit.image2;
          this.image3 = this.RequestOtherNonCreadit.image3;
          this.image4 = this.RequestOtherNonCreadit.image4;
          this.image5 = this.RequestOtherNonCreadit.image5;
          this.getDataProductDetailstepThreeOther(
            localStorage.getItem('requestId')
          );
          this.saveDraftstatus = 'success';
          setTimeout(() => {
            this.saveDraftstatus = 'fail';
          }, 3000);
          this.getList();
        }
        ,
        500
      );
    });
  }

  hideDownloadFile() {
    if (this.dataSource_file.data.length > 0) {
      this.downloadfile_by = [];

      for (let index = 0; index < this.dataSource_file.data.length; index++) {
        const element = this.dataSource_file.data[index];
        console.log('ele', element);

        if ('path' in element) {
          this.downloadfile_by.push('path');
        } else {
          this.downloadfile_by.push('base64');
        }
      }
    }
    console.log('by:', this.downloadfile_by);
  }
  downloadFile(pathdata: any) {
    this.status_loading = true;
    const contentType = '';
    const sendpath = pathdata;
    console.log('path', sendpath);
    this.sidebarService.getfile(sendpath).subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        this.status_loading = false;
        const datagetfile = res['data'];
        const b64Data = datagetfile['data'];
        const filename = datagetfile['fileName'];
        console.log('base64', b64Data);
        const config = this.b64toBlob(b64Data, contentType);
        saveAs(config, filename);
      }

    });
  }

  b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const convertbyte = atob(b64Data);
    const byteCharacters = atob(convertbyte);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  downloadFile64(data: any) {
    console.log('>>>>', data);
    this.status_loading = true;
    const contentType = '';
    this.status_loading = false;
    const b64Data = data.base64File;
    const filename = data.fileName;
    console.log('base64', b64Data);
    console.log('filename', filename);
    const config = this.base64(b64Data, contentType);
    saveAs(config, filename);
  }
  
  base64(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
  
      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
  
      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
  
    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  sandwork() {
    this.status_loading = true;
    console.log('save summary base-step-2');
    this.sendWorkService.sendWork(localStorage.getItem('requestId')).subscribe(res_sendwork => {
      // this.sidebarService.sendEvent();
      if (res_sendwork['message'] === 'success') {
        this.status_loading = false;
        this.router.navigate(['/comment']);
      }
      if (res_sendwork['message'] === 'กรุณากรอกข้อมูลผู้ดำเนินการ') {
        this.status_loading = false;
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
        this.router.navigate(['/save-manager']);
      } else if (res_sendwork['status'] === 'fail' || res_sendwork['status'] === 'error') {
        this.status_loading = false;
        Swal.fire({
          title: 'error',
          text: res_sendwork['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 2000,
        });
      } else {
        this.router.navigate(['/comment']);
      }
    });
  }
  show() {
    if (this.showData === true) {
      this.showData = false;
    } else {
      this.showData = true;
    }
  }
  showSearch(index: any) {
    if (this.showsearch[index].value === true) {
      this.showsearch[index].value = false;
    } else {
      this.showsearch[index].value = true;
    }
  }
  getSummarNew(index: any, requestId: any) {
    console.log(index);
    console.log(requestId);
    this.image1 = [];
    this.image2 = [];
    this.image3 = [];
    this.image4 = [];
    this.image5 = [];
    this.getDataProductDetailstepThreeOther(requestId);
    for (let i = 0; i < this.showsearch.length; i++) {
      if (index !== this.showsearch[i].index) {
        this.showsearch[i].value = true;
      }
    }
  }

  // ngOnDestroy(): void {
  //   const data = this.sidebarService.getsavePage();
  //   let saveInFoStatus = false;
  //   saveInFoStatus = data.saveStatusInfo;
  //   if (saveInFoStatus === true) {
  //     this.outOfPageSave = true;
  //     this.linkTopage = data.goToLink;
  //     console.log('ngOnDestroy', this.linkTopage);
  //     this.saveDraft();
  //     this.sidebarService.resetSaveStatusInfo();
  //   }

  // ngDoCheck() {
  //   if (this.openDoCheckc === true) {
  //     this.pageChange = this.sidebarService.changePageGoToLink();
  //     if (this.pageChange) {
  //       const data = this.sidebarService.getsavePage();
  //       let saveInFoStatus = false;
  //       saveInFoStatus = data.saveStatusInfo;
  //       if (saveInFoStatus === true) {
  //         this.outOfPageSave = true;
  //         this.linkTopage = data.goToLink;
  //         this.saveDraftData('navbarSummary', this.linkTopage );
  //         this.sidebarService.resetSaveStatu();
  //       }
  //     }
  //   }
  // }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate request');
    const subject = new Subject<boolean>();
    const status = this.sidebarService.outPageStatus();
    if (status === true) {
      // -----
      const dialogRef =  this.dialog.open(DialogSaveStatusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnDetail: 'null',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        if (data.returnStatus === true) {
          const raturnStatus = this.RequestOtherNonCreadit.saveDraft();
          console.log('summary raturnStatus', raturnStatus);
            if (raturnStatus) {
              console.log('raturnStatus T', this.RequestOtherNonCreadit.saveDraftstatus);
              subject.next(true);
            } else {
              console.log('raturnStatus F', this.RequestOtherNonCreadit.saveDraftstatus);
              localStorage.setItem('check', this.stepPage);
              subject.next(false);
            }
        } else if (data.returnStatus === false) {
          this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          localStorage.setItem('check', this.stepPage);
          subject.next(false);
        }
      });
      return subject;
    } else {
      return true;
    }
  }
}

