import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NonCreditSummaryComponent } from './non-credit.component';

describe('NonCreditSummaryComponent', () => {
  let component: NonCreditSummaryComponent;
  let fixture: ComponentFixture<NonCreditSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NonCreditSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NonCreditSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
