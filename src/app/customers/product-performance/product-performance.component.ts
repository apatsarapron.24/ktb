import { Component, OnInit } from '@angular/core';
import { ProductPerformanceService } from '../../services/product-performance/product-performance.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { CanDeactivateGuard } from './../../services/configGuard/config-guard.service';
import { MatDialog } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { DialogSaveStatusPerformanceComponent } from './dialog-save-status-performance/dialog-save-status-performance/dialog-save-status-performance.component';
import { Subject } from 'rxjs/Subject';
declare var $: any;

@Component({
  selector: 'app-product-performance',
  templateUrl: './product-performance.component.html',
  styleUrls: ['./product-performance.component.scss']
})
export class ProductPerformanceComponent implements OnInit {

  requestid: any;
  dataproductperformance: any = [];
  header = ['ที่เกิดขึ้นจริง ปี '];
  subheader = ['Q1', '%', 'Q2', '%', 'Q3', '%', 'Q4', '%'];
  years: any = [];
  transaction: any = [];
  compensation: any = [];
  control: any = [];
  analyze = '';
  allresult = [];
  confirmState = 'confirm';
  saveDraftstatus = null;
  validate: boolean;
  saveDraftstatus_State = false;

  constructor(
    private productperformanceService: ProductPerformanceService,
    private router: Router,
    public dialog: MatDialog,
    private guardService: CanDeactivateGuard
  ) { }

  ngOnInit() {
    this.requestid = localStorage.getItem('requestId');
    this.saveDraftstatus = null;
    this.getProductPerformance();
  }

  getProductPerformance() {
    this.productperformanceService.getProductPerformance(this.requestid).subscribe(res => {

      if (res['data']) {
        this.dataproductperformance = res['data'];
        this.years = this.dataproductperformance.performance.year;
        this.transaction = this.dataproductperformance.performance.transaction;
        this.compensation = this.dataproductperformance.performance.compensation;
        this.control = this.dataproductperformance.performance.control;
        this.analyze = this.dataproductperformance.analyze;
        this.validate = this.dataproductperformance.validate;
      }
    });
  }

  onPushData() {
    let _resultstransaction: any;
    let _resultscompensation: any;
    let _resultscontrol: any;
    const _allresult = [];

    // transaction
    for (let i = 0; i < this.transaction.length; i++) {
      const element = this.transaction[i];
      if (element) {
        _resultstransaction = element.results;

        for (let j = 0; j < _resultstransaction.length; j++) {
          const _result = _resultstransaction[j];
          _allresult.push(_result);
        }
      }
    }

    // compensation
    for (let i = 0; i < this.compensation.length; i++) {
      const element = this.compensation[i];
      if (element) {
        _resultscompensation = element.results;

        for (let j = 0; j < _resultscompensation.length; j++) {
          const _result = _resultscompensation[j];
          _allresult.push(_result);
        }
      }
    }

    // control
    for (let i = 0; i < this.control.length; i++) {
      const element = this.control[i];
      if (element) {
        _resultscontrol = element.results;

        for (let j = 0; j < _resultscontrol.length; j++) {
          const _result = _resultscontrol[j];
          _allresult.push(_result);
        }
      }
    }
    this.allresult = _allresult;
  }

  onSave() {
    this.onPushData();
    $('#confirmModal').modal('hide');
    const data = {
      'requestId': localStorage.getItem('requestId'),
      'send': false,
      'results': this.allresult,
      'analyze': this.analyze
    };

    console.log('data', data);
    this.productperformanceService.postProductPerformance(data).subscribe(res => {
      console.log(res);
      if (res['status'] === 'success') {
        this.saveDraftstatus = 'success';
        this.saveDraftstatus_State = false;
        setTimeout(() => {
          this.saveDraftstatus = 'fail';
        }, 3000);
      } else {
        Swal.fire({
          title: 'error',
          text: res['message'],
          icon: 'error',
        });
      }
    });
  }

  onSend() {
    this.onPushData();
    $('#confirmModal').modal('hide');
    const data = {
      'requestId': localStorage.getItem('requestId'),
      'send': true,
      'results': this.allresult,
      'analyze': this.analyze
    };

    console.log('data', data);
    this.productperformanceService.postProductPerformance(data).subscribe(res => {
      console.log(res);
      if (res['status'] === 'success') {
        this.confirmState = 'success';
        this.saveDraftstatus_State = false;
        Swal.fire({
          title: 'success',
          text: 'ส่งงานเรียบร้อย',
          icon: 'success',
          showConfirmButton: false,
          timer: 3000,
        })
        setTimeout(() => {
          this.confirmState = 'confirm';
        }, 3000);

      } else {
        Swal.fire({
          title: 'error',
          text: res['message'],
          icon: 'error',
        });
      }
    }, err => {
      console.log(err);
    });

  }

  backToDashboardPage() {
    localStorage.setItem('requestId', '');
    this.router.navigate(['dashboard1']);
  }
  autoGrowTextZone(e) {
    e.target.style.height = '0px';
    e.target.style.overflow = 'hidden';
    e.target.style.height = (e.target.scrollHeight + 0) + 'px';
  }

  changeSaveDraft() {
    this.saveDraftstatus_State = true;
    this.productperformanceService.status_state = true;
    console.log('saveDraftstatus:', this.saveDraftstatus_State);
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate');
    // return confirm();
    const subject = new Subject<boolean>();
    // const status = this.sidebarService.outPageStatus();
    // console.log('000 sidebarService status:', status);
    if (this.saveDraftstatus_State === true) {
      // console.log('000 sidebarService status:', status);
      const dialogRef = this.dialog.open(DialogSaveStatusPerformanceComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        console.log('data::', data);
        if (data.returnStatus === true) {
          subject.next(true);
          this.onSave();
          this.saveDraftstatus_State = false;
        } else if (data.returnStatus === false) {
          console.log('000000 data::', data.returnStatus);
          // this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
        }
      });
      console.log('=====', subject);
      return subject;
    } else {
      return true;
    }
  }
}




