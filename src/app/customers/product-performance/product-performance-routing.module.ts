import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductPerformanceComponent } from './product-performance.component';
import { CanDeactivateGuard } from './../../services/configGuard/config-guard.service';

const routes: Routes = [
  {
    path: '',
    component: ProductPerformanceComponent,
    canDeactivate: [CanDeactivateGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductPerformanceRoutingModule { }
