import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogSaveStatusPerformanceComponent } from './dialog-save-status-performance.component';

describe('DialogSaveStatusPerformanceComponent', () => {
  let component: DialogSaveStatusPerformanceComponent;
  let fixture: ComponentFixture<DialogSaveStatusPerformanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogSaveStatusPerformanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogSaveStatusPerformanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
