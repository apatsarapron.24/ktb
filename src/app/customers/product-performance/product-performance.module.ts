import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductPerformanceRoutingModule } from './product-performance-routing.module';
import { ProductPerformanceComponent } from './product-performance.component';
import { MatDialogModule, MatTableModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { TextareaAutoresizeDirectiveModule } from '../../textarea-autoresize.directive/textarea-autoresize.directive.module';
import { DialogSaveStatusPerformanceComponent } from './dialog-save-status-performance/dialog-save-status-performance/dialog-save-status-performance.component';
import { CanDeactivateGuard } from './../../services/configGuard/config-guard.service';

@NgModule({
  declarations: [
    ProductPerformanceComponent,
    DialogSaveStatusPerformanceComponent,
  ],
  imports: [
    CommonModule,
    ProductPerformanceRoutingModule,
    FormsModule,
    MatTableModule,
    TextareaAutoresizeDirectiveModule,
    MatDialogModule
  ],
  providers: [CanDeactivateGuard],
  entryComponents: [DialogSaveStatusPerformanceComponent]
})
export class ProductPerformanceModule { }
