import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExecutiveSummaryRoutingModule } from './executive-summary-routing.module';
import { ExecutiveSummaryComponent } from './executive-summary.component';
import { CommentPointModule } from './../comment-point/comment-point.module';
import { CommentComplianceComponent } from './comment-compliance/comment-compliance.component';
import { CommentRiskComponent } from './comment-risk/comment-risk.component';
import { CommentOperationComponent } from './comment-operation/comment-operation.component';
import { CommentFinanceComponent } from './comment-finance/comment-finance.component';
import { CommentRetailComponent } from './comment-retail/comment-retail.component';
import { RequestModule } from '../request/request.module';
import { CommentAndApprovalModule } from '../comment-and-approval/comment-and-approval.module';
import { RequestSummaryModule } from '../request-summary/request-summary.module';
@NgModule({
  declarations: [
    ExecutiveSummaryComponent,
    CommentComplianceComponent,
    CommentRiskComponent,
    CommentOperationComponent,
    CommentFinanceComponent,
    CommentRetailComponent
  ],
  imports: [
    CommonModule,
    ExecutiveSummaryRoutingModule,
    CommentPointModule,
    RequestModule,
    RequestSummaryModule,
    CommentAndApprovalModule,
  ]
})
export class ExecutiveSummaryModule { }
