import { Component, OnInit } from '@angular/core';
import { RetailService } from '../../../services/comment-point/retail/retail.service';

@Component({
  selector: 'app-comment-retail',
  templateUrl: './comment-retail.component.html',
  styleUrls: ['./comment-retail.component.scss']
})
export class CommentRetailComponent implements OnInit {

  constructor(private retailService: RetailService) { }

  header_table = ['กระบวนการย่อย', 'In/Out', 'IT Related'];
  note = [
    'No	คือ	ไม่ใช่ประเด็นคงค้าง',
    'Pending 1	คือ	ต้องแก้ไขก่อนประกาศใช้ผลิตภัณฑ์',
    'Pending 2 คือ	สามารถใช้ผลิตภัณฑ์ได้ก่อน แล้วค่อยแก้ไขตามหลังได้ ',
    'Pending 3	คือ	ไม่เกี่ยวข้องกับการประกาศใช้ผลิตภัณฑ์',
    'Pending 4	คือ	ต้องแก้ไขให้แล้วเสร็จก่อนนำเสนอเรื่องต่อบอร์ด',
    ];
  // POData: any;
  convertDatePO: any = [];

  POData: any = {
    validate: false,
    listRetailProcessConsiders: [],
    listRetailProcessConsidersExtra: [],
    listRetailOperationDetail: [],
    listRetailOperationDetailExtra: [],
  };

  ngOnInit() {
   this.getDetail();
  }

  getDetail() {
    this.retailService.getRetail_PC(localStorage.getItem('requestId')).subscribe((res) => {
      console.log('res PC', res);
      this.POData = res['data'];

      for (
        let index = 0;
        index < this.POData.listRetailOperationDetail.length;
        index++
      ) {
        // console.log('dueDate:',   this.retailData.commentOperationProcess[index].commentOperation);
        this.POData.listRetailOperationDetail[index].commentOperation.forEach(
          (item, i) => {
            // console.log('dueDate:',   item.dueDate.substr(0, 10));
            const type = String(item.dueDate);
            const year = Number(type.substr(0, 4)) + 543;
            const month = type.substr(5, 2);
            const day = type.substr(8, 2);
            const date = day + '/' + month + '/' + year;
            this.convertDatePO.push({ Convert: date });
          }
        );
      }
    });
  }

}
