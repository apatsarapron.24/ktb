import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentRetailComponent } from './comment-retail.component';

describe('CommentRetailComponent', () => {
  let component: CommentRetailComponent;
  let fixture: ComponentFixture<CommentRetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentRetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentRetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
