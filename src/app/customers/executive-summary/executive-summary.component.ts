import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
// import { Component, OnInit, ElementRef ,ViewChild} from '@angular/core';
import { CommentFinanceComponent } from './comment-finance/comment-finance.component';
import { CommentRetailComponent } from './comment-retail/comment-retail.component';
import { CommentOperationComponent } from './comment-operation/comment-operation.component';
import { CommentComplianceComponent } from './comment-compliance/comment-compliance.component';
import { CommentRiskComponent } from './comment-risk/comment-risk.component';
import { RouterLink, Router, ActivatedRoute } from '@angular/router';
// tslint:disable-next-line:max-line-length
import { SummaryOfConsiderationService } from '../../services/comment-and-approval/summary-of-consideration/summary-of-consideration.service';
import { ProductDefultFirstStepService } from '../../services/request/information-base/first-step/product-defult-first-step.service';
import 'jspdf';
declare let jsPDF;
import { saveAs } from 'file-saver';
// const jsCollapse = require('./showCollapse.js');
// import html2canvas from 'html2canvas';
// import * as jspdf from 'jspdf';


@Component({
  selector: 'app-executive-summary',
  templateUrl: './executive-summary.component.html',
  styleUrls: ['./executive-summary.component.scss']
})

// export class HtmltopdfComponent {
//   public captureScreen() {
//     const data = document.getElementById('contentToConvert');
//     html2canvas(data).then(canvas => {
//       const imgWidth = 208;
//       const pageHeight = 295;
//       const imgHeight = canvas.height * imgWidth / canvas.width;
//       const heightLeft = imgHeight;
//       const contentDataURL = canvas.toDataURL('image/png');
//       const pdf = new jsPDF('Portrait', 'mm', 'A4');
//       const position = 0;
//       pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);
//       pdf.save('MYPdf.pdf');
//     });
//   }
// }
export class ExecutiveSummaryComponent implements OnInit {

  constructor(
    private router: Router,
    private service: SummaryOfConsiderationService,
    private productDefultFirstStep: ProductDefultFirstStepService,
  ) { }

  @ViewChild(CommentFinanceComponent) commentFinance: CommentFinanceComponent;
  @ViewChild(CommentRetailComponent) commentRetail: CommentRetailComponent;
  @ViewChild(CommentOperationComponent) commentOperation: CommentOperationComponent;
  @ViewChild(CommentComplianceComponent) commentCompliance: CommentComplianceComponent;
  @ViewChild(CommentRiskComponent) commentRisk: CommentRiskComponent;

  @ViewChild('content') content: ElementRef;



  information_1 = false;
  information_2 = false;
  information_3 = false;
  information_4_1 = false;
  information_4_2 = false;
  information_4_3 = false;
  information_4_4 = false;
  information_4_5 = false;
  information_4_6 = false;
  information_10 = false;

  information_20 = false;
  information_21 = false;
  information_22 = false;
  information_23 = false;

  comment_risk_status = false;
  comment_compliance_status = false;
  comment_operation_status = false;
  comment_finance_status = false;
  comment_retail_status = false;

  outstanding_status = false;
  dataissue: any = [];

  businessStatus = false;
  personalStatus = false;
  housingStatus = false;
  otherStatus = false;
  nonCreditStatus = false;
  otherNonCreditStatus = false;

  validate = false;
  objective = null;

  exportStatus = true;

  remark = [
    { no: '1', detail: 'No คือ ไม่ใช่ประเด็นคงค้าง' },
    { no: '2', detail: 'Pending 1	คือ	ต้องแก้ไขก่อนประกาศใช้ผลิตภัณฑ์' },
    { no: '3', detail: 'Pending 2 คือ	สามารถใช้ผลิตภัณฑ์ได้ก่อน แล้วค่อยแก้ไขตามหลังได้' },
    { no: '4', detail: 'Pending 3	คือ	ไม่เกี่ยวข้องกับการประกาศใช้ผลิตภัณฑ์' },
    { no: '5', detail: 'Pending 4	คือ	ต้องแก้ไขให้แล้วเสร็จก่อนนำเสนอเรื่องต่อบอร์ด' },
  ];

  status_loading = true;

  printComponent() {
    // console.log('printComponent');

    // this.exportStatus = !this.exportStatus;
    //   setTimeout(() => {
    //      this.exportStatus = !this.exportStatus;
    //   }, 500);
    //   setTimeout(() => {
    //     this.exportStatus = !this.exportStatus;
    //  }, 700);

    // this.exportStatus = false;
    // setTimeout(() => {
    //   this.exportStatus = true;
    //   this.information_1 = true;
    //   this.information_2 = true;
    //   this.information_3 = true;
    //   this.information_4_1 = true;
    //   this.information_4_2 = true;
    //   this.information_4_3 = true;
    //   this.information_4_4 = true;
    //   this.information_4_5 = true;
    //   this.information_4_6 = true;
    //   this.information_10 = true;

    //   this.information_20 = true;
    //   this.information_21 = true;
    //   this.information_22 = true;
    //   this.information_23 = true;

    //   this.comment_risk_status = true;
    //   this.comment_compliance_status = true;
    //   this.comment_operation_status = true;
    //   this.comment_finance_status = true;
    //   this.comment_retail_status = true;
    // }, 500);
    // this.refresh_data();
    // this.exportStatus = false;
    // this.exportStatus = true;
    // setTimeout(() => {
    //   window.print();
    // }, 1500);
  }

  downloadFile() {
    this.status_loading = true;
    const contentType = '';
    const requestId = Number(localStorage.getItem('requestId'));

    console.log('requestId', requestId);
    this.service.postexportPDF(requestId).subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        this.status_loading = false;
        const datagetfile = res['data'];
        // const b64Data = datagetfile['data'];
        const filename = 'ExecutiveSummary.pdf';
        console.log('base64', datagetfile);
        const config = this.b64toBlob(datagetfile, contentType);
        saveAs(config, filename);
      }

    });
  }

  downloadFileWord() {
    this.status_loading = true;
    const contentType = '';
    const requestId = Number(localStorage.getItem('requestId'));

    console.log('requestId', requestId);
    this.service.postexportWord(requestId).subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        this.status_loading = false;
        const datagetfile = res['data'];
        // const b64Data = datagetfile['data'];
        const filename = 'ExecutiveSummary.docx';
        console.log('base64', datagetfile);
        const config = this.b64toBlob(datagetfile, contentType);
        saveAs(config, filename);
      }

    });
  }

  b64toBlob(datagetfile, contentType = '', sliceSize = 512) {
    // const convertbyte = atob(datagetfile);
    const byteCharacters = atob(datagetfile);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  ngOnInit() {
    this.status_loading = false;
    this.CheckPage();
    this.checkDocument();
    this.getIssueMasterFileReport();
  }
  back() {
    localStorage.removeItem('requestId');
    this.router.navigate(['dashboard1']);
  }


  CheckPage() {
    if (localStorage) {
      this.service.productGetGroup(localStorage.getItem('requestId')).subscribe(res => {
        // console.log('=> three =>', res);
        this.businessStatus = false;
        this.personalStatus = false;
        this.housingStatus = false;
        this.otherStatus = false;
        this.nonCreditStatus = false;
        this.otherNonCreditStatus = false;
        const productGet = res['data'];
        // console.log('productGet =>', productGet);
        productGet.product.forEach(ele => {
          if (ele === 'business_loan') {
            this.businessStatus = true;
          }
          if (ele === 'persenal_loan') {
            this.personalStatus = true;
          }
          if (ele === 'housing_loan') {
            this.housingStatus = true;
          }
          if (ele === 'other') {
            this.otherStatus = true;
          }
          if (ele === 'non_credit_product') {
            this.nonCreditStatus = true;
          }
          if (ele === 'other_non_creadit') {
            this.otherNonCreditStatus = true;
          }

          this.status_loading = false;
        });
      });
    }
  }

  async refresh_data() {
    this.status_loading = true;
    this.commentFinance.getData();
    this.commentRetail.getDetail();
    this.commentOperation.getDetail();
    this.commentCompliance.getDetail();
    this.commentRisk.getDetail();
    const result = await this.resolveAfter2Seconds();
  }

  resolveAfter2Seconds() {
    return new Promise(resolve => {
      setTimeout(() => {
        this.status_loading = false;
      }, 1500);
    });
  }

  checkDocument() {
    if (localStorage.getItem('requestId')) {
      const requestId = Number(localStorage.getItem('requestId'));
      this.productDefultFirstStep.getDefultFirstStep(requestId).subscribe(res => {
        if (res['data'] !== null) {
          const productDefaultStep1 = res['data'];
          if (productDefaultStep1.validate === true) {
            this.validate = false;
          } else {
            this.validate = true;
          }
          this.objective = productDefaultStep1.objective;
          // console.log('this.objective 00000=>', this.objective);
        }
      });
    }
  }

  // ----------------------------- ประเด็นคงค้าง / สิ่งที่ต้องดำเนินการ ---------------
  getIssueMasterFileReport() {
    if (localStorage.getItem('requestId')) {
      const requestId = Number(localStorage.getItem('requestId'));
      this.service.getIssueMasterFileReport(requestId).subscribe(res => {
        if (res['data']) {
          this.dataissue = res['data'];
        }
      });
    }
  }
  // ----------------------------- End ประเด็นคงค้าง / สิ่งที่ต้องดำเนินการ ---------------


}
