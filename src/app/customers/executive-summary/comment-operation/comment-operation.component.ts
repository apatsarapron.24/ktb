import { Component, OnInit } from '@angular/core';
import { OperationService } from '../../../services/comment-point/operation/operation.service';

@Component({
  selector: 'app-comment-operation',
  templateUrl: './comment-operation.component.html',
  styleUrls: ['./comment-operation.component.scss']
})
export class CommentOperationComponent implements OnInit {

  constructor(private Operation_Service: OperationService) { }

  header_table = ['กระบวนการย่อย', 'In/Out', 'IT Related'];
  convertDatePO: any = [];
  Res_PO: any;
  GetData_PO: any = {
    commentOperationProcess: [],
    processConsiders: [],
    commentOperation1: [],
    commentOperation2: [],
    question: [],
  };

  note = [
    'No	คือ	ไม่ใช่ประเด็นคงค้าง',
    'Pending 1	คือ	ต้องแก้ไขก่อนประกาศใช้ผลิตภัณฑ์',
    'Pending 2 คือ	สามารถใช้ผลิตภัณฑ์ได้ก่อน แล้วค่อยแก้ไขตามหลังได้ ',
    'Pending 3	คือ	ไม่เกี่ยวข้องกับการประกาศใช้ผลิตภัณฑ์',
    'Pending 4	คือ	ต้องแก้ไขให้แล้วเสร็จก่อนนำเสนอเรื่องต่อบอร์ด',
    ];

  ngOnInit() {
    this.getDetail();
  }

  getDetail() {
    this.Operation_Service.Get_PC_Operation(
      Number(localStorage.getItem('requestId'))
    ).subscribe((res) => {
      this.Res_PO = res;
      // this.GetData_PO = this.Res_PO.data;
      console.log('res PC', res);
      this.GetData_PO.processConsiders = this.Res_PO.data.processConsiders;
      this.GetData_PO.commentOperationProcess = this.Res_PO.data.commentOperationProcess;
      this.GetData_PO.commentOperation1 = this.Res_PO.data.commentOperation1;
      this.GetData_PO.commentOperation2 = this.Res_PO.data.commentOperation2;

      for (
        let index = 0;
        index < this.GetData_PO.commentOperationProcess.length;
        index++
      ) {
        this.GetData_PO.commentOperationProcess[
          index
        ].commentOperation.forEach((item, i) => {
          // console.log('dueDate:',   item.dueDate.substr(0, 10));
          const typePO = String(item.dueDate);
          const yearPO = Number(typePO.substr(0, 4)) + 543;
          const monthPO = typePO.substr(5, 2);
          const dayPO = typePO.substr(8, 2);

          const datePO = dayPO + '/' + monthPO + '/' + yearPO;
          // console.log('dueDate:',  date);
          this.convertDatePO.push({ Convert: datePO });
        });
      }
    });
  }

}
