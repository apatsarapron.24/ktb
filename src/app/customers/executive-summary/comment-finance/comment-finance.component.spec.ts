import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentFinanceComponent } from './comment-finance.component';

describe('CommentFinanceComponent', () => {
  let component: CommentFinanceComponent;
  let fixture: ComponentFixture<CommentFinanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentFinanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentFinanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
