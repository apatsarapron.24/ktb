import { Component, OnInit } from '@angular/core';
import { FinanceService } from '../../../services/comment-point/finance/finance.service';

@Component({
  selector: 'app-comment-finance',
  templateUrl: './comment-finance.component.html',
  styleUrls: ['./comment-finance.component.scss']
})
export class CommentFinanceComponent implements OnInit {

  constructor(private financeService: FinanceService) { }

  sendWorkStatus = false;

  note = [
    'No	คือ	ไม่ใช่ประเด็นคงค้าง',
    'Pending 1	คือ	ต้องแก้ไขก่อนประกาศใช้ผลิตภัณฑ์',
    'Pending 2 คือ	สามารถใช้ผลิตภัณฑ์ได้ก่อน แล้วค่อยแก้ไขตามหลังได้ ',
    'Pending 3	คือ	ไม่เกี่ยวข้องกับการประกาศใช้ผลิตภัณฑ์',
    'Pending 4	คือ	ต้องแก้ไขให้แล้วเสร็จก่อนนำเสนอเรื่องต่อบอร์ด',
    ];
  data = {
    deleteFinanceBudget: [],
    deleteFinancePerformance: [],
    deleteFinanceGovernance: [],
    template: [
      {
        tableHead: 'การบัญชี, งบประมาณและเบิกจ่าย',
        tableBody: [
          {
            id: null,
            topic: '1. มีการประมาณการต้นทุน และค่าใช้จ่ายในการดำเนินงานของทั้งโครงการ จากการออกผลิตภัณฑ์/บริการ',
            requestId: null,
            inspection: null,
            comment: null,
            reviews: null,
            poComment: null,
            pcComment: null,
            pcExtraComment: null,
            pcIssue: null
          },
          {
            topic: '2. ผลิตภัณฑ์ที่ออกหรือปรับปรุงใหม่มีการสร้างรหัสบัญชีใหม่หรือไม่',
            requestId: null,
            inspection: null,
            comment: null,
            reviews: null,
            poComment: null,
            pcComment: null,
            pcExtraComment: null,
            pcIssue: null
          },
          {
            topic: '3. มีการกำหนดวิธีการบันทึกบัญชีใหม่หรือไม่',
            requestId: null,
            inspection: null,
            comment: null,
            reviews: null,
            poComment: null,
            pcComment: null,
            pcExtraComment: null,
            pcIssue: null
          },
          {
            topic: '4. ผลิตภัณฑ์มีรายได้ที่เกี่ยวข้องกับภาษีหรือไม่',
            requestId: null,
            inspection: null,
            comment: null,
            reviews: null,
            poComment: null,
            pcComment: null,
            pcExtraComment: null,
            pcIssue: null
          },
          {
            topic: '5. มีการจัดทำมาตรฐานการรายงานทางการเงิน ฉบับที่ 9 (IFRS 9) หรือไม่',
            requestId: null,
            inspection: null,
            comment: null,
            reviews: null,
            poComment: null,
            pcComment: null,
            pcExtraComment: null,
            pcIssue: null
          },
        ]
      },
      {
        tableHead: 'การวัดผลงานภายหลังออกผลิตภัณฑ์',
        tableBody: [
          {
            id: null,
            topic: '1. มีการกำหนดแนวทาง/ตัวชี้วัดความสามารถในการทำกำไรของทั้งโครงการ จากออกผลิตภัณฑ์/บริการ',
            requestId: null,
            inspection: null,
            comment: null,
            reviews: null,
            poComment: null,
            pcComment: null,
            pcExtraComment: null,
            pcIssue: null
          },
          {
            id: null,
            topic: '2. มีการกำหนด Field Requirement สำหรับการออกผลิตภัณฑ์หรือการปรับปรุงผลิตภัณฑ์ ครบถ้วน (ตามเอกสารแนบ Field Requirment)',
            requestId: null,
            inspection: null,
            comment: null,
            reviews: null,
            poComment: null,
            pcComment: null,
            pcExtraComment: null,
            pcIssue: null
          },
          {
            id: null,
            // tslint:disable-next-line:max-line-length
            topic: '3. ผลิตภัณฑ์ที่ออกหรือปรับปรุงใหม่มีความพร้อมในการออกรายงาน เช่น ชื่อรายงาน, รูปแบบและเงื่อนไข, ความถี่ในการออกรายงาน,สิทธิ์ของผู้ใช้ข้อมูลหรือรายงาน และมีช่องทางในการเผยแพร่ครบถ้วนหรือไม่',
            requestId: null,
            inspection: null,
            comment: null,
            reviews: null,
            poComment: null,
            pcComment: null,
            pcExtraComment: null,
            pcIssue: null
          },
          {
            id: null,
            // tslint:disable-next-line:max-line-length
            topic: '4. มีการหารือและกำหนดแนวทางในการจัดสรรรายได้และค่าใช้จ่าย (Revenue & Cost Allocation) พร้อมทั้งระบุ product owner ให้ชัดเจน',
            requestId: null,
            inspection: null,
            comment: null,
            reviews: null,
            poComment: null,
            pcComment: null,
            pcExtraComment: null,
            pcIssue: null
          },
        ]
      },
      {
        tableHead: 'Data Governance (DG)',
        tableBody: [
          {
            id: null,
            // tslint:disable-next-line:max-line-length
            topic: '1. ผลิตภัณฑ์ที่ออกหรือปรับปรุงใหม่มีผังเส้นทางเดินของข้อมูล (Data Flow) และผังการทำงานของระบบงาน (Application Flow) และมีกำหนดแนวทางการจัดเก็บข้อมูลสำรอง (Back up Process)',
            requestId: null,
            inspection: null,
            comment: null,
            reviews: null,
            poComment: null,
            pcComment: null,
            pcExtraComment: null,
            pcIssue: null
          },
          {
            id: null,
            // tslint:disable-next-line:max-line-length
            topic: '2. ผลิตภัณฑ์ที่ออกหรือปรับปรุงใหม่มีแนวทางการรักษาความปลอดภัยของระบบ (security) และแนวทางรายงานเหตุการณ์ผิดปกติของระบบ (Incident Response)',
            requestId: null,
            inspection: null,
            comment: null,
            reviews: null,
            poComment: null,
            pcComment: null,
            pcExtraComment: null,
            pcIssue: null
          },
          {
            id: null,
            // tslint:disable-next-line:max-line-length
            topic: '3. ผลิตภัณฑ์ที่ออกหรือปรับปรุงใหม่มีคู่มือปฏิบัติงานของระบบงาน รวมทั้งมีรายชื่อ Fields ในระบบพร้อมคำอธิบายที่เกี่ยวข้อง  (Data Dictionary)',
            requestId: null,
            inspection: null,
            comment: null,
            reviews: null,
            poComment: null,
            pcComment: null,
            pcExtraComment: null,
            pcIssue: null
          },
          {
            id: null,
            // tslint:disable-next-line:max-line-length
            topic: '4. ผลิตภัณฑ์ที่ออกหรือปรับปรุงใหม่มีความพร้อมในการส่งข้อมูลเข้า BDW (Process Flow)',
            requestId: null,
            inspection: null,
            comment: null,
            reviews: null,
            poComment: null,
            pcComment: null,
            pcExtraComment: null,
            pcIssue: null
          },
          {
            id: null,
            // tslint:disable-next-line:max-line-length
            topic: '5. ผลิตภัณฑ์ที่ออกหรือปรับปรุงใหม่ต้องมีการรายงานต่อ ธปท. หรือไม่ (หากมี มีขั้นตอนอะไรบ้าง)',
            requestId: null,
            inspection: null,
            comment: null,
            reviews: null,
            poComment: null,
            pcComment: null,
            pcExtraComment: null,
            pcIssue: null
          },
        ]
      },
    ],
    comments: [
      {
        id: 1,
        question: {
          id: 1,
          date: ' 02/06/2563  เวลา 13:00:54 น.',
          by: 'พนักงานดูแลหน่วยงานบริหารความเสี่ยง',
          detail: 'detail text',
          attachments: {
            file: [],
            fileDelete: []
          },
          edit_status: true, // เช็คว่าเเก้ไขได้หรอไม่
          edit_comment_status: false, // เช็คสถานะการเเก้ไขความคิดเห็น
          status_submit: false // เช็คว่าเคยส่งงานหรือยัง
        },
        answers: {
          id: 1,
          date: ' 02/06/2563  เวลา 13:00:54 น.',
          by: 'พนักงานดูแลหน่วยงานบริหารความเสี่ยง',
          detail: 'detail text',
          attachments: {
            file: [],
            fileDelete: []
          },
          edit_status: true,
          edit_comment_status: false,
          status_submit: false
        }
      },
      {
        id: 2,
        question: {
          id: 2,
          date: ' 02/06/2563  เวลา 13:00:54 น.',
          by: 'พนักงานดูแลหน่วยงานบริหารความเสี่ยง',
          detail: 'detail text',
          attachments: {
            file: [],
            fileDelete: []
          },
          edit_status: true,
          edit_comment_status: false,
          status_submit: false
        },
        answers: {
          id: 2,
          date: ' 02/06/2563  เวลา 13:00:54 น.',
          by: 'พนักงานดูแลหน่วยงานบริหารความเสี่ยง',
          detail: 'detail text',
          attachments: {
            file: [],
            fileDelete: []
          },
          edit_status: true,
          edit_comment_status: false,
          status_submit: false
        }
      },
    ]
  };
  ngOnInit() {
   this.getData();
  }

  checkDetail_PO() {
    let checkDtail = true;
    this.data.template.forEach( ele => {
      ele.tableBody.forEach(eles => {
        if (eles.poComment === null || eles.poComment === '') {
          checkDtail = false;
        }
      });
    });
    this.sendWorkStatus = checkDtail;
    console.log('this.sendWorkStatus 777', this.sendWorkStatus);
  }

  getData() {
    console.log('get data finance');
    this.financeService.getFinance(localStorage.getItem('requestId'),  'PO').subscribe(res => {
      console.log('res', res);
      const dataRes = res['data'];
      this.data.template[0].tableBody = dataRes.financeBudget;
      this.data.template[1].tableBody = dataRes.financePerformance;
      this.data.template[2].tableBody = dataRes.financeGovernance;
      this.checkDetail_PO();
    });
  }

}
