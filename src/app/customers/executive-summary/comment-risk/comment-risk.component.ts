import { Component, OnInit } from '@angular/core';
import { RiskService } from '../../../services/comment-point/risk/risk.service';

@Component({
  selector: 'app-comment-risk',
  templateUrl: './comment-risk.component.html',
  styleUrls: ['./comment-risk.component.scss']
})
export class CommentRiskComponent implements OnInit {

  constructor(private riskService: RiskService) { }

  header_table = ['กระบวนการย่อย', 'In/Out', 'IT Related'];
  ResData: any;
  FormData: any;
  validate: false;
  convertDatePC: any = [];

  ngOnInit() {
    this.getDetail();
  }

  getDetail() {
    this.riskService.getRisk_PC(localStorage.getItem('requestId')).subscribe((res) => {
      this.ResData = res;
      this.FormData = this.ResData.data;
      for (let index = 0; index < this.FormData.length; index++) {
        if (this.FormData[index].validate === true) {
          this.validate = this.FormData[index].validate;
        }
           // tslint:disable-next-line: no-shadowed-variable
           const Data = this.FormData[index].readiness;
           console.log('date:', Data);
           if ( Data !== null) {
             for (let i = 0; i < Data.length; i++) {
               const Readeiness = Data[i].detail;
               Readeiness.forEach((item, _i) => {
               // console.log('dueDate:',   item.dueDate.substr(0, 10));
               const type = String(item.dueDate);
               const year = Number(type.substr(0, 4)) + 543;
               const month = type.substr(5, 2);
               const day = type.substr(8, 2);
               const date = day + '/' + month + '/' + year;
               this.convertDatePC.push({ Convert: date });
             });
           }
           }
      }
      console.log('res PC:', this.FormData);
    });
  }
}
