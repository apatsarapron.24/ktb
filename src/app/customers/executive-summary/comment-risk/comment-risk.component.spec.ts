import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentRiskComponent } from './comment-risk.component';

describe('CommentRiskComponent', () => {
  let component: CommentRiskComponent;
  let fixture: ComponentFixture<CommentRiskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentRiskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentRiskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
