import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentComplianceComponent } from './comment-compliance.component';

describe('CommentComplianceComponent', () => {
  let component: CommentComplianceComponent;
  let fixture: ComponentFixture<CommentComplianceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentComplianceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentComplianceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
