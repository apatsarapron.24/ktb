import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem, CdkDragExit, CdkDragEnter, CdkDragStart } from '@angular/cdk/drag-drop';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ComplianceService } from './../../../services/comment-point/compliance/compliance.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
    selector: 'app-comment-compliance',
    templateUrl: './comment-compliance.component.html',
    styleUrls: ['./comment-compliance.component.scss']
})
export class CommentComplianceComponent implements OnInit {

    data = {
        'validate': false,
        'complianceItems': [
            {
                'id': null,
                'complianceId': null,
                'unit': '',
                'bookNumber': '',
                'topic': '',
                'allow': '',
                'other': '',
                'createdAt': '',
                'updatedAt': '',
                'deletedAt': ''
            },
        ],
        'account': [
            {
                'id': null,
                'regulation': '',
                'topic': [
                    {
                        'id': null,
                        'topic': '',
                        'detail': [
                            {
                                'id': null,
                                'detail': '',
                                'comply': null,
                                'review': null,
                                'buCommentId': null,
                                'poComment': '',
                                'pcComment': '',
                                'pcExtraComment': '',
                                'pcIssue': ''
                            }
                        ]
                    }
                ]
            },
        ],
        'department1': [
            {
                'id': null,
                'department': '',
                'bookNumber': '',
                'topic': [
                    {
                        'id': null,
                        'topic': '',
                        'detail': [
                            {
                                'id': null,
                                'detail': '',
                                'comply': null,
                                'review': null,
                                'buCommentId': null,
                                'poComment': '',
                                'pcComment': '',
                                'pcExtraComment': '',
                                'pcIssue': 'hjk'
                            }
                        ]
                    }
                ]
            },
        ],
        'department2': [
            {
                'id': null,
                'department': '',
                'bookNumber': '',
                'topic': [
                    {
                        'id': null,
                        'topic': '',
                        'detail': [
                            {
                                'id': null,
                                'detail': '',
                                'comply': null,
                                'review': null,
                                'buCommentId': null,
                                'poComment': '',
                                'pcComment': '',
                                'pcExtraComment': '',
                                'pcIssue': ''
                            }
                        ]
                    }
                ]
            },
        ],
        'rule': [
            {
                'id': null,
                'department': '',
                'bookNumber': '',
                'topic': [
                    {
                        'id': null,
                        'topic': '',
                        'detail': [
                            {
                                'id': null,
                                'detail': '',
                                'comply': null,
                                'review': null,
                                'buCommentId': null,
                                'poComment': '',
                                'pcComment': '',
                                'pcExtraComment': '',
                                'pcIssue': ''
                            },
                        ]
                    }
                ]
            },
        ],
        'law': [
            {
                'id': null,
                'department': '',
                'bookNumber': '',
                'topic': [
                    {
                        'id': null,
                        'topic': '',
                        'detail': [
                            {
                                'id': null,
                                'detail': '',
                                'comply': null,
                                'review': null,
                                'buCommentId': null,
                                'poComment': '',
                                'pcComment': '',
                                'pcExtraComment': '',
                                'pcIssue': ''
                            },
                        ]
                    }
                ]
            }
        ]
    };
    results: any;
    constructor(public complianceService: ComplianceService) { }

    ngOnInit() {
        this.getDetail();
    }

    getDetail() {
        this.complianceService.getPCCompliance(localStorage.getItem('requestId')).subscribe(res => {
            this.results = res;
            console.log('getDetailฬฬฬฬ', this.results);
            if (this.results.data !== null || this.results.data !== undefined) {
                this.data = this.results.data;
            } else {
                this.data = this.data;
            }
        });
    }

}
