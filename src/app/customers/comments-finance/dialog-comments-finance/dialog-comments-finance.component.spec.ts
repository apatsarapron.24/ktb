import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogCommentsFinanceComponent } from './dialog-comments-finance.component';

describe('DialogCommentsFinanceComponent', () => {
  let component: DialogCommentsFinanceComponent;
  let fixture: ComponentFixture<DialogCommentsFinanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogCommentsFinanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogCommentsFinanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
