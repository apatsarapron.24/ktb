import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { CommentsFinanceService } from '../../../services/customers/comments-finance.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

export interface DialogData {
  dialogData: any;
  status: any;
}

@Component({
  selector: 'app-dialog-comments-finance',
  templateUrl: './dialog-comments-finance.component.html',
  styleUrls: ['./dialog-comments-finance.component.scss']
})
export class DialogCommentsFinanceComponent implements OnInit {

  constructor(
    private router: Router,
    private commentsFinanceService: CommentsFinanceService,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<DialogCommentsFinanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.dialogRef.disableClose = true;
  }

  close() {
    this.data.status = false;
    this.dialogRef.close(this.data);
  }

  back() {
    this.data.status = false;
    this.dialogRef.close(this.data);
  }

  save() {
    // this.data.status = true;
    // this.dialogRef.close(this.data);
    this.commentsFinanceService.getPortfolioList(this.data.dialogData).subscribe(res => {
      console.log(res);
      if (res['status'] === 'success') {
        this.data.status = true;
        this.dialogRef.close(this.data);
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'คุณได้บันทึกข้อมูลเรียบร้อยแล้ว',
          showConfirmButton: false,
        });
      } else {
        this.data.status = false;
        this.dialogRef.close(this.data);
        Swal.fire({
          position: 'center',
          icon: 'error',
          title: 'เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้',
          showConfirmButton: false,
        });
      }
    },
    error => {
      console.log(error.error.message);
      this.data.status = false;
      this.dialogRef.close(this.data);
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'เกิดข้อผิดพลาดไม่สามารถ update ข้อมูลได้',
        showConfirmButton: false,
      });
    });
  }

  ngOnInit() {
  }

}
