import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommentsFinanceComponent } from './comments-finance.component';
const routes: Routes = [
  {
    path: '',
    component: CommentsFinanceComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommentsFinanceRoutingModule { }
