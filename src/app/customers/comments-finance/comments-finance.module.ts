import { FormsModule } from '@angular/forms';
import { CommentsFinanceComponent } from './comments-finance.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommentsFinanceRoutingModule } from './comments-finance-routing.module';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { MatDialogModule } from '@angular/material';
import { DialogCommentsFinanceComponent } from './dialog-comments-finance/dialog-comments-finance.component';
import {TextareaAutoresizeDirectiveModule} from '../../textarea-autoresize.directive/textarea-autoresize.directive.module';

@NgModule({
  declarations: [CommentsFinanceComponent, DialogCommentsFinanceComponent],
  imports: [
    CommonModule,
    CommentsFinanceRoutingModule,
    FormsModule,
    DragDropModule,
    MatDialogModule,
    TextareaAutoresizeDirectiveModule
  ],
  entryComponents: [DialogCommentsFinanceComponent]
})
export class CommentsFinanceModule { }
