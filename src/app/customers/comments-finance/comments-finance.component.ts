import { async } from '@angular/core/testing';
import { Component, OnInit } from '@angular/core';
import { count } from 'rxjs/operators';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { CommentsFinanceService } from '../../services/customers/comments-finance.service';
import { RouterLink, Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogCommentsFinanceComponent } from './dialog-comments-finance/dialog-comments-finance.component';
import Swal from 'sweetalert2';



export interface Data {
  accounting: Array<string>;
  product: Array<string>;
  governance: Array<string>;
}


const data: Data = {
  accounting: [],
  product: [],
  governance: []
};

@Component({
  selector: 'app-comments-finance',
  templateUrl: './comments-finance.component.html',
  styleUrls: ['./comments-finance.component.scss']
})

export class CommentsFinanceComponent implements OnInit {
  dataFinancialManagement = {
    budget: [
      // {
      //     id: 1,
      //     topic: 'topic budget 2'
      // },
      // {
      //     id: 9,
      //     topic: 'topic budget 1111'
      // }
    ],
    budgetDelete: [],
    performance: [
      // {
      //     id: 1,
      //     topic: 'topic performance 2'
      // },
      // {
      //     id: 9,
      //     topic: 'topic performance 11111'
      // }
    ],
    performanceDelete: [],
    governance: [
      // {
      //     id: 1,
      //     topic: 'topic governance 1111'
      // },
      // {
      //     id: 8,
      //     topic: 'topic governance 2'
      // }
    ],
    governanceDelete: [],
  };

  sendData: Data;
  savemode = false;
  showAccounting = false;
  showEvaluation = false;
  showGovernance = false;
  // dataAccounting: Data;
  // dataFinancialManagement: any;

  // dialog
  viewStatus = null;

  checkValue() {

    if (this.dataFinancialManagement['budget'].length > 0) {
      this.showAccounting = true;
    } else { this.showAccounting = false; }
    if (this.dataFinancialManagement['performance'].length > 0) {
      this.showEvaluation = true;
    } else { this.showEvaluation = false; }
    if (this.dataFinancialManagement['governance'].length > 0) {
      this.showGovernance = true;
    } else { this.showGovernance = false; }
    this.checkValueInput();
  }
  checkValueInput() {
    let check = true;
    this.dataFinancialManagement['budget'].forEach(item => {
      if (item.topic.length === 0) {
        check = false;
      }
    });
    this.dataFinancialManagement['performance'].forEach(item => {
      if (item.topic.length === 0) {
        check = false;
      }
    });
    this.dataFinancialManagement['governance'].forEach(item => {
      if (item.topic.length === 0) {
        check = false;
      }
    });
    if (check) {
      this.savemode = true;
    } else {
      this.savemode = false;
    }
  }
  saveDataAll() {
    const dialogRef = this.dialog.open(DialogCommentsFinanceComponent,
      { disableClose: true, data: { dialogData: this.dataFinancialManagement, status: this.viewStatus } });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      this.viewStatus = result.status;
    });
  }

  backpage() {
    localStorage.setItem('requestId', '');
    this.router.navigate(['dashboard1']);
  }

  // ----------- CUD Accounting ------------
  addAccounting() {
    const addData = {
      id: null,
      topic: ''
    };
    this.dataFinancialManagement.budget.push(addData);
    this.checkValue();
  }
  deleteAccounting(index, id) {
    if (id !== null) {
      this.dataFinancialManagement.budgetDelete.push(id);
    }
    this.dataFinancialManagement.budget.splice(index, 1);
    this.checkValue();
  }


  AccountingDrop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.dataFinancialManagement.budget, event.previousIndex, event.currentIndex);
  }

  // --------------- End CDL Accounting ----------------------
  // ----------- CUD Accounting ------------
  addEvaluation() {
    const addData = {
      id: null,
      topic: ''
    };
    this.dataFinancialManagement.performance.push(addData);
    this.checkValue();
  }
  deleteEvaluation(index, id) {
    if (id !== null) {
      this.dataFinancialManagement.performanceDelete.push(id);
    }
    this.dataFinancialManagement.performance.splice(index, 1);
    this.checkValue();
  }

  EvaluationDrop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.dataFinancialManagement.performance, event.previousIndex, event.currentIndex);
  }
  // --------------- End CDL Accounting ----------------------
  // ----------- CUD Governance ------------
  addGovernance() {
    const addData = {
      id: null,
      topic: ''
    };
    this.dataFinancialManagement.governance.push(addData);
    this.checkValue();
  }
  deleteGovernance(index, id) {
    if (id !== null) {
      this.dataFinancialManagement.governanceDelete.push(id);
    }
    this.dataFinancialManagement.governance.splice(index, 1);
    this.checkValue();
  }

  GovernanceDrop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.dataFinancialManagement.governance, event.previousIndex, event.currentIndex);
  }


  // ------------------- get api -------------------------
  financialManagement() {
    // this.dataFinancialManagement =  this.data;
    // console.log('this.data', this.data);
    this.FinancialManagement.getFinancialManagement().subscribe(res => {
      this.dataFinancialManagement = res['data'];
      // console.log('this.dataFinancialManagement', this.dataFinancialManagement);
      // set Delete item
      this.dataFinancialManagement.budgetDelete = [];
      this.dataFinancialManagement.performanceDelete = [];
      this.dataFinancialManagement.governanceDelete = [];

      // set viewStatus
      if (this.dataFinancialManagement.budget.length > 0
        && this.dataFinancialManagement.governance.length > 0
        && this.dataFinancialManagement.performance.length > 0) {
        this.viewStatus = true;
      } else {
        this.viewStatus = false;
      }

      this.checkValue();
    }, error => {
      console.log('error', error);
      this.checkValue();
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'เกิดข้อผิดพลาดไม่สามารถดคงข้อมูลจากฐานข้อมูลได้',
        showConfirmButton: false,
      });
    });
  }

  constructor(private FinancialManagement: CommentsFinanceService,
    public dialog: MatDialog,
    private router: Router,
  ) { }

  async ngOnInit() {
    this.financialManagement();
  }

  autoGrowTextZone(e) {
    e.target.style.height = 'auto';
    // e.target.style.overflow = 'hidden';
    e.target.style.height = (e.target.scrollHeight + 0) + 'px';
  }
}
