import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentsFinanceComponent } from './comments-finance.component';

describe('CommentsFinanceComponent', () => {
  let component: CommentsFinanceComponent;
  let fixture: ComponentFixture<CommentsFinanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentsFinanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentsFinanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
