import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import * as moment from 'moment';
import {
  AngularMyDatePickerDirective,
  IAngularMyDpOptions,
  IMyDateModel,
} from 'angular-mydatepicker';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { AlertDeleteComponent } from './alert-delete/alert-delete.component';
import { RouterLink, Router, NavigationEnd } from '@angular/router';
import { SelectionModel } from '@angular/cdk/collections';
import { AgendaManageService } from '../../services/agenda/agenda-manage/agenda-manage.service';
import { MoveAgendaManageDialogComponent } from './move-agenda-manage-dialog/move-agenda-manage-dialog.component';
import { FailAddagendaDialogComponent } from './fail-addagenda-dialog/fail-addagenda-dialog.component';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { DateComponent } from '../datepicker/date/date.component';
import { saveAs } from 'file-saver';
import Swal from 'sweetalert2';

export interface FileList {
  fileName: string;
  fileSize: string;
  base64File: string;
}

const File_data: FileList[] = [];

@Component({
  selector: 'app-agenda-manage',
  templateUrl: './agenda-manage.component.html',
  styleUrls: ['./agenda-manage.component.scss'],
})
export class AgendaManageComponent implements OnInit {
  header = [
    {
      name: 'year',
      value: 'ประจำปี',
    },
    {
      name: 'month',
      value: 'เดือน',
    },
    {
      name: 'meetingDate',
      value: 'วันที่ประชุม',
    },
    {
      name: 'meetingTime',
      value: 'เวลา',
    },
    {
      name: 'countingInteger',
      value: ' ครั้งที่',
    },
  ];
  sub_header_1 = [
    {
      name: 'consider',
      value: 'เพื่อพิจารณา',
    },
    {
      name: 'inform',
      value: 'เพื่อทราบ',
    },
    {
      name: 'considerAndInform',
      value: 'เพื่อพิจารณาและเพื่อทราบ',
    },
  ];

  start_hours: any = [
    '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21',
  ];
  start_minutes: any = [
    '00', '05', '10', '15', '20', '25', '30', '35', '40', '45', '50', '55',
  ];
  year: any = [
    '2559', '2560', '2561', '2562', '2563', '2564', '2565', '2566', '2567', '2568',
  ];
  month: any = [
    'มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฏาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
  ];
  term: any = [
    'วาระสืบเนื่องจากครั้งก่อน', 'วาระปกติ', 'วาระเพิ่มเติม',
  ];
  presentation: any = [
    'เพื่อพิจารณา', 'เพื่อทราบ', 'เพื่อพิจารณาและเพื่อทราบ',
  ];

  displayedColumns_file: string[] = ['fileName', 'fileSize', 'delete'];
  dataSource_file: MatTableDataSource<FileList>;
  count = true;
  myDpOptions: IAngularMyDpOptions = {
    dateRange: false,
    dateFormat: 'dd/mm/yyyy',
    // other options are here...
    stylesData: {
      selector: 'dp',
      styles: `
           .dp .top:auto
           .dp .myDpIconLeftArrow,
           .dp .myDpIconRightArrow,
           .dp .myDpHeaderBtn {
               color: #3855c1;
            }
           .dp .myDpHeaderBtn:focus,
           .dp .myDpMonthLabel:focus,
           .dp .myDpYearLabel:focus {
               color: #3855c1;
            }
           .dp .myDpDaycell:focus,
           .dp .myDpMonthcell:focus,
           .dp .myDpYearcell:focus {
               box-shadow: inset 0 0 0 1px #66afe9;
            }
           .dp .myDpSelector:focus {
               border: 1px solid #ADD8E6;
            }
           .dp .myDpSelectorArrow:focus:before {
               border-bottom-color: #ADD8E6;
            }
           .dp .myDpCurrMonth,
           .dp .myDpMonthcell,
           .dp .myDpYearcell {
               color: #00a6e6;
            }
           .dp .myDpDaycellWeekNbr {
               color: #3855c1;
            }
           .dp .myDpPrevMonth,
           .dp .myDpNextMonth {
               color: #0098D2;
            }
           .dp .myDpWeekDayTitle {
               background-color: #0098D2;
               color: #ffffff;
            }
           .dp .myDpHeaderBtnEnabled:hover,
           .dp .myDpMonthLabel:hover,
           .dp .myDpYearLabel:hover {
               color:#0098D2;
            }
           .dp .myDpMarkCurrDay,
           .dp .myDpMarkCurrMonth,
           .dp .myDpMarkCurrYear {
               border-bottom: 2px solid #3855c1;
            }
           .dp .myDpDisabled {
               color: #999;
            }
           .dp .myDpHighlight {
               color: #74CBEC;
            }
           .dp .myDpTableSingleDay:hover,
           .dp .myDpTableSingleMonth:hover,
           .dp .myDpTableSingleYear:hover {
               background-color: #add8e6;
               color: #0098D2;
            }
           .dp .myDpRangeColor {
               background-color: #dbeaff;
            }
           .dp .myDpSelectedDay,
           .dp .myDpSelectedMonth,
           .dp .myDpSelectedYear {
               background-color: #0098D2;
               color: #ffffff;
            }
            `,
    },
  };
  // private model: Object = {};
  model: any = null;
  @ViewChild('dp') mydp: AngularMyDatePickerDirective;

  @ViewChild(DateComponent) Date: DateComponent;

  myDpOptions_search: IAngularMyDpOptions = {
    dateRange: false,
    dateFormat: 'dd/mm/yyyy',
    // other options are here...
    stylesData: {
      selector: 'dp1',
      styles: `
           .dp1 .top:auto
           .dp1 .myDpIconLeftArrow,
           .dp1 .myDpIconRightArrow,
           .dp1 .myDpHeaderBtn {
               color: #3855c1;
            }
           .dp1 .myDpHeaderBtn:focus,
           .dp1 .myDpMonthLabel:focus,
           .dp1 .myDpYearLabel:focus {
               color: #3855c1;
            }
           .dp1 .myDpDaycell:focus,
           .dp1 .myDpMonthcell:focus,
           .dp1 .myDpYearcell:focus {
               box-shadow: inset 0 0 0 1px #66afe9;
            }
           .dp1 .myDpSelector:focus {
               border: 1px solid #ADD8E6;
            }
           .dp1 .myDpSelectorArrow:focus:before {
               border-bottom-color: #ADD8E6;
            }
           .dp1 .myDpCurrMonth,
           .dp1 .myDpMonthcell,
           .dp1 .myDpYearcell {
               color: #00a6e6;
            }
           .dp1 .myDpDaycellWeekNbr {
               color: #3855c1;
            }
           .dp1 .myDpPrevMonth,
           .dp1 .myDpNextMonth {
               color: #0098D2;
            }
           .dp1 .myDpWeekDayTitle {
               background-color: #0098D2;
               color: #ffffff;
            }
           .dp1 .myDpHeaderBtnEnabled:hover,
           .dp1 .myDpMonthLabel:hover,
           .dp1 .myDpYearLabel:hover {
               color:#0098D2;
            }
           .dp1 .myDpMarkCurrDay,
           .dp1 .myDpMarkCurrMonth,
           .dp1 .myDpMarkCurrYear {
               border-bottom: 2px solid #3855c1;
            }
           .dp1 .myDpDisabled {
               color: #999;
            }
           .dp1 .myDpHighlight {
               color: #74CBEC;
            }
           .dp1 .myDpTableSingleDay:hover,
           .dp1 .myDpTableSingleMonth:hover,
           .dp1 .myDpTableSingleYear:hover {
               background-color: #add8e6;
               color: #0098D2;
            }
           .dp1 .myDpRangeColor {
               background-color: #dbeaff;
            }
           .dp1 .myDpSelectedDay,
           .dp1 .myDpSelectedMonth,
           .dp1 .myDpSelectedYear {
               background-color: #0098D2;
               color: #ffffff;
            }
            `,
    },
  };
  // private model: Object = {};
  model_search: any = null;
  @ViewChild('dp1') mydp1: AngularMyDatePickerDirective;

  files: any = {
    file: [],
    fileDelete: [],
  };
  delete_row: any;
  saveDraftstatus = null;
  modedeleteagenda = false;
  selection = new SelectionModel<any>(true, []);
  totalselected = 0;
  datameetinglist: any;
  selectdatetime = null;
  dropdownmeetinglist = null;
  dropdownmeetinglistSelect = null;
  _meetingid: number;
  meetingtargetid: number;

  timestart_hours = '00';
  timestart_minute = '00';
  timeend_hours = '00';
  timeend_minute = '00';
  timestart = '';
  timeend = '';
  agendadate = '';
  sendfile = '';

  yearsearch = '';
  monthsearch = '';
  termsearch = '';
  presentationsearch = '';
  nosearch = '';
  productsearch = '';
  fileToUpload: any = [];
  failaddagenda: any;

  currentPage = 1;
  pageSize = 10;
  length = 0;
  returnedArray: any = [];
  startItem: any = 0;
  endItem: any = 10;
  searchData: any;
  showless = 'true';

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private agendamanageService: AgendaManageService
  ) {
    this.dataSource_file = new MatTableDataSource(File_data);
  }

  ngOnInit() {
    this.saveDraftstatus = null;
    this.getMeetingList();
    this.getDropdownAllMeetingList();
  }

  getMeetingList() {
    this.agendamanageService.getMeetingList('', '', '', '', '', '', '').subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        this.datameetinglist = res['data'];
        this.searchData = res['data'];
        this.year = [];
        for (let index = 0; index < this.searchData.length; index++) {

          if (this.year.includes(this.searchData[index].year) === false) {
            this.year.push(this.searchData[index].year);
          }
        }

        this.length = this.datameetinglist.length;
        this.returnedArray = this.datameetinglist;
        this.returnedArray = this.datameetinglist.slice(0, 10);
      }
    });
  }
  pageChanged(event: any) {
    this.currentPage = 1;
    this.startItem = (event.page - 1) * event.itemsPerPage;

    this.endItem = (event.page * event.itemsPerPage);

    if (this.endItem > this.datameetinglist.length) {
      this.endItem = this.datameetinglist.length;
    }
    this.returnedArray = this.datameetinglist.slice(this.startItem, this.endItem);

  }

  limitOnChange() {
    // this.currentPage  = pageNo;
    if (this.datameetinglist.length === 0) {
      this.startItem = -1;
      this.endItem = this.datameetinglist.length;
    } else if (this.datameetinglist.length !== 0 && this.datameetinglist.length < this.pageSize) {
      this.startItem = 0;
      this.endItem = this.datameetinglist.length;

    } else {
      this.startItem = (this.currentPage - 1) * this.pageSize;
      this.endItem = (this.currentPage * this.pageSize);
    }

    this.returnedArray = this.datameetinglist.slice(0, this.pageSize);
    // this.pageChanged(event);
  }
  searchMeeting() {
    // let date = 0;
    // let dateFormated = '';

    if (this.model_search !== null) {
      // date = this.model_search.singleDate.date.year + 543;
      // // tslint:disable-next-line: max-line-length
      // dateFormated = ('0' + (this.model_search.singleDate.date.day)).slice(-2) + '/' +
      //   ('0' + (this.model_search.singleDate.date.month)).slice(-2) + '/' + date.toString();
      this.model_search = this.model_search;
    }

    const search = {
      'year': this.yearsearch,
      'month': this.monthsearch,
      'meetingDate': this.model_search,
      'term': this.termsearch,
      'presentation': this.presentationsearch,
      'no': this.nosearch,
      'product': this.productsearch
    };
    // this.agendamanageService.searchMeetingList(search).subscribe(res => {
    //   console.log('search:', res);
    //   if (res['status'] === 'success' && res['data']) {
    //     this.datameetinglist = res['data'];
    //     console.log(res['data']);
    //     this.limitOnChange();

    //     if (this.datameetinglist.length === 0) {
    //       this.startItem = -1;
    //       this.endItem = this.datameetinglist.length ;
    //     } else if (this.datameetinglist.length !== 0 && this.datameetinglist.length < this.pageSize) {
    //       this.startItem = 0;
    //       this.endItem = this.datameetinglist.length ;

    //     } else {
    //       this.startItem = (this.currentPage - 1) * this.pageSize;
    //       this.endItem = (this.currentPage  * this.pageSize) ;
    //     }
    //     this.length = this.datameetinglist.length ;
    //   }
    // });
    console.log('search:', search);
  }
  getDropdownAllMeetingList() {
    this.agendamanageService.getDropdownAllMeetingList().subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        this.dropdownmeetinglistSelect = res['data'];
        this.dropdownmeetinglist = this.dropdownmeetinglistSelect.dropdownMeettingList;
      }
    });
  }


  time() {
    // let _year = 0;
    this.timestart = this.timestart_hours + ':' + this.timestart_minute;
    this.timeend = this.timeend_hours + ':' + this.timeend_minute;
    // // this.agendadate = this.model.singleDate.formatted;
    // _year = this.model.singleDate.date.year + 543;
    // this.agendadate = this.model.singleDate.date.day + '/' + this.model.singleDate.date.month + '/' + _year.toString();
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      const file = event.target.files;

      for (let index = 0; index < file.length; index++) {
        // const element = file[index];
        this.fileToUpload.push(file[index]);

      }
      // this.fileToUpload = file;
      this.sendfile = 'true';

      for (let i = 0; i < filesAmount; i++) {
        if (
          // file[i].type === 'image/svg+xml' ||
          file[i].type === 'image/jpeg' ||
          // file[i].type === 'image/raw' ||
          // file[i].type === 'image/svg' ||
          // file[i].type === 'image/tif' ||
          // file[i].type === 'image/gif' ||
          file[i].type === 'image/jpg' ||
          file[i].type === 'image/png' ||
          file[i].type === 'application/doc' ||
          file[i].type === 'application/pdf' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
          file[i].type === 'application/pptx' ||
          file[i].type === 'application/docx' ||
          (file[i].type === 'application/ppt' && file[i].size)
        ) {
          if (file[i].size <= 5000000) {
            // tslint:disable-next-line: prefer-const
            let select = {
              id: '',
              file: file[i],
            };
            // console.log('file:', select);
            this.multiple_file(select, i);
          }
        } else {
          alert('File Not Support');
        }
      }
    }
  }

  multiple_file(file, index) {
    const reader = new FileReader();
    if (file.file) {
      reader.readAsDataURL(file.file);
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');
        // this.count =  this.files.file.length;
        if (this.files.file.length <= 4) {
          this.files.file.push({
            id: file.id,
            fileName: file.file.name,
            fileSize: file.file.size,
            base64File: splitFile[1],
          });
          // console.log('this.urls',   this.files.file);
          this.dataSource_file = new MatTableDataSource(this.files.file);
        }
      };
    }
  }

  delete_mutifile(data: any, index: any) {
    console.log('data:', data, 'index::', index);
    this.dataSource_file.data.splice(index, 1);
    console.log('urls:', this.files);

    this.fileToUpload.splice(index, 1);

    // this.urls.splice(index, 1);
    // this.muti_file.splice(index, 1);
    if (data.id !== '') {
      this.files.fileDelete.push(data.id);
    }
    this.dataSource_file = new MatTableDataSource(this.dataSource_file.data);
    this.files.file = this.dataSource_file.data;
    console.log('form dee:', this.files);
  }

  addAgenda() {
    // console.log('timestart', this.timestart);
    // console.log('timeend', this.timeend);
    // console.log('agendadate', this.agendadate);
    if (this.timestart !== null && this.timeend !== '' && this.agendadate !== '') {
      const data = {
        'meetingDate': this.agendadate,
        'meetingTimeStart': this.timestart,
        'meetingTimeEnd': this.timeend
      };
      console.log(data);
      this.agendamanageService.postAddMeetingData(data).subscribe(res => {
        console.log(res);
        if (res['status'] === 'fail') {
          this.failaddagenda = res['fail'];
          this.openFailAddagendaDialog(this.failaddagenda);
        } else if (res['status'] === 'success') {
          this.saveDraftstatus = 'success';
          setTimeout(() => {
            this.saveDraftstatus = 'fail';
          }, 3000);
        }
        this.getMeetingList();
      });

    }
    if (this.sendfile === 'true') {
      this.agendamanageService.postSetMeetingFile(this.fileToUpload).subscribe(res => {
        console.log(res);
        if (res['status'] === 'fail') {
          this.failaddagenda = res['fail'];
          this.openFailAddagendaDialog(this.failaddagenda);
        } else if (res['status'] === 'success' && res['fail'].length > 0) {
          this.failaddagenda = res['fail'];
          this.openFailAddagendaDialog(this.failaddagenda);
        } else if (res['status'] === 'success') {
          for (let index = 0; index < this.dataSource_file.data.length; index++) {
            const element = this.dataSource_file.data[index];
            if (element) {
              this.dataSource_file.data.splice(index, 1);
            }
          }
          this.dataSource_file = new MatTableDataSource();
          this.fileToUpload = [];
          this.saveDraftstatus = 'success';
          setTimeout(() => {
            this.saveDraftstatus = 'fail';
          }, 3000);
        }
        this.getMeetingList();
      },
        (error) => console.log(error)
      );
    }

  }

  edit_data(_row) {
    this.router.navigate(['agenda-manage/edit']);
    localStorage.setItem('meetingId', _row.id);
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });
    return true;
  }

  delete_agenda_btn() {
    this.modedeleteagenda = true;
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;

    return numSelected >= 1;
  }

  masterToggle() {

    this.isAllSelected() ?
      this.selection.clear() :
      this.datameetinglist.forEach(row => {

        if (row.canDelete === true) {
          this.selection.select(row)
        }
      }
      );
  }

  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
  }

  cencel_btn() {
    this.selection.selected.length = 0;
    this.selection.clear();
    this.modedeleteagenda = false;
  }

  delete_agenda_main() {
    let _id: any;
    const _data = [];
    this.selection.selected.forEach(s => {
      for (let index = 0; index < this.datameetinglist.length; index++) {
        const element = this.datameetinglist[index];

        // tslint:disable-next-line: triple-equals
        if (element && element.id == s.id) {
          _id = element.id;
          _data.push(_id);
          this.datameetinglist.splice(index, 1);
        }
      }
    });
    const data = {
      'meetingId': _data,
    };
    this.agendamanageService.deleteMeeting(data).subscribe(res => {
      if (res['status'] === 'success') {
        this.getMeetingList();
        console.log('res', res);
      } else {
        Swal.fire({

          text: res['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 3000,
        });
        this.getMeetingList();
      }
    });

    this.selection.selected.length = 0;
    this.selection.clear();
    this.modedeleteagenda = false;
  }

  openMoveAgendaManageDialog(_id): void {
    this._meetingid = _id;
    const dialogRef = this.dialog.open(MoveAgendaManageDialogComponent, {
      width: '640px',
      height: '362px',
      data: {}
    });

    dialogRef.componentInstance.meetingid = _id;

    dialogRef.afterClosed().subscribe(result => {
      this.meetingtargetid = result;
      document.getElementById('modalConfirmMoveAgendaDatetime').click();
    });
  }

  confirmMoveAgenda() {
    const data = {
      'meetingId': this._meetingid,
      'meetingTargetId': this.meetingtargetid
    };
    console.log('data', data);

    this.agendamanageService.postMoveAgenda(data).subscribe(res => {
      console.log(res);
      this.getMeetingList();
    });

  }
  changeDateSearch(dateTime: any) {
    if (dateTime !== null) {
      // tslint:disable-next-line: max-line-length
      const date = moment(dateTime);
      date.locale('th');
      const buddhishYear = (parseInt(date.format('YYYY'), 10) + 543).toString();
      return date.format('DD/MM') + '/' + buddhishYear;
    } else {
      return '';
    }

  }
  onSearch() {
    // if (this.model_search === null) {
    //   this.agendamanageService.getMeetingList(
    //     // tslint:disable-next-line: max-line-length
    //     this.yearsearch, this.monthsearch, '', this.termsearch, this.presentationsearch, this.nosearch, this.productsearch)
    //     .subscribe(res => {
    //       if (res['status'] === 'success' && res['data']) {
    //         this.datameetinglist = res['data'];
    //         console.log(res['data']);
    //       }
    //     });
    // } else if (this.model_search !== null) {
    //   let _dateserach = 0;
    //   let _date = '';

    //   _dateserach = this.model_search.singleDate.date.year + 543;
    //   // tslint:disable-next-line: max-line-length
    //   _date = this.model_search.singleDate.date.day + '/' + this.model_search.singleDate.date.month + '/' + _dateserach.toString();

    //   this.agendamanageService.getMeetingList(
    //     // tslint:disable-next-line: max-line-length
    //     this.yearsearch, this.monthsearch, _date, this.termsearch, this.presentationsearch, this.nosearch, this.productsearch)
    //     .subscribe(res => {
    //       if (res['status'] === 'success' && res['data']) {
    //         this.datameetinglist = res['data'];
    //         console.log(res['data']);

    //       }
    //     });
    // }

    const date = 0;
    const dateFormated = '';

    // if (this.model_search !== null) {
    //   date = this.model_search.singleDate.date.year + 543;
    //   // tslint:disable-next-line: max-line-length
    //   dateFormated = ('0' + (this.model_search.singleDate.date.day)).slice(-2) + '/' +
    //   ('0' + (this.model_search.singleDate.date.month)).slice(-2) + '/' + date.toString();
    // }

    if (this.model_search !== null) {
      // date = this.model_search.singleDate.date.year + 543;
      // // tslint:disable-next-line: max-line-length
      // dateFormated = ('0' + (this.model_search.singleDate.date.day)).slice(-2) + '/' +
      //   ('0' + (this.model_search.singleDate.date.month)).slice(-2) + '/' + date.toString();
      this.model_search = this.model_search;
    }
    const search = {
      'year': this.yearsearch,
      'month': this.monthsearch,
      'meetingDate': this.model_search,
      'term': this.termsearch,
      'presentation': this.presentationsearch,
      'no': this.nosearch,
      'product': this.productsearch
    };
    this.agendamanageService.searchMeetingList(search).subscribe(res => {
      console.log('search:', res);
      if (res['status'] === 'success' && res['data']) {
        this.datameetinglist = res['data'];
        console.log(res['data']);
        this.limitOnChange();

        if (this.datameetinglist.length === 0) {
          this.startItem = -1;
          this.endItem = this.datameetinglist.length;
        } else if (this.datameetinglist.length !== 0 && this.datameetinglist.length < this.pageSize) {
          this.startItem = 0;
          this.endItem = this.datameetinglist.length;

        } else {
          this.startItem = (this.currentPage - 1) * this.pageSize;
          this.endItem = (this.currentPage * this.pageSize);
        }
        this.length = this.datameetinglist.length;
      }
    });
    console.log('search:', search);
  }

  openFailAddagendaDialog(_faildata) {
    const dialogRef = this.dialog.open(FailAddagendaDialogComponent, {
      data: {}
    });

    dialogRef.componentInstance.faildata = _faildata;

    dialogRef.afterClosed().subscribe(result => {
      for (let index = 0; index < this.dataSource_file.data.length; index++) {
        const element = this.dataSource_file.data[index];
        if (element) {
          this.dataSource_file.data.splice(index, 1);
        }
      }
      this.failaddagenda = null;
      this.dataSource_file = new MatTableDataSource();
      this.fileToUpload = [];
      this.getMeetingList();
    });
  }

  showlessResult() {
    if (this.showless === 'true') {
      this.showless = 'false';
    } else {
      this.showless = 'true';
    }
  }

  backToDashboardPage() {
    localStorage.setItem('requestId', '');
    this.router.navigate(['dashboard1']);
  }

  downloadFile64(data: any) {
    const contentType = '';
    const b64Data = data.base64File;
    const filename = data.fileName;
    console.log('base64', b64Data);

    const config = this.base64(b64Data, contentType);
    saveAs(config, filename);
  }

  base64(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

}
