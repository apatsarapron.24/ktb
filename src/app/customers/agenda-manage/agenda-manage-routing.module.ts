import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgendaManageComponent } from './agenda-manage.component';
import { EditComponent } from './edit/edit.component';
const routes: Routes = [
  {
    path: '',
    component: AgendaManageComponent,
  },
  {
    path: 'edit',
    component: EditComponent,
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AgendaManageRoutingModule { }
