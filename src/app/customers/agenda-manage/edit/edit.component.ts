import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { registerLocaleData, DatePipe } from '@angular/common';
import * as moment from 'moment';
import {
  AngularMyDatePickerDirective,
  IAngularMyDpOptions,
  IMyDateModel,
} from 'angular-mydatepicker';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { Router } from '@angular/router';
import { AgendaManageService } from '../../../services/agenda/agenda-manage/agenda-manage.service';
import { DateComponent } from '../../datepicker/date/date.component';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class EditComponent implements OnInit {
  @ViewChild(DateComponent) Date: DateComponent;
  all_header: any = [
    {
      name: 'index',
      value: 'วาระที่',
    },
    {
      name: 'agenda',
      value: 'ประเภทวาระ',
    },
    {
      name: 'presentation',
      value: 'ประเภทการนำเสนอ',
    },
    {
      name: 'agendaId',
      value: 'ลำดับ',
    },
    {
      name: 'requestName',
      value: ' ชื่อผลิตภัณฑ์',
    },
    {
      name: 'requestNo',
      value: ' เลขที่ใบคำขอ',
    },
    {
      name: 'requestObjective',
      value: ' วัตถุประสงค์การนำเสนอ',
    },
    {
      name: 'productOwner',
      value: ' Product Owner',
    },
    {
      name: 'sctr',
      value: ' กลุ่มงาน',
    },
    {
      name: 'grp',
      value: ' สายงาน',
    },

  ];

  delete_row: any;
  saveDraftstatus = null;
  edit_saved_data = null;
  start_hours: any = [
    '00',
    '07',
    '08',
    '09',
    '10',
    '11',
    '12',
    '13',
    '14',
    '15',
    '16',
    '17',
    '18',
    '19',
    '20',
    '21',
  ];
  start_minutes: any = [
    '00',
    '05',
    '10',
    '15',
    '20',
    '25',
    '30',
    '35',
    '40',
    '45',
    '50',
    '55',
  ];

  // time_start: any = {
  //   hours: '00',
  //   minute: '00',
  // };

  // time_end: any = {
  //   hours: '00',
  //   minute: '00',
  // };

  myDpOptions: IAngularMyDpOptions = {
    dateRange: false,
    dateFormat: 'dd/mm/yyyy',
    // other options are here...
    stylesData: {
      selector: 'dp',
      styles: `
           .dp .top:auto
           .dp .myDpIconLeftArrow,
           .dp .myDpIconRightArrow,
           .dp .myDpHeaderBtn {
               color: #3855c1;
            }
           .dp .myDpHeaderBtn:focus,
           .dp .myDpMonthLabel:focus,
           .dp .myDpYearLabel:focus {
               color: #3855c1;
            }
           .dp .myDpDaycell:focus,
           .dp .myDpMonthcell:focus,
           .dp .myDpYearcell:focus {
               box-shadow: inset 0 0 0 1px #66afe9;
            }
           .dp .myDpSelector:focus {
               border: 1px solid #ADD8E6;
            }
           .dp .myDpSelectorArrow:focus:before {
               border-bottom-color: #ADD8E6;
            }
           .dp .myDpCurrMonth,
           .dp .myDpMonthcell,
           .dp .myDpYearcell {
               color: #00a6e6;
            }
           .dp .myDpDaycellWeekNbr {
               color: #3855c1;
            }
           .dp .myDpPrevMonth,
           .dp .myDpNextMonth {
               color: #0098D2;
            }
           .dp .myDpWeekDayTitle {
               background-color: #0098D2;
               color: #ffffff;
            }
           .dp .myDpHeaderBtnEnabled:hover,
           .dp .myDpMonthLabel:hover,
           .dp .myDpYearLabel:hover {
               color:#0098D2;
            }
           .dp .myDpMarkCurrDay,
           .dp .myDpMarkCurrMonth,
           .dp .myDpMarkCurrYear {
               border-bottom: 2px solid #3855c1;
            }
           .dp .myDpDisabled {
               color: #999;
            }
           .dp .myDpHighlight {
               color: #74CBEC;
            }
           .dp .myDpTableSingleDay:hover,
           .dp .myDpTableSingleMonth:hover,
           .dp .myDpTableSingleYear:hover {
               background-color: #add8e6;
               color: #0098D2;
            }
           .dp .myDpRangeColor {
               background-color: #dbeaff;
            }
           .dp .myDpSelectedDay,
           .dp .myDpSelectedMonth,
           .dp .myDpSelectedYear {
               background-color: #0098D2;
               color: #ffffff;
            }
            `,
    },
  };
  // private model: Object = {};
  model: any = null;
  @ViewChild('dp') mydp: AngularMyDatePickerDirective;

  dataagendadetail: any = {
    agenda: [
      {
        agenda: '',
        presentation: '',
        agendaList: [
          {
            agendaId: 0,
            requestId: 0,
            requestName: '',
            requestNo: '',
            requestObjective: '',
            productOwner: '',
            sctr: '',
            grp: '',
            agendaDetail: ''
          }
        ]
      }
    ]
  };
  meetingid: string;
  requestId: any;
  timestart_hours = '00';
  timestart_minute = '00';
  timeend_hours = '00';
  timeend_minute = '00';
  timestart = '';
  timeend = '';
  timechange = '';
  agendadate = '';
  updown = '';
  checksavetime = true;
  myDate = new Date();
  date: string;
  constructor(private dialog: MatDialog,
    private router: Router,
    private agendamanageService: AgendaManageService,
    private datePipe: DatePipe) { }

  ngOnInit() {
    this.saveDraftstatus = null;
    this.meetingid = localStorage.getItem('meetingId');
    this.requestId = localStorage.getItem('requestId');
    this.getAgendaMeetingDetail();
  }
  changeDateSearch(dateTime: any) {
    if (dateTime !== null) {
      // tslint:disable-next-line: max-line-length
      const date = moment(dateTime);
      date.locale('th');
      const buddhishYear = (parseInt(date.format('YYYY'), 10) + 543).toString();
      return date.format('DD/MM') + '/' + buddhishYear;
    } else {
      return '';
    }

  }
  getAgendaMeetingDetail() {
    this.agendamanageService.getAgendaMeetingDetail(this.meetingid).subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        this.dataagendadetail = res['data'];
        console.log('this.dataagendadetail', this.dataagendadetail);
      }
    });
  }

  edit_time() {
    this.edit_saved_data = 'edit';
    this.saveDraftstatus = null;
  }

  time() {
    let _year = 0;
    this.timestart = this.timestart_hours + ':' + this.timestart_minute;
    this.timeend = this.timeend_hours + ':' + this.timeend_minute;
    // this.agendadate = this.model.singleDate.formatted;
    _year = this.model.singleDate.date.year + 543;
    this.agendadate = this.model.singleDate.date.day + '/' + this.model.singleDate.date.month + '/' + _year.toString();
  }

  save_time() {
    if (this.edit_saved_data === 'edit') {
      if (this.timestart !== '' && this.timeend !== '' && this.agendadate !== '') {
        this.edit_saved_data = '';
        this.timechange = 'timechange';
      } else {
        alert('กรุณาระบุวันที่และเวลา');
        this.checksavetime = false;
      }
    }
  }

  onSave() {
    let data: any;
    this.save_time();

    if (this.timechange === 'timechange') {
      this.dataagendadetail.meetingDate = this.agendadate;
      this.dataagendadetail.meetingTimeStart = this.timestart;
      this.dataagendadetail.meetingTimeEnd = this.timeend;
      console.log('After: ', this.dataagendadetail);
      data = this.dataagendadetail;
    } else if (this.timechange === 'timechange' && this.updown === 'true') {
      this.dataagendadetail.meetingDate = this.agendadate;
      this.dataagendadetail.meetingTimeStart = this.timestart;
      this.dataagendadetail.meetingTimeEnd = this.timeend;
      console.log('After: ', this.dataagendadetail);
      data = this.dataagendadetail;
    } else if (this.updown === 'true') {
      this.dataagendadetail.meetingDate = this.dataagendadetail.meetingDate;
      this.dataagendadetail.meetingTimeStart = this.dataagendadetail.meetingTimeStart;
      this.dataagendadetail.meetingTimeEnd = this.dataagendadetail.meetingTimeEnd;
      console.log('After: ', this.dataagendadetail);
      data = this.dataagendadetail;
    }

    if (this.checksavetime === true) {
      this.agendamanageService.postUpdateMeetingAgenda(data).subscribe(res => {
        console.log('res', res['status']);
        if (res['status'] === 'fail') {
          alert(res['message']);
        } else {
          this.saveDraftstatus = 'success';
          setTimeout(() => {
            this.saveDraftstatus = 'fail';
          }, 3000);
        }
      }
      );

      this.timechange = '';
      this.updown = '';
    } else {
      this.checksavetime = true;
    }
  }

  delete_data(data: any) {
    console.log('data:', data);
    this.delete_row = data;
  }

  checkSaveData() {
    if (this.edit_saved_data === 'edit') {
      document.getElementById('NotSaveData').click();
    } else {
      this.back_main();
    }
  }

  back_main() {
    this.router.navigate(['agenda-manage']);
  }

  edit_data() {
    this.router.navigate(['agenda-manage/edit']);
  }

  edit_agendaadd(id) {
    localStorage.setItem('requestId', id);
    this.router.navigate(['agenda-add']);
  }

  up_data(_data: any, _subdata: any, i: number) {
    let _agendaid_1: any;
    let _agendaid_2: any;
    let _agendalist = {};
    const min = i;

    for (let index = 0; index < this.dataagendadetail.agenda.length; index++) {
      const element = this.dataagendadetail.agenda[index];

      if (element === _data) {
        this.updown = 'true';
        console.log(this.dataagendadetail.agenda[index].agendaList);
      }
    }

    if (min > 0) {
      _agendaid_1 = _data.agendaList[i - 1].agendaId;
      _agendaid_2 = _data.agendaList[i].agendaId;

      _agendalist = _data.agendaList[i - 1];
      _data.agendaList[i - 1] = _data.agendaList[i];
      _data.agendaList[i] = _agendalist;

      _data.agendaList[i - 1].agendaId = _agendaid_1;
      _data.agendaList[i].agendaId = _agendaid_2;
    }
  }

  down_data(_data: any, _subdata: any, i: number) {
    let _agendaid_1: any;
    let _agendaid_2: any;
    let _agendalist = {};
    const max = i + 1;

    for (let index = 0; index < this.dataagendadetail.agenda.length; index++) {
      const element = this.dataagendadetail.agenda[index];

      if (element === _data) {
        this.updown = 'true';
        console.log(this.dataagendadetail.agenda[index].agendaList);
      }
    }

    if (max < _data.agendaList.length) {
      _agendaid_1 = _data.agendaList[i + 1].agendaId;
      _agendaid_2 = _data.agendaList[i].agendaId;

      _agendalist = _data.agendaList[i + 1];
      _data.agendaList[i + 1] = _data.agendaList[i];
      _data.agendaList[i] = _agendalist;

      _data.agendaList[i + 1].agendaId = _agendaid_1;
      _data.agendaList[i].agendaId = _agendaid_2;

    }
  }
}

