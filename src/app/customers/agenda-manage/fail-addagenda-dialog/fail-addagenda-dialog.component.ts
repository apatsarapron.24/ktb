import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-fail-addagenda-dialog',
  templateUrl: './fail-addagenda-dialog.component.html',
  styleUrls: ['./fail-addagenda-dialog.component.scss']
})
export class FailAddagendaDialogComponent implements OnInit {

  faildata: any;

  constructor(
    public dialogRef: MatDialogRef<FailAddagendaDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: any,
  ) { }

  ngOnInit() {
  }

  close() {
    this.dialogRef.close();
  }

}
