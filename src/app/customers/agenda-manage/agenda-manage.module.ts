import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgendaManageComponent } from './agenda-manage.component';
import { AgendaManageRoutingModule } from './agenda-manage-routing.module';
import { AngularMyDatePickerModule } from 'angular-mydatepicker';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { AlertDeleteComponent } from './alert-delete/alert-delete.component';
import { MatDialogModule } from '@angular/material/dialog';
import { EditComponent } from './edit/edit.component';
import { MatCheckboxModule } from '@angular/material';
import { MoveAgendaManageDialogComponent } from './move-agenda-manage-dialog/move-agenda-manage-dialog.component';
import { FailAddagendaDialogComponent } from './fail-addagenda-dialog/fail-addagenda-dialog.component';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { DateModule } from '../datepicker/date/date.module';

@NgModule({
  declarations: [AgendaManageComponent, AlertDeleteComponent, EditComponent, MoveAgendaManageDialogComponent, FailAddagendaDialogComponent],
  imports: [
    CommonModule,
    AgendaManageRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    MatTableModule,
    MatInputModule,
    MatDialogModule,
    AngularMyDatePickerModule,
    MatCheckboxModule,
    PaginationModule.forRoot(),
    DateModule
  ],
  entryComponents: [
    AlertDeleteComponent,
    MoveAgendaManageDialogComponent,
    FailAddagendaDialogComponent
  ],
})
export class AgendaManageModule { }
