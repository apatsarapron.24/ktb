import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AgendaManageService } from '../../../services/agenda/agenda-manage/agenda-manage.service';

@Component({
  selector: 'app-move-agenda-manage-dialog',
  templateUrl: './move-agenda-manage-dialog.component.html',
  styleUrls: ['./move-agenda-manage-dialog.component.scss']
})
export class MoveAgendaManageDialogComponent implements OnInit {

  selectdatetime = 0;
  dropdownmeetinglist = null;
  dropdownmeetinglistSelect = null;
  meetingid: 0;

  constructor(
    public dialogRef: MatDialogRef<MoveAgendaManageDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: any,
    private agendamanageService: AgendaManageService
  ) { }

  ngOnInit() {
    this.getDropdownMeetingList();
  }

  getDropdownMeetingList() {
    this.agendamanageService.getDropdownMeettingList(this.meetingid).subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        this.dropdownmeetinglistSelect = res['data'];
        this.dropdownmeetinglist = this.dropdownmeetinglistSelect.dropdownMeettingList;
      }
    });
  }

  clearSelectMoveDatetime() {
    this.dialogRef.close();
    this.selectdatetime = 0;
  }

  openModelConfirm() {
    this.dialogRef.close(this.selectdatetime);
  }

}
