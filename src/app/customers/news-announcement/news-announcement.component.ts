import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material';
import { RouterLink, Router, ActivatedRoute } from '@angular/router';
import {
  AngularMyDatePickerDirective,
  IAngularMyDpOptions,
  IMyDateModel,
} from 'angular-mydatepicker';
import * as moment from 'moment';
import { NewsEditComponent } from './news-edit/news-edit.component';
import { NewsAddComponent } from './news-add/news-add.component';
import { NewsSubmitComponent } from './news-submit/news-submit.component';
import { NewsAnnouncementService } from './../../services/news-announcement/news-announcement.service';
import { DateAdapter } from '@angular/material';

export interface PeriodicElement {
  id: Number;
  no: Number;
  topic: String;
  announcement_date: String;
  start_date: String;
  end_date: String;
  status: String;
  creator: String;
  objective: any;
  attachments: any;
}

const initData = [];

@Component({
  selector: 'app-news-announcement',
  templateUrl: './news-announcement.component.html',
  styleUrls: ['./news-announcement.component.scss']
})
export class NewsAnnouncementComponent implements OnInit {
  displayedColumns: string[] = ['select', 'no', 'topic', 'announcement_date', 'start_date', 'end_date', 'status', 'creator', 'edit'];
  dataSourceSearch = new MatTableDataSource(initData);
  selection = new SelectionModel<PeriodicElement>(true, []);

  displayedColumns_file: string[] = ['name', 'size', 'delete'];

  search_data = true;
  check_delete_news = false;
  showsearch = true;
  image1 = [];
  deleteId = [];
  data_searh = {
    topic: '',
    status: '',
    start_date: '',
    end_date: '',
  };
  results: any;
  startDate = [];
  endDate = [];
  nullData = [
    {
      topic: '-',
      informationDate: '-',
      informationStartDate: '-',
      informationEndDate: '-',
      status: '-',
      creator: '-',
    }
  ];

  constructor(private router: Router,
    private dialog: MatDialog,
    private newsAnnouncementService: NewsAnnouncementService,
    private adapter: DateAdapter<any>
  ) {
    this.adapter.setLocale('th-TH');
  }

  ngOnInit() {
    this.setupCalendar(0);
    // this.dataSourceSearch = new MatTableDataSource(fecthData);
    console.log('this.dataSourceSearch', this.dataSourceSearch);
    this.selection = new SelectionModel<PeriodicElement>(true, []);
    this.searchResult();


  }

  onDateChanged(event: IMyDateModel, type): void {
    const date = moment(event.singleDate.jsDate);
    if (type === 'start_date') {
      this.data_searh.start_date = date.format('YYYY-MM-DD');
    }
    if (type === 'end_date') {
      this.data_searh.end_date = date.format('YYYY-MM-DD');
    }
    console.log(this.data_searh);
  }

  searchResult() {
    this.search_data = true;
    console.log('data_search', this.data_searh);
    this.newsAnnouncementService.newsSearch(this.data_searh).subscribe((res) => {
      console.log('Search News:', res);
      this.results = res;
      if (this.results.data !== null) {
        this.dataSourceSearch = new MatTableDataSource(this.results.data);
        this.results.data.forEach((data, index) => {
          // data.id = index + 1;
          if (data.informationDate !== null) {
            data.informationDate = this.dateToBuddha(data.informationDate);
          } else {
            data.informationDate = '-';
          }
          if (data.informationStartDate !== null) {
            data.informationStartDate = this.dateToBuddha(data.informationStartDate);
          } else {
            data.informationStartDate = '-';
          }
          if (data.informationEndDate !== null) {
            data.informationEndDate = this.dateToBuddha(data.informationEndDate);
          } else {
            data.informationEndDate = '-';
          }
        });
      } else {
        this.dataSourceSearch = new MatTableDataSource(this.nullData);
        this.search_data = false;
      }
    });
  }

  deleteNews() {
    this.check_delete_news = !this.check_delete_news;
  }


  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSourceSearch.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSourceSearch.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: PeriodicElement): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    // return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.select + 1}`;
  }

  editDialog(i) {
    const dialogRef = this.dialog.open(NewsEditComponent, { width: '1300px', disableClose: true, autoFocus: false });
    console.log(this.dataSourceSearch.data[i]);
    // localStorage.removeItem('confirmSavePermissionData');
    sessionStorage.setItem('editId', JSON.stringify(this.dataSourceSearch.data[i].id));
    dialogRef.afterClosed().subscribe(result => {
      this.searchResult();
    });
  }

  addDialog() {
    const dialogRef = this.dialog.open(NewsAddComponent, { width: '1300px', disableClose: true, autoFocus: false });
    // this.dialog.open(NewsSubmitComponent, { disableClose: true , autoFocus: false});
    // localStorage.removeItem('confirmSavePermissionData');
    // sessionStorage.setItem('editDialog', JSON.stringify(this.dataSourceSearch.data[i]) );
    dialogRef.afterClosed().subscribe(result => {
      this.searchResult();
    });
  }

  deleteDialog() {
    console.log(this.selection.selected);
    for (let i = 0; i < this.selection.selected.length; i++) {
      this.deleteId.push(this.selection.selected[i].id);
    }
    console.log('delete id', this.deleteId);
    const dialogRef = this.dialog.open(NewsSubmitComponent, { disableClose: true, autoFocus: false });
    sessionStorage.setItem('deleteDialog', JSON.stringify(this.deleteId));
    sessionStorage.setItem('newsType', 'การลบการประกาศข่าว');
    dialogRef.afterClosed().subscribe(result => {
      this.searchResult();
      this.check_delete_news = false;
    });
  }

  // ********************** buddist calendar calendar **********************
  setupCalendar(calendarIndex, option?) {
    const templateCalendar = {
      minStartDate: new Date(1900, 0, 1),
      maxStartDate: undefined,
      minEndDate: new Date(1900, 0, 1),
      maxEndDate: undefined,
      selectedDate: '',
      selectedStartDate: new Date(),
      selectedEndDate: new Date(),
      modifiedStartDate: this.dateToBuddha(new Date()),
      modifiedEndDate: this.dateToBuddha(new Date()),
      calendarState: false
    };
    const templateCalendar2 = {
      minStartDate: new Date(1900, 0, 1),
      maxStartDate: undefined,
      minEndDate: new Date(1900, 0, 1),
      maxEndDate: undefined,
      selectedDate: '',
      selectedStartDate: new Date(),
      selectedEndDate: new Date(),
      modifiedStartDate: this.dateToBuddha(new Date()),
      modifiedEndDate: this.dateToBuddha(new Date()),
      calendarState: false
    };

    this.startDate[calendarIndex] = option ? option : templateCalendar;
    this.endDate[calendarIndex] = option ? option : templateCalendar2;
    console.log(this.startDate);

  }
  dateToBuddha(dateNormal) {
    const now = moment(dateNormal);
    now.locale('th');
    const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();
    return now.format('DD/MM') + '/' + buddhishYear;
  }
  onSelectStart(event, calendarIndex, dateType) {
    console.log(event);
    this.startDate[calendarIndex].selectedStartDate = event;
    this.startDate[calendarIndex].modifiedStartDate = this.dateToBuddha(event);
    console.log(this.startDate[calendarIndex].modifiedStartDate);

    this.startDate[calendarIndex].minEndDate = this.startDate[calendarIndex].selectedStartDate;
    console.log('dd', this.startDate[0].minEndDate);
    console.log('startDate', this.startDate);
    // this.model = {isRange: false, singleDate: {jsDate: new Date(this.startDate[0].minEndDate), }, };
  }

  onSelectEnd(event, calendarIndex) {
    this.endDate[calendarIndex].selectedStartDate = event;
    this.endDate[calendarIndex].modifiedStartDate = this.dateToBuddha(event);
    console.log(this.endDate[calendarIndex].modifiedStartDate);

    this.endDate[calendarIndex].minEndDate = this.endDate[calendarIndex].selectedStartDate;
    console.log('dd', this.endDate[0].minEndDate);
    console.log('endDate', this.endDate);
  }
  openCalendar(calendarIndex, dateType) {
    if (dateType === 'startDate') {
      console.log('start open calendar', this.startDate);
      this.startDate[calendarIndex].calendarState = true;
      if (this.startDate[calendarIndex].selectedDate) {
        console.log('in openCalendar', calendarIndex);
        // dd/mm/yyyy -> yyyy-mm-dd
        const splitData = this.startDate[calendarIndex].selectedDate.replace(/\s/g, '').split('-');
        const convertYear = splitData.map(d => {
          return `${(+d.split('/')[2] - 543)}-${d.split('/')[1]}-${d.split('/')[0]}`;
        });

        // set current date when open calendar
        this.startDate[calendarIndex].selectedStartDate = new Date(convertYear[0]);
        this.startDate[calendarIndex].selectedEndDate = new Date(convertYear[1]);
        this.startDate[calendarIndex].modifiedStartDate = splitData[0];
        this.startDate[calendarIndex].modifiedEndDate = splitData[1];
      }
    } else if (dateType === 'endDate') {
      console.log('in open calendar', this.endDate);
      this.endDate[calendarIndex].calendarState = true;
      if (this.endDate[calendarIndex].selectedDate) {
        console.log('in openCalendar', calendarIndex);
        // dd/mm/yyyy -> yyyy-mm-dd
        console.log('in if');
        const splitData = this.endDate[calendarIndex].selectedDate.replace(/\s/g, '').split('-');
        const convertYear = splitData.map(d => {
          return `${(+d.split('/')[2] - 543)}-${d.split('/')[1]}-${d.split('/')[0]}`;
        });

        // set current date when open calendar
        this.endDate[calendarIndex].selectedStartDate = new Date(convertYear[0]);
        this.endDate[calendarIndex].selectedEndDate = new Date(convertYear[1]);
        this.endDate[calendarIndex].modifiedStartDate = splitData[0];
        this.endDate[calendarIndex].modifiedEndDate = splitData[1];
      }
    }
  }

  applyCalendar(calendarIndex, dateType) {
    if (dateType === 'startDate') {
      this.startDate[calendarIndex].selectedDate = `${this.startDate[calendarIndex].modifiedStartDate}`;
      this.startDate[calendarIndex].calendarState = false;
      this.getLogUserSearchData(dateType);
    } else if (dateType === 'endDate') {
      this.endDate[calendarIndex].selectedDate = `${this.endDate[calendarIndex].modifiedStartDate}`;
      this.endDate[calendarIndex].calendarState = false;
      this.getLogUserSearchData(dateType);
    }
  }
  closeCalendar(calendarIndex, dateType) {
    if (dateType === 'startDate') {
      this.startDate[calendarIndex].calendarState = false;
    } else if (dateType === 'endDate') {
      this.endDate[calendarIndex].calendarState = false;
    }
  }

  clearCalendar(calendarIndex, dateType, option?) {
    if (dateType === 'startDate') {
      const templateCalendar = {
        minStartDate: new Date(1900, 0, 1),
        maxStartDate: undefined,
        minEndDate: new Date(1900, 0, 1),
        maxEndDate: undefined,
        selectedDate: '',
        selectedStartDate: new Date(),
        selectedEndDate: new Date(),
        modifiedStartDate: this.dateToBuddha(new Date()),
        modifiedEndDate: this.dateToBuddha(new Date()),
        calendarState: false
      };
      this.startDate[calendarIndex] = option ? option : templateCalendar;
      this.data_searh.start_date = '';
    } else if (dateType === 'endDate') {
      const templateCalendar2 = {
        minStartDate: new Date(1900, 0, 1),
        maxStartDate: undefined,
        minEndDate: new Date(1900, 0, 1),
        maxEndDate: undefined,
        selectedDate: '',
        selectedStartDate: new Date(),
        selectedEndDate: new Date(),
        modifiedStartDate: this.dateToBuddha(new Date()),
        modifiedEndDate: this.dateToBuddha(new Date()),
        calendarState: false
      };
      this.endDate[calendarIndex] = option ? option : templateCalendar2;
      this.data_searh.end_date = '';
    }
  }

  getLogUserSearchData(dateType) {

    if (dateType === 'startDate') {
      if (this.startDate[0].selectedDate !== '') {
        this.data_searh.start_date = moment(this.startDate[0].selectedStartDate).format('YYYY-MM-DD');
      } else {
        this.data_searh.start_date = '';
        console.log('null');
      }
    } else if (dateType === 'endDate') {
      if (this.endDate[0].selectedDate !== '') {
        this.data_searh.end_date = moment(this.endDate[0].selectedStartDate).format('YYYY-MM-DD');
      } else {
        this.data_searh.end_date = '';
        console.log('null');
      }
    }
  }
  add(element) { }
  input_date() { }

  backToDashboardPage() {
    localStorage.setItem('requestId', '');
    this.router.navigate(['dashboard1']);
  }

  ShowSearch() {
    if (this.showsearch === true) {
      this.showsearch = false;
    } else {
      this.showsearch = true;
    }
  }
}
