import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsSubmitComponent } from './news-submit.component';

describe('NewsSubmitComponent', () => {
  let component: NewsSubmitComponent;
  let fixture: ComponentFixture<NewsSubmitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsSubmitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsSubmitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
