import { NewsAnnouncementComponent } from './../news-announcement.component';
import { Component, OnInit, EventEmitter, Inject } from '@angular/core';
import { MatDialog } from '@angular/material';
import { MatDialogRef } from '@angular/material';
import { NewsAddComponent } from './../news-add/news-add.component';
import { NewsEditComponent } from './../news-edit/news-edit.component';
import { NewsAnnouncementService } from './../../../services/news-announcement/news-announcement.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-news-submit',
  templateUrl: './news-submit.component.html',
  styleUrls: ['./news-submit.component.scss']
})
export class NewsSubmitComponent implements OnInit {
  constructor(private dialog: MatDialog,
    private dialogRef: MatDialogRef<NewsAddComponent>,
    private newsAnnouncementService: NewsAnnouncementService) {
    this.dialogRef.disableClose = true;
  }

  newsType: any;
  datasendsave: any;
  datasendedit: any;
  status_loading = false;
  // tslint:disable-next-line:no-output-rename
  ngOnInit() {
    this.newsType = sessionStorage.getItem('newsType');
    console.log(this.newsType);
  }

  back() {

    if (this.newsType === 'การแก้ไขการประกาศข่าว') {
      console.log('edit', sessionStorage.getItem('editDialog'));
      this.dialog.open(NewsEditComponent, { width: '1300px', disableClose: true, autoFocus: false });

      this.dialogRef.close();
    }
    if (this.newsType === 'การเพิ่มการประกาศข่าว') {
      console.log('add', sessionStorage.getItem('addDialog'));
      this.dialog.open(NewsAddComponent, { width: '1300px', disableClose: true, autoFocus: false });
      this.dialogRef.close();
    }
    if (this.newsType === 'การลบการประกาศข่าว') {
      console.log('Delete', sessionStorage.getItem('deleteDialog'));
      this.dialogRef.close();
    }
  }

  save() {
    this.status_loading = true;
    console.log('getitem',sessionStorage.getItem('newsType'));
    if (this.newsType === 'การแก้ไขการประกาศข่าว') {
      console.log('edit', this.datasendedit);
      this.newsAnnouncementService.newsAddOrDelete(this.datasendedit).subscribe(res => {
        console.log('>>>>>>>',res)
        const results = res;
        if (res['status'] === 'success') {
          this.status_loading = false;
          this.dialogRef.close();
          Swal.fire({
            title: 'success',
            text: this.newsType + 'เรียบร้อย',
            icon: 'success',
            showConfirmButton: false,
            timer: 1500,
          });
        } else {
          this.status_loading = false;
          this.dialogRef.close();
          Swal.fire({
            title: 'error',
            text: res['message'],
            icon: 'error',
            showConfirmButton: false,
            timer: 1500,
          });
          this.back();
        }
      }, err => {
        this.status_loading = false;
        console.log(err);
        this.dialogRef.close();
        Swal.fire({
          title: 'error',
          text: err['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 3000,
        });
      });
      console.log('remove Data');
      sessionStorage.removeItem('editDialog');
      console.log('remove ID');
      sessionStorage.removeItem('editId');
      // this.dialogRef.close();
    }
    if (this.newsType === 'การเพิ่มการประกาศข่าว') {
      console.log('add', this.datasendsave);
      this.status_loading = true;
      this.newsAnnouncementService.newsAddOrDelete(this.datasendsave).subscribe(res => {
        console.log('>>>>>>>>',res);
        const results = res;
        if (res['status'] === 'success') {
          this.status_loading = false;
          this.dialogRef.close();
          Swal.fire({
            title: 'success',
            text: this.newsType + 'เรียบร้อย',
            icon: 'success',
            showConfirmButton: false,
            timer: 1500,
          });
        } else {
          this.status_loading = false;
          this.dialogRef.close();
          Swal.fire({
            title: 'error',
            text: res['message'],
            icon: 'error',
            showConfirmButton: false,
            timer: 1500,
          });
          this.back();
        }
      }, err => {
        console.log(err);
        this.dialogRef.close();
        Swal.fire({
          title: 'error',
          text: err['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 3000,
        });
      });
      sessionStorage.removeItem('addDialog');
      // this.dialogRef.close();
    }
    if (this.newsType === 'การลบการประกาศข่าว') {
      const datasend = {
        'deleteId': JSON.parse(sessionStorage.getItem('deleteDialog'))
      };
      this.status_loading = true;
      this.newsAnnouncementService.newsDelete(datasend).subscribe(res => {
        const results = res;
        if (res['status'] === 'success') {
          this.dialogRef.close();
          Swal.fire({
            title: 'success',
            text: this.newsType + 'เรียบร้อย',
            icon: 'success',
            showConfirmButton: false,
            timer: 1500,
          });
          this.status_loading = false;
        } else {
          this.dialogRef.close();
          Swal.fire({
            title: 'error',
            text: res['message'],
            icon: 'error',
            showConfirmButton: false,
            timer: 1500,
          });
          this.back();
          this.status_loading = false;
        }
      }, err => {
        console.log(err);
        this.dialogRef.close();
        Swal.fire({
          title: 'error',
          text: err['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 3000,
        });
      });
      sessionStorage.removeItem('deleteDialog');
      // this.dialogRef.close();
    }
  }
}
