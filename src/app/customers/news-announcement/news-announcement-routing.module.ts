import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewsAnnouncementComponent } from './news-announcement.component';

const routes: Routes = [
  {
    path: '',
    component: NewsAnnouncementComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewsAnnouncementRoutingModule { }
