import { Data } from './../../dashboard-cus/dashboard-cus.component';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { MatDialog } from '@angular/material';
import {
  AngularMyDatePickerDirective,
  IAngularMyDpOptions,
  IMyDateModel,
} from 'angular-mydatepicker';
import * as moment from 'moment';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { NewsSubmitComponent } from './../news-submit/news-submit.component';
import { NewsAnnouncementService } from './../../../services/news-announcement/news-announcement.service';
import { DateAdapter } from '@angular/material';
import { saveAs } from 'file-saver';


export interface FileList {
  fileName: string;
  fileSize: string;
  base64File: string;
}

@Component({
  selector: 'app-news-edit',
  templateUrl: './news-edit.component.html',
  styleUrls: ['./news-edit.component.scss']
})
export class NewsEditComponent implements OnInit {

  OverSize: any;

  constructor(private dialog: MatDialog,
    private dialogRef: MatDialogRef<NewsEditComponent>,
    private newsAnnouncementService: NewsAnnouncementService,
    private adapter: DateAdapter<any>) {
    this.dialogRef.disableClose = true;
    this.adapter.setLocale('th-TH');
  }
  displayedColumns_file: string[] = ['name', 'size', 'delete'];
  dataSource_file: MatTableDataSource<FileList>;

  informationDate: any;
  informationStartDate: any;
  informationEndDate: any;

  startDate = [];
  endDate = [];
  announceDate = [];

  data_edit = {
    id: null,
    topic: '',
    informationDate: '',
    status: false,
    informationStartDate: '',
    informationEndDate: '',
    detail: '',
    picture: {
      file: [],
      fileDelete: []
    },
    attachments: {
      file: [],
      fileDelete: []
    },
  };

  message = null;
  file1_1: any;
  file1_2: any;
  result: any;

  myDpOptions: IAngularMyDpOptions = {
    dateRange: false,
    dateFormat: 'dd/mm/yyyy',
    // other options are here...
    stylesData: {
      selector: 'dp',
      styles: `
           .dp .top:auto
           .dp .myDpIconLeftArrow,
           .dp .myDpIconRightArrow,
           .dp .myDpHeaderBtn {
               color: #3855c1;
            }
           .dp .myDpHeaderBtn:focus,
           .dp .myDpMonthLabel:focus,
           .dp .myDpYearLabel:focus {
               color: #3855c1;
            }
           .dp .myDpDaycell:focus,
           .dp .myDpMonthcell:focus,
           .dp .myDpYearcell:focus {
               box-shadow: inset 0 0 0 1px #66afe9;
            }
           .dp .myDpSelector:focus {
               border: 1px solid #ADD8E6;
            }
           .dp .myDpSelectorArrow:focus:before {
               border-bottom-color: #ADD8E6;
            }
           .dp .myDpCurrMonth,
           .dp .myDpMonthcell,
           .dp .myDpYearcell {
               color: #00a6e6;
            }
           .dp .myDpDaycellWeekNbr {
               color: #3855c1;
            }
           .dp .myDpPrevMonth,
           .dp .myDpNextMonth {
               color: #0098D2;
            }
           .dp .myDpWeekDayTitle {
               background-color: #0098D2;
               color: #ffffff;
            }
           .dp .myDpHeaderBtnEnabled:hover,
           .dp .myDpMonthLabel:hover,
           .dp .myDpYearLabel:hover {
               color:#0098D2;
            }
           .dp .myDpMarkCurrDay,
           .dp .myDpMarkCurrMonth,
           .dp .myDpMarkCurrYear {
               border-bottom: 2px solid #3855c1;
            }
           .dp .myDpDisabled {
               color: #999;
            }
           .dp .myDpHighlight {
               color: #74CBEC;
            }
           .dp .myDpTableSingleDay:hover,
           .dp .myDpTableSingleMonth:hover,
           .dp .myDpTableSingleYear:hover {
               background-color: #add8e6;
               color: #0098D2;
            }
           .dp .myDpRangeColor {
               background-color: #dbeaff;
            }
           .dp .myDpSelectedDay,
           .dp .myDpSelectedMonth,
           .dp .myDpSelectedYear {
               background-color: #0098D2;
               color: #ffffff;
            }
            `,
    },
  };

  // Image Model
  showModel = false;
  modalmainImageIndex: number;
  modalmainImagedetailIndex: number;
  indexSelect: number;
  modalImage = [];
  modalImagedetail = [];
  fileValue: number;

  document: any;
  image1 = [];
  downloadfile_by = [];

  ngOnInit() {
    this.setupCalendar(0);
    // console.log('id', sessionStorage.getItem('editId'));
    if ('editDialog' in sessionStorage) {
      if (sessionStorage.getItem('editDialog')) {
        this.result = JSON.parse(sessionStorage.getItem('editDialog'));
        // console.log('result', this.result);
        this.data_edit.id = this.result.id;
        this.data_edit.topic = this.result.topic;
        this.data_edit.informationDate = this.result.informationDate;
        this.data_edit.status = this.result.status;
        this.data_edit.informationStartDate = this.result.informationStartDate;
        this.data_edit.informationEndDate = this.result.informationEndDate;
        this.data_edit.detail = this.result.detail;

        // console.log('testttttttttt', this.result.informationStartDate);
        if (this.result.informationDate !== null) {
          this.announceDate[0].selectedDate = this.dateToBuddha(this.result.informationDate);
        }
        if (this.result.informationStartDate !== null) {
          this.startDate[0].selectedDate = this.dateToBuddha(this.result.informationStartDate);
        }
        if (this.result.informationEndDate !== null) {
          this.endDate[0].selectedDate = this.dateToBuddha(this.result.informationEndDate);
        }
        for (let i = 0; i < this.result.attachments.file.length; i++) {
          this.data_edit.attachments.file.push(this.result.attachments.file[i]);
        }
        for (let i = 0; i < this.result.picture.file.length; i++) {
          this.preview(1, this.result.picture.file[i], i);
          this.data_edit.picture.file.push(this.result.picture.file[i]);

        }
        this.dataSource_file = new MatTableDataSource(this.data_edit.attachments.file);
        this.hideDownloadFile();
      }
    } else {
      const datasend = {
        'id': JSON.parse(sessionStorage.getItem('editId'))
      };
      this.newsAnnouncementService.newsGet(datasend).subscribe(res => {
        this.result = res;
        // console.log('newsEdit result', this.result.data);
        // console.log('in get start', this.result);
        this.data_edit.id = this.result.data.id;
        this.data_edit.topic = this.result.data.topic;
        this.data_edit.informationDate = this.result.data.informationDate;
        this.data_edit.status = this.result.data.status;
        this.data_edit.informationStartDate = this.result.data.informationStartDate;
        this.data_edit.informationEndDate = this.result.data.informationEndDate;
        this.data_edit.detail = this.result.data.detail;
        // console.log('this.result.informationDate', this.result.data.informationDate);
        // console.log('announce date', this.announceDate);
        if (this.result.data.informationDate !== null) {
          this.announceDate[0].selectedDate = this.dateToBuddha(this.result.data.informationDate);
        }
        if (this.result.data.informationStartDate !== null) {
          this.startDate[0].selectedDate = this.dateToBuddha(this.result.data.informationStartDate);
        }
        if (this.result.data.informationEndDate !== null) {
          this.endDate[0].selectedDate = this.dateToBuddha(this.result.data.informationEndDate);
        }
        for (let i = 0; i < this.result.data.attachments.file.length; i++) {
          this.data_edit.attachments.file.push(this.result.data.attachments.file[i]);
        }
        this.dataSource_file = new MatTableDataSource(this.data_edit.attachments.file);
        for (let i = 0; i < this.result.data.picture.file.length; i++) {
          this.data_edit.picture.file.push(this.result.data.picture.file[i]);
        }
        for (let i = 0; i < this.data_edit.picture.file.length; i++) {
          this.preview(1, this.data_edit.picture.file[i], i);
        }
        this.hideDownloadFile();
      });
    }
  }

  autogrow() {
    const textArea = document.getElementById('description');
    textArea.style.overflow = 'hidden';
    textArea.style.height = 'auto';
    textArea.style.height = textArea.scrollHeight + 'px';
  }
  close() {
    sessionStorage.removeItem('editDialog');
    this.dialogRef.close();

  }

  // onDateChanged(event: IMyDateModel, type): void {
  //   const date = moment(event.singleDate.jsDate);
  //   if (type === 'informationStartDate') {
  //     this.data_edit.informationStartDate = date.format('YYYY-MM-DD');
  //   }
  //   if (type === 'informationEndDate') {
  //     this.data_edit.informationEndDate = date.format('YYYY-MM-DD');
  //   }
  //   if (type === 'informationDate') {
  //     this.data_edit.informationDate = date.format('YYYY-MM-DD');
  //   }
  //   console.log(this.data_edit);
  // }

  getFileDetails(fileIndex, event) {
    const files = event.target.files;

    for (let i = 0; i < files.length; i++) {
      const mimeType = files[i].type;
      // console.log(mimeType);
      if (mimeType.match('image/jpeg|image/png') == null) {
        this.message = 'Only images are supported.';
        alert(this.message);
        return;
      }
      const file = files[i];
      // console.log('file:', file);
      const reader = new FileReader();
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');

        const templateFile = {
          fileName: file.name,
          base64File: splitFile[1],
        };

        if (fileIndex === 1) {
          // console.log('00000000000000000000000000000001');
          this.data_edit.picture.file.push(templateFile);

          for (let index = 0; index < this.data_edit.picture.file.length; index++) {
            this.preview(fileIndex, this.data_edit.picture.file[index], index);
          }
          this.file1_1 = undefined;
          this.file1_2 = undefined;
        }

      };
      reader.readAsDataURL(file);

    }
    // this.changeSaveDraft();
  }

  delete_image(fileIndex, index) {
    if (fileIndex === 1) {
      if ('id' in this.data_edit.picture.file[index]) {
        // console.log(this.data_edit.picture.file[index]);
        const id = this.data_edit.picture.file[index].id;
        if ('fileDelete' in this.data_edit.picture) {
          this.data_edit.picture.fileDelete.push(id);
        }
      }
      this.data_edit.picture.file.splice(index, 1);
      this.image1.splice(index, 1);

      for (let i = 0; i < this.data_edit.picture.file.length; i++) {
        this.preview(fileIndex, this.data_edit.picture.file[i], i);
      }
    }
  }

  preview(fileIndex, file, index) {

    const name = file.fileName;
    const typeFiles = name.match(/\.\w+$/g);
    const typeFile = typeFiles[0].replace('.', '');

    const tempPicture = `data:image/${typeFile};base64,${file.base64File}`;

    if (fileIndex === 1) {
      if (file) {
        this.image1[index] = tempPicture;
      }
    }
  }

  image_click(colId, id) {
    this.showModel = true;
    document.getElementById('myModal').style.display = 'block';
    this.imageModal(colId, id);
  }

  imageModal(fileIndex, index) {
    if (fileIndex === 1) {
      this.modalmainImageIndex = index;
      this.modalImage = this.image1;
      this.modalImagedetail = this.data_edit.picture.file;
      this.indexSelect = index;
      this.fileValue = fileIndex;
    }
  }

  closeModal() {
    this.showModel = false;
    document.getElementById('myModal').style.display = 'none';
  }


  onSelectFile(event) {

    this.OverSize = [];

    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      var file = event.target.files;
      for (let i = 0; i < filesAmount; i++) {
        if (
          // file[i].type === 'image/svg+xml' ||
          file[i].type === 'image/jpeg' ||
          // file[i].type === 'image/raw' ||
          // file[i].type === 'image/svg' ||
          // file[i].type === 'image/tif' ||
          // file[i].type === 'image/gif' ||
          file[i].type === 'image/jpg' ||
          file[i].type === 'image/png' ||
          file[i].type === 'application/doc' ||
          file[i].type === 'application/pdf' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
          file[i].type ===
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
          file[i].type === 'application/pptx' ||
          file[i].type === 'application/docx' ||
          (file[i].type === 'application/ppt' && file[i].size)
        ) {
          if (file[i].size <= 5000000) {
            this.multiple_file(file[i], i);
          } else {
            this.OverSize.push(file[i].name);
          }
        } else {
          alert('File Not Support');
        }
      }
    }
    if (this.OverSize.length !== 0) {
      // console.log('open modal');
      document.getElementById('overfilesize').click();
    }
  }

  multiple_file(file, index) {
    // console.log('download');
    const reader = new FileReader();
    if (file) {
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');
        // this.urls.push({
        const x = {
          fileName: file.name,
          fileSize: file.size,
          base64File: splitFile[1],
        };
        // });
        this.data_edit.attachments.file.push(x);
        // console.log('this.urls', this.data_edit.attachments.file);
        this.dataSource_file = new MatTableDataSource(this.data_edit.attachments.file);
        this.hideDownloadFile();
        // console.log(this.data_edit.attachments.file);
      };
    }
  }

  delete_mutifile(data: any, index: any) {
    // console.log('data:', data, 'index::', index);
    if ('id' in this.data_edit.attachments.file[index]) {
      const id = this.data_edit.attachments.file[index].id;
      // console.log('found id');
      if ('fileDelete' in this.data_edit.attachments) {
        this.data_edit.attachments.fileDelete.push(id);
      }
    }
    // console.log('fileDelete attachments : ', this.data_edit.attachments.fileDelete);
    this.data_edit.attachments.file.splice(index, 1);
    this.dataSource_file = new MatTableDataSource(this.data_edit.attachments.file);
    // console.log(this.data_edit.attachments);
  }

  // downloadFile
  downloadFile(pathdata: any) {
    const contentType = '';
    const sendpath = pathdata;
    // console.log('path', sendpath);
    this.newsAnnouncementService.getfile(sendpath).subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        const datagetfile = res['data'];
        const b64Data = datagetfile['data'];
        const filename = datagetfile['fileName'];
        // console.log('base64', b64Data);
        const config = this.b64toBlob(b64Data, contentType);
        saveAs(config, filename);
      }

    });
  }

  b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const convertbyte = atob(b64Data);
    const byteCharacters = atob(convertbyte);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  downloadFile64(data: any) {
    const contentType = '';
    const b64Data = data.base64File;
    const filename = data.fileName;
    console.log('base64', b64Data);

    const config = this.base64(b64Data, contentType);
    saveAs(config, filename);
  }

  base64(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  hideDownloadFile() {
    if (this.dataSource_file.data.length > 0) {
      this.downloadfile_by = [];

      for (let index = 0; index < this.dataSource_file.data.length; index++) {
        const element = this.dataSource_file.data[index];
        console.log('ele', element);

        if ('path' in element) {
          this.downloadfile_by.push('path');
        } else {
          this.downloadfile_by.push('base64');
        }
      }
    }
    console.log('by:', this.downloadfile_by);
  }


  save() {
    console.log(this.data_edit);
    if (this.data_edit.topic !== '' && this.data_edit.topic !== null
      && this.data_edit.informationDate !== '' && this.data_edit.informationDate !== null
      && this.data_edit.detail !== '' && this.data_edit.detail !== null) {

      const dialogRef = this.dialog.open(NewsSubmitComponent, { disableClose: true, autoFocus: false });
      dialogRef.componentInstance.datasendedit = this.data_edit;
      // sessionStorage.setItem('editDialog', JSON.stringify(this.data_edit));
      sessionStorage.setItem('newsType', 'การแก้ไขการประกาศข่าว');
      dialogRef.afterClosed().subscribe(result => {
        this.dialogRef.close();
      });
    } else {
      alert('กรุณากรอกรายละเอียดให้ครบ');
    }
  }

  // ********************** buddist calendar calendar **********************

  setupCalendar(calendarIndex, option?) {
    // console.log('in setup calendar', this.startDate);
    const templateCalendar = {
      minStartDate: new Date(1900, 0, 1),
      maxStartDate: undefined,
      minEndDate: new Date(1900, 0, 1),
      maxEndDate: undefined,
      selectedDate: '',
      selectedStartDate: new Date(),
      selectedEndDate: new Date(),
      modifiedStartDate: this.dateToBuddha(new Date()),
      modifiedEndDate: this.dateToBuddha(new Date()),
      calendarState: false
    };
    const templateCalendar2 = {
      minStartDate: new Date(1900, 0, 1),
      maxStartDate: undefined,
      minEndDate: new Date(1900, 0, 1),
      maxEndDate: undefined,
      selectedDate: '',
      selectedStartDate: new Date(),
      selectedEndDate: new Date(),
      modifiedStartDate: this.dateToBuddha(new Date()),
      modifiedEndDate: this.dateToBuddha(new Date()),
      calendarState: false
    };
    const templateCalendar3 = {
      minStartDate: new Date(1900, 0, 1),
      maxStartDate: undefined,
      minEndDate: new Date(1900, 0, 1),
      maxEndDate: undefined,
      selectedDate: '',
      selectedStartDate: new Date(),
      selectedEndDate: new Date(),
      modifiedStartDate: this.dateToBuddha(new Date()),
      modifiedEndDate: this.dateToBuddha(new Date()),
      calendarState: false
    };

    this.startDate[calendarIndex] = option ? option : templateCalendar;
    this.endDate[calendarIndex] = option ? option : templateCalendar2;
    this.announceDate[calendarIndex] = option ? option : templateCalendar3;

    // console.log(this.endDate);

  }
  dateToBuddha(dateNormal) {
    // console.log('dateNormal', dateNormal);
    const now = moment(dateNormal);
    now.locale('th');
    const buddhishYear = (parseInt(now.format('YYYY'), 10) + 543).toString();
    return now.format('DD/MM') + '/' + buddhishYear;
  }
  onSelectStart(event, calendarIndex, dateType) {
    // console.log(event);
    this.startDate[calendarIndex].selectedStartDate = event;
    this.startDate[calendarIndex].modifiedStartDate = this.dateToBuddha(event);
    // console.log(this.startDate[calendarIndex].modifiedStartDate);

    this.startDate[calendarIndex].minEndDate = this.startDate[calendarIndex].selectedStartDate;
    // console.log('dd', this.startDate[0].minEndDate);
    // console.log('startDate', this.startDate);
    // this.model = {isRange: false, singleDate: {jsDate: new Date(this.startDate[0].minEndDate), }, };
  }

  onSelectEnd(event, calendarIndex) {
    this.endDate[calendarIndex].selectedStartDate = event;
    this.endDate[calendarIndex].modifiedStartDate = this.dateToBuddha(event);
    // console.log(this.endDate[calendarIndex].modifiedStartDate);

    this.endDate[calendarIndex].minEndDate = this.endDate[calendarIndex].selectedStartDate;
    // console.log('dd', this.endDate[0].minEndDate);
    // console.log('endDate', this.endDate);
  }
  onSelectAnnounce(event, calendarIndex) {
    this.announceDate[calendarIndex].selectedStartDate = event;
    this.announceDate[calendarIndex].modifiedStartDate = this.dateToBuddha(event);
    // console.log(this.announceDate[calendarIndex].modifiedStartDate);

    this.announceDate[calendarIndex].minEndDate = this.announceDate[calendarIndex].selectedStartDate;
    // console.log('dd', this.announceDate[0].minEndDate);
    // console.log('announceDate', this.announceDate);
  }
  openCalendar(calendarIndex, dateType) {
    if (dateType === 'startDate') {
      // console.log('start open calendar', this.startDate);
      this.startDate[calendarIndex].calendarState = true;
      if (this.startDate[calendarIndex].selectedDate) {
        // console.log('in openCalendar', calendarIndex);
        // dd/mm/yyyy -> yyyy-mm-dd
        const splitData = this.startDate[calendarIndex].selectedDate.replace(/\s/g, '').split('-');
        const convertYear = splitData.map(d => {
          return `${(+d.split('/')[2] - 543)}-${d.split('/')[1]}-${d.split('/')[0]}`;
        });

        // set current date when open calendar
        this.startDate[calendarIndex].selectedStartDate = new Date(convertYear[0]);
        this.startDate[calendarIndex].selectedEndDate = new Date(convertYear[1]);
        this.startDate[calendarIndex].modifiedStartDate = splitData[0];
        this.startDate[calendarIndex].modifiedEndDate = splitData[1];
      }
    } else if (dateType === 'endDate') {
      // console.log('in open calendar', this.endDate);
      this.endDate[calendarIndex].calendarState = true;
      if (this.endDate[calendarIndex].selectedDate) {
        // console.log('in openCalendar', calendarIndex);
        // dd/mm/yyyy -> yyyy-mm-dd
        // console.log('in if');
        const splitData = this.endDate[calendarIndex].selectedDate.replace(/\s/g, '').split('-');
        const convertYear = splitData.map(d => {
          return `${(+d.split('/')[2] - 543)}-${d.split('/')[1]}-${d.split('/')[0]}`;
        });

        // set current date when open calendar
        this.endDate[calendarIndex].selectedStartDate = new Date(convertYear[0]);
        this.endDate[calendarIndex].selectedEndDate = new Date(convertYear[1]);
        this.endDate[calendarIndex].modifiedStartDate = splitData[0];
        this.endDate[calendarIndex].modifiedEndDate = splitData[1];
      }
    } else if (dateType === 'announceDate') {
      // console.log('in open calendar', this.announceDate);
      this.announceDate[calendarIndex].calendarState = true;
      if (this.announceDate[calendarIndex].selectedDate) {
        // console.log('in openCalendar', calendarIndex);
        // dd/mm/yyyy -> yyyy-mm-dd
        // console.log('in if');
        const splitData = this.announceDate[calendarIndex].selectedDate.replace(/\s/g, '').split('-');
        const convertYear = splitData.map(d => {
          return `${(+d.split('/')[2] - 543)}-${d.split('/')[1]}-${d.split('/')[0]}`;
        });

        // set current date when open calendar
        this.announceDate[calendarIndex].selectedStartDate = new Date(convertYear[0]);
        this.announceDate[calendarIndex].selectedEndDate = new Date(convertYear[1]);
        this.announceDate[calendarIndex].modifiedStartDate = splitData[0];
        this.announceDate[calendarIndex].modifiedEndDate = splitData[1];
      }
    }
  }

  applyCalendar(calendarIndex, dateType) {
    if (dateType === 'startDate') {
      this.startDate[calendarIndex].selectedDate = `${this.startDate[calendarIndex].modifiedStartDate}`;
      this.startDate[calendarIndex].calendarState = false;
      // console.log(this.startDate);
      this.getLogUserSearchData(dateType);
    } else if (dateType === 'endDate') {
      this.endDate[calendarIndex].selectedDate = `${this.endDate[calendarIndex].modifiedStartDate}`;
      this.endDate[calendarIndex].calendarState = false;
      this.getLogUserSearchData(dateType);
    } else if (dateType === 'announceDate') {
      this.announceDate[calendarIndex].selectedDate = `${this.announceDate[calendarIndex].modifiedStartDate}`;
      this.announceDate[calendarIndex].calendarState = false;
      this.getLogUserSearchData(dateType);
    }
  }
  closeCalendar(calendarIndex, dateType) {
    if (dateType === 'startDate') {
      this.startDate[calendarIndex].calendarState = false;
    } else if (dateType === 'endDate') {
      this.endDate[calendarIndex].calendarState = false;
    } else if (dateType === 'announceDate') {
      this.announceDate[calendarIndex].calendarState = false;
    }
  }

  clearCalendar(calendarIndex, dateType, option?) {
    if (dateType === 'startDate') {
      const templateCalendar = {
        minStartDate: new Date(1900, 0, 1),
        maxStartDate: undefined,
        minEndDate: new Date(1900, 0, 1),
        maxEndDate: undefined,
        selectedDate: '',
        selectedStartDate: new Date(),
        selectedEndDate: new Date(),
        modifiedStartDate: this.dateToBuddha(new Date()),
        modifiedEndDate: this.dateToBuddha(new Date()),
        calendarState: false
      };
      this.startDate[calendarIndex] = option ? option : templateCalendar;
      this.data_edit.informationStartDate = '';
    } else if (dateType === 'endDate') {
      const templateCalendar2 = {
        minStartDate: new Date(1900, 0, 1),
        maxStartDate: undefined,
        minEndDate: new Date(1900, 0, 1),
        maxEndDate: undefined,
        selectedDate: '',
        selectedStartDate: new Date(),
        selectedEndDate: new Date(),
        modifiedStartDate: this.dateToBuddha(new Date()),
        modifiedEndDate: this.dateToBuddha(new Date()),
        calendarState: false
      };
      this.endDate[calendarIndex] = option ? option : templateCalendar2;
      this.data_edit.informationEndDate = '';
    } else if (dateType === 'announceDate') {
      const templateCalendar3 = {
        minStartDate: new Date(1900, 0, 1),
        maxStartDate: undefined,
        minEndDate: new Date(1900, 0, 1),
        maxEndDate: undefined,
        selectedDate: '',
        selectedStartDate: new Date(),
        selectedEndDate: new Date(),
        modifiedStartDate: this.dateToBuddha(new Date()),
        modifiedEndDate: this.dateToBuddha(new Date()),
        calendarState: false
      };
      this.announceDate[calendarIndex] = option ? option : templateCalendar3;
      this.data_edit.informationDate = '';
    }
  }
  getLogUserSearchData(dateType) {
    if (dateType === 'startDate') {
      if (this.startDate[0].selectedDate !== '') {
        this.data_edit.informationStartDate = moment(this.startDate[0].selectedStartDate).format('YYYY-MM-DD');
      } else {
        this.data_edit.informationStartDate = '';
        console.log('null');
      }
    } else if (dateType === 'endDate') {
      if (this.endDate[0].selectedDate !== '') {
        this.data_edit.informationEndDate = moment(this.endDate[0].selectedStartDate).format('YYYY-MM-DD');
      } else {
        this.data_edit.informationEndDate = '';
        console.log('null');
      }
    } else if (dateType === 'announceDate') {
      if (this.announceDate[0].selectedDate !== '') {
        this.data_edit.informationDate = moment(this.announceDate[0].selectedStartDate).format('YYYY-MM-DD');
      } else {
        this.data_edit.informationDate = '';
        console.log('null');
      }
    }
  }

  input_date() { }
}
