import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { from } from 'rxjs';
// ====== chart ========
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { MatCheckboxModule } from '@angular/material/checkbox';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { NewsAnnouncementComponent } from './news-announcement.component';
import { NewsAnnouncementRoutingModule } from '../news-announcement/news-announcement-routing.module';
import { AngularMyDatePickerModule } from 'angular-mydatepicker';
import { NewsEditComponent } from './news-edit/news-edit.component';
import { MatDialogModule } from '@angular/material/dialog';
import { NewsAddComponent } from './news-add/news-add.component';
import { NewsSubmitComponent } from './news-submit/news-submit.component';
import { MatNativeDateModule } from '@angular/material';

import { DateModule } from '../datepicker/date/date.module';

@NgModule({
  declarations: [NewsAnnouncementComponent, NewsEditComponent, NewsAddComponent, NewsSubmitComponent],
  imports: [
    CommonModule,
    NewsAnnouncementRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatSelectModule,
    MatDatepickerModule,
    ModalModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMyDatePickerModule,
    MatCheckboxModule,
    MatDialogModule,
    MatNativeDateModule,
    DateModule
  ],
  entryComponents: [NewsEditComponent, NewsAddComponent, NewsSubmitComponent],
  providers: [
    DatePipe,
    MatDatepickerModule,
    MatNativeDateModule,

  ]

})
export class NewsAnnouncementModule { }
