import { Component, OnInit } from '@angular/core';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
  CdkDragExit,
  CdkDragEnter,
  CdkDragStart,
  CdkDragEnd,
} from '@angular/cdk/drag-drop';
import { SysManagementRulesService } from '../../services/sys-management-rules/sys-management-rules.service';
import { Router } from '@angular/router';
import {
  MatDialogRef,
  MatDialog,
  MAT_DIALOG_DATA,
  MatDialogConfig,
} from '@angular/material';
import { SavedComponent } from '../system-management-cus/saved/saved.component';
import { Observable } from 'rxjs/Observable';
import { DialogSaveStatusSystemCusComponent } from './dialog-save-status-system-cus/dialog-save-status-system-cus.component';
import { CanDeactivateGuard } from './../../services/configGuard/config-guard.service';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-system-management-cus',
  templateUrl: './system-management-cus.component.html',
  styleUrls: ['./system-management-cus.component.scss'],
})
export class SystemManagementCusComponent implements OnInit {
  // drag and drop

  saveDraftstatus = null;

  System = {
    account: [
      {
        id: '',
        regulation: 'ขั้นตอน A',
        topic: [
          {
            id: '5',
            topic: 'หัวข้อ',
            detail: [
              {
                id: '7',
                detail: 'ประเด็น2',
              },
            ],
          },
        ],
      },
    ],
    accountDelete: [],
    accountTopicDelete: [],
    accountDetailDelete: [],

    department1: [
      {
        id: '',
        department: '',
        bookNumber: '',
        topic: [
          {
            id: '1',
            topic: 'หัวข้อ',
            detail: [
              {
                id: '4',
                detail: '',
              },
            ],
          },
        ],
      },
    ],
    department1Delete: [],
    department1TopicDelete: [],
    department1DetailDelete: [],

    department2: [
      {
        id: '',
        department: '',
        bookNumber: '',
        topic: [
          {
            id: '2',
            topic: 'หัวข้อ',
            detail: [
              {
                id: '4',
                detail: '',
              },
            ],
          },
        ],
      },
    ],
    department2Delete: [],
    department2TopicDelete: [],
    department2DetailDelete: [],

    rule: [
      {
        id: '',
        department: '',
        bookNumber: '',
        topic: [
          {
            id: '4',
            topic: 'หัวข้อ',
            detail: [
              {
                id: '4',
                detail: '',
              },
            ],
          },
        ],
      },
    ],
    ruleDelete: [],
    ruleTopicDelete: [],
    ruleDetailDelete: [],

    law: [
      {
        id: '',
        department: '',
        bookNumber: '',
        topic: [
          {
            id: '6',
            topic: 'หัวข้อ',
            detail: [
              {
                id: '4',
                detail: '',
              },
            ],
          },
        ],
      },
    ],
    lawDelete: [],
    lawTopicDelete: [],
    lawDetailDelete: [],
  };

  // Risk sub_table
  Risk_dropIndex = 0;
  staticTitleIndexRisk = [];
  dropIndex: number;
  staticTitleIndex = [];

  // Risk sub_table
  Risk_dropIndex_1 = 0;
  staticTitleIndexRisk_1 = [];
  dropIndex_1: number;
  staticTitleIndex_1 = [];

  // Risk sub_table
  Risk_dropIndex_2 = 0;
  staticTitleIndexRisk_2 = [];
  dropIndex_2: number;
  staticTitleIndex_2 = [];

  // Risk sub_table
  Risk_dropIndex_of = 0;
  staticTitleIndexRisk_of = [];
  dropIndex_of: number;
  staticTitleIndex_of = [];

  // Risk sub_table
  Risk_dropIndex_law = 0;
  staticTitleIndexRisk_law = [];
  dropIndex_law: number;
  staticTitleIndex_law = [];

  // ===============================================================================================
  status_loading = false;
  saveDraftstatus_State = false;

  constructor(
    private SysManagementRules: SysManagementRulesService,
    private router: Router,
    private dialog: MatDialog,
    private guardService: CanDeactivateGuard
  ) { }

  ngOnInit() {
    this.status_loading = true;
    this.saveDraftstatus_State = false;
    this.SysManagementRules.GetDetailComplianceLegal().subscribe((res) => {

      console.log('res::', res);
      this.System.account = res['data'].account;
      this.System.department1 = res['data'].department1;
      this.System.department2 = res['data'].department2;
      this.System.rule = res['data'].rule;
      this.System.law = res['data'].law;
      // ===========================Gen id ไม่ซ้ำกัน ====================//
      // ======account=======//
      for (let index = 0; index < this.System.account.length; index++) {
        for (let k = 0; k < this.System.account[index].topic.length; k++) {
          const topic = this.System.account[index].topic[k].id;
          this.System.account[index].topic[k].id = topic + '/account';
        }
      }
      // ======department1=======//
      for (let index = 0; index < this.System.department1.length; index++) {
        for (let k = 0; k < this.System.department1[index].topic.length; k++) {
          const topic = this.System.department1[index].topic[k].id;
          this.System.department1[index].topic[k].id = topic + '/department1';
        }
      }
      // ======department2=======//
      for (let index = 0; index < this.System.department2.length; index++) {
        for (let k = 0; k < this.System.department2[index].topic.length; k++) {
          const topic = this.System.department2[index].topic[k].id;
          this.System.department2[index].topic[k].id = topic + '/department2';
        }
      }
      // ======rule=======//
      for (let index = 0; index < this.System.rule.length; index++) {
        for (let k = 0; k < this.System.rule[index].topic.length; k++) {
          const topic = this.System.rule[index].topic[k].id;
          this.System.rule[index].topic[k].id = topic + '/rule';
        }
      }
      // ======law=======//
      for (let index = 0; index < this.System.law.length; index++) {
        for (let k = 0; k < this.System.law[index].topic.length; k++) {
          const topic = this.System.law[index].topic[k].id;
          this.System.law[index].topic[k].id = topic + '/law';
        }
      }
      this.status_loading = false
    });
  }
  getData() {
    this.status_loading = true;
    this.SysManagementRules.GetDetailComplianceLegal().subscribe((res) => {

      console.log('res::', res);
      this.System.account = res['data'].account;
      this.System.department1 = res['data'].department1;
      this.System.department2 = res['data'].department2;
      this.System.rule = res['data'].rule;
      this.System.law = res['data'].law;
      // ===========================Gen id ไม่ซ้ำกัน ====================//
      // ======account=======//
      for (let index = 0; index < this.System.account.length; index++) {
        for (let k = 0; k < this.System.account[index].topic.length; k++) {
          const topic = this.System.account[index].topic[k].id;
          this.System.account[index].topic[k].id = topic + '/account';
        }
      }
      // ======department1=======//
      for (let index = 0; index < this.System.department1.length; index++) {
        for (let k = 0; k < this.System.department1[index].topic.length; k++) {
          const topic = this.System.department1[index].topic[k].id;
          this.System.department1[index].topic[k].id = topic + '/department1';
        }
      }
      // ======department2=======//
      for (let index = 0; index < this.System.department2.length; index++) {
        for (let k = 0; k < this.System.department2[index].topic.length; k++) {
          const topic = this.System.department2[index].topic[k].id;
          this.System.department2[index].topic[k].id = topic + '/department2';
        }
      }
      // ======rule=======//
      for (let index = 0; index < this.System.rule.length; index++) {
        for (let k = 0; k < this.System.rule[index].topic.length; k++) {
          const topic = this.System.rule[index].topic[k].id;
          this.System.rule[index].topic[k].id = topic + '/rule';
        }
      }
      // ======law=======//
      for (let index = 0; index < this.System.law.length; index++) {
        for (let k = 0; k < this.System.law[index].topic.length; k++) {
          const topic = this.System.law[index].topic[k].id;
          this.System.law[index].topic[k].id = topic + '/law';
        }
      }
      this.status_loading = false;
    });
  }
  saveDataAll() { }
  changeSaveDraft() { }
  backpage() {
    localStorage.setItem('requestId', '');
    this.router.navigate(['dashboard1']);
  }
  CheckEmptyValue() {
    const EmptyArryAccount = []
    for (let i = 0; i < this.System.account.length; i++) {
      if (this.System.account[i].id == '' && this.System.account[i].regulation == '') {
        for (let k = 0; k < this.System.account[i].topic.length; k++) {
          if (this.System.account[i].topic[k].topic == '' && typeof (this.System.account[i].topic[k].id) == 'string') {
            for (let p = 0; p < this.System.account[i].topic[k].detail.length; p++) {
              if (this.System.account[i].topic[k].detail[p].id == '' && this.System.account[i].topic[k].detail[p].detail == '') {
                EmptyArryAccount.push(this.System.account[i])
                break;
              }
            }
          }

        }
      }
    }
    this.System.account = this.System.account.filter(function (val) {
      return EmptyArryAccount.indexOf(val) == -1;
    });
    console.log(EmptyArryAccount);
    console.log(this.System.account);
    // ================================================================================================//
    const EmptyArryDepartment1 = []
    for (let i = 0; i < this.System.department1.length; i++) {
      if (this.System.department1[i].id == '' && this.System.department1[i].department == '' &&
        this.System.department1[i].bookNumber == '') {
        for (let k = 0; k < this.System.department1[i].topic.length; k++) {
          if (this.System.department1[i].topic[k].topic == '' && typeof (this.System.department1[i].topic[k].id) == 'string') {
            for (let p = 0; p < this.System.department1[i].topic[k].detail.length; p++) {
              if (this.System.department1[i].topic[k].detail[p].id == '' && this.System.department1[i].topic[k].detail[p].detail == '') {
                EmptyArryDepartment1.push(this.System.department1[i])
                break;
              }
            }
          }

        }
      }
    }
    this.System.department1 = this.System.department1.filter(function (val) {
      return EmptyArryDepartment1.indexOf(val) == -1;
    });
    console.log(EmptyArryDepartment1);
    console.log(this.System.department1);
    // ================================================================================================//
    const EmptyArryDepartment2 = []
    for (let i = 0; i < this.System.department2.length; i++) {
      if (this.System.department2[i].id == '' && this.System.department2[i].department == '' &&
        this.System.department2[i].bookNumber == '') {
        for (let k = 0; k < this.System.department2[i].topic.length; k++) {
          if (this.System.department2[i].topic[k].topic == '' && typeof (this.System.department2[i].topic[k].id) == 'string') {
            for (let p = 0; p < this.System.department2[i].topic[k].detail.length; p++) {
              if (this.System.department2[i].topic[k].detail[p].id == '' && this.System.department2[i].topic[k].detail[p].detail == '') {
                EmptyArryDepartment2.push(this.System.department2[i])
                break;
              }
            }
          }

        }
      }
    }
    this.System.department2 = this.System.department2.filter(function (val) {
      return EmptyArryDepartment2.indexOf(val) == -1;
    });
    console.log(EmptyArryDepartment2);
    console.log(this.System.department2);
    // ================================================================================================//
    const EmptyArryRule = []
    for (let i = 0; i < this.System.rule.length; i++) {
      if (this.System.rule[i].id == '' && this.System.rule[i].department == '' &&
        this.System.rule[i].bookNumber == '') {
        for (let k = 0; k < this.System.rule[i].topic.length; k++) {
          if (this.System.rule[i].topic[k].topic == '' && typeof (this.System.rule[i].topic[k].id) == 'string') {
            for (let p = 0; p < this.System.rule[i].topic[k].detail.length; p++) {
              if (this.System.rule[i].topic[k].detail[p].id == '' && this.System.rule[i].topic[k].detail[p].detail == '') {
                EmptyArryRule.push(this.System.rule[i])
                break;
              }
            }
          }

        }
      }
    }
    this.System.rule = this.System.rule.filter(function (val) {
      return EmptyArryRule.indexOf(val) == -1;
    });
    console.log(EmptyArryRule);
    console.log(this.System.rule);
    // ================================================================================================//
    const EmptyArryLaw = []
    for (let i = 0; i < this.System.law.length; i++) {
      if (this.System.law[i].id == '' && this.System.law[i].department == '' &&
        this.System.law[i].bookNumber == '') {
        for (let k = 0; k < this.System.law[i].topic.length; k++) {
          if (this.System.law[i].topic[k].topic == '' && typeof (this.System.law[i].topic[k].id) == 'string') {
            for (let p = 0; p < this.System.law[i].topic[k].detail.length; p++) {
              if (this.System.law[i].topic[k].detail[p].id == '' && this.System.law[i].topic[k].detail[p].detail == '') {
                EmptyArryLaw.push(this.System.law[i])
                break;
              }
            }
          }

        }
      }
    }
    this.System.law = this.System.law.filter(function (val) {
      return EmptyArryLaw.indexOf(val) == -1;
    });
    console.log(EmptyArryLaw);
    console.log(this.System.law)
    // =======================================================================================================
  }
  openSaved() {
    // this.status_loading = true;
    console.log(this.System);
    this.CheckEmptyValue()
    if (this.CheckValueAccount() === true &&
      this.CheckValueDepartment1() === true &&
      this.CheckValueDepartment2() === true &&
      this.CheckValueRule() === true &&
      this.CheckValueLaw() === true) {
      const dialogData = {
        data: this.System,
        status: null
      };
      console.log('account:', this.System.account)
      const Save = this.dialog.open(SavedComponent, dialogData);

      Save.afterClosed().subscribe((data) => {
        console.log('_response:', data);
        if (data.status == 'save') {
          this.ngOnInit();
        } else if (data.status == 'close') {
          this.ngOnInit();
        } else {

        }
        // console.log('closed:', this.edit_data);
        // tslint:disable-next-line: no-shadowed-variable
        // this.ngOnInit();
      });
    } else {
      const Alert = [];
      if (this.CheckValueAccount() === false) {
        Alert.push('ฝ่ายกำกับระเบียบและการปฏิบัติงาน')
      }
      if (this.CheckValueDepartment1() === false) {
        Alert.push('ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ธุรกิจ 1')
      }
      if (this.CheckValueDepartment2() === false) {
        Alert.push('ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ธุรกิจ 2')
      }
      if (this.CheckValueRule() === false) {
        Alert.push('ฝ่ายกำกับการปฏิบัติตามกฎเกณฑ์ทางการ')
      }
      if (this.CheckValueLaw() === false) {
        Alert.push('ฝ่ายนิติการ')
      }
      // console.log('dataSave::', this.System)
      // this.CheckEmptyValue()
      alert('กรุณากรอกข้อมูลตาราง ' + Alert + ' ให้ครบถ้วน');
    }

  }
  // new table ================================================================================
  checkStaticTitle(index) {
    return this.staticTitleIndex.includes(index);
  }

  drop(event: CdkDragDrop<string[]>) {
    const fixedBody = this.staticTitleIndex;
    console.log('event Group:', event);
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (event.previousContainer.data.length > 1) {
      // check if only one left child
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (fixedBody.includes(this.dropIndex)) {
      // check only 1 child left and static type move only not delete
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      const templatesubTitleRisk = {
        id: this.makeid(7),
        topic: '',
        detail: [
          {
            id: '',
            detail: '',
          },
        ],
      };

      this.System.account[this.dropIndex].topic.push(templatesubTitleRisk);
    } else {
      // check only 1 child left and not a static type move and delete
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      // remove current row
      this.removeAddItem(this.dropIndex);
      this.dropIndex = null;
    }
    this.changeSaveDraft();
    this.changeSaveDraftStatus();
  }

  dragExited(event: CdkDragExit<string[]>) {
    console.log('Exited', event);
    this.changeSaveDraftStatus();
  }

  entered(event: CdkDragEnter<string[]>) {
    console.log('Entered', event);
  }

  startDrag(event: CdkDragStart<string[]>, istep) {
    // console.log('startDrag', event);
    // console.log('startDrag index', istep);
    this.dropIndex = istep;
  }

  removeAddItem(index) {
    if (index != null) {
      this.System.accountDelete.push(this.System.account[index].id);
      this.System.account.splice(index, 1);
    }
  }

  // ==================================================================
  dropItem(event: CdkDragDrop<string[]>) {
    console.log('event item :', event);
    console.log('previousContainer :', event.container.data.length);
    console.log('index group', this.dropIndex);
    console.log('index item:', this.Risk_dropIndex);
    const fixedBody = this.staticTitleIndexRisk;

    // if (
    //   event.previousContainer.data.length === 1 &&
    //   event.container.data.length !== 1
    // ) {
    //   console.log('index:', this.dropIndex);
    //   this.removeAddItem(this.dropIndex);
    //   this.dropIndex = null;
    // }
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (event.previousContainer.data.length > 1) {
      // check if only one left child
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      // check only 1 child left and not a static type move and delete
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      // remove current row
      this.removeAddItemRisk(this.dropIndex, this.Risk_dropIndex);
      this.dropIndex = null;
      this.Risk_dropIndex = null;
    }
    this.changeSaveDraft();
  }

  connectItem(): any[] {
    const mapping = [];
    for (let index = 0; index < this.System.account.length; index++) {
      const Group = this.System.account[index].topic;

      // tslint:disable-next-line: no-shadowed-variable
      // this.System.operations[index].topic.forEach(
      //   ( item, i) =>
      //    mapping.push(`${this.System.operations[index].topic[i].id}`)
      // );

      for (let j = 0; j < Group.length; j++) {
        // mapping.push(String(Group[j].id + 'account') );
        mapping.push(`${Group[j].id}`);
      }
    }

    // console.log('map', mapping);
    return mapping;
  }

  removeAddItemRisk(subIndex, Riskkindex) {
    // ======account=======//

    this.System.account[subIndex].topic[Riskkindex].id = this.System.account[
      subIndex
    ].topic[Riskkindex].id
      .split('/account')
      .join('');

    if (this.System.account[subIndex].topic.length === 1) {
      this.System.accountDelete.push(this.System.account[subIndex].id);
      this.System.account.splice(subIndex, 1);
    } else if (
      this.System.account[subIndex].topic.length > 1 &&
      Riskkindex != null
    ) {
      this.System.accountTopicDelete.push(
        this.System.account[subIndex].topic[Riskkindex].id
      );
      this.System.account[subIndex].topic.splice(Riskkindex, 1);
    }
  }

  startDrag_Risk(event: CdkDragStart<string[]>, isup, istep) {
    console.log('Risks tartDrag', event);
    console.log('Risk startDrag index', isup);
    this.Risk_dropIndex = isup;
    this.dropIndex = istep;
  }
  enteredRisk(event: CdkDragEnter<string[]>) {
    console.log('Entered Risk', event);
  }
  dragExitedRisk(event: CdkDragExit<string[]>) {
    console.log('Exited Risk', event);
  }

  dragEnded(event: CdkDragEnd) {
    console.log('dragEnded Event > item', event.source.data);
  }

  checkStaticTitle_Risk(index) {
    return this.staticTitleIndexRisk.includes(index);
  }
  // ===========================================================================================================
  removeDataItem(stepindex, subindex, riskIndex) {
    console.log('stepindex', stepindex);
    console.log('subindex', subindex);
    console.log('riskIndex', riskIndex);
    console.log(this.System)

    console.log(this.System.account[stepindex].topic[subindex].id)
    const GroupId = this.System.account[stepindex].topic[subindex].id
      .split('/account')
      .join('');
    this.System.account[stepindex].topic[subindex].id = GroupId;
    const fixedBody = [];
    // tslint:disable-next-line: max-line-length
    if (
      fixedBody.includes(stepindex) &&
      this.System.account[stepindex].topic[subindex].detail.length > 1 &&
      this.Risk_dropIndex !== 0
    ) {
      console.log('delete fixed row only have many risk[]');
      // this.System.operations[stepindex].detailDelete.push(this.System.operations[stepindex].topic[subindex]['id']);
      // console.log(
      //   this.System.processConsiders[stepindex].topic[subindex]
      //     .detail[riskIndex].id
      // );
      if (
        this.System.account[stepindex].topic[subindex].detail[riskIndex].id !==
        ''
      ) {
        this.System.accountDetailDelete.push(
          this.System.account[stepindex].topic[subindex].detail[riskIndex].id
        );
      }

      this.System.account[stepindex].topic[subindex].detail.splice(
        riskIndex,
        1
      );
    } else if (
      fixedBody.includes(stepindex) &&
      this.System.account[stepindex].topic[subindex].detail.length === 1
    ) {
      console.log('3 clear fixed row only');

      if (subindex !== 0) {
        if (this.System.account[stepindex].topic[subindex].id !== '') {
          this.System.accountTopicDelete.push(
            this.System.account[stepindex].topic[subindex].id
          );
        }
        this.System.account[stepindex].topic.splice(subindex, 1);
      } else {
        this.System.accountTopicDelete.push(
          this.System.account[stepindex].topic[subindex].id
        );
        this.System.account[stepindex].regulation = '';
        Object.keys(this.System.account[stepindex].topic[subindex]).forEach(
          (key, index) => {
            if (key === 'detail') {
              // tslint:disable-next-line: no-shadowed-variable
              for (
                // tslint:disable-next-line: no-shadowed-variable
                let index = 0;
                index <
                this.System.account[stepindex].topic[subindex][key].length;
                index++
              ) {
                Object.keys(
                  this.System.account[stepindex].topic[subindex][key][riskIndex]
                ).forEach((sub, i) => {
                  this.System.account[stepindex].topic[subindex][key][index][
                    sub
                  ] = '';
                });
              }
            } else {
              this.System.account[stepindex].topic[subindex][key] = '';
            }
          }
        );
      }

      // console.log(
      //   'tt',
      //   this.System.operations[stepindex].topic[subindex]
      // );

      // this.System.operations[stepindex].detailDelete.push(
      //   this.System.operations[stepindex].topic[subindex]['id']
      // );
    } else if (
      this.System.account[stepindex].topic.length === 1 &&
      this.System.account[stepindex].topic[subindex].detail.length > 1
    ) {
      console.log(' delete risk row only have 1 row');
      if (
        this.System.account[stepindex].topic[subindex].detail[riskIndex].id !==
        ''
      ) {
        this.System.accountDetailDelete.push(
          this.System.account[stepindex].topic[subindex].detail[riskIndex].id
        );
      }
      this.System.account[stepindex].topic[subindex].detail.splice(
        riskIndex,
        1
      );
    } else if (
      this.System.account[stepindex].topic.length > 1 &&
      this.System.account[stepindex].topic[subindex].detail.length > 1
    ) {
      console.log(' delete risk row only have many row');
      // this.System.operations[stepindex].detailDelete.push(
      //   this.System.operations[stepindex].topic[subindex]['id']
      // );
      if (
        this.System.account[stepindex].topic[subindex].detail[riskIndex].id !==
        ''
      ) {
        this.System.accountDetailDelete.push(
          this.System.account[stepindex].topic[subindex].detail[riskIndex].id
        );
      }
      this.System.account[stepindex].topic[subindex].detail.splice(
        riskIndex,
        1
      );
    } else if (
      this.System.account[stepindex].topic[subindex].detail.length === 0
    ) {
      console.log('delete row permanent', this.System.account[stepindex].id);
      // this.System.operationsDelete.push(
      //   this.System.operations[stepindex].id
      // );
      if (this.System.account[stepindex].id !== '') {
        this.System.accountDelete.push(this.System.account[stepindex].id);
        this.System.account.splice(stepindex, 1);
      }
    } else if (
      this.System.account[stepindex].topic[subindex].detail.length === 1 &&
      subindex !== 0 &&
      this.System.account[stepindex].topic.length > 0
    ) {
      console.log('delete row have 1 sub , 1 risk');
      if (this.System.account[stepindex].topic[subindex].id !== '') {
        this.System.accountTopicDelete.push(
          this.System.account[stepindex].topic[subindex].id
        );
        this.System.account[stepindex].topic.splice(subindex, 1);
      }
    } else if (
      this.System.account[stepindex].topic[subindex].detail.length === 1 &&
      subindex === 0 &&
      this.System.account[stepindex].topic.length === 1
    ) {
      console.log('delete row have 1 sub , 1 risk and delete main row');
      if (this.System.account[stepindex].id !== '') {
        this.System.accountDelete.push(this.System.account[stepindex].id);
      }
      this.System.account.splice(stepindex, 1);
    } else if (
      this.System.account[stepindex].topic[subindex].detail.length === 1 &&
      subindex === 0 &&
      this.System.account[stepindex].topic.length > 1
    ) {
      console.log('delete row have many sub , 1 risk and delete 1 sub ');
      if (this.System.account[stepindex].topic[subindex].id !== '') {
        this.System.accountTopicDelete.push(
          this.System.account[stepindex].topic[subindex].id
        );
      }
      this.System.account[stepindex].topic.splice(subindex, 1);
    } else {
      console.log('not in case');
    }
    this.changeSaveDraft();
    this.changeSaveDraftStatus();
  }
  // ===========================================================================================================//
  addRow() {
    const tempateAddRow = {
      id: '',
      regulation: '',
      topic: [
        {
          id: this.makeid(7),
          topic: '',
          detail: [
            {
              id: '',
              detail: '',
            },
          ],
        },
      ],
    };
    this.System.account.push(tempateAddRow);
    this.changeSaveDraftStatus();
  }
  makeid(length) {
    let result = '';
    const characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  // ======================================================================================================================//
  // new table ================================================================================
  checkStaticTitle_1(index) {
    return this.staticTitleIndex_1.includes(index);
  }

  drop_1(event: CdkDragDrop<string[]>) {
    const fixedBody = this.staticTitleIndex_1;
    console.log('event Group:', event);
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (event.previousContainer.data.length > 1) {
      // check if only one left child
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (fixedBody.includes(this.dropIndex)) {
      // check only 1 child left and static type move only not delete
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      const templatesubTitleRisk = {
        id: this.makeid(7),
        topic: '',
        detail: [
          {
            id: '',
            detail: '',
          },
        ],
      };

      this.System.department1[this.dropIndex_1].topic.push(
        templatesubTitleRisk
      );
    } else {
      // check only 1 child left and not a static type move and delete
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      // remove current row
      this.removeAddItem_1(this.dropIndex_1);
      this.dropIndex_1 = null;
    }
    this.changeSaveDraft();
  }

  startDrag_1(event: CdkDragStart<string[]>, istep) {
    // console.log('startDrag', event);
    // console.log('startDrag index', istep);
    this.dropIndex_1 = istep;
  }

  removeAddItem_1(index) {
    if (index != null) {
      this.System.department1Delete.push(this.System.department1[index].id);
      this.System.department1.splice(index, 1);
    }
  }

  // ==================================================================
  dropItem_1(event: CdkDragDrop<string[]>) {
    console.log('event item :', event);
    console.log('previousContainer :', event.container.data.length);
    console.log('index group', this.dropIndex_1);
    console.log('index item:', this.Risk_dropIndex_1);
    const fixedBody = this.staticTitleIndexRisk_1;

    // if (
    //   event.previousContainer.data.length === 1 &&
    //   event.container.data.length !== 1
    // ) {
    //   console.log('index:', this.dropIndex);
    //   this.removeAddItem(this.dropIndex);
    //   this.dropIndex = null;
    // }
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (event.previousContainer.data.length > 1) {
      // check if only one left child
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      // check only 1 child left and not a static type move and delete
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      // remove current row
      this.removeAddItemRisk_1(this.dropIndex_1, this.Risk_dropIndex_1);
      this.dropIndex_1 = null;
      this.Risk_dropIndex_1 = null;
    }
    this.changeSaveDraft();
  }

  connectItem_1(): any[] {
    const mapping = [];
    for (let index = 0; index < this.System.department1.length; index++) {
      const Group = this.System.department1[index].topic;

      // tslint:disable-next-line: no-shadowed-variable
      // this.System.operations[index].topic.forEach(
      //   ( item, i) =>
      //    mapping.push(`${this.System.operations[index].topic[i].id}`)
      // );

      for (let j = 0; j < Group.length; j++) {
        // mapping.push(String(Group[j].id) + 'department1');
        mapping.push(`${Group[j].id}`);
      }
    }

    // console.log('map', mapping);
    return mapping;
  }

  removeAddItemRisk_1(subIndex, Riskkindex) {
    const GroupId = this.System.department1[subIndex].topic[Riskkindex].id
      .split('/department1')
      .join('');
    this.System.department1[subIndex].topic[Riskkindex].id = GroupId;

    if (this.System.department1[subIndex].topic.length === 1) {
      this.System.department1Delete.push(this.System.department1[subIndex].id);
      this.System.department1.splice(subIndex, 1);
    } else if (
      this.System.department1[subIndex].topic.length > 1 &&
      Riskkindex != null
    ) {
      this.System.department1TopicDelete.push(
        this.System.department1[subIndex].topic[Riskkindex].id
      );
      this.System.department1[subIndex].topic.splice(Riskkindex, 1);
    }
  }

  startDrag_Risk_1(event: CdkDragStart<string[]>, isup, istep) {
    console.log('Risks tartDrag', event);
    console.log('Risk startDrag index', isup);
    this.Risk_dropIndex_1 = isup;
    this.dropIndex_1 = istep;
  }

  checkStaticTitle_Risk_1(index) {
    return this.staticTitleIndexRisk_1.includes(index);
  }
  // ===========================================================================================================
  removeDataItem_1(stepindex, subindex, riskIndex) {
    console.log(this.System.department1[stepindex].topic[subindex].id);
    const GroupId = this.System.department1[stepindex].topic[subindex].id
      .split('/department1')
      .join('');
    this.System.department1[stepindex].topic[subindex].id = GroupId;

    console.log('stepindex', stepindex);
    console.log('subindex', subindex);
    console.log('riskIndex', riskIndex);
    const fixedBody = [];
    // tslint:disable-next-line: max-line-length
    if (
      fixedBody.includes(stepindex) &&
      this.System.department1[stepindex].topic[subindex].detail.length > 1 &&
      this.Risk_dropIndex_1 !== 0
    ) {
      console.log('delete fixed row only have many risk[]');
      // this.System.operations[stepindex].detailDelete.push(this.System.operations[stepindex].topic[subindex]['id']);
      // console.log(
      //   this.System.processConsiders[stepindex].topic[subindex]
      //     .detail[riskIndex].id
      // );
      if (
        this.System.department1[stepindex].topic[subindex].detail[riskIndex]
          .id !== ''
      ) {
        this.System.department1DetailDelete.push(
          this.System.department1[stepindex].topic[subindex].detail[riskIndex]
            .id
        );
      }

      this.System.department1[stepindex].topic[subindex].detail.splice(
        riskIndex,
        1
      );
    } else if (
      fixedBody.includes(stepindex) &&
      this.System.department1[stepindex].topic[subindex].detail.length === 1
    ) {
      console.log('3 clear fixed row only');

      if (subindex !== 0) {
        if (this.System.department1[stepindex].topic[subindex].id !== '') {
          this.System.department1TopicDelete.push(
            this.System.department1[stepindex].topic[subindex].id
          );
        }
        this.System.department1[stepindex].topic.splice(subindex, 1);
      } else {
        this.System.department1TopicDelete.push(
          this.System.department1[stepindex].topic[subindex].id
        );
        this.System.department1[stepindex].department = '';
        this.System.department1[stepindex].bookNumber = '';
        Object.keys(this.System.department1[stepindex].topic[subindex]).forEach(
          (key, index) => {
            if (key === 'detail') {
              // tslint:disable-next-line: no-shadowed-variable
              for (
                // tslint:disable-next-line: no-shadowed-variable
                let index = 0;
                index <
                this.System.department1[stepindex].topic[subindex][key].length;
                index++
              ) {
                Object.keys(
                  this.System.department1[stepindex].topic[subindex][key][
                  riskIndex
                  ]
                ).forEach((sub, i) => {
                  this.System.department1[stepindex].topic[subindex][key][
                    index
                  ][sub] = '';
                });
              }
            } else {
              this.System.department1[stepindex].topic[subindex][key] = '';
            }
          }
        );
      }

      // console.log(
      //   'tt',
      //   this.System.operations[stepindex].topic[subindex]
      // );

      // this.System.operations[stepindex].detailDelete.push(
      //   this.System.operations[stepindex].topic[subindex]['id']
      // );
    } else if (
      this.System.department1[stepindex].topic.length === 1 &&
      this.System.department1[stepindex].topic[subindex].detail.length > 1
    ) {
      console.log(' delete risk row only have 1 row');
      if (
        this.System.department1[stepindex].topic[subindex].detail[riskIndex]
          .id !== ''
      ) {
        this.System.department1DetailDelete.push(
          this.System.department1[stepindex].topic[subindex].detail[riskIndex]
            .id
        );
      }
      this.System.department1[stepindex].topic[subindex].detail.splice(
        riskIndex,
        1
      );
    } else if (
      this.System.department1[stepindex].topic.length > 1 &&
      this.System.department1[stepindex].topic[subindex].detail.length > 1
    ) {
      console.log(' delete risk row only have many row');
      // this.System.operations[stepindex].detailDelete.push(
      //   this.System.operations[stepindex].topic[subindex]['id']
      // );
      if (
        this.System.department1[stepindex].topic[subindex].detail[riskIndex]
          .id !== ''
      ) {
        this.System.department1DetailDelete.push(
          this.System.department1[stepindex].topic[subindex].detail[riskIndex]
            .id
        );
      }
      this.System.department1[stepindex].topic[subindex].detail.splice(
        riskIndex,
        1
      );
    } else if (
      this.System.department1[stepindex].topic[subindex].detail.length === 0
    ) {
      console.log(
        'delete row permanent',
        this.System.department1[stepindex].id
      );
      // this.System.operationsDelete.push(
      //   this.System.operations[stepindex].id
      // );
      if (this.System.department1[stepindex].id !== '') {
        this.System.department1Delete.push(
          this.System.department1[stepindex].id
        );
        this.System.department1.splice(stepindex, 1);
      }
    } else if (
      this.System.department1[stepindex].topic[subindex].detail.length === 1 &&
      subindex !== 0 &&
      this.System.department1[stepindex].topic.length > 0
    ) {
      console.log('delete row have 1 sub , 1 risk');
      if (this.System.department1[stepindex].topic[subindex].id !== '') {
        this.System.department1DetailDelete.push(
          this.System.department1[stepindex].topic[subindex].id
        );
        this.System.department1[stepindex].topic.splice(subindex, 1);
      }
    } else if (
      this.System.department1[stepindex].topic[subindex].detail.length === 1 &&
      subindex === 0 &&
      this.System.department1[stepindex].topic.length === 1
    ) {
      console.log('delete row have 1 sub , 1 risk and delete main row');
      if (this.System.department1[stepindex].id !== '') {
        this.System.department1Delete.push(
          this.System.department1[stepindex].id
        );
      }
      this.System.department1.splice(stepindex, 1);
    } else if (
      this.System.department1[stepindex].topic[subindex].detail.length === 1 &&
      subindex === 0 &&
      this.System.department1[stepindex].topic.length > 1
    ) {
      console.log('delete row have many sub , 1 risk and delete 1 sub ');
      if (this.System.department1[stepindex].topic[subindex].id !== '') {
        this.System.department1TopicDelete.push(
          this.System.department1[stepindex].topic[subindex].id
        );
      }
      this.System.department1[stepindex].topic.splice(subindex, 1);
    } else {
      console.log('not in case');
    }
    this.changeSaveDraft();
    this.changeSaveDraftStatus();
  }

  addRow_1() {
    const tempateAddRow = {
      id: '',
      department: '',
      bookNumber: '',
      topic: [
        {
          id: this.makeid(7),
          topic: '',
          detail: [
            {
              id: '',
              detail: '',
            },
          ],
        },
      ],
    };
    this.System.department1.push(tempateAddRow);
    this.changeSaveDraftStatus();
  }

  // ======================================================================================================================//

  checkStaticTitle_2(index) {
    return this.staticTitleIndex_2.includes(index);
  }

  drop_2(event: CdkDragDrop<string[]>) {
    const fixedBody = this.staticTitleIndex_2;
    console.log('event Group:', event);
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (event.previousContainer.data.length > 1) {
      // check if only one left child
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (fixedBody.includes(this.dropIndex_2)) {
      // check only 1 child left and static type move only not delete
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      const templatesubTitleRisk = {
        id: this.makeid(7),
        topic: '',
        detail: [
          {
            id: '',
            detail: '',
          },
        ],
      };

      this.System.department2[this.dropIndex_2].topic.push(
        templatesubTitleRisk
      );
    } else {
      // check only 1 child left and not a static type move and delete
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      // remove current row
      this.removeAddItem_2(this.dropIndex_2);
      this.dropIndex_2 = null;
    }
    this.changeSaveDraft();
  }

  startDrag_2(event: CdkDragStart<string[]>, istep) {
    // console.log('startDrag', event);
    // console.log('startDrag index', istep);
    this.dropIndex_2 = istep;
  }

  removeAddItem_2(index) {
    if (index != null) {
      this.System.department2Delete.push(this.System.department2[index].id);
      this.System.department2.splice(index, 1);
    }
  }

  // ==================================================================
  dropItem_2(event: CdkDragDrop<string[]>) {
    console.log('event item :', event);
    console.log('previousContainer :', event.container.data.length);
    console.log('index group', this.dropIndex_2);
    console.log('index item:', this.Risk_dropIndex_2);
    const fixedBody = this.staticTitleIndexRisk_2;

    // if (
    //   event.previousContainer.data.length === 1 &&
    //   event.container.data.length !== 1
    // ) {
    //   console.log('index:', this.dropIndex);
    //   this.removeAddItem(this.dropIndex);
    //   this.dropIndex = null;
    // }
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (event.previousContainer.data.length > 1) {
      // check if only one left child
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      // check only 1 child left and not a static type move and delete
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      // remove current row
      this.removeAddItemRisk_2(this.dropIndex_2, this.Risk_dropIndex_2);
      this.dropIndex_2 = null;
      this.Risk_dropIndex_2 = null;
    }
    this.changeSaveDraft();
  }

  connectItem_2(): any[] {
    const mapping = [];
    for (let index = 0; index < this.System.department2.length; index++) {
      const Group = this.System.department2[index].topic;

      // tslint:disable-next-line: no-shadowed-variable
      // this.System.operations[index].topic.forEach(
      //   ( item, i) =>
      //    mapping.push(`${this.System.operations[index].topic[i].id}`)
      // );

      for (let j = 0; j < Group.length; j++) {
        // mapping.push(String(Group[j].id + 'department2') );
        mapping.push(`${Group[j].id}`);
      }
    }

    // console.log('map', mapping);
    return mapping;
  }

  removeAddItemRisk_2(subIndex, Riskkindex) {
    const GroupId = this.System.department2[subIndex].topic[Riskkindex].id
      .split('/department2')
      .join('');
    this.System.department2[subIndex].topic[Riskkindex].id = GroupId;

    if (this.System.department2[subIndex].topic.length === 1) {
      this.System.department2TopicDelete.push(
        this.System.department2[subIndex].id
      );
      this.System.department2.splice(subIndex, 1);
    } else if (
      this.System.department2[subIndex].topic.length > 1 &&
      Riskkindex != null
    ) {
      this.System.department2TopicDelete.push(
        this.System.department2[subIndex].topic[Riskkindex].id
      );
      this.System.department2[subIndex].topic.splice(Riskkindex, 1);
    }
  }

  startDrag_Risk_2(event: CdkDragStart<string[]>, isup, istep) {
    console.log('Risks tartDrag', event);
    console.log('Risk startDrag index', isup);
    this.Risk_dropIndex_2 = isup;
    this.dropIndex_2 = istep;
  }

  checkStaticTitle_Risk_2(index) {
    return this.staticTitleIndexRisk_2.includes(index);
  }
  // ===========================================================================================================
  removeDataItem_2(stepindex, subindex, riskIndex) {
    console.log(this.System.department2[stepindex].topic[subindex].id);
    const GroupId = this.System.department2[stepindex].topic[subindex].id
      .split('/department2')
      .join('');
    this.System.department2[stepindex].topic[subindex].id = GroupId;

    console.log('stepindex', stepindex);
    console.log('subindex', subindex);
    console.log('riskIndex', riskIndex);
    const fixedBody = [];
    // tslint:disable-next-line: max-line-length
    if (
      fixedBody.includes(stepindex) &&
      this.System.department2[stepindex].topic[subindex].detail.length > 1 &&
      this.Risk_dropIndex_2 !== 0
    ) {
      console.log('delete fixed row only have many risk[]');
      // this.System.operations[stepindex].detailDelete.push(this.System.operations[stepindex].topic[subindex]['id']);
      // console.log(
      //   this.System.processConsiders[stepindex].topic[subindex]
      //     .detail[riskIndex].id
      // );
      if (
        this.System.department2[stepindex].topic[subindex].detail[riskIndex]
          .id !== ''
      ) {
        this.System.department2DetailDelete.push(
          this.System.department2[stepindex].topic[subindex].detail[riskIndex]
            .id
        );
      }

      this.System.department2[stepindex].topic[subindex].detail.splice(
        riskIndex,
        1
      );
    } else if (
      fixedBody.includes(stepindex) &&
      this.System.department2[stepindex].topic[subindex].detail.length === 1
    ) {
      console.log('3 clear fixed row only');

      if (subindex !== 0) {
        if (this.System.department2[stepindex].topic[subindex].id !== '') {
          this.System.department2TopicDelete.push(
            this.System.department2[stepindex].topic[subindex].id
          );
        }
        this.System.department2[stepindex].topic.splice(subindex, 1);
      } else {
        this.System.department2TopicDelete.push(
          this.System.department2[stepindex].topic[subindex].id
        );
        this.System.department2[stepindex].department = '';
        this.System.department2[stepindex].bookNumber = '';
        Object.keys(this.System.department2[stepindex].topic[subindex]).forEach(
          (key, index) => {
            if (key === 'detail') {
              // tslint:disable-next-line: no-shadowed-variable
              for (
                // tslint:disable-next-line: no-shadowed-variable
                let index = 0;
                index <
                this.System.department2[stepindex].topic[subindex][key].length;
                index++
              ) {
                Object.keys(
                  this.System.department2[stepindex].topic[subindex][key][
                  riskIndex
                  ]
                ).forEach((sub, i) => {
                  this.System.department2[stepindex].topic[subindex][key][
                    index
                  ][sub] = '';
                });
              }
            } else {
              this.System.department2[stepindex].topic[subindex][key] = '';
            }
          }
        );
      }

      // console.log(
      //   'tt',
      //   this.System.operations[stepindex].topic[subindex]
      // );

      // this.System.operations[stepindex].detailDelete.push(
      //   this.System.operations[stepindex].topic[subindex]['id']
      // );
    } else if (
      this.System.department2[stepindex].topic.length === 1 &&
      this.System.department2[stepindex].topic[subindex].detail.length > 1
    ) {
      console.log(' delete risk row only have 1 row');
      if (
        this.System.department2[stepindex].topic[subindex].detail[riskIndex]
          .id !== ''
      ) {
        this.System.department2DetailDelete.push(
          this.System.department2[stepindex].topic[subindex].detail[riskIndex]
            .id
        );
      }
      this.System.department2[stepindex].topic[subindex].detail.splice(
        riskIndex,
        1
      );
    } else if (
      this.System.department2[stepindex].topic.length > 1 &&
      this.System.department2[stepindex].topic[subindex].detail.length > 1
    ) {
      console.log(' delete risk row only have many row');
      // this.System.operations[stepindex].detailDelete.push(
      //   this.System.operations[stepindex].topic[subindex]['id']
      // );
      if (
        this.System.department2[stepindex].topic[subindex].detail[riskIndex]
          .id !== ''
      ) {
        this.System.department2DetailDelete.push(
          this.System.department2[stepindex].topic[subindex].detail[riskIndex]
            .id
        );
      }
      this.System.department2[stepindex].topic[subindex].detail.splice(
        riskIndex,
        1
      );
    } else if (
      this.System.department2[stepindex].topic[subindex].detail.length === 0
    ) {
      console.log(
        'delete row permanent',
        this.System.department2[stepindex].id
      );
      // this.System.operationsDelete.push(
      //   this.System.operations[stepindex].id
      // );
      if (this.System.department2[stepindex].id !== '') {
        this.System.department2Delete.push(
          this.System.department2[stepindex].id
        );
        this.System.department2.splice(stepindex, 1);
      }
    } else if (
      this.System.department2[stepindex].topic[subindex].detail.length === 1 &&
      subindex !== 0 &&
      this.System.department2[stepindex].topic.length > 0
    ) {
      console.log('delete row have 1 sub , 1 risk');
      if (this.System.department2[stepindex].topic[subindex].id !== '') {
        this.System.department2TopicDelete.push(
          this.System.department2[stepindex].topic[subindex].id
        );
        this.System.department2[stepindex].topic.splice(subindex, 1);
      }
    } else if (
      this.System.department2[stepindex].topic[subindex].detail.length === 1 &&
      subindex === 0 &&
      this.System.department2[stepindex].topic.length === 1
    ) {
      console.log('delete row have 1 sub , 1 risk and delete main row');
      if (this.System.department2[stepindex].id !== '') {
        this.System.department2Delete.push(
          this.System.department2[stepindex].id
        );
      }
      this.System.department2.splice(stepindex, 1);
    } else if (
      this.System.department2[stepindex].topic[subindex].detail.length === 1 &&
      subindex === 0 &&
      this.System.department2[stepindex].topic.length > 1
    ) {
      console.log('delete row have many sub , 1 risk and delete 1 sub ');
      if (this.System.department2[stepindex].topic[subindex].id !== '') {
        this.System.department2TopicDelete.push(
          this.System.department2[stepindex].topic[subindex].id
        );
      }
      this.System.department2[stepindex].topic.splice(subindex, 1);
    } else {
      console.log('not in case');
    }
    this.changeSaveDraft();
    this.changeSaveDraftStatus();
  }
  addRow_2() {
    const tempateAddRow = {
      id: '',
      department: '',
      bookNumber: '',
      topic: [
        {
          id: this.makeid(7),
          topic: '',
          detail: [
            {
              id: '',
              detail: '',
            },
          ],
        },
      ],
    };
    this.System.department2.push(tempateAddRow);
    this.changeSaveDraftStatus();
  }
  // ======================================================================================================================//
  checkStaticTitle_of(index) {
    return this.staticTitleIndex_of.includes(index);
  }

  drop_of(event: CdkDragDrop<string[]>) {
    const fixedBody = this.staticTitleIndex_of;
    console.log('event Group:', event);
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (event.previousContainer.data.length > 1) {
      // check if only one left child
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (fixedBody.includes(this.dropIndex_of)) {
      // check only 1 child left and static type move only not delete
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      const templatesubTitleRisk = {
        id: this.makeid(7),
        topic: '',
        detail: [
          {
            id: '',
            detail: '',
          },
        ],
      };

      this.System.rule[this.dropIndex_of].topic.push(templatesubTitleRisk);
    } else {
      // check only 1 child left and not a static type move and delete
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      // remove current row
      this.removeAddItem_of(this.dropIndex_of);
      this.dropIndex_of = null;
    }
    this.changeSaveDraft();
  }

  startDrag_of(event: CdkDragStart<string[]>, istep) {
    // console.log('startDrag', event);
    // console.log('startDrag index', istep);
    this.dropIndex_of = istep;
  }

  removeAddItem_of(index) {
    if (index != null) {
      this.System.ruleDelete.push(this.System.rule[index].id);
      this.System.rule.splice(index, 1);
    }
  }

  // ==================================================================
  dropItem_of(event: CdkDragDrop<string[]>) {
    console.log('event item :', event);
    console.log('previousContainer :', event.container.data.length);
    console.log('index group', this.dropIndex_of);
    console.log('index item:', this.Risk_dropIndex_of);
    const fixedBody = this.staticTitleIndexRisk_of;

    // if (
    //   event.previousContainer.data.length === 1 &&
    //   event.container.data.length !== 1
    // ) {
    //   console.log('index:', this.dropIndex);
    //   this.removeAddItem(this.dropIndex);
    //   this.dropIndex = null;
    // }
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (event.previousContainer.data.length > 1) {
      // check if only one left child
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      // check only 1 child left and not a static type move and delete
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      // remove current row
      this.removeAddItemRisk_of(this.dropIndex_of, this.Risk_dropIndex_of);
      this.dropIndex_of = null;
      this.Risk_dropIndex_of = null;
    }
    this.changeSaveDraft();
  }

  connectItem_of(): any[] {
    const mapping = [];
    for (let index = 0; index < this.System.rule.length; index++) {
      const Group = this.System.rule[index].topic;

      // tslint:disable-next-line: no-shadowed-variable
      // this.System.operations[index].topic.forEach(
      //   ( item, i) =>
      //    mapping.push(`${this.System.operations[index].topic[i].id}`)
      // );

      for (let j = 0; j < Group.length; j++) {
        // mapping.push(String(Group[j].id));
        mapping.push(`${Group[j].id}`);
      }
    }

    // console.log('map', mapping);
    return mapping;
  }

  removeAddItemRisk_of(subIndex, Riskkindex) {
    const GroupId = this.System.rule[subIndex].topic[Riskkindex].id
      .split('/rule')
      .join('');
    this.System.rule[subIndex].topic[Riskkindex].id = GroupId;

    if (this.System.rule[subIndex].topic.length === 1) {
      this.System.ruleTopicDelete.push(this.System.rule[subIndex].id);
      this.System.rule.splice(subIndex, 1);
    } else if (
      this.System.rule[subIndex].topic.length > 1 &&
      Riskkindex != null
    ) {
      this.System.ruleTopicDelete.push(
        this.System.rule[subIndex].topic[Riskkindex].id
      );
      this.System.rule[subIndex].topic.splice(Riskkindex, 1);
    }
  }

  startDrag_Risk_of(event: CdkDragStart<string[]>, isup, istep) {
    console.log('Risks tartDrag', event);
    console.log('Risk startDrag index', isup);
    this.Risk_dropIndex_of = isup;
    this.dropIndex_of = istep;
  }

  checkStaticTitle_Risk_of(index) {
    return this.staticTitleIndexRisk_of.includes(index);
  }
  // ===========================================================================================================
  removeDataItem_of(stepindex, subindex, riskIndex) {
    console.log(this.System.rule[stepindex].topic[subindex].id)
    const GroupId = this.System.rule[stepindex].topic[subindex].id
      .split('/rule')
      .join('');
    this.System.rule[stepindex].topic[subindex].id = GroupId;

    console.log('stepindex', stepindex);
    console.log('subindex', subindex);
    console.log('riskIndex', riskIndex);
    const fixedBody = [];
    // tslint:disable-next-line: max-line-length
    if (
      fixedBody.includes(stepindex) &&
      this.System.rule[stepindex].topic[subindex].detail.length > 1 &&
      this.Risk_dropIndex_of !== 0
    ) {
      console.log('delete fixed row only have many risk[]');
      // this.System.operations[stepindex].detailDelete.push(this.System.operations[stepindex].topic[subindex]['id']);
      // console.log(
      //   this.System.processConsiders[stepindex].topic[subindex]
      //     .detail[riskIndex].id
      // );
      if (
        this.System.rule[stepindex].topic[subindex].detail[riskIndex].id !== ''
      ) {
        this.System.ruleDetailDelete.push(
          this.System.rule[stepindex].topic[subindex].detail[riskIndex].id
        );
      }

      this.System.rule[stepindex].topic[subindex].detail.splice(riskIndex, 1);
    } else if (
      fixedBody.includes(stepindex) &&
      this.System.rule[stepindex].topic[subindex].detail.length === 1
    ) {
      console.log('3 clear fixed row only');

      if (subindex !== 0) {
        if (this.System.rule[stepindex].topic[subindex].id !== '') {
          this.System.ruleTopicDelete.push(
            this.System.rule[stepindex].topic[subindex].id
          );
        }
        this.System.rule[stepindex].topic.splice(subindex, 1);
      } else {
        this.System.ruleTopicDelete.push(
          this.System.rule[stepindex].topic[subindex].id
        );
        this.System.rule[stepindex].department = '';
        this.System.rule[stepindex].bookNumber = '';
        Object.keys(this.System.rule[stepindex].topic[subindex]).forEach(
          (key, index) => {
            if (key === 'detail') {
              // tslint:disable-next-line: no-shadowed-variable
              for (
                // tslint:disable-next-line: no-shadowed-variable
                let index = 0;
                index < this.System.rule[stepindex].topic[subindex][key].length;
                index++
              ) {
                Object.keys(
                  this.System.rule[stepindex].topic[subindex][key][riskIndex]
                ).forEach((sub, i) => {
                  this.System.rule[stepindex].topic[subindex][key][index][sub] =
                    '';
                });
              }
            } else {
              this.System.rule[stepindex].topic[subindex][key] = '';
            }
          }
        );
      }

      // console.log(
      //   'tt',
      //   this.System.operations[stepindex].topic[subindex]
      // );

      // this.System.operations[stepindex].detailDelete.push(
      //   this.System.operations[stepindex].topic[subindex]['id']
      // );
    } else if (
      this.System.rule[stepindex].topic.length === 1 &&
      this.System.rule[stepindex].topic[subindex].detail.length > 1
    ) {
      console.log(' delete risk row only have 1 row');
      if (
        this.System.rule[stepindex].topic[subindex].detail[riskIndex].id !== ''
      ) {
        this.System.ruleDetailDelete.push(
          this.System.rule[stepindex].topic[subindex].detail[riskIndex].id
        );
      }
      this.System.rule[stepindex].topic[subindex].detail.splice(riskIndex, 1);
    } else if (
      this.System.rule[stepindex].topic.length > 1 &&
      this.System.rule[stepindex].topic[subindex].detail.length > 1
    ) {
      console.log(' delete risk row only have many row');
      // this.System.operations[stepindex].detailDelete.push(
      //   this.System.operations[stepindex].topic[subindex]['id']
      // );
      if (
        this.System.rule[stepindex].topic[subindex].detail[riskIndex].id !== ''
      ) {
        this.System.ruleDetailDelete.push(
          this.System.rule[stepindex].topic[subindex].detail[riskIndex].id
        );
      }
      this.System.rule[stepindex].topic[subindex].detail.splice(riskIndex, 1);
    } else if (
      this.System.rule[stepindex].topic[subindex].detail.length === 0
    ) {
      console.log('delete row permanent', this.System.rule[stepindex].id);
      // this.System.operationsDelete.push(
      //   this.System.operations[stepindex].id
      // );
      if (this.System.rule[stepindex].id !== '') {
        this.System.ruleDelete.push(this.System.rule[stepindex].id);
        this.System.rule.splice(stepindex, 1);
      }
    } else if (
      this.System.rule[stepindex].topic[subindex].detail.length === 1 &&
      subindex !== 0 &&
      this.System.rule[stepindex].topic.length > 0
    ) {
      console.log('delete row have 1 sub , 1 risk');
      if (this.System.rule[stepindex].topic[subindex].id !== '') {
        this.System.ruleTopicDelete.push(
          this.System.rule[stepindex].topic[subindex].id
        );
        this.System.rule[stepindex].topic.splice(subindex, 1);
      }
    } else if (
      this.System.rule[stepindex].topic[subindex].detail.length === 1 &&
      subindex === 0 &&
      this.System.rule[stepindex].topic.length === 1
    ) {
      console.log('delete row have 1 sub , 1 risk and delete main row');
      if (this.System.rule[stepindex].id !== '') {
        this.System.ruleDelete.push(this.System.rule[stepindex].id);
      }
      this.System.rule.splice(stepindex, 1);
    } else if (
      this.System.rule[stepindex].topic[subindex].detail.length === 1 &&
      subindex === 0 &&
      this.System.rule[stepindex].topic.length > 1
    ) {
      console.log('delete row have many sub , 1 risk and delete 1 sub ');
      if (this.System.rule[stepindex].topic[subindex].id !== '') {
        this.System.ruleDetailDelete.push(
          this.System.rule[stepindex].topic[subindex].id
        );
      }
      this.System.rule[stepindex].topic.splice(subindex, 1);
    } else {
      console.log('not in case');
    }
    this.changeSaveDraft();
  }
  addRow_of() {
    const tempateAddRow = {
      id: '',
      department: '',
      bookNumber: '',
      topic: [
        {
          id: this.makeid(7),
          topic: '',
          detail: [
            {
              id: '',
              detail: '',
            },
          ],
        },
      ],
    };
    this.System.rule.push(tempateAddRow);
    this.changeSaveDraftStatus();
  }
  // ====================================================================================================================//
  checkStaticTitle_law(index) {
    return this.staticTitleIndex_law.includes(index);
  }

  drop_law(event: CdkDragDrop<string[]>) {
    const fixedBody = this.staticTitleIndex_law;
    console.log('event Group:', event);
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (event.previousContainer.data.length > 1) {
      // check if only one left child
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (fixedBody.includes(this.dropIndex_law)) {
      // check only 1 child left and static type move only not delete
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      const templatesubTitleRisk = {
        id: this.makeid(7),
        topic: '',
        detail: [
          {
            id: '',
            detail: '',
          },
        ],
      };

      this.System.law[this.dropIndex_law].topic.push(templatesubTitleRisk);
    } else {
      // check only 1 child left and not a static type move and delete
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      // remove current row
      this.removeAddItem_law(this.dropIndex_law);
      this.dropIndex_law = null;
    }
    this.changeSaveDraft();
  }

  startDrag_law(event: CdkDragStart<string[]>, istep) {
    // console.log('startDrag', event);
    // console.log('startDrag index', istep);
    this.dropIndex_law = istep;
  }

  removeAddItem_law(index) {
    if (index != null) {
      this.System.lawDelete.push(this.System.law[index].id);
      this.System.law.splice(index, 1);
    }
  }

  // ==================================================================
  dropItem_law(event: CdkDragDrop<string[]>) {
    console.log('event item :', event);
    console.log('previousContainer :', event.container.data.length);
    console.log('index group', this.dropIndex_law);
    console.log('index item:', this.Risk_dropIndex_law);
    const fixedBody = this.staticTitleIndexRisk_law;

    // if (
    //   event.previousContainer.data.length === 1 &&
    //   event.container.data.length !== 1
    // ) {
    //   console.log('index:', this.dropIndex);
    //   this.removeAddItem(this.dropIndex);
    //   this.dropIndex = null;
    // }
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (event.previousContainer.data.length > 1) {
      // check if only one left child
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      // check only 1 child left and not a static type move and delete
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );

      // remove current row
      this.removeAddItemRisk_law(this.dropIndex_law, this.Risk_dropIndex_law);
      this.dropIndex_law = null;
      this.Risk_dropIndex_law = null;
    }
    this.changeSaveDraft();
  }

  connectItem_law(): any[] {
    const mapping = [];
    for (let index = 0; index < this.System.law.length; index++) {
      const Group = this.System.law[index].topic;

      // tslint:disable-next-line: no-shadowed-variable
      // this.System.operations[index].topic.forEach(
      //   ( item, i) =>
      //    mapping.push(`${this.System.operations[index].topic[i].id}`)
      // );

      for (let j = 0; j < Group.length; j++) {
        // mapping.push(String(Group[j].id));
        mapping.push(`${Group[j].id}`);
      }
    }

    // console.log('map', mapping);
    return mapping;
  }

  removeAddItemRisk_law(subIndex, Riskkindex) {
    const GroupId = this.System.law[subIndex].topic[Riskkindex].id
      .split('/law')
      .join('');
    this.System.law[subIndex].topic[Riskkindex].id = GroupId;

    if (this.System.law[subIndex].topic.length === 1) {
      this.System.lawTopicDelete.push(this.System.law[subIndex].id);
      this.System.law.splice(subIndex, 1);
    } else if (
      this.System.law[subIndex].topic.length > 1 &&
      Riskkindex != null
    ) {
      this.System.lawTopicDelete.push(
        this.System.law[subIndex].topic[Riskkindex].id
      );
      this.System.law[subIndex].topic.splice(Riskkindex, 1);
    }
  }

  startDrag_Risk_law(event: CdkDragStart<string[]>, isup, istep) {
    console.log('Risks tartDrag', event);
    console.log('Risk startDrag index', isup);
    this.Risk_dropIndex_law = isup;
    this.dropIndex_law = istep;
  }

  checkStaticTitle_Risk_law(index) {
    return this.staticTitleIndexRisk_law.includes(index);
  }
  // ===========================================================================================================
  removeDataItem_law(stepindex, subindex, riskIndex) {
    console.log(this.System.law[stepindex].topic[subindex].id)
    const GroupId = this.System.law[stepindex].topic[subindex].id
      .split('/law')
      .join('');
    this.System.law[stepindex].topic[subindex].id = GroupId;

    console.log('stepindex', stepindex);
    console.log('subindex', subindex);
    console.log('riskIndex', riskIndex);
    const fixedBody = [];
    // tslint:disable-next-line: max-line-length
    if (
      fixedBody.includes(stepindex) &&
      this.System.law[stepindex].topic[subindex].detail.length > 1 &&
      this.Risk_dropIndex_law !== 0
    ) {
      console.log('delete fixed row only have many risk[]');
      // this.System.operations[stepindex].detailDelete.push(this.System.operations[stepindex].topic[subindex]['id']);
      // console.log(
      //   this.System.processConsiders[stepindex].topic[subindex]
      //     .detail[riskIndex].id
      // );
      if (
        this.System.law[stepindex].topic[subindex].detail[riskIndex].id !== ''
      ) {
        this.System.lawDetailDelete.push(
          this.System.law[stepindex].topic[subindex].detail[riskIndex].id
        );
      }

      this.System.law[stepindex].topic[subindex].detail.splice(riskIndex, 1);
    } else if (
      fixedBody.includes(stepindex) &&
      this.System.law[stepindex].topic[subindex].detail.length === 1
    ) {
      console.log('3 clear fixed row only');
      if (subindex !== 0) {
        if (this.System.law[stepindex].topic[subindex].id !== '') {
          this.System.lawTopicDelete.push(
            this.System.law[stepindex].topic[subindex].id
          );
        }
        this.System.law[stepindex].topic.splice(subindex, 1);
      } else {
        this.System.lawTopicDelete.push(
          this.System.law[stepindex].topic[subindex].id
        );
        this.System.law[stepindex].department = '';
        this.System.law[stepindex].bookNumber = '';
        Object.keys(this.System.law[stepindex].topic[subindex]).forEach(
          (key, index) => {
            if (key === 'detail') {
              // tslint:disable-next-line: no-shadowed-variable
              for (
                // tslint:disable-next-line: no-shadowed-variable
                let index = 0;
                index < this.System.law[stepindex].topic[subindex][key].length;
                index++
              ) {
                Object.keys(
                  this.System.law[stepindex].topic[subindex][key][riskIndex]
                ).forEach((sub, i) => {
                  this.System.law[stepindex].topic[subindex][key][index][sub] =
                    '';
                });
              }
            } else {
              this.System.law[stepindex].topic[subindex][key] = '';
            }
          }
        );
      }

      // console.log(
      //   'tt',
      //   this.System.operations[stepindex].topic[subindex]
      // );

      // this.System.operations[stepindex].detailDelete.push(
      //   this.System.operations[stepindex].topic[subindex]['id']
      // );
    } else if (
      this.System.law[stepindex].topic.length === 1 &&
      this.System.law[stepindex].topic[subindex].detail.length > 1
    ) {
      console.log(' delete risk row only have 1 row');
      if (
        this.System.law[stepindex].topic[subindex].detail[riskIndex].id !== ''
      ) {
        this.System.lawDetailDelete.push(
          this.System.law[stepindex].topic[subindex].detail[riskIndex].id
        );
      }
      this.System.law[stepindex].topic[subindex].detail.splice(riskIndex, 1);
    } else if (
      this.System.law[stepindex].topic.length > 1 &&
      this.System.law[stepindex].topic[subindex].detail.length > 1
    ) {
      console.log(' delete risk row only have many row');
      // this.System.operations[stepindex].detailDelete.push(
      //   this.System.operations[stepindex].topic[subindex]['id']
      // );
      if (
        this.System.law[stepindex].topic[subindex].detail[riskIndex].id !== ''
      ) {
        this.System.lawDetailDelete.push(
          this.System.law[stepindex].topic[subindex].detail[riskIndex].id
        );
      }
      this.System.law[stepindex].topic[subindex].detail.splice(riskIndex, 1);
    } else if (this.System.law[stepindex].topic[subindex].detail.length === 0) {
      console.log('delete row permanent', this.System.law[stepindex].id);
      // this.System.operationsDelete.push(
      //   this.System.operations[stepindex].id
      // );
      if (this.System.law[stepindex].id !== '') {
        this.System.lawDelete.push(this.System.law[stepindex].id);
        this.System.law.splice(stepindex, 1);
      }
    } else if (
      this.System.law[stepindex].topic[subindex].detail.length === 1 &&
      subindex !== 0 &&
      this.System.law[stepindex].topic.length > 0
    ) {
      console.log('delete row have 1 sub , 1 risk');
      if (this.System.law[stepindex].topic[subindex].id !== '') {
        this.System.lawTopicDelete.push(
          this.System.law[stepindex].topic[subindex].id
        );
        this.System.law[stepindex].topic.splice(subindex, 1);
      }
    } else if (
      this.System.law[stepindex].topic[subindex].detail.length === 1 &&
      subindex === 0 &&
      this.System.law[stepindex].topic.length === 1
    ) {
      console.log('delete row have 1 sub , 1 risk and delete main row');
      if (this.System.law[stepindex].id !== '') {
        this.System.lawDelete.push(this.System.law[stepindex].id);
      }
      this.System.law.splice(stepindex, 1);
    } else if (
      this.System.law[stepindex].topic[subindex].detail.length === 1 &&
      subindex === 0 &&
      this.System.law[stepindex].topic.length > 1
    ) {
      console.log('delete row have many sub , 1 risk and delete 1 sub ');
      if (this.System.law[stepindex].topic[subindex].id !== '') {
        this.System.lawTopicDelete.push(
          this.System.law[stepindex].topic[subindex].id
        );
      }
      this.System.law[stepindex].topic.splice(subindex, 1);
    } else {
      console.log('not in case');
    }
    this.changeSaveDraft();
    this.changeSaveDraftStatus();
  }
  addRow_law() {
    const tempateAddRow = {
      id: '',
      department: '',
      bookNumber: '',
      topic: [
        {
          id: this.makeid(7),
          topic: '',
          detail: [
            {
              id: '',
              detail: '',
            },
          ],
        },
      ],
    };
    this.System.law.push(tempateAddRow);
    this.changeSaveDraftStatus();
  }

  autoGrowTextZone(e) {
    e.target.style.height = '0px';
    e.target.style.overflow = 'hidden';
    e.target.style.height = (e.target.scrollHeight + 0) + 'px';
  }

  CheckValueAccount() {
    console.log(this.System);
    let status = true;
    const listkey = [];
    let dataCheck = [];
    dataCheck = this.System.account;

    console.log('Account::', dataCheck);
    console.log(listkey);

    for (let index = 0; index < dataCheck.length; index++) {
      if (
        dataCheck[index]['regulation'] === '' ||
        dataCheck[index]['regulation'] === null
      ) {
        status = false;

      } else {
        for (let k = 0; k < dataCheck[index].topic.length; k++) {
          if (
            dataCheck[index].topic[k].topic === '' ||
            dataCheck[index].topic[k].topic === null
          ) {
            status = false;

          } else {
            for (
              let p = 0;
              p < dataCheck[index].topic[k].detail.length;
              p++
            ) {
              if (
                dataCheck[index].topic[k].topic === '' ||
                dataCheck[index].topic[k].topic === null
              ) {
                status = false;
              } else {
                for (
                  let l = 0;
                  l < dataCheck[index].topic[k].detail.length;
                  l++
                ) {

                  if (
                    dataCheck[index].topic[k].detail[l]['detail'] ===
                    '' ||
                    dataCheck[index].topic[k].detail[l]['detail'] === null
                    ||
                    dataCheck[index].topic[k].detail[l]['detail'] === 'null'
                  ) {
                    status = false;
                  }

                }
              }
            }
          }
        }
      }
    }

    // console.log('data::', dataCheck)
    if (dataCheck && status) {
      return (status = true);
    } else {
      return (status = false);
    }
  }

  CheckValueDepartment1() {
    console.log(this.System);
    let status = true;
    const listkey = [];
    let dataCheck = [];
    dataCheck = this.System.department1;

    for (let index = 0; index < dataCheck.length; index++) {
      if (
        dataCheck[index]['department'] === '' ||
        dataCheck[index]['department'] === null ||
        dataCheck[index]['bookNumber'] === ''
      ) {
        status = false;

      } else {
        for (let k = 0; k < dataCheck[index].topic.length; k++) {
          if (
            dataCheck[index].topic[k].topic === '' ||
            dataCheck[index].topic[k].topic === null
          ) {
            status = false;

          } else {
            for (
              let p = 0;
              p < dataCheck[index].topic[k].detail.length;
              p++
            ) {
              if (
                dataCheck[index].topic[k].topic === '' ||
                dataCheck[index].topic[k].topic === null
              ) {
                status = false;
              } else {
                for (
                  let l = 0;
                  l < dataCheck[index].topic[k].detail.length;
                  l++
                ) {
                  for (let i = 0; i < listkey.length; i++) {
                    if (
                      dataCheck[index].topic[k].detail[l]['detail'] ===
                      '' ||
                      dataCheck[index].topic[k].detail[l]['detail'] === null
                      ||
                      dataCheck[index].topic[k].detail[l]['detail'] === 'null'
                    ) {
                      status = false;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    if (dataCheck && status) {
      return (status = true);
    } else {
      return (status = false);
    }
  }
  CheckValueDepartment2() {
    console.log(this.System);
    let status = true;
    const listkey = [];
    let dataCheck = [];
    dataCheck = this.System.department2;
    console.log(dataCheck);
    // ----list key Detail-----


    for (let index = 0; index < dataCheck.length; index++) {
      if (
        dataCheck[index]['department'] === '' ||
        dataCheck[index]['department'] === null ||
        dataCheck[index]['bookNumber'] === ''
      ) {
        status = false;

      } else {
        for (let k = 0; k < dataCheck[index].topic.length; k++) {
          if (
            dataCheck[index].topic[k].topic === '' ||
            dataCheck[index].topic[k].topic === null
          ) {
            status = false;

          } else {
            for (
              let p = 0;
              p < dataCheck[index].topic[k].detail.length;
              p++
            ) {
              if (
                dataCheck[index].topic[k].topic === '' ||
                dataCheck[index].topic[k].topic === null
              ) {
                status = false;
              } else {
                for (
                  let l = 0;
                  l < dataCheck[index].topic[k].detail.length;
                  l++
                ) {

                  if (
                    dataCheck[index].topic[k].detail[l]['detail'] ===
                    '' ||
                    dataCheck[index].topic[k].detail[l]['detail'] === null
                    ||
                    dataCheck[index].topic[k].detail[l]['detail'] === 'null'
                  ) {
                    status = false;
                  }

                }
              }
            }
          }
        }
      }
    }
    if (dataCheck && status) {
      return (status = true);
    } else {
      return (status = false);
    }
  }
  CheckValueRule() {
    console.log(this.System);
    let status = true;
    const listkey = [];
    let dataCheck = [];
    dataCheck = this.System.rule;
    console.log(dataCheck);


    for (let index = 0; index < dataCheck.length; index++) {
      if (
        dataCheck[index]['department'] === '' ||
        dataCheck[index]['department'] === null ||
        dataCheck[index]['bookNumber'] === ''
      ) {
        status = false;

      } else {
        for (let k = 0; k < dataCheck[index].topic.length; k++) {
          if (
            dataCheck[index].topic[k].topic === '' ||
            dataCheck[index].topic[k].topic === null
          ) {
            status = false;

          } else {
            for (
              let p = 0;
              p < dataCheck[index].topic[k].detail.length;
              p++
            ) {
              if (
                dataCheck[index].topic[k].topic === '' ||
                dataCheck[index].topic[k].topic === null
              ) {
                status = false;
              } else {
                for (
                  let l = 0;
                  l < dataCheck[index].topic[k].detail.length;
                  l++
                ) {

                  if (
                    dataCheck[index].topic[k].detail[l]['detail'] ===
                    '' ||
                    dataCheck[index].topic[k].detail[l]['detail'] === null
                    ||
                    dataCheck[index].topic[k].detail[l]['detail'] === 'null'
                  ) {
                    status = false;

                  }
                }
              }
            }
          }
        }
      }
    }

    if (dataCheck && status) {
      return (status = true);
    } else {
      return (status = false);
    }
  }
  CheckValueLaw() {
    console.log(this.System);
    let status = true;
    const listkey = [];
    let dataCheck = [];
    dataCheck = this.System.law;
    console.log(dataCheck);

    for (let index = 0; index < dataCheck.length; index++) {
      if (
        dataCheck[index]['department'] === '' ||
        dataCheck[index]['department'] === null ||
        dataCheck[index]['bookNumber'] === ''
      ) {
        status = false;

      } else {
        for (let k = 0; k < dataCheck[index].topic.length; k++) {
          if (
            dataCheck[index].topic[k].topic === '' ||
            dataCheck[index].topic[k].topic === null
          ) {
            status = false;

          } else {
            for (
              let p = 0;
              p < dataCheck[index].topic[k].detail.length;
              p++
            ) {
              if (
                dataCheck[index].topic[k].topic === '' ||
                dataCheck[index].topic[k].topic === null
              ) {
                status = false;
              } else {
                for (
                  let l = 0;
                  l < dataCheck[index].topic[k].detail.length;
                  l++
                ) {

                  if (
                    dataCheck[index].topic[k].detail[l]['detail'] ===
                    '' ||
                    dataCheck[index].topic[k].detail[l]['detail'] === null
                    ||
                    dataCheck[index].topic[k].detail[l]['detail'] === 'null'
                  ) {
                    status = false;

                  }
                }
              }
            }
          }
        }
      }
    }

    if (dataCheck && status) {
      return (status = true);
    } else {
      return (status = false);
    }
  }

  changeSaveDraftStatus() {
    this.saveDraftstatus_State = true;
    this.SysManagementRules.status_state = true;
    console.log('saveDraftstatus:', this.saveDraftstatus_State);
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate');
    // return confirm();
    const subject = new Subject<boolean>();
    // const status = this.sidebarService.outPageStatus();
    // console.log('000 sidebarService status:', status);
    if (this.saveDraftstatus_State === true) {
      // console.log('000 sidebarService status:', status);
      const dialogRef = this.dialog.open(DialogSaveStatusSystemCusComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        console.log('data::', data);
        if (data.returnStatus === true) {
          subject.next(true);
          this.openSaved();
          this.saveDraftstatus_State = false;
        } else if (data.returnStatus === false) {
          console.log('000000 data::', data.returnStatus);
          // this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
        }
      });
      console.log('=====', subject);
      return subject;
    } else {
      return true;
    }
  }



}
