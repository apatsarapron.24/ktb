import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SystemManagementCusComponent } from './system-management-cus.component';

describe('SystemManagementCusComponent', () => {
  let component: SystemManagementCusComponent;
  let fixture: ComponentFixture<SystemManagementCusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SystemManagementCusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemManagementCusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
