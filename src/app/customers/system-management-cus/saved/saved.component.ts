import { Component, OnInit, Inject } from '@angular/core';
import {
  MatDialogRef,
  MatDialog,
  MAT_DIALOG_DATA,
  MatDialogConfig,
} from '@angular/material';
import Swal from 'sweetalert2';
import { SysManagementRulesService } from '../../../services/sys-management-rules/sys-management-rules.service';
@Component({
  selector: 'app-saved',
  templateUrl: './saved.component.html',
  styleUrls: ['./saved.component.scss'],
})
export class SavedComponent implements OnInit {
  data: any = [];
  constructor(
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<SavedComponent>,
    @Inject(MAT_DIALOG_DATA) public Dialogdata: any,
    private SysManagement: SysManagementRulesService
  ) {}
  status_loading = true;

  ngOnInit() {
    this.data = this.Dialogdata;

    // console.log('Saved:', this.data);
  }
  back_edit() {
    this.data.status='edit'
    this.dialogRef.close(this.data);
  }
  close() {
    this.data.status='close'
    this.dialogRef.close(this.data);
  }
  save() {
    this.data.status='save'
    console.log('BeforeSave:', this.data);
   // ======account=======//
   for (let index = 0; index < this.data.account.length; index++) {
    for (let k = 0; k < this.data.account[index].topic.length; k++) {
      const topic = this.data.account[index].topic[k].id;
      if(typeof(topic) == 'number') {
        this.data.account[index].topic[k].id = topic
      }else if(typeof(topic) == 'string' && topic !== '') {
        this.data.account[index].topic[k].id = Number(topic.split('/account').join(''));
      }else {
        this.data.account[index].topic[k].id = topic
      }
     
    }
  }
  // ======department1=======//
  for (let index = 0; index < this.data.department1.length; index++) {
    for (let k = 0; k < this.data.department1[index].topic.length; k++) {
      const topic = this.data.department1[index].topic[k].id;
      if(typeof(topic) == 'number') {
        this.data.department1[index].topic[k].id = topic
      }else if(typeof(topic) == 'string' && topic !== '') {
        this.data.department1[index].topic[k].id = Number(topic.split('/department1').join(''));
      }else {
        this.data.department1[index].topic[k].id = topic
      }
     
      // this.data.department1[index].topic[k].id = Number(topic.split('/department1').join(''));
    }
  }
  // ======department2=======//
  for (let index = 0; index < this.data.department2.length; index++) {
    for (let k = 0; k < this.data.department2[index].topic.length; k++) {
      const topic = this.data.department2[index].topic[k].id;
      if(typeof(topic) == 'number') {
        this.data.department2[index].topic[k].id = topic
      }else if(typeof(topic) == 'string' && topic !== '') {
        this.data.department2[index].topic[k].id = Number(topic.split('/department2').join(''));
      }else {
        this.data.department2[index].topic[k].id = topic
      }
      // this.data.department2[index].topic[k].id = Number(topic.split('/department2').join(''));
    }
  }
  // ======rule=======//
  for (let index = 0; index < this.data.rule.length; index++) {
    for (let k = 0; k < this.data.rule[index].topic.length; k++) {
      const topic = this.data.rule[index].topic[k].id;
      if(typeof(topic) == 'number') {
        this.data.rule[index].topic[k].id = topic
      }else if(typeof(topic) == 'string' && topic !== '') {
        this.data.rule[index].topic[k].id = Number(topic.split('/rule').join(''));
      }else {
        this.data.rule[index].topic[k].id = topic
      }
      // this.data.rule[index].topic[k].id = Number(topic.split('/rule').join(''));
    }
  }
  // ======law=======//
  for (let index = 0; index < this.data.law.length; index++) {
    for (let k = 0; k < this.data.law[index].topic.length; k++) {
      const topic = this.data.law[index].topic[k].id;
      if(typeof(topic) == 'number') {
        this.data.law[index].topic[k].id = topic
      }else if(typeof(topic) == 'string' && topic !== '') {
        this.data.law[index].topic[k].id = Number(topic.split('/law').join(''));
      }else {
        this.data.law[index].topic[k].id = topic
      }
      // this.data.law[index].topic[k].id = Number(topic.split('/law').join(''));
    }
  }
  this.status_loading = true;
    console.log('data:', this.data);
    this.SysManagement.updateSystemManagement(this.data).subscribe((res) => {
      console.log(res);
      if (res['status'] === 'success') {
        this.dialogRef.close(this.data);
        this.status_loading = false;
        console.log('updated');
        // tslint:disable-next-line: no-use-before-declare
        Swal.fire({
          title: 'success',
          text: 'คุณได้บันทึกข้อมูลเรียบร้อยแล้ว',
          icon: 'success',
          showConfirmButton: false,
          timer: 3000
        });
        this.status_loading = false;
        // const success = this.dialog.open(AlertComponent);
        // setTimeout(() => {
        //   success.close();
        // }, 1000);
        this.data = null;
      
        // this.reSet_data();
      } else {
        this.status_loading = false;
        alert('ไม่สามารถบันทึกข้อมูลได้');

      }
    });
  }

}

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
})
export class AlertComponent {
  constructor(public dialogRef: MatDialogRef<AlertComponent>) {}
}
