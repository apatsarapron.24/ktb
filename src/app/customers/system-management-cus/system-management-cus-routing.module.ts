import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SystemManagementCusComponent } from './system-management-cus.component';
import { CanDeactivateGuard } from './../../services/configGuard/config-guard.service';

const routes: Routes = [
  {
    path: '',
    component: SystemManagementCusComponent,
    canDeactivate: [CanDeactivateGuard]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SystemManagementCusRoutingModule { }
