import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SystemManagementCusComponent } from './system-management-cus.component';
import { SystemManagementCusRoutingModule } from './system-management-cus-routing.module';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { SavedComponent, AlertComponent } from './saved/saved.component';
import { TextareaAutoresizeDirectiveModule } from '../../textarea-autoresize.directive/textarea-autoresize.directive.module';
import { DialogSaveStatusSystemCusComponent } from './dialog-save-status-system-cus/dialog-save-status-system-cus.component';
import { CanDeactivateGuard } from './../../services/configGuard/config-guard.service';
@NgModule({
  declarations: [SystemManagementCusComponent, SavedComponent, AlertComponent, DialogSaveStatusSystemCusComponent,
  ],
  imports: [
    CommonModule,
    SystemManagementCusRoutingModule,
    FormsModule,
    MatSortModule,
    MatTableModule,
    MatPaginatorModule,
    DragDropModule,
    MatInputModule,
    ReactiveFormsModule,
    MatDialogModule,
    TextareaAutoresizeDirectiveModule
  ],
  providers: [CanDeactivateGuard],
  entryComponents: [SavedComponent, AlertComponent, DialogSaveStatusSystemCusComponent],


})
export class SystemManagementCusModule { }
