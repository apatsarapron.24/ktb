import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SystemManageService {

  columnNames = ['department', 'main', 'data', 'delete'];
  dataTable = [
    {
      id: 'GT001',
      department: 'หน่วยงานที่ที่จัดทำ 1 ',
      main: 'หัวข้อที่ 1',
      data: 'ประเด็นที่ 1 เรื่องการจัดการ'
    },
    {
      id: 'GT002',
      department: 'หน่วยงานที่ที่จัดทำ 1 ',
      main: 'หัวข้อที่ 1',
      data: 'ประเด็นที่ 2 เรื่องการเงิน'
    },
    {
      id: 'GT003',
      department: 'หน่วยงานที่ที่จัดทำ 1 ',
      main: 'หัวข้อที่ 2',
      data: 'ประเด็นที่ 1 เรื่อง งบประมาณ'
    },
    {
      id: 'GT004',
      department: 'หน่วยงานที่ที่จัดทำ 1 ',
      main: 'หัวข้อที่ 3',
      data: 'ประเด็นที่ 1 เรื่อง ทรัพยากร'
    },
    {
      id: 'GT005',
      department: 'หน่วยงานที่ที่จัดทำพิเศษ ',
      main: 'หัวข้อที่ 1',
      data: 'ประเด็นที่ 1 เรื่อง ความสัมพันธ์ระหว่างหน่วย'
    },
    {
      id: 'GT006',
      department: 'หน่วยงานที่ที่จัดทำพิเศษ',
      main: 'หัวข้อที่ 1',
      data: 'ประเด็นที่ 2 เรื่อง องค์ประกอบย่อย'
    },
    {
      id: 'GT007',
      department: 'หน่วยงานที่ที่จัดทำพิเศษ',
      main: 'หัวข้อที่ 2',
      data: 'ประเด็นที่ 1 เรื่อง การเปลี่ยนแปลง'
    },
    {
      id: 'GT008',
      department: 'หน่วยงานเสริม',
      main: 'หัวข้อที่ 1',
      data: 'ประเด็นที่ 1 เรื่อง อุปกรณ์และเทคโนโลยี'
    }
  ];

  headerlist_1 = ['หน่วยงาน', 'เลขที่หนังสือ', 'หัวข้อ', 'ประเด็น'];
  dataTable_1 = [
    {
      id: 'GT001',
      department: 'หน่วยงานที่ที่จัดทำ 1 dataTable_1',
      book_id: 45215,
      main: 'หัวข้อที่ 1',
      data: 'ประเด็นที่ 1 เรื่องการจัดการ'
    },
    {
      id: 'GT002',
      department: 'หน่วยงานที่ที่จัดทำ 1 dataTable_1',
      book_id: 3284,
      main: 'หัวข้อที่ 1',
      data: 'ประเด็นที่ 2 เรื่องการเงิน'
    },
    {
      id: 'GT005',
      department: 'หน่วยงานที่ที่จัดทำพิเศษ dataTable_1',
      book_id: 4006,
      main: 'หัวข้อที่ 1',
      data: 'ประเด็นที่ 1 เรื่อง องค์ประกอบย่อย'
    },
    {
      id: 'GT007',
      department: 'หน่วยงานที่ที่จัดทำพิเศษ dataTable_1',
      book_id: 5454,
      main: 'หัวข้อที่ 2',
      data: 'ประเด็นที่ 1 เรื่อง การเปลี่ยนแปลง'
    },
    {
      id: 'GT008',
      department: 'หน่วยงานเสริม',
      book_id: 57544,
      main: 'หัวข้อที่ 1',
      data: 'ประเด็นที่ 1 เรื่อง อุปกรณ์และเทคโนโลยี'
    },
    {
      id: 'GT012',
      department: 'หน่วยงานที่เกี่ยวข้อง dataTable_1',
      book_id: 45389,
      main: 'หัวข้อที่ 1',
      data: 'ประเด็นที่ 1 เรื่อง ระบบจัดการ'
    },
    {
      id: 'GT0052',
      department: 'หน่วยงานที่เกี่ยวข้อง dataTable_1',
      book_id: 11240,
      main: 'หัวข้อที่ 1',
      data: 'ประเด็นที่ 2 เรื่อง การเงิน'
    }
  ];

  constructor() {}
}
