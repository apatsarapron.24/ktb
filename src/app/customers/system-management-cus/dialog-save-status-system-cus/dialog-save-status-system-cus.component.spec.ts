import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogSaveStatusSystemCusComponent } from './dialog-save-status-system-cus.component';

describe('DialogSaveStatusSystemCusComponent', () => {
  let component: DialogSaveStatusSystemCusComponent;
  let fixture: ComponentFixture<DialogSaveStatusSystemCusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogSaveStatusSystemCusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogSaveStatusSystemCusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
