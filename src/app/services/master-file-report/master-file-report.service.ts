import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MasterFileReportService {
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    })
  };

  constructor(
    private http: HttpClient
  ) { }

  divisionList() {
    return this.http.get(this.api_Url + '/api/pps/admin/list/grp');
  } // สายงาน

  workgroupList() {
    return this.http.get(this.api_Url + '/api/pps/admin/list/sctr');
  } // กลุ่มงาน

  departmentList() {
    return this.http.get(this.api_Url + '/api/pps/admin/list/dept');
  } // ฝ่ายงาน

  getSearchRiskControlReport(data) {
    return this.http.post(this.api_Url + '/api/pps/product/report/searchRiskControlReport',
      data
    );
  }

  getDefaultData(requestId: number) {
    return this.http.post(this.api_Url + '/api/pps/product/defaultData',
      {
        'requestId': requestId,
      },
      this.httpOptions
    );
  }

  getHistoryProduct(requestId: number) {
    return this.http.post(this.api_Url + '/api/pps/product/detail/detail/page14',
      { 'requestId': requestId });
  }

  getIssueMasterFileReport(requestId: number) {
    return this.http.post(this.api_Url + '/api/pps/product/report/issueMasterFileReport',
      { 'requestId': requestId });
  }

  getProductPerformance(requestId: number) {
    return this.http.post(this.api_Url + '/api/pps/product/report/perfomanceMasterFileReport',
      { 'requestId': requestId });
  }

  getPerfomanceTriger(requestId: number) {
    return this.http.post(this.api_Url + '/api/pps/product/report/perfomanceTrigerMasterFileReport',
      { 'requestId': requestId });
  }

  productGetGroup(requestId) {
    return this.http.post(this.api_Url + '/api/pps/product/detail/get/group',
      { 'requestId': requestId });
  }


}
