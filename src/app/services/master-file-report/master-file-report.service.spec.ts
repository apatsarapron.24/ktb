import { TestBed } from '@angular/core/testing';

import { MasterFileReportService } from './master-file-report.service';

describe('MasterFileReportService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MasterFileReportService = TestBed.get(MasterFileReportService);
    expect(service).toBeTruthy();
  });
});
