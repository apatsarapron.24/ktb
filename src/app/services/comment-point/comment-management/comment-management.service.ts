import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CommentManagementService {

  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    })
  };
  results: any;

  constructor(private http: HttpClient) { }
  getQuestion_answer_PO(id: any, rolename: any) {
    console.log('rolename api', rolename);
    return this.http.post(this.api_Url + '/api/pps/product/POComment/get/question',
    {
      requestId: id,
      role: rolename
    },
    this.httpOptions
    );
  }

  getQuestion_answer_BU(id: any) {
    return this.http.post(this.api_Url + '/api/pps/product/BUComment/get/question-answer',
    {requestId: id},
    this.httpOptions
    );
  }

  sendQuestion(data: any) {
    return this.http.post(this.api_Url + '/api/pps/product/BUComment/send/question',
    data,
    this.httpOptions
    );
  }
  editQuestion(data: any) {
    return this.http.post(this.api_Url + '/api/pps/product/BUComment/send/question',
      data,
      this.httpOptions
    );
  }

  sendAnswer(data: any) {
    return this.http.post(this.api_Url + '/api/pps/product/BUComment/send/answer',
      data,
      this.httpOptions
    );
  }

  editAnswer(data: any) {
    return this.http.post(this.api_Url + '/api/pps/product/BUComment/send/answer',
      data,
      this.httpOptions
    );
  }

  deleteQuestion(id: any) {
    return this.http.post(this.api_Url + '/api/pps/product/BUComment/delete/question',
      {id: id},
      this.httpOptions
    );
  }

  deleteAnswer(id: any) {
    return this.http.post(this.api_Url + '/api/pps/product/BUComment/delete/answer',
      {id: id},
      this.httpOptions
    );
  }
}
