import { TestBed } from '@angular/core/testing';

import { CommentManagementService } from './comment-management.service';

describe('CommentManagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CommentManagementService = TestBed.get(CommentManagementService);
    expect(service).toBeTruthy();
  });
});
