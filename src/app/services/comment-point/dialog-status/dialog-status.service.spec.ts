import { TestBed } from '@angular/core/testing';

import { DialogStatusService } from './dialog-status.service';

describe('DialogStatusService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DialogStatusService = TestBed.get(DialogStatusService);
    expect(service).toBeTruthy();
  });
});
