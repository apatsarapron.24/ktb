import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class FinanceService {

  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    })
  };
  results: any;
  status_state = false;
  constructor(private http: HttpClient, private router: Router) { }

  getFinance(requestId: any, role: any) {
    console.log('role 5555', role);
    console.log('requestId', requestId);
    if (role === 'BU') {
      return this.http.post(this.api_Url + '/api/pps/product/BUComment/finance',
        {
          'requestId': requestId
        }
      );
    } else if (role === 'PO') {
      return this.http.post(this.api_Url + '/api/pps/product/POComment/finance',
        {
          'requestId': requestId
        }
      );
    }

  }

  updateFinance(data: any, role: any) {
    if (role === 'BU') {
      return this.http.post(this.api_Url + '/api/pps/product/BUComment/finance/update',
        data,
        this.httpOptions
      );
    } else if (role === 'PO') {
      return this.http.post(this.api_Url + '/api/pps/product/POComment/finance/update',
        data,
        this.httpOptions
      );
    }

  }

  sendDocument_BU(requestId: any) {
    return this.http.post(this.api_Url + '/api/pps/product/BUComment/submit',
      {
        'requestId': requestId
      }
      
    );
  }


  sendDocument_PO(requestId: any) {
    return this.http.post(this.api_Url + '/api/pps/product/POComment/send',
      {
        'requestId': requestId,
        'bu': 'Finance'
      }
      
    );
  }
  linkToAny() {
    console.log('send');
    this.sendDocument_BU(localStorage.getItem('requestId')).subscribe( (res) => {
      if (res['message'] === 'กรุณากรอกข้อมูลผู้ดำเนินการ') {
        this.router.navigate(['/save-manager']);
      }
    });

  }
}
