import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class RetailService {
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
    }),
  };
  results: any;
  status_state = false;
  constructor(private http: HttpClient) { }

  getRetail(requestId: any) {
    return this.http.post(
      this.api_Url + '/api/pps/product/BUComment/retail',
      {
        requestId: requestId,
      },
      this.httpOptions
    );
  }

  updateRetail(data: any) {
    console.log('listRetailOperationDetail:', data.listRetailOperationDetail);

    // console.log('data Service:', data.requestId);
    return this.http.post(
      this.api_Url + '/api/pps/product/BUComment/retail/update',
      {
        requestId: data.requestId,
        status: data.status,
        listRetailProcessConsiders: data.listRetailProcessConsiders,
        processConsidersSubRiskDelete: [],

        listRetailProcessConsidersExtra: data.listRetailProcessConsidersExtra,
        deleteProcessConsidersExtra: [],

        listRetailOperationDetail: data.listRetailOperationDetail,
        deleteOperationDetail: [],

        listRetailOperationDetailExtra: data.listRetailOperationDetailExtra,
        deleteOperationDetailExtra: [],
        requestProcessConsidersDelete: data.requestProcessConsidersDelete,
        requestProcessConsidersSubDelete: data.requestProcessConsidersSubDelete,
        requestProcessConsidersSubRiskDelete:
          data.requestProcessConsidersSubRiskDelete,
      },
      this.httpOptions
    );
  }
  // ================================PO Role====================================================//
  getRetail_PO(requestId: any) {
    return this.http.post(
      this.api_Url + '/api/pps/product/POComment/retail',
      {
        requestId: requestId,
      },
      this.httpOptions
    );
  }

  UpdateRetail_PO(data: any) {
    return this.http.post(
      this.api_Url + '/api/pps/product/POComment/retail/update',
      data,
      this.httpOptions
    );
  }
  // ==========================================================================================//

  // ======================================PC role ============================================//
  getRetail_PC(requestId: any) {
    return this.http.post(
      this.api_Url + '/api/pps/product/PCComment/retail',
      {
        requestId: requestId,
      },
      this.httpOptions
    );
  }

  UpdateRetail_PC(data: any) {
    return this.http.post(
      this.api_Url + '/api/pps/product/PCComment/retail/update',
      {
        requestId: data.requestId,
        status: data.status,
        listRetailProcessConsiders: data.listRetailProcessConsiders,
        listRetailProcessConsidersExtra: data.listRetailProcessConsidersExtra,
        listRetailOperationDetail: data.listRetailOperationDetail,
        listRetailOperationDetailExtra: data.listRetailOperationDetailExtra,
      },
      this.httpOptions
    );
  }
  // ==========================================================================================//
}
