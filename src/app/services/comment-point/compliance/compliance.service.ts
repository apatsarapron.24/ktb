import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ComplianceService {

  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    })
  };
  results: any;
  status_state = false;
  constructor(private http: HttpClient) { }

  getCompliance(requestId: any) {
    console.log('in compliance', requestId);
    return this.http.post(this.api_Url + '/api/pps/product/BUComment/compliance',
    {
      'requestId' : requestId
    }
    );
  }

  updateCompliance(data: any) {
    return this.http.post(this.api_Url + '/api/pps/product/BUComment/compliance/update',
      data,
      this.httpOptions
    );
  }

  getPOCompliance(requestId: any) {
    console.log('in compliance', requestId);
    return this.http.post(this.api_Url + '/api/pps/product/POComment/compliance',
    {
      'requestId' : requestId
    }
    );
  }

  updatePOCompliance(data: any) {
    console.log('in update PO');
    return this.http.post(this.api_Url + '/api/pps/product/POComment/compliance/update',
      data,
      this.httpOptions
    );
  }

  submit(requestId: any) {
    console.log('in compliance', requestId);
    return this.http.post(this.api_Url + '/api/pps/product/BUComment/submit',
    {
      'requestId' : requestId
    }
    );
  }

  getPCCompliance(requestId: any) {
    return this.http.post(this.api_Url + '/api/pps/product/PCComment/compliance',
    {
      'requestId' : requestId
    }
    );
  }

  updatePCCompliance(data: any) {
    return this.http.post(this.api_Url + '/api/pps/product/PCComment/compliance/update',
      data,
      this.httpOptions
    );
  }

  getDropDrownlist() {
    return this.http.get(this.api_Url + '/api/pps/admin/detailComplianceLegal',
    this.httpOptions
  );
  }
}

