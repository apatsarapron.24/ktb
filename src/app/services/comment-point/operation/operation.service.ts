import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class OperationService {
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
    }),
  };
  results: any;
  status_state = false;
  constructor(private http: HttpClient) { }

  getOperation(requestId: any) {
    return this.http.post(
      this.api_Url + '/api/pps/product/BUComment/operation',
      {
        requestId: requestId,
      },
      this.httpOptions
    );
  }

  UpdateOperation(Updatedata: any) {
    console.log('Updatedata:', Updatedata);
    return this.http.post(
      this.api_Url + '/api/pps/product/BUComment/operation/update',
      {
        requestId: Updatedata.requestId,
        commentOperationProcess: Updatedata.commentOperationProcess,
        commentOperation1: Updatedata.commentOperation1,
        processConsiders: Updatedata.processConsiders,
        commentOperation2: Updatedata.commentOperation2,
        deleteCommentOperation1: Updatedata.deleteCommentOperation1,
        deleteCommentOperation2: Updatedata.deleteCommentOperation2,
        requestProcessConsidersDelete: Updatedata.requestProcessConsidersDelete,
        requestProcessConsidersSubDelete:
          Updatedata.requestProcessConsidersSubDelete,
        requestProcessConsidersSubRiskDelete:
          Updatedata.requestProcessConsidersSubRiskDelete,
      },
      this.httpOptions
    );
  }

  getOperation_PO(requestId: any) {
    return this.http.post(
      this.api_Url + '/api/pps/product/POComment/operation',
      {
        requestId: requestId,
      },
      this.httpOptions
    );
  }

  UpdataCommentOperation_PO(data: any) {
    console.log('service:', data);
    // console.log('httpOptions');
    return this.http.post(
      this.api_Url + '/api/pps/product/POComment/operation/update',
      {
        requestId: data.requestId,
        status: data.status,
        commentOperationProcess: data.commentOperationProcess,
        processConsiders: data.processConsiders,
        commentOperation1: data.commentOperation1,
        commentOperation2: data.commentOperation2,
      },
      this.httpOptions
    );
  }

  Get_PC_Operation(requestId: any) {
    return this.http.post(
      this.api_Url + '/api/pps/product/PCComment/get/operation',
      {
        requestId: requestId,
      },
      this.httpOptions
    );
  }

  UpdataCommentOperation_PC(data: any) {
    // console.log('service:', data);
    // console.log('httpOptions');
    return this.http.post(
      this.api_Url + '/api/pps/product/PCComment/save/operation',
      {
        requestId: data.requestId,
        status: data.status,
        commentOperationProcess: data.commentOperationProcess,
        processConsiders: data.processConsiders,
        commentOperation1: data.commentOperation1,
        commentOperation2: data.commentOperation2,
      },
      this.httpOptions
    );
  }
}
