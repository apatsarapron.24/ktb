import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class SubmitService {
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
    }),
  };
  results: any;


  constructor(private http: HttpClient, private router: Router) { }

  SubmitWork(requestId: any) {
    return this.http.post(
      this.api_Url + '/api/pps/product/BUComment/submit',
      {
        requestId: requestId,
      },
      this.httpOptions
    );
  }
  SendBackWork(requestId: any) {
    return this.http.post(
      this.api_Url + '/api/pps/product/BUComment/replySubmit',
      {
        requestId: requestId,
      },
      this.httpOptions
    );
  }
  linkToAny() {
    console.log('send');
    this.SubmitWork(localStorage.getItem('requestId')).subscribe((res) => {
      if (res['message'] === 'กรุณากรอกข้อมูลผู้ดำเนินการ') {
        this.router.navigate(['/save-manager']);
      }
    });

  }

  sendDocument_PO(requestId: any, page: any) {
    return this.http.post(this.api_Url + '/api/pps/product/POComment/send',
      {
        'requestId': requestId,
        'bu': page
      }

    );
  }

  sendComment_PC(requestId: any) {
    return this.http.post(this.api_Url + '/api/pps/product/PCComment/send',
      {
        'requestId': requestId
      }

    );
  }
}
