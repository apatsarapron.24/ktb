import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class RiskService {
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
    }),
  };
  results: any;

  status_state = false;
  constructor(private http: HttpClient) { }

  getRisk(requestId: any) {
    return this.http.post(
      this.api_Url + '/api/pps/product/BUComment/risk',
      {
        requestId: requestId,
      },
      this.httpOptions
    );
  }

  UpdateRisk(data: any) {
    console.log('Updatedata:', data);
    return this.http.post(
      this.api_Url + '/api/pps/product/BUComment/risk/update',
      // {
      //   requestId: data.requestId,
      //   processConsiders: data.processConsiders,
      //   processConsidersDelete: data.processConsidersDelete,
      //   processConsidersSubDelete: data.processConsidersSubDelete,
      //   processConsidersSubRiskDelete: data.processConsidersSubRiskDelete,
      //   readiness: data.readiness,
      //   readinessDelete: [],
      //   readinessDetailDelete: [],
      //   extraComment: data.extraComment,
      //   extraCommentDelete: data.extraCommentDelete,
      //   status: data.status
      // },
      data,
      this.httpOptions
    );
  }

  getRisk_PO(requestId: any) {
    return this.http.post(
      this.api_Url + '/api/pps/product/POComment/risk',
      {
        requestId: requestId,
      },
      this.httpOptions
    );
  }

  UpdataCommentRisk_PO(data: any) {
    console.log('service:', data);
    // console.log('httpOptions');
    return this.http.post(
      this.api_Url + '/api/pps/product/POComment/risk/update',
      data,
      this.httpOptions
    );
  }
  // ========================================PC role======================================================//
  getRisk_PC(requestId: any) {
    return this.http.post(
      this.api_Url + '/api/pps/product/PCComment/risk',
      {
        requestId: requestId,
      },
      this.httpOptions
    );
  }

  UpdataCommentRisk_PC(data: any) {
    console.log('service:', data);
    // console.log('httpOptions');
    return this.http.post(
      this.api_Url + '/api/pps/product/PCComment/risk/update',
      data,
      this.httpOptions
    );
  }
}
