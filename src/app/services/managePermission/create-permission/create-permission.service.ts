import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CreatePermissionService {
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    })
  };

  constructor(private http: HttpClient) { }

  divisionList() {
    return  this.http.get(this.api_Url + '/api/pps/admin/list/grp');
  } // สายงาน

  workgroupList() {
    return  this.http.get(this.api_Url + '/api/pps/admin/list/sctr');
  } // กลุ่มงาน

  departmentList() {
    return  this.http.get(this.api_Url + '/api/pps/admin/list/dept');
  } // ฝ่ายงาน

  userSearch(search) {
    return  this.http.post(this.api_Url + '/api/pps/admin/avanceSearch', search);
  }

  getRole() {
    return  this.http.get(this.api_Url + '/api/pps/admin/role-permission');
  }

  saveUser(saveData) {
    const sendSave = { data: saveData};
    return  this.http.post(this.api_Url + '/api/pps/admin/saveUserRole', sendSave);
  }
}
