import { TestBed } from '@angular/core/testing';

import { CreatePermissionService } from './create-permission.service';

describe('CreatePermissionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CreatePermissionService = TestBed.get(CreatePermissionService);
    expect(service).toBeTruthy();
  });
});
