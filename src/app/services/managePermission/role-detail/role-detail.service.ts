import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import { environment } from '../../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class RoleDetailService {
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    })
  };
  results: any;

  constructor(private http: HttpClient) {
  }

  getRole() {
    console.log(this.api_Url);
    return this.http.get(this.api_Url + '/api/pps/admin/role-permission');
  }
  getRoleDetail(data: any) {
    const id = { id: data };
    console.log(this.api_Url);
    return this.http.post(this.api_Url + '/api/pps/admin/role-permission-detail', id);
  }
  getUser() {
    return this.http.get(this.api_Url + '/api/pps/admin/getUserInSystem');
  }
  deleteUser(data: any) {
    return this.http.post(
      this.api_Url + '/api/pps/admin/deleteUserRole',
      data,
      this.httpOptions
    );
  }

  postRolePermissionDetail(data) {
    return this.http.post(this.api_Url + '/api/pps/admin/role-permission-detail/update',
      data, this.httpOptions
    );
  }
}
