import { TestBed } from '@angular/core/testing';

import { RoleDetailService } from './role-detail.service';

describe('RoleDetailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RoleDetailService = TestBed.get(RoleDetailService);
    expect(service).toBeTruthy();
  });
});
