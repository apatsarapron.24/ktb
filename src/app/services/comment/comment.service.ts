import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommentService {
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    })
  };
  results: any;
  status_state = false;

  constructor(private http: HttpClient) {
  }

  checkComment(id) {
    return this.http.post(this.api_Url + '/api/pps/product/checkcomment/permission', { requestId: id });
  }

  commentList(id) {
    return this.http.post(this.api_Url + '/api/pps/product/comment/list', { requestId: id });
  }

  commentDraft(id) {
    return this.http.post(this.api_Url + '/api/pps/product/getcommentdraft', { requestId: id });
  }

  commentSend(body) {
    return this.http.post(this.api_Url + '/api/pps/product/send/comment', body);
  }
}
