import { TestBed } from '@angular/core/testing';

import { ConsiderBoardService } from './consider-board.service';

describe('ConsiderBoardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConsiderBoardService = TestBed.get(ConsiderBoardService);
    expect(service).toBeTruthy();
  });
});
