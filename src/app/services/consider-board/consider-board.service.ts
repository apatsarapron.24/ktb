import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConsiderBoardService {
  status_state = false;
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    })
  };
  results: any;
  constructor(private http: HttpClient) { }

  // updateผลการพิจารณา
  UpdateconsiderResult(data: any) {
    return this.http.post(this.api_Url + '/api/pps/product/considerResult/update',
      data,
      this.httpOptions
    );
  }
  // getผลการพิจารณา
  postconsiderResult(data: any) {
    return this.http.post(this.api_Url + '/api/pps/product/considerResult',
      data,
      this.httpOptions
    );
  }
  // getผลการพิจารณาหลังส่งงาน
  postconsiderResultAfterSend(data: any) {
    return this.http.post(this.api_Url + '/api/pps/product/considerResultAfterSend',
      data,
      this.httpOptions
    );
  }
}
