import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class SysManagementRulesService {
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      // 'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
    }),
  };
  results: any;
  status_state = false;

  constructor(private http: HttpClient) { }
  getSysmanagementRule() {
    return this.http.get(
      this.api_Url + '/api/pps/admin/System-manage-ment',
      this.httpOptions
    );
  }

  updateAndDeleteSystemManagementRule(data) {
    return this.http.post(
      this.api_Url + '/api/pps/admin/System-manage-ment/saveOrDel',
      {
        sysManagement: {
          update: data.sysManagement.update,
          delete: data.sysManagement.delete,
        },
        sysManagementRuleOfficial: {
          update: data.sysManagementRuleOfficial.update,
          delete: data.sysManagementRuleOfficial.delete,
        },
        sysManagementRuleLaw: {
          update: data.sysManagementRuleLaw.update,
          delete: data.sysManagementRuleLaw.delete,
        },
        sysManagementRule2: {
          update: data.sysManagementRule2.update,
          delete: data.sysManagementRule2.delete,
        },
        sysManagementRule1: {
          update: data.sysManagementRule1.update,
          delete: data.sysManagementRule1.delete,
        },
      },
      this.httpOptions
    );
  }
  // =========================================New API ===============================================//
  GetDetailComplianceLegal() {
    return this.http.get(
      this.api_Url + '/api/pps/admin/detailComplianceLegal',
      this.httpOptions
    );
  }

  updateSystemManagement(data: any) {
    return this.http.post(
      this.api_Url + '/api/pps/admin/updateComplianceLegal',
      data,
      this.httpOptions
    );
  }


}


