import { TestBed } from '@angular/core/testing';

import { SysManagementRulesService } from './sys-management-rules.service';

describe('SysManagementRulesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SysManagementRulesService = TestBed.get(SysManagementRulesService);
    expect(service).toBeTruthy();
  });
});
