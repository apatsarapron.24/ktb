import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    })
  };
  results: any;

  constructor(private http: HttpClient) { }

  getDashboard() {
    console.log(this.httpOptions);
    return this.http.get(this.api_Url + '/api/pps/product/getlist');
  }

  getsummaryIssue(requestId: number) {
    return this.http.post(this.api_Url + '/api/pps/product/issue/summaryIssue',
      { 'requestId': requestId });
  }
}
