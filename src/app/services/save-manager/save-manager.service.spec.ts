import { TestBed } from '@angular/core/testing';

import { SaveManagerService } from './save-manager.service';

describe('SaveManagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SaveManagerService = TestBed.get(SaveManagerService);
    expect(service).toBeTruthy();
  });
});
