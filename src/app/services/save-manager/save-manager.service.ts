import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class SaveManagerService {
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      // 'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
    }),
  };
  results: any;

  constructor(private http: HttpClient) { }

  GetList_creators(requestId: any) {
    // console.log('requestId API', requestId);
    return this.http.post(
      this.api_Url + '/api/pps/product/creators',
      {
        requestId: requestId,
      },
      this.httpOptions
    );
  }

  Search_employee(search: any) {
    return this.http.post(
      this.api_Url + '/api/pps/product/employee/search',
      {
        employeeSearch: search,
      },
      this.httpOptions
    );
  }

  Create_employee(data: any) {
    return this.http.post(
      this.api_Url + '/api/pps/product/creators/create',
      data,
      this.httpOptions
    );
  }

  Update_employee(data: any) {
    console.log(' Update DD', data);
    return this.http.post(
      this.api_Url + '/api/pps/product/creators/update',
      {
        requestId: Number(localStorage.getItem('requestId')),
        oldType: data.oldType,
        newType: data.newType,
        oldId: data.oldId,
        isEmpower: data.isEmpower,
        attachments: data.attachments
      },
      this.httpOptions
    );
  }

  Delete_employee(data: any) {
    console.log('DD', data);
    return this.http.post(
      this.api_Url + '/api/pps/product/creators/delete',
      {
        requestId: Number(data.requestId),
        type: data.type,
        id: data.id,
      },
      this.httpOptions
    );
  }
}
