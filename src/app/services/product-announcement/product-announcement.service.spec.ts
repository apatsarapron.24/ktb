import { TestBed } from '@angular/core/testing';

import { ProductAnnouncementService } from './product-announcement.service';

describe('ProductAnnouncementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductAnnouncementService = TestBed.get(ProductAnnouncementService);
    expect(service).toBeTruthy();
  });
});
