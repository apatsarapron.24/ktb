import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductAnnouncementService {
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    })
  };
  status_state = false;

  constructor(private http: HttpClient) { }

  getpromulgate(data: any) {
    return this.http.post(this.api_Url + '/api/pps/product/promulgate',
      data,
      this.httpOptions
    );
  }

  postpromulgate(data: any) {
    return this.http.post(this.api_Url + '/api/pps/product/promulgate/save',
      data,
      this.httpOptions
    );
  }

  getfile(path) {
    return this.http.post(this.api_Url + '/api/pps/admin/file',
      {
        'path': path
      });
  }

}
