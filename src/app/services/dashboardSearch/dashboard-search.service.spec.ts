import { TestBed } from '@angular/core/testing';

import { DashboardSearchService } from './dashboard-search.service';

describe('DashboardSearchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DashboardSearchService = TestBed.get(DashboardSearchService);
    expect(service).toBeTruthy();
  });
});
