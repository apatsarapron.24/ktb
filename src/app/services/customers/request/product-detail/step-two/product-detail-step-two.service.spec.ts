import { TestBed } from '@angular/core/testing';

import { ProductDetailStepTwoService } from './product-detail-step-two.service';

describe('ProductDetailStepTwoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductDetailStepTwoService = TestBed.get(ProductDetailStepTwoService);
    expect(service).toBeTruthy();
  });
});
