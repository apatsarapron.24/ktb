import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import { environment } from '../../../../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ProductDetailStepTwoService {
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    })
  };
  results: any;

  constructor(private http: HttpClient) {
  }

  getProductDetailStepTwo(documentID: number) {
    console.log(this.httpOptions);
    return this.http.post(this.api_Url + '/api/pps/product/detail/detail/page2', {'requestId': documentID});
  }

  updateProductDetailStepTwo(data: object) {
    console.log(this.httpOptions);
    return  this.http.post(this.api_Url + '/api/pps/product/detail/update/page2', data);
  }
}
