import { TestBed } from '@angular/core/testing';

import { CommentsFinanceService } from './comments-finance.service';

describe('CommentsFinanceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CommentsFinanceService = TestBed.get(CommentsFinanceService);
    expect(service).toBeTruthy();
  });
});
