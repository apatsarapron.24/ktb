import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import { environment } from '../../../environments/environment';



@Injectable({
  providedIn: 'root'
})
export class CommentsFinanceService {
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    })
  };
  results: any;

  constructor(private http: HttpClient) {
  }

  getFinancialManagement() {
    console.log(this.httpOptions);
    return this.http.get(this.api_Url + '/api/pps/admin/getSupInformationFinancial');
  }

  getPortfolioList(data: object) {
    console.log(this.httpOptions);
    return this.http.post(this.api_Url + '/api/pps/admin/updateSupInformationFinancial', data);
  }
}
