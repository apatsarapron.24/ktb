import { TestBed } from '@angular/core/testing';

import { StepTwoSummaryService } from './step-two.service';

describe('StepTwoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StepTwoSummaryService = TestBed.get(StepTwoSummaryService);
    expect(service).toBeTruthy();
  });
});
