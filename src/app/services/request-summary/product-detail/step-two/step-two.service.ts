import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { stringify } from '@angular/compiler/src/util';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class StepTwoSummaryService {

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
    }),
  };

  getStepTwo(requestId: any) {
    return this.http.post(
      this.api_Url + '/api/pps/product/detail/get/barriers',
      {
        requestId: requestId,
      },
      this.httpOptions
    );
  }

  updateStepTwo(data: any) {
    return this.http.post(this.api_Url + '/api/pps/product/detail/update/barriers', data);
  }
}
