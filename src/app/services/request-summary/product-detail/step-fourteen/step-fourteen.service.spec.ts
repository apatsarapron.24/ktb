import { TestBed } from '@angular/core/testing';

import { StepFourteenService } from './step-fourteen.service';

describe('StepFourteenService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StepFourteenService = TestBed.get(StepFourteenService);
    expect(service).toBeTruthy();
  });
});
