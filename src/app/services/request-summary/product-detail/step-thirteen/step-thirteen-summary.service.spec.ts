import { TestBed } from '@angular/core/testing';

import { StepThirteenSummaryService } from './step-thirteen-summary.service';

describe('StepThirteenSummaryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StepThirteenSummaryService = TestBed.get(StepThirteenSummaryService);
    expect(service).toBeTruthy();
  });
});
