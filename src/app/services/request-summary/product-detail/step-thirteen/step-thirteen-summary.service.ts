import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { stringify } from '@angular/compiler/src/util';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class StepThirteenSummaryService {

  constructor(
    private http: HttpClient,
    private router: Router
    ) { }

  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
    }),
  };

  getStepThirteen(requestId: any) {
    return this.http.post(
      this.api_Url + '/api/pps/product/detail/detail/page12',
      {
        requestId: requestId,
      },
      this.httpOptions
    );
  }

  updateStepThirteen(data) {
    return this.http.post(this.api_Url + '/api/pps/product/detail/update/page12', data);
  }
}
