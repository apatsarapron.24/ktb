import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { stringify } from '@angular/compiler/src/util';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class InFoFirstStepSummaryService {
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
    }),
  };

  getDefultFirstStep(id: number) {
    console.log(this.api_Url);
    // const documentId = { 'id': id };
    return this.http.post(this.api_Url + '/api/pps/product/default/detail/page1', { id: id });
  }

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }
}
