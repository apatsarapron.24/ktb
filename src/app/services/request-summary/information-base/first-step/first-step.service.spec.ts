import { TestBed } from '@angular/core/testing';

import { InFoFirstStepSummaryService } from './first-step.service';

describe('InFoFirstStepSummaryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InFoFirstStepSummaryService = TestBed.get(InFoFirstStepSummaryService);
    expect(service).toBeTruthy();
  });
});
