import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NotificationMgmtService {
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    })
  };
  results: any;

  constructor(private http: HttpClient) { }

  findEmailConfig(process, condition) {
    return this.http
      .post(this.api_Url + '/api/pps/product/findEmailConfig',
        {
          'process': process,
          'condition': condition
        });
  }

  addConfigEmail(obj) {
    return this.http
      .post(this.api_Url + '/api/pps/product/AddConfigEmail',
        obj);
  }

  clearData(process, condition) {
    return this.http
      .post(this.api_Url + '/api/pps/product/deleteEmailConfig',
        {
          'process': process,
          'condition': condition
        });
  }

}
