import { TestBed } from '@angular/core/testing';

import { NotificationMgmtService } from './notification-mgmt.service';

describe('NotificationMgmtService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NotificationMgmtService = TestBed.get(NotificationMgmtService);
    expect(service).toBeTruthy();
  });
});
