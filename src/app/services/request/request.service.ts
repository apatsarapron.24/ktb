import { Injectable, ViewChild } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RequestService {


  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      // 'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    })
  };
  results: any;

  // @ViewChild(informationBaseFirstStepComponent) StepOne: informationBaseFirstStepComponent;

  constructor(
    private router: Router,
    private http: HttpClient,
  ) { }

  private subject = new Subject<any>();

  changeStatusInfo = false; // false = ไม่มีการเเก้ไข , true = มีการเเก้ไข
  saveStatusInfo = false; // false = ไม่ให้บันทึก , true = ให้บันทึก
  pathname = null; // จากหน้า
  goToLink = null; // ส่งไปหน้า

  sendEvent() {
    this.subject.next();
  }

  getEvent(): Observable<any> {
    return this.subject.asObservable();
  }

  statusSibar(requestId) {
    // console.log('requestId API', requestId);
    return this.http.post(this.api_Url + '/api/pps/product/pagestatus',
      {
        'requestId': requestId
      });
  }

  checkMenu(requestId) {
    return this.http.post(this.api_Url + '/api/pps/admin/role-permission-page',
      {
        'requestId': requestId
      });
  }

  checkDefault(requestId) {
    // check step 4
    return this.http.post(this.api_Url + '/api/pps/product/detail/check/default',
      {
        'requestId': requestId
      });
  }

  getfile(path) {
    return this.http.post(this.api_Url + '/api/pps/admin/file',
      {
        'path': path
      });
  }



  // updatePage(obj: any) {
  //   return this.http.post(this.api_Url + '/api/pps/product/detail/update/page1', obj)
  // }

  // -> เข้ามาเปลี่ยน status การเเก้ไขข้อมูล (ใช้ตรวจสถานะการบันทึก)
  inPageStatus(status?) {
    this.changeStatusInfo = status;
    console.log('inPageStatus', this.changeStatusInfo);
    console.log('inPageStatus status:', status);
  }

  // -> เข้ามาเช็ค status การเเก้ไขข้อมูล (ใช้ตรวจสถานะการบันทึก)
  outPageStatus() {
    // this.changeStatusInfo = true;
    console.log('outPageStatus 001', this.changeStatusInfo);
    if (this.changeStatusInfo === true || this.changeStatusInfo === false) {
      console.log('outPageStatus 002', this.changeStatusInfo);
      return this.changeStatusInfo;
    } else {
      console.log('outPageStatus non data 003', this.changeStatusInfo);
      return false;
    }
  }

  savePage(data?) {
    this.pathname = data.pathname;
    this.goToLink = data.goToLink;
    console.log('pathname:',this.pathname )
    console.log('goToLink:',this.goToLink )
  }

  getsavePage() {
    // console.log('getsavePage');
    return { saveStatusInfo: this.saveStatusInfo, pathname: this.pathname, goToLink: this.goToLink };
  }

  changePageGoToLink() {
    return this.goToLink;
  }

  resetPageInfo() {
    this.pathname = null;
    this.goToLink = null;
  }

  resetSaveStatu() {
    console.log('resetSaveStatu');
    this.changeStatusInfo = false;
  }

  resetSaveStatusInfo() {
    console.log('resetSaveStatusInfo');
    this.changeStatusInfo = false;
    // this.saveStatusInfo = false;
    // this.pathname = null;
    // this.goToLink = null;
  }
  OtherHeaderBar(gotolink) {
    this.goToLink = gotolink
    console.log('OtherHeaderBar:', this.goToLink)

  }
}


