import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import { environment } from '../../../../../environments/environment';



@Injectable({
  providedIn: 'root'
})
export class ProductDefultFirstStepService {
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    })
  };
  results: any;

  constructor(private http: HttpClient) {
  }

  getDefultFirstStep(id: number, getConclusions?: string ) {
    let conclusions = null;
    if (getConclusions === 'summary') {
      conclusions = true;
    }
    console.log(this.api_Url);
    // const documentId = { 'id': id };
    return this.http.post(this.api_Url + '/api/pps/product/default/detail/page1', { id: id , conclusions: conclusions});
  }
  upDateDefultFirstStep(data: any) {
    console.log(this.api_Url);
    // const documentId = { 'data': data };
    return this.http.post(this.api_Url + '/api/pps/product/default/update/page1', data);
  }

  // summary
  getSummaryRequestList() {
    console.log(this.api_Url);
    return this.http.get(this.api_Url + '/api/pps/product/getRequestForRepeat');
  }

  repeatProduct(data: any) {
    console.log(this.api_Url);
    return this.http.post(this.api_Url + '/api/pps/product/repeatProduct', data);
  }

}
