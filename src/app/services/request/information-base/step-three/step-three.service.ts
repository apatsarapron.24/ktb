import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';

import { Observable, ReplaySubject } from 'rxjs';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StepThreeService {
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    })
  };
  results: any;

  constructor(private http: HttpClient) {
  }

  savePage3(body) {
    console.log(this.httpOptions);
    return this.http.post(this.api_Url + '/api/pps/product/default/update/page3', body);
  }

  getPage3(body) {
    return this.http.post(this.api_Url + '/api/pps/product/default/detail/page3', body);
  }
}






