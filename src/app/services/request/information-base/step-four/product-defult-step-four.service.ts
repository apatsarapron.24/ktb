import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import { environment } from '../../../../../environments/environment';



@Injectable({
  providedIn: 'root'
})
export class ProductDefultStepFourService {
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    })
  };
  results: any;

  constructor(private http: HttpClient) {
  }

  getDefultFirstStep(documentId: number) {
    console.log(this.api_Url);
    return this.http.post(this.api_Url + '/api/pps/product/default/detail/page4', { id: documentId });
  }
  upDateDefultFirstStep(data: any) {
    console.log(this.api_Url);
    return this.http.post(this.api_Url + '/api/pps/product/default/update/page4', data);
  }
}
