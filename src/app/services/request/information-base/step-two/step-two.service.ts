import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';

import { Observable, ReplaySubject } from 'rxjs';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StepTwoService {
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    })
  };
  results: any;

  constructor(private http: HttpClient) {
  }

  savePage2(body) {
    console.log(this.httpOptions);
    return this.http.post(this.api_Url + '/api/pps/product/default/update/page2', body);
  }

  getPage2(body) {
    return this.http.post(this.api_Url + '/api/pps/product/default/detail/page2', body);
  }

  getFilterSystemManagement() {
    return this.http.get(this.api_Url + '/api/pps/admin/system-management/filter'
    );
  }
}
