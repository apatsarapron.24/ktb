import { TestBed } from '@angular/core/testing';

import { StepEightService } from './step-eight.service';

describe('StepEightService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StepEightService = TestBed.get(StepEightService);
    expect(service).toBeTruthy();
  });
});
