import { TestBed } from '@angular/core/testing';

import { StepSevenService } from './step-seven.service';

describe('StepSevenService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StepSevenService = TestBed.get(StepSevenService);
    expect(service).toBeTruthy();
  });
});
