import { TestBed } from '@angular/core/testing';

import { StepElevenService } from './step-eleven.service';

describe('StepElevenService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StepElevenService = TestBed.get(StepElevenService);
    expect(service).toBeTruthy();
  });
});
