import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StepTwoService {

  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    })
  };
  results: any;

  constructor(private http: HttpClient) {
  }

  getProductDetailStepTwo(documentID: number, getConclusions?: string) {
    let conclusions = null;
    if (getConclusions === 'summary') {
      conclusions = true;
    }
    console.log(this.httpOptions);
    return this.http.post(this.api_Url + '/api/pps/product/detail/detail/page2', {'requestId': documentID, conclusions: conclusions});
  }

  updateProductDetailStepTwo(data: object) {
    console.log(this.httpOptions);
    return  this.http.post(this.api_Url + '/api/pps/product/detail/update/page2', data);
  }
}
