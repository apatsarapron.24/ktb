import { dateFormat } from 'highcharts';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StepFiveService {

  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
    }),
  };
  results: any;

  constructor(private http: HttpClient) { }

  dateFormat(dateForm) {
    let dateData = null;
    dateData = { isRange: false, singleDate: { jsDate: new Date(dateForm['releaseDate']) } };
    return dateData;
  }

  getProduct_detail_step_five(requestId: any, getConclusions?: string) {
    let conclusions = null;
    if ( getConclusions === 'summary') {
      conclusions = true;
    }
    return this.http.post(
      this.api_Url + '/api/pps/product/detail/detail/page5',
      {
        requestId: requestId,
        conclusions: conclusions
      },
      this.httpOptions
    );
  }

  updateProduct_detail_step_five(data: any) {
    return this.http.post(
      this.api_Url + '/api/pps/product/detail/update/page5',
      data,
      this.httpOptions
    );
  }
}
