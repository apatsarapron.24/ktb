import { TestBed } from '@angular/core/testing';

import { StepFiveService } from './step-five.service';

describe('StepFiveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StepFiveService = TestBed.get(StepFiveService);
    expect(service).toBeTruthy();
  });
});
