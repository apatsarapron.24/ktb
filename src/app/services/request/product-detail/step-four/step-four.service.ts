import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StepFourService {
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    })
  };
  results: any;

  constructor(private http: HttpClient) {
  }

  getPage4(requestId: any, getConclusions?: string) {
    let conclusions = null;
    if ( getConclusions === 'summary') {
      conclusions = true;
    }
    console.log('requestId : ', requestId);
    console.log(this.httpOptions);
    return this.http.post(this.api_Url + '/api/pps/product/detail/detail/page4',
    {
      'requestId' : requestId,
      'conclusions': conclusions
    }
    );
  }

  savePage4(data: any) {
    return this.http.post(this.api_Url + '/api/pps/product/detail/update/page4',
    data,
    this.httpOptions
    );
  }
}
