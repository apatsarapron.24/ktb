import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { stringify } from '@angular/compiler/src/util';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root',
})
export class StepThreeService {

  constructor(private http: HttpClient,
    private router: Router) { }
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
    }),
  };
  results: any;
  requestId(requestId: any) {
    throw new Error('Method not implemented.');
  }
  checkDefault(requestId) {
    // check page
    return this.http.post(this.api_Url + '/api/pps/product/detail/get/group',
      {
        'requestId': requestId
      });
  }

  getProduct_detail_step_three(requestId: any, getConclusions?: string) {
    let conclusions = null;
    if ( getConclusions === 'summary') {
      conclusions = true;
    }
    return this.http.post(
      this.api_Url + '/api/pps/product/detail/detail/page3.1',
      {
        requestId: requestId,
        conclusions: conclusions
      },
      this.httpOptions
    );
  }

  update_step_three_business(data: any) {
    return this.http.post(
      this.api_Url + '/api/pps/product/detail/update/page3.1',
      data,
      this.httpOptions
    );
  }

  get_step_three_personal(requestId: any, getConclusions?: string) {
    let conclusions = null;
    if ( getConclusions === 'summary') {
      conclusions = true;
    }
    return this.http.post(
      this.api_Url + '/api/pps/product/detail/detail/page3.2',
      {
        requestId: requestId,
        conclusions: conclusions
      },
      this.httpOptions
    );
  }

  update_step_three_personal(data: any) {
    return this.http.post(
      this.api_Url + '/api/pps/product/detail/update/page3.2',
      data,
      this.httpOptions
    );
  }

  get_step_three_housing(requestId: any, getConclusions?: string) {
    let conclusions = null;
    if ( getConclusions === 'summary') {
      conclusions = true;
    }
    return this.http.post(
      this.api_Url + '/api/pps/product/detail/detail/page3.3',
      {
        requestId: requestId,
        conclusions: conclusions
      },
      this.httpOptions
    );
  }

  get_step_three_non_credit(requestId: any, getConclusions?: string) {
    let conclusions = null;
    if ( getConclusions === 'summary') {
      conclusions = true;
    }
    return this.http.post(
      this.api_Url + '/api/pps/product/detail/detail/page3.4',
      {
        requestId: requestId,
        conclusions: conclusions
      },
      this.httpOptions
    );
  }

  update_step_three_housing(data: any) {
    return this.http.post(
      this.api_Url + '/api/pps/product/detail/update/page3.3',
      data,
      this.httpOptions
    );
  }

  update_step_three_non_credit(data: any) {
    return this.http.post(
      this.api_Url + '/api/pps/product/detail/update/page3.4', data
    );
  }

  get_step_three_other(requestId: any, checkPageNon: any, getConclusions?: string) {
    let conclusions = null;
    if ( getConclusions === 'summary') {
      conclusions = true;
    }
    return this.http.post(
      this.api_Url + '/api/pps/product/detail/detail/page3.5',
      {
        requestId: requestId,
        type: checkPageNon,
        conclusions: conclusions
      },
      this.httpOptions
    );
  }

  update_step_three_other(data: any) {

    console.log('service:', data);
    return this.http.post(
      this.api_Url + '/api/pps/product/detail/update/page3.5',
      data,
      this.httpOptions
    );
  }

  checkRoutPage(res, number) {
    if (res['data'].product[Number(number)] !== 'other_non_creadit') {
      this.router.navigate(['/request/product-detail/step3/' + res['data'].product[Number(number)]]);
      localStorage.setItem('check', number);
      sessionStorage.setItem('check_non', '');

    } else {
      this.router.navigate(['/request/product-detail/step3/other_non_creadit']);
      localStorage.setItem('check', number);
      sessionStorage.setItem('check_non', 'NonCredit');
      console.log('mmmmmmNonCredit','NonCredit');
    }
  }

  CheckNextPage(page, action) {
    this.checkDefault(localStorage.getItem('requestId')).subscribe(res => {
      console.log('this.getPage.checkDefault', res['data']);
      sessionStorage.setItem('page', '');

      console.log(sessionStorage.getItem('page'), res['data'].product.length, page);
    if (Number(page) < res['data'].product.length - 1 && action === 'next') {
      const number = stringify(Number(page) + 1);
      this.checkRoutPage(res, number);
      if (Number(page) === res['data'].product.length - 2 || Number(page) === res['data'].product.length) {
        sessionStorage.setItem('page', 'last');
      }
    } else if (Number(page) < res['data'].product.length && action === 'back' && Number(page) !== 0) {
      sessionStorage.setItem('page', '');
      const number = stringify(Number(page) - 1);
      this.checkRoutPage(res, number);
    } else if (page === 'A') {
      this.checkRoutPage(res, 0);
    } else {
      console.log('ครบแล้ว');
      this.router.navigate(['/request/product-detail/step4']);
      sessionStorage.setItem('page', '');
      sessionStorage.setItem('check_non', '');
    }
  });
}


checkRoutPageSummary(res, number) {
  if (res['data'].product[Number(number)] !== 'other_non_creadit') {
    this.router.navigate(['/request-summary/product-detail/step4/' + res['data'].product[Number(number)]]);
    localStorage.setItem('check', number);
    sessionStorage.setItem('check_non', '');

  } else {
    this.router.navigate(['/request-summary/product-detail/step4/other_non_creadit']);
    localStorage.setItem('check', number);
    sessionStorage.setItem('check_non', 'NonCredit');
    console.log('NonCredit');
  }
}

CheckNextPageSummary(page, action) {
  this.checkDefault(localStorage.getItem('requestId')).subscribe(res => {
    console.log('this.getPage.checkDefault', res['data']);
    sessionStorage.setItem('page', '');

    console.log(sessionStorage.getItem('page'), res['data'].product.length, page);
  if (Number(page) < res['data'].product.length - 1 && action === 'next') {
    const number = stringify(Number(page) + 1);
    this.checkRoutPageSummary(res, number);
    if (Number(page) === res['data'].product.length - 2 || Number(page) === res['data'].product.length) {
      sessionStorage.setItem('page', 'last');
    }
  } else if (Number(page) < res['data'].product.length && action === 'back' && Number(page) !== 0) {
    sessionStorage.setItem('page', '');
    const number = stringify(Number(page) - 1);
    this.checkRoutPageSummary(res, number);
  } else if (page === 'A') {
    this.checkRoutPageSummary(res, 0);
  } else {
    console.log('ครบแล้ว');
    this.router.navigate(['/request-summary/product-detail/step5']);
    sessionStorage.setItem('page', '');
    sessionStorage.setItem('check_non', '');
  }
});
}
}
