import { TestBed } from '@angular/core/testing';

import { StepTenService } from './step-ten.service';

describe('StepTenService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StepTenService = TestBed.get(StepTenService);
    expect(service).toBeTruthy();
  });
});
