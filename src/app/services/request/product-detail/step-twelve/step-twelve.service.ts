import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StepTwelveService {
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      // 'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    })
  };
  results: any;

  constructor(private http: HttpClient) { }
  getDetail(requestId: any) {
    return this.http.post(this.api_Url + '/api/pps/product/detail/detail/page12',
    {
      'requestId': requestId
    });
  }

  updateDetail(dataSend) {
    return  this.http.post(this.api_Url + '/api/pps/product/detail/update/page12', dataSend);
  }
}
