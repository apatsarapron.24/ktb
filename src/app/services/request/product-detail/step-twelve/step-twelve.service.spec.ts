import { TestBed } from '@angular/core/testing';

import { StepTwelveService } from './step-twelve.service';

describe('StepTwelveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StepTwelveService = TestBed.get(StepTwelveService);
    expect(service).toBeTruthy();
  });
});
