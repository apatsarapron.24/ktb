import { TestBed } from '@angular/core/testing';

import { StepSixService } from './step-six.service';

describe('StepSixService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StepSixService = TestBed.get(StepSixService);
    expect(service).toBeTruthy();
  });
});
