import { TestBed } from '@angular/core/testing';

import { SendWorkService } from './send-work.service';

describe('SendWorkService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SendWorkService = TestBed.get(SendWorkService);
    expect(service).toBeTruthy();
  });
});
