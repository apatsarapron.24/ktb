import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class StepNineService {
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      // 'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
    }),
  };
  results: any;

  constructor(private http: HttpClient) {}

  getData_step_nine(requestId: any, getConclusions?: string) {
    let conclusions = null;
    if ( getConclusions === 'summary') {
      conclusions = true;
    }
    return this.http.post(
      this.api_Url + '/api/pps/product/detail/detail/page9',
      {
        requestId: requestId,
        conclusions: conclusions
      },
      this.httpOptions
    );
  }

  Update_step_nine(data: any) {
    return this.http.post(
      this.api_Url + '/api/pps/product/detail/update/page9',
      data,
      this.httpOptions
    );
  }
}
