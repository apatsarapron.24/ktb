import { TestBed } from '@angular/core/testing';

import { StepNineService } from './step-nine.service';

describe('StepNineService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StepNineService = TestBed.get(StepNineService);
    expect(service).toBeTruthy();
  });
});
