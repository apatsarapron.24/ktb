import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import { environment } from '../../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class SummaryOfConsiderationService {
  status_state = false;
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    })
  };
  results: any;

  constructor(
    private http: HttpClient
  ) { }

  productGetGroup(requestId) {
    return this.http.post(this.api_Url + '/api/pps/product/detail/get/group', { 'requestId': requestId });
  }
  productDetailPageStatus(requestId) {
    return this.http.post(this.api_Url + '/api/pps/product/pagestatus', { 'requestId': requestId });
  }

  getReview(requestId) {
    return this.http.post(this.api_Url + '/api/pps/product/collect', { 'requestId': requestId });
  }

  upDateReview(data) {
    return this.http.post(this.api_Url + '/api/pps/product/collect/update', data);
  }

  getIssueMasterFileReport(requestId: number) {
    return this.http.post(this.api_Url + '/api/pps/product/report/issueMasterFileReport',
      { 'requestId': requestId });
  }

  postexportPDF(requestId: number) {
    return this.http.post(this.api_Url + '/api/pps/product/exportPDF',
      { 'requestId': requestId });
  }

  postexportWord(requestId: number) {
    return this.http.post(this.api_Url + '/api/pps/product/exportMSWord',
      { 'requestId': requestId });
  }
}
