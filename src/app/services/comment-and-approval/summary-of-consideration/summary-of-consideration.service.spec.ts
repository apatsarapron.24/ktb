import { TestBed } from '@angular/core/testing';

import { SummaryOfConsiderationService } from './summary-of-consideration.service';

describe('SummaryOfConsiderationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SummaryOfConsiderationService = TestBed.get(SummaryOfConsiderationService);
    expect(service).toBeTruthy();
  });
});
