import { TestBed } from '@angular/core/testing';

import { ManageSystemService } from './manage-system.service';

describe('ManageSystemService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManageSystemService = TestBed.get(ManageSystemService);
    expect(service).toBeTruthy();
  });
});
