import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ManageSystemService {
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    })
  };
  status_state = false;

  constructor(private http: HttpClient) { }

  getSystemManagement() {
    return this.http.get(this.api_Url + '/api/pps/admin/system-management',
      this.httpOptions
    );
  }

  divisionList() {
    return this.http.get(this.api_Url + '/api/pps/admin/list/grp');
  } // สายงาน

  workgroupList() {
    return this.http.get(this.api_Url + '/api/pps/admin/list/sctr');
  } // กลุ่มงาน

  departmentList() {
    return this.http.get(this.api_Url + '/api/pps/admin/list/dept');
  } // ฝ่ายงาน

  postSystemManagement(data) {
    return this.http.post(this.api_Url + '/api/pps/admin/save/system-management',
      data
    );
  }
}
