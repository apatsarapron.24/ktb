import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import { environment } from '../../../environments/environment';
import * as Highcharts from 'highcharts';
@Injectable({
  providedIn: 'root'
})
export class ProductTurnaroundService {
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    })
  };
  constructor(private http: HttpClient) { }
  divisionList() {
    return this.http.get(this.api_Url + '/api/pps/admin/list/grp');
  } // สายงาน

  workgroupList() {
    return this.http.get(this.api_Url + '/api/pps/admin/list/sctr');
  } // กลุ่มงาน

  departmentList() {
    return this.http.get(this.api_Url + '/api/pps/admin/list/dept');
  } // ฝ่ายงาน

  // getproductTurnaroundTimeReport
  getProductTurnaround(searchdata: any) {
    return this.http.post(this.api_Url + '/api/pps/product/report/productTurnaroundTimeReport',
      searchdata,
      this.httpOptions
    );
  }
  createChart(el, cfg) {
    Highcharts.chart(el, cfg);
  }
}
