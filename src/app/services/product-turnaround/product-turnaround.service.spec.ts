import { TestBed } from '@angular/core/testing';

import { ProductTurnaroundService } from './product-turnaround.service';

describe('ProductTurnaroundService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductTurnaroundService = TestBed.get(ProductTurnaroundService);
    expect(service).toBeTruthy();
  });
});
