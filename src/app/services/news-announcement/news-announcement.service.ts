import { Injectable } from '@angular/core';
import { environment } from './../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NewsAnnouncementService {
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    })
  };
  results: any;

  constructor(private http: HttpClient) { }

  newsSearch(data: any) {
    console.log('in news search');
    return this.http.post(this.api_Url + '/api/pps/admin/infomation/search',
      data,
      this.httpOptions
    );
  }

  newsDelete(data: any) {
    return this.http.post(this.api_Url + '/api/pps/admin/infomation/delete',
      data,
      this.httpOptions
    );
  }

  newsAddOrDelete(data: any) {
    return this.http.post(this.api_Url + '/api/pps/admin/infomation/save',
      data,
      this.httpOptions
    );
  }

  newsGet(data: any) {
    console.log('in news get', data);
    return this.http.post(this.api_Url + '/api/pps/admin/infomation/get',
      data,
      this.httpOptions
    );
  }

  getfile(path) {
    return this.http.post(this.api_Url + '/api/pps/admin/file',
      {
        'path': path
      });
  }
}
