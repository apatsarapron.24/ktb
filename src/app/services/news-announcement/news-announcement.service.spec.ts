import { TestBed } from '@angular/core/testing';

import { NewsAnnouncementService } from './news-announcement.service';

describe('NewsAnnouncementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NewsAnnouncementService = TestBed.get(NewsAnnouncementService);
    expect(service).toBeTruthy();
  });
});
