import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OutstandingService {

  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    })
  };
  status_state = false;
  constructor(private http: HttpClient) { }

  getOutstanding(requestId: any) {
    return this.http.post(this.api_Url + '/api/pps/product/issue',
      {
        'requestId': requestId,
      },
      this.httpOptions
    );
  }
  updateOutstanding(data) {
    return this.http.post(this.api_Url + '/api/pps/product/issue/update',
      data,
      this.httpOptions
    );
  }

  SendDataOutstanding(requestId) {
    return this.http.post(this.api_Url + '/api/pps/product/issue/send',
      {
        requestId: requestId,
      },
      this.httpOptions
    );
  }

  getCollectIssue(requestId: any) {
    return this.http.post(this.api_Url + '/api/pps/product/collect-issue',
      {
        'requestId': requestId,
      },
      this.httpOptions
    );
  }

  updateCollectIssue(data) {
    return this.http.post(this.api_Url + '/api/pps/product/collect-issue/update',
      data,
      this.httpOptions
    );
  }


}
