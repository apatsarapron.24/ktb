import { TestBed } from '@angular/core/testing';

import { ProductPerformanceService } from './product-performance.service';

describe('ProductPerformanceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductPerformanceService = TestBed.get(ProductPerformanceService);
    expect(service).toBeTruthy();
  });
});
