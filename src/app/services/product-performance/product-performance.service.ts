import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductPerformanceService {

  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    })
  };
  status_state = false;

  constructor(private http: HttpClient) { }

  getProductPerformance(requestId: number) {
    return this.http.post(this.api_Url + '/api/pps/product/performance',
      {
        'requestId': requestId,
      },
      this.httpOptions
    );
  }

  postProductPerformance(data: any) {
    return this.http.post(this.api_Url + '/api/pps/product/performance/save',
      data,
      this.httpOptions
    );
  }
}
