import { TestBed } from '@angular/core/testing';

import { ConfigGuardService } from './config-guard.service';

describe('ConfigGuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConfigGuardService = TestBed.get(ConfigGuardService);
    expect(service).toBeTruthy();
  });
});
