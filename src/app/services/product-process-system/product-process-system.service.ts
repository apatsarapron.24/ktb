import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ProductProcessSystemService {
    api_Url = environment.apiUrl;
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
        })
    };

    constructor(private http: HttpClient) {
    }

    Getproduct_process_system() {
        return this.http.get(this.api_Url + '/api/pps/admin/infomation/userGet',
            this.httpOptions
        );
    }

    Getdetailnews(id: number) {
        return this.http.post(this.api_Url + '/api/pps/admin/infomation/get',
            { id: id });
    }
}
