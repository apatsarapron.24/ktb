import { TestBed } from '@angular/core/testing';

import { ProductProcessSystemService } from './product-process-system.service';

describe('ProductProcessSystemService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductProcessSystemService = TestBed.get(ProductProcessSystemService);
    expect(service).toBeTruthy();
  });
});
