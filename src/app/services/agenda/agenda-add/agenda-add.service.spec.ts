import { TestBed } from '@angular/core/testing';

import { AgendaAddService } from './agenda-add.service';

describe('AgendaAddService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AgendaAddService = TestBed.get(AgendaAddService);
    expect(service).toBeTruthy();
  });
});
