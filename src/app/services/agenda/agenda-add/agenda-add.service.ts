import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AgendaAddService {
  status_state = false;
  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    })
  };
  results: any;
  
  constructor(private http: HttpClient) { }
  getDropdownMeettingList() {
    // console.log(this.httpOptions);
    return this.http.get(this.api_Url + '/api/pps/product/getMeetingList');
  }


  productMeetingAgenda(data: any) {
    return this.http.post(this.api_Url + '/api/pps/product/productMeetingAgenda/save',
      data,
      this.httpOptions
    );
  }

  returnAgenda(data: any) {
    return this.http.post(this.api_Url + '/api/pps/product/productMeetingAgenda',
      data,
      this.httpOptions
    );
  }

  getAgenda(requestid: any) {
    return this.http.post(this.api_Url + '/api/pps/product/productMeetingAgenda/get',
    requestid,
      this.httpOptions
    );
  }
}
