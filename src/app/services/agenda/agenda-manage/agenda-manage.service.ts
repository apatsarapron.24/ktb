import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AgendaManageService {

  api_Url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
    })
  };

  // httpOptionsFile = {
  //   headers: new HttpHeaders({
  //     'Accept': 'application/json',
  //     'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
  //   })
  // };

  results: any;

  constructor(private http: HttpClient) { }

  getMeetingList(year: string, month: string, meetingDate: string, term: string, presentation: string, no: string, product: string) {

    return this.http.post(this.api_Url + '/api/pps/admin/meeting',
      {
        'year': year,
        'month': month,
        'meetingDate': meetingDate,
        'term': term,
        'presentation': presentation,
        'no': no,
        'product': product
      },
      this.httpOptions
    );
  }
  searchMeetingList(search: any) {
    return this.http.post(this.api_Url + '/api/pps/admin/meeting',
      search,
      this.httpOptions
    );
  }
  getAgendaMeetingDetail(meetingId: string) {
    return this.http.post(this.api_Url + '/api/pps/admin/agendaDetailList',
      {
        'meetingId': meetingId,
      },
      this.httpOptions
    );
  }

  getDropdownMeettingList(meetingId: number) {
    return this.http.post(this.api_Url + '/api/pps/admin/getDropdownMeeting',
      {
        'meetingId': meetingId,
      },
      this.httpOptions
    );
  }

  postMoveAgenda(data: any) {
    return this.http.post(this.api_Url + '/api/pps/admin/moveAgenda',
      data,
      this.httpOptions
    );
  }

  postAddMeetingData(data: any) {
    return this.http.post(this.api_Url + '/api/pps/admin/setmeeting/data',
      data,
      this.httpOptions
    );
  }

  deleteMeeting(data: any) {
    return this.http.post(this.api_Url + '/api/pps/admin/deleteMeeting',
      data,
      this.httpOptions
    );
  }

  postUpdateMeetingAgenda(data: any) {
    return this.http.post(this.api_Url + '/api/pps/admin/update/MeetingAgenda',
      data,
      this.httpOptions
    );
  }

  postSetMeetingFile(fileToUpload: any) {
    const formData: FormData = new FormData();
    for (let index = 0; index < fileToUpload.length; index++) {
      formData.append('files', fileToUpload[index], fileToUpload[index].name);
    }
    // console.log(this.httpOptionsFile)
    return this.http.post(this.api_Url + '/api/pps/admin/setmeeting/files',
      formData
    );
  }

  getDropdownAllMeetingList() {
    return this.http.get(this.api_Url + '/api/pps/product/getMeetingList');
  }




}
