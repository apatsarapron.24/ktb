import { TestBed } from '@angular/core/testing';

import { AgendaManageService } from './agenda-manage.service';

describe('AgendaManageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AgendaManageService = TestBed.get(AgendaManageService);
    expect(service).toBeTruthy();
  });
});
