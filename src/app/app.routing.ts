import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';
import { AdminLayoutComponent } from './containers';
import { CustomersLayoutComponent } from './containers';
import { RequestLayoutComponent } from './containers';


import { LoginComponent } from './authener/login/login.component';
import { ErrorComponent } from './authener/error-permission/error.component';
import { AuthenerGuardsService } from './authener/guards/authener-guards.service';
import { from } from 'rxjs';
import { SelectComponent } from './authener/select/select.component';
export const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  // {
  //   path: '404',
  //   component: P404Component,
  //   data: {
  //     title: 'Page 404'
  //   }
  // },
  // {
  //   path: '500',
  //   component: P500Component,
  //   data: {
  //     title: 'Page 500'
  //   }
  // },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'select-role',
    component: SelectComponent,
    canActivate: [AuthenerGuardsService],
    data: {
      title: 'Select Role'
    }
  },
  {
    path: 'error-permission',
    component: ErrorComponent,
    canActivate: [AuthenerGuardsService],
    data: {
      title: 'Error-permission'
    }
  },

  {
    path: '',
    component: CustomersLayoutComponent,
    canActivate: [AuthenerGuardsService],
    data: {
      title: ''
    },
    children: [

      {
        path: 'dashboard1',
        loadChildren: './customers/dashboard-cus/dashboard-cus.module#DashboardCusModule',
        canActivate: [AuthenerGuardsService]
      },
      {
        path: 'system-management',
        loadChildren: './admin/manage-system/manage-system.module#ManageSystemModule',
        canActivate: [AuthenerGuardsService]
      },
      {
        path: 'system-management-customer',
        loadChildren: './customers/system-management-cus/system-management-cus.module#SystemManagementCusModule',
        canActivate: [AuthenerGuardsService]
      },
      {
        path: 'save-manager',
        loadChildren: './admin/save-manager/save-manager.module#SaveManagerModule',
        canActivate: [AuthenerGuardsService]
      },
      {
        path: 'agenda-manage',
        loadChildren: './customers/agenda-manage/agenda-manage.module#AgendaManageModule',
        canActivate: [AuthenerGuardsService]
      },


      {
        path: 'comment',
        loadChildren: './customers/comment/comment.module#CommentModule',
        canActivate: [AuthenerGuardsService]
      },
      {
        path: 'comments-finance',
        loadChildren: './customers/comments-finance/comments-finance.module#CommentsFinanceModule',
      },
      {
        path: 'notification-mgmt',
        loadChildren: './customers/notification-mgmt/notification-mgmt.module#NotificationMgmtModule',
        canActivate: [AuthenerGuardsService]
      },
      {
        path: 'manage-permission',
        loadChildren: './customers/managePermission/manage-permission.module#ManagePermissionModule',
        canActivate: [AuthenerGuardsService]
      },
      {
        path: 'news-announcement',
        loadChildren: './customers/news-announcement/news-announcement.module#NewsAnnouncementModule',
        // canActivate: [AuthenerGuardsService]
      },
      {
        path: 'comment-point',
        loadChildren: './customers/comment-point/comment-point.module#CommentPointModule',
        canActivate: [AuthenerGuardsService]
      },
      {
        path: 'comment-and-approval',
        loadChildren: './customers/comment-and-approval/comment-and-approval.module#CommentAndApprovalModule',
        canActivate: [AuthenerGuardsService]
      },
      {
        path: 'agenda-add',
        loadChildren: './customers/agenda-add/agenda-add.module#AgendaAddModule',
        canActivate: [AuthenerGuardsService]
      },
      {
        path: 'consider-board',
        loadChildren: './customers/consider-board/consider-board.module#ConsiderBoardModule',
        canActivate: [AuthenerGuardsService]
      },
      {
        path: 'outstanding',
        loadChildren: './customers/outstanding/outstanding.module#OutstandingModule',
        canActivate: [AuthenerGuardsService]
      },
      {
        path: 'product-announcement',
        loadChildren: './customers/product-announcement/product-announcement.module#ProductAnnouncementModule',
        canActivate: [AuthenerGuardsService]
      },
      {
        path: 'reports-tab',
        loadChildren: './customers/reports-tab/reports-tab.module#ReportsTabModule',
        canActivate: [AuthenerGuardsService]
      },
      {
        path: 'product-performance',
        loadChildren: './customers/product-performance/product-performance.module#ProductPerformanceModule',
        canActivate: [AuthenerGuardsService]
      },
      {
        path: 'executive-summary',
        loadChildren: './customers/executive-summary/executive-summary.module#ExecutiveSummaryModule',
        canActivate: [AuthenerGuardsService]
      },
      {
        path: 'product-process-system',
        loadChildren: './customers/Product-Process-System/product-process-system.module#ProductProcessSystemModule',
        canActivate: [AuthenerGuardsService]
      },
    ]
  },
  {
    path: 'request',
    component: RequestLayoutComponent,
    data: {
      title: 'request'
    },
    children: [
      {
        path: '',
        loadChildren: './customers/request/request.module#RequestModule'
      },
    ]
  },
  {
    path: 'request-summary',
    component: RequestLayoutComponent,
    data: {
      title: 'request-summary'
    },
    children: [
      {
        path: '',
        loadChildren: './customers/request-summary/request-summary.module#RequestSummaryModule'
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
