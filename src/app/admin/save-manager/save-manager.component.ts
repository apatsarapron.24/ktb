import { Component, OnInit, Inject } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MAT_DIALOG_DATA, MatDialogConfig } from '@angular/material';
import { AutoCompleteComponent } from './auto-complete/auto-complete.component';
import { AddUserComponent } from './add-user/add-user.component';
import { SaveManagerService } from '../../services/save-manager/save-manager.service';
import { EditUserComponent } from './edit-user/edit-user.component';
import Swal from 'sweetalert2';
import { saveAs } from 'file-saver';
import { RequestService } from './../../services/request/request.service';

@Component({
  selector: 'app-save-manager',
  templateUrl: './save-manager.component.html',
  styleUrls: ['./save-manager.component.scss'],
})
export class SaveManagerComponent implements OnInit {
  displayedColumns = [
    'userId',
    'empePosition',
    'dept',
    'grp',
    'stcr',
    'action',
  ];
  dataSource = new MatTableDataSource();

  displayedColumns_co = [
    'userId',
    'empePosition',
    'dept',
    'grp',
    'stcr',
    'action',
  ];
  dataSource_co = new MatTableDataSource();

  displayedColumns_department = [
    'userId',
    'empePosition',
    'dept',
    'grp',
    'stcr',
    'action',
  ];
  dataSource_department = new MatTableDataSource();

  displayedColumns_group = [
    'userId',
    'empePosition',
    'dept',
    'grp',
    'stcr',
    'action',
  ];
  dataSource_group = new MatTableDataSource();

  displayedColumns_division = [
    'userId',
    'empePosition',
    'dept',
    'grp',
    'stcr',
    'action',
  ];
  dataSource_division = new MatTableDataSource();

  get_data: any = '';

  creater_type: any = '';
  creater_type_edit: any = '';
  creater_id_officer: any = '';
  creater_name: any = '';

  send_edit_1: any = {
    oldType: '',
    newType: '',
    oldId: '',
    isEmpower: false,
    attachments: {
      file: [],
      fileDelete: []
    }
  };
  delete: any = {
    type: '',
    id: '',
  };
  data_type: any = [
    {
      name: 'ผู้จัดทำใบคำขอหลัก (Creater)',
      type: 'creators',
    },
    {
      name: 'ผู้ช่วยจัดทำใบคำขอ (Co-Creater)',
      type: 'co_creators',
    },
    {
      name: 'ผู้บริหารฝ่าย',
      type: 'departments',
    },
    {
      name: 'ผู้บริหารกลุ่ม',
      type: 'groups',
    },
    {
      name: 'ผู้บริหารสาย',
      type: 'divisions',
    },
  ];

  list_table: any = [];
  delete_officer_line: any;
  requestId: any;
  get_res: any;

  element: HTMLElement;

  attachments: any = {
    file: [],
    fileDelete: []
  };
  lengthFile = 0;
  FileOverSize: any;
  file: any;
  urls: any = [];
  displayedColumns_file: string[] = ['name', 'size', 'delete'];
  dataSource_file: MatTableDataSource<FileList>;
  isEmpowerStatus = false;

  roleLevel: any;
  allowSave = false;
  status_loading = false;
  get_role: any = '';
  get_level: any = '';
  role_list_length = 0;

  downloadfile_by = [];


  constructor(
    private dialog: MatDialog,
    private SaveManage: SaveManagerService,
    private RequestService: RequestService,
    @Inject(MatDialog) public edit_data: any
  ) { }

  ngOnInit() {
    this.status_loading = true;
    this.get_role = localStorage.getItem('role');
    this.get_level = localStorage.getItem('level');
    localStorage.setItem('add_officer', JSON.stringify(''));
    if (localStorage) {
      this.requestId = localStorage.getItem('requestId');
      this.SaveManage.GetList_creators(this.requestId).subscribe((res) => {
        this.get_res = res;
        this.role_list_length = this.get_res.data.complianceRoleList.length;
        this.edit_data = '';
        // console.log('res:', this.get_res);
        if (res['status'] === 'success') {
          this.status_loading = false;
        } else {
          this.status_loading = false;
        }

        this.dataSource = new MatTableDataSource(this.get_res.data.creators);
        this.dataSource_co = new MatTableDataSource(
          this.get_res.data.co_creators
        );
        this.dataSource_department = new MatTableDataSource(
          this.get_res.data.departments
        );
        this.dataSource_group = new MatTableDataSource(
          this.get_res.data.groups
        );
        this.dataSource_division = new MatTableDataSource(
          this.get_res.data.divisions
        );
      });
    }
  }

  openSimpleDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
      data: this.edit_data,
      Alltable: {
        creators: this.dataSource.data,
        co_creators: this.dataSource_co.data,
        departments: this.dataSource_department.data,
        groups: this.dataSource_group.data,
        divisions: this.dataSource_division.data,
      },
    };
    const dialogRef = this.dialog.open(AutoCompleteComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((data) => {
      // console.log('data::', data);
      // this.edit_data = data;
      if (data !== '') {
        this.dialog.open(AddUserComponent, {
          disableClose: true,
          data: data,
        });
      }
    });

    this.dialog.afterAllClosed.subscribe((res) => {
      // // console.log('_response:', response);
      // // console.log('closed:', this.edit_data);
      // tslint:disable-next-line: no-shadowed-variable
      this.reSet_data();
    });
  }
  reSet_data() {
    this.SaveManage.GetList_creators(this.requestId).subscribe((res) => {
      this.get_res = res;
      this.role_list_length = this.get_res.data.complianceRoleList.length;
      if (res['status'] === 'success') {
        this.status_loading = false;
      } else {
        this.status_loading = false;
      }
      this.dataSource = new MatTableDataSource(this.get_res.data.creators);
      this.dataSource_co = new MatTableDataSource(
        this.get_res.data.co_creators
      );
      this.dataSource_department = new MatTableDataSource(
        this.get_res.data.departments
      );
      this.dataSource_group = new MatTableDataSource(this.get_res.data.groups);
      this.dataSource_division = new MatTableDataSource(
        this.get_res.data.divisions
      );
    });
  }

  edit_creater(data: any) {
    console.log('list:>>>', data);
    this.roleLevel = data['roleLevel'];

    // // console.log(data.isEmpower);

    console.log(this.creater_type_edit);
    if (this.creater_type_edit !== '') {
      this.creater_type = this.creater_type_edit;
      this.creater_type = this.delete_officer_line.type;
      this.creater_id_officer = this.delete_officer_line.userId;
      this.creater_name =
        this.delete_officer_line.titleNameTh +
        '  ' +
        this.delete_officer_line.empeNameTh +
        '  ' +
        this.delete_officer_line.empeLastNameTh;
    } else {
      this.delete_officer_line = data;
      this.creater_type = data.type;
      this.creater_type_edit = data.type;
      console.log('delete:', this.delete_officer_line);
      this.creater_id_officer = data.userId;
      this.creater_name =
        data.titleNameTh + '  ' + data.empeNameTh + '  ' + data.empeLastNameTh;
      if (data.isEmpower === true) {
        this.lengthFile = data['attachments'].file.length;
        this.isEmpowerStatus = true;
        this.dataSource_file = data['attachments'].file;
        this.attachments.file = data['attachments'].file;
        this.hideDownloadFile();
        // // console.log('conditionget', this.attachments)
      } else {
        this.isEmpowerStatus = false;
        data['attachments'] = {
          file: [],
          fileDelete: []
        };
      }
      console.log(this.delete_officer_line);
    }
  }

  save_edit1() {
    // this.creater_type_edit = '';
    this.send_edit_1 = {
      oldType: this.delete_officer_line.type,
      newType: this.creater_type,
      oldId: this.delete_officer_line.id,
      isEmpower: this.isEmpowerStatus,
      attachments: this.attachments
    };
    // console.log('edit_data:', this.send_edit_1);

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
      data: this.send_edit_1,
      table: this.dataSource.data,
    };

    const dialogRef = this.dialog.open(EditUserComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((data) => {
      // // console.log('delete_officer_line:', this.delete_officer_line);
      if (data === '') {
        // console.log('closed');
        this.creater_type_edit = '';
        this.allowSave = false;
        this.reSet_data();
      } else {
        document.getElementById('edit_11').click();
        this.creater_type = this.creater_type_edit;
        // this.creater_type_edit = '';
        // console.log('edited');
        this.reSet_data();
      }
    });
    this.reSet_data();
  }

  openAddFile(type, level) {
    if (type === 'groups' && level === 'ฝ่าย') {
      // console.log('ฝ่ายแทนกลุ่ม');
      this.isEmpowerStatus = true;
      this.attachments.file = [];
      this.dataSource_file = this.attachments.file;
      this.hideDownloadFile();
      this.allowSave = false;
    } else if (type === 'divisions' && level === 'ฝ่าย') {
      // console.log('ฝ่ายแทนสาย');
      this.attachments.file = [];
      this.isEmpowerStatus = true;
      this.dataSource_file = this.attachments.file;
      this.hideDownloadFile();
      this.allowSave = false;
    } else if (type === 'divisions' && level === 'กลุ่ม') {
      // console.log('ฝ่ายแทนสาย');
      this.attachments.file = [];
      this.dataSource_file = this.attachments.file;
      this.hideDownloadFile();
      this.isEmpowerStatus = true;
      this.allowSave = false;
    } else {
      this.attachments.file = [];
      this.isEmpowerStatus = false;
      this.allowSave = true;
    }
  }

  close_modal() {
    this.creater_type_edit = '';
    this.isEmpowerStatus = false;
    this.allowSave = false;
  }

  addOfficer(item: any) {
    // console.log('type:', item);
    this.creater_type_edit = item;
    this.openAddFile(item, this.roleLevel)
  }

  delete_officer() {
    // this.status_loading = true;

    this.delete = {
      requestId: localStorage.getItem('requestId'),
      type: this.delete_officer_line.type,
      id: this.delete_officer_line.id,
    };
    console.log('delete:', this.delete);
    // // console.log('delete:', this.delete);
    // // console.log('delete type:', this.delete.type);

    // if (this.delete.type === 'creators') {
    //   if (this.dataSource.data.length === 1) {
    //     // alert('ไม่สามารถลบผู้ดำเนินการได้');
    //     Swal.fire({
    //       title: 'error',
    //       text: 'ไม่สามารถลบผู้ดำเนินการได้',
    //       icon: 'error',
    //       showConfirmButton: false,
    //       timer: 1500
    //     });
    //   }
    //    else {
    //     this.SaveManage.Delete_employee(this.delete).subscribe((res) => {
    //       if (res['status'] === 'success') {
    //         // console.log('deleted_1');
    //         Swal.fire({
    //           title: res['status'],
    //           text: 'ลบผู้บันทึกดำเนินการเรียบร้อย',
    //           icon: 'success',
    //           showConfirmButton: false,
    //           timer: 1500
    //         });
    //         this.reSet_data();
    //       }
    //     });
    //   }
    // } else {

    console.log('deletedeletedelete:', this.delete);
    this.SaveManage.Delete_employee(this.delete).subscribe((res) => {
      // console.log('res:', res);
      if (res['status'] === 'success') {
        this.status_loading = false;
        this.delete = {
          requestId: null,
          type: null,
          id: null,
        };
        this.creater_type_edit = '';
        // console.log('deleted_2');
        Swal.fire({
          title: res['status'],
          text: 'ลบผู้บันทึกดำเนินการเรียบร้อย',
          icon: 'success',
          showConfirmButton: false,
          timer: 1500
        });
        this.reSet_data();
      } else {
        this.status_loading = false;
        Swal.fire({
          title: 'error',
          text: res['message'],
          icon: 'error',
          showConfirmButton: false,
          timer: 1500
        });
      }
    });
    // }
  }


  onSelectFile(event) {
    this.FileOverSize = [];
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      const file = event.target.files;
      for (let i = 0; i < filesAmount; i++) {
        // console.log('type:', file[i]);
        if (file[i].size <= 5000000) {
          this.multiple_file(file[i], i);
        } else {
          this.FileOverSize.push(file[i].name);
        }
      }
      if (this.FileOverSize.length !== 0) {
        // console.log('open modal');
        document.getElementById('testttt').click();
      }
    }
  }

  multiple_file(file, index) {
    // console.log('download');
    const reader = new FileReader();
    if (file) {
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');
        // this.urls.push({
        const x = {
          fileName: file.name,
          fileSize: file.size,
          base64File: splitFile[1],
        };
        // });
        this.attachments.file.push(x);
        // // console.log('this.urls', this.attachments.file);
        this.dataSource_file = new MatTableDataSource(this.attachments.file);
        this.hideDownloadFile();
        // console.log(this.attachments.file);
        // console.log(this.isEmpowerStatus, '>>', this.attachments.file.length, '>>', this.creater_type_edit)
        if (this.isEmpowerStatus === true && this.attachments.file.length !== 0 && this.creater_type_edit !== '') {
          this.allowSave = true;
        }

        if (this.lengthFile < this.attachments.file.length || this.lengthFile > this.attachments.file.length) {
          this.allowSave = true;
        }
      };
    }
  }

  delete_mutifile(data: any, index: any) {
    // console.log('>>>>>', this.attachments);
    // console.log('data:', data, 'index::', index);
    // console.log(data.id)
    if ('id' in this.attachments.file[index]) {
      const id = this.attachments.file[index].id;
      // console.log('found id');
      if ('fileDelete' in this.attachments) {
        this.attachments.fileDelete.push(id);
        // // console.log()

      }
    }
    // console.log('fileDelete attachments : ', this.attachments);
    this.attachments.file.splice(index, 1);
    if (this.lengthFile < this.attachments.file.length || this.lengthFile > this.attachments.file.length) {
      // console.log('this.attachments.file.length', this.attachments.file.length)
      if (this.attachments.file.length !== 0) {
        this.allowSave = true;
      }
    }
    this.dataSource_file = new MatTableDataSource(this.attachments.file);
    // console.log(this.attachments);
  }

  downloadFile(pathdata: any) {
    const contentType = '';
    const sendpath = pathdata;
    // console.log('path', sendpath);
    this.RequestService.getfile(sendpath).subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        const datagetfile = res['data'];
        const b64Data = datagetfile['data'];
        const filename = datagetfile['fileName'];
        // console.log('base64', b64Data);
        const config = this.b64toBlob(b64Data, contentType);
        saveAs(config, filename);
      }

    });
  }

  b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const convertbyte = atob(b64Data);
    const byteCharacters = atob(convertbyte);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  downloadFile64(data: any) {
    const contentType = '';
    const b64Data = data.base64File;
    const filename = data.fileName;
    console.log('base64', b64Data);

    const config = this.base64(b64Data, contentType);
    saveAs(config, filename);
  }

  base64(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  hideDownloadFile() {
    if (this.attachments.file.length > 0) {
      this.downloadfile_by = [];

      for (let index = 0; index < this.attachments.file.length; index++) {
        const element = this.attachments.file[index];
        console.log('ele', element);

        if ('path' in element) {
          this.downloadfile_by.push('path');
        } else {
          this.downloadfile_by.push('base64');
        }
      }
    }
    console.log('by:', this.downloadfile_by);
  }

}
