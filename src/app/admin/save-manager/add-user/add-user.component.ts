import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA, MatDialogConfig } from '@angular/material';
import { SaveManagerService } from '../../../services/save-manager/save-manager.service';
import { AutoCompleteComponent } from '../auto-complete/auto-complete.component';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss'],
})
export class AddUserComponent implements OnInit {
  constructor(
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<AddUserComponent>,
    private SaveManage: SaveManagerService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  status_loading = false;

  ngOnInit() {
    console.log('data:', this.data);
  }

  close() {
    this.data = '';
    this.dialogRef.close(this.data);
  }
  back_edit() {
    this.dialogRef.close();
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = this.data;

    const dialogRef = this.dialog.open(AutoCompleteComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((data) => {
      console.log('data::', data);
      // this.edit_data = data;

      if (data !== '') {
        this.dialog.open(AddUserComponent, {
          disableClose: true,
          data: data,
        });
      }

    });
  }

  add_user() {
    // check สถานะ status frontend
    const ckeck_usrId = {
      creators: true,
      co_creators: true,
      departments: true,
      groups: true,
      divisions: true
    };
    console.log('ADD::', this.data);
    // for (let index = 0; index < this.data.Alltable.creators.length; index++) {
    //   if ( this.data.data.userId === this.data.Alltable.creators[index].userId) {
    //     ckeck_usrId.creators = false;
    //     break;
    //   }
    // }

    // for (let index = 0; index < this.data.Alltable.co_creators.length; index++) {
    //   if ( this.data.data.userId === this.data.Alltable.co_creators[index].userId) {
    //     ckeck_usrId.co_creators = false;
    //     break;
    //   }
    // }
    // for (let index = 0; index < this.data.Alltable.departments.length; index++) {
    //   if ( this.data.data.userId === this.data.Alltable.departments[index].userId) {
    //     ckeck_usrId.departments = false;
    //     break;
    //   }
    // }
    // for (let index = 0; index < this.data.Alltable.groups.length; index++) {
    //   if ( this.data.data.userId === this.data.Alltable.groups[index].userId) {
    //     ckeck_usrId.groups = false;
    //     break;
    //   }
    // }
    // for (let index = 0; index < this.data.Alltable.divisions.length; index++) {
    //   if ( this.data.data.userId === this.data.Alltable.divisions[index].userId) {
    //     ckeck_usrId.divisions = false;
    //     break;
    //   }
    // }

    // if (ckeck_usrId.creators === false ||
    //   ckeck_usrId.co_creators === false ||
    //   ckeck_usrId.departments === false ||
    //   ckeck_usrId.groups === false ||
    //   ckeck_usrId.divisions === false ) {
    //     console.log('false', ckeck_usrId);
    //     Swal.fire({
    //       title: 'เกิดข้อผิดพลาด',
    //       text: 'ไม่สามารถเพิ่มผู้ดำเนินการได้ รหัสพนักงานมีในระบบแล้ว',
    //       icon: 'error'
    //     });
    //    } else {
    //     console.log('true', ckeck_usrId);
    this.status_loading = true;
    if (localStorage.getItem('level') !== 'ฝ่าย' &&
      localStorage.getItem('level') !== 'กลุ่ม' &&
      localStorage.getItem('level') !== 'สาย') {
      this.SaveManage.Create_employee(this.data.data).subscribe(res => {
        console.log('res', res);
        if (res['status'] === 'success') {
          this.status_loading = false;
          this.data.data = '';
          this.dialogRef.close(this.data);
          // tslint:disable-next-line: no-use-before-declare
          const success = this.dialog.open(AddAlertComponent);
          setTimeout(() => {
            success.close();
          }, 1000);
        } else {
          this.status_loading = false;
          Swal.fire({
            title: 'เกิดข้อผิดพลาด',
            text: res['message'],
            icon: 'error'
          });
        }
      });
    } else {
      this.status_loading = false;
      this.data.data = '';
      this.dialogRef.close(this.data);
      Swal.fire({
        title: 'เกิดข้อผิดพลาด',
        text: 'คุณไม่มีสิทธิ์แก้ไขหรือเข้าถึงข้อมูล',
        icon: 'error',
        timer:3000
      });
    }

  }
}



@Component({
  selector: 'app-add-alert',
  templateUrl: './add-alert.component.html',
  styleUrls: ['./add-alert.component.scss'],
})

export class AddAlertComponent {

  constructor(
    public dialogRef: MatDialogRef<AddAlertComponent>) { }

}
