import { Component, OnInit, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { FormControl } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { SaveManagerService } from '../../../services/save-manager/save-manager.service';
import { AddUserComponent } from '../add-user/add-user.component';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { saveAs } from 'file-saver';
import { RequestService } from '../../../services/request/request.service';
import { DataRowOutlet } from '@angular/cdk/table';

export interface User {
  empeNo: string;
  ttlNmTh: string;
  empeNmTh: string;
  empeLstNmTh: string;
  role: string;
  roleLevel: string;
}

export interface FileList {
  fileName: string;
  fileSize: string;
  base64File: string;
}

const User: User[] = [];


@Component({
  selector: 'app-auto-complete',
  templateUrl: './auto-complete.component.html',
  styleUrls: ['./auto-complete.component.scss'],
})

export class AutoCompleteComponent implements OnInit {
  selectedtype: any = '';
  selectOfficer: any = '';
  status_loading = false;

  myControl = new FormControl();
  options: User[] = [];
  filteredOptions: Observable<User[]>;

  id_officer: any = '';
  name_officer: any = '';
  get_id: any = '';
  role: any = '';
  role_level: any = '';

  add_data: any = {
    requestId: '',
    type: '',
    userId: '',
    isEmpower: false,
    attachments: {
      file: [],
      fileDelete: []
    }
  };

  requestId: any;
  data: any;
  data_res: any;
  search: any;
  loadingSpin = false;
  selectSearch: any = '';
  edit: any;
  txtQueryChanged: Subject<string> = new Subject<string>();
  text_alert: any = '';

  attachments: any = {
    file: [],
    fileDelete: []
  };
  FileOverSize: any;
  file: any;
  urls: any = [];
  displayedColumns_file: string[] = ['name', 'size', 'delete'];
  dataSource_file: MatTableDataSource<FileList>;
  isEmpowerStatus = false;

  allowSave = false;
  downloadfile_by = [];

  constructor(
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<AutoCompleteComponent>,
    private SaveManage: SaveManagerService,
    private sidebarService: RequestService,
    @Inject(MAT_DIALOG_DATA) public edit_data: any
  ) {
    this.dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.loadingSpin = false;
    this.get_data();
    this.filter();

    this.requestId = localStorage.getItem('requestId');
    this.SaveManage.GetList_creators(this.requestId).subscribe((res) => {
      this.data = res;
      // console.log('res', this.data.data);
      // console.log('edit_data', this.edit_data);
      // this.edit_data = '';
      // // console.log('edit_data', this.edit_data);

      if (this.edit_data.data !== '') {
        this.selectedtype = this.edit_data.data.type;
        this.id_officer = this.edit_data.data.userId;
        // tslint:disable-next-line: no-shadowed-variable
        this.SaveManage.Search_employee(this.id_officer).subscribe((_res) => {
          this.edit = _res;
          // // console.log('selectedtype:', this.selectedtype);
          // console.log('res__:', this.edit.data);
          this.name_officer =
            this.edit.data[0].ttlNmTh +
            '  ' +
            this.edit.data[0].empeNmTh +
            '  ' +
            this.edit.data[0].empeLstNmTh;
        });
      }
    });

    this.txtQueryChanged
      .pipe(
        // set time to debounce he
        debounceTime(1000),
        // distinctUntilChanged()
      )
      .subscribe((model) => {
        // console.log('model : ', model);
        this.selectSearch = model;
        // Call api here
        // this.getRoleFromEmployeeId(this.employeeIdSelected);
        this.search_creators(this.selectSearch);
      });

  }

  get_data() {
    this.options = User;
  }
  filter() {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      // map((value) => (typeof value === 'string' ? value : value.empeNo)),
      map((empeNo) => (empeNo ? this._filter(empeNo) : this.options))
    );
  }
  displayFn(user?: User): string | undefined {
    return user ? user.empeNo : undefined;
  }

  private _filter(id_officer: string): User[] {
    const filterValue = id_officer.toLowerCase();
    // this.employeeSearch = filterValue;

    return this.options.filter(
      (option) => option.empeNo.toLowerCase().indexOf(filterValue) === 0
    );
  }
  search_text(e: any) {
    console.log('e', e);
  }

  search_creators(employeeSearch: any) {
    this.SaveManage.Search_employee(employeeSearch).subscribe((res) => {
      this.search = res;
      console.log('search:', this.search.data);
      this.loadingSpin = false;
      this.options = this.search.data;
      if (this.options == null) {
        this.options = [];
      } else {
        this.text_alert = '';
      }
      if (this.options.length === 0) {
        this.text_alert = 'ไม่พบข้อมูลพนักงาน';
      }
      this.filter();
    });
  }

  addOfficer() {
    console.log('selectOfficer', this.selectedtype);
    this.openAddFile(this.selectedtype, this.options[0]['roleLevel']);
  }
  close() {
    this.edit_data.data = '';
    this.dialogRef.close(this.edit_data.data);
  }
  option_change(e: any, data: any) {
    console.log('????', data)
    if (e.source.selected === true) {
      this.id_officer = data.empeNo;
      console.log('>>>>??', this.options)
      this.openAddFile(this.selectedtype, data['roleLevel']);
      // this.add_data.id_officer = this.get_id;
      this.role = data['role'];
      this.role_level = data['roleLevel'];
      this.name_officer = data['ttlNmTh'] + '  ' + data['empeNmTh'] + '  ' + data['empeLstNmTh'];
      //       '  ' +
      // for (let index = 0; index < this.options.length; index++) {
      //   if (this.id_officer === this.options[index].empeNo) {
      //     this.name_officer =
      //       this.options[index].ttlNmTh +
      //       '  ' +
      //       this.options[index].empeNmTh +
      //       '  ' +
      //       this.options[index].empeLstNmTh;
      //     this.role = data['role'];
      //     this.role_level = this.options[index].roleLevel;
      //   }
      // }
    }

    if (this.role !== '' && this.role_level !== '' && this.selectedtype !== '') {
      if (this.isEmpowerStatus !== true) {
        this.allowSave = true;
      }
    } else {
      this.allowSave = false;
    }
  }

  openAddFile(type, level) {
    if (type === 'groups' && level === 'ฝ่าย') {
      // console.log('ฝ่ายแทนกลุ่ม');
      this.allowSave = false;
      this.isEmpowerStatus = true;
      this.attachments.file = [];
      this.dataSource_file = new MatTableDataSource(this.attachments.file);
      this.hideDownloadFile();
    } else if (type === 'divisions' && level === 'ฝ่าย') {
      // console.log('ฝ่ายแทนสาย');
      this.isEmpowerStatus = true;
      this.allowSave = false;
      this.attachments.file = [];
      this.dataSource_file = new MatTableDataSource(this.attachments.file);
      this.hideDownloadFile();
    } else if (type === 'divisions' && level === 'กลุ่ม') {
      // console.log('กลุ่มแทนสาย');
      this.isEmpowerStatus = true;
      this.allowSave = false;
      this.attachments.file = [];
      this.dataSource_file = new MatTableDataSource(this.attachments.file);
      this.hideDownloadFile();
    } else {
      this.isEmpowerStatus = false;
      this.allowSave = true;
    }
  }


  open_add_modal() {

    this.add_data = {
      requestId: Number(this.requestId),
      type: this.selectedtype,
      userId: this.id_officer,
      isEmpower: this.isEmpowerStatus,
      attachments: this.attachments,
      role: this.role,
      roleLevel: this.role_level
    };
    console.log(this.add_data)

    this.edit_data.data = this.add_data;
    this.dialogRef.close(this.edit_data);
  }
  input_search(e: any) {
    // console.log('e:', e);
    this.txtQueryChanged.next(e);
    this.loadingSpin = true;

  }


  onSelectFile(event) {
    this.FileOverSize = [];
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      const file = event.target.files;
      for (let i = 0; i < filesAmount; i++) {
        // console.log('type:', file[i]);
        if (file[i].size <= 5000000) {
          this.multiple_file(file[i], i);
        } else {
          this.FileOverSize.push(file[i].name);
        }
      }
      if (this.FileOverSize.length !== 0) {
        // console.log('open modal');
        document.getElementById('testttt').click();
      }
    }
  }

  multiple_file(file, index) {
    // console.log('download');
    const reader = new FileReader();
    if (file) {
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        const rawReadFile = reader.result as string;
        const splitFile = rawReadFile.split(',');
        // this.urls.push({
        const x = {
          fileName: file.name,
          fileSize: file.size,
          base64File: splitFile[1],
        };
        this.attachments.file.push(x);
        // // console.log('this.urls', this.attachments.file);
        this.dataSource_file = new MatTableDataSource(this.attachments.file);
        this.hideDownloadFile();
        // console.log(this.attachments.file);
        if (this.isEmpowerStatus === true && this.id_officer !== ''
          && this.selectedtype !== '' && this.attachments.file.length !== 0) {
          this.allowSave = true;
        }
      };

    }
  }

  delete_mutifile(data: any, index: any) {
    // console.log('data:', data, 'index::', index);
    if ('id' in this.attachments.file[index]) {
      const id = this.attachments.file[index].id;
      // console.log('found id');
      if ('fileDelete' in this.attachments) {
        this.attachments.fileDelete.push(id);
      }
    }
    // console.log('fileDelete attachments : ', this.attachments.fileDelete);
    this.attachments.file.splice(index, 1);

    if (this.attachments.file.length === 0) {
      this.allowSave = false;
    }

    this.dataSource_file = new MatTableDataSource(this.attachments.file);
    // console.log(this.attachments);
  }

  downloadFile(pathdata: any) {
    const contentType = '';
    const sendpath = pathdata;
    // console.log('path', sendpath);
    this.sidebarService.getfile(sendpath).subscribe(res => {
      if (res['status'] === 'success' && res['data']) {
        const datagetfile = res['data'];
        const b64Data = datagetfile['data'];
        const filename = datagetfile['fileName'];
        // console.log('base64', b64Data);
        const config = this.b64toBlob(b64Data, contentType);
        saveAs(config, filename);
      }
    });
  }


  b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const convertbyte = atob(b64Data);
    const byteCharacters = atob(convertbyte);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  downloadFile64(data: any) {
    const contentType = '';
    const b64Data = data.base64File;
    const filename = data.fileName;
    console.log('base64', b64Data);

    const config = this.base64(b64Data, contentType);
    saveAs(config, filename);
  }

  base64(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  hideDownloadFile() {
    if (this.dataSource_file.data.length > 0) {
      this.downloadfile_by = [];

      for (let index = 0; index < this.dataSource_file.data.length; index++) {
        const element = this.dataSource_file.data[index];
        console.log('ele', element);

        if ('path' in element) {
          this.downloadfile_by.push('path');
        } else {
          this.downloadfile_by.push('base64');
        }
      }
    }
    console.log('by:', this.downloadfile_by);
  }

}
