import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SaveManagerComponent } from '../save-manager/save-manager.component';

const routes: Routes = [
  {
    path: '',
    component: SaveManagerComponent,
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SaveManagerRoutingModule { }


