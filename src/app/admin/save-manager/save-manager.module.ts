import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaveManagerComponent } from '../save-manager/save-manager.component';
import { SaveManagerRoutingModule } from './save-manager-routing.module';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { AutoCompleteComponent } from './auto-complete/auto-complete.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDialogModule } from '@angular/material/dialog';
import {
  AddUserComponent,
  AddAlertComponent,
} from './add-user/add-user.component';
import { EditUserComponent, EditAlertComponent } from './edit-user/edit-user.component';
@NgModule({
  declarations: [
    SaveManagerComponent,
    AutoCompleteComponent,
    AddUserComponent,
    AddAlertComponent,
    EditUserComponent,
    EditAlertComponent
  ],
  imports: [
    CommonModule,
    SaveManagerRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatAutocompleteModule,
    MatInputModule,
    MatSelectModule,
    MatDialogModule,
    MatProgressSpinnerModule
  ],
  entryComponents: [
    AutoCompleteComponent,
    AddUserComponent,
    AddAlertComponent,
    EditAlertComponent,
    EditUserComponent,
  ],
})
export class SaveManagerModule {}
