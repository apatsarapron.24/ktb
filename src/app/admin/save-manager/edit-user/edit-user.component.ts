import { Component, OnInit, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { SaveManagerService } from '../../../services/save-manager/save-manager.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss'],
})
export class EditUserComponent implements OnInit {
  edit_saved: any;
  status_loading = false;

  constructor(
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<EditUserComponent>,
    private SaveManage: SaveManagerService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    // // console.log('data:', this.data);
    this.edit_saved = this.data.data;
  }

  close() {
    this.data = '';
    this.dialogRef.close(this.data);
  }

  save_edit() {
    this.status_loading = true;
    // console.log('dataaa::', this.data);
    // console.log('saved', this.edit_saved);

    if (localStorage.getItem('level') !== 'ฝ่าย' &&
      localStorage.getItem('level') !== 'กลุ่ม' &&
      localStorage.getItem('level') !== 'สาย') {
      this.SaveManage.Update_employee(this.edit_saved).subscribe((res) => {
        // console.log('updated:', res);

        if (res['status'] === 'success') {
          this.status_loading = false;
          // tslint:disable-next-line: no-use-before-declare
          const success = this.dialog.open(EditAlertComponent);
          setTimeout(() => {
            success.close();
          }, 1000);
          this.data.data = '';
          this.dialogRef.close(this.data.data);
          // this.reSet_data();
        } else {
          this.status_loading = false;
          Swal.fire({
            title: 'เกิดข้อผิดพลาด',
            text: res['message'],
            icon: 'warning'
          });
        }
      });
    }
    else {
      this.status_loading = false;
      this.data.data = '';
      this.dialogRef.close(this.data.data);
      Swal.fire({
        title: 'เกิดข้อผิดพลาด',
        text: 'คุณไม่มีสิทธิ์แก้ไขหรือเข้าถึงข้อมูล',
        icon: 'error',
        timer: 3000
      });
    }

  }
  back_edit() {
    this.dialogRef.close(this.data.data);
  }
}

@Component({
  selector: 'app-edit-alert',
  templateUrl: './edit-alert.component.html',
  styleUrls: ['./edit-alert.component.scss'],
})
export class EditAlertComponent {
  constructor(public dialogRef: MatDialogRef<EditAlertComponent>) { }
}
