import { Component, OnInit,  ViewChild} from '@angular/core';
import { delay } from 'q';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RouterLink, Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  displayedColumns: string[] = ['user', 'company', 'role', 'create_date', 'edit'];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private http: HttpClient, private router: Router) { }
  api_url = environment.apiUrl;
  public confirm;
  public infoModal;
  message = '';


  // show data
  data_user;
  customer_data;

  // add user
  role_status = false;
  customer_name = '';
  customer_id = '';

  // remove user
  // modalRemove = false;
  user_id = '';
  get_username = '';

  // alert
  check_success = false;
  check_error = false;

 



  // ------------------ add user -------------------------

  // check_password = false;
  style_password = '10px solid red';
  style_confrim = '10px solid red';
  style_username = ''
  check_user = /^[a-z][a-z0-9\\d._]{1,48}[a-z0-9\\d]$/;
  check_password = /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[@$!%*#?&])[a-zA-Z0-9@$!%*#?&]+$/;
  pet_pass_eng = /^[a-zA-Z0-9@$!%*#?&]+$/;
  pet_pass_special = /^(?=.*[@$!%*#?&])[a-zA-Z0-9@$!%*#?&]+$/;
  pet_pass_number = /^(?=.*[0-9])[a-zA-Z0-9@$!%*#?&]+$/;
  pat_pass_lower = /^(?=.*[a-z])[a-zA-Z0-9@$!%*#?&]+$/;
  pat_pass_upper = /^(?=.*[A-Z])[a-zA-Z0-9@$!%*#?&]+$/;
  

  username = undefined;
  password = undefined;
  confirm_pass = undefined;
  role = undefined;
  customer = undefined;
  error_msg = '';

  // page control
  table_users = true;
  add_user = false;
  page_2 = false;

  service_list = [];

  ngOnInit() {
    this.http.get(this.api_url + '/users').subscribe(data => {
      this.data_user = data['result'];
      this.dataSource = new MatTableDataSource(data['result']);
      this.dataSource.paginator = this.paginator;
      
    });
    
    this.http.get(this.api_url + '/admin/customers').subscribe(data => { this.customer_data = data['result']; });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  // ================================setTimeout alert===========================
  countTime() {
    if (this.check_success || this.check_error) {
            setTimeout(() => {
              this.check_success = false;
              this.check_error = false;
                }, 3000);
        }
  }


  checkUser(username): void {
    // console.log('>>>>>>',username)
    if (username === true) {
      this.style_username = '10px solid #42A948';
    } else {
      this.style_username = '10px solid red';
    }
  }

  checkPass(password): void {
    // console.log('>>>>>>',password)
    if (password === true) {
      this.style_password = '10px solid #42A948';
    } else {
      this.style_password = '10px solid red';
    }
  }

  checkConfirmPass(confirm, password): void {
    if (confirm === password) {
      this.style_confrim = '10px solid #42A948';
    } else {
      this.style_confrim = '10px solid red';
    }
  }

  clearValues(): void {
    this.service_list = [];
    this.username = undefined;
    this.password = undefined;
    this.confirm_pass = undefined;
    this.role = undefined;
    this.customer = undefined;
    this.style_password = '10px solid red';
    this.style_confrim = '10px solid red';
    this.style_username = '10px solid red';
  }

  async comfirmCustomer(infoModal) {
    await delay(2000);
    infoModal.hide();
    this.add_user = false;
    this.table_users = true;
    this.clearValues();
  }

  cancleCreate(): void {
    this.table_users = true;
    this.add_user = false;
    this.clearValues();
  }

  onChange_role(event) {
    this.role = event;
  }

  onChange_customer(event) {
    this.customer_name = event;
    for (const k in this.customer_data) {
      if (this.customer_name === this.customer_data[k]['company']) {
        this.customer_id = this.customer_data[k]['customer_id'];
        // console.log('>>>>',this.customer_id)
      }
    }
  }

  // ------------------------------- add user & map login-------------------
  map_login() {
    const json_map = {
      username: this.username,
      customer_id: this.customer_id,
      role: this.role
    };
    //console.log(json_map);
    this.http.post(this.api_url + '/map_login', json_map).subscribe(
      data => {
          this.message = 'Insert success';
          this.ngOnInit(); 
        
      },
      error => {
        this.error_msg = error.error.message;
        //console.log(error);
      }
    );
  }


  create_user(username, password, confirm_password) {
    this.username = username;
    const json_user = {
      username: username,
      password: password,
      confirm_password: confirm_password,
      role: this.role
    };
    this.http.post(this.api_url + '/users', json_user).subscribe(
      data => {
          // console.log(data['message'])
          this.message = data['message']
          this.check_success = true
          this.countTime();
          this.map_login();
          this.cancleCreate();
          this.ngOnInit()
          // this.http.get(this.api_url + '/users').subscribe(data => { this.data_user = data['result']; });
      },
      error => {
        this.error_msg = error.error.message;
        this.check_error = true;
        this.countTime();
      }
    );
  }

  // =================== Delete User ==================
  getIdUser(id, user) {
    this.user_id = id;
    this.get_username = user;
    // console.log(this.user_id)
  }

  deleteUser() {
    this.http.delete(this.api_url + '/users/' + this.user_id).subscribe(
      data => {
        // console.log(data['message'])
        this.message = data['message']
        this.check_success = true
        this.countTime()
        this.ngOnInit()
        // this.http.get(this.api_url + '/users').subscribe(data => { this.data_user = data['result']; });
      },
      error => {
        this.error_msg = error.error.message;
      });
  }

}
