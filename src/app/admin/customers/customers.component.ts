import { Component, OnInit, ViewChild } from '@angular/core';
import { delay } from 'q';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RouterLink, Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import bsCustomFileInput from 'bs-custom-file-input';
import { Key } from 'protractor';
import {ResoiutiondataService} from '../../resoiutiondata.service';
// import { triggerAsyncId } from 'async_hooks';
import { setMaxListeners } from 'cluster';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';




@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {
  fileName: any;
  
  displayedColumns: string[] = ['Customer_Name', 'Project_Manager', 'Sale', 'Pre_Sale', 'create_date', 'edit'];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  
  constructor(private http: HttpClient, private router: Router, private resoiution:ResoiutiondataService) { }
  api_url = environment.apiUrl;
  public confirm;
  public infoModal;
  public edit_cus;
  public test;

  data_service = [];

  // page control
  table_customer = true;
  page_1 = false;
  page_2 = false;
  page_3 = false;

  // value in page 1
  name = undefined;
  company = '';
  street = undefined;
  city = '';
  postal = '';
  country = undefined;
  tel = undefined;

  customer_center = [];
  address = '';
  postal_code = '';
  project_manager = '';
  sale_name = '';
  presale_name = '';
  project_name = '';


  // add customer
  edit_page_1 = false;
  edit_page_2 = false;
  edit_page_3 = false;
  customer_name = '';
  customer_code = '';
  service_platform = [];
  service_category = [];
  detail_service_catagory = [];
  service_data;
  service_category_detail = [];
  service_category_detail_style =[{style_service: '10px solid red',
  style_url:'10px solid red'}];
  detail = [];
  list_detail = [];
  service_category_id = [];
  platform_data;
  list_platform = [];
  service_cat = ''

  // edit customer
  // Display all customer on page
  customers = undefined;

  // Parameter of Edit term
  edit_id: string;
  edit_company: string;
  edit_address: string;
  edit_project_manager: string;
  edit_sale_name: string;
  edit_presale_name: string;
  edit_create_date: string;
  edit_project_name: string;
  edit_project_id: string;
  edit_service_id: string;

  edit_service_name = '';
  detail_edit = {}
  //remove user
  modalRemove = false;
  get_customer_id = '';
  get_customer_name = '';

  //select diagram
  url = '';
  reader;
  type_image = '';
  base64textString = '';
  edit_service_detail = {};
  diagram_img: string = '';

  check_success = false;
  message ='';
  // =========================================================================

  config = {
    displayKey: 'customer_name',
    search: true,
    height: 'auto',
    placeholder: 'Select Customer',
    customComparator: () => { },
    limitTo: 10,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search',
    searchOnKey: 'customer_name',
  };

  list_edit_platform = [];
  service_list = [];
  service_list_style = [];

  edit_project: any;
  edit_service: any;
  data_service_id: '';
  detail_update = [];
  // city = false

  check_select_customer = false;



  //  ----------- rextest --------------
  rex_address = /^[\ก-\๙a-zA-Z\d][\ก-\๙a-zA-Z'()-./\s\d]+$/;
  rex_name = /^[\ก-\๐a-zA-Z,-]{1}[\ก-\๐a-zA-Z'\s]{0,100}$/;
  rex_postal_code = /^[0-9,-]{1,5}$/;
  rex_project =/^[\ก-\ฮa-zA-Z,-]{1}[\ก-\๙a-zA-Z'\s()+&,\d]{0,60}$/;
  rex_url = /^(?:http(s)?:\/\/)?[\w.-]{0,70}(?:\.[\w\.-]+){0,50}[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
  rex_custom = /^[a-zA-Z0-9\\d,-]{1,50}[a-zA-Z0-9\\d .,&_-]{0,50}[a-zA-Z0-9\\d]{0,50}$/;

  style_city = '10px solid red';
  style_postal='10px solid red';
  style_project = '10px solid red';
  style_service = '10px solid red';
  style_url = '10px solid red';

  style_address =''
  list_detail_status =[];

  // checkregdetail(i,key){
  //   this.list_detail[i][key]=this.service_category_detail[i]['service_detail'][key]
  // }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  checkDetail(i){
    const list_status =[]
    const index = 0
    Object.entries(this.list_detail[i]).forEach((k:any,v) => {  
      this.list_detail[i][k]=this.service_category_detail[i]['service_detail'][k]
      if (v){
        list_status[index]=true
      } else {
        list_status[index]=false
      }
      
    });
    this.list_detail_status[i]=list_status.some(event=>event==false)
    // console.log(this.list_detail_status)
  }

  addStyle(url) {
    // this.service_list_style.push(this.service_list_style.length);
    this.service_category_detail_style.push({
      style_service: '10px solid red',
      style_url:'10px solid red',
    });
    // console.log(this.service_category_detail_style)
  }

  onDeleteStyle(index){
    this.service_category_detail_style.splice(index,1);
  }


  checkUrl(url,i): void {
    // console.log(i)
    if (url === true) {
      this.style_url = '10px solid #42A948';
    } else {
      this.style_url = '10px solid red';
    }
    this.service_category_detail_style[i]['style_url']= this.style_url
    // console.log(this.service_category_detail_style)
  }

  checkService(service,i): void {
    if (service === true) {
      this.style_service = '10px solid #42A948';
    } else {
      this.style_service = '10px solid red';
    }
    this.service_category_detail_style[i]['style_service']= this.style_service
    // console.log(this.service_category_detail_style)
  }

  checkProject(project): void {
    if (project === true) {
      this.style_project = '10px solid #42A948';
    } else {
      this.style_project = '10px solid red';
    }
  }

  checkCity(city): void {
    if (city === true) {
      this.style_city = '10px solid #42A948';
    } else {
      this.style_city = '10px solid red';
    }
  }

  checkPostal(postal): void {
    
    if (postal === true) {
      this.style_postal = '10px solid #42A948';
    } else {
      this.style_postal = '10px solid red';
    }
  }




  //  ================================== edit customer =================
  

  apiEditCustomer(send_data) {
    return this.http.put(environment.apiUrl + '/customers', send_data);
  }

  showAllCustomers() {
    this.http.get(environment.apiUrl + '/admin/customers').subscribe((result) => {
      this.customers = result['result'];
      this.dataSource = new MatTableDataSource(result['result']);
      this.dataSource.paginator = this.paginator;
      
    });
  }

  showEditService() {
    this.edit_project = this.customers.filter(e => e.customer_id === this.edit_id)[0]['project'];
    this.edit_project_name = this.customers.filter(e => e.customer_id === this.edit_id)[0]['project'][0]['project_name'];
    this.edit_project_id = this.edit_project[0]['project_id']
    // console.log(`DEBUG: Show diagram, ${this.edit_project_id}`);
    const url = `${this.api_url}/diagrams/${this.edit_project_id}`;
    this.http.get(url).subscribe(
      (img: any) => {
        this.diagram_img = `data:image/png;base64,${img['result']}`;
      }
    )
    // console.log(this.edit_project_id)
    this.edit_service = this.edit_project[0]['service'];
    this.detail_update = [];
    for (let i = 0; i < this.edit_service.length; i++) {
      this.detail_update[i] = {}
    }
  }


  showEdit(cus_id, company, address, project_manager, sale_name, presale_name, create_date): void {
    this.edit_id = cus_id;
    this.edit_company = company;
    this.edit_address = address;
    this.edit_project_manager = project_manager;
    this.edit_sale_name = sale_name;
    this.edit_presale_name = presale_name;
    this.edit_create_date = create_date;
  }

  
 

  onEditService(event, i) {

    this.data_service_id = this.edit_service[i]['service_id']
    this.edit_service_detail = this.edit_service[i]['service_detail'];


    this.service_category_id[i] = this.data_service.filter(service_category =>
      service_category.service_category_name === event)[0]['service_category_id'];


    this.http.get(this.api_url + '/service/platform/' + this.service_category_id[i]).subscribe(data => {
      this.platform_data = data['result'];


      this.list_edit_platform[i] = [];
      for (const index in this.platform_data) {
        this.list_edit_platform[i].push(this.platform_data[index]['service_platform_name']);
      }
      // console.log('ggggh', this.list_edit_platform);
    });
  }

  ///////////////edit dtail//////////////////
  onchangeDetail(key, event, index) {
    this.detail_update[index][key] = event
  }

  editService(name, url, service_detail, index) {
    this.edit_service_detail = service_detail
    //  console.log(this.detail_update[index]);
    Object.keys(this.edit_service_detail).forEach((key_old) => {
      Object.entries(this.detail_update[index]).forEach(([key_new, v_new]) => {
        if (key_old == key_new) {
          this.edit_service_detail[key_old] = v_new;
        }
      });
    });

    var json_detail = {
      service_id: this.data_service_id,
      service_name: name,
      service_detail: JSON.stringify(this.edit_service_detail),
      url: url
    }
    //console.log(json_detail);
    this.http.put(this.api_url + "/service", json_detail).subscribe(
      data => {
        this.showAllCustomers()
        this.detail_edit = {}
        //console.log('edit detail>>>', data)
      },
      error => {
        //console.log(error)
      }
    )

  }


  ///////////////edit dtail end//////////////////

  editDiagram() {
    var json_diagram = {
      project_id: this.edit_project_id,
      diagram_type: this.type_image,
      diagram_img: this.base64textString
    }
    
    //console.log('edit diagram data >>>>>>>',json_diagram)
    this.http.put(this.api_url + "/diagrams", json_diagram).subscribe(
      data => {
        this.showAllCustomers()
        this.url = ''
        // console.log('edit diagram>>>', data)
      },
      error => {
        // console.log(error)
      }
    )

  }


  editProject() {
    var json_data = {
      project_id: this.edit_project_id,
      project_name: this.edit_project_name
    }
    this.http.put(this.api_url + "/projects", json_data).subscribe(
      data => {
        this.showAllCustomers()
        //console.log('edit project>>>', data)
      },
      error => {
        //console.log(error)
      }
    )
  }


  editCustomer(): void {
    const send_data = {
      customer_id: this.edit_id,
      address: this.edit_address,
      company: this.edit_company,
      presale_name: this.edit_presale_name,
      project_manager: this.edit_project_manager,
      sale_name: this.edit_sale_name
    };

    this.apiEditCustomer(send_data).subscribe((response) => {
      this.showAllCustomers();
    }, (error) => {
      //console.log(error);
    });
  }

  // =============================================================================================
  selectionChanged(event) {
    this.customer_code = event['value']['customer_code'];
    this.customer_name = event['value']['customer_name'];
  }

  onDrop(event) {
    event.preventDefault();
    const data: FileList = event.target.files;
  }

  onDragOver(event) {
    event.stopPropagation();
    event.preventDefault();
  }

  clearValues(): void {
    this.service_list = [];
    this.name = undefined;
    this.company = undefined;
    this.street = undefined;
    this.country = undefined;
    this.tel = undefined;
    // new user /new customer
    this.address = undefined;
    this.postal_code = undefined;
    this.city = undefined;
    this.project_name = undefined;
    this.project_manager = undefined;
    this.presale_name = undefined;
    this.sale_name = undefined;
    this.base64textString = undefined;
    this.type_image = undefined;
    this.list_detail = [];
    this.service_category_detail = [];
    this.addDiv();
    this.customer_name = undefined;
    
  }


  cancleCreate(): void {

    this.table_customer = true;
    this.page_1 = false;
    this.page_2 = false;
    this.page_3 = false;
    this.clearValues();
    this.url = undefined;
    this.edit_page_1 = false;
    this.edit_page_2 = false;
    this.edit_page_3 = false;
    this.modalRemove = false;
    this.style_city = '10px solid red';
    this.style_postal='10px solid red';
    this.style_project = '10px solid red';
    this.style_service = '10px solid red';
    this.style_url = '10px solid red';
    
    this.service_category_detail_style =[{style_service: '10px solid red',style_url:'10px solid red'}];
  }

  ngOnInit() {

    this.http.get(this.api_url + '/customers/center').subscribe(data => { this.customer_center = data['result']; });
    this.serviceCatagory();
    this.addDiv();
    // this.addStyle(url);
    this.showAllCustomers();
  }

  image_edit 
  getImg(id){
    this.http.get(this.api_url + '/diagrams/'+id).subscribe(data => {this.image_edit=data})
  }


  // =============================== Remove Customer ==============================================
  getCustomer(id, name) {
    // console.log(id, name)
    this.get_customer_id = id
    this.get_customer_name = name
  }

  onDeletecustomer() {
    this.http.delete(this.api_url + '/customers/' + this.get_customer_id).subscribe(
      data => {
        this.cancleCreate()
        this.message = data['message']
        this.check_success = true
        this.countTime()
        this.showAllCustomers()
      }
    )
  }

  time() {
    if (this.check_select_customer) {
            setTimeout(() => {
              this.check_select_customer = false;
                }, 3000);
        }
  }

  checkCusNull(){
    this.check_select_customer = true
    this.message = 'Please select customer'
    this.time()
  }


  // ============================== add customer ===================================================
  checkUser(edit_address): void {
    // console.log('>>>>>>',username)
    if (edit_address === true) {
      this.style_address = '';
    } else {
      this.style_address = '10px solid red';
    }
  }

  
  addCustomer(address, city, postal_code, project_manager, sale_name, presale_name, project_name) {
    const data_add_customer = {
      company: this.customer_name,
      customer_code: this.customer_code,
      address: address + ', ' + city + ', ' + postal_code,
      project_manager: project_manager,
      sale_name: sale_name,
      presale_name: presale_name,

      project: [
        {
          project_name: project_name,
          diagram_img: this.base64textString,
          diagram_type: this.type_image,
          service: this.service_category_detail
        }]
    };

    console.log(data_add_customer)

    this.http.post(this.api_url + "/admin/customers", data_add_customer).subscribe(
      data => {
        this.showAllCustomers()
        this.message = data['message']
        this.cancleCreate()
        this.check_success = true
        this.countTime()
        
      },
      error => {
        this.message = 'error'
        this.check_select_customer = true
        this.time()
        console.log(error)
      }
    )
  }


  countTime() {
    if (this.check_success) {
            setTimeout(() => {
              this.check_success = false;
                }, 3000);
        }
  }

  serviceCatagory() {
    this.http.get(this.api_url + '/service/category').subscribe(data => {
      // console.log(data)
      const jsonData = data['result'];
      this.data_service = jsonData;
      for (const i in jsonData) {
        this.service_category.push({ service_category_name: jsonData[i]['service_category_name'], service_category_id: jsonData[i]['service_category_id'] });
      }
      console.log('dddddddd',this.service_category)
    });
  }


  addDiv() {
    this.service_list.push(this.service_list.length);
    this.list_detail_status.push(false);
    // console.log(this.service_list);
    this.service_category_detail.push({
      service_name: '',
      service_category_name: '',
      service_platform_name: '',
      service_detail: {},
      url: ''
    });
  }

  onDeleteAdd(index){
    // this.service_list.splice(index,1);
    this.service_category_detail.splice(index,1);
  }

  addDetail() {
    this.detail = this.service_category_detail.map(detail => ({
      service_name: detail.service_name,
      service_category_name: detail.service_category_name,
      service_platform_name: detail.service_platform_name,
      service_detail: detail.service_detail,
      url: detail.url
    }));
    // console.log('data service', this.data_service[0]['service_category_name'])
    // console.log('add detail>>> ', this.detail);
  }



  onChangeService(event, i) {
    // console.log('onChangeService')
    
    this.list_platform[i] = [];
    this.list_detail[i] = this.data_service.filter(service_category =>
      service_category.service_category_name === event)[0]['service_detail'];
      // console.log(this.list_detail)
      Object.keys(this.list_detail[i]).forEach( (k ,v) => {
        this.list_detail[i][k]=''
      });

    this.service_category_id[i] = this.data_service.filter(service_category =>
      service_category.service_category_name === event)[0]['service_category_id'];

    // console.log(this.service_category_id[i])
    this.http.get(this.api_url + '/service/platform/' + this.service_category_id[i]).subscribe(data => {
      // console.log(data)
      this.platform_data = data['result'];
      
      for (const index in this.platform_data) {
        this.list_platform[i].push(this.platform_data[index]['service_platform_name']);
      }
      // console.log(this.platform_data)
    });

  }


  type_size ='';
  

  resizeImage(file) {
    var canvas = document.createElement('canvas');
    var context = canvas.getContext('2d');
    var maxW = 100;
    var maxH = 100;
    var img = document.createElement('img');

    img.onload = function() {
      var iw = img.width;
      var ih = img.height;
      var scale = Math.min((maxW / iw), (maxH / ih));
      var iwScaled = iw * scale;
      var ihScaled = ih * scale;
      canvas.width = iwScaled;
      canvas.height = ihScaled;
      context.drawImage(img, 0, 0, iwScaled, ihScaled);
      // console.log('resize>>>',canvas.toDataURL(file.target.file[0]));
      
      
      document.body.innerHTML+=canvas.toDataURL();
      // console.log(document.body.innerHTML)
    }
    img.src = URL.createObjectURL(file);

    // console.log('resize>>>',img.onload)
  }

  
  

  onSelectFile(event: any) {
    
    let file = event.target.files[0];
  
    if (event.target.files && event.target.files[0]) {
      
      this.reader = new FileReader();
      this.reader.readAsDataURL(event.target.files[0]); 
      this.type_image = event.target.files[0].type;
      // console.log(this.type_image)
      this.type_size = event.target.files[0].size;
      this.reader.onload = (event: any) => { 
        this.url = event.target['result'].split(',')
        this.base64textString = this.url[1];
        this.diagram_img = `data:image/png;base64,${this.url[1]}`;
      }
    }
  }
}
