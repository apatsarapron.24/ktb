import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomersComponent } from './customers.component';
import { AuthenerGuardsService } from '../../authener/guards/authener-guards.service';

const routes: Routes = [
  {
    path: '',
    component: CustomersComponent,
    canActivate: [AuthenerGuardsService],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomersRoutingModule { }
