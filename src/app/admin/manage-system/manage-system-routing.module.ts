import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManageSystemComponent } from './manage-system.component';
import { from } from 'rxjs';
import { CanDeactivateGuard } from './../../services/configGuard/config-guard.service';

const routes: Routes = [
  {
    path: '',
    component: ManageSystemComponent,
    canDeactivate: [CanDeactivateGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageSystemRoutingModule { }
