import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';

export interface DialogData {
  headerDetail: any;
  bodyDetail1: any;
  bodyDetail2: any;
  returnStatus: any;
  icon: any;
}

@Component({
  selector: 'app-dialog-save-status-managesystem',
  templateUrl: './dialog-save-status-managesystem.component.html',
  styleUrls: ['./dialog-save-status-managesystem.component.scss']
})
export class DialogSaveStatusManagesystemComponent implements OnInit {

  constructor(
    private router: Router,
    private dialogRef: MatDialogRef<DialogSaveStatusManagesystemComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.dialogRef.disableClose = true;
  }

  ngOnInit() {
  }

  close() {
    this.data.returnStatus = null;
    this.dialogRef.close(this.data);
  }

  back() {
    this.data.returnStatus = false;
    this.dialogRef.close(this.data);
  }

  save() {
    this.data.returnStatus = true;
    this.dialogRef.close(this.data);
  }
}
