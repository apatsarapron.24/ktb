import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogSaveStatusManagesystemComponent } from './dialog-save-status-managesystem.component';

describe('DialogSaveStatusManagesystemComponent', () => {
  let component: DialogSaveStatusManagesystemComponent;
  let fixture: ComponentFixture<DialogSaveStatusManagesystemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogSaveStatusManagesystemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogSaveStatusManagesystemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
