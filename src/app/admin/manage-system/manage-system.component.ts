import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { ManageSystemService } from '../../services/manage-system/manage-system.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { CanDeactivateGuard } from './../../services/configGuard/config-guard.service';
import { DialogSaveStatusManagesystemComponent } from './dialog-save-status-managesystem/dialog-save-status-managesystem.component';
import { MatDialog } from '@angular/material';
import { Subject } from 'rxjs/Subject';

declare var $: any;

export interface UserData {
  id: string;
  name: string;
  code: string;
  objective: string;
  yearPlan: string;
  quarter: string;
  planStatus: string;
  planStatusDetail: string;
  division: {
    ouCode: string;
    ouName: string;
    groupOuCod: string;
  };
  group: {
    ouCode: string;
    ouName: string;
  };
  department: {
    ouCode: string;
    ouName: string;
    groupOuCod: string;
    sctrOuCode: string;
  };
  poName: string;
}

@Component({
  selector: 'app-manage-system',
  templateUrl: './manage-system.component.html',
  styleUrls: ['./manage-system.component.scss'],
})
export class ManageSystemComponent implements OnInit {
  displayedColumns: string[] = [
    'name',
    'code',
    'objective',
    'yearPlan',
    'quarter',
    'planStatus',
    'planStatusDetail',
    'division',
    'group',
    'department',
    'poName',
    'action',
  ];
  dataSource: MatTableDataSource<UserData>;
  WorkgroupControl = new FormControl();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<any>;

  // MatPaginator Inputs
  length = 100;
  pageSize = 10;
  currentPage = 0;
  levelpage=0;
  pageSizeOptions: number[] = [10, 25, 50];

  // MatPaginator Output
  pageEvent: PageEvent;

  savemode = false;
  backpagemode = true;

  DataSysem: any = [];
  Delete_system: any = [];

  get_objective = [
    {
      name: 'ผลิตภัณฑ์ใหม่',
      value: 'ผลิตภัณฑ์ใหม่',
    },
    {
      name: 'ทบทวนผลิตภัณฑ์ระหว่างปี',
      value: 'ทบทวนผลิตภัณฑ์ระหว่างปี',
    },
    {
      name: 'ทบทวนผลิตภัณฑ์ประจำปี',
      value: 'ทบทวนผลิตภัณฑ์ประจำปี',
    },
  ];
  get_quarter = [
    {
      name: '1',
      value: '1',
    },
    {
      name: '2',
      value: '2',
    },
    {
      name: '3',
      value: '3',
    },
    {
      name: '4',
      value: '4',
    },
  ];
  get_planstatus = [
    {
      name: 'ปรับแผน',
      value: 'ปรับแผน',
    },
    {
      name: 'ล่าช้า',
      value: 'ล่าช้า',
    },
    {
      name: 'ยกเลิก',
      value: 'ยกเลิก',
    },
    {
      name: 'เป็นไปตามแผน',
      value: 'เป็นไปตามแผน',
    },
  ];
  get_yearplan = [
    {
      name: 'Plan',
      value: 'Plan',
    },
    {
      name: 'Unplan',
      value: 'Unplan',
    },
  ];
  departmentListOucode = null;
  workgroupListOucode = null;
  divisionListtOucode = null;
  departmentList: any = [];
  workgroupList: any = [];
  divisionList: any = [];
  divisionList_data: any;
  workgroupList_data: any;
  departmentList_data: any;
  divisionList_status = false;
  workgroupList_status = false;
  departmentList_status = false;
  dataSource_status = false;

  name: any = null;
  code: any = null;
  objective: any = null;
  yearPlan: any = null;
  quarter: any = null;
  planStatus: any = null;
  planStatusDetail: any = null;
  division: any = null;
  group: any = null;
  department: any = null;
  poName: any = null;
  action: any;

  changResetStatus = [];

  deletesystem = [];
  yearplanmanagement = [];
  confirmState = 'confirm';
  modestatus = 'view';
  status_loading = true;

  formControlGroup = new FormControl();
  saveDraftstatus_State = false;

  constructor(
    private router: Router,
    private managesystemservice: ManageSystemService,
    public dialog: MatDialog,
    private guardService: CanDeactivateGuard
  ) {
    this.dataSource = new MatTableDataSource();
  }

  ngOnInit() {
    this.getSystemManagement();
    this.dataSource.paginator = this.paginator;

    this.dataSource.sort = this.sort;
    this.getDropdown();
  }
  handlePage(event) {
    this.levelpage = event.pageSize;
    this.currentPage = event.pageIndex;
    console.log(event.pageSize)
    console.log(this.currentPage)
  }
  getSystemManagement() {
    this.status_loading = true;
    this.managesystemservice.getSystemManagement().subscribe((res) => {
      if (res['data']) {
        this.status_loading = false;
        this.dataSource.data = res['data'];
        console.log('000 this.dataSource.data =>', this.dataSource.data);
        this.dataSource_status = true;
        this.checkDropdown();
        // this.status_loading = false;
      }
    });
  }

  getDropdown() {
    this.managesystemservice.divisionList().subscribe((res) => {
      console.log('สาย grp', res);
      if (res['status'] === 'success') {
        this.divisionList_data = res['data'];
        this.divisionList_status = true;
        this.checkDropdown();
      }
    });
    this.managesystemservice.workgroupList().subscribe((res) => {
      console.log('กลุ่ม sctr', res);
      if (res['status'] === 'success') {
        this.workgroupList_data = res['data'];
        this.workgroupList_status = true;
        this.checkDropdown();
      }
    });
    this.managesystemservice.departmentList().subscribe((res) => {
      console.log('ฝ่าย dept', res);
      if (res['status'] === 'success') {
        this.departmentList_data = res['data'];
        this.departmentList_status = true;
        this.checkDropdown();
      }
    });
  }

  putDataDropdown() {
    for (let index = 0; index < this.dataSource.data.length; index++) {
      // changResetStatus
      this.changResetStatus.push(true);

      // สาย
      if (this.dataSource.data[index].division !== null || this.dataSource.data[index].division === undefined) {
        const divisionAdd = [this.dataSource.data[index].division];
        this.divisionList.push(divisionAdd);
      } else {
        this.divisionList.push(this.divisionList_data);
      }

      // กลุ่ม
      if (this.dataSource.data[index].group !== null || this.dataSource.data[index].group === undefined) {
        const groupAdd = [this.dataSource.data[index].group];
        this.workgroupList.push(groupAdd);
      } else {
        this.workgroupList.push(this.workgroupList_data);
      }

      // ฝ่าย
      if (this.dataSource.data[index].department !== null || this.dataSource.data[index].department === undefined) {
        const departmentAdd = [this.dataSource.data[index].department];
        this.departmentList.push(departmentAdd);
      } else {
        this.departmentList.push(this.departmentList_data);
      }

      if (this.dataSource.data.length === index + 1) {
        this.status_loading = false;
      }

    }
    // console.log('this.divisionList', this.divisionList);
    // console.log('this.workgroupList', this.workgroupList);
    // console.log('this.departmentList', this.departmentList);
  }
  // Check filter status
  checkFilterStatus(index) {
    // console.log(index)
    if ((this.dataSource.data[index].division === null || this.dataSource.data[index].division === undefined)
      && (this.dataSource.data[index].group === null || this.dataSource.data[index].group === undefined)
      && (this.dataSource.data[index].department === null || this.dataSource.data[index].department === undefined)
    ) {
      return false;
    } else { return true; }
  }

  // filter
  filterSearchNew(type, filtdata, index) {
    console.log('==== filterSearch New ====');
    console.log('filterSearchNew type =', type);
    console.log('filterSearchNew data =', filtdata);
    console.log('filterSearchNew index =', index);

    if (type === 'grp' && filtdata !== 'grpReset') {

      const filldataNew = this.divisionList_data.filter((ele) => {
        return ele.ouCode === filtdata.ouCode;
      });
      this.dataSource.data[index].division = JSON.parse(JSON.stringify(filldataNew[0]));
      const arr = JSON.parse(JSON.stringify(filldataNew[0]));

      if ((this.dataSource.data[index].group === null || this.dataSource.data[index].group === undefined)
        && (this.dataSource.data[index].department === null || this.dataSource.data[index].department === undefined)) {
        this.filterSearchWorkgroup(type, arr.ouCode, index);
        if (this.workgroupList[index] !== 'nondata') {
          this.filterSearchDepartment(type, arr.ouCode, index);
        } else if (this.workgroupList[index] === 'nondata') {
          this.filterSearchDepartment('grpSctr', arr.ouCode, index, null);
        }
      } else if ((this.dataSource.data[index].group === null || this.dataSource.data[index].group === undefined)
        && (this.dataSource.data[index].department !== null && this.dataSource.data[index].department !== undefined)) {
        const arrSctr = this.dataSource.data[index].department;
        this.filterSearchWorkgroup('grpDept', arr.ouCode, index, arrSctr.sctrOuCode);
        this.filterSearchDepartment('grpDept', arr.ouCode, index, arrSctr.ouCode);
      } else if (this.dataSource.data[index].group !== null && this.dataSource.data[index].department === null) {

      } else if ((this.dataSource.data[index].group !== null && this.dataSource.data[index].group !== undefined)
        && (this.dataSource.data[index].department !== null && this.dataSource.data[index].department !== undefined)) {
        const arrSctr = this.dataSource.data[index].group;
        const arrSctr2 = this.dataSource.data[index].department;
        this.filterSearchDepartment('grpSctrdept', arr.ouCode, index, arrSctr.ouCode, arrSctr2.ouCode);
      } else {
        if (this.dataSource.data[index].department === null || this.dataSource.data[index].department === undefined) {
          const arrSctr = this.dataSource.data[index].group;
          this.filterSearchDepartment('grpSctr', arr.ouCode, index, arrSctr);
        } else {
          this.filterSearchDepartment('grp', arr.ouCode, index);
        }
      }
    } else if (type === 'sctr' && filtdata !== 'sctrReset') {
      const filldataNew = this.workgroupList_data.filter((ele) => {
        return ele.ouCode === filtdata.ouCode;
      });
      this.dataSource.data[index].group = JSON.parse(JSON.stringify(filldataNew[0]));
      const arr = JSON.parse(JSON.stringify(filldataNew[0]));
      if (this.dataSource.data[index].division === null || this.dataSource.data[index].division === undefined) {
        this.filterSearchDivision(type, arr.groupOuCode, index);
        if ((this.dataSource.data[index].department === null || this.dataSource.data[index].department === undefined)
          && this.divisionList[index] !== 'nondata') {
          this.filterSearchDepartment(type, arr.ouCode, index);
        } else if ((this.dataSource.data[index].department === null || this.dataSource.data[index].department === undefined)
          && this.divisionList[index] === 'nondata') {
          const arrSctr = this.dataSource.data[index].group;
          this.filterSearchDepartment('grpSctr', null, index, arr.ouCode);
        } else {
          this.filterSearchDepartment(type, arr.ouCode, index);
        }
      } else {
        this.filterSearchDivision(type, arr.groupOuCode, index);
        if ((this.dataSource.data[index].department === null || this.dataSource.data[index].department === undefined)
          && this.divisionList[index] !== 'nondata') {
          this.filterSearchDepartment(type, arr.ouCode, index);
        } else if ((this.dataSource.data[index].department === null || this.dataSource.data[index].department === undefined)
          && this.divisionList[index] === 'nondata') {
          const arrSctr = this.dataSource.data[index].division;
          this.filterSearchDepartment('grpSctr', arrSctr.ouCode, index, arr.ouCode);
        } else {
          const arrSctr = this.dataSource.data[index].department;
          this.filterSearchDepartment('grpSctrdept', arr.groupOuCode, index, arr.ouCode, arrSctr.ouCode);
        }
      }


    } else if (type === 'dept' && filtdata !== 'deptReset') {
      const filldataNew = this.departmentList_data.filter((ele) => {
        return ele.ouCode === filtdata.ouCode;
      });
      this.dataSource.data[index].department = JSON.parse(JSON.stringify(filldataNew[0]));
      const arr = JSON.parse(JSON.stringify(filldataNew[0]));

      this.filterSearchDivision(type, arr.groupOuCode, index);
      this.filterSearchWorkgroup(type, arr.sctrOuCode, index);
    }

    if ((type === 'grp' && filtdata === 'grpReset')
      || (type === 'sctr' && filtdata === 'sctrReset')
      || (type === 'dept' && filtdata === 'deptReset')) {
      if (this.changResetStatus[index] === true) {
        this.dataSource.data[index].division = undefined;
        this.dataSource.data[index].group = undefined;
        this.dataSource.data[index].department = undefined;
        this.divisionList[index] = JSON.parse(JSON.stringify(this.divisionList_data));
        this.workgroupList[index] = JSON.parse(JSON.stringify(this.workgroupList_data));
        this.departmentList[index] = JSON.parse(JSON.stringify(this.departmentList_data));
        this.changResetStatus[index] = !this.changResetStatus[index];
        console.log('001 sctrReset New undefined');
      } else {
        this.dataSource.data[index].division = null;
        this.dataSource.data[index].group = null;
        this.dataSource.data[index].department = null;
        this.divisionList[index] = JSON.parse(JSON.stringify(this.divisionList_data));
        this.workgroupList[index] = JSON.parse(JSON.stringify(this.workgroupList_data));
        this.departmentList[index] = JSON.parse(JSON.stringify(this.departmentList_data));
        this.changResetStatus[index] = !this.changResetStatus[index];
        console.log('002 sctrReset New null');
      }
    }

  }
  clearForm(index) {
    this.dataSource.data[index].division = undefined;
    this.dataSource.data[index].group = undefined;
    this.dataSource.data[index].department = undefined;
  }

  filterSearch(type, filtdata, index) {
    // ฝ่าย:dept - กลุ่ม:sctr - สาย:grp
    console.log('==== filterSearch ====');
    console.log('filterSearch type =', type);
    console.log('filterSearch data =', filtdata);
    console.log('filterSearch index =', index);


    if (type === 'grp' && filtdata.ouCode !== 'grpReset') {

      const filldataNew = this.divisionList_data.filter((ele) => {
        return ele.ouCode === filtdata.ouCode;
      });
      this.dataSource.data[index].division = JSON.parse(JSON.stringify(filldataNew[0]));
      const arr = JSON.parse(JSON.stringify(filldataNew[0]));

      if ((this.dataSource.data[index].group === null || this.dataSource.data[index].group === undefined)
        && (this.dataSource.data[index].department === null || this.dataSource.data[index].department === undefined)) {
        this.filterSearchWorkgroup(type, arr.ouCode, index);
        if (this.workgroupList[index] !== 'nondata') {
          this.filterSearchDepartment(type, arr.ouCode, index);
        } else if (this.workgroupList[index] === 'nondata') {
          this.filterSearchDepartment('grpSctr', arr.ouCode, index, null);
        }
      } else if ((this.dataSource.data[index].group === null || this.dataSource.data[index].group === null)
        && (this.dataSource.data[index].department !== null && this.dataSource.data[index].department !== undefined)) {
        const arrSctr = this.dataSource.data[index].department;
        this.filterSearchWorkgroup('grpDept', arr.ouCode, index, arrSctr.sctrOuCode);
        this.filterSearchDepartment('grpDept', arr.ouCode, index, arrSctr.ouCode);
      } else {
        if (this.dataSource.data[index].department === null || this.dataSource.data[index].department === undefined) {
          const arrSctr = this.dataSource.data[index].group;
          this.filterSearchDepartment('grpSctr', arr.ouCode, index, arrSctr);
        } else {
          this.filterSearchDepartment('grp', arr.ouCode, index);
        }
      }

    } else if (type === 'sctr' && filtdata.ouCode !== 'sctrReset') {
      const filldataNew = this.workgroupList_data.filter((ele) => {
        return ele.ouCode === filtdata.ouCode;
      });
      this.dataSource.data[index].group = JSON.parse(JSON.stringify(filldataNew[0]));
      const arr = JSON.parse(JSON.stringify(filldataNew[0]));

      if (this.dataSource.data[index].division === null || this.dataSource.data[index].division === undefined) {
        this.filterSearchDivision(type, arr.groupOuCode, index);
        if ((this.dataSource.data[index].department === null || this.dataSource.data[index].department === null)
          && this.divisionList[index] !== 'nondata') {
          this.filterSearchDepartment(type, arr.ouCode, index);
        } else if ((this.dataSource.data[index].department === null || this.dataSource.data[index].department === undefined)
          && this.divisionList[index] === 'nondata') {
          this.filterSearchDepartment('grpSctr', null, index, arr.ouCode);
        } else {
          this.filterSearchDepartment(type, arr.ouCode, index);
        }
      } else {
        this.filterSearchDivision(type, arr.groupOuCode, index);
        if ((this.dataSource.data[index].department === null || this.dataSource.data[index].department === undefined)
          && this.divisionList[index] !== 'nondata') {
          this.filterSearchDepartment(type, arr.ouCode, index);
        } else if ((this.dataSource.data[index].department === null || this.dataSource.data[index].department === undefined)
          && this.divisionList[index] === 'nondata') {
          this.filterSearchDepartment('grpSctr', arr.ouCode, index);
        } else {
          this.filterSearchDepartment(type, arr.ouCode, index);
        }
      }


    } else if (type === 'dept' && filtdata.ouCode !== 'deptReset') {
      const filldataNew = this.departmentList_data.filter((ele) => {
        return ele.ouCode === filtdata.ouCode;
      });
      this.dataSource.data[index].department = JSON.parse(JSON.stringify(filldataNew[0]));
      const arr = JSON.parse(JSON.stringify(filldataNew[0]));

      this.filterSearchDivision(type, arr.groupOuCode, index);
      this.filterSearchWorkgroup(type, arr.sctrOuCode, index);
    }

    if ((type === 'grp' && filtdata.ouCode === 'grpReset')
      || (type === 'sctr' && filtdata.ouCode === 'sctrReset')
      || (type === 'dept' && filtdata.ouCode === 'deptReset')) {
      if (this.changResetStatus[index] === true) {
        this.dataSource.data[index].division = undefined;
        this.dataSource.data[index].group = undefined;
        this.dataSource.data[index].department = undefined;
        this.divisionList[index] = JSON.parse(JSON.stringify(this.divisionList_data));
        this.workgroupList[index] = JSON.parse(JSON.stringify(this.workgroupList_data));
        this.departmentList[index] = JSON.parse(JSON.stringify(this.departmentList_data));
        this.changResetStatus[index] = !this.changResetStatus[index];
        console.log('001 sctrReset undefined');
      } else {
        this.dataSource.data[index].division = null;
        this.dataSource.data[index].group = null;
        this.dataSource.data[index].department = null;
        this.divisionList[index] = JSON.parse(JSON.stringify(this.divisionList_data));
        this.workgroupList[index] = JSON.parse(JSON.stringify(this.workgroupList_data));
        this.departmentList[index] = JSON.parse(JSON.stringify(this.departmentList_data));
        this.changResetStatus[index] = !this.changResetStatus[index];
        console.log('002 sctrReset null');
      }
    }

  }

  filterSearchDivision(type, code, index) {
    console.log('filterSearchDivision code =', code);
    console.log('filterSearchDivision index =', index);

    // search grp
    const arr_divisionList = this.divisionList_data.filter((ele) => {
      return ele.ouCode === code;
    });
    if (arr_divisionList.length !== 0) {
      this.divisionList[index] = arr_divisionList;
    } else {
      this.divisionList[index] = 'nondata';
    }


    if (this.divisionList[index] !== 'nondata') {
      let sctrCheck = null;
      if (this.dataSource.data[index].division) {
        // sctrCheck
        sctrCheck = this.divisionList[index].filter((ele) => {
          return ele.ouCode === this.dataSource.data[index].division.ouCode;
        });
        // set in put sctr
        if (sctrCheck.length === 0) {
          this.dataSource.data[index].division = undefined;
        }
      }
    } else if (this.divisionList[index] === 'nondata') {
      this.dataSource.data[index].division = null;
      if (type === 'sctr') {
        this.filterSearchDepartment('grpSctr', null, index, code);
      }
    }

  }

  filterSearchWorkgroup(type, code, index, code2?) {
    // console.log('filterSearchWorkgroup code =', code);
    // console.log('filterSearchWorkgroup index =', index);


    // search sctr
    if (type === 'grp') {

      const arr_workgroupList = this.workgroupList_data.filter((ele) => {
        return ele.groupOuCode === code;
      });

      if (arr_workgroupList.length > 0) {
        this.workgroupList[index] = arr_workgroupList;
      } else {
        this.workgroupList[index] = 'nondata';
      }
    } else if (type === 'dept') {
      const arr_workgroupList = this.workgroupList_data.filter((ele) => {
        return ele.ouCode === code;
      });

      if (arr_workgroupList.length > 0) {
        this.workgroupList[index] = arr_workgroupList;
      } else {
        this.workgroupList[index] = 'nondata';
      }
    } else if (type === 'grpDept') {
      const arr_workgroupList = this.workgroupList_data.filter((ele) => {
        return ele.ouCode === code2;
      });
      if (arr_workgroupList.length !== 0) {
        this.workgroupList[index] = arr_workgroupList;
      } else {
        this.workgroupList[index] = 'nondata';
      }
    }

    if (this.workgroupList[index] !== 'nondata') {
      // sctrCheck
      let sctrCheck = null;
      if (this.dataSource.data[index].group) {
        sctrCheck = this.workgroupList[index].filter((ele) => {
          return ele.ouCode === this.dataSource.data[index].group.ouCode;
        });
        // set in put sctr
        if (sctrCheck.length === 0) {
          this.dataSource.data[index].group = null;
        }
      }
    } else if (this.workgroupList[index] === 'nondata') {
      this.dataSource.data[index].group = null;
    }

    this.group = null;
    this.department = null;
  }

  filterSearchDepartment(type, code, index, code2?, code3?) {
    // console.log('filterSearchDepartment type =', type);
    // console.log('filterSearchDepartment code =', code);
    // console.log('filterSearchDepartment index =', index);
    // console.log('filterSearchDepartment code2 =', code2);
    // console.log('filterSearchDepartment code3 =', code3);

    if (type === 'grp') {
      const arr_Department = this.departmentList_data.filter((ele) => {
        return ele.groupOuCode === code;
      });
      if (arr_Department.length > 0) {
        this.departmentList[index] = arr_Department;
      } else {
        this.departmentList[index] = 'nondata';
      }

      if (this.departmentList[index] !== 'nondata') {
        let deptCheck = null;
        if (this.dataSource.data[index].department) {
          // dept Check
          deptCheck = this.departmentList[index].filter((ele) => {
            return ele.ouCode === this.dataSource.data[index].department.ouCode;
          });
          // set in put dept
          if (deptCheck.length === 0) {
            this.dataSource.data[index].department = null;
          }
        }
      } else if (this.departmentList[index] === 'nondata') {
        this.dataSource.data[index].department = null;
      }


    } else if (type === 'sctr') {
      const arr_Department = this.departmentList_data.filter((ele) => {
        return ele.sctrOuCode === code;
      });
      if (arr_Department.length > 0) {
        this.departmentList[index] = arr_Department;
      } else {
        this.departmentList[index] = 'nondata';
      }

      if (this.departmentList[index] !== 'nondata') {
        let deptCheck = null;
        if (this.dataSource.data[index].department) {
          // dept Check
          deptCheck = this.departmentList[index].filter((ele) => {
            return ele.ouCode === this.dataSource.data[index].department.ouCode;
          });
          // set in put dept
          if (deptCheck.length === 0) {
            this.dataSource.data[index].department = null;
          }
        }
      } else if (this.departmentList[index] === 'nondata') {
        this.dataSource.data[index].department = null;
      }

    } else if (type === 'grpSctr') {
      const arr_Department = this.departmentList_data.filter((ele) => {
        return ele.groupOuCode === code && ele.sctrOuCode === code2;
      });
      if (arr_Department.length > 0) {
        this.departmentList[index] = arr_Department;
      } else {
        this.departmentList[index] = 'nondata';
      }

      if (this.departmentList[index] !== 'nondata') {
        let deptCheck = null;
        if (this.dataSource.data[index].department) {
          // dept Check
          deptCheck = this.departmentList[index].filter((ele) => {
            return ele.ouCode === this.dataSource.data[index].department.ouCode;
          });
          // set in put dept
          if (deptCheck.length === 0) {
            this.dataSource.data[index].department = null;
          }
        }
      } else if (this.departmentList[index] === 'nondata') {
        this.dataSource.data[index].department = null;
      }
    } else if (type === 'grpDept') {
      const arr_Department = this.departmentList_data.filter((ele) => {
        return ele.groupOuCode === code && ele.ouCode === code2;
      });
      if (arr_Department.length > 0) {
        this.departmentList[index] = arr_Department;
      } else {
        this.departmentList[index] = 'nondata';
      }

      if (this.departmentList[index] !== 'nondata') {
        let deptCheck = null;
        if (this.dataSource.data[index].department) {
          // dept Check
          deptCheck = this.departmentList[index].filter((ele) => {
            return ele.ouCode === this.dataSource.data[index].department.ouCode;
          });
          // set in put dept
          if (deptCheck.length === 0) {
            this.dataSource.data[index].department = null;
          }
        }
      } else if (this.departmentList[index] === 'nondata') {
        this.dataSource.data[index].department = null;
      }
    } else if (type === 'grpSctrdept') {
      const arr_Department = this.departmentList_data.filter((ele) => {
        return ele.groupOuCode === code && ele.sctrOuCode === code2 && ele.ouCode === code3;
      });
      console.log('5556666', arr_Department);
      if (arr_Department.length > 0) {
        this.departmentList[index] = arr_Department;
      } else {
        this.departmentList[index] = 'nondata';
      }

      if (this.departmentList[index] !== 'nondata') {
        let deptCheck = null;
        if (this.dataSource.data[index].department) {
          // dept Check
          deptCheck = this.departmentList[index].filter((ele) => {
            return ele.ouCode === this.dataSource.data[index].department.ouCode;
          });
          // set in put dept
          if (deptCheck.length === 0) {
            this.dataSource.data[index].department = null;
          }
        }
      } else if (this.departmentList[index] === 'nondata') {
        this.dataSource.data[index].department = null;
      }
    }

    this.department = null;
  }

  backToDashboardPage() {
    localStorage.setItem('requestId', '');
    this.router.navigate(['dashboard1']);
  }

  autoGrowTextZone(e) {
    e.target.style.height = '0px';
    e.target.style.height = e.target.scrollHeight + 0 + 'px';
  }

  addData() {
    const _data = {
      id: null,
      name: null,
      code: null,
      objective: null,
      yearPlan: null,
      quarter: null,
      planStatus: null,
      planStatusDetail: null,
      division: null,
      group: null,
      department: null,
      poName: null,
    };

    // push
    this.changResetStatus.push(true);
    this.dataSource.data.push(_data);
    this.dataSource = new MatTableDataSource(this.dataSource.data);
    this.dataSource.paginator = this.paginator;
    this.divisionList.push(this.divisionList_data);
    this.workgroupList.push(this.workgroupList_data);
    this.departmentList.push(this.departmentList_data);
    this.changeSaveDraft();

    // unshift
    // this.changResetStatus.unshift(true);
    // this.dataSource.data.unshift(_data);
    // this.dataSource = new MatTableDataSource(this.dataSource.data);
    // this.dataSource.paginator = this.paginator;
    // this.divisionList.unshift(this.divisionList_data);
    // this.workgroupList.unshift(this.workgroupList_data);
    // this.departmentList.unshift(this.departmentList_data);

  }

  deleteRow(data, i, pageSize) {
    // console.log(data)
    // console.log(pageSize)
    console.log("index:", i, "page::", this.currentPage)
    // console.log(i + Number(this.currentPage * pageSize))
    console.log(i + Number(this.levelpage * this.currentPage))
    if (data.code === null) {
      this.dataSource = new MatTableDataSource(this.dataSource.data);
      this.dataSource.data.splice(i + Number(this.levelpage * this.currentPage), 1);
      this.departmentList.splice(i + Number(this.levelpage * this.currentPage), 1);
      this.workgroupList.splice(i + Number(this.levelpage * this.currentPage), 1);
      this.divisionList.splice(i + Number(this.levelpage * this.currentPage), 1);
      this.changResetStatus.splice(i + Number(this.levelpage * this.currentPage), 1);
      // this.divisionList_data.splice(i, 1);
      // this.workgroupList_data.splice(i, 1);
      // this.departmentList_data.splice(i, 1);
      this.deletesystem.push(data.id);
      this.dataSource.paginator = this.paginator;
      this.changeSaveDraft();
    }
  }

  onSave() {
    this.status_loading = true;
    const _data = {
      yearPlanManagement: this.dataSource.data,
      deleteSystemManagement: this.deletesystem,
    };
    console.log('save',_data);
    this.managesystemservice.postSystemManagement(_data).subscribe(
      (res) => {
        console.log(res);

        if (res['status'] === 'success') {
          this.confirmState = 'success';
          this.modestatus = 'view';
          this.status_loading = false;
          this.saveDraftstatus_State = false;
          setTimeout(() => {
            $('#confirmModal').modal('hide');
            this.confirmState = 'confirm';
          }, 3000);
        } else {
          this.status_loading = false;
          Swal.fire({
            title: 'error',
            text: res['message'],
            icon: 'error',
          });
        }
      },
      (err) => {
        console.log(err);
        this.status_loading = false;
        Swal.fire({
          title: 'error',
          text: err['message'],
          icon: 'error',
        });
      }
    );

    this.deletesystem = [];
  }

  async onEdit() {
    this.modestatus = 'edit';
  }

  checkDropdown() {
    // console.log('divisionList_status', this.divisionList_status);
    // console.log('workgroupList_status', this.workgroupList_status);
    // console.log('departmentList_status', this.departmentList_status);
    // console.log('dataSource_status', this.dataSource_status);
    if (this.divisionList_status && this.workgroupList_status && this.departmentList_status && this.dataSource_status) {
      this.putDataDropdown();
    }

  }

  changeSaveDraft() {
    this.saveDraftstatus_State = true;
    this.managesystemservice.status_state = true;
    console.log('saveDraftstatus:', this.saveDraftstatus_State);
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canDeactivate');
    // return confirm();
    const subject = new Subject<boolean>();
    // const status = this.sidebarService.outPageStatus();
    // console.log('000 sidebarService status:', status);
    if (this.saveDraftstatus_State === true) {
      // console.log('000 sidebarService status:', status);
      const dialogRef = this.dialog.open(DialogSaveStatusManagesystemComponent, {
        disableClose: true,
        data: {
          headerDetail: 'ยืนยันการดำเนินการ',
          bodyDetail1: 'ท่านยังไม่ได้บันทึกข้อมูล',
          bodyDetail2: 'ต้องการบันทึกข้อมูลหรือไม่',
          returnStatus: false,
          icon: 'assets/img/Alarm-blue.svg',
        }
      });
      dialogRef.afterClosed().subscribe((data) => {
        console.log('data::', data);
        if (data.returnStatus === true) {
          subject.next(true);
          this.onSave();
          this.saveDraftstatus_State = false;
        } else if (data.returnStatus === false) {
          console.log('000000 data::', data.returnStatus);
          // this.sidebarService.inPageStatus(false);
          subject.next(true);
          return true;
        } else {
          subject.next(false);
        }
      });
      console.log('=====', subject);
      return subject;
    } else {
      return true;
    }
  }


}
