import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ManageSystemRoutingModule } from './manage-system-routing.module';
import { ManageSystemComponent } from './manage-system.component'
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { from } from 'rxjs';
import { TextareaAutoresizeDirectiveModule } from '../../textarea-autoresize.directive/textarea-autoresize.directive.module';
import { DialogSaveStatusManagesystemComponent } from './dialog-save-status-managesystem/dialog-save-status-managesystem.component';
import { CanDeactivateGuard } from './../../services/configGuard/config-guard.service';
import { MatDialogModule } from '@angular/material';
@NgModule({
  declarations: [
    ManageSystemComponent,
    DialogSaveStatusManagesystemComponent
  ],
  imports: [
    CommonModule,
    ManageSystemRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    FormsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    TextareaAutoresizeDirectiveModule,
    MatDialogModule
  ],
  providers: [CanDeactivateGuard],
  entryComponents: [DialogSaveStatusManagesystemComponent]
})
export class ManageSystemModule { }
