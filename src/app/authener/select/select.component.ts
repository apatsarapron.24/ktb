import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../_service/authentication.service';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent implements OnInit {

  constructor(private router: Router, private authenticationService: AuthenticationService, ) { }

  selectData: any = null;

  selectrole = [];
  checkSelect = true;
  status_loading = false;
  ngOnInit() {
    this.authenticationService.GetRole().subscribe( res => {
      // console.log('role:', res);
      this.selectrole = res['roles'];
    });
  }
  onChange(event) {
    if (this.selectData !== null) {
      this.checkSelect = false;
    }
    // console.log(event);
  }
  backtologin() {
    this.router.navigate(['login']);
  }
  sumitRole() {
    this.authenticationService.SelectRole(this.selectData.id).subscribe( res => {
      console.log('select:', res);
      if (res['status'] === 'success') {
        this.authenticationService.setAuthorizationToken(res);
        this.router.navigate(['dashboard1']);
        this.status_loading = true;
        localStorage.setItem('timeLogin' ,
        String(
          Number(new Date().getHours() * 60 * 60) +
          Number(new Date().getMinutes() * 60)  +
          Number(new Date().getSeconds())
          )
        );
        this.authenticationService.countTimer();
      }
    });
  }
}
