import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { AlertLoginComponent } from './alert-login/alert-login.component';
import { ErrorComponent } from './error-permission/error.component';
import { BrowserModule, Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { ClarityModule } from '@clr/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ThaiDatePipe } from '../directive/thaidate.pipe';
import { MatDialogModule } from '@angular/material/dialog';
import { SelectComponent } from './select/select.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    // ClarityModule,
    FormsModule,
    MatDialogModule,
    ReactiveFormsModule,
  ],
  declarations: [
    LoginComponent,
    ThaiDatePipe,
    AlertLoginComponent,
    SelectComponent,
    ErrorComponent
  ],
  entryComponents: [AlertLoginComponent],
  // exports: [AlertLoginComponent]
})
export class AuthenerModule {}
