import { Component, OnInit } from '@angular/core';
import { RouterLink, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {Location} from '@angular/common';

@Component({
  selector: 'app-dashboard',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit {
  
  status_loading = false;
  constructor(private router: Router,private _location: Location) {}

  ngOnInit() {

  }
  backpage() {
    this.status_loading = true;
    this._location.back();
  }
  close() {
    this.status_loading = true;
    this.router.navigate(['dashboard1']);
  }
}
