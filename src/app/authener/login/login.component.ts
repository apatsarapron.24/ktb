import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../_service/authentication.service';
import { RouterLink, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-dashboard',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username = '';
  password = '';
  error_msg = '';
  error_msgs: string;
  loading_status = false;
  roleuser = '';
  loginForm: FormGroup;
  date: any;
  myDate = new Date();

  countTimer: any = null;
  CountTime: any;
  GETDATE: any;
  disableLogin = true;
  counter = 30;

  constructor(private authenticationService: AuthenticationService,
    private router: Router, private datePipe: DatePipe
  ) {

  }

  setUsername(username: string) {
    localStorage.setItem('username', username);
    this.username = username;
  }

  ngOnInit() {
    this.authenticationService.removeAuthorizationToken();
    this.authenticationService.removeSession();

    // this.authenticationService.logout()
    this.loginForm = new FormGroup({
      Username: new FormControl('', [Validators.required]),
      Password: new FormControl('', [Validators.required])
    });

  }

  login() {
    this.username = this.loginForm.value.Username;
    this.password = this.loginForm.value.Password;
    // console.log(this.loginForm.value);
    if (this.username && this.password) {
      this.loading_status = true;
      this.authenticationService.login(this.username, this.password).subscribe(responseLogin => {
        this.authenticationService.setAuthorizationToken(responseLogin);
        console.log('login', responseLogin);
        // console.log('time_login_next', responseLogin['time_login_next']);
        if (responseLogin['time_login_next'] !== undefined) {
          this.disableLogin = false;
          this.settingTimer(responseLogin);
        }
        if (responseLogin['status'] === 'success') {
          this.loading_status = false;
          if (localStorage) {
            localStorage.setItem('username', this.username);
            this.router.navigate(['dashboard1']);
            this.authenticationService.countTimer();
            localStorage.setItem('requestId', '');
            localStorage.setItem('timeLogin',
              String(
                Number(new Date().getHours() * 60 * 60) +
                Number(new Date().getMinutes() * 60) +
                Number(new Date().getSeconds())
              )
            );
            localStorage.setItem('Time',
              String(
                new Date().getHours() + ':' +
                new Date().getMinutes() + ':' +
                new Date().getSeconds()
              )
            );
            // console.log('time:', this.authenticationService.mins , ':', this.authenticationService.seconds);
          } else {
            // console.log(`browser dosen't support please use newer browser such as chrome or firefox`);
            this.error_msg = `browser dosen't support please use newer browser such as chrome or firefox`;
            this.loading_status = false;
          }
        } else if (responseLogin['status'] === 'fail' &&
          responseLogin['message'] === 'Please use other endpoint has more than one role') {
          this.router.navigate(['select-role']);
        } else {
          // console.log('login fail, check your username or password again');
          this.error_msg = 'ล๊อคอินไม่สำเร็จ กรุณาตรวจสอบ รหัสพนักงานและรหัสผ่าน (AD/LDAP)';
          this.loading_status = false;
        }

      },
        _error => {
          this.loading_status = false;
          this.error_msgs = 'ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง';
        });
    } else {
      this.error_msg = 'กรุณาข้อมูลรหัสพนักงาน และรหัสผ่าน (AD/LDAP) ให้ครบถ้วน';
    }
  }
  settingTimer(responseLogin) {
    this.countTimer = new Date(responseLogin['time_login_next']);
    this.GETDATE = new Date();
    var hh = Number(new Date(this.countTimer.getTime() - this.GETDATE.getTime()).getHours() - 7) * 3600;
    var mm = Number(new Date(this.countTimer.getTime() - this.GETDATE.getTime()).getMinutes()) * 60;
    var ss = Number(new Date(this.countTimer.getTime() - this.GETDATE.getTime()).getSeconds());
    this.counter = Number(hh) + Number(mm) + Number(ss);
    this.CountTime = setInterval(() => { this.startTimer(); }, 1000);
  }
  startTimer() {
    this.GETDATE = new Date();
    // this.counter = this.counter - 1;
    if (this.GETDATE.getTime() <= this.countTimer.getTime()) {
      // console.log(this.counter);
      var hh = Number(new Date(this.countTimer.getTime() - this.GETDATE.getTime()).getHours() - 7) * 3600;
      var mm = Number(new Date(this.countTimer.getTime() - this.GETDATE.getTime()).getMinutes()) * 60;
      var ss = Number(new Date(this.countTimer.getTime() - this.GETDATE.getTime()).getSeconds());
      this.counter = Number(hh) + Number(mm) + Number(ss);
      this.loginForm.controls['Username'].disable();
      this.loginForm.controls['Password'].disable();
    } else {
      this.disableLogin = true;
      this.counter = 30;
      clearInterval(this.CountTime);
      this.countTimer = null;
      this.error_msg = '';
      this.loginForm.controls['Username'].enable();
      this.loginForm.controls['Password'].enable();
    }


  }

}
