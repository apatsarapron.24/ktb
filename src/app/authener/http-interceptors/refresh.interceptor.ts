/** Injectable */
import { Injectable } from '@angular/core';
/** HTTP */
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
} from '@angular/common/http';
/** RxJS */
import { Observable, throwError } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { TokenInterceptor } from './token-interceptor';
import { AuthenticationService } from '../_service/authentication.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ReplaySubject } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class RefreshInterceptor implements HttpInterceptor {
  serviceCount = 0;
  /** Constructor */
  constructor(
    private router: Router,
    private _authenInterceptor: TokenInterceptor,
    private _callbackService: AuthenticationService
  ) { }
  /** Intercept http request w/ set authorized header */
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // Check url white list before
    /** Return interceptor request */
    if (request.url.endsWith('/login') || request.url.indexOf('/state/') >= 0) {
      return next.handle(request);
    } else {
      this.serviceCount = this._callbackService.count;
      return next.handle(request).pipe(
        map((res) => res),
        catchError((err: HttpErrorResponse) => {
          if (err instanceof HttpErrorResponse) {
            if (err.status === 400 || err.status === 401) {
              // const auth = this._callbackService.refreshNewToken();
              return this._callbackService.refreshNewToken().pipe(mergeMap(() => {
                console.log('เข้า map')
                return this._authenInterceptor.intercept(request, next);
              })
              );

              // return next.handle(request);
              // return throwError(err);

            } else if (err.status === 406) {
              this.router.navigate(['error-permission']);
            }
            else {
              return throwError(err);
            }
            /** Throw error */
            return throwError(err);
          }

        })
      );

    }

  }


  /** Matches function */
}

// return this._callbackService.refreshNewToken().pipe(mergeMap(() => {
//     return this._authenInterceptor.intercept(request, next);
//     }));
