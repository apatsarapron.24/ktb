import { Component, OnInit, Inject } from '@angular/core';
import { RouterLink, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthenticationService } from '../_service/authentication.service';

export interface DialogTime {
  time: any
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './alert-login.component.html',
  styleUrls: ['./alert-login.component.scss']
})
export class AlertLoginComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogTime,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<AlertLoginComponent>) {
    this.dialogRef.disableClose = true;
  }

  ngOnInit() {}

  logout() {
    this.dialogRef.close('logout');
  }

  continue() {
    this.dialogRef.close('refresh');
  }
}
