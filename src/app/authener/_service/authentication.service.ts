import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams, HttpEvent } from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import { environment } from '../../../environments/environment';
import { tap } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { AlertLoginComponent } from '../alert-login/alert-login.component';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  count = 30 * 60 * 1000;
  mins = 0;
  seconds = 0;
  timnew: any;
  TimerCountdown = 0;
  start: any;
  constructor(private http: HttpClient,
    private dialog: MatDialog) { }

  apiUrl = environment.apiUrl;
  // HttpOptions = {
  //   headers: new HttpHeaders({
  //     'Content-Type': 'application/json',
  //     'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
  //   })
  // };

  countTimer() {
    // this.count = 30 * 60 * 1000;
    this.mins = 1800 / 60;
    this.seconds = 1800 % 60;
    this.start = setInterval(() => {
      this.timnew = Number(new Date().getHours() * 60 * 60) +
        Number(new Date().getMinutes() * 60) + Number(new Date().getSeconds());
      // console.log('login::', sessionStorage.getItem('timeLogin'));
      // console.log('timer::', this.timnew);
      this.TimerCountdown = this.timnew - Number(localStorage.getItem('timeLogin'));
      // console.log('TimerCountdown::', this.TimerCountdown);
      this.mins = Math.floor((1800 - this.TimerCountdown) / 60);
      this.seconds = (1800 - this.TimerCountdown) % 60;
      // console.log('mins::', this.mins);
      // console.log('seconds::', this.seconds);
      if ((1800 - this.TimerCountdown) <= 0) {
        clearInterval(this.start);
        this.dialog.open(AlertLoginComponent, {
          data: {
            time: Number(1800 - this.TimerCountdown)
          }
        }).afterClosed().subscribe(result => {
          if (result === 'logout') {
            this.logout();
          } else if (result === 'refresh') {
            localStorage.setItem('timeLogin',
              String(
                Number(new Date().getHours() * 60 * 60) +
                Number(new Date().getMinutes() * 60) +
                Number(new Date().getSeconds())
              )
            );
            localStorage.setItem('Time',
              String(
                new Date().getHours() + ':' +
                new Date().getMinutes() + ':' +
                new Date().getSeconds()
              )
            );
            this.countTimer();
          }
        }, (err) => {
          // console.log(err)
          if (err === 'logout') {
            this.dialog.closeAll();
            this.logout();
          } else {
            this.dialog.closeAll();
            localStorage.setItem('timeLogin',
              String(
                Number(new Date().getHours() * 60 * 60) +
                Number(new Date().getMinutes() * 60) +
                Number(new Date().getSeconds())
              )
            );
            localStorage.setItem('Time',
              String(
                new Date().getHours() + ':' +
                new Date().getMinutes() + ':' +
                new Date().getSeconds()
              )
            );
            this.countTimer();
          }
        });
      }
      localStorage.setItem('countTimer', String(1800 - this.TimerCountdown));
    }, 1000);
  }
  login(username: string, password: string): Observable<{}> {
    // console.log(username, password);

    return this.http.post(this.apiUrl + '/pps/auth/login', {
      username: username,
      password: password
    });
  }

  logout() {
    // clearInterval(this.start);

    this.http.get(this.apiUrl + '/api/pps/auth/logOut').subscribe(res => {
      // console.log(res)
      if (res['status'] === 'success') {
      // this.removeAuthorizationToken();
      // this.removeSession();
      location.href = '/login';

      }
    }, (err) => {
      console.log(err)
    });

  }

  GetRole() {
    return this.http.get(this.apiUrl + '/api/pps/auth/get-role');
  }

  SelectRole(id) {
    console.log('Id:', id);
    return this.http.post(this.apiUrl + '/api/pps/auth/add-token-role',
      {
        'id': id
      });
  }
  refreshNewToken() {
    const url = this.apiUrl + '/api/pps/auth/refresh-token';
    const refreshObservable = this.http.get(url);
    const refreshSubject = new ReplaySubject(1);
    console.log('refreshObservable', refreshObservable);
    console.log('refreshSubject', refreshSubject);
    refreshObservable.subscribe(r => {
      console.log('refreshSubject>>>>', r);
      if (r['status'] === 'success') {
        if (r['access_token']) {
          // this.setAuthorizationToken(r);
          localStorage.setItem('accessToken', r['access_token']);
          console.log('>>>>>>>????', r['access_token'])
        }
      }
    }, (err) => {
      if (err instanceof HttpErrorResponse) {
        if (err.status === 401 || err.status === 403 || err.status === 405) {
          console.log('?????');
          this.logout();
          location.href = '/login';
        } else {
          console.log('else ?????');
          this.logout();
          location.href = '/login';
        }
      }
    }
    );
    console.log('err.status');
    return refreshObservable;
  }

  setAuthorizationToken(token: {}) {
    console.log(token);
    localStorage.setItem('accessToken', token['access_token']);
    localStorage.setItem('uid', token['uid']);
    localStorage.setItem('role', token['role']);
    localStorage.setItem('level', token['level']);
    localStorage.setItem('role_detail', token['role_detail']);
    localStorage.setItem('ouName', token['ouName']);
    localStorage.setItem('fullname', token['fullname']);
    // localStorage.setItem('refreshToken', token['refresh_token']);
  }

  removeAuthorizationToken() {
    localStorage.clear();
    clearInterval(this.start);
  }
  removeSession() {
    sessionStorage.clear();
  }
  getAccessToken(): string {
    return localStorage.getItem('access_token');
  }

  isLoggedIn(): boolean {
    /** Check valid parameter */
    const access_token = localStorage.getItem('accessToken');
    const uid = localStorage.getItem('uid');
    const expire_at = localStorage.getItem('expireAt');
    if (access_token && uid && expire_at) {
      return true;
    }
    return false;
  }
}
