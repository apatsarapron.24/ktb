import { TestBed } from '@angular/core/testing';

import { AuthenerGuardsService } from './authener-guards.service';

describe('AuthenerGuardsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuthenerGuardsService = TestBed.get(AuthenerGuardsService);
    expect(service).toBeTruthy();
  });
});
