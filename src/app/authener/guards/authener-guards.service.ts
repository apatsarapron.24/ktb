import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../_service/authentication.service';
// import { HeaderBarComponent } from '../../containers/customers-layout/header-bar/header-bar.component';

@Injectable({
  providedIn: 'root'
})
export class AuthenerGuardsService implements CanActivate {

  constructor(private _authenService: AuthenticationService,
    private route: Router, private _router: Router,
    // private headerBar: HeaderBarComponent,
  ) {

  }
  expire = '';
  token = '';
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    this.expire = localStorage.getItem('expirationDate');
    this.token = localStorage.getItem('accessToken');
    console.log('call guards');
    console.log('next>>>>>>', location.pathname);

    // this.checkPermissionPath();
    if (this.checkToken()) {
      console.log('Auth');
      return true;
    } else {
      this._router.navigate(['/login']);
    }




    return false;
  }

  checkAlreadyExpire(): boolean {
    if (this.expire === 'undefined') {
      return false;
    }
    const timeNow = new Date;
    const timeExpire = new Date(this.expire);
    // console.log(this.expire, timeNow, timeExpire);
    return timeNow > timeExpire ? true : false;
  }

  checkRequestId() {
    localStorage.removeItem('requestId');
  }

  checkPermissionPath() {
    // this.headerBar.ngOnInit()
  }

  checkToken(): boolean {
    if (this.token.length > 0 && this.token !== 'undefined') {
      return true;
    }
    console.log('token is undefined or incorect');
    return false;
  }

}
