import { TestBed, inject } from '@angular/core/testing';

import { ResoiutiondataService } from './resoiutiondata.service';

describe('ResoiutiondataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ResoiutiondataService]
    });
  });

  it('should be created', inject([ResoiutiondataService], (service: ResoiutiondataService) => {
    expect(service).toBeTruthy();
  }));
});
